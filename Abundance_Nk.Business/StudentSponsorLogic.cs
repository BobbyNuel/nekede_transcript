﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class StudentSponsorLogic : BusinessBaseLogic<StudentSponsor, STUDENT_SPONSOR>
    {
        public StudentSponsorLogic()
        {
            translator = new StudentSponsorTranslator();
        }

        public bool Modify(StudentSponsor studentSponsor)
        {
            try
            {
                STUDENT_SPONSOR entity = GetEntityBy(s => s.Person_Id == studentSponsor.Student.Id);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Sponsor_Name = studentSponsor.Name;
                entity.Sponsor_Contact_Address = studentSponsor.ContactAddress;
                entity.Sponsor_Mobile_Phone = studentSponsor.MobilePhone;
                entity.Email = studentSponsor.Email;

                if (studentSponsor.Relationship != null && studentSponsor.Relationship.Id > 0)
                {
                    entity.Relationship_Id = studentSponsor.Relationship.Id;
                }

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
