﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class StudentLogic : BusinessBaseLogic<Student, STUDENT>
    {
        private PersonLogic personLogic;
        private StudentMatricNumberAssignmentLogic studentMatricNumberAssignmentLogic;

        public StudentLogic()
        {
            personLogic = new PersonLogic();
            translator = new StudentTranslator();
            studentMatricNumberAssignmentLogic = new StudentMatricNumberAssignmentLogic();
        }

        public Student GetBy(string matricNumber)
        {
            try
            {
                Expression<Func<STUDENT, bool>> selector = s => s.Matric_Number == matricNumber;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Student GetBy(long id)
        {
            try
            {
                Expression<Func<STUDENT, bool>> selector = s => s.Person_Id == id;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(Student student)
        {
            try
            {
                STUDENT entity = GetEntityBy(s => s.Person_Id == student.Id);
                entity.Person_Id = student.Id;
                entity.Student_Number = student.Number;
                entity.Matric_Number = student.MatricNumber;
                entity.School_Contact_Address = student.SchoolContactAddress;
                
                if (student.Status != null && student.Status.Id >0)
                {
                    entity.Student_Status_Id = student.Status.Id;
                }
                if (student.Type != null && student.Type.Id >0 )
                {
                    entity.Student_Type_Id = student.Type.Id;
                }
                if (student.Category!= null && student.Category.Id >0)
                {
                    entity.Student_Category_Id = student.Category.Id;
                }
                if (student.Title != null && student.Title.Id > 0)
                {
                    entity.Title_Id = student.Title.Id;
                }
                if (student.MaritalStatus != null && student.MaritalStatus.Id > 0)
                {
                    entity.Marital_Status_Id = student.MaritalStatus.Id;
                }
                if (student.BloodGroup != null && student.BloodGroup.Id > 0)
                {
                    entity.Blood_Group_Id = student.BloodGroup.Id;
                }
                if (student.Genotype != null && student.Genotype.Id > 0)
                {
                    entity.Genotype_Id = student.Genotype.Id;
                }
                if (student.ApplicationForm != null && student.ApplicationForm.Id > 0)
                {
                    entity.Application_Form_Id = student.ApplicationForm.Id;
                }
                
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                   return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public override Student Add(Student student)
        {
            try
            {
                Person person = personLogic.Create(student);
                student.Id = person.Id;

                Student newStudent = base.Create(student);
                if (newStudent != null)
                {
                    newStudent.FullName = person.FullName;
                }

                return newStudent;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AssignMatricNumber(Person person, long studentNumber, string matricNumber)
        {
            try
            {
                STUDENT studentEntity = GetEntityBy(s => s.Person_Id == person.Id);
                studentEntity.Matric_Number = matricNumber;
                studentEntity.Student_Number = studentNumber;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AssignMatricNumber(ApplicationFormView applicant)
        {
            try
            {
                //assign matric no to applicant
                Faculty faculty = new Faculty() { Id = applicant.FacultyId };
                Department department = new Department() { Id = applicant.DepartmentId };
                Session session = new Session() { Id = applicant.SessionId };
                Programme programme = new Programme() { Id = applicant.ProgrammeId };
                Level level;

                if (applicant.ProgrammeId > 2)
                {
                    level = new Level { Id = 3 };
                }
                else
                {
                    level = new Level { Id = 1 };
                }
               

                StudentMatricNumberAssignment startMatricNo = studentMatricNumberAssignmentLogic.GetBy(faculty, department, level, session);
                if (startMatricNo != null)
                {
                    long studentNumber = 0;
                    string matricNumber = "";

                    if (startMatricNo.Used)
                    {
                        string[] matricNoArray = startMatricNo.MatricNoStartFrom.Split('/');

                        studentNumber = GetNextStudentNumber(faculty, department, level, session);
                        matricNoArray[matricNoArray.Length - 1] = UtilityLogic.PaddNumber(studentNumber, 4);
                        matricNumber = string.Join("/", matricNoArray);
                    }
                    else
                    {
                        matricNumber = startMatricNo.MatricNoStartFrom;
                        studentNumber = startMatricNo.MatricSerialNoStartFrom;
                        bool markedAsUsed = studentMatricNumberAssignmentLogic.MarkAsUsed(startMatricNo);
                    }

                    Student student = new Student();
                    student.Id = applicant.PersonId;
                    student.ApplicationForm = new ApplicationForm() { Id = applicant.FormId };
                    student.Type = new StudentType() { Id = 1 };
                    student.Category = new StudentCategory() { Id = applicant.ProgrammeId <= 2 ? 1 : 2 };
                    student.Status = new StudentStatus() { Id = 1 };
                    student.Number = studentNumber;
                    student.MatricNumber = matricNumber;
                    Student newStudent = base.Create(student);


                    StudentLevel studentLevel = new StudentLevel();
                    studentLevel.Session = new Session() { Id = 1 };
                    studentLevel.Level = new Level() { Id = applicant.ProgrammeId <= 2 ? 1 : 3 };
                    studentLevel.Student = student;
                    studentLevel.Department = department;
                    studentLevel.Programme = programme;

                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    return studentLevelLogic.Create(studentLevel) != null ? true : false;
                }
                else
                {
                    throw new Exception(applicant.LevelName + " for " + applicant.DepartmentName + " for the current academic session has not been set! Please contact your system administrator.");
                }
            }
            catch(Exception)
            {
                throw;
            }
        }

        public long GetNextStudentNumber(Faculty faculty, Department department, Level level, Session session)
        {
            try
            {
                long newStudentNumber = 0;
                List<ApplicationFormView> applicationForms = (from a in repository.GetBy<VW_ASSIGNED_MATRIC_NUMBER>(a => a.Faculty_Id == faculty.Id && a.Department_Id == department.Id && a.Level_Id == level.Id && a.Session_Id == session.Id)
                                                              select new ApplicationFormView
                                                              {
                                                                  FormId = a.Application_Form_Id.Value,
                                                                  StudentNumber = a.Student_Number,
                                                                  MatricNumber = a.Matric_Number,
                                                                  PersonId = a.Person_Id,
                                                              }).ToList();

                if (applicationForms != null && applicationForms.Count > 0)
                {
                    long rawMaxStudentNumber = applicationForms.Max(s => s.StudentNumber);
                    newStudentNumber = rawMaxStudentNumber + 1;
                }

                return newStudentNumber;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Student CreateStudent(Student model)
        {
            var record = GetBy(model.MatricNumber);
            if (record != null && record.Id > 0)
            {
                return record;
            }
            else
            {
                return base.Create(model);
            }
            
        }

        



        //public override void ModifyHelper(Student student)
        //{
        //    try
        //    {
        //        personBizLogic.Modify(student);
        //        STUDENT studentEntity = GetEntityBy(s => s.Person_Id == student.Id);

        //        studentEntity.Student_Type_Id = student.Type.Id;
        //        studentEntity.Student_Category_Id = student.Category.Id;
        //        studentEntity.Student_Number = student.Number;
        //        studentEntity.Admission_Number = student.AdmissionNumber;
        //        studentEntity.Pay_School_Fees = student.PaySchoolFees;
        //        studentEntity.IsStudent = student.IsStudent;
        //        studentEntity.Are_You_A_Catholic = student.AreYouACatholic;
        //        studentEntity.Date_Confirmed = student.DateConfirmed;
        //        studentEntity.Date_Baptised = student.DateBaptised;
        //        studentEntity.Date_Of_First_Holy_Communion = student.DateOfFirstHolyCommunion;
        //        studentEntity.Denomination = student.Denomination;

        //        student.Sponsor.Student = student;
        //        SPONSOR sponsorEntity = sponsorLogic.GetEntityBy(s => s.Person_Id == student.Id);
        //        if (sponsorEntity != null)
        //        {
        //            student.Sponsor.Id = sponsorEntity.Sponsor_Id;
        //            sponsorLogic.Modify(student.Sponsor);
        //        }
        //        else
        //        {
        //            sponsorLogic.Add(student.Sponsor);
        //        }


        //        student.Education.Student = student;
        //        STUDENT_EDUCATION studentEducationEntity = studentEducationLogic.GetEntityBy(s => s.Person_Id == student.Id);
        //        if (studentEducationEntity != null)
        //        {
        //            student.Education.Id = studentEducationEntity.Education_History_Id;
        //            studentEducationLogic.Modify(student.Education);
        //        }
        //        else
        //        {
        //            studentEducationLogic.Add(student.Education);
        //        }


        //        student.Health.Id = student.Id;
        //        STUDENT_HEALTH studentHealthEntity = studentHealthLogic.GetEntityBy(s => s.Person_Id == student.Id);
        //        if (studentHealthEntity != null)
        //        {
        //            studentHealthLogic.Modify(student.Health);
        //        }
        //        else
        //        {
        //            studentHealthLogic.Add(student.Health);
        //        }


        //        student.Term.Student = student;
        //        STUDENT_TERM studentTermEntity = studentTermLogic.GetEntityBy(s => s.Person_Id == student.Id);
        //        if (studentTermEntity != null)
        //        {
        //            studentTermLogic.Modify(student.Term);
        //        }
        //        else
        //        {
        //            studentTermLogic.Add(student.Term);
        //        }


        //        student.Level.Student = student;
        //        STUDENT_LEVEL studentLevelEntity = studentLevelLogic.GetEntityBy(s => s.Person_Id == student.Id);
        //        if (studentLevelEntity != null)
        //        {
        //            studentLevelLogic.Modify(student.Level);
        //        }
        //        else
        //        {
        //            studentLevelLogic.Add(student.Level);
        //        }

        //        //student.Payment.Student = student;
        //        STUDENT_PAYMENT studentPaymentEntity = studentPaymentLogic.GetEntityBy(s => s.Payment_Id == student.Payment.Id);
        //        if (studentPaymentEntity != null)
        //        {
        //            studentPaymentLogic.Modify(student.Payment);
        //        }
        //        else
        //        {
        //            studentPaymentLogic.Add(student.Payment);
        //        }

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}


    }
}
