﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class TranscriptRequestLogic : BusinessBaseLogic<TranscriptRequest,TRANSCRIPT_REQUEST>
    {
        public TranscriptRequestLogic()
        {
            translator = new TranscriptRequestTranslator();
        }

        public TranscriptRequest GetBy(long Id)
        {
            TranscriptRequest request = null;
            try
            {
                request = GetModelBy(a => a.Transcript_Request_Id == Id);
                request.PaymentInterswitch = SetPaymentInterswitch(request);
            }
            catch (Exception)
            {
                
                throw;
            }
            return request;
        }

        public TranscriptRequest GetBy(Payment payment)
        {
            TranscriptRequest request = null;
            try
            {
                request = GetModelBy(a => a.Payment_Id == payment.Id);
                request.PaymentInterswitch = SetPaymentInterswitch(request);
            }
            catch (Exception)
            {
                
                throw;
            }
            return request;
        }

        public List<TranscriptRequest> GetBy(Student student)
        {
            List<TranscriptRequest> request = new List<TranscriptRequest>();
            try
            {
               var data = GetModelsBy(a => a.Student_id == student.Id);
                foreach (var transcriptRequest in data)
                {
                    transcriptRequest.PaymentInterswitch = SetPaymentInterswitch(transcriptRequest);
                    request.Add(transcriptRequest);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return request;
        }

        private static PaymentInterswitch SetPaymentInterswitch(TranscriptRequest transcriptRequest)
        {
            PaymentInterswitch paymentInterswitch = new PaymentInterswitch(); ;
            try
            {
                //PaymentInterswitchLogic paymentInterswitchLogic = new PaymentInterswitchLogic();
                //return paymentInterswitchLogic.GetBy(transcriptRequest.payment);

                string responseCode = null, responseDescription = null;

                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                RemitaPayment remitaPayment = remitaPaymentLogic.GetModelBy(p => p.Payment_Id == transcriptRequest.payment.Id);
                if (remitaPayment != null)
                {
                    if (remitaPayment.Status.Contains("00"))
                    {
                        responseCode = "00";
                        responseDescription = "Transaction Completed Successfully.";
                    }
                    else if (remitaPayment.Status.Contains("01"))
                    {
                        responseCode = "01";
                        responseDescription = "Transaction Approved.";
                    }
                    else if (remitaPayment.Status == "02")
                    {
                        responseCode = "02";
                        responseDescription = "Transaction Failed.";
                    }
                    else if (remitaPayment.Status.Contains("021"))
                    {
                        responseCode = "021";
                        responseDescription = "Transaction Pending.";
                    }
                    else if (remitaPayment.Status.Contains("025"))
                    {
                        responseCode = "025";
                        responseDescription = "Payment Reference Generated.";
                    }
                    else
                    {
                        responseCode = remitaPayment.Status;
                    }

                    paymentInterswitch.RemitaOrderId = remitaPayment.OrderId;
                }

                paymentInterswitch.ResponseCode = responseCode;
                paymentInterswitch.ResponseDescription = responseDescription;
            }
            catch (Exception)
            {
                throw;
            }

            return paymentInterswitch;
        }

        public bool Modify (TranscriptRequest model)
        {
            try
            {
                Expression<Func<TRANSCRIPT_REQUEST, bool>> selector = af => af.Transcript_Request_Id == model.Id;
                TRANSCRIPT_REQUEST entity = GetEntityBy(selector);
                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                if (model.payment != null)
                {
                    entity.Payment_Id = model.payment.Id;
                }

                entity.Receiver = model.Receiver;
                entity.Destination_Address = model.DestinationAddress;
                entity.Destination_State_Id = model.DestinationState.Id;
                entity.Destination_Country_Id = model.DestinationCountry.Id;
                entity.Transcript_clearance_Status_Id = model.transcriptClearanceStatus.TranscriptClearanceStatusId;
                entity.Transcript_Status_Id = model.transcriptStatus.TranscriptStatusId;
                entity.Verified = model.Verified;
                entity.Processed = model.Processed;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
  
    }
}
