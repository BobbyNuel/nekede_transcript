﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class ApplicationFormLogic : BusinessBaseLogic<ApplicationForm, APPLICATION_FORM>
    {
        private CardPaymentLogic cardPaymentLogic;
        private ScratchCardLogic scratchCardLogic;
        private PaymentLogic paymentLogic;

        public ApplicationFormLogic()
        {
            translator = new ApplicationFormTranslator();
            scratchCardLogic = new ScratchCardLogic();
            cardPaymentLogic = new CardPaymentLogic();
            paymentLogic = new PaymentLogic();
        }

        public ApplicationForm GetBy(long applicationFormId)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = a => a.Application_Form_Id == applicationFormId;

                ApplicationForm applicationForm = GetModelBy(selector);
                if (applicationForm != null && applicationForm.Id > 0)
                {
                    applicationForm.Payment = paymentLogic.GetBy(applicationForm.Payment.Id);
                }

                return applicationForm;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ApplicationForm GetBy(string applicationFormNumber)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = a => a.Application_Form_Number == applicationFormNumber;

                ApplicationForm applicationForm = GetModelBy(selector);
                if (applicationForm != null && applicationForm.Id > 0)
                {
                    applicationForm.Payment = paymentLogic.GetBy(applicationForm.Payment.Id);
                }

                return applicationForm;
            }
            catch (Exception)
            {
                throw;
            }
        }

       

        public ApplicationForm GetBy(Person person)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = a => a.Person_Id == person.Id;

                ApplicationForm applicationForm = GetModelBy(selector);
                if (applicationForm != null && applicationForm.Id > 0)
                {
                    applicationForm.Payment = paymentLogic.GetBy(applicationForm.Payment.Id);
                }

                return applicationForm;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool IsValidApplicationNumberAndPin(string applicationNumber, string pin)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = af => af.Application_Form_Number == applicationNumber;
                ApplicationForm applicationForm = GetModelBy(selector);
                if (applicationForm == null)
                {
                    return false;
                }

                Payment payment = InvalidConfirmationOrderNumber(pin, applicationForm,5);
                if (payment == null || payment.Id <= 0)
                {
                    return false;
                }

               
                if (payment != null && payment.Id > 0 && applicationForm.Id > 0)
                {
                    PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
                    bool pinUseStatus = etranzactLogic.IsPinUsed(pin, (int)payment.Person.Id);
                    if (pinUseStatus)
                    {
                        return false;
                    }

                   etranzactLogic.UpdatePin(payment, payment.Person);
                   return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Payment InvalidConfirmationOrderNumber(string confirmationOrderNumber, ApplicationForm applicant ,int feeType_Id)
        {
            Payment payment = new Payment();
            PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
            PaymentEtranzact etranzactDetails = etranzactLogic.GetModelBy(m => m.Confirmation_No == confirmationOrderNumber);
            if (etranzactDetails == null || etranzactDetails.ReceiptNo == null)
            {
                PaymentTerminal paymentTerminal = new PaymentTerminal();
                PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == feeType_Id && p.Session_Id == applicant.ProgrammeFee.Session.Id);

                etranzactDetails = etranzactLogic.RetrievePinsWithoutInvoice(confirmationOrderNumber,applicant,feeType_Id, paymentTerminal);
                if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                    if (payment != null && payment.Id > 0)
                    {
                        FeeDetail feeDetail = new FeeDetail();
                        FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                        feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id);
                        if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                        {
                            throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");
                           
                        }

                    }
                    else
                    {
                        throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");

                    }
                }
                else
                {
                    throw new Exception("Confirmation Order Number entered seems not to be valid! Please cross check and try again.");

                }
            }
            else
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                if (payment != null && payment.Id > 0)
                {
                    FeeDetail feeDetail = new FeeDetail();
                    FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                    feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id);

                    if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                    {
                        throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");
                       
                    }
                }
                else
                {
                    throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");
                   
                }
            }

            return payment;
        }
       

        public ApplicationForm Create(ApplicationForm form, AppliedCourse appliedCourse)
        {
            try
            {
                ApplicationForm newForm = base.Create(form);
                if (newForm == null || newForm.Id <= 0)
                {
                    throw new Exception("Application Form creation failed!");
                }

                newForm.Setting = form.Setting;
                newForm.ProgrammeFee = form.ProgrammeFee;
                newForm = SetNextApplicationFormNumber(newForm);
                newForm = SetNextExamNumber(newForm, appliedCourse);

                SetApplicationAndExamNumber(newForm);

                return newForm;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ApplicationFormSummary> GetSummary(Session session)
        {
            try
            {
                List<ApplicationFormSummary> applicationForms = (from a in repository.GetBy<VW_APPLICATION_FORM_SUMMARY>(a => a.Session_Id == session.Id)
                                                                 select new ApplicationFormSummary
                                                              {
                                                                  ProgrammeId = a.Programme_Id,
                                                                  ProgrammeName = a.Programme_Name,
                                                                  DepartmentName = a.Department_Name,
                                                                  SessionName = a.Session_Name,
                                                                  FormCount = (int)a.Application_Form_Count,
                                                              }).ToList();

                return applicationForms;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ApplicationForm SetNextExamNumber(ApplicationForm form, AppliedCourse appliedCourse)
        {
            try
            {
                int newExamSerialNumber = 0;
                List<ApplicationForm> applicationForms = (from a in repository.GetBy<VW_APPLICATION_EXAM_NUMBER>(a => a.Application_Form_Setting_Id == form.Setting.Id && a.Programme_Id == appliedCourse.Programme.Id && a.Department_Id == appliedCourse.Department.Id)
                                                          select new ApplicationForm
                                                          {
                                                              Id = a.Application_Form_Id,
                                                              SerialNumber = a.Serial_Number,
                                                              ExamSerialNumber = a.Exam_Serial_Number,
                                                              ExamNumber = a.Exam_Number,
                                                          }).ToList();

                if (applicationForms != null && applicationForms.Count > 0)
                {
                    int rawMaxExamSerialNumber = applicationForms.Max(s => (int)s.ExamSerialNumber);
                    newExamSerialNumber = rawMaxExamSerialNumber + 1;
                }
                else
                {
                    newExamSerialNumber = 1;
                }

                form.ExamSerialNumber = newExamSerialNumber;
                form.ExamNumber = appliedCourse.Department.Code + PaddNumber(newExamSerialNumber, 5);

                return form;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SetRejectReason(ApplicationForm form)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = a => a.Application_Form_Id == form.Id;
                APPLICATION_FORM entity = GetEntityBy(selector);

                //if (entity == null)
                //{
                //    throw new Exception(NoItemFound);
                //}

                entity.Reject_Reason = form.RejectReason;
                entity.Rejected = form.Rejected;
                entity.Release = form.Release;

                int modifiedRecordCount = Save();
                //if (modifiedRecordCount <= 0)
                //{
                //    throw new Exception(NoItemModified);
                //}

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
         
        public bool AcceptOrReject(List<ApplicationForm> applications, bool accept)
        {
            try
            {
                if (applications == null || applications.Count <= 0)
                {
                    throw new Exception("No aplication found to Accept!");
                }

                bool done = false;
                foreach(ApplicationForm application in applications)
                {
                    Expression<Func<APPLICATION_FORM, bool>> selector = a => a.Application_Form_Id == application.Id;
                    APPLICATION_FORM entity = GetEntityBy(selector);

                    if (entity != null)
                    {
                        entity.Rejected = accept;
                    }

                    done = repository.Save() > 0 ? true : false;
                    
                }

                return done;
            }
            catch (Exception)
            {
                throw;
            }
        }


        //public ApplicationForm SetNextApplicationExamNumber(ApplicationForm form)
        //{
        //    try
        //    {
        //        long newSerialNumber = 0;
        //        Func<APPLICATION_FORM, long> selector = s => s.Serial_Number;
        //        long rawMaxSerialNumber = repository.GetMaxValueBy(selector);
        //        if (rawMaxSerialNumber > 0)
        //        {
        //            newSerialNumber = rawMaxSerialNumber + 1;
        //        }
        //        else
        //        {
        //            newSerialNumber = 1;
        //        }

        //        form.SerialNumber = newSerialNumber;
        //        form.ExamNumber = "FPN/" + form.ProgrammeFee.Programme.ShortName + "/" + DateTime.Now.ToString("yy") + "/" + PaddNumber(newSerialNumber, 10);

        //        return form;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public ApplicationForm SetNextApplicationFormNumber(ApplicationForm form)
        {
            try
            {
                long newSerialNumber = form.Id;
                //if (newSerialNumber <= 0)
                //{
                //    newSerialNumber = 1;
                //}

                form.SerialNumber = newSerialNumber;
                form.Number = "FPN/" + form.ProgrammeFee.Programme.ShortName + "/" + DateTime.Now.ToString("yy") + "/" + PaddNumber(newSerialNumber, 10);

                return form;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public ApplicationForm SetNextApplicationFormNumber(ApplicationForm form)
        //{
        //    try
        //    {
        //        long newSerialNumber = repository.GetCount<APPLICATION_FORM>(f => f.Application_Form_Id <= form.Id);
        //        if (newSerialNumber <= 0)
        //        {
        //            newSerialNumber = 1;
        //        }

        //        form.SerialNumber = newSerialNumber;
        //        form.Number = "FPN/" + form.ProgrammeFee.Programme.ShortName + "/" + DateTime.Now.ToString("yy") + "/" + PaddNumber(newSerialNumber, 10);

        //        return form;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}


        //public ApplicationForm SetNextApplicationFormNumber(ApplicationForm form)
        //{
        //    try
        //    {
        //        long newSerialNumber = 0;
        //        Func<APPLICATION_FORM, long> selector = s => s.Serial_Number;
        //        long rawMaxSerialNumber = repository.GetMaxValueBy(selector);
        //        if (rawMaxSerialNumber > 0)
        //        {
        //            newSerialNumber = rawMaxSerialNumber + 1;
        //        }
        //        else
        //        {
        //            newSerialNumber = 1;
        //        }

        //        form.SerialNumber = newSerialNumber;
        //        form.Number = "FPN/" + form.ProgrammeFee.Programme.ShortName + "/" + DateTime.Now.ToString("yy") + "/" + PaddNumber(newSerialNumber, 10);
               
        //        return form;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        public static string PaddNumber(long id, int maxCount)
        {
            try
            {
                string idInString = id.ToString();
                string paddNumbers = "";
                if (idInString.Count() < maxCount)
                {
                    int zeroCount = maxCount - id.ToString().Count();
                    StringBuilder builder = new StringBuilder();
                    for (int counter = 0; counter < zeroCount; counter++)
                    {
                        builder.Append("0");
                    }

                    builder.Append(id);
                    paddNumbers = builder.ToString();
                    return paddNumbers;
                }

                return paddNumbers;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public List<PhotoCard> GetPostJAMBApplications(Session session)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id)
                              select new PhotoCard
                              {
                                  PersonId = a.Person_Id,
                                  Name = a.Name,
                                  AplicationNumber = a.Application_Form_Id,
                                  PaymentNumber = a.Payment_Id,
                                  FirstChoiceDepartment = a.First_Choice_Department_Name,
                                  MobilePhone = a.Mobile_Phone,
                                  AppliedProgrammeName = a.Programme_Name,
                                  PassportUrl = a.Image_File_Url,
                                  AplicationFormNumber = a.Application_Form_Number,
                              };

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PhotoCard> GetPostJAMBApplicationsBy(Session session, Programme programme, SortOption sortOpton)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id)
                                   select new PhotoCard
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       AplicationSerialNumber = a.Serial_Number,
                                       ExamNumber = a.Exam_Number,
                                       ExamSerialNumber = a.Exam_Serial_Number,
                                   };

                applications = SortApplicantList(sortOpton, applications);

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PhotoCard> GetPostJAMBApplicationsBy(Session session, Programme programme, Department department, SortOption sortOpton)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id)
                                   select new PhotoCard
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       AplicationSerialNumber = a.Serial_Number,
                                       ExamNumber = a.Exam_Number,
                                       ExamSerialNumber = a.Exam_Serial_Number,
                                   };

                applications = SortApplicantList(sortOpton, applications);

                //var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id)
                //                   select new PhotoCard
                //                   {
                //                       PersonId = a.Person_Id,
                //                       Name = a.Name,
                //                       AplicationNumber = a.Application_Form_Id,
                //                       PaymentNumber = a.Payment_Id,
                //                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                //                       MobilePhone = a.Mobile_Phone,
                //                       AppliedProgrammeName = a.Programme_Name,
                //                       PassportUrl = a.Image_File_Url,
                //                       AplicationFormNumber = a.Application_Form_Number,
                //                       SessionName = a.Session_Name,
                //                       AplicationSerialNumber = a.Serial_Number,
                //                       ExamNumber = a.Exam_Number,
                //                       ExamSerialNumber = a.Exam_Serial_Number,
                //                   };

                //applications = SortApplicantList(sortOpton, applications);

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private IEnumerable<PhotoCard> SortApplicantList(SortOption sortOpton, IEnumerable<PhotoCard> applications)
        {
            try
            {
                if (applications != null && applications.Count() > 0)
                {
                    switch (sortOpton)
                    {
                        case SortOption.Name:
                            {
                                applications = applications.OrderBy(a => a.Name);
                                break;
                            }
                        case SortOption.ExamNo:
                            {
                                applications = applications.OrderBy(a => a.ExamSerialNumber);
                                break;
                            }
                        case SortOption.ApplicationNo:
                            {
                                applications = applications.OrderBy(a => a.AplicationSerialNumber);
                                break;
                            }
                    }
                }

                return applications;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PhotoCard> GetPostJAMBApplicationsBy(Session session, Programme programme)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id)
                                   select new PhotoCard
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       ExamNumber = a.Exam_Number,
                                   };

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<PhotoCard>> GetPostJAMBApplicationsAsyncBy(Session session, Programme programme, Department department)
        {
            try
            {
                //List<VW_POST_JAMP_APPLICATION> applications = await repository.GetAsyncBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id);


               var applications = (from a in await repository.GetAsyncBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id)
                                                      select new PhotoCard
                                                      {
                                                          PersonId = a.Person_Id,
                                                          Name = a.Name,
                                                          AplicationNumber = a.Application_Form_Id,
                                                          PaymentNumber = a.Payment_Id,
                                                          FirstChoiceDepartment = a.First_Choice_Department_Name,
                                                          MobilePhone = a.Mobile_Phone,
                                                          AppliedProgrammeName = a.Programme_Name,
                                                          PassportUrl = a.Image_File_Url,
                                                          AplicationFormNumber = a.Application_Form_Number,
                                                          SessionName = a.Session_Name,
                                                          ExamNumber = a.Exam_Number,
                                                      });


                if (applications != null && applications.Count() > 0)
                {
                    applications = applications.OrderBy(a => a.Name);
                }

                return applications;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PhotoCard> GetPostJAMBApplicationsBy(Session session, Programme programme, Department department)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id)
                                   select new PhotoCard
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       ExamNumber = a.Exam_Number,
                                   };

                if (applications != null && applications.Count() > 0)
                {
                    applications = applications.OrderBy(a => a.Name);
                }

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<Slug> GetPostJAMBSlugDataBy(Session session, Programme programme, Department department)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id)
                                   select new Slug
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       ExamNumber = a.Exam_Number,
                                       JambNumber = a.Applicant_Jamb_Registration_Number,
                                       JambScore = a.Applicant_Jamb_Score,
                                   };
                if (applications != null && applications.Count() > 0)
                {
                    applications = applications.OrderBy(a => a.ExamNumber);
                }

                return applications.ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }



        private APPLICATION_FORM GetEntityBy(Person person)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = s => s.Person_Id == person.Id;
                APPLICATION_FORM entity = GetEntityBy(selector);

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SetApplicationAndExamNumber(ApplicationForm applicationForm)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = af => af.Application_Form_Id == applicationForm.Id;
                APPLICATION_FORM entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Application_Form_Number = applicationForm.Number;
                entity.Serial_Number = applicationForm.SerialNumber;
                entity.Exam_Number = applicationForm.ExamNumber;
                entity.Exam_Serial_Number = applicationForm.ExamSerialNumber;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool Modify(ApplicationForm applicationForm)
        {
            try
            {
                APPLICATION_FORM entity = GetEntityBy(applicationForm.Person);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Exam_Number = applicationForm.ExamNumber;
                entity.Exam_Serial_Number = applicationForm.ExamSerialNumber;
                entity.Release = applicationForm.Release;
                entity.Rejected = applicationForm.Rejected;
                entity.Reject_Reason = applicationForm.RejectReason;
                entity.Remarks = applicationForm.Remarks;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<PhotoCard> GetApplicantList(Session session, Programme programme, Department department, DateTime dateFrom, DateTime dateTo)
        {
            List<PhotoCard> applicants = new List<PhotoCard>();
            try
            {
                applicants = (from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id && (a.Date_Submitted >= dateFrom && a.Date_Submitted <= dateTo))
                              select new PhotoCard
                              {
                                  Name = a.Name,
                                  MobilePhone = a.Mobile_Phone,
                                  AplicationFormNumber = a.Application_Form_Number,
                                  ExamNumber = a.Exam_Number,
                                  AppliedProgrammeName = a.Programme_Name,
                                  FirstChoiceDepartment = a.First_Choice_Department_Name,
                                  SessionName = a.Session_Name,
                                  DateSubmitted = a.Date_Submitted.ToLongDateString()
                              }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return applicants.OrderBy(a => a.AppliedProgrammeName).ThenBy(a => a.FirstChoiceDepartment).ThenBy(a => a.AplicationFormNumber).ToList();
        }
        public List<PhotoCard> GetApplicantListBulk(Session session, DateTime dateFrom, DateTime dateTo)
        {
            List<PhotoCard> applicants = new List<PhotoCard>();
            try
            {
                applicants = (from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id && (a.Date_Submitted >= dateFrom ) && ( a.Date_Submitted <= dateTo) )
                              select new PhotoCard
                              {
                                  Name = a.Name,
                                  MobilePhone = a.Mobile_Phone,
                                  AplicationFormNumber = a.Application_Form_Number,
                                  ExamNumber = a.Exam_Number,
                                  AppliedProgrammeName = a.Programme_Name,
                                  FirstChoiceDepartment = a.First_Choice_Department_Name,
                                  SessionName = a.Session_Name,
                                  DateSubmitted = a.Date_Submitted.ToLongDateString()
                              }).ToList();
            }
            catch (Exception)
            {
                throw;
            }

            return applicants.OrderBy(a => a.AppliedProgrammeName).ThenBy(a => a.FirstChoiceDepartment).ThenBy(a => a.AplicationFormNumber).ToList();
        }

        public List<Slug> GetPostJambSlugDataBulk(Session session)
        {
            try
            {
                var applications = from a in repository.GetBy<VW_POST_JAMP_APPLICATION>(a => a.Session_Id == session.Id)
                                   select new Slug
                                   {
                                       PersonId = a.Person_Id,
                                       Name = a.Name,
                                       AplicationNumber = a.Application_Form_Id,
                                       PaymentNumber = a.Payment_Id,
                                       FirstChoiceDepartment = a.First_Choice_Department_Name,
                                       MobilePhone = a.Mobile_Phone,
                                       AppliedProgrammeName = a.Programme_Name,
                                       PassportUrl = a.Image_File_Url,
                                       AplicationFormNumber = a.Application_Form_Number,
                                       SessionName = a.Session_Name,
                                       ExamNumber = a.Exam_Number

                                   };

                if (applications != null && applications.Count() > 0)
                {
                    applications = applications.OrderBy(a => a.ExamNumber);
                }

                return applications.ToList();

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<PutmeResultModel> GetPutmeResults(Session session)
        {
            List<PutmeResultModel> results = new List<PutmeResultModel>();
            try
            {
                if (session != null && session.Id > 0)
                {


                    results = (from a in repository.GetBy<VW_PUTME_RESULT>(a => a.Session_Id == session.Id)
                               select new PutmeResultModel
                               {
                                   Fullname = a.FULLNAME,
                                   PhoneNumber = a.Mobile_Phone,
                                   Sex = a.Sex_Name,
                                   ApplicationNumber = a.Application_Form_Number,
                                   ExamNumber = a.Exam_Number,
                                   ExamScore = a.RAW_SCORE,
                                   JambNumber = a.REGNO,
                                   JambScore = a.JAMB_SCORE,
                                   Department = a.Department_Name
                               }).ToList();
                }
                else
                {
                    results = (from a in repository.GetBy<PUTME_RESULT>()
                               select new PutmeResultModel
                               {
                                   Fullname = a.FULLNAME,
                                   ExamNumber = a.EXAMNO,
                                   ExamScore = a.RAW_SCORE,
                                   Department = a.COURSE,
                                   JambNumber = a.REGNO
                               }).ToList();
                }

            }
            catch (Exception)
            {
                throw;
            }

            return results.OrderBy(a => a.Programme).ThenBy(a => a.Department).ThenBy(a => a.ApplicationNumber).ToList();
        }
 


    }


}
