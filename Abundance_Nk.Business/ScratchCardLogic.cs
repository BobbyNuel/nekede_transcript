﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class ScratchCardLogic : BusinessBaseLogic<ScratchCard, SCRATCH_CARD>
    {
        public ScratchCardLogic()
        {
            base.translator = new ScratchCardTranslator();
        }

        public ScratchCard GetBy(string pin)
        {
            try
            {
                Expression<Func<SCRATCH_CARD, bool>> selector = s => s.Pin == pin;
                return GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }






    }
}
