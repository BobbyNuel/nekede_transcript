﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Business
{
    public class UtmeScreeningLogic
    {
        public int ScreenApplicant(List<OLevelResultDetail> oLevelResultDetails, ApplicantJambDetail applicantJambDetail,int Sitting)
        {
            int ScreeningScore = 0;
            try
            {
               
                if (Sitting == 1)
                {
                    ScreeningScore += 10;
                }
                else if (Sitting == 2)
                {
                    ScreeningScore += 3;
                }
                 ScreeningScore += GetOlevelGradeScore(oLevelResultDetails);
                 ScreeningScore += GetJambScoreFromRange(Convert.ToInt32(applicantJambDetail.JambScore));
            }
            catch (Exception ex)
            {
                    
                throw ex;
            }
            return ScreeningScore;
        }
        public int GetOlevelGradeScore(List<OLevelResultDetail> oLevelResultDetails)
        {
            int score = 0;
            try
            {
                if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                {
                    foreach (OLevelResultDetail oLevelResultDetail in oLevelResultDetails)
                    {
                        score += GetGradeValue(oLevelResultDetail.Grade.Name.ToUpper());
                    }
                }

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return score;
        }
        public int GetGradeValue(string grade)
        {
            int score = 0;
            try
            {
                if (!string.IsNullOrEmpty(grade))
                {
                    if (grade.ToUpper().Trim() == "A1")
                    {
                        score = (int)ScreeningSetting.GradeAScore;
                    }
                    else if (grade.ToUpper().Trim() == "B2" || grade.ToUpper().Trim() == "B3" )
                    {
                        score = (int)ScreeningSetting.GradeBScore;
                    }
                    else if (grade.ToUpper().Trim() == "C4" || grade.ToUpper().Trim() == "C5" || grade.ToUpper().Trim() == "C6" )
                    {
                        score = (int)ScreeningSetting.GradeCScore;;
                    }
                }
                
                return score;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int GetJambScoreFromRange(int jambScore)
        {
            int score = 0;
            try
            {
                if (jambScore >= 180 && jambScore <= 185)
                {
                   score = 20;
                }
                else if (jambScore >= 186 && jambScore <= 190)
                {
                    score = 21;
                }
                 else if (jambScore >= 191 && jambScore <= 195)
                {
                    score = 22;
                }
                else if (jambScore >= 196 && jambScore <= 200)
                {
                    score = 23;
                }
                 else if (jambScore >= 201 && jambScore <= 205)
                {
                    score = 24;
                }
                 else if (jambScore >= 206 && jambScore <= 210)
                {
                    score = 25;
                }
                 else if (jambScore >= 211 && jambScore <= 215)
                {
                    score = 26;
                }
                 else if (jambScore >= 216 && jambScore <= 220)
                {
                    score = 27;
                }
                 else if (jambScore >= 221 && jambScore <= 225)
                {
                    score = 28;
                }
                 else if (jambScore >= 226 && jambScore <= 230)
                {
                    score = 29;
                }
                 else if (jambScore >= 231 && jambScore <= 235)
                {
                    score = 30;
                }
                 else if (jambScore >= 236 && jambScore <= 240)
                {
                    score = 31;
                }
                 else if (jambScore >= 241 && jambScore <= 245)
                {
                    score = 32;
                }
                 else if (jambScore >= 246 && jambScore <= 250)
                {
                    score = 33;
                }
                 else if (jambScore >= 251 && jambScore <= 255)
                {
                    score = 34;
                }
                 else if (jambScore >= 256 && jambScore <= 260)
                {
                    score = 35;
                }
                 else if (jambScore >= 261 && jambScore <= 265)
                {
                    score = 36;
                }
                 else if (jambScore >= 266 && jambScore <= 270)
                {
                    score = 37;
                }
                 else if (jambScore >= 271 && jambScore <= 275)
                {
                    score = 38;
                }
                 else if (jambScore >= 276 && jambScore <= 280)
                {
                    score = 39;
                }
                 else if (jambScore >= 281 && jambScore <= 285)
                {
                    score = 40;
                }
                 else if (jambScore >= 286 && jambScore <= 290)
                {
                    score = 41;
                }
                 else if (jambScore >= 291 && jambScore <= 295)
                {
                    score = 42;
                }
                 else if (jambScore >= 296 && jambScore <= 300)
                {
                    score = 43;
                }
                 else if (jambScore >= 301 && jambScore <= 305)
                {
                    score = 44;
                }
                 else if (jambScore >= 306 && jambScore <= 310)
                {
                    score = 45;
                }
                 else if (jambScore >= 311 && jambScore <= 315)
                {
                    score = 46;
                }
                 else if (jambScore >= 316 && jambScore <= 320)
                {
                    score = 47;
                }
                 else if (jambScore >= 321 && jambScore <= 325)
                {
                    score = 48;
                }
                 else if (jambScore >= 326 && jambScore <= 330)
                {
                    score = 49;
                }
                 else if (jambScore >= 331 && jambScore <= 335)
                {
                    score = 50;
                }
                 else if (jambScore >= 336 && jambScore <= 340)
                {
                    score = 51;
                }
                 else if (jambScore >= 341 && jambScore <= 345)
                {
                    score = 52;
                }
                 else if (jambScore >= 346 && jambScore <= 350)
                {
                    score = 53;
                }
                 else if (jambScore >= 351 && jambScore <= 355)
                {
                    score = 54;
                }
                 else if (jambScore >= 356 && jambScore <= 360)
                {
                    score = 55;
                }
                 else if (jambScore >= 361 && jambScore <= 365)
                {
                    score = 56;
                }
                 else if (jambScore >= 366 && jambScore <= 370)
                {
                    score = 57;
                }
                 else if (jambScore >= 371 && jambScore <= 375)
                {
                    score = 58;
                }
                else if (jambScore >= 376 && jambScore <= 380)
                {
                    score = 59;
                }
                else if (jambScore >= 381 && jambScore <= 400)
                {
                    score = 60;
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return score;
        }

    }
}
