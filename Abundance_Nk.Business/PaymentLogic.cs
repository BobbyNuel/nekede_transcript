﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class PaymentLogic : BusinessBaseLogic<Payment, PAYMENT>
    {
        private FeeDetailLogic feeDetailLogic;

        public PaymentLogic()
        {
            feeDetailLogic = new FeeDetailLogic();
            translator = new PaymentTranslator();
        }

        public List<PaymentView> GetBy(Person person)
        {
            try
            {
                List<PaymentView> payments = (from p in repository.GetBy<VW_PAYMENT>(p => p.Person_Id == person.Id && p.Bank_Code != null)
                                              select new PaymentView
                                              {
                                                  PersonId = p.Person_Id,
                                                  PaymentId = p.Payment_Id,
                                                  InvoiceNumber = p.Invoice_Number,
                                                  ReceiptNumber = p.Receipt_No,
                                                  ConfirmationOrderNumber = p.Confirmation_No,
                                                  BankCode = p.Bank_Code,
                                                  BankName = p.Bank_Name,
                                                  BranchCode = p.Branch_Code,
                                                  BranchName = p.Branch_Name,
                                                  //InvoiceGenerationDate = p.Date_Paid,
                                                  PaymentDate = p.Transaction_Date,
                                                  FeeTypeId = p.Fee_Type_Id,
                                                  FeeTypeName = p.Fee_Type_Name,
                                                  PaymentTypeId = p.Payment_Type_Id,
                                                  PaymentTypeName = p.Payment_Type_Name,
                                                  Amount = p.Transaction_Amount,
                                              }).ToList();
                return payments;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public Payment GetBy(Person person, FeeType feeType)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Person_Id == person.Id && p.Fee_Type_Id == feeType.Id;
                Payment payment = GetModelBy(selector);

                SetFeeDetails(payment);

                return payment;
            }
            catch(Exception)
            {
                throw;
            }
        }

        private void SetFeeDetails(Payment payment)
        {
            try
            {
                if (payment != null && payment.Id > 0)
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    Payment newPayment = paymentLogic.GetModelBy(p => p.Payment_Id == payment.Id);
                    payment.FeeDetails = feeDetailLogic.GetModelsBy(f => f.Fee_Type_Id == newPayment.FeeType.Id && f.Session_Id == newPayment.Session.Id);
                    
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<FeeDetail> SetFeeDetails(long FeeId)
        {
            List<FeeDetail> feedetail = new List<FeeDetail>();
            try
            {
                if (FeeId > 0)
                {
                    feedetail = feeDetailLogic.GetModelsBy(f => f.Fee_Id == FeeId);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return feedetail;
        }

        public Payment GetBy(long id)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Payment_Id == id;
                Payment payment = GetModelBy(selector);

                SetFeeDetails(payment);

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Payment GetBy(string invoiceNumber)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Invoice_Number == invoiceNumber;
                Payment payment = GetModelBy(selector);

                SetFeeDetails(payment);

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Payment GetBy(FeeType feeType, Person person, Session session)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Fee_Type_Id == feeType.Id && p.Person_Id == person.Id && p.Session_Id == session.Id;
                Payment payment = GetModelBy(selector);

                SetFeeDetails(payment);

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool PaymentAlreadyMade(Payment payment)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Fee_Type_Id == payment.FeeType.Id && p.Payment_Mode_Id == payment.PaymentMode.Id && p.Payment_Type_Id == payment.PaymentType.Id && p.Person_Id == payment.Person.Id && p.Person_Type_Id == payment.PersonType.Id;
                List<Payment> payments = GetModelsBy(selector);
                if (payments != null && payments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SetInvoiceNumber(Payment payment)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Payment_Id == payment.Id;
                PAYMENT entity = base.GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Payment_Serial_Number = payment.SerialNumber;
                entity.Invoice_Number = payment.InvoiceNumber;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override Payment Create(Payment payment)
        {
            try
            {
                Payment newPayment = base.Create(payment);
                if (newPayment == null || newPayment.Id <= 0)
                {
                    throw new Exception("Payment ID not set!");
                }

                newPayment = SetNextPaymentNumber(newPayment);

                SetInvoiceNumber(newPayment);

                newPayment.FeeType = payment.FeeType;
                SetFeeDetails(newPayment);

                return newPayment;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Payment SetNextPaymentNumber(Payment payment)
        {
            try
            {
                payment.SerialNumber = payment.Id;
                payment.InvoiceNumber = "FPN" + DateTime.Now.ToString("yy") + UtilityLogic.PaddNumber(payment.Id, 10);

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool InvalidConfirmationOrderNumber(string invoiceNo, string confirmationOrderNo)
        {
            try
            {
                List<PaymentEtranzact> payments = (from p in repository.GetBy<VW_PAYMENT>(p => p.Invoice_Number == invoiceNo)
                                                   select new PaymentEtranzact
                                                   {
                                                       ConfirmationNo = p.Confirmation_No,
                                                   }).ToList();

                if (payments != null)
                {
                    if (payments.Count > 1)
                    {
                        throw new Exception("Duplicate Invoice Number '" + invoiceNo + "' detected! Please contact your system administrator.");
                    }
                    else if (payments.Count == 1)
                    {
                        if (payments[0].ConfirmationNo == confirmationOrderNo)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Payment InvalidConfirmationOrderNumber(string confirmationOrderNumber, Session session)
        {
            try
            {
                Payment payment = new Payment();
                PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
                PaymentEtranzact etranzactDetails = etranzactLogic.GetModelBy(m => m.Confirmation_No == confirmationOrderNumber);
                if (etranzactDetails == null || etranzactDetails.ReceiptNo == null)
                {
                    PaymentTerminal paymentTerminal = new PaymentTerminal();
                    PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                    paymentTerminal = paymentTerminalLogic.GetModelsBy(p => p.Fee_Type_Id == (int)FeeTypes.AcceptanceFee && p.Session_Id == session.Id).FirstOrDefault();

                    etranzactDetails = etranzactLogic.RetrievePinAlternative(confirmationOrderNumber, paymentTerminal);
                    if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
                    {
                        PaymentLogic paymentLogic = new PaymentLogic();
                        payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                        if (payment != null && payment.Id > 0)
                        {
                            FeeDetail feeDetail = new FeeDetail();
                            FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                            feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == session.Id);
                            if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                            {
                               throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");
                               
                            }
                        }
                        else
                        {
                            throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");
                        }
                    }
                    else
                    {
                        throw new Exception("Confirmation Order Number entered seems not to be valid! Please cross check and try again.");
                    }
                }
                else
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                    if (payment != null && payment.Id > 0)
                    {
                        //FeeDetail feeDetail = new FeeDetail();
                        FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                        //feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id);

                        List<FeeDetail> feeDetails = feeDetailLogic.GetModelsBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);
                        decimal amount = feeDetails.Sum(a => a.Fee.Amount);
                        if (!etranzactLogic.ValidatePin(etranzactDetails, payment, amount))
                        {
                            throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");
                            //payment = null;
                            //return payment;
                        }
                    }
                    else
                    {
                        throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");
                    }
                }

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(Payment payment)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = p => p.Payment_Id == payment.Id;
                PAYMENT entity = GetEntityBy(selector);

                if (entity == null || entity.Person_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }


                entity.Fee_Type_Id = payment.FeeType.Id;
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public decimal GetPaymentAmount(Payment payment)
        {
            decimal Amount = 0;
            try
            {
                FeeDetail feeDetail = new FeeDetail();
                FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                feeDetail = feeDetailLogic.GetModelBy(f => f.Fee_Type_Id == payment.FeeType.Id && f.Session_Id == payment.Session.Id);
                Amount = feeDetail.Fee.Amount;
            }
            catch (Exception ex)
            {

                throw;
            }
            return Amount;
        }
        public void DeleteBy(long PaymentID)
        {
            try
            {
                Expression<Func<PAYMENT, bool>> selector = a => a.Payment_Id == PaymentID;
                Delete(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<AcceptanceReport> GetAcceptanceReports(Department department, Programme programme,Session session)
        {
            try
            {
                List<AcceptanceReport> payments = (from p in repository.GetBy<VW_ACCEPTANCE_REPORT>(p => p.Department_Id == department.Id && p.Programme_Id == programme.Id && p.Session_Id == session.Id)
                                                 select new AcceptanceReport
                                                 {
                                                     Person_Id = p.Person_Id,
                                                     Application_Exam_Number = p.Application_Exam_Number,
                                                     Invoice_Number = p.Invoice_Number,
                                                     Application_Form_Number = p.Application_Form_Number,
                                                     First_Choice_Department_Name = p.Department_Name,
                                                     Name = p.SURNAME + " " + p.FIRSTNAME + " " + p.OTHER_NAMES,
                                                     RRR = p.Invoice_Number,
                                                     Programme_Name = p.Programme_Name,
                                                 }).OrderBy(b => b.Name).ToList();
                return payments;
            }
            catch (Exception)
            {
                    
                throw;
            }
        }

        public List<Payment> GetMissingRemitaPayments()
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();

                List<Payment> payments = new List<Payment>();

                //payments = (from p in repository.GetAll<VW_MISSING_REMITA_PAYMENTS>()
                //    select new Payment
                //    {
                //        Id = p.Payment_Id,
                //        InvoiceNumber = p.Invoice_Number,
                //        DatePaid = p.Date_Paid
                //    }).ToList();

                for (int i = 0; i < payments.Count; i++)
                {
                    Payment currentPayment = payments[i];
                    Payment payment = paymentLogic.GetModelBy(p => p.Payment_Id == currentPayment.Id);

                    payments[i] = payment;
                }

                return payments;
            }
            catch (Exception)
            {
                throw;
            }
        } 
    }


}

