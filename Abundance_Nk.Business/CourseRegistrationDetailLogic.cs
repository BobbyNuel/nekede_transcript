﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class CourseRegistrationDetailLogic : BusinessBaseLogic<CourseRegistrationDetail, STUDENT_COURSE_REGISTRATION_DETAIL>
    {
        private SessionSemesterLogic sessionSemesterLogic;

        public CourseRegistrationDetailLogic()
        {
            translator = new CourseRegistrationDetailTranslator();
            sessionSemesterLogic = new SessionSemesterLogic();
        }

        public bool UpdateCourseRegistrationScore(List<StudentResultDetail> results)
        {
            try
            {
                List<STUDENT_COURSE_REGISTRATION_DETAIL> registeredCourseEntities = new List<STUDENT_COURSE_REGISTRATION_DETAIL>();
                SessionSemester sessionSemester = sessionSemesterLogic.GetBy(results[0].Header.SessionSemester.Id);
                foreach (StudentResultDetail result in results)
                {
                    STUDENT_COURSE_REGISTRATION_DETAIL registeredCourseEntity = GetBy(result.Student, result.Header.Level, result.Header.Programme, result.Header.Department, result.Course, sessionSemester.Session, sessionSemester.Semester);
                    if (registeredCourseEntity != null && registeredCourseEntity.Student_Course_Registration_Detail_Id > 0)
                    {
                        if (result.Header.Type.Id == 1)
                        {
                            registeredCourseEntity.Test_Score = result.Score;
                            registeredCourseEntity.Special_Case = result.SpecialCaseMessage;
                        }
                        else if (result.Header.Type.Id == 2)
                        {
                            registeredCourseEntity.Exam_Score = result.Score;
                            registeredCourseEntity.Special_Case = result.SpecialCaseMessage;
                        }

                        registeredCourseEntities.Add(registeredCourseEntity);
                    }
                }

               // return Save() > 0 ? true : false;
                Save();
                return  true ;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateCourseRegistrationScore(List<CourseRegistrationDetail> results)
        {
            try
            {
                List<STUDENT_COURSE_REGISTRATION_DETAIL> registeredCourseEntities = new List<STUDENT_COURSE_REGISTRATION_DETAIL>();
                foreach (CourseRegistrationDetail result in results)
                {
                    STUDENT_COURSE_REGISTRATION_DETAIL registeredCourseEntity = GetEntityBy(a => a.Student_Course_Registration_Detail_Id == result.Id);
                    if (registeredCourseEntity != null && registeredCourseEntity.Student_Course_Registration_Detail_Id > 0)
                    {
                        string strTestScore = result.TestScore.ToString();
                        string strExamScore = result.ExamScore.ToString();
                        registeredCourseEntity.Test_Score = result.TestScore;
                        registeredCourseEntity.Exam_Score = result.ExamScore;
                        if (result.TestScore == 101 && result.ExamScore == 101)
                        {
                            SessionLogic sessionLogic = new SessionLogic();
                            SESSION session = sessionLogic.GetEntityBy(p=>p.Activated == true);
                            List<STUDENT_COURSE_REGISTRATION_DETAIL> courseRegDetailCheck = GetEntitiesBy(p=>p.Course_Id == registeredCourseEntity.Course_Id  && p.Semester_Id == registeredCourseEntity.Semester_Id && p.STUDENT_COURSE_REGISTRATION.Session_Id == session.Session_Id && p.STUDENT_COURSE_REGISTRATION.Person_Id == registeredCourseEntity.STUDENT_COURSE_REGISTRATION.Person_Id);
                            if (courseRegDetailCheck != null)
	                        {
                                bool isCarryOverDeleted = Delete(p => p.Course_Id == registeredCourseEntity.Course_Id && p.Semester_Id == registeredCourseEntity.Semester_Id && p.STUDENT_COURSE_REGISTRATION.Session_Id == session.Session_Id);
                                bool isCurrenCourseDeleted = Delete(p => p.Student_Course_Registration_Detail_Id == registeredCourseEntity.Student_Course_Registration_Detail_Id);
	                        }
                           
                        }
                        else
                        {
                            if ((result.TestScore + result.ExamScore) > 0)
                            {
                                registeredCourseEntity.Special_Case = null;
                            }

                            else
                            {
                                registeredCourseEntity.Special_Case = result.SpecialCase;
                            }
                            registeredCourseEntities.Add(registeredCourseEntity);
                        }
                      

                        //StudentResultDetailLogic resultDetailLogic = new StudentResultDetailLogic();
                        //SessionSemester sessionSemeter = sessionSemesterLogic.GetModelBy(a => a.Session_Id == registeredCourseEntity.STUDENT_COURSE_REGISTRATION.Session_Id && a.Semester_Id == registeredCourseEntity.Semester_Id);
                        //Person person = new Person() { Id = registeredCourseEntity.STUDENT_COURSE_REGISTRATION.Person_Id };
                        //Course course = new Course() { Id = registeredCourseEntity.Course_Id };

                        //resultDetailLogic.Modify(person, course, sessionSemeter, registeredCourseEntity.Test_Score, registeredCourseEntity.Exam_Score);

                    }
                }

                Save();
                return true;

            }
            catch (Exception)
            {
                throw;
            }
        }


        private STUDENT_COURSE_REGISTRATION_DETAIL GetBy(Student student, Level level, Programme programme, Department department, Course course, Session session, Semester semester)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = cr => cr.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && cr.STUDENT_COURSE_REGISTRATION.Level_Id == level.Id && cr.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && cr.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && cr.Course_Id == course.Id && cr.STUDENT_COURSE_REGISTRATION.Session_Id == session.Id && cr.Semester_Id == semester.Id;
                return base.GetEntityBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CourseRegistrationDetail> GetBy(CourseRegistration courseRegistration)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = crd => crd.Student_Course_Registration_Id == courseRegistration.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CourseRegistrationDetail> GetBy(Student student, Course course, Level level, Session session, Semester semester)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = crd => crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && crd.Course_Id == course.Id && crd.STUDENT_COURSE_REGISTRATION.Level_Id == level.Id && crd.STUDENT_COURSE_REGISTRATION.Session_Id == session.Id && crd.Semester_Id == semester.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CourseRegistrationDetail> GetResultsBy(Student student, SessionSemester sessionSemester)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = crd => crd.STUDENT_COURSE_REGISTRATION.STUDENT.Matric_Number == student.MatricNumber && crd.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id && crd.Semester_Id == sessionSemester.Semester.Id;
                return base.GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Course> GetCarryOverCoursesBy(CourseRegistration courseRegistration, Semester semester)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = cr => cr.Student_Course_Registration_Id == courseRegistration.Id && cr.Course_Mode_Id == 2 && cr.Semester_Id == semester.Id;
                List<CourseRegistrationDetail> courseRegistrationDetails = GetModelsBy(selector);

                List<Course> courses = null;
                if (courseRegistrationDetails != null && courseRegistrationDetails.Count > 0)
                {
                    courses = new List<Course>();
                    foreach(CourseRegistrationDetail courseRegistrationDetail in courseRegistrationDetails)
                    {
                        courses.Add(courseRegistrationDetail.Course);

                    }
                }

                return courses;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CourseRegistrationDetail> GetCarryOverBy(Student student, Session session)
        {
            try
            {
              
                List<CourseRegistrationDetail> carryOverCourses = (from cr in repository.GetBy<VW_STUDENT_REGISTERED_COURSE_CARRYOVER>(cr => cr.Person_Id == student.Id && cr.Session_Id != session.Id)
                                                                   select new CourseRegistrationDetail
                                                                   {
                                                                       Id = cr.Student_Course_Registration_Detail_Id,
                                                                       
                                                                   }).ToList();
                
                List<CourseRegistrationDetail> courseRegistrationDetails = null;
                if (carryOverCourses != null && carryOverCourses.Count > 0)
                {
                    int totalUnit = 0;
                    courseRegistrationDetails = new List<CourseRegistrationDetail>();
                    foreach (CourseRegistrationDetail carryOverCourse in carryOverCourses)
                    {
                        Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> selector = cr => cr.Student_Course_Registration_Detail_Id == carryOverCourse.Id;
                        CourseRegistrationDetail courseRegistrationDetail = GetModelBy(selector);

                        if (courseRegistrationDetail != null)
                        {
                            totalUnit += courseRegistrationDetail.Course.Unit;
                            courseRegistrationDetail.Mode = new CourseMode() { Id = 2 };
                            courseRegistrationDetails.Add(courseRegistrationDetail);
                        }
                    }

                    
                }

                return courseRegistrationDetails;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Modify(List<CourseRegistrationDetail> oldCourseRegistrationDetails, List<CourseRegistrationDetail> newCourseRegistrationDetails)
        {
            try
            {
                for (int i = 0; i < oldCourseRegistrationDetails.Count; i++)
                {
                    oldCourseRegistrationDetails[i].Course.Id = newCourseRegistrationDetails[i].Course.Id;
                    if (!Modify(oldCourseRegistrationDetails[i]))
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool Modify(CourseRegistrationDetail courseRegistrationDetail)
        {
            try
            {
                Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> predicate = m => m.Student_Course_Registration_Detail_Id == courseRegistrationDetail.Id;
                STUDENT_COURSE_REGISTRATION_DETAIL entity = GetEntityBy(predicate);
                entity.Course_Id = courseRegistrationDetail.Course.Id;
                entity.Grade = courseRegistrationDetail.Grade;
                entity.Course_Unit = courseRegistrationDetail.CourseUnit;

                if (courseRegistrationDetail.ExamScore >= 0)
                {
                    entity.Exam_Score = courseRegistrationDetail.ExamScore;
                }

                if (courseRegistrationDetail.TestScore >= 0)
                {
                    entity.Test_Score = courseRegistrationDetail.TestScore;
                }

                if (courseRegistrationDetail.Mode != null)
                {
                    entity.Course_Mode_Id = courseRegistrationDetail.Mode.Id;
                }

                int rowsAffected = base.Save();
                if (rowsAffected > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException(ArgumentNullException);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Remove(List<CourseRegistrationDetail> courseRegistrationDetails)
        {
            try
            {
                int rowsDeleted = 0;
                if (courseRegistrationDetails != null && courseRegistrationDetails.Count > 0)
                {
                    foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationDetails)
                    {
                        if (courseRegistrationDetail.ExamScore == null && courseRegistrationDetail.TestScore == null)
                        {
                            Expression<Func<STUDENT_COURSE_REGISTRATION_DETAIL, bool>> predicate = m => m.Student_Course_Registration_Detail_Id == courseRegistrationDetail.Id;
                            rowsDeleted += base.Delete(predicate) == true ? 1 : 0;
                        }
                        
                    }
                }

                if (rowsDeleted > 0 && rowsDeleted == courseRegistrationDetails.Count)
                {
                    return true;
                }
                else
                {
                    throw new Exception("Number of deletable rows does not match the actual number rows deleted! Please try again.");
                }
            }
            catch (NullReferenceException)
            {
                throw new NullReferenceException(ArgumentNullException);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<AttendanceFormat> GetAttendanceList(Session session, Semester semester, Programme programme, Department department, Level level, Course course)
        {
            try
            {
                List<AttendanceFormat> attendanceFormatList = new List<AttendanceFormat>();
                List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();
                List<CourseRegistrationDetail> courseRegistrationDetailList = new List<CourseRegistrationDetail>();

                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                courseRegistrationList = courseRegistrationLogic.GetModelsBy(p => p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Programme_Id == programme.Id);
                if (course != null && semester != null && courseRegistrationList.Count > 0)
                {
                    foreach (CourseRegistration courseRegistration in courseRegistrationList)
                    {
                        courseRegistrationDetailList = courseRegistrationDetailLogic.GetModelsBy(p => p.Course_Id == course.Id && p.Semester_Id == semester.Id && p.Student_Course_Registration_Id == courseRegistration.Id);
                        if (courseRegistrationDetailList.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetailItem in courseRegistrationDetailList)
                            {
                                AttendanceFormat attendanceFormat = new AttendanceFormat();
                                attendanceFormat.COURSE = courseRegistrationDetailItem.Course.Name;
                                attendanceFormat.MATRICNO = courseRegistrationDetailItem.CourseRegistration.Student.MatricNumber;
                                attendanceFormat.NAME = courseRegistrationDetailItem.CourseRegistration.Student.FullName;
                                attendanceFormat.DEPARTMENT = courseRegistrationDetailItem.CourseRegistration.Department.Name;
                                attendanceFormat.LEVEL = courseRegistrationDetailItem.CourseRegistration.Level.Name;
                                attendanceFormat.PROGRAMME = courseRegistrationDetailItem.CourseRegistration.Programme.Name;
                                attendanceFormat.SEMESTER = courseRegistrationDetailItem.Semester.Name;
                                attendanceFormat.SESSION = courseRegistrationDetailItem.CourseRegistration.Session.Name;

                                attendanceFormatList.Add(attendanceFormat);
                                attendanceFormatList.OrderBy(p => p.MATRICNO);
                            }
                        }
                    }
                }

                return attendanceFormatList.OrderBy(p => p.NAME).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CourseRegistrationReportModel> GetCourseRegistrationBy(Session session, Programme programme, Department department, Level level)
        {
            try
            {
                CourseLogic courseLogic = new CourseLogic();
                List<Course> courses = courseLogic.GetModelsBy(c => c.Department_Id == department.Id);
                List<CourseRegistrationReportModel> registeredCourses = new List<CourseRegistrationReportModel>();

                foreach (Course course in courses)
                {
                    if (level != null && level.Id > 0)
                    {
                        List<CourseRegistrationDetail> courseRegistrationDetails = GetModelsBy(crd => crd.Course_Id == course.Id && crd.STUDENT_COURSE_REGISTRATION.Session_Id == session.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && crd.STUDENT_COURSE_REGISTRATION.Level_Id == level.Id);
                        if (courseRegistrationDetails != null && courseRegistrationDetails.Count > 0)
                        {
                            CourseRegistrationReportModel courseRegistrationReportModel = new CourseRegistrationReportModel();
                            courseRegistrationReportModel.CourseCode = course.Code;
                            courseRegistrationReportModel.CourseName = course.Name;
                            courseRegistrationReportModel.CourseUnit = course.Unit;
                            courseRegistrationReportModel.Department = department.Name;
                            courseRegistrationReportModel.Level = level.Name;
                            courseRegistrationReportModel.Programme = programme.Name;
                            courseRegistrationReportModel.Semester = course.Semester.Name;
                            courseRegistrationReportModel.Session = session.Name;

                            registeredCourses.Add(courseRegistrationReportModel);
                        }
                    }
                    else if (level == null || level.Id <= 0)
                    {
                        List<CourseRegistrationDetail> courseRegistrationDetails = GetModelsBy(crd => crd.Course_Id == course.Id && crd.STUDENT_COURSE_REGISTRATION.Session_Id == session.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id);
                        if (courseRegistrationDetails != null && courseRegistrationDetails.Count > 0)
                        {
                            CourseRegistrationReportModel courseRegistrationReportModel = new CourseRegistrationReportModel();
                            courseRegistrationReportModel.CourseCode = course.Code;
                            courseRegistrationReportModel.CourseName = course.Name;
                            courseRegistrationReportModel.CourseUnit = course.Unit;
                            courseRegistrationReportModel.Department = department.Name;
                            //courseRegistrationReportModel.Level = level.Name;
                            courseRegistrationReportModel.Programme = programme.Name;
                            courseRegistrationReportModel.Semester = course.Semester.Name;
                            courseRegistrationReportModel.Session = session.Name;

                            registeredCourses.Add(courseRegistrationReportModel);
                        }
                    }

                }

                return registeredCourses.OrderBy(rc => rc.CourseName).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<AttendanceFormat> GetCourseAttendanceSheet(Session session, Semester semester, Programme programme, Department department, Level level, Course course)
        {
            try
            {
                List<AttendanceFormat> attendanceFormatList = new List<AttendanceFormat>();

                if (session != null && semester != null && programme != null && department != null && level != null && course != null)
                {
                    attendanceFormatList = (from crd in repository.GetBy<VW_STUDENT_RESULT_ALT>(c => c.Course_Id == course.Id && c.Semester_Id == semester.Id && c.Session_Id == session.Id && c.Level_Id == level.Id && c.Department_Id == department.Id && c.Programme_Id == programme.Id)
                                            select new AttendanceFormat
                                            {
                                                COURSE = crd.Course_Name,
                                                MATRICNO = crd.Matric_Number,
                                                NAME = crd.Name,
                                                DEPARTMENT = crd.Department_Name,
                                                LEVEL = crd.Level_Name,
                                                PROGRAMME = crd.Programme_Name,
                                                SEMESTER = crd.Semester_Name,
                                                SESSION = crd.Session_Name,
                                                COURSE_CODE = crd.Course_Code,
                                                COURSE_UNIT = crd.Course_Unit,
                                                SCHOOL = crd.Faculty_Name,
                                            }).ToList();
                }

                return attendanceFormatList.OrderBy(a => a.NAME).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveDuplicateCourseRegistration(List<CourseRegistration> courseRegistrationList)
        {
            try
            {
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                bool isDeleted = false;
                if (courseRegistrationList.Count > 1)
                {
                    long firstCourseRegisteredId = courseRegistrationList.FirstOrDefault().Id;
                    long secondCourseRegisteredId = courseRegistrationList.LastOrDefault().Id;

                    List<STUDENT_COURSE_REGISTRATION_DETAIL> courseRegistrationDetailList = GetEntitiesBy(p => p.Student_Course_Registration_Id == secondCourseRegisteredId);
                    foreach (STUDENT_COURSE_REGISTRATION_DETAIL courseRegDetailItem in courseRegistrationDetailList)
                    {
                        courseRegDetailItem.Student_Course_Registration_Id = firstCourseRegisteredId;
                    }
                    int count = Save();

                    isDeleted = courseRegistrationLogic.Delete(p => p.Student_Course_Registration_Id == secondCourseRegisteredId);
                 
                    //Save();
                }
                return isDeleted;
            }
            catch (Exception)
            {

                throw;
            }

        }
    }


}
