﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class StudentAcademicInformationLogic : BusinessBaseLogic<StudentAcademicInformation, STUDENT_ACADEMIC_INFORMATION>
    {
        public StudentAcademicInformationLogic()
        {
            translator = new StudentAcademicInformationTranslator();
        }

        public bool Modify(StudentAcademicInformation model)
        {
            try
            {
                STUDENT_ACADEMIC_INFORMATION entity = GetEntityBy(s => s.Person_Id == model.Student.Id);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Year_Of_Admission = !string.IsNullOrEmpty(model.YearOfAdmission) ? model.YearOfAdmission : entity.Year_Of_Admission;
                entity.Year_Of_Graduation = model.YearOfGraduation > 0 ? model.YearOfGraduation : entity.Year_Of_Graduation;
                entity.Date_Of_Entry = model.DateOfEntry != null ? model.DateOfEntry : entity.Date_Of_Entry;
                entity.Last_School_Attended = model.LastSchoolAttended ?? entity.Last_School_Attended;
                entity.Student_Award = model.StudentAward ?? entity.Student_Award;
                entity.Award_Date = model.AwardDate ?? entity.Award_Date;
                entity.CGPA = model.CGPA > 0 ? model.CGPA : entity.CGPA;
                entity.IsModified = model.IsModified ?? null;

                if (model.ModeOfStudy != null && model.ModeOfStudy.Id > 0)
                {
                    entity.Mode_Of_Study_Id = model.ModeOfStudy.Id;
                }
                if (model.ModeOfEntry != null && model.ModeOfEntry.Id > 0)
                {
                    entity.Mode_Of_Entry_Id = model.ModeOfEntry.Id;
                }
                if (model.Level != null && model.Level.Id > 0)
                {
                    entity.Level_Id = model.Level.Id;
                }
                if (model.MonthOfGraduation != null)
                {
                    entity.Month_Of_Graduation = model.MonthOfGraduation;
                }

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

}
