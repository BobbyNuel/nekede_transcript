﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class PersonLogic : BusinessBaseLogic<Person, PERSON>
    {
        private PersonAuditLogic personAuditLogic;

        public PersonLogic()
        {
            translator = new PersonTranslator();
        }

        public Person GetBy(long Id)
        {
            try
            {
               Expression<Func<PERSON, bool>> selector = p => p.Person_Id == Id;
               return GetModelBy(selector);

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public bool Modify(Person person)
        {
            try
            {
                Expression<Func<PERSON, bool>> selector = p => p.Person_Id == person.Id;
                PERSON personEntity = GetEntityBy(selector);

                if (personEntity == null || personEntity.Person_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                if (person.FirstName != null)
                {
                    personEntity.First_Name = person.FirstName;
                }
                if (person.LastName != null)
                {
                    personEntity.Last_Name = person.LastName;
                }
                if (person.OtherName != null)
                {
                    personEntity.Other_Name = person.OtherName;
                }
                if (person.ContactAddress != null)
                {
                    personEntity.Contact_Address = person.ContactAddress;
                }
                if (person.Email != null)
                {
                    personEntity.Email = person.Email;
                }
                if (person.MobilePhone != null)
                {
                    personEntity.Mobile_Phone = person.MobilePhone;
                }
                if (person.SignatureFileUrl != null)
                {
                    personEntity.Signature_File_Url = person.SignatureFileUrl;
                }
                if (person.ImageFileUrl != null)
                {
                    personEntity.Image_File_Url = person.ImageFileUrl;
                }
                if (person.DateOfBirth != null)
                {
                    personEntity.Date_Of_Birth = person.DateOfBirth;
                }
                if (person.HomeTown != null)
                {
                    personEntity.Home_Town = person.HomeTown;
                }
                if (person.HomeAddress != null)
                {
                    personEntity.Home_Address = person.HomeAddress;
                }
                if (person.DateEntered != null)
                {
                    personEntity.Date_Entered = personEntity.Date_Entered;
                }
                if (person.Initial != null)
                {
                    personEntity.Initial = person.Initial;
                }
                if (person.Title != null)
                {
                    personEntity.Title = person.Title;
                }

                if (person.Role != null && person.Role.Id > 0)
                {
                    personEntity.Role_Id = person.Role.Id;
                }
                if (person.Nationality != null && person.Nationality.Id > 0)
                {
                    personEntity.Nationality_Id = person.Nationality.Id;
                }
                if (person.State != null && !string.IsNullOrEmpty(person.State.Id))
                {
                    personEntity.State_Id = person.State.Id;
                }
                if (person.PersonType != null && person.PersonType.Id > 0)
                {
                    personEntity.Person_Type_Id = person.PersonType.Id;
                }
                if (person.Religion != null)
                {
                    personEntity.Religion_Id = person.Religion.Id;
                }
                if (person.LocalGovernment != null)
                {
                    personEntity.Local_Government_Id = person.LocalGovernment.Id;
                }
                if (person.Sex != null)
                {
                    personEntity.Sex_Id = person.Sex.Id;
                }
                if (person.PlaceOfBirth != null)
                {
                    personEntity.Place_Of_Birth = person.PlaceOfBirth;
                }

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(Person person, PersonAudit audit)
        {
            try
            {
                Expression<Func<PERSON, bool>> selector = p => p.Person_Id == person.Id;
                PERSON personEntity = GetEntityBy(selector);

                bool audited = CreateAudit(person, audit, personEntity);
                if (audited)
                {
                    if (personEntity == null || personEntity.Person_Id <= 0)
                    {
                        throw new Exception(NoItemFound);
                    }

                    if (person.FirstName != null)
                    {
                        personEntity.First_Name = person.FirstName;
                    }
                    if (person.LastName != null)
                    {
                        personEntity.Last_Name = person.LastName;
                    }
                    if (person.OtherName != null)
                    {
                        personEntity.Other_Name = person.OtherName;
                    }
                    if (person.ContactAddress != null)
                    {
                        personEntity.Contact_Address = person.ContactAddress;
                    }
                    if (person.Email != null)
                    {
                        personEntity.Email = person.Email;
                    }
                    if (person.MobilePhone != null)
                    {
                        personEntity.Mobile_Phone = person.MobilePhone;
                    }
                    if (person.SignatureFileUrl != null)
                    {
                        personEntity.Signature_File_Url = person.SignatureFileUrl;
                    }
                    if (person.ImageFileUrl != null)
                    {
                        personEntity.Image_File_Url = person.ImageFileUrl;
                    }
                    if (person.DateOfBirth != null)
                    {
                        personEntity.Date_Of_Birth = person.DateOfBirth;
                    }
                    if (person.HomeTown != null)
                    {
                        personEntity.Home_Town = person.HomeTown;
                    }
                    if (person.HomeAddress != null)
                    {
                        personEntity.Home_Address = person.HomeAddress;
                    }
                    if (person.DateEntered != null)
                    {
                        personEntity.Date_Entered = personEntity.Date_Entered;
                    }
                    if (person.Initial != null)
                    {
                        personEntity.Initial = person.Initial;
                    }
                    if (person.Title != null)
                    {
                        personEntity.Title = person.Title;
                    }

                    if (person.Role != null && person.Role.Id > 0)
                    {
                        personEntity.Role_Id = person.Role.Id;
                    }
                    if (person.Nationality != null && person.Nationality.Id > 0)
                    {
                        personEntity.Nationality_Id = person.Nationality.Id;
                    }
                    if (person.State != null && !string.IsNullOrEmpty(person.State.Id))
                    {
                        personEntity.State_Id = person.State.Id;
                    }
                    if (person.PersonType != null && person.PersonType.Id > 0)
                    {
                        personEntity.Person_Type_Id = person.PersonType.Id;
                    }
                    if (person.Religion != null && person.Religion.Id > 0)
                    {
                        personEntity.Religion_Id = person.Religion.Id;
                    }
                    if (person.LocalGovernment != null && person.LocalGovernment.Id > 0)
                    {
                        personEntity.Local_Government_Id = person.LocalGovernment.Id;
                    }
                    if (person.Sex != null && person.Sex.Id > 0)
                    {
                        personEntity.Sex_Id = person.Sex.Id;
                    }
                    if (person.PlaceOfBirth != null)
                    {
                        personEntity.Place_Of_Birth = person.PlaceOfBirth;
                    }

                    int modifiedRecordCount = Save();
                    if (modifiedRecordCount <= 0)
                    {
                        return false;
                    }

                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CreateAudit(Person person, PersonAudit audit, PERSON personEntity)
        {
            try
            {
                audit.Person = new Person() { Id = person.Id };
                audit.FirstName = person.FirstName;
                audit.LastName = person.LastName;
                audit.PersonType = person.PersonType;
                audit.OtherName = person.OtherName;
                audit.Sex = person.Sex;
                audit.ContactAddress = person.ContactAddress;
                audit.Email = person.Email;
                audit.MobilePhone = person.MobilePhone;
                audit.SignatureFileUrl = person.SignatureFileUrl;
                audit.ImageFileUrl = person.ImageFileUrl;
                audit.DateOfBirth = person.DateOfBirth;
                audit.State = person.State;
                audit.LocalGovernment = person.LocalGovernment;
                audit.HomeTown = person.HomeTown;
                audit.HomeAddress = person.HomeAddress;
                audit.Nationality = person.Nationality;
                audit.DateEntered = person.DateEntered;
                audit.Role = person.Role;
                audit.Initial = person.Initial;
                audit.Title = person.Title;
                audit.Religion = person.Religion;


                Person oldPerson = translator.Translate(personEntity);
                audit.OldFirstName = oldPerson.FirstName;
                audit.OldLastName = oldPerson.LastName;
                audit.OldPersonType = oldPerson.PersonType;
                audit.OldOtherName = oldPerson.OtherName;
                audit.OldSex = oldPerson.Sex;
                audit.OldContactAddress = oldPerson.ContactAddress;
                audit.OldEmail = oldPerson.Email;
                audit.OldMobilePhone = oldPerson.MobilePhone;
                audit.OldSignatureFileUrl = oldPerson.SignatureFileUrl;
                audit.OldImageFileUrl = oldPerson.ImageFileUrl;
                audit.OldDateOfBirth = oldPerson.DateOfBirth;
                audit.OldState = oldPerson.State;
                audit.OldLocalGovernment = oldPerson.LocalGovernment;
                audit.OldHomeTown = oldPerson.HomeTown;
                audit.OldHomeAddress = oldPerson.HomeAddress;
                audit.OldNationality = oldPerson.Nationality;
                audit.OldDateEntered = oldPerson.DateEntered;
                audit.OldRole = oldPerson.Role;
                audit.OldInitial = oldPerson.Initial;
                audit.OldTitle = oldPerson.Title;
                audit.OldReligion = oldPerson.Religion;

                personAuditLogic = new PersonAuditLogic();
                PersonAudit personAudit = personAuditLogic.Create(audit);
                if (personAudit == null || personAudit.Id <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<PersonReport> GetPersonInfromation(STUDENT student, StudentClassListReport StudentCGPA)
        {
            List<PersonReport> personReportList = new List<PersonReport>();
            try
            {
                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Person_Id == student.Person_Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,
                                            FacultyName = sr.Faculty_Name,
                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,
                                            Email = sr.Email,
                                            SpecialCase = sr.Special_Case,
                                            Address = sr.Contact_Address,
                                            MobilePhone = sr.Mobile_Phone,
                                            PassportUrl = sr.Image_File_Url,
                                            GPCU = sr.Grade_Point * sr.Course_Unit,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            Student_Type_Id = sr.Student_Type_Id,
                                            SessionName = sr.Session_Name,
                                            Semestername = sr.Semester_Name,
                                            LevelName = sr.Level_Name,
                                            WGP = sr.WGP,
                                            Activated = sr.Activated,
                                            Reason = sr.Reason,
                                            RejectCategory = sr.Reject_Category,
                                            firstname_middle = sr.Othernames,
                                            ProgrammeName = sr.Programme_Name,
                                            Surname = sr.Last_Name,
                                            Firstname = sr.First_Name,
                                            Othername = sr.Other_Name,
                                            TotalScore = sr.Total_Score,
                                            SessionSemesterId = sr.Session_Semester_Id,
                                            SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            DepartmentName = sr.Department_Name,
                                        }).ToList();
                if (results.Count > 0)
                {
                    PersonReport personInformation = new PersonReport();
                    StudentLogic studentLogic = new StudentLogic();
                    PERSON person = new PERSON();
                    StudentSponsorLogic sponsorLogic = new StudentSponsorLogic();
                    STUDENT_SPONSOR sponsor = new STUDENT_SPONSOR();
                    PERSON_GUARDIAN personGuardian = new PERSON_GUARDIAN();
                    PersonGuardianLogic personGuardianLogic = new PersonGuardianLogic();

                    sponsor = sponsorLogic.GetEntityBy(p => p.Person_Id == student.Person_Id);
                    student = studentLogic.GetEntityBy(s => s.Person_Id == student.Person_Id);
                    person = GetEntityBy(p => p.Person_Id == student.Person_Id);

                    personInformation.Address = person.Contact_Address;
                    personInformation.LastName = person.Last_Name;
                    personInformation.FirstName = person.First_Name;
                    personInformation.MiddleName = person.Other_Name;
                    personInformation.PlaceOfBirth = person.Place_Of_Birth;
                    personInformation.RegNo = student.Matric_Number;

                    personInformation.GraduatingTitle = GetGraduatingTitle(StudentCGPA);

                    if (results.FirstOrDefault().ProgrammeName.Contains("HND"))
                    {
                        personInformation.ProgrammeCode = "HND";
                    }
                    else
                    {
                        personInformation.ProgrammeCode = "ND";
                    }
                    if (person.NATIONALITY != null)
                    {
                        personInformation.Nationality = person.NATIONALITY.Nationality_Name;
                    }
                   
                    personInformation.Department = results.FirstOrDefault().DepartmentName;
                    personInformation.Faculty = results.FirstOrDefault().FacultyName;
                    
                    StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                    STUDENT_ACADEMIC_INFORMATION academicInformation = academicInformationLogic.GetEntitiesBy(a => a.Person_Id == student.Person_Id).LastOrDefault();

                    if (academicInformation != null)
                    {
                        personInformation.BasisOfAdmission = academicInformation.MODE_OF_ENTRY.Mode_Of_Entry_Name;
                        personInformation.YearOfEntry = academicInformation.Year_Of_Admission.ToString();
                        personInformation.LastSchoolAttended = academicInformation.Last_School_Attended;
                        personInformation.StudentAward = academicInformation.Student_Award;
                        personInformation.AwardDate = academicInformation.Award_Date != null ? academicInformation.Award_Date.Value.ToShortDateString() : null;
                        //personInformation.YearOfGraduation = academicInformation.Year_Of_Graduation.ToString();
                        personInformation.YearOfGraduation = academicInformation.Month_Of_Graduation + " " + academicInformation.Year_Of_Graduation.ToString();
                        personInformation.Grade = academicInformation.CGPA.ToString();
                    }

                    personInformation.DateOfBirth = person.Date_Of_Birth != null ? person.Date_Of_Birth.Value.ToLongDateString() : "";
                    
                    if (student.MARITAL_STATUS != null)
                    {
                        personInformation.MaritalStatus = student.MARITAL_STATUS.Marital_Status_Name; ;
                    }
                    if (person.RELIGION != null)
                    {
                        personInformation.Religion = person.RELIGION.Religion_Name;
                    }

                    personGuardian = personGuardianLogic.GetEntitiesBy(p => p.Person_Id == student.Person_Id).LastOrDefault();
                    if (personGuardian != null)
                    {
                        personInformation.GuardianName = personGuardian.Guardian_Name;
                        personInformation.GuardianAddress = personGuardian.Address;
                        personInformation.GuardianOccupation = personGuardian.Occupation;
                    }

                    personInformation.SponsorAddress = sponsor != null ? sponsor.Sponsor_Contact_Address : null;
                    personInformation.SponsorName = sponsor != null ? sponsor.Sponsor_Name : null; ;
                    //personInformation.SponsorOccupation = sponsor.Occupation ?? null;;
                    if (person.STATE != null)
                    {
                        personInformation.State = person.STATE.State_Name;
                    }

                    if (person.Last_Name != null)
                    {
                        personInformation.LocalGovernmentArea = person.LOCAL_GOVERNMENT.Local_Government_Name;
                    }

                    personInformation.Sex = person.SEX.Sex_Name;

                    personReportList.Add(personInformation);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return personReportList;
        }

        private string GetGraduatingTitle(StudentClassListReport StudentCGPA)
        {
            string title = null;
            try
            {
                if (StudentCGPA.CGPA >= 3.5M && StudentCGPA.CGPA <= 4.0M)
                {
                    title = "DISTICTION";
                }
                else if (StudentCGPA.CGPA >= 3.0M && StudentCGPA.CGPA <= 3.49M)
                {
                    title = "UPPER CREDIT";
                }
                else if (StudentCGPA.CGPA >= 2.5M && StudentCGPA.CGPA <= 2.99M)
                {
                    title = "LOWER CREDIT";
                }
                else if (StudentCGPA.CGPA >= 2.0M && StudentCGPA.CGPA <= 2.49M)
                {
                    title = "PASS";
                }
                else if (StudentCGPA.CGPA < 2.0M)
                {
                    title = "POOR";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return title;
        }
    }
}
