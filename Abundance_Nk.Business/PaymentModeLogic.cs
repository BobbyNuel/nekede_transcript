﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class PaymentModeLogic : BusinessBaseLogic<PaymentMode, PAYMENT_MODE>
    {
        public PaymentModeLogic()
        {
            translator = new PaymentModeTranslator();
        }

        public bool Modify(PaymentMode paymentMode)
        {
            try
            {
                Expression<Func<PAYMENT_MODE, bool>> selector = p => p.Payment_Mode_Id == paymentMode.Id;
                PAYMENT_MODE entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Payment_Mode_Name = paymentMode.Name;
                entity.Payment_Mode_Description = paymentMode.Description;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }



    }



}


