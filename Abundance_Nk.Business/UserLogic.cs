﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;
using System.Transactions;

namespace Abundance_Nk.Business
{
    public class UserLogic : BusinessBaseLogic<User, USER>
    {
       public UserLogic()
       {
           translator = new UserTranslator();
       }

       public bool ValidateUser(string Username, string Password)
       {
           try
           {
               Expression<Func<USER, bool>> selector = p => p.User_Name == Username && p.Password == Password && p.activated == true;
               User UserDetails = GetModelBy(selector);
               //string[] usersToAccept = { "william", "joy" };
               if (UserDetails != null && UserDetails.Activated != null && (UserDetails.Password != null && UserDetails.Activated.Value))
               {
                   UpdateLastLogin(UserDetails);
                   return true;
               }
               else
               {
                   return false;
               }
           }
           catch (Exception)
           {
               throw;
           }
       }
    
        public bool UpdateLastLogin(User user)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.User_Name == user.Username && p.Password == user.Password;
                USER userEntity = GetEntityBy(selector);
                if (userEntity == null || userEntity.User_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                userEntity.User_Name = user.Username;
                userEntity.Password = user.Password;
                userEntity.Email = user.Email;
                userEntity.Role_Id = user.Role.Id;
                userEntity.Security_Question_Id = user.SecurityQuestion.Id;
                userEntity.Security_Answer = user.SecurityAnswer;
                userEntity.LastLoginDate = DateTime.Now;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangeUserPassword(User user)
        {
            try
            {
                Expression<Func<USER, bool>> selector = p => p.User_Name == user.Username;
                USER userEntity = GetEntityBy(selector);
                if (userEntity == null || userEntity.User_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                userEntity.User_Name = user.Username;
                userEntity.Password = user.Password;
                userEntity.Email = user.Email;
                userEntity.Role_Id = user.Role.Id;
                userEntity.Security_Question_Id = user.SecurityQuestion.Id;
                userEntity.Security_Answer = user.SecurityAnswer;
                userEntity.LastLoginDate = user.LastLoginDate;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public override User Create(User user)
        //{
        //    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //    {
                
        //        User newUser = new User();
        //        PersonType personType = new PersonType() { Id = 1 };
        //        Nationality nationality = new Nationality() { Id = 1 };
        //        State state = new State() { Id = "AB" };
        //        user.person.PersonType = personType;
        //        user.person.Nationality = nationality;
        //        user.person.State = state;
        //        user.person.DateEntered = DateTime.Now;
        //        Person newPerson = new Person();
        //        PersonLogic personLogic = new PersonLogic();
        //        newPerson = personLogic.Create(user.person);
        //        user.person = newPerson;
        //        newUser = base.Create(user);
        //        transaction.Complete();
        //        return newUser;
        //    }
        //}

        public override User Create(User user)
        {
            try
            {
                User newUser = new User();
                Person newPerson = new Person() { Id = 91 };
                user.person = newPerson;
                user.Activated = true;

                newUser = base.Create(user);

                return newUser;
            }
            catch (Exception)
            {
                throw;
            } 
        }
        public User Update(User model)
        {
            try
            {
                Expression<Func<USER, bool>> selector = a => a.User_Id == model.Id;
                USER entity = GetEntityBy(selector);

                entity.User_Name = model.Username;
                entity.Password = model.Password;
                entity.Email = model.Email;
                if (model.Role != null)
                {
                    entity.Role_Id = model.Role.Id;
                }
                entity.Security_Answer = model.SecurityAnswer;
                if (model.SecurityQuestion != null && model.SecurityQuestion.Id > 0)
                {
                    entity.Security_Question_Id = model.SecurityQuestion.Id;
                }
                //entity.Person_Id = model.person.Id;
                //if (model.person.Id != null) 
                //{
                //    entity.Person_Id = model.person.Id;
                //}

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return model;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool Modify(User model)
        {
            try
            {
                Expression<Func<USER, bool>> selector = u => u.User_Id == model.Id;
                USER entity = GetEntityBy(selector);
                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                if (model.Password != null)
                {
                    entity.Password = model.Password;
                }

                if (model.Role != null)
                {
                    entity.Role_Id = model.Role.Id;
                }
                //entity.Person_Id = model.person.Id;
                //if (model.person.Id != null)
                //{
                //    entity.Person_Id = model.person.Id;
                //}
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
      
    }
}
