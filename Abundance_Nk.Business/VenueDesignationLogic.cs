﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class VenueDesignationLogic : BusinessBaseLogic<venueDesignation, VENUE_DESIGNATION>
    {
        public VenueDesignationLogic()
        {
            translator = new venueDesignationTranslator();
        }
    }
}
