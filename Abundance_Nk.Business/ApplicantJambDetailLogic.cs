﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class ApplicantJambDetailLogic : BusinessBaseLogic<ApplicantJambDetail, APPLICANT_JAMB_DETAIL>
    {
        public ApplicantJambDetailLogic()
        {
            translator = new ApplicantJambDetailTranslator();
        }

        public bool Modify(ApplicantJambDetail jambDetail)
        {
            try
            {
                Expression<Func<APPLICANT_JAMB_DETAIL, bool>> selector = p => p.Person_Id == jambDetail.Person.Id;
                APPLICANT_JAMB_DETAIL entity = GetEntityBy(selector);

                if (entity == null || entity.Person_Id <= 0)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Person_Id = jambDetail.Person.Id;
                entity.Applicant_Jamb_Registration_Number = jambDetail.JambRegistrationNumber;
                entity.Applicant_Jamb_Score = jambDetail.JambScore;

                if (jambDetail.InstitutionChoice != null)
                {
                    entity.Institution_Choice_Id = jambDetail.InstitutionChoice.Id;
                }

                if (jambDetail.ApplicationForm != null)
                {
                    entity.Application_Form_Id = jambDetail.ApplicationForm.Id;
                }

                if (jambDetail.JambSubject1 != null && jambDetail.JambSubject1.Id > 0)
                {
                    entity.Subject1 = jambDetail.JambSubject1.Id;
                }
                 if (jambDetail.JambSubject2 != null && jambDetail.JambSubject2.Id > 0)
                {
                    entity.Subject2 = jambDetail.JambSubject2.Id;
                }
                 if (jambDetail.JambSubject3 != null && jambDetail.JambSubject3.Id > 0)
                {
                    entity.Subject3 = jambDetail.JambSubject3.Id;
                }
                 if (jambDetail.JambSubject4 != null && jambDetail.JambSubject4.Id > 0)
                {
                    entity.Subject4 = jambDetail.JambSubject4.Id;
                }
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public List<ApplicantData> GetNDMApplicantRanking(Department department, Session session, string dateFrom, string dateTo, int feeTypeId)
        {
            List<ApplicantData> applicantList = new List<ApplicantData>();
            try
            {
                if (!string.IsNullOrEmpty(dateFrom) && !string.IsNullOrEmpty(dateTo))
                {
                    DateTime applicationFrom = ConvertToDate(dateFrom);
                    DateTime applicationTo = ConvertToDate(dateTo);

                    if (feeTypeId <= 0)
                    {
                        applicantList = (from ar in repository.GetBy<VW_NDM_RANKING>(x => x.Department_Id == department.Id && x.Session_Id == session.Id && (x.Date_Submitted >= applicationFrom && x.Date_Submitted <= applicationTo))
                                         select new ApplicantData()
                                         {
                                             Name = ar.Name,
                                             JambRegNumber = ar.Applicant_Jamb_Registration_Number,
                                             ApplicationNumber = ar.Application_Form_Number,
                                             Department = ar.Department_Name,
                                             Session = ar.Session_Name,
                                             RankingScore = ar.Total.ToString(),
                                             JambScore = ar.Applicant_Jamb_Score.ToString(),
                                             Subject1 = ar.Subj1,
                                             Subject2 = ar.Subj2,
                                             Subject3 = ar.Subj3,
                                             Subject4 = ar.Subj4,
                                             Subject5 = ar.Subj5,
                                             QualifiedStatus = ar.Qualified.Value ? "Qualified" : "Not qualified"
                                         }).ToList();
                    }
                    else
                    {
                        applicantList = (from ar in repository.GetBy<VW_NDM_RANKING>(x => x.Department_Id == department.Id && x.Session_Id == session.Id && x.Fee_Type_Id == feeTypeId && (x.Date_Submitted >= applicationFrom && x.Date_Submitted <= applicationTo))
                                         select new ApplicantData()
                                         {
                                             Name = ar.Name,
                                             JambRegNumber = ar.Applicant_Jamb_Registration_Number,
                                             ApplicationNumber = ar.Application_Form_Number,
                                             Department = ar.Department_Name,
                                             Session = ar.Session_Name + " " + ar.Fee_Type_Name,
                                             RankingScore = ar.Total.ToString(),
                                             JambScore = ar.Applicant_Jamb_Score.ToString(),
                                             Subject1 = ar.Subj1,
                                             Subject2 = ar.Subj2,
                                             Subject3 = ar.Subj3,
                                             Subject4 = ar.Subj4,
                                             Subject5 = ar.Subj5,
                                             QualifiedStatus = ar.Qualified.Value ? "Qualified" : "Not qualified"
                                         }).ToList();
                    }
                }
                else
                {
                    if (feeTypeId <= 0)
                    {
                        applicantList = (from ar in repository.GetBy<VW_NDM_RANKING>(x => x.Department_Id == department.Id && x.Session_Id == session.Id)
                                         select new ApplicantData()
                                         {
                                             Name = ar.Name,
                                             JambRegNumber = ar.Applicant_Jamb_Registration_Number,
                                             ApplicationNumber = ar.Application_Form_Number,
                                             Department = ar.Department_Name,
                                             Session = ar.Session_Name,
                                             RankingScore = ar.Total.ToString(),
                                             JambScore = ar.Applicant_Jamb_Score.ToString(),
                                             Subject1 = ar.Subj1,
                                             Subject2 = ar.Subj2,
                                             Subject3 = ar.Subj3,
                                             Subject4 = ar.Subj4,
                                             Subject5 = ar.Subj5,
                                             QualifiedStatus = ar.Qualified.Value ? "Qualified" : "Not qualified"
                                         }).ToList();
                    }
                    else
                    {
                        applicantList = (from ar in repository.GetBy<VW_NDM_RANKING>(x => x.Department_Id == department.Id && x.Session_Id == session.Id && x.Fee_Type_Id == feeTypeId)
                                         select new ApplicantData()
                                         {
                                             Name = ar.Name,
                                             JambRegNumber = ar.Applicant_Jamb_Registration_Number,
                                             ApplicationNumber = ar.Application_Form_Number,
                                             Department = ar.Department_Name,
                                             Session = ar.Session_Name + " " + ar.Fee_Type_Name,
                                             RankingScore = ar.Total.ToString(),
                                             JambScore = ar.Applicant_Jamb_Score.ToString(),
                                             Subject1 = ar.Subj1,
                                             Subject2 = ar.Subj2,
                                             Subject3 = ar.Subj3,
                                             Subject4 = ar.Subj4,
                                             Subject5 = ar.Subj5,
                                             QualifiedStatus = ar.Qualified.Value ? "Qualified" : "Not qualified"
                                         }).ToList();
                    }
                }
            }
            catch (Exception)
            {   
                throw;
            }

            return applicantList;
        }

        private DateTime ConvertToDate(string date)
        {
            DateTime newDate = new DateTime();
            try
            {
                string[] dateSplit = date.Split('/');
                newDate = new DateTime(Convert.ToInt32(dateSplit[2]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[0]));
            }
            catch (Exception)
            {
                throw;
            }

            return newDate;
        }
        
    }




}
