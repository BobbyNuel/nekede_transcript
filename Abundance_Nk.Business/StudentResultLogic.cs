﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Transactions;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class StudentResultLogic : BusinessBaseLogic<StudentResult, STUDENT_RESULT>
    {
        private StudentResultDetailLogic studentResultDetailLogic;

        public StudentResultLogic()
        {
            translator = new StudentResultTranslator();
            studentResultDetailLogic = new StudentResultDetailLogic();
        }

        public List<StudentResult> GetBy(Level level, Programme programme, Department department, SessionSemester sessionSemester)
        {
            try
            {
                Expression<Func<STUDENT_RESULT, bool>> selector = sr => sr.Level_Id == level.Id && sr.Programme_Id == programme.Id && sr.Department_Id == department.Id && sr.Session_Semester_Id == sessionSemester.Id;
                return GetModelsBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetTotalMaximumObtainableScore(Level level, Programme programme, Department department, SessionSemester sessionSemester)
        {
            try
            {
                //List<int?> result = (from sr in repository.GetBy<VW_MAXIMUM_OBTAINABLE_SCORE>(x => x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                //                     select sr.Maximum_Score_Obtainable.Value);


                Expression<Func<STUDENT_RESULT, bool>> selector = sr => sr.Level_Id == level.Id && sr.Programme_Id == programme.Id && sr.Department_Id == department.Id && sr.Session_Semester_Id == sessionSemester.Id;
                List<StudentResult> studentResults = GetModelsBy(selector);
                return studentResults.Sum(s =>s.MaximumObtainableScore);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool Save(StudentResult resultHeader)
        {
            try
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    if (resultHeader.Results != null && resultHeader.Results.Count > 0)
                    {
                        Add(resultHeader);
                        transaction.Complete();
                    }
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public override StudentResult Add(StudentResult resultHeader)
        {
            try
            {
                StudentResult newResultHeader = base.Create(resultHeader);
                if (newResultHeader == null || newResultHeader.Id == 0)
                {
                    throw new Exception("Result Header add opeartion failed! Please try again");
                }

                resultHeader.Id = newResultHeader.Id;
                //List<StudentResultDetail> results = SetHeader(resultHeader, newResultHeader);

                List<StudentResultDetail> results = SetHeader(resultHeader);
                int rowsAdded = studentResultDetailLogic.Create(results);
                if (rowsAdded == 0)
                {
                    throw new Exception("Result Header was succesfully added, but Result Detail Add operation failed! Please try again");
                }

                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                if (courseRegistrationDetailLogic.UpdateCourseRegistrationScore(results))
                {
                    //return newResultHeader;
                    return resultHeader;
                }
                else
                {
                    throw new Exception("Registered course failed on update! Please try again");
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<StudentResultDetail> SetHeader(StudentResult resultHeader)
        {
            try
            {
                foreach (StudentResultDetail result in resultHeader.Results)
                {
                    result.Header = resultHeader;
                    //result.Header.Id = newResultHeader.Id;
                }

                return resultHeader.Results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<Result> GetTranscriptBy(Student student)
        //{
        //    try
        //    {
        //        if ( student == null || student.Id <= 0)
        //        {
        //            throw new Exception("Student not set! Please select student and try again.");
        //        }

        //        List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_2>(x =>  x.Person_Id == student.Id)
        //                                select new Result
        //                                {
        //                                    StudentId = sr.Person_Id,
        //                                    Sex = sr.Sex_Name,
        //                                    Name = sr.Name,
        //                                    MatricNumber = sr.Matric_Number,
        //                                    CourseId = sr.Course_Id,
        //                                    CourseCode = sr.Course_Code,
        //                                    CourseName = sr.Course_Name,
        //                                    CourseUnit = sr.Course_Unit,
        //                                    FacultyName = sr.Faculty_Name,
        //                                    TestScore = sr.Test_Score,
        //                                    ExamScore = sr.Exam_Score,
        //                                    Score = sr.Total_Score,
        //                                    Grade = sr.Grade,
        //                                    GradePoint = sr.Grade_Point,
        //                                    Email = sr.Email,
        //                                    Address = sr.Contact_Address,
        //                                    MobilePhone = sr.Mobile_Phone,
        //                                    PassportUrl = sr.Image_File_Url,
        //                                    GPCU = sr.Grade_Point * sr.Course_Unit,
        //                                    TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,

        //                                    SessionName = sr.Session_Name,
        //                                    Semestername = sr.Semester_Name,
        //                                    LevelName = sr.Level_Name,
        //                                    ProgrammeName = sr.Programme_Name,

        //                                    SessionSemesterId = sr.Session_Semester_Id,
        //                                    SessionSemesterSequenceNumber = sr.Sequence_Number,
        //                                }).ToList();

        //        return results;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //public List<StatementOfResultSummary> GetStatementOfResultSummaryBy(SessionSemester sessionSemester, Level level, Programme programme, Department department, Student student)
        //{
        //    try
        //    {
        //        if (level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0 || student == null || student.Id <= 0)
        //        {
        //            throw new Exception("One or more criteria to get Master Result Sheet not set! Please check your input criteria selection and try again.");
        //        }
      
     
        //        List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_SUMMARY>(x => x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Person_Id == student.Id)
        //                                select new Result
        //                                {
        //                                    StudentId = sr.Person_Id,
        //                                    Sex = sr.Sex_Name,
        //                                    Name = sr.Name,
        //                                    MatricNumber = sr.Matric_Number,
        //                                    CourseUnit = (int)sr.Course_Unit,
        //                                    FacultyName = sr.Faculty_Name,
        //                                    TestScore = sr.Test_Score,
        //                                    ExamScore = sr.Exam_Score,
        //                                    Score = sr.Total_Score,
        //                                    SessionSemesterId = sr.Session_Semester_Id,
        //                                    SessionSemesterSequenceNumber = sr.Session_Semester_Sequence_Number,
        //                                    GradePoint = sr.Grade_Point,
        //                                    GPA = sr.GPA,
        //                                    WGP = sr.WGP,
        //                                    UnitPassed = sr.Unit_Passed,
        //                                    UnitOutstanding = sr.Unit_Outstanding,
        //                                    GPCU = sr.Grade_Point * sr.Course_Unit,
        //                                    TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
        //                                }).ToList();

        //        List<StatementOfResultSummary> resultSummaries = new List<StatementOfResultSummary>();
        //        if (results != null && results.Count > 0)
        //        {
        //            SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
        //            SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

        //            Result currentSemesterResult = results.Where(r => r.SessionSemesterId == sessionSemester.Id ).SingleOrDefault();
        //            Result previousSemesterResult = results.Where(r => r.SessionSemesterSequenceNumber == ss.SequenceNumber - 1).SingleOrDefault();

        //            StatementOfResultSummary unitsAttempted = new StatementOfResultSummary();
        //            StatementOfResultSummary wgp = new StatementOfResultSummary();
        //            StatementOfResultSummary gpa = new StatementOfResultSummary();
        //            StatementOfResultSummary unitPassed = new StatementOfResultSummary();
        //            StatementOfResultSummary unitsOutstanding = new StatementOfResultSummary();

        //            unitsAttempted.Item = "UNITS ATTEMPTED";
        //            wgp.Item = "WEIGHT GRADE POINTS";
        //            gpa.Item = "GRADE POINT AVERAGE";
        //            unitPassed.Item = "UNITS PASSED";
        //            unitsOutstanding.Item = "UNITS OUTSTANDING";

        //            if (previousSemesterResult != null)
        //            {
        //                unitsAttempted.PreviousSemester = previousSemesterResult.CourseUnit.ToString();
        //                wgp.PreviousSemester = previousSemesterResult.WGP.ToString();
        //                gpa.PreviousSemester = Math.Round((decimal)previousSemesterResult.GPA, 2).ToString();

        //                unitPassed.PreviousSemester = previousSemesterResult.UnitPassed.ToString();
        //                unitsOutstanding.PreviousSemester = previousSemesterResult.UnitOutstanding.ToString();
        //            }

        //            if (currentSemesterResult != null)
        //            {
        //                unitsAttempted.CurrentSemester = currentSemesterResult.CourseUnit.ToString();
        //                wgp.CurrentSemester = currentSemesterResult.WGP.ToString();

        //                gpa.CurrentSemester = Math.Round((decimal)currentSemesterResult.GPA, 2).ToString();
        //                unitPassed.CurrentSemester = currentSemesterResult.UnitPassed.ToString();
        //                unitsOutstanding.CurrentSemester = currentSemesterResult.UnitOutstanding.ToString();
        //            }

        //            unitsAttempted.AllSemester = results.Sum(r => r.CourseUnit).ToString();
        //            wgp.AllSemester = results.Sum(r => r.WGP).ToString();

        //            gpa.AllSemester = Math.Round((decimal)results.Sum(r => r.GPA), 2).ToString();
        //            unitPassed.AllSemester = results.Sum(r => r.UnitPassed).ToString();
        //            unitsOutstanding.AllSemester = results.Sum(r => r.UnitOutstanding).ToString();
                    
        //            resultSummaries.Add(unitsAttempted);
        //            resultSummaries.Add(wgp);
        //            resultSummaries.Add(gpa);
        //            resultSummaries.Add(unitPassed);
        //            resultSummaries.Add(unitsOutstanding);
        //        }
                                
        //        return resultSummaries;

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<Result> GetProcessedResutBy(Session session, Semester semester, Level level, Department department, Programme programme)
        {
            try
            {
                if (session == null || session.Id < 0 || level == null || level.Id <= 0 || department == null || department.Id <= 0 || programme == null || programme.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Result not set! Please check your input criteria selection and try again.");
                }
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == session.Id);
                string[] sessionItems = sessions.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                if (sessionNameInt >= 2015)
                {

                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && (p.Activated == true || p.Activated == null))
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                SpecialCase = sr.Special_Case,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }
                else
                {

                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && (p.Activated == true || p.Activated == null))
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                SpecialCase = sr.Special_Case,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }




            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetStudentProcessedResultBy(Session session, Level level, Department department,Student student,Semester semester, Programme programme)
        {
            try
            {

                if (session == null || session.Id < 0 || level == null || level.Id <= 0 || department == null || department.Id <= 0 || student == null || student.Id <= 0 || semester == null || semester.Id <= 0 || programme == null || programme.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Result not set! Please check your input criteria selection and try again.");
                }
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == session.Id);
                string[] sessionItems = sessions.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                if (sessionNameInt >= 2015)
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Person_Id == student.Id && p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && (p.Activated == true || p.Activated == null))
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                DepartmentName = sr.Department_Name,
                                                ProgrammeId = sr.Programme_Id,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                SpecialCase = sr.Special_Case,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }
                else
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Person_Id == student.Id && p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && (p.Activated == true || p.Activated == null))
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                DepartmentName = sr.Department_Name,
                                                ProgrammeId = sr.Programme_Id,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                SpecialCase = sr.Special_Case,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }
    


              

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetDeactivatedProcessedResutBy(Session session, Semester semester, Level level, Department department, Programme programme)
        {
            try
            {
                if (session == null || session.Id < 0 || level == null || level.Id <= 0 || department == null || department.Id <= 0 || programme == null || programme.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Result not set! Please check your input criteria selection and try again.");
                }
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == session.Id);
                string[] sessionItems = sessions.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                if (sessionNameInt >= 2015)
                {

                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && p.Activated == false)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                SpecialCase = sr.Special_Case,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }
                else
                {

                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && p.Activated == false)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                SpecialCase = sr.Special_Case,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                }




            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetDeactivatedStudentProcessedResultBy(Session session, Level level, Department department, Student student, Semester semester, Programme programme)
        {
            try
            {
                if (session == null || session.Id < 0 || level == null || level.Id <= 0 || department == null || department.Id <= 0 || student == null || student.Id <= 0 || semester == null || semester.Id <= 0 || programme == null || programme.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Result not set! Please check your input criteria selection and try again.");
                }
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == session.Id);
                string[] sessionItems = sessions.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(p => p.Person_Id == student.Id && p.Programme_Id == programme.Id && p.Session_Id == session.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Semester_Id == semester.Id && p.Activated == false)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                DepartmentName = sr.Department_Name,
                                                ProgrammeId = sr.Programme_Id,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                Email = sr.Email,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                                Student_Type_Id = sr.Student_Type_Id,
                                                SessionName = sr.Session_Name,
                                                Semestername = sr.Semester_Name,
                                                LevelName = sr.Level_Name,
                                                WGP = sr.WGP,
                                                SpecialCase = sr.Special_Case,
                                                Activated = sr.Activated,
                                                Reason = sr.Reason,
                                                RejectCategory = sr.Reject_Category,
                                                firstname_middle = sr.Othernames,
                                                ProgrammeName = sr.Programme_Name,
                                                Surname = sr.Last_Name,
                                                Firstname = sr.First_Name,
                                                Othername = sr.Other_Name,
                                                TotalScore = sr.Total_Score,
                                                SessionSemesterId = sr.Session_Semester_Id,
                                                SessionSemesterSequenceNumber = sr.Sequence_Number,
                                            }).ToList();

                    return results;
                
               




            }
            catch (Exception)
            {
                throw;
            }
        }
        //public List<SemesterReport> GetStudentTranscriptSemesterResultBy(Level level, Semester semester, Student student)
        //{
        //    try
        //    {
        //        if (semester == null || semester.Id <= 0 || level == null || level.Id <= 0 || student == null || student.Id <= 0)
        //        {
        //            throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
        //        }


        //        List<SemesterReport> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Semester_Id == semester.Id && x.Level_Id == level.Id && x.Person_Id == student.Id)
        //                                    select new SemesterReport
        //                                    {
        //                                        CourseCode = sr.Course_Code,
        //                                        CourseTitle = sr.Course_Name,
        //                                        DeptCourse = sr.Department_Name,
        //                                        GradePoint_CourseUnit = Convert.ToDouble(sr.Grade_Point * sr.Course_Unit),
        //                                        Grade = sr.Grade,
        //                                        Level = sr.Level_Name,
        //                                       Semester = sr.Semester_Name,
        //                                       Session = sr.Session_Name,
        //                                      GradePoint = (double)sr.Grade_Point,

        //                                    }).ToList();

        //            return results;
                
                
    

        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        public List<Result> GetStudentResultBy(SessionSemester sessionSemester, Level level, Programme programme, Department department, Student student)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0 || student == null || student.Id <= 0)
                {
                    throw new Exception("One or more criteria to get this report not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == ss.Session.Id);
                string[] sessionItems = ss.Session.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Person_Id == student.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,

                                                Email = sr.Email,
                                                Address = sr.Contact_Address,
                                                MobilePhone = sr.Mobile_Phone,
                                                PassportUrl = sr.Image_File_Url,

                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();

                    return results;
                
                
    

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetStudentResultBy(SessionSemester sessionSemester, Level level, Programme programme, Department department, Course course)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0 || course == null || course.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == ss.Session.Id);
                string[] sessionItems = ss.Session.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                if (sessionNameInt >= 2015)
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Course_Id == course.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,

                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();

                    return results;
                }
                else
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Course_Id == course.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,

                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();

                    return results;
                }


            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetStudentResultByCourse(SessionSemester sessionSemester, Level level, Programme programme, Department department, Course course)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0 || course == null || course.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == ss.Session.Id);
                string[] sessionItems = ss.Session.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Course_Id == course.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                FacultyName = sr.Faculty_Name,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                ProgrammeName = sr.Programme_Name,
                                                DepartmentName = sr.Department_Name,
                                                Semestername = sr.Semester_Name,
                                                SessionName = sr.Session_Name,
                                                LevelName = sr.Level_Name,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();

                    return results;
                
                


            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetMaterSheetBy(SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == ss.Session.Id);
                string[] sessionItems = ss.Session.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                if (sessionNameInt > 2015)
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                SpecialCase = sr.Special_Case,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,

                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();


                    return results;
                }
                else
                {
                    List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                SpecialCase = sr.Special_Case,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,

                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();


                    return results;
                }
            

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetMaterSheetDetailsBy(SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                SessionLogic sessionLogic = new SessionLogic();
                Session sessions = sessionLogic.GetModelBy(p => p.Session_Id == ss.Session.Id);
                string[] sessionItems = ss.Session.Name.Split('/');
                string sessionNameStr = sessionItems[0];
                int sessionNameInt = Convert.ToInt32(sessionNameStr);
                List<Result> results = null;

                string identifier = null;
                string departmentCode = GetDepartmentCode(department.Id);
                string levels = GetLevelName(level.Id);
                string semesterCode = GetSemesterCodeBy(ss.Semester.Id);
                string sessionCode = GetSessionCodeBy(ss.Session.Name);
                identifier = departmentCode + levels + semesterCode + sessionCode;



                if (sessionNameInt >= 2015)
                {
                     results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                SpecialCase = sr.Special_Case,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                DepartmentName = sr.Department_Name,
                                                ProgrammeName = sr.Programme_Name,
                                                LevelName = sr.Level_Name,
                                                Semestername = sr.Semester_Name,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();
                }
                else
                {
                    results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                            select new Result
                                            {
                                                StudentId = sr.Person_Id,
                                                Name = sr.Name,
                                                MatricNumber = sr.Matric_Number,
                                                CourseId = sr.Course_Id,
                                                CourseCode = sr.Course_Code,
                                                CourseName = sr.Course_Name,
                                                CourseUnit = sr.Course_Unit,
                                                SpecialCase = sr.Special_Case,
                                                TestScore = sr.Test_Score,
                                                ExamScore = sr.Exam_Score,
                                                Score = sr.Total_Score,
                                                Grade = sr.Grade,
                                                GradePoint = sr.Grade_Point,
                                                DepartmentName = sr.Department_Name,
                                                ProgrammeName = sr.Programme_Name,
                                                LevelName = sr.Level_Name,
                                                Semestername = sr.Semester_Name,
                                                GPCU = sr.Grade_Point * sr.Course_Unit,
                                                TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                            }).ToList();
                }
    
                sessionSemester = sessionSemesterLogic.GetModelBy(p => p.Session_Semester_Id == sessionSemester.Id);
                List<Result> studentsResult = GetResultList(sessionSemester, level, programme, department).ToList(); 
                List<Result> masterSheetResult = new List<Result>();
                foreach (Result resultItem in studentsResult)
                {
                    resultItem.Identifier = identifier;
                    Result result = ViewProcessedStudentResult(resultItem.StudentId, sessionSemester, level, programme, department);
                    masterSheetResult.Add(result);
                }

                
                foreach (Result result in masterSheetResult)
                {
                    List<Result> studentResults = results.Where(p => p.StudentId == result.StudentId).ToList();
                    foreach (Result resultItem in studentResults)
                    {
                        resultItem.Identifier = identifier;
                        resultItem.CGPA = result.CGPA;
                        resultItem.Remark = result.Remark;
                        resultItem.GPA = result.GPA;
                        resultItem.UnitOutstanding =
                            studentResults.Where(
                                s => s.Grade == "CD" || s.Grade == "D" || s.Grade == "E" || s.Grade == "F")
                                .Sum(s => s.CourseUnit);
                        resultItem.UnitPassed =
                            studentResults.Where(
                                s => s.Grade == "A" || s.Grade == "AB" || s.Grade == "B" || s.Grade == "BC" || s.Grade == "C")
                                .Sum(s => s.CourseUnit);
                    }
                    
                }

                return results.OrderBy(a=>a.MatricNumber).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<Result> GetResultList(SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                List<Result> filteredResult = new List<Result>();
                StudentResultLogic studentResultLogic = new StudentResultLogic();
                List<string> resultList = studentResultLogic.GetProcessedResutBy(sessionSemester.Session, sessionSemester.Semester, level, department, programme).Select(p => p.StudentId.ToString()).AsParallel().Distinct().ToList();
                List<Result> result = studentResultLogic.GetProcessedResutBy(sessionSemester.Session, sessionSemester.Semester, level, department, programme);
                foreach (string item in resultList)
                {
                    Result resultItem = result.Where(p => p.StudentId.ToString() == item).FirstOrDefault();
                    filteredResult.Add(resultItem);
                }
                return  filteredResult.ToList();
            }
            catch (Exception)
            {

                throw;
            }

        }
        public Result ViewProcessedStudentResult(long id, SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            Result ProcessedResult = new Result();
            string Remark = null;
            try
            {
                
                if (id > 0)
                {
                    Abundance_Nk.Model.Model.Student student = new Model.Model.Student() { Id = id };
                    StudentLogic studentLogic = new StudentLogic();
                    StudentResultLogic studentResultLogic = new StudentResultLogic();


                    if (sessionSemester.Semester != null && sessionSemester.Session != null && programme != null && department != null && level != null)
                    {
                        if (level.Id == 1 || level.Id == 3)
                        {


                            Abundance_Nk.Model.Model.Student studentCheck = studentLogic.GetModelBy(p => p.Person_Id == id);

                            if (sessionSemester.Semester.Id == 1)
                            {
                                List<Result> result = null;
                                if (studentCheck.Activated == true || studentCheck.Activated == null)
                                {
                                    result = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, sessionSemester.Semester, programme);
                                }
                                //else
                                //{
                                //    result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, sessionSemester.Semester, programme);
                                //}
                                List<Result> modifiedResultList = new List<Result>();

                                int totalSemesterCourseUnit = 0;
                                foreach (Result resultItem in result)
                                {
                                  
                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalSemesterCourseUnit == 0)
                                        {
                                            totalSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                    }
                                    modifiedResultList.Add(resultItem);
                                }
                                decimal? firstSemesterGPCUSum = modifiedResultList.Sum(p => p.GPCU);
                                int? firstSemesterTotalSemesterCourseUnit = 0;
                                Result firstYearFirstSemesterResult = modifiedResultList.FirstOrDefault();
                                firstSemesterTotalSemesterCourseUnit = modifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                                decimal? firstSemesterGPA = firstSemesterGPCUSum / firstSemesterTotalSemesterCourseUnit;
                                firstYearFirstSemesterResult.GPA = Decimal.Round((decimal)firstSemesterGPA, 2);
                                firstYearFirstSemesterResult.CGPA = Decimal.Round((decimal)firstSemesterGPA, 2);
                                firstYearFirstSemesterResult.GPCU = firstSemesterGPCUSum;
                                firstYearFirstSemesterResult.TotalSemesterCourseUnit = firstSemesterTotalSemesterCourseUnit;
                                Remark = GetGraduationStatus(firstYearFirstSemesterResult.CGPA, GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student));
                                firstYearFirstSemesterResult.Remark = Remark;
                                ProcessedResult = firstYearFirstSemesterResult;
                               
                            }
                            else
                            {
                                List<Result> result = null;
                                Semester firstSemester = new Semester() { Id = 1 };
                                if (studentCheck.Activated == true || studentCheck.Activated == null)
                                {
                                    result = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, firstSemester, programme);

                                }
                                else
                                {
                                    result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, firstSemester, programme);
                                }
                                List<Result> firstSemesterModifiedResultList = new List<Result>();

                                int totalFirstSemesterCourseUnit = 0;
                                foreach (Result resultItem in result)
                                {

                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalFirstSemesterCourseUnit == 0)
                                        {
                                            totalFirstSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalFirstSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalFirstSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalFirstSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalFirstSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalFirstSemesterCourseUnit;
                                    }
                                    firstSemesterModifiedResultList.Add(resultItem);
                                }
                                decimal? firstSemesterGPCUSum = firstSemesterModifiedResultList.Sum(p => p.GPCU);
                                int? firstSemesterTotalSemesterCourseUnit = 0;
                                Result firstYearFirstSemesterResult = firstSemesterModifiedResultList.FirstOrDefault();
                                firstSemesterTotalSemesterCourseUnit = firstSemesterModifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                                decimal? firstSemesterGPA = firstSemesterGPCUSum / firstSemesterTotalSemesterCourseUnit;
                                firstYearFirstSemesterResult.GPA = Decimal.Round((decimal)firstSemesterGPA);

                                Semester secondSemester = new Semester() { Id = 2 };
                                List<Result> secondSemesterResult = null;
                                if (studentCheck.Activated == true || studentCheck.Activated == null)
                                {
                                    secondSemesterResult = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, secondSemester, programme);
                                }
                                else
                                {
                                    secondSemesterResult = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, secondSemester, programme);
                                }
                                List<Result> secondSemesterModifiedResultList = new List<Result>();

                                int totalSecondSemesterCourseUnit = 0;
                                foreach (Result resultItem in secondSemesterResult)
                                {

                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalSecondSemesterCourseUnit == 0)
                                        {
                                            totalSecondSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalSecondSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalSecondSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                    }
                                    secondSemesterModifiedResultList.Add(resultItem);
                                }
                                decimal? secondSemesterGPCUSum = secondSemesterModifiedResultList.Sum(p => p.GPCU);
                                Result secondSemesterStudentResult = secondSemesterModifiedResultList.FirstOrDefault();
                                
                                secondSemesterStudentResult.GPA = Decimal.Round((decimal)(secondSemesterGPCUSum / (decimal)(secondSemesterModifiedResultList.Min(p => p.TotalSemesterCourseUnit))), 2);
                                secondSemesterStudentResult.CGPA = Decimal.Round((decimal)((firstSemesterGPCUSum + secondSemesterGPCUSum) / (secondSemesterModifiedResultList.Min(p => p.TotalSemesterCourseUnit) + firstSemesterTotalSemesterCourseUnit)), 2);
                                if (secondSemesterStudentResult.GPA < 2.0M && firstYearFirstSemesterResult.GPA < 2.0M)
                                {
                                    Remark = GetGraduationStatus(firstYearFirstSemesterResult.CGPA, firstYearFirstSemesterResult.GPA, secondSemesterStudentResult.GPA, GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student));

                                }
                                else
                                {
                                    Remark = GetGraduationStatus(firstYearFirstSemesterResult.CGPA, GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student));

                                }
                                secondSemesterStudentResult.Remark = Remark;
                                ProcessedResult = secondSemesterStudentResult;
                             
                            }
                            
                        }
                        else
                        {
                            decimal firstYearFirstSemesterGPCUSum = 0;
                            int firstYearFirstSemesterTotalCourseUnit = 0;
                            decimal firstYearSecondSemesterGPCUSum = 0;
                            int firstYearSecondSemesterTotalCourseUnit = 0;
                            decimal secondYearFirstSemesterGPCUSum = 0;
                            int secondYearFirstSemesterTotalCourseUnit = 0;
                            decimal secondYearSecondSemesterGPCUSum = 0;
                            int secondYearSecondSemesterTotalCourseUnit = 0;

                            Result firstYearFirstSemester = GetFirstYearFirstSemesterResultInfo(sessionSemester,  level, programme,  department, student);
                            Result firstYearSecondSemester = GetFirstYearSecondSemesterResultInfo(sessionSemester, level, programme, department, student);
                            if (sessionSemester.Semester.Id == 1)
                            {

                                List<Result> result = null;


                                Abundance_Nk.Model.Model.Student studentCheck = studentLogic.GetModelBy(p => p.Person_Id == student.Id);
                                Semester semester = new Semester() { Id = 1 };

                                if (student.Activated == true || studentCheck.Activated == null)
                                {
                                    result = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, semester, programme);
                                }
                                else
                                {
                                    result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, semester, programme);
                                }
                                List<Result> modifiedResultList = new List<Result>();
                                int totalSemesterCourseUnit = 0;
                                foreach (Result resultItem in result)
                                {

                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalSemesterCourseUnit == 0)
                                        {
                                            totalSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                    }
                                    modifiedResultList.Add(resultItem);
                                }
                                Result secondYearFirstSemesterResult = new Result();
                                decimal? firstSemesterGPCUSum = modifiedResultList.Sum(p => p.GPCU);
                                int? secondYearfirstSemesterTotalSemesterCourseUnit = 0;
                                secondYearFirstSemesterResult = modifiedResultList.FirstOrDefault();
                                secondYearfirstSemesterTotalSemesterCourseUnit = modifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                                secondYearFirstSemesterResult.TotalSemesterCourseUnit = secondYearfirstSemesterTotalSemesterCourseUnit;
                                secondYearFirstSemesterResult.GPCU = firstSemesterGPCUSum;
                                decimal? firstSemesterGPA = firstSemesterGPCUSum / secondYearfirstSemesterTotalSemesterCourseUnit;
                                secondYearFirstSemesterResult.GPA = Decimal.Round((decimal)firstSemesterGPA);
                                if(firstYearFirstSemester.TotalSemesterCourseUnit == null || firstYearSecondSemester.TotalSemesterCourseUnit == null)
                                {
                                    secondYearFirstSemesterResult.CGPA = Decimal.Round((decimal)((firstSemesterGPCUSum + firstYearFirstSemester.GPCU + firstYearSecondSemester.GPCU) / (secondYearfirstSemesterTotalSemesterCourseUnit)), 2);
                                }
                                else
                                {
                                    secondYearFirstSemesterResult.CGPA = Decimal.Round((decimal)((firstSemesterGPCUSum + firstYearFirstSemester.GPCU + firstYearSecondSemester.GPCU) / (firstYearSecondSemester.TotalSemesterCourseUnit + firstYearFirstSemester.TotalSemesterCourseUnit + secondYearfirstSemesterTotalSemesterCourseUnit)), 2);
                               
                                }
                                // List<string> firstYearCarryOverCourses = GetFirstYearCarryOverCourses(sessionSemester, level, programme, department, student);
                                List<string> secondYearFirstSemetserCarryOverCourses = GetSecondYearCarryOverCourses(sessionSemester, level, programme, department, student);
                                secondYearFirstSemesterResult.Remark = GetGraduationStatus(secondYearFirstSemesterResult.CGPA, secondYearFirstSemetserCarryOverCourses);

                                ProcessedResult = secondYearFirstSemesterResult;

                            }
                            else if (sessionSemester.Semester.Id == 2)
                            {

                                List<Result> result = null;


                                Abundance_Nk.Model.Model.Student studentCheck = studentLogic.GetModelBy(p => p.Person_Id == student.Id);
                                Semester semester = new Semester() { Id = 1 };

                                if (student.Activated == true || studentCheck.Activated == null)
                                {
                                    result = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, semester, programme);
                                }
                                else
                                {
                                    result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, semester, programme);
                                }
                                List<Result> modifiedResultList = new List<Result>();
                                int totalSemesterCourseUnit = 0;
                                foreach (Result resultItem in result)
                                {

                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalSemesterCourseUnit == 0)
                                        {
                                            totalSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                    }
                                    modifiedResultList.Add(resultItem);
                                }
                                Result secondYearFirstSemesterResult = new Result();
                                decimal? secondYearfirstSemesterGPCUSum = modifiedResultList.Sum(p => p.GPCU);
                                int? secondYearfirstSemesterTotalSemesterCourseUnit = 0;
                                secondYearfirstSemesterTotalSemesterCourseUnit = modifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                                secondYearFirstSemesterResult.TotalSemesterCourseUnit = secondYearfirstSemesterTotalSemesterCourseUnit;
                                secondYearFirstSemesterResult.GPCU = secondYearfirstSemesterGPCUSum;
                                decimal? firstSemesterGPA = secondYearfirstSemesterGPCUSum / secondYearfirstSemesterTotalSemesterCourseUnit;
                                secondYearFirstSemesterResult.GPA = Decimal.Round((decimal)firstSemesterGPA, 2);

                                //Second semester second year

                                List<Result> secondSemesterResult = null;



                                Semester secondSemester = new Semester() { Id = 2 };

                                if (student.Activated == true || studentCheck.Activated == null)
                                {
                                    secondSemesterResult = studentResultLogic.GetStudentProcessedResultBy(sessionSemester.Session, level, department, student, secondSemester, programme);
                                }
                                else
                                {
                                    secondSemesterResult = studentResultLogic.GetDeactivatedStudentProcessedResultBy(sessionSemester.Session, level, department, student, secondSemester, programme);
                                }
                                List<Result> secondSemesterModifiedResultList = new List<Result>();
                                int totalSecondSemesterCourseUnit = 0;
                                foreach (Result resultItem in secondSemesterResult)
                                {

                                    decimal WGP = 0;

                                    if (resultItem.SpecialCase != null)
                                    {

                                        resultItem.GPCU = 0;
                                        if (totalSecondSemesterCourseUnit == 0)
                                        {
                                            totalSecondSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }
                                        else
                                        {
                                            totalSecondSemesterCourseUnit -= resultItem.CourseUnit;
                                            resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                            resultItem.Grade = "-";
                                        }

                                    }
                                    if (totalSecondSemesterCourseUnit > 0)
                                    {
                                        resultItem.TotalSemesterCourseUnit = totalSecondSemesterCourseUnit;
                                    }
                                    secondSemesterModifiedResultList.Add(resultItem);
                                }
                                Result secondYearSecondtSemesterResult = new Result();
                                decimal? secondYearSecondtSemesterGPCUSum = secondSemesterModifiedResultList.Sum(p => p.GPCU);
                                int? secondYearSecondSemesterTotalSemesterCourseUnit = 0;
                                secondYearSecondSemesterTotalSemesterCourseUnit = secondSemesterModifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                                secondYearSecondtSemesterResult = secondSemesterModifiedResultList.FirstOrDefault();
                                secondYearSecondtSemesterResult.TotalSemesterCourseUnit = secondYearSecondSemesterTotalSemesterCourseUnit;
                                secondYearSecondtSemesterResult.GPCU = secondYearSecondtSemesterGPCUSum;
                                decimal? secondYearSecondSmesterGPA = secondYearSecondtSemesterGPCUSum / secondYearSecondSemesterTotalSemesterCourseUnit;
                               // viewModel.Result = secondSemesterModifiedResultList.FirstOrDefault();
                                secondYearSecondtSemesterResult.GPA = Decimal.Round((decimal)secondYearSecondSmesterGPA, 2);
                                secondYearSecondtSemesterResult.CGPA = Decimal.Round((decimal)((secondYearfirstSemesterGPCUSum + firstYearFirstSemester.GPCU + firstYearSecondSemester.GPCU + secondYearSecondtSemesterGPCUSum) / (firstYearSecondSemester.TotalSemesterCourseUnit + firstYearFirstSemester.TotalSemesterCourseUnit + secondYearfirstSemesterTotalSemesterCourseUnit + secondYearSecondSemesterTotalSemesterCourseUnit)), 2);
                                List<string> secondYearSecondSemetserCarryOverCourses = GetSecondYearCarryOverCourses(sessionSemester, level, programme, department, student);
                                if (secondYearSecondtSemesterResult.GPA < 2.0M && secondYearFirstSemesterResult.GPA < 2.0M)
                                {
                                    secondYearSecondtSemesterResult.Remark = GetGraduationStatus(secondYearFirstSemesterResult.CGPA, secondYearFirstSemesterResult.GPA, secondYearSecondtSemesterResult.GPA, secondYearSecondSemetserCarryOverCourses);

                                }
                                else
                                {
                                    secondYearSecondtSemesterResult.Remark = GetGraduationStatus(secondYearFirstSemesterResult.CGPA, secondYearSecondSemetserCarryOverCourses);

                                }
                               
                                ProcessedResult = secondYearSecondtSemesterResult;
                            }



                        }
                    }
                }
                
            }
            catch (Exception )
            {

               
            }
            return ProcessedResult;
        }
        private List<string> GetFirstYearCarryOverCourses(SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Student student)
        {
            try
            {
                List<CourseRegistrationDetail> courseRegistrationdetails = new List<CourseRegistrationDetail>();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                List<string> courseCodes =  new List<string>();
                if (lvl.Id == 1 || lvl.Id == 3)
                {
                    courseRegistrationdetails = courseRegistrationDetailLogic.GetModelsBy(crd => crd.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && (crd.Test_Score + crd.Exam_Score) < 40 && crd.Special_Case == null);
                    if (sessionSemester.Semester.Id == 1)
                    {
                        courseRegistrationdetails = courseRegistrationdetails.Where(p => p.Semester.Id == 1).ToList();
                        if (courseRegistrationdetails.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                            {
                                if (courseRegistrationDetail.SpecialCase == null)
                                {
                                    
                                    courseCodes.Add(courseRegistrationDetail.Course.Code);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (courseRegistrationdetails.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                            {
                                if (courseRegistrationDetail.SpecialCase == null)
                                {
                                  
                                    courseCodes.Add(courseRegistrationDetail.Course.Code);
                                }
                            }
                        }
                    }
                    
                }

                return courseCodes;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        private List<string> GetSecondYearCarryOverCourses(SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Student student)
        {
            try
            {
                List<CourseRegistrationDetail> courseRegistrationdetails = new List<CourseRegistrationDetail>();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                List<string> courseCodes = courseCodes = new List<string>();
                List<string> firstYearCarryOverCourseCodes = null;
                
                StudentLevel studentLevel = null;

                if (lvl.Id == 2)
                {
                   studentLevel = studentLevelLogic.GetModelsBy(p => p.Person_Id == student.Id && p.Level_Id == 1 && p.Department_Id == department.Id && p.Programme_Id == programme.Id).FirstOrDefault();//ND1
                   if (studentLevel != null)
                    {
                        SessionSemester ss = new SessionSemester();
                        ss.Session = studentLevel.Session;
                        ss.Semester = new Semester() { Id = 2 };// Second semester to get all carry over for first year

                        firstYearCarryOverCourseCodes = GetFirstYearCarryOverCourses(ss, studentLevel.Level, studentLevel.Programme, studentLevel.Department, studentLevel.Student);
                
                    }
                    }
                else if(lvl.Id == 4)
                {
                    studentLevel = studentLevelLogic.GetModelsBy(p => p.Person_Id == student.Id && p.Level_Id == 3 && p.Department_Id == department.Id && p.Programme_Id == programme.Id).FirstOrDefault();//HND1
                    SessionSemester ss = new SessionSemester();
                    if (studentLevel != null)
                    {
                        ss.Session = studentLevel.Session;
                        ss.Semester = new Semester() { Id = 2 };// Second semester to get all carry over for first year

                        firstYearCarryOverCourseCodes = GetFirstYearCarryOverCourses(ss, studentLevel.Level, studentLevel.Programme, studentLevel.Department, studentLevel.Student);
                    }
                    
                }
               
                if (lvl.Id == 2 || lvl.Id == 4)
                {
                    courseRegistrationdetails = courseRegistrationDetailLogic.GetModelsBy(crd => crd.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && (crd.Test_Score + crd.Exam_Score) < 40 && crd.Special_Case == null);
                    if (sessionSemester.Semester.Id == 1)
                    {
                        courseRegistrationdetails = courseRegistrationdetails.Where(p => p.Semester.Id == 1).ToList();
                        if (courseRegistrationdetails.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                            {
                                if (courseRegistrationDetail.SpecialCase == null)
                                {
                                    
                                    courseCodes.Add(courseRegistrationDetail.Course.Code);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (courseRegistrationdetails.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                            {
                                if (courseRegistrationDetail.SpecialCase == null)
                                {
                                    
                                    courseCodes.Add(courseRegistrationDetail.Course.Code);
                                }
                            }
                        }
                    }

                }
                //compare courses
                courseCodes = CompareCourses(courseCodes, firstYearCarryOverCourseCodes, sessionSemester, lvl, programme, department, student);
                return courseCodes;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private List<string> CompareCourses(List<string> courseCodes, List<string> firstYearCarryOverCourseCodes,SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Student student)
        {
           
            try
            {
                if (firstYearCarryOverCourseCodes != null)
                {
                    CourseRegistrationDetailLogic courseRegistrationDetail = new CourseRegistrationDetailLogic();
                    for (int i = 0; i < firstYearCarryOverCourseCodes.Count(); i++)
                    {
                        if (courseCodes != null && firstYearCarryOverCourseCodes != null)
                        {
                            if (courseCodes.Contains(firstYearCarryOverCourseCodes[i]))
                            {
                                courseCodes.Add(firstYearCarryOverCourseCodes[i]);
                                firstYearCarryOverCourseCodes.RemoveAt(i);
                            }
                            else
                            {
                                string Coursecode = firstYearCarryOverCourseCodes[i];
                                CourseRegistrationDetail course = courseRegistrationDetail.GetModelBy(p => p.COURSE.Course_Code == Coursecode && p.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && p.Semester_Id == sessionSemester.Semester.Id && p.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id);
                                if (course != null)
                                {
                                    firstYearCarryOverCourseCodes.RemoveAt(i);
                                }
                                else
                                {
                                    courseCodes.Add(firstYearCarryOverCourseCodes[i]);
                                }
                            }
                        }

                    }
                }
                
            }
            catch (Exception)
            {
                
                throw;
            }
            return courseCodes;
        }
        private Result GetFirstYearSecondSemesterResultInfo(SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Model.Model.Student student)
        {
            try
            {
                List<Result> result = null;
                StudentLogic studentLogic = new StudentLogic();
                StudentResultLogic studentResultLogic = new StudentResultLogic();
                Abundance_Nk.Model.Model.Student studentCheck = studentLogic.GetModelBy(p => p.Person_Id == student.Id);
                Semester semester = new Semester() { Id = 2 };
                Level level = null;
                if (lvl.Id == 2)
                {
                    level = new Level() { Id = 1 };
                }
                else
                {
                    level = new Level() { Id = 3 };
                }
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLevel studentLevel = studentLevelLogic.GetModelsBy(p => p.Person_Id == student.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Programme_Id == programme.Id).FirstOrDefault();

                List<Result> modifiedResultList = new List<Result>();

                if (studentLevel != null)
                {
                    if (student.Activated == true || studentCheck.Activated == null)
                    {
                        result = studentResultLogic.GetStudentProcessedResultBy(studentLevel.Session, level, studentLevel.Department, student, semester, studentLevel.Programme);
                    }
                    else
                    {
                        result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(studentLevel.Session, level, studentLevel.Department, student, semester, studentLevel.Programme);
                    }
                    
                    int totalSemesterCourseUnit = 0;
                    foreach (Result resultItem in result)
                    {

                        decimal WGP = 0;

                        if (resultItem.SpecialCase != null)
                        {

                            resultItem.GPCU = 0;
                            if (totalSemesterCourseUnit == 0)
                            {
                                totalSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                resultItem.Grade = "-";
                            }
                            else
                            {
                                totalSemesterCourseUnit -= resultItem.CourseUnit;
                                resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                resultItem.Grade = "-";
                            }

                        }
                        if (totalSemesterCourseUnit > 0)
                        {
                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                        }
                        modifiedResultList.Add(resultItem);
                    }
                }

                
                Result firstYearFirstSemesterResult = new Result();
                decimal? firstSemesterGPCUSum = modifiedResultList.Sum(p => p.GPCU);
                int? firstSemesterTotalSemesterCourseUnit = 0;
                firstSemesterTotalSemesterCourseUnit = modifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                firstYearFirstSemesterResult.TotalSemesterCourseUnit = firstSemesterTotalSemesterCourseUnit;
                firstYearFirstSemesterResult.GPCU = firstSemesterGPCUSum;
                return firstYearFirstSemesterResult;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private Result GetFirstYearFirstSemesterResultInfo(SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Model.Model.Student student)
        {
            try
            {
                List<Result> result = null;
                StudentLogic studentLogic = new StudentLogic();
                StudentResultLogic studentResultLogic = new StudentResultLogic();
                Abundance_Nk.Model.Model.Student studentCheck = studentLogic.GetModelBy(p => p.Person_Id == student.Id);


                Semester semester = new Semester() { Id = 1 };
                Level level = null;
                if (lvl.Id == 2)
                {
                    level = new Level() { Id = 1 };
                }
                else
                {
                    level = new Level() { Id = 3 };
                }
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLevel studentLevel = studentLevelLogic.GetModelsBy(p => p.Person_Id == student.Id && p.Level_Id == level.Id && p.Department_Id == department.Id && p.Programme_Id == programme.Id).FirstOrDefault();

                List<Result> modifiedResultList = new List<Result>();

                if (studentLevel != null)
                {
                    if (student.Activated == true || studentCheck.Activated == null)
                    {
                        result = studentResultLogic.GetStudentProcessedResultBy(studentLevel.Session, level, studentLevel.Department, student, semester, studentLevel.Programme);
                    }
                    else
                    {
                        result = studentResultLogic.GetDeactivatedStudentProcessedResultBy(studentLevel.Session, level, studentLevel.Department, student, semester, studentLevel.Programme);
                    }
                    
                    int totalSemesterCourseUnit = 0;
                    foreach (Result resultItem in result)
                    {

                        decimal WGP = 0;

                        if (resultItem.SpecialCase != null)
                        {

                            resultItem.GPCU = 0;
                            if (totalSemesterCourseUnit == 0)
                            {
                                totalSemesterCourseUnit = (int)resultItem.TotalSemesterCourseUnit - resultItem.CourseUnit;
                                resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                resultItem.Grade = "-";
                            }
                            else
                            {
                                totalSemesterCourseUnit -= resultItem.CourseUnit;
                                resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                                resultItem.Grade = "-";
                            }

                        }
                        if (totalSemesterCourseUnit > 0)
                        {
                            resultItem.TotalSemesterCourseUnit = totalSemesterCourseUnit;
                        }
                        modifiedResultList.Add(resultItem);
                    }
                }
                
                Result firstYearFirstSemesterResult = new Result();
                decimal? firstSemesterGPCUSum = modifiedResultList.Sum(p => p.GPCU);
                int? firstSemesterTotalSemesterCourseUnit = 0;
                firstSemesterTotalSemesterCourseUnit = modifiedResultList.Min(p => p.TotalSemesterCourseUnit);
                firstYearFirstSemesterResult.TotalSemesterCourseUnit = firstSemesterTotalSemesterCourseUnit;
                firstYearFirstSemesterResult.GPCU = firstSemesterGPCUSum;
                return firstYearFirstSemesterResult;

            }
            catch (Exception)
            {

                throw;
            }
        }
        private List<string> GetCarryOverCourseCodes(SessionSemester ss, Level lvl, Programme programme, Department department, Model.Model.Student student)
        {
            try
            {
                List<string> courseCodes = new List<string>();
                List<CourseRegistrationDetail> courseRegistrationdetails = new List<CourseRegistrationDetail>();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                StudentLevel studentLevel = new StudentLevel();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();

     
                Level level = null;
                if (lvl.Id == 1 || lvl.Id == 3)
                {
                    level = new Level() { Id = 1 };
                    courseCodes = GetFirstYearCarryOverCourses(ss, lvl, programme, department, student);

                }
                else
                {
                    level = new Level() { Id = 3 };
                }
            courseRegistrationdetails = courseRegistrationDetailLogic.GetModelsBy(crd => crd.STUDENT_COURSE_REGISTRATION.Session_Id == ss.Session.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && (crd.Test_Score + crd.Exam_Score) < 40 && crd.Special_Case == null);

            if (courseRegistrationdetails != null)
            {
                if (ss.Semester.Id == 1)
                {
                    courseRegistrationdetails = courseRegistrationdetails.Where(p => p.Semester.Id == 1).ToList();
                    if (courseRegistrationdetails.Count > 0)
                    {
                        foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                        {
                            if (courseRegistrationDetail.SpecialCase == null)
                            {
                                courseCodes.Add(courseRegistrationDetail.Course.Code);
                            }
                        }
                    }
                }
                else
                {
                    if (courseRegistrationdetails.Count > 0)
                    {
                        foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                        {
                            if (courseRegistrationDetail.SpecialCase == null)
                            {
                                courseCodes.Add(courseRegistrationDetail.Course.Code);
                            }
                        }
                    }
                }
            }
            return courseCodes;
            }
            catch (Exception)
            {
                
                throw;
            }
   
            }
        private string GetGraduationStatus(decimal? CGPA, List<string> courseCodes)
        {
            string remark = null;
            int count = 0;
            try
            {
                if (courseCodes.Count > 0)
                {
                    for (int i = 0; i < courseCodes.Count; i++)
                    {
                        count += 1;
                    }

                    remark = count + "R";
                    //if (CGPA > 3.50M && CGPA <= 4.00M)
                    //{
                    //    remark = "DISTINCTION";
                    //}
                    //else if (CGPA > 3.00M && CGPA <= 3.50M)
                    //{
                    //    remark = "VERY GOOD";
                    //}
                    //else if (CGPA > 2.50M && CGPA <= 3.00M)
                    //{
                    //    remark = "GOOD";
                    //}
                    //else if (CGPA > 2.00M && CGPA <= 2.50M)
                    //{
                    //    remark = "FAIRLY GOOD";
                    //}
                    //else if (CGPA > 1.50M && CGPA <= 2.00M)
                    //{
                    //    remark = "PASS";
                    //}
                    //else if (CGPA > 0.00M && CGPA <= 1.50M)
                    //{
                    //    remark = "FAIL";
                    //}
                    //else if (CGPA == 0.0M)
                    //{
                    //    remark = "WORTHLESS";
                    //}
                }
                else
                {
                    remark = "PASS";
                }   
            }
            catch(Exception )
            {
                throw;
            }
            return remark;
        }
        private string GetGraduationStatus(decimal? CGPA, decimal? firstSemesterGPA, decimal? secondSemesterGPA, List<string> courseCodes)
        {
            string remark = null;
            try
            {
                if (firstSemesterGPA != null && secondSemesterGPA != null)
                {
                    if (firstSemesterGPA < 2.0M && secondSemesterGPA < 2.0M)
                    {
                        remark = "PROBATION ";
                    }
                    if (courseCodes.Count != 0)
                    {
                        remark += "CO-";
                            for (int i = 0; i < courseCodes.Count(); i++)
                            {
                                remark += ("|" + courseCodes[i]);
                            }
                    }
                }
                //if (courseCodes.Count == 0)
                //{
                //    if (CGPA >= 3.5M && CGPA <= 4.0M)
                //    {
                //        remark = "RHL; PASSED: DISTICTION";
                //    }
                //    else if (CGPA >= 3.25M && CGPA <= 3.49M)
                //    {
                //        remark = "DHL; PASSED: UPPER CREDIT";
                //    }
                //    else if (CGPA >= 3.0M && CGPA < 3.25M)
                //    {
                //        remark = "PAS; PASSED: UPPER CREDIT";
                //    }
                //    else if (CGPA >= 2.5M && CGPA <= 2.99M)
                //    {
                //        remark = "PAS; PASSED: LOWER CREDIT";
                //    }
                //    else if (CGPA >= 2.0M && CGPA <= 2.49M)
                //    {
                //        remark = "PAS; PASSED: PASS";
                //    }
                //    else if (CGPA < 2.0M)
                //    {
                //        remark = "FAIL";
                //    }
                //}
                //else
                //{
                //   c
                //}


            }
            catch (Exception)
            {
            }
            return remark;
        }
   
        public List<UploadedCourseFormat> GetUploadedCourses(Session session, Semester semester)
        {
            try
            {
                if (session == null || session.Id <= 0 || semester == null || semester.Id <= 0)
                {
                    throw new Exception("One or more criteria to get the uploaded courses is not set! Please check your input criteria selection and try again.");
                }
                List<UploadedCourseFormat> uploadedCourses = (from uc in repository.GetBy<VW_UPLOADED_COURSES>(x => x.Session_Id == session.Id && x.Semester_Id == semester.Id )
                                                              select new UploadedCourseFormat
                                                              {
                                                                  Programme = uc.Programme_Name,
                                                                  Level = uc.Level_Name,
                                                                  Department = uc.Department_Name,
                                                                  CourseCode = uc.Course_Code,
                                                                  CourseTitle = uc.Course_Name,
                                                                  ProgrammeId = uc.Programme_Id,
                                                                  DepartmentId = uc.Department_Id,
                                                                  SessionId = uc.Session_Id,
                                                                  SemesterId = uc.Semester_Id,
                                                                  LevelId = uc.Level_Id,
                                                                  CourseId = uc.Course_Id 
                                                              }).ToList();
                return uploadedCourses.OrderBy(uc => uc.Programme).ToList();
            }
            catch (Exception)
            {   
                throw;
            }
        } 
    
        public List<UploadedCourseFormat> GetUploadedAlternateCourses(Session session, Semester semester)
        {
            try
            {
                if (session == null || session.Id <= 0 || semester == null || semester.Id <= 0)
                {
                    throw new Exception("One or more criteria to get the uploaded courses is not set! Please check your input criteria selection and try again.");
                }
                List<UploadedCourseFormat> uploadedCourses = (from uc in repository.GetBy<VW_STUDENT_RESULT_RAW_SCORE_SHEET_UNREGISTERED>(x => x.Session_Id == session.Id && x.Semester_Id == semester.Id )
                                                              select new UploadedCourseFormat
                                                              {
                                                                  Programme = uc.Programme_Name,
                                                                  Level = uc.Level_Name,
                                                                  Department = uc.Department_Name,
                                                                  CourseCode = uc.Course_Code,
                                                                  CourseTitle = uc.Course_Name,
                                                                  LecturerName = "",
                                                                  ProgrammeId = uc.Programme_Id,
                                                                  DepartmentId = 1,
                                                                  SessionId = uc.Session_Id,
                                                                  SemesterId = uc.Semester_Id,
                                                                  LevelId = uc.Level_Id,
                                                                  CourseId = uc.Course_Id
                                                              }).ToList();

                return uploadedCourses.OrderBy(uc => uc.Programme).ToList();
            }
            catch (Exception)
            {   
                throw;
            }
        } 
    
        private string GetIdentifierBy(STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED rawscoresheetItem)
        {
            try
            {
                string identifier = null;
                string departmentCode = rawscoresheetItem.COURSE.DEPARTMENT.Department_Code;
                string level = rawscoresheetItem.LEVEL.Level_Name;
                string semesterCode = GetSemesterCodeBy(rawscoresheetItem.Semester_Id);
                string sessionCode = GetSessionCodeBy(rawscoresheetItem.SESSION.Session_Name);
                identifier = departmentCode + level + semesterCode + sessionCode;
                return identifier;
            }
            catch(Exception)
            {

                throw;
            }
        }
        public List<StatementOfResultSummary> GetStatementOfResultSummaryBy(SessionSemester sessionSemester, Level level, Programme programme, Department department, Student student)
        {
            try
            {
                if (level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0 || student == null || student.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Master Result Sheet not set! Please check your input criteria selection and try again.");
                }


                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_SUMMARY>(x => x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && x.Person_Id == student.Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            CourseUnit = (int)sr.Course_Unit,
                                            FacultyName = sr.Faculty_Name,
                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            SessionSemesterId = sr.Session_Semester_Id,
                                            SessionSemesterSequenceNumber = sr.Session_Semester_Sequence_Number,
                                            GradePoint = sr.Grade_Point,
                                            GPA = sr.GPA,
                                            WGP = sr.WGP,
                                            UnitPassed = sr.Unit_Passed,
                                            UnitOutstanding = sr.Unit_Outstanding,
                                            GPCU = sr.Grade_Point * sr.Course_Unit,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                List<StatementOfResultSummary> resultSummaries = new List<StatementOfResultSummary>();
                if (results != null && results.Count > 0)
                {
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                    Result currentSemesterResult = results.Where(r => r.SessionSemesterId == sessionSemester.Id).SingleOrDefault();
                    Result previousSemesterResult = results.Where(r => r.SessionSemesterSequenceNumber == ss.SequenceNumber - 1).SingleOrDefault();

                    StatementOfResultSummary unitsAttempted = new StatementOfResultSummary();
                    StatementOfResultSummary wgp = new StatementOfResultSummary();
                    StatementOfResultSummary gpa = new StatementOfResultSummary();
                    StatementOfResultSummary unitPassed = new StatementOfResultSummary();
                    StatementOfResultSummary unitsOutstanding = new StatementOfResultSummary();

                    unitsAttempted.Item = "UNITS ATTEMPTED";
                    wgp.Item = "CUMULATIVE GRADE POINT";
                    gpa.Item = "GRADE POINT AVERAGE";
                    unitPassed.Item = "UNITS PASSED";
                    unitsOutstanding.Item = "UNITS OUTSTANDING";

                    if (previousSemesterResult != null)
                    {
                        unitsAttempted.PreviousSemester = previousSemesterResult.CourseUnit.ToString();
                        wgp.PreviousSemester = previousSemesterResult.WGP.ToString();
                        gpa.PreviousSemester = Math.Round((decimal)previousSemesterResult.GPA, 2).ToString();

                        unitPassed.PreviousSemester = previousSemesterResult.UnitPassed.ToString();
                        unitsOutstanding.PreviousSemester = previousSemesterResult.UnitOutstanding.ToString();
                    }

                    if (currentSemesterResult != null)
                    {
                        unitsAttempted.CurrentSemester = currentSemesterResult.CourseUnit.ToString();
                        wgp.CurrentSemester = currentSemesterResult.WGP.ToString();

                        gpa.CurrentSemester = Math.Round((decimal)currentSemesterResult.GPA, 2).ToString();
                        unitPassed.CurrentSemester = currentSemesterResult.UnitPassed.ToString();
                        unitsOutstanding.CurrentSemester = currentSemesterResult.UnitOutstanding.ToString();
                    }

                    unitsAttempted.AllSemester = results.Sum(r => r.CourseUnit).ToString();
                    wgp.AllSemester = results.Sum(r => r.WGP).ToString();

                    gpa.AllSemester = Math.Round((decimal)results.Sum(r => r.GPA), 2).ToString();
                    unitPassed.AllSemester = results.Sum(r => r.UnitPassed).ToString();
                    unitsOutstanding.AllSemester = results.Sum(r => r.UnitOutstanding).ToString();

                    resultSummaries.Add(unitsAttempted);
                    resultSummaries.Add(wgp);
                    resultSummaries.Add(gpa);
                    resultSummaries.Add(unitPassed);
                    resultSummaries.Add(unitsOutstanding);
                }

                return resultSummaries;

            }
            catch (Exception)
            {
                throw;
            }
        }
        
        private string GetSessionCodeBy(string sessionName)
        {
            try
            {
                string sessionCode = null;
                string[] sessionArray = sessionName.Split('/');
                string sessionYear = sessionArray[1];
                sessionCode = sessionYear.Substring(2,2);
                return sessionCode;
            }
            catch(Exception)
            {

                throw;
            }
        }

        private string GetSemesterCodeBy(int semesterId)
        {
            try
            {
                if(semesterId == 1)
                {
                    return "F";
                }
                else
                {
                    return "S";
                }
            }
            catch(Exception)
            {

                throw;
            }
        }

        private string GetDepartmentCode(int departmentid)
        {
            string code = "";
            try
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                code = departmentLogic.GetModelBy(m => m.Department_Id == departmentid).Code;
            }
            catch (Exception)
            {
                
                throw;
            }
            return code;
        }
        private string GetLevelName(int levelId)
        {
            string code = "";
            try
            {
                LevelLogic levelLogic = new LevelLogic();
                code = levelLogic.GetModelBy(m => m.Level_Id == levelId).Name;
            }
            catch (Exception)
            {
                
                throw;
            }
            return code;
        }
        public List<Result> GetExamReferenceList(SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Exam Pass List not set! Please check your input criteria selection and try again.");
                }
                StudentLogic studentLogic = new StudentLogic();
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                string[] failureGrades = {"F", "CD", "D", "E"};

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && failureGrades.Contains(x.Grade))
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            SessionName = sr.Session_Name,
                                            FacultyName = sr.Faculty_Name,
                                            ProgrammeName = sr.Programme_Name,
                                            LevelName = sr.Level_Name,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                sessionSemester = sessionSemesterLogic.GetModelBy(p => p.Session_Semester_Id == sessionSemester.Id);
                List<Result> studentsResult = GetResultList(sessionSemester, level, programme, department).ToList();
                List<Result> masterSheetResult = new List<Result>();
                List<Result> mainSheetResult = new List<Result>();
                foreach (Result resultItem in studentsResult)
                {
                    Result result = ViewProcessedStudentResult(resultItem.StudentId, sessionSemester, level, programme, department);
                    masterSheetResult.Add(result);
                }
                
                foreach (Result result in masterSheetResult)
                {
                    List<Result> studentResults = results.Where(p => p.StudentId == result.StudentId).ToList();
                    foreach (Result resultItem in studentResults)
                    {
                        resultItem.CGPA = result.CGPA;
                        
                        resultItem.GPA = result.GPA;
                        resultItem.UnitOutstanding =
                            studentResults.Where(
                                s => s.Grade == "CD" || s.Grade == "D" || s.Grade == "E" || s.Grade == "F")
                                .Sum(s => s.CourseUnit);
                        resultItem.UnitPassed =
                            studentResults.Where(
                                s => s.Grade == "A" || s.Grade == "AB" || s.Grade == "B" || s.Grade == "BC" || s.Grade == "C")
                                .Sum(s => s.CourseUnit);
                        if (resultItem.UnitOutstanding > 0)
                        {
                            if (resultItem.Grade == "CD" || resultItem.Grade == "D" || resultItem.Grade == "E" || resultItem.Grade == "F")
                            {
                                results.Where(p => p.StudentId == result.StudentId).LastOrDefault().Remark += resultItem.CourseCode + " | "; 
                            } 
                        }
                    }
                    if (studentResults.Count > 0)
                    {
                        mainSheetResult.Add(results.Where(p => p.StudentId == result.StudentId).LastOrDefault());  
                    }
                    
                }

                return mainSheetResult.OrderBy(a => a.MatricNumber).ToList();

            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetExamPassList(SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Exam Pass List not set! Please check your input criteria selection and try again.");
                }

                StudentLogic studentLogic = new StudentLogic();
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);

                string[] failureGrades = { "F", "CD", "D", "E" };

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == ss.Semester.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id && !failureGrades.Contains(x.Grade))
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            SessionName = sr.Session_Name,
                                            FacultyName = sr.Faculty_Name,
                                            ProgrammeName = sr.Programme_Name,
                                            LevelName = sr.Level_Name,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                sessionSemester = sessionSemesterLogic.GetModelBy(p => p.Session_Semester_Id == sessionSemester.Id);
                List<Result> studentsResult = GetResultList(sessionSemester, level, programme, department).ToList();
                List<Result> masterSheetResult = new List<Result>();
                foreach (Result resultItem in studentsResult)
                {
                    Result result = ViewProcessedStudentResult(resultItem.StudentId, sessionSemester, level, programme, department);
                    masterSheetResult.Add(result);
                }


                foreach (Result result in masterSheetResult)
                {
                    List<Result> studentResults = results.Where(p => p.StudentId == result.StudentId).ToList();
                    foreach (Result resultItem in studentResults)
                    {
                        resultItem.CGPA = result.CGPA;
                        resultItem.Remark = result.Remark;
                        resultItem.GPA = result.GPA;
                        resultItem.UnitOutstanding =
                            studentResults.Where(
                                s => s.Grade == "CD" || s.Grade == "D" || s.Grade == "E" || s.Grade == "F")
                                .Sum(s => s.CourseUnit);
                        resultItem.UnitPassed =
                            studentResults.Where(
                                s => s.Grade == "A" || s.Grade == "AB" || s.Grade == "B" || s.Grade == "BC" || s.Grade == "C")
                                .Sum(s => s.CourseUnit);
                    }

                }

                return results.OrderBy(a => a.MatricNumber).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetPreviousSemesterPassList(Student student, SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                Semester semester = new Semester() { Id = 1 };

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == semester.Id && x.Person_Id == student.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();

                string[] failureGrades = {"CD", "D", "E", "F"};
                foreach (Result result in results)
                {
                    if (failureGrades.Contains(result.Grade))
                    {
                        return new List<Result>();
                    }
                }
                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetFirstYearFirstSemesterResult(List<string> carryOverCourses, Student student, SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                Semester semester = new Semester() { Id = 1 };

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == semester.Id && x.Person_Id == student.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();


                string[] sessionArray = sessionSemester.Session.Name.Split('/');
                int secondIndex = Convert.ToInt32(sessionArray[1]) + 1;
                string currentSession = sessionArray[1] + "/" + secondIndex;

                int referenceCount = 0;
                int reference = 0;
                foreach (Result result in results)
                {
                    if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                    {
                        if (carryOverCourses.Contains(result.CourseCode))
                        {
                            referenceCount += 1;
                            reference = referenceCount;
                        }
                        else
                        {
                            CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                            CourseRegistrationDetail courseRegistrationDetail = courseRegistrationDetailLogic.GetModelBy(crd => crd.Course_Id == result.CourseId && crd.Semester_Id == 1 && crd.STUDENT_COURSE_REGISTRATION.SESSION.Session_Name == currentSession && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id);
                            result.Grade = courseRegistrationDetail.Grade;
                            ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                            ScoreGrade scoreGrade = scoreGradeLogic.GetModelBy(sc => sc.Grade == result.Grade);
                            result.GPCU = scoreGrade.GradePoint * result.CourseUnit;
                        }
                    }
                    if (referenceCount != 0 && reference != null)
                    {
                        result.Remark = reference.ToString();
                    }
                }
                if (results.Count > 0)
                {
                    if (results.LastOrDefault().Remark == null)
                    {
                        results.LastOrDefault().Remark = "PASS";
                    }
                }

                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private List<string> GetSecondYearFirstSemesterCarryOverCourses(SessionSemester sessionSemester, Level lvl, Programme programme, Department department, Student student)
        {
            try
            {
                List<CourseRegistrationDetail> courseRegistrationdetails = new List<CourseRegistrationDetail>();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                List<string> courseCodes = new List<string>();
                if (lvl.Id == 2 || lvl.Id == 4)
                {
                    courseRegistrationdetails = courseRegistrationDetailLogic.GetModelsBy(crd => crd.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id && crd.STUDENT_COURSE_REGISTRATION.Department_Id == department.Id && crd.STUDENT_COURSE_REGISTRATION.Programme_Id == programme.Id && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && (crd.Grade == "CD" || crd.Grade == "D" || crd.Grade == "E" || crd.Grade == "F"));
                    if (sessionSemester.Semester.Id == 1)
                    {
                        courseRegistrationdetails = courseRegistrationdetails.Where(p => p.Semester.Id == 1).ToList();
                        if (courseRegistrationdetails.Count > 0)
                        {
                            foreach (CourseRegistrationDetail courseRegistrationDetail in courseRegistrationdetails)
                            {
                                courseCodes.Add(courseRegistrationDetail.Course.Code);
                            }
                        }
                    }
                }

                return courseCodes;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetPreviousSemesterResult(Student student, SessionSemester sessionSemester, Level level, Programme programme, Department department)
        {
            try
            {
                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();
                SessionSemester ss = sessionSemesterLogic.GetBy(sessionSemester.Id);
                Semester semester = new Semester() { Id = 1 };

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Session_Id == ss.Session.Id && x.Semester_Id == semester.Id && x.Person_Id == student.Id && x.Level_Id == level.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).ToList();


                int referenceCount = 0;
                string reference = null;
                foreach (Result result in results)
                {
                    if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                    {
                        referenceCount += 1;
                        reference = referenceCount.ToString() + "R";
                    }
                    if (referenceCount != 0 && reference != null)
                    {
                        result.Remark = reference;
                    }
                }
                if (results.Count > 0)
                {
                    if (results.LastOrDefault().Remark == null)
                    {
                        results.LastOrDefault().Remark = "PASS";
                    }
                }


                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<Result> GetFirstYearResult(Student student, SessionSemester sessionSemester, Level level, Programme programme, Department department, List<string> carryOverCourses)
        {
            try
            {
                if (sessionSemester == null || sessionSemester.Id <= 0 || level == null || level.Id <= 0 || programme == null || programme.Id <= 0 || department == null || department.Id <= 0)
                {
                    throw new Exception("One or more criteria to get Mater Result Sheet not set! Please check your input criteria selection and try again.");
                }

                string[] sessionArray = sessionSemester.Session.Name.Split('/');
                int firstIndex = Convert.ToInt32(sessionArray[0]) - 1;
                string previousSession = firstIndex + "/" + sessionArray[0];

                SessionLogic sessionLogic = new SessionLogic();
                Session session = sessionLogic.GetModelBy(s => s.Session_Name == previousSession);
                Semester firstSemester = new Semester() { Id = 1 };
                Semester secondSemester = new Semester() { Id = 2 };
                Level previousLevel = new Level();

                if (level.Id == 4)
                {
                    previousLevel = new Level() { Id = 3 };
                }
                else if (level.Id == 2)
                {
                    previousLevel = new Level() { Id = 1 };
                }

                SessionSemesterLogic sessionSemesterLogic = new Business.SessionSemesterLogic();

                List<Result> results = (from sr in repository.GetBy<VW_STUDENT_RESULT_ALT>(x => x.Person_Id == student.Id && x.Session_Id == session.Id && x.Semester_Id == secondSemester.Id && x.Level_Id == previousLevel.Id && x.Programme_Id == programme.Id && x.Department_Id == department.Id)
                                        select new Result
                                        {
                                            StudentId = sr.Person_Id,
                                            Name = sr.Name,
                                            MatricNumber = sr.Matric_Number,
                                            SessionName = sr.Session_Name,
                                            FacultyName = sr.Faculty_Name,
                                            ProgrammeName = sr.Programme_Name,
                                            LevelName = sr.Level_Name,
                                            CourseId = sr.Course_Id,
                                            CourseCode = sr.Course_Code,
                                            CourseName = sr.Course_Name,
                                            CourseUnit = sr.Course_Unit,

                                            TestScore = sr.Test_Score,
                                            ExamScore = sr.Exam_Score,
                                            Score = sr.Total_Score,
                                            Grade = sr.Grade,
                                            GradePoint = sr.Grade_Point,

                                            GPCU = sr.WGP,
                                            TotalSemesterCourseUnit = sr.Total_Semester_Course_Unit,
                                        }).OrderBy(p => p.MatricNumber).ToList();

                SessionSemester prevSessionSemester = sessionSemesterLogic.GetModelBy(ssm => ssm.Session_Id == session.Id && ssm.Semester_Id == firstSemester.Id);
                List<Result> previousSemesterResult = GetFirstYearFirstSemesterResult(carryOverCourses, student, prevSessionSemester, previousLevel, programme, department);

                int referenceCount = 0;
                int reference = 0;
                foreach (Result result in results)
                {
                    if (previousSemesterResult.Count > 0)
                    {
                        result.PreviousSemesterTotalCourseUnit = previousSemesterResult.FirstOrDefault().TotalSemesterCourseUnit;
                        result.PreviousSemesterTotalGPCU = previousSemesterResult.Sum(p => p.GPCU);
                    }
                    else
                    {
                        result.PreviousSemesterTotalCourseUnit = 0;
                        result.PreviousSemesterTotalGPCU = 0;
                    }
                    if (result.Grade == "F" || result.Grade == "CD" || result.Grade == "D" || result.Grade == "E")
                    {
                        if (carryOverCourses.Contains(result.CourseCode))
                        {
                            referenceCount += 1;
                            reference = referenceCount;
                        }
                        else
                        {
                            CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                            CourseRegistrationDetail courseRegistrationDetail = courseRegistrationDetailLogic.GetModelBy(crd => crd.Course_Id == result.CourseId && crd.STUDENT_COURSE_REGISTRATION.Session_Id == sessionSemester.Session.Id && crd.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id);
                            if (courseRegistrationDetail != null)
                            {
                                result.Grade = courseRegistrationDetail.Grade;
                                ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                                ScoreGrade scoreGrade = scoreGradeLogic.GetModelBy(sc => sc.Grade == result.Grade);
                                result.GPCU = scoreGrade.GradePoint * result.CourseUnit;
                            }
                        }
                    }
                    if (referenceCount != 0 && reference != 0 && previousSemesterResult.Count > 0 && previousSemesterResult.LastOrDefault().Remark != "PASS")
                    {
                        result.Remark = (reference + Convert.ToInt32(previousSemesterResult.LastOrDefault().Remark.Substring(0, 1))).ToString();
                    }
                    else if (referenceCount != 0 && reference != 0 && previousSemesterResult.Count > 0 && previousSemesterResult.LastOrDefault().Remark == "PASS")
                    {
                        result.Remark = reference.ToString();
                    }
                    else if (referenceCount == 0 && reference == 0 && previousSemesterResult.Count > 0 && previousSemesterResult.LastOrDefault().Remark != "PASS")
                    {
                        result.Remark = previousSemesterResult.LastOrDefault().Remark.Substring(0, 1).ToString();
                    }
                    else if (referenceCount == 0 && reference == 0 && previousSemesterResult.Count > 0 && previousSemesterResult.LastOrDefault().Remark == "PASS")
                    {
                        result.Remark = "PASS";
                    }
                }

                return results;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public StudentClassListReport GetStudentWithCGPA(STUDENT studentItem)
        {
            StudentClassListReport studentClassListReport = new StudentClassListReport();
            try
            {
                List<Result> report = (from a in repository.GetBy<VW_STUDENT_RESULT_ALT>(s => s.Person_Id == studentItem.Person_Id)
                              select new Result()
                            {
                                Name = a.Name,
                                MatricNumber = a.Matric_Number,
                                GPCU = a.WGP,
                                TotalSemesterCourseUnit = a.Total_Semester_Course_Unit,
                                CourseUnit = a.Course_Unit

                            }).ToList();

                if (report.Count > 0)
                {
                    studentClassListReport.Name = report.FirstOrDefault().Name;
                    studentClassListReport.RegistrationNumber = report.FirstOrDefault().MatricNumber;

                    decimal? totalGPCU = report.Sum(s => s.GPCU);
                    int? totalCourseUnit = report.Sum(s => s.CourseUnit);
                    studentClassListReport.CGPA = totalGPCU / totalCourseUnit;

                    if (studentClassListReport.CGPA > 0)
                    {
                        StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                        StudentAcademicInformation existingStudentAcademicInformation = academicInformationLogic.GetModelBy(s => s.Person_Id == studentItem.Person_Id);
                        if (existingStudentAcademicInformation != null && (existingStudentAcademicInformation.IsModified != true || existingStudentAcademicInformation.CGPA == null))
                        {
                            existingStudentAcademicInformation.CGPA = studentClassListReport.CGPA;
                            academicInformationLogic.Modify(existingStudentAcademicInformation);
                        }
                    }

                    return studentClassListReport;
                }
                
            }
            catch (Exception)
            {
                throw;
            }

            return studentClassListReport;
        }

        public List<SemesterReport> GetSemesterResultInformation(CourseRegistration firstCourseRegistration, Semester firstSemester, Level firstLevel)
        {
            List<SemesterReport> result = new List<SemesterReport>();
            try
            {
                if (firstCourseRegistration != null && firstCourseRegistration.Id > 0)
                {
                    result = (from a in repository.GetBy<VW_STUDENT_RESULT_ALT>(s => s.Person_Id == firstCourseRegistration.Student.Id && s.Session_Id == firstCourseRegistration.Session.Id && s.Semester_Id == firstSemester.Id)
                              select new SemesterReport()
                              {
                                  CourseCode = a.Course_Code,
                                  CourseTitle = a.Course_Name,
                                  CourseUnit = a.Course_Unit,
                                  GradeScore = Convert.ToString(a.Grade_Point),
                                  GradePoint = Convert.ToDecimal(a.Grade_Point),
                                  Grade = a.Grade,
                                  Session = a.Session_Name,
                                  Semester = a.Semester_Name,
                                  Level = a.Level_Name,
                                  GradePoint_CourseUnit = Convert.ToDecimal(a.WGP)
                              }).ToList();
                }
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }
        public List<SemesterReport> GetSemesterResultInformation(CourseRegistration firstCourseRegistration, Semester semester)
        {

            List<SemesterReport> result = new List<SemesterReport>();
            try
            {
               
                if (firstCourseRegistration != null && firstCourseRegistration.Id > 0)
                {
                    result = (from a in repository.GetBy<VW_STUDENT_RESULT_ALT>(s => s.Person_Id == firstCourseRegistration.Student.Id && s.Semester_Id == semester.Id)
                              select new SemesterReport()
                              {
                                  CourseCode = a.Course_Code,
                                  CourseTitle = a.Course_Name,
                                  CourseUnit = a.Course_Unit,
                                  GradeScore = Convert.ToString(a.Grade_Point),
                                  GradePoint = Convert.ToDecimal(a.Grade_Point),
                                  Grade = a.Grade,
                                  Session = a.Session_Name,
                                  Semester = a.Semester_Name,
                                  Level = a.Level_Name,
                                  FirstSemester = a.Session_Id,
                                  SessionId = a.Session_Id,
                                  SessionSemesterId = a.Session_Semester_Id,
                                  GradePoint_CourseUnit = Convert.ToDecimal(a.WGP),
                                 
                              }).Distinct().ToList();
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }
    }

}
