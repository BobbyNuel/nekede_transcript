﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class TranscriptRequestUpdateLogic : BusinessBaseLogic<TranscriptRequestUpdate, TRANSCRIPT_REQUEST_UPDATE>
    {
        public TranscriptRequestUpdateLogic()
        {
            translator = new TranscriptRequestUpdateTranslator();
        }

        public bool Modify(TranscriptRequestUpdate model)
        {
            try
            {
                Expression<Func<TRANSCRIPT_REQUEST_UPDATE, bool>> selector = t => t.Transcript_Request_Update_Id == model.Id;
                TRANSCRIPT_REQUEST_UPDATE entity = GetEntityBy(selector);
                if (entity == null)
                {
                    return false;
                }

                entity.Count = model.Count;
                entity.Last_Request_Id = model.LastRequestId;
                entity.Last_Update_On = model.LastUpdateOn;

                int modifiedRecordCount = Save();
                return modifiedRecordCount > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
