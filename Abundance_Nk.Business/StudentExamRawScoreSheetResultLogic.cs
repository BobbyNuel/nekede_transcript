﻿using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Translator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Business
{
    public class StudentExamRawScoreSheetResultLogic : BusinessBaseLogic<StudentExamRawScoreSheet, STUDENT_EXAM_RAW_SCORE_SHEET_RESULT>
    {
        public StudentExamRawScoreSheetResultLogic()
        {
            translator = new StudentExamRawScoreSheetTranslator();
        }

        public List<ExamRawScoreSheetReport> GetScoreSheetBy(Session session, Semester semester, Course course, Department department, Level level, Programme programme)
        {
            List<ExamRawScoreSheetReport> studentExamRawScoreSheetReportList = new List<ExamRawScoreSheetReport>();
            try
            {
               
                 if (session != null && semester != null && course != null && department != null && level != null && programme != null)
                {
                    if (programme.Id == 1 || programme.Id == 2 || programme.Id == 5)
                    {
                        programme.Name = "NATIONAL DIPLOMA";
                    }
                    else
                    {
                        programme.Name = "HIGHER NATIONAL DIPLOMA";
                    }
                    decimal benchMarkScore = 40M;                
                      var TotalClassCount =
                        from a in
                            repository.GetBy<VW_STUDENT_RESULT_ALT>(
                                a =>
                                    a.Session_Id == session.Id && a.Semester_Id == semester.Id &&
                                    a.Course_Id == course.Id && a.Level_Id == level.Id && a.Programme_Id == programme.Id )
                        select new ExamRawScoreSheetReport{};
                   
                    var passedStudents =
                        from a in
                            repository.GetBy<VW_STUDENT_RESULT_ALT>(
                                a =>
                                    a.Session_Id == session.Id && a.Semester_Id == semester.Id &&
                                    a.Course_Id == course.Id && a.Level_Id == level.Id && a.Programme_Id == programme.Id && a.Total_Score >= benchMarkScore)
                        select new ExamRawScoreSheetReport{};

                    double PassStudentCount = Convert.ToDouble(passedStudents.Count());
                    double StudentCount = Convert.ToDouble(TotalClassCount.Count());
                    double successRate = (PassStudentCount / StudentCount) * (100);

                    var studentExamRawScoreSheetDetails = from a in repository.GetBy<VW_STUDENT_RESULT_ALT>(a => a.Session_Id == session.Id && a.Semester_Id == semester.Id && a.Course_Id == course.Id && a.Level_Id == level.Id && a.Programme_Id == programme.Id)
                                                                           select new ExamRawScoreSheetReport
                                                                           {
                                                                               CourseCode = a.Course_Code,
                                                                               CourseTitle = a.Course_Name,
                                                                               CourseUnit = a.Course_Unit,
                                                                               Department = a.Department_Name,
                                                                               Programme = programme.Name,
                                                                               MatricNumber = a.Matric_Number,
                                                                               Date = DateTime.Now.Month + "/" + DateTime.Now.Day + "/" + DateTime.Now.Year,
                                                                               //Identifier = a.Department_Code+a.Level_Name+ GetSemesterCodeBy(a.Semester_Id) +GetSessionCodeBy(a.Session_Name),
                                                                               Session = a.Session_Name,
                                                                               Semester = a.Semester_Name,
                                                                               Faculty = a.Faculty_Name,
                                                                               //Remark = a.Remark,
                                                                               SuccessRate = successRate,
                                                                               Level = a.Level_Name,
                                                                               //StaffName = a.User_Name,
                                                                               Fullname = a.Name,
                                                                               T_EX = Convert.ToDouble(a.Exam_Score),
                                                                               T_CA = Convert.ToDouble(a.Test_Score),
                                                                               Grade = a.Grade,
                                                                               EX_CA = Convert.ToDouble(a.Total_Score),
                                                                               Practical = Convert.ToDouble(a.Practical_Score)
                                                                           };
                    List<ExamRawScoreSheetReport> results = studentExamRawScoreSheetDetails.ToList();
                    for (int i = 0; i < results.Count; i++)
                    {
                        double totalScore = results[i].T_CA + results[i].T_EX + results[i].Practical;
                        results[i].Grade = GetGrade(totalScore);
                        results[i].EX_CA = totalScore;
                        results[i].Remark = GetRemark(results[i].T_EX, results[i].T_CA);
                        results[i].NumberOfA = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 79 && (r.T_CA + r.T_EX + r.Practical) <= 100);
                        results[i].NumberOfAB = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 69 && (r.T_CA + r.T_EX + r.Practical) <= 79);
                        results[i].NumberOfB = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 59 && (r.T_CA + r.T_EX + r.Practical) <= 69);
                        results[i].NumberOfBC = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 49 && (r.T_CA + r.T_EX + r.Practical) <= 59);
                        results[i].NumberOfC = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 39 && (r.T_CA + r.T_EX + r.Practical) <= 49);
                        results[i].NumberOfCD = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 29 && (r.T_CA + r.T_EX + r.Practical) <= 39);
                        results[i].NumberOfD = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 19 && (r.T_CA + r.T_EX + r.Practical) <= 29);
                        results[i].NumberOfE = results.Count(r => (r.T_CA + r.T_EX + r.Practical) > 9 && (r.T_CA + r.T_EX + r.Practical) <= 19);
                        results[i].NumberOfF = results.Count(r => (r.T_CA + r.T_EX + r.Practical) <= 9);
                    }

                    studentExamRawScoreSheetReportList = results.OrderBy(a => a.MatricNumber).ToList();
                 
                }     
            }
            catch (Exception)
            {     
                throw;
            }

            return studentExamRawScoreSheetReportList;
        }

        private string GetGrade(double totalScore)
        {
            string grade = "";
            try
            {
                if (totalScore <= 9)
                {
                    grade = "F";
                }
                if (totalScore > 9 && totalScore <= 19)
                {
                    grade = "E";
                }
                if (totalScore > 19 && totalScore <= 29)
                {
                    grade = "D";
                }
                if (totalScore > 29 && totalScore <= 39)
                {
                    grade = "CD";
                }
                if (totalScore > 39 && totalScore <= 49)
                {
                    grade = "C";
                }
                if (totalScore > 49 && totalScore <= 59)
                {
                    grade = "BC";
                }
                if (totalScore > 59 && totalScore <= 69)
                {
                    grade = "B";
                }
                if (totalScore > 69 && totalScore <= 79)
                {
                    grade = "AB";
                }
                if (totalScore > 79 && totalScore <= 100)
                {
                    grade = "A";
                }
            }
            catch (Exception)
            {   
                throw;
            }

            return grade;
        }

        private string GetIdentifierBy(STUDENT_EXAM_RAW_SCORE_SHEET_RESULT rawscoresheetItem)
        {
            try
            {
                string identifier = null;
                string departmentCode = rawscoresheetItem.COURSE.DEPARTMENT.Department_Code;
                string level = rawscoresheetItem.LEVEL.Level_Name;
                string semesterCode = GetSemesterCodeBy(rawscoresheetItem.Semester_Id);
                string sessionCode = GetSessionCodeBy(rawscoresheetItem.SESSION.Session_Name);
                identifier = departmentCode + level + semesterCode + sessionCode;
                return identifier;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetSessionCodeBy(string sessionName)
        {
            try
            {
                string sessionCode = null;
                string[] sessionArray = sessionName.Split('/');
                string sessionYear = sessionArray[1];
                sessionCode = sessionYear.Substring(2, 2);
                return sessionCode;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetSemesterCodeBy(int semesterId)
        {
            try
            {
                if (semesterId == 1)
                {
                    return "F";
                }
                else
                {
                    return "S";
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetLevelBy(int levelId, int programmeId)
        {
            string level = null;
            string progType = null;
            try
            {
                if (levelId == 1)
                {
                    level = "100 LEVEL";
                    progType = GetProgramme(programmeId);
                    level = level + " " + progType;
                }
                else if (levelId == 2)
                {
                    level = "200 LEVEL";
                    progType = GetProgramme(programmeId);
                    level = level + " " + progType;
                }
                else if (levelId == 3)
                {
                    level = "300 LEVEL";
                    progType = GetProgramme(programmeId);
                    level = level + " " + progType;
                }
                else
                {
                    level = "400 LEVEL";
                    progType = GetProgramme(programmeId);
                    level = level + " " + progType;
                }
                return level;
              
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetProgramme(int programmeId)
        {
            try
            {
                if (programmeId == 1 || programmeId == 3)
                {
                    return "[FULL TIME]";
                }
                else
                {
                    return "[PART TIME]";
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private string GetRemark(double? totalExam, double? totalTest)
        {
            string remark = null;
            try
            {
                //if (totalExam == 101)
                //{
                //    remark = "SICK: EXAM";
                //    return remark;
                //}
                //else if (totalTest == 101)
                //{
                //    remark = "SICK: TEST";
                //    return remark;
                //}
                double? total = totalExam + totalTest;

                if (total >= 40.0 && total <= 100.0)
                {
                    remark = "PASS";
                }
                if (total >= 0.0 && total <= 39.0)
                {
                    remark = "FAIL";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return remark;
        }

        public bool Modify(StudentExamRawScoreSheet studentExamRawScoreSheet)
        {
            try
            {
                if (studentExamRawScoreSheet != null)
                {
                    Expression<Func<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT, bool>> selector = p => p.Student_Result_Id == studentExamRawScoreSheet.Id;
                    STUDENT_EXAM_RAW_SCORE_SHEET_RESULT entity = GetEntityBy(selector);
                    entity.EX_CA = (double)studentExamRawScoreSheet.EX_CA;
                    entity.T_CA = (double)studentExamRawScoreSheet.T_CA;
                    entity.T_EX = (double)studentExamRawScoreSheet.T_EX;
                    entity.QU1 = (double)studentExamRawScoreSheet.QU1;
                    entity.QU2 = (double)studentExamRawScoreSheet.QU2;
                    entity.QU3 = (double)studentExamRawScoreSheet.QU3;
                    entity.QU4 = (double)studentExamRawScoreSheet.QU4;
                    entity.QU5 = (double)studentExamRawScoreSheet.QU5;
                    entity.QU6 = (double)studentExamRawScoreSheet.QU6;
                    entity.QU7 = (double)studentExamRawScoreSheet.QU7;
                    entity.QU8 = (double)studentExamRawScoreSheet.QU8;
                    entity.QU9 = (double)studentExamRawScoreSheet.QU9;
                    entity.Special_Case = studentExamRawScoreSheet.Special_Case;
                    entity.Remark = studentExamRawScoreSheet.Special_Case;
                    entity.Uploader_Id = studentExamRawScoreSheet.Uploader.Id;
                    int modifyCount = Save();
                   
                }
            }
            catch (Exception)
            {

                throw;
            }
            return true;
        }
    }
}
