﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class PreviousEducationLogic : BusinessBaseLogic<PreviousEducation, APPLICANT_PREVIOUS_EDUCATION>
    {
        public PreviousEducationLogic()
        {
            translator = new PreviousEducationTranslator();
        }

        public bool Modify(PreviousEducation previousEducation)
        {
            try
            {
                Expression<Func<APPLICANT_PREVIOUS_EDUCATION, bool>> selector = p => p.Applicant_Previous_Education_Id == previousEducation.Id;
                APPLICANT_PREVIOUS_EDUCATION entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Person_Id = previousEducation.Person.Id;
                entity.Previous_School_Name = previousEducation.SchoolName;
                entity.Previous_Course = previousEducation.Course;
                entity.Previous_Education_Start_Date = previousEducation.StartDate;
                entity.Previous_Education_End_Date = previousEducation.EndDate;
                entity.Educational_Qualification_Id = previousEducation.Qualification.Id;
                entity.Result_Grade_Id = previousEducation.ResultGrade.Id;
                entity.IT_Duration_Id = previousEducation.ITDuration.Id;
                entity.Application_Form_Id = previousEducation.ApplicationForm.Id;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }




    }

}
