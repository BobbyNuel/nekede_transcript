﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class PersonGuardianLogic : BusinessBaseLogic<PersonGuardian, PERSON_GUARDIAN>
    {
        public PersonGuardianLogic()
        {
            translator = new PersonGuardianTranslator();
        }

        public bool Modify(PersonGuardian personGuardian)
        {
            try
            {
                PERSON_GUARDIAN entity = GetEntityBy(s => s.Person_Guardian_Id == personGuardian.Id);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Guardian_Name = personGuardian.GuardianName;
                entity.Address = personGuardian.Address;
                entity.Occupation = personGuardian.Occupation;
                entity.Email = personGuardian.Email;
                entity.Mobile_Phone = personGuardian.MobilePhone;

                if (personGuardian.Person != null && personGuardian.Person.Id > 0)
                {
                    entity.Person_Id = personGuardian.Person.Id;
                }

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
