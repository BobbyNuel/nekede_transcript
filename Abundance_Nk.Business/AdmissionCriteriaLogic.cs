﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class AdmissionCriteriaLogic : BusinessBaseLogic<AdmissionCriteria, ADMISSION_CRITERIA>
    {
        private OLevelResultLogic oLevelResultLogic;
        private OLevelResultDetailLogic oLevelResultDetailLogic;
        private AdmissionCriteriaForOLevelSubjectLogic admissionCriteriaForOLevelSubjectLogic;
        private AdmissionCriteriaForOLevelTypeLogic admissionCriteriaForOLevelTypeLogic;
        private AdmissionCriteriaForOLevelSubjectAlternativeLogic admissionCriteriaForOLevelSubjectAlternativeLogic;

        public AdmissionCriteriaLogic()
        {
            translator = new AdmissionCriteriaTranslator();

            admissionCriteriaForOLevelTypeLogic = new AdmissionCriteriaForOLevelTypeLogic();
            admissionCriteriaForOLevelSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
            admissionCriteriaForOLevelSubjectAlternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();
            oLevelResultDetailLogic = new OLevelResultDetailLogic();
            oLevelResultLogic = new OLevelResultLogic();
        }


        public string ClearStudent(AppliedCourse applicantAppliedCourse)
        {
            try
            {
                Person applicant = applicantAppliedCourse.Person;
                Programme programme = applicantAppliedCourse.Programme;
                Department department = applicantAppliedCourse.Department;

                //ADMISSION CRITERIA
                //get admission criteria
                Expression<Func<ADMISSION_CRITERIA, bool>> selector = ac => ac.Department_Id == department.Id;
                AdmissionCriteria admissionCriteria = GetModelBy(selector);

                if (admissionCriteria == null || admissionCriteria.Id <= 0)
                {
                    throw new Exception("No Admission Criteria found for " + programme.Name + " in " + department.Name);
                }

                //get admission criteria olevel type
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_TYPE, bool>> otSelector = ac => ac.Admission_Criteria_Id == admissionCriteria.Id;
                List<AdmissionCriteriaForOLevelType> requiredOlevelTypes ;//admissionCriteriaForOLevelTypeLogic.GetModelsBy(otSelector);

                //get admission criteria olevel subject
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT, bool>> scSelector = s => s.Admission_Criteria_Id == admissionCriteria.Id && s.Is_Compulsory == true;
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT, bool>> osSelector = s => s.Admission_Criteria_Id == admissionCriteria.Id && s.Is_Compulsory == false;
                List<AdmissionCriteriaForOLevelSubject> compulsorySubjects = admissionCriteriaForOLevelSubjectLogic.GetModelsBy(scSelector);
                List<AdmissionCriteriaForOLevelSubject> otherSubjects = admissionCriteriaForOLevelSubjectLogic.GetModelsBy(osSelector);



                //APPLICANT
                //get applicant result
                Expression<Func<APPLICANT_O_LEVEL_RESULT, bool>> firstSittingOlevelSelector = f => f.Person_Id == applicant.Id && f.O_Level_Exam_Sitting_Id == 1;
                Expression<Func<APPLICANT_O_LEVEL_RESULT, bool>> secondSittingOlevelSelector = s => s.Person_Id == applicant.Id && s.O_Level_Exam_Sitting_Id == 2;
                OLevelResult firstSittingOlevelResult = oLevelResultLogic.GetModelBy(firstSittingOlevelSelector);
                OLevelResult secondSittingOlevelResult = oLevelResultLogic.GetModelBy(secondSittingOlevelSelector);


                //get applicant olevel subjects
                List<OLevelResultDetail> firstSittingResultDetails = null;
                List<OLevelResultDetail> secondSittingResultDetails = null;
                if (firstSittingOlevelResult != null && firstSittingOlevelResult.Id > 0)
                {
                    Expression<Func<APPLICANT_O_LEVEL_RESULT_DETAIL, bool>> firstSittingSubjectSelector = fs => fs.Applicant_O_Level_Result_Id == firstSittingOlevelResult.Id;
                    firstSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(firstSittingSubjectSelector);
                }

                if (secondSittingOlevelResult != null && secondSittingOlevelResult.Id > 0)
                {
                    Expression<Func<APPLICANT_O_LEVEL_RESULT_DETAIL, bool>> secondSittingSubjectSelector = ss => ss.Applicant_O_Level_Result_Id == secondSittingOlevelResult.Id;
                    secondSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(secondSittingSubjectSelector);
                }

                if ((firstSittingResultDetails == null || firstSittingResultDetails.Count <= 0) && (secondSittingResultDetails == null || secondSittingResultDetails.Count <= 0))
                {
                    return "No O-Level result found for applicant!";
                }

                string oLevelTypeRejectReason = "";// CheckOlevelType(requiredOlevelTypes, firstSittingOlevelResult, secondSittingOlevelResult, applicantAppliedCourse);
                string requiredSubjectsRejectReason = CheckRequiredOlevelSubjects(compulsorySubjects, otherSubjects, firstSittingResultDetails, secondSittingResultDetails, applicantAppliedCourse);



                string rejectReason = null;
                if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = oLevelTypeRejectReason + ". " + requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason == null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason == null)
                {
                    rejectReason = oLevelTypeRejectReason;
                }

                if (!string.IsNullOrEmpty(rejectReason))
                {
                    rejectReason += " Therefore did not qualify for admission.";
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string EvaluateApplication(AppliedCourse applicantAppliedCourse)
        {
            try
            {
                Person applicant = applicantAppliedCourse.Person;
                Programme programme = applicantAppliedCourse.Programme;
                Department department = applicantAppliedCourse.Department;

                //ADMISSION CRITERIA
                //get admission criteria
                Expression<Func<ADMISSION_CRITERIA, bool>> selector = ac => ac.Department_Id == department.Id && ac.Programme_Id == programme.Id;
                AdmissionCriteria admissionCriteria = GetModelBy(selector);

                if (admissionCriteria == null || admissionCriteria.Id <= 0)
                {
                    throw new Exception("No Admission Criteria found for " + programme.Name + " in " + department.Name);
                }

                //get admission criteria olevel type
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_TYPE, bool>> otSelector = ac => ac.Admission_Criteria_Id == admissionCriteria.Id;
                List<AdmissionCriteriaForOLevelType> requiredOlevelTypes = admissionCriteriaForOLevelTypeLogic.GetModelsBy(otSelector);

                //get admission criteria olevel subject
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT, bool>> scSelector = s => s.Admission_Criteria_Id == admissionCriteria.Id && s.Is_Compulsory == true;
                Expression<Func<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT, bool>> osSelector = s => s.Admission_Criteria_Id == admissionCriteria.Id && s.Is_Compulsory == false;
                List<AdmissionCriteriaForOLevelSubject> compulsorySubjects = admissionCriteriaForOLevelSubjectLogic.GetModelsBy(scSelector);
                List<AdmissionCriteriaForOLevelSubject> otherSubjects = admissionCriteriaForOLevelSubjectLogic.GetModelsBy(osSelector);



                //APPLICANT
                //get applicant result
                Expression<Func<APPLICANT_O_LEVEL_RESULT, bool>> firstSittingOlevelSelector = f => f.Person_Id == applicant.Id && f.O_Level_Exam_Sitting_Id == 1;
                Expression<Func<APPLICANT_O_LEVEL_RESULT, bool>> secondSittingOlevelSelector = s => s.Person_Id == applicant.Id && s.O_Level_Exam_Sitting_Id == 2;
                OLevelResult firstSittingOlevelResult = oLevelResultLogic.GetModelBy(firstSittingOlevelSelector);
                OLevelResult secondSittingOlevelResult = oLevelResultLogic.GetModelBy(secondSittingOlevelSelector);
                

                //get applicant olevel subjects
                List<OLevelResultDetail> firstSittingResultDetails = null;
                List<OLevelResultDetail> secondSittingResultDetails = null;
                if (firstSittingOlevelResult != null && firstSittingOlevelResult.Id > 0)
                {
                    Expression<Func<APPLICANT_O_LEVEL_RESULT_DETAIL, bool>> firstSittingSubjectSelector = fs => fs.Applicant_O_Level_Result_Id == firstSittingOlevelResult.Id;
                    firstSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(firstSittingSubjectSelector);
                }

                if (secondSittingOlevelResult != null && secondSittingOlevelResult.Id > 0)
                {
                    Expression<Func<APPLICANT_O_LEVEL_RESULT_DETAIL, bool>> secondSittingSubjectSelector = ss => ss.Applicant_O_Level_Result_Id == secondSittingOlevelResult.Id;
                    secondSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(secondSittingSubjectSelector);
                }

                if ((firstSittingResultDetails == null || firstSittingResultDetails.Count <= 0) && (secondSittingResultDetails == null || secondSittingResultDetails.Count <= 0))
                {
                    return "No O-Level result found for applicant!";
                }

                string oLevelTypeRejectReason = CheckOlevelType(requiredOlevelTypes, firstSittingOlevelResult, secondSittingOlevelResult, applicantAppliedCourse);
                string requiredSubjectsRejectReason = CheckRequiredOlevelSubjects(compulsorySubjects, otherSubjects, firstSittingResultDetails, secondSittingResultDetails, applicantAppliedCourse);



                string rejectReason = null;
                if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = oLevelTypeRejectReason + ". " + requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason == null && requiredSubjectsRejectReason != null)
                {
                    rejectReason = requiredSubjectsRejectReason;
                }
                else if (oLevelTypeRejectReason != null && requiredSubjectsRejectReason == null)
                {
                    rejectReason = oLevelTypeRejectReason;
                }

                if (!string.IsNullOrEmpty(rejectReason))
                {
                    rejectReason += " Therefore did not qualify for admission.";
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<OLevelResultDetail> GetConsolidatedApplicantOlevelResultAboveOrEqualToC6(List<OLevelResultDetail> firstSittingResultDetails, List<OLevelResultDetail> secondSittingResultDetails)
        {
            try
            {
                List<OLevelResultDetail> firstSittingOLevelResultsAboveOrEqualToC6 = GetOLevelResultsAboveOrEqualToC6(firstSittingResultDetails);
                List<OLevelResultDetail> secondSittingOLevelResultsAboveOrEqualToC6 = GetOLevelResultsAboveOrEqualToC6(secondSittingResultDetails);

                List<OLevelResultDetail> consolidatedOLevelResultsAboveOrEqualToC6 = new List<OLevelResultDetail>();
                if (firstSittingOLevelResultsAboveOrEqualToC6 != null && firstSittingOLevelResultsAboveOrEqualToC6.Count > 0)
                {
                    if (secondSittingOLevelResultsAboveOrEqualToC6 != null && secondSittingOLevelResultsAboveOrEqualToC6.Count > 0)
                    {
                        foreach (OLevelResultDetail oLevelResultDetail in firstSittingOLevelResultsAboveOrEqualToC6)
                        {
                            OLevelResultDetail oLevelResultDetailMatch = secondSittingOLevelResultsAboveOrEqualToC6.Where(ss => ss.Subject.Id == oLevelResultDetail.Subject.Id).SingleOrDefault();
                            if (oLevelResultDetailMatch != null)
                            {
                                if (oLevelResultDetailMatch.Grade.Id > oLevelResultDetail.Grade.Id)
                                {
                                    consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                                }
                                else
                                {
                                    consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetailMatch);
                                }
                            }
                            else
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }

                        foreach (OLevelResultDetail oLevelResultDetail in secondSittingOLevelResultsAboveOrEqualToC6)
                        {
                            OLevelResultDetail oLevelResultDetailMatch = firstSittingOLevelResultsAboveOrEqualToC6.Where(ss => ss.Subject.Id == oLevelResultDetail.Subject.Id).SingleOrDefault();
                            if (oLevelResultDetailMatch == null)
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }
                    }
                    else
                    {
                        foreach (OLevelResultDetail oLevelResultDetail in firstSittingOLevelResultsAboveOrEqualToC6)
                        {
                            if (oLevelResultDetail.Grade.Id <= 6)
                            {
                                consolidatedOLevelResultsAboveOrEqualToC6.Add(oLevelResultDetail);
                            }
                        }
                    }
                }
                
                return consolidatedOLevelResultsAboveOrEqualToC6;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<OLevelResultDetail> GetOLevelResultsAboveOrEqualToC6(List<OLevelResultDetail> resultDetails)
        {
            try
            {
                List<OLevelResultDetail> oLevelResultsAboveOrEqualToC6 = new List<OLevelResultDetail>();
                if (resultDetails != null && resultDetails.Count > 0)
                {
                    foreach (OLevelResultDetail firstSittingResultDetail in resultDetails)
                    {
                        if (firstSittingResultDetail.Grade.Id <= 6)
                        {
                            oLevelResultsAboveOrEqualToC6.Add(firstSittingResultDetail);
                        }
                    }
                }

                return oLevelResultsAboveOrEqualToC6;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string CheckRequiredOlevelSubjects(List<AdmissionCriteriaForOLevelSubject> compulsorySubjects, List<AdmissionCriteriaForOLevelSubject> otherSubjects, List<OLevelResultDetail> firstSittingResultDetails, List<OLevelResultDetail> secondSittingResultDetails, AppliedCourse applicantAppliedCourse)
        {
            try
            {
                //check for the compulsory subjects
                if ((compulsorySubjects == null || compulsorySubjects.Count <= 0) && (otherSubjects == null || otherSubjects.Count <= 0))
                {
                    throw new Exception("Subject has not been set in Admission Criteria for " + applicantAppliedCourse.Programme.Name.ToUpper() + " in " + applicantAppliedCourse.Department.Name + "! Please contact you system administrator");
                }


                int minimumNoOfSubjectWithCreditOrAbove = compulsorySubjects[0].MainCriteria.MinimumRequiredNumberOfSubject;

                List<OLevelResultDetail> applicantConsolidatedResultsWithCreditOrAbove = GetConsolidatedApplicantOlevelResultAboveOrEqualToC6(firstSittingResultDetails, secondSittingResultDetails);
                if (applicantConsolidatedResultsWithCreditOrAbove == null || applicantConsolidatedResultsWithCreditOrAbove.Count <= 0)
                {
                    return "Required minimum number of subjects at Credit level for " + applicantAppliedCourse.Department.Name.ToUpper() + " is " + minimumNoOfSubjectWithCreditOrAbove + " and the applicant got none, ";
                }
                else if (applicantConsolidatedResultsWithCreditOrAbove.Count < minimumNoOfSubjectWithCreditOrAbove)
                {
                    return "Required minimum number of subjects at Credit level for " + applicantAppliedCourse.Department.Name.ToUpper() + " is " + minimumNoOfSubjectWithCreditOrAbove + " the applicant got only " + applicantConsolidatedResultsWithCreditOrAbove.Count + ".";
                }

                string rejectReason = null;
               
                
                //check for main compulsory subjects
                int compulsorySubjectMatchCount = 0;
                List<string> requiredSubjects = new List<string>();
                foreach (AdmissionCriteriaForOLevelSubject compulsoryOLevelSubject in compulsorySubjects)
                {
                    string alternativeSubjects = null;
                    List<OLevelResultDetail> oLevelResultDetailAlternatives = new List<OLevelResultDetail>();
                    OLevelResultDetail oLevelResultDetail = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.Subject.Id == compulsoryOLevelSubject.Subject.Id).SingleOrDefault();

                    List<AdmissionCriteriaForOLevelSubjectAlternative> compulsoryOLevelSubjectAlternatives = admissionCriteriaForOLevelSubjectAlternativeLogic.GetModelsBy(m => m.Admission_Criteria_For_O_Level_Subject_Id == compulsoryOLevelSubject.Id).ToList();

                    
                    if (compulsoryOLevelSubjectAlternatives != null && compulsoryOLevelSubjectAlternatives.Count > 0)
                    {
                        foreach (AdmissionCriteriaForOLevelSubjectAlternative compulsoryOLevelSubjectAlternative in compulsoryOLevelSubjectAlternatives)
                        {
                            OLevelResultDetail oLevelResultDetailAlternative = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.Subject.Id == compulsoryOLevelSubjectAlternative.OLevelSubject.Id).SingleOrDefault();
                            if (oLevelResultDetailAlternative != null)
                            {
                                oLevelResultDetailAlternatives.Add(oLevelResultDetailAlternative);
                            }
                        }
                    }
                                      
                    if (oLevelResultDetail == null && oLevelResultDetailAlternatives.Count <= 0)
                    {
                        //get all compulsory alternative subjects
                        if (compulsoryOLevelSubjectAlternatives != null && compulsoryOLevelSubjectAlternatives.Count > 0)
                        {
                            foreach (AdmissionCriteriaForOLevelSubjectAlternative alternative in compulsoryOLevelSubjectAlternatives)
                            {
                                alternativeSubjects += "/" + alternative.OLevelSubject.Name;
                            }
                        }

                        if (string.IsNullOrEmpty(alternativeSubjects))
                        {
                            requiredSubjects.Add(compulsoryOLevelSubject.Subject.Name.ToUpper());
                            //rejectReason += compulsoryOLevelSubject.Subject.Name.ToUpper() + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but was not obtained by the applicant. Therefore did not qualify for admission.";
                        }
                        else
                        {
                            requiredSubjects.Add(" " + compulsoryOLevelSubject.Subject.Name.ToUpper() + alternativeSubjects.ToUpper());
                            //rejectReason += "Any one of " + compulsoryOLevelSubject.Subject.Name.ToUpper() + alternativeSubjects.ToUpper() + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but none of them was obtained by the applicant. Therefore did not qualify for admission.";
                        }
                    }
                    else
                    {
                        compulsorySubjectMatchCount++;
                    }
                }

                if (requiredSubjects.Count > 0)
                {
                    string[] sbjectsNotGot = requiredSubjects.ToArray();
                    string missingSubjects = string.Join(", ", sbjectsNotGot);
                    rejectReason += missingSubjects + " is a compulsory requirement for " + applicantAppliedCourse.Department.Name.ToUpper() + ", but was not obtained by the applicant.";
                }


                //
                int orderSubjectMatchCount = 0;
                //List<string> otherNeededSubjects = new List<string>();
                foreach (AdmissionCriteriaForOLevelSubject otherSubject in otherSubjects)
                {
                    //string alternativeSubjects = null;
                    List<OLevelResultDetail> oLevelResultDetailAlternatives = new List<OLevelResultDetail>();
                    OLevelResultDetail oLevelResultDetail = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.Subject.Id == otherSubject.Subject.Id).SingleOrDefault();
                    
                    List<AdmissionCriteriaForOLevelSubjectAlternative> otherSubjectAlternatives = admissionCriteriaForOLevelSubjectAlternativeLogic.GetModelsBy(m => m.Admission_Criteria_For_O_Level_Subject_Id == otherSubject.Id).ToList();
                    if (otherSubjectAlternatives != null && otherSubjectAlternatives.Count > 0)
                    {
                        foreach (AdmissionCriteriaForOLevelSubjectAlternative otherSubjectAlternative in otherSubjectAlternatives)
                        {
                            OLevelResultDetail oLevelResultDetailAlternative = applicantConsolidatedResultsWithCreditOrAbove.Where(cs => cs.Subject.Id == otherSubjectAlternative.OLevelSubject.Id).SingleOrDefault();
                            if (oLevelResultDetailAlternative != null)
                            {
                                //alternativeSubjects += "/" + oLevelResultDetailAlternative.Subject.Name;
                                oLevelResultDetailAlternatives.Add(oLevelResultDetailAlternative);
                            }
                        }
                    }

                    if (oLevelResultDetail != null || oLevelResultDetailAlternatives.Count > 0)
                    {
                        orderSubjectMatchCount++;
                    }
                }

                int totalSubjectsObtained = orderSubjectMatchCount + compulsorySubjectMatchCount;
                int otherSubjectCount = minimumNoOfSubjectWithCreditOrAbove - compulsorySubjects.Count;
                if (otherSubjectCount > orderSubjectMatchCount)
                {
                    if (orderSubjectMatchCount > 0)
                    {
                        rejectReason += "Applicant made a total of " + compulsorySubjectMatchCount + " compulsory subjects, and " + orderSubjectMatchCount + " other subject, which totals " + totalSubjectsObtained + " and does not meet the minimum requirement of " + minimumNoOfSubjectWithCreditOrAbove + " subjects for " + applicantAppliedCourse.Department.Name.ToUpper() + ".";
                    }
                    else
                    {
                        List<string> anyOfTheseSubjects = new List<string>();
                        foreach (AdmissionCriteriaForOLevelSubject otherOLevelSubject in otherSubjects)
                        {
                            anyOfTheseSubjects.Add(otherOLevelSubject.Subject.Name.ToUpper());
                        }
                                                
                        string[] anyOne = anyOfTheseSubjects.ToArray();
                        int totalSubjectsOutstanding = minimumNoOfSubjectWithCreditOrAbove - compulsorySubjects.Count;
                        string subjectsOutstanding = totalSubjectsOutstanding > 1 ? totalSubjectsOutstanding + " subjects" : totalSubjectsOutstanding + " subject";
                        rejectReason += "Applicant made a total of " + compulsorySubjectMatchCount + " compulsory subjects, but did not obtain any other " + subjectsOutstanding + " from (" + string.Join(", ", anyOne) + ") at credit level to complement. Which does not meet the minimum requirement of " + minimumNoOfSubjectWithCreditOrAbove + " subjects for " + applicantAppliedCourse.Department.Name.ToUpper() + ".";
                    }
                }
                                
                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        private string CheckOlevelType(List<AdmissionCriteriaForOLevelType> requiredOlevelTypes, OLevelResult firstSittingOlevelResult, OLevelResult secondSittingOlevelResult, AppliedCourse applicantAppliedCourse)
        {
            try
            {
                string rejectReason = null;
                if (requiredOlevelTypes == null || requiredOlevelTypes.Count <= 0)
                {
                    throw new Exception("O-Level Type has not been set in Adission Criteria for " +  applicantAppliedCourse.Programme.Name + " in " + applicantAppliedCourse.Department.Name + "! Please contact your system administrator.");
                }

                string applicantOlevelTypes = null;
                if (firstSittingOlevelResult != null && firstSittingOlevelResult.Id > 0)
                {
                    applicantOlevelTypes += firstSittingOlevelResult.Type.Name;

                    AdmissionCriteriaForOLevelType secondSittingAdmissionCriteriaOLevelType = null;
                    AdmissionCriteriaForOLevelType firstSittingAdmissionCriteriaOLevelType = requiredOlevelTypes.Where(ac => ac.OLevelType.Id == firstSittingOlevelResult.Type.Id).SingleOrDefault();
                    if (firstSittingAdmissionCriteriaOLevelType == null)
                    {
                        if (secondSittingOlevelResult != null && secondSittingOlevelResult.Id > 0)
                        {
                            applicantOlevelTypes += " & " + secondSittingOlevelResult.Type.Name;
                            secondSittingAdmissionCriteriaOLevelType = requiredOlevelTypes.Where(ac => ac.OLevelType.Id == secondSittingOlevelResult.Type.Id).SingleOrDefault();
                        }
                    }

                    if (firstSittingAdmissionCriteriaOLevelType == null && secondSittingAdmissionCriteriaOLevelType == null)
                    {
                        string validOlevelTypes = GetStringOfValidOlevelTypes(requiredOlevelTypes);

                        //int totalRequiredOLevelTypeCount = GetDistinctOLevelTypeCountFrom(requiredOlevelTypes);
                        //if (totalRequiredOLevelTypeCount == 1)
                        //{
                        rejectReason = "'" + applicantOlevelTypes + "' O-Level Type obtained by the applicant does not meet the O-Level Type (" + validOlevelTypes + ") requirement for " + applicantAppliedCourse.Department.Name.ToUpper();
                        //}
                        //else
                        //{
                        //    rejectReason = "'" + applicantOlevelTypes + "' O-Level Type. But required Types are " + validOlevelTypes;
                        //}
                    }
                }

                return rejectReason;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetStringOfValidOlevelTypes(List<AdmissionCriteriaForOLevelType> admissionCriteriaOLevelTypes)
        {
            try
            {
                string validOlevelTypes = null;
                if (admissionCriteriaOLevelTypes != null && admissionCriteriaOLevelTypes.Count > 0)
                {
                    string[] oLevelTypes = GetDistinctOLevelTypesFrom(admissionCriteriaOLevelTypes);
                    validOlevelTypes = string.Join(", ", oLevelTypes);


                    //for (int i = 0; i < admissionCriteriaOLevelTypes.Count; i++)
                    //{
                    //    if (admissionCriteriaOLevelTypes.Count == i - 1)
                    //    {
                    //        validOlevelTypes += admissionCriteriaOLevelTypes[i].OLevelType.ShortName;
                    //    }
                    //    else
                    //    {
                    //        validOlevelTypes += admissionCriteriaOLevelTypes[i].OLevelType.ShortName + ", ";
                    //    }
                    //}
                }

                return validOlevelTypes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string[] GetDistinctOLevelTypesFrom(List<AdmissionCriteriaForOLevelType> admissionCriteriaOLevelTypes)
        {
            try
            {
                string[] oLevelTypes = null;
                if (admissionCriteriaOLevelTypes != null && admissionCriteriaOLevelTypes.Count > 0)
                {
                    oLevelTypes = admissionCriteriaOLevelTypes.Select(o => o.OLevelType.ShortName).Distinct().ToArray();
                }

                return oLevelTypes;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int GetDistinctOLevelTypeCountFrom(List<AdmissionCriteriaForOLevelType> admissionCriteriaOLevelTypes)
        {
            try
            {
                string[] oLevelTypes = GetDistinctOLevelTypesFrom(admissionCriteriaOLevelTypes);
                if (oLevelTypes != null && oLevelTypes.Length > 0)
                {
                    return oLevelTypes.Length;
                }
                else
                {
                    return 0;
                }

            }
            catch (Exception)
            {
                throw;
            }
        }






    }

}
