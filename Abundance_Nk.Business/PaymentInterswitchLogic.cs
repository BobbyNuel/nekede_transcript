﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Translator;

namespace Abundance_Nk.Business
{
    public class PaymentInterswitchLogic:BusinessBaseLogic<PaymentInterswitch,PAYMENT_INTERSWITCH>
    {
        public PaymentInterswitchLogic()
        {
            translator = new PaymentInterswitchTranslator();
        }

        public PaymentInterswitch GetBy(long Id)
        {
            PaymentInterswitch request = null;
            try
            {
                request = GetModelBy(a => a.Payment_Id == Id);
            }
            catch (Exception)
            {
                
                throw;
            }
            return request;
        }
        public PaymentInterswitch GetBy(string TransactionReference)
        {
            PaymentInterswitch request = null;
            try
            {
                request = GetModelBy(a => a.PAYMENT.Invoice_Number == TransactionReference);
            }
            catch (Exception)
            {
                
                throw;
            }
            return request;
        }
        public PaymentInterswitch GetBy(Payment payment)
        {
            PaymentInterswitch request = null;
            try
            {
                request = GetModelBy(a => a.Payment_Id == payment.Id);
            }
            catch (Exception)
            {
                
                throw;
            }
            return request;
        }

        public bool Modify(PaymentInterswitch modifyPaymentInterswitch)
        {
            try
            {
                Expression<Func<PAYMENT_INTERSWITCH, bool>> selector = f => f.Payment_Id == modifyPaymentInterswitch.Payment.Id;
                PAYMENT_INTERSWITCH entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Amount = modifyPaymentInterswitch.Amount;
                entity.Card_Number = modifyPaymentInterswitch.CardNumber;
                entity.LeadBankCbnCode = modifyPaymentInterswitch.LeadBankCbnCode;
                entity.LeadBankName = modifyPaymentInterswitch.LeadBankName;
                entity.MerchantReference = modifyPaymentInterswitch.MerchantReference;
                entity.PaymentReference = modifyPaymentInterswitch.PaymentReference;
                entity.Payment_Id = modifyPaymentInterswitch.Payment.Id;
                entity.ResponseCode = modifyPaymentInterswitch.ResponseCode;
                entity.ResponseDescription = modifyPaymentInterswitch.ResponseDescription;
                entity.RetrievalReferenceNumber = modifyPaymentInterswitch.RetrievalReferenceNumber;
                if (modifyPaymentInterswitch.SplitAccounts != null && modifyPaymentInterswitch.SplitAccounts.Count() > 0)
                {
                    entity.SplitAccounts = modifyPaymentInterswitch.SplitAccounts[0];
                }
                entity.TransactionDate = modifyPaymentInterswitch.TransactionDate;
               
                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                   return false;
                }

                return true;
            }
            catch (Exception)
            {
                    
                throw;
            }
        }
    }
}
