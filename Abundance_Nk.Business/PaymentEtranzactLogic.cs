﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;

using Abundance_Nk.Business.eTranzactWebService;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;
using System.Transactions;
using System.Net;
using System.IO;

namespace Abundance_Nk.Business
{
    public class PaymentEtranzactLogic : BusinessBaseLogic<PaymentEtranzact, PAYMENT_ETRANZACT>
    {
        string baseUrl = "http://www.etranzact.net/WebConnectPlus/query.jsp";
        public PaymentEtranzactLogic()
        {
            translator = new PaymentEtranzactTranslator();
        }

        public PaymentEtranzact GetBy(Payment payment)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Payment_Id == payment.Id;
                return GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PaymentEtranzact GetBy(string confirmationNumber)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Confirmation_No == confirmationNumber;
                return GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(PaymentEtranzact paymetEtranzact)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Confirmation_No == paymetEtranzact.ConfirmationNo;
                PAYMENT_ETRANZACT paymentEtranzactEntity = GetEntityBy(selector);
                if (paymentEtranzactEntity != null)
                {
                    paymentEtranzactEntity.Payment_Id = paymetEtranzact.Payment.Payment.Id;
                    paymentEtranzactEntity.Customer_Id = paymetEtranzact.Payment.Payment.InvoiceNumber;
                    paymentEtranzactEntity.Confirmation_No = paymetEtranzact.ConfirmationNo;
                    paymentEtranzactEntity.Transaction_Amount = paymetEtranzact.TransactionAmount;
                    paymentEtranzactEntity.Customer_Name = paymetEtranzact.CustomerName;

                    int modified = Save();
                    if (modified > 0)
                    {
                        return true;
                    }

                }

                return false;

            }
            catch (Exception ex)
            {

                throw;
            }
        }
        //Function to retrieve payment details from eTranzact
        public bool ValidatePin(PaymentEtranzact etranzactPayment, Payment payment, decimal Amount)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Confirmation_No == etranzactPayment.ConfirmationNo && p.Customer_Id == payment.InvoiceNumber && p.Transaction_Amount == Amount;
                List<PaymentEtranzact> etranzactPayments = GetModelsBy(selector);
                if (etranzactPayments != null && etranzactPayments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsPinUsed(string confirmationOrderNumber, int personId)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Confirmation_No == confirmationOrderNumber && p.Used == true && p.Used_By_Person_Id != personId;
                List<PaymentEtranzact> etranzactPayments = GetModelsBy(selector);
                if (etranzactPayments != null && etranzactPayments.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PaymentEtranzact RetrievePin(string confirmationNo, PaymentTerminal TerminalID)
        {
            try
            {
                string ReceiptNo = "";
                string PaymentCode = "";
                string MerchantCode = "";
                string TransactionAmount = "";
                string TransactionDescription = "";
                string BankCode = "";
                string BankBranchCode = "";
                string CustomerName = "";
                string CustomerAddress = "";
                string CustomerId = "";
                //string Session = "";

                Hashtable hsParams = new Hashtable();
                hsParams.Clear();
                queryPayoutletTransaction payoutletTransaction = new queryPayoutletTransaction();
                queryPayoutletTransactionResponse gateWayResponse = new queryPayoutletTransactionResponse();

                payoutletTransaction.confirmationNo = confirmationNo.Trim();

                payoutletTransaction.terminalId = TerminalID.TerminalId;
                eTranzactWebService.QueryPayoutletTransactionClient ws = new QueryPayoutletTransactionClient();

                gateWayResponse = ws.queryPayoutletTransaction(payoutletTransaction);
                string Result = gateWayResponse.@return;

                PaymentEtranzact paymentEtz = new PaymentEtranzact();
                if (Result != "-1")
                {
                    String[] RSplit = Result.Replace("%20&", "%20and").Replace("%20", " ").Split('&');
                    String[] Rsplitx;
                    foreach (string s in RSplit)
                    {
                        Rsplitx = s.Split('=');
                        hsParams.Add(Rsplitx[0], Rsplitx[1]);
                    }

                    ReceiptNo = hsParams["RECEIPT_NO"].ToString().Trim();
                    PaymentCode = hsParams["PAYMENT_CODE"].ToString().Trim();
                    MerchantCode = hsParams["MERCHANT_CODE"].ToString().Trim();
                    TransactionAmount = hsParams["TRANS_AMOUNT"].ToString().Trim();
                    TransactionDescription = hsParams["TRANS_DESCR"].ToString().Trim();
                    BankCode = hsParams["BANK_CODE"].ToString().Trim();
                    BankBranchCode = hsParams["BRANCH_CODE"].ToString().Trim();
                    CustomerName = hsParams["CUSTOMER_NAME"].ToString().Trim();
                    CustomerAddress = hsParams["CUSTOMER_ADDRESS"].ToString().Trim();
                    CustomerId = hsParams["CUSTOMER_ID"].ToString().Trim();
                    //Session = "1";
                    hsParams.Clear();

                    paymentEtz.BankCode = BankCode;
                    paymentEtz.BranchCode = BankBranchCode;
                    paymentEtz.ConfirmationNo = PaymentCode;
                    paymentEtz.CustomerAddress = CustomerAddress;
                    paymentEtz.CustomerID = CustomerId;
                    paymentEtz.CustomerName = CustomerName;
                    paymentEtz.MerchantCode = MerchantCode;
                    paymentEtz.PaymentCode = PaymentCode;
                    paymentEtz.ReceiptNo = ReceiptNo;
                    paymentEtz.TransactionAmount = Convert.ToDecimal(TransactionAmount);
                    paymentEtz.TransactionDate = DateTime.Now;
                    paymentEtz.TransactionDescription = TransactionDescription;
                    paymentEtz.Used = false;
                    paymentEtz.Terminal = TerminalID;
                    paymentEtz.UsedBy = 0;

                    PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                    PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                    paymentEtranzactType = paymentEtranzactTypeLogic.GetModelBy(p => p.Fee_Type_Id == TerminalID.FeeType.Id);

                    paymentEtz.EtranzactType = paymentEtranzactType;

                    Payment payment = new Payment();
                    PaymentLogic paymentLogic = new PaymentLogic();
                    //CustomerId
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == CustomerId);

                    if (payment != null)
                    {
                        OnlinePayment onlinePayment = new OnlinePayment();
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        onlinePayment = onlinePaymentLogic.GetModelBy(c => c.PAYMENT_CHANNEL.Payment_Channnel_Id == (int)PaymentChannel.Channels.Etranzact && c.Payment_Id == payment.Id);

                        paymentEtz.Payment = onlinePayment;
                    }


                    //paymentEtz.SessionId = Convert.ToInt16(Session);

                    base.Create(paymentEtz);
                    return paymentEtz;
                }


                return paymentEtz;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PaymentEtranzact RetrievePinsWithoutInvoice(string confirmationNo, ApplicationForm applicant, int feeType_Id, PaymentTerminal TerminalID)
        {
            try
            {
                PaymentEtranzact paymentEtz = new PaymentEtranzact();
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
                {
                    string ReceiptNo = "";
                    string PaymentCode = "";
                    string MerchantCode = "";
                    string TransactionAmount = "";
                    string TransactionDescription = "";
                    string BankCode = "";
                    string BankBranchCode = "";
                    string CustomerName = "";
                    string CustomerAddress = "";
                    string CustomerId = "";
                    //string Session = "";

                    Hashtable hsParams = new Hashtable();
                    hsParams.Clear();
                    //queryPayoutletTransaction payoutletTransaction = new queryPayoutletTransaction();
                    //queryPayoutletTransactionResponse gateWayResponse = new queryPayoutletTransactionResponse();

                    //payoutletTransaction.confirmationNo = confirmationNo.Trim();

                    //payoutletTransaction.terminalId = TerminalID.TerminalId;
                    //eTranzactWebService.QueryPayoutletTransactionClient ws = new QueryPayoutletTransactionClient();

                    //gateWayResponse = ws.queryPayoutletTransaction(payoutletTransaction);
                    //string Result = gateWayResponse.@return;

                    string Result = "";
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    string postData = "TERMINAL_ID=" + TerminalID.TerminalId + "&CONFIRMATION_NO=" + confirmationNo;
                    byte[] bytes = Encoding.UTF8.GetBytes(postData);
                    request.ContentLength = bytes.Length;

                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(bytes, 0, bytes.Length);

                    WebResponse response = request.GetResponse();
                    Stream stream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(stream);

                    Result = reader.ReadToEnd();
                    stream.Dispose();
                    reader.Dispose();

                  
                    if (Result != "-1")
                    {
                        String[] RSplit = Result.Replace("\r\n", "").Replace("</html>", "").Replace("%20&", "%20and").Replace("%20", " ").Split('&');
                    
                        String[] Rsplitx;
                        foreach (string s in RSplit)
                        {
                            Rsplitx = s.Split('=');
                            hsParams.Add(Rsplitx[0], Rsplitx[1]);
                        }

                        ReceiptNo = hsParams["RECEIPT_NO"].ToString().Trim();
                        PaymentCode = hsParams["PAYMENT_CODE"].ToString().Trim();
                        MerchantCode = hsParams["MERCHANT_CODE"].ToString().Trim();
                        TransactionAmount = hsParams["TRANS_AMOUNT"].ToString().Trim();
                        TransactionDescription = hsParams["TRANS_DESCR"].ToString().Trim();
                        BankCode = hsParams["BANK_CODE"].ToString().Trim();
                        BankBranchCode = hsParams["BRANCH_CODE"].ToString().Trim();
                        CustomerName = hsParams["CUSTOMER_NAME"].ToString().Trim();
                        CustomerAddress = hsParams["CUSTOMER_ADDRESS"].ToString().Trim();
                        CustomerId = hsParams["CUSTOMER_ID"].ToString().Trim();
                        //Session = "1";
                        hsParams.Clear();

                        paymentEtz.BankCode = BankCode;
                        paymentEtz.BranchCode = BankBranchCode;
                        paymentEtz.ConfirmationNo = PaymentCode;
                        paymentEtz.CustomerAddress = CustomerAddress;
                        paymentEtz.CustomerName = CustomerName;
                        paymentEtz.MerchantCode = MerchantCode;
                        paymentEtz.PaymentCode = PaymentCode;
                        paymentEtz.ReceiptNo = ReceiptNo;
                        paymentEtz.TransactionAmount = Convert.ToDecimal(TransactionAmount);
                        paymentEtz.TransactionDate = DateTime.Now;
                        paymentEtz.TransactionDescription = TransactionDescription;
                        paymentEtz.Used = false;
                        paymentEtz.Terminal = TerminalID;
                        paymentEtz.UsedBy = 0;

                        //if (CustomerId != applicant.Number)
                        //{

                        //}

                        FeeDetail fd = new FeeDetail();
                        FeeDetailLogic fdLogic = new FeeDetailLogic();
                        Decimal amt = Convert.ToDecimal(TransactionAmount);
                        fd = fdLogic.GetModelBy(m => m.Fee_Type_Id == feeType_Id);

                        Programme pgm = new Programme();
                        pgm = applicant.ProgrammeFee.Programme;


                        Session CurrentSession = GetCurrentSession();
                        FeeType feeType = new FeeType();
                        FeeTypeLogic feeTypeLogic = new FeeTypeLogic();
                        feeType = feeTypeLogic.GetModelBy(f => f.Fee_Type_Id == feeType_Id);
                        PaymentEtranzactType pet = new PaymentEtranzactType();
                        pet = GetPaymentTypeBy(feeType);
                        ApplicationFormSetting AS = new ApplicationFormSetting();
                        AS = GetApplicationFormSettingBy(CurrentSession);

                        if (AS != null)
                        {
                            PaymentType PT = new PaymentType();
                            PT = AS.PaymentType;
                        }
                        Person person = new Person();
                        person = applicant.Person;
                        Payment pmt = new Payment();
                        Payment payment = CreatePayment(person, AS, feeType);

                        pmt = new Payment() { Id = payment.Id, InvoiceNumber = payment.InvoiceNumber, PaymentType = payment.PaymentType, Person = person, FeeDetails = payment.FeeDetails, Session = CurrentSession };

                        PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                        PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                        paymentEtranzactType = paymentEtranzactTypeLogic.GetModelBy(p => p.Fee_Type_Id == feeType.Id);

                        paymentEtz.EtranzactType = paymentEtranzactType;


                        if (payment != null)
                        {
                            OnlinePayment onlinePayment = new OnlinePayment();
                            OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                            onlinePayment = onlinePaymentLogic.GetModelBy(c => c.PAYMENT_CHANNEL.Payment_Channnel_Id == (int)PaymentChannel.Channels.Etranzact && c.Payment_Id == payment.Id);
                            paymentEtz.Payment = onlinePayment;
                        }



                        paymentEtz.CustomerID = payment.InvoiceNumber;

                        base.Create(paymentEtz);
                        transaction.Complete();
                        return paymentEtz;
                    }
                }


                return paymentEtz;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePin(Payment payment, Person person)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Payment_Id == payment.Id;
                PAYMENT_ETRANZACT paymentEtranzactEntity = GetEntityBy(selector);

                if (paymentEtranzactEntity == null || paymentEtranzactEntity.Payment_Id <= 0)
                {
                    return false;
                }

                paymentEtranzactEntity.Used = true;
                paymentEtranzactEntity.Used_By_Person_Id = person.Id;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool RetrieveErrorPin(string confirmationNo, string TerminalID)
        {
            try
            {
                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.Snapshot }))
                {
                    string ReceiptNo = "";
                    string PaymentCode = "";
                    string MerchantCode = "";
                    string TransactionAmount = "";
                    string TransactionDescription = "";
                    string BankCode = "";
                    string BankBranchCode = "";
                    string CustomerName = "";
                    string CustomerAddress = "";
                    string CustomerId = "";
                    //string Session = "";

                    Hashtable hsParams = new Hashtable();
                    hsParams.Clear();
                    queryPayoutletTransaction payoutletTransaction = new queryPayoutletTransaction();
                    queryPayoutletTransactionResponse gateWayResponse = new queryPayoutletTransactionResponse();

                    payoutletTransaction.confirmationNo = confirmationNo.Trim();

                    payoutletTransaction.terminalId = TerminalID;
                    eTranzactWebService.QueryPayoutletTransactionClient ws = new QueryPayoutletTransactionClient();

                    gateWayResponse = ws.queryPayoutletTransaction(payoutletTransaction);
                    string Result = gateWayResponse.@return;

                    PaymentEtranzact paymentEtz = new PaymentEtranzact();
                    if (Result != "-1")
                    {
                        String[] RSplit = Result.Replace("%20&", "%20and").Replace("%20", " ").Split('&');
                        String[] Rsplitx;
                        foreach (string s in RSplit)
                        {
                            Rsplitx = s.Split('=');
                            hsParams.Add(Rsplitx[0], Rsplitx[1]);
                        }

                        ReceiptNo = hsParams["RECEIPT_NO"].ToString().Trim();
                        PaymentCode = hsParams["PAYMENT_CODE"].ToString().Trim();
                        MerchantCode = hsParams["MERCHANT_CODE"].ToString().Trim();
                        TransactionAmount = hsParams["TRANS_AMOUNT"].ToString().Trim();
                        TransactionDescription = hsParams["TRANS_DESCR"].ToString().Trim();
                        BankCode = hsParams["BANK_CODE"].ToString().Trim();
                        BankBranchCode = hsParams["BRANCH_CODE"].ToString().Trim();
                        CustomerName = hsParams["CUSTOMER_NAME"].ToString().Trim();
                        CustomerAddress = hsParams["CUSTOMER_ADDRESS"].ToString().Trim();
                        CustomerId = hsParams["CUSTOMER_ID"].ToString().Trim();
                        //Session = "1";
                        hsParams.Clear();

                        paymentEtz.BankCode = BankCode;
                        paymentEtz.BranchCode = BankBranchCode;
                        paymentEtz.ConfirmationNo = PaymentCode;
                        paymentEtz.CustomerAddress = CustomerAddress;
                        paymentEtz.CustomerName = CustomerName;
                        paymentEtz.MerchantCode = MerchantCode;
                        paymentEtz.PaymentCode = PaymentCode;
                        paymentEtz.ReceiptNo = ReceiptNo;
                        paymentEtz.TransactionAmount = Convert.ToDecimal(TransactionAmount);
                        paymentEtz.TransactionDate = DateTime.Now;
                        paymentEtz.TransactionDescription = TransactionDescription;
                        paymentEtz.Used = false;

                        PaymentTerminal Pterminal = new PaymentTerminal();
                        PaymentTerminalLogic pterminalLogic = new PaymentTerminalLogic();
                        Pterminal = pterminalLogic.GetModelBy(pt => pt.Terminal_Id == TerminalID);

                        paymentEtz.Terminal = Pterminal;
                        paymentEtz.UsedBy = 0;

                        FeeDetail fd = new FeeDetail();
                        FeeDetailLogic fdLogic = new FeeDetailLogic();
                        Decimal amt = Convert.ToDecimal(TransactionAmount);
                        fd = fdLogic.GetModelBy(m => m.FEE.Amount == amt && (m.Fee_Type_Id == 1 || m.Fee_Type_Id == 4));

                        Programme pgm = new Programme();
                        ProgrammeLogic pgmLogic = new ProgrammeLogic();
                        if (Convert.ToDecimal(TransactionAmount) == 8000)
                        {
                            pgm = pgmLogic.GetModelBy(k => k.Programme_Id == 1);
                        }
                        else
                        {
                            pgm = pgmLogic.GetModelBy(k => k.Programme_Id == 5);
                        }


                        Session CurrentSession = GetCurrentSession();
                        FeeType feeType = new FeeType();
                        feeType = GetFeeTypeBy(CurrentSession, pgm);
                        PaymentEtranzactType pet = new PaymentEtranzactType();
                        pet = GetPaymentTypeBy(feeType);
                        ApplicationFormSetting AS = new ApplicationFormSetting();
                        AS = GetApplicationFormSettingBy(CurrentSession);

                        if (AS != null)
                        {
                            PaymentType PT = new PaymentType();
                            PT = AS.PaymentType;
                        }

                        string[] fullnames = CustomerName.Split(new char[0], StringSplitOptions.RemoveEmptyEntries);
                        Person ps = new Person();
                        ps.LastName = fullnames[0];
                        ps.FirstName = fullnames[1];

                        if (fullnames.Length > 2)
                        {
                            ps.OtherName = fullnames[2];
                        }

                        ps.State = new State() { Id = "IM" };
                        ps.MobilePhone = "07031234567";


                        Person person = CreatePerson(ps, AS);
                        Payment pmt = new Payment();
                        Payment payment = CreatePayment(person, AS, feeType);

                        pmt = new Payment() { Id = payment.Id, InvoiceNumber = payment.InvoiceNumber, PaymentType = payment.PaymentType, Person = person, FeeDetails = payment.FeeDetails };

                        string search = CustomerAddress.Substring(CustomerAddress.IndexOf('-') + 1);
                        Department department = new Department();
                        DepartmentLogic departmentLogic = new DepartmentLogic();
                        department = departmentLogic.GetModelBy(d => d.Department_Name == search);
                        if (department != null)
                        {
                            AppliedCourse appliedCourse = CreateAppliedCourse(pgm, department, person);

                        }

                        PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                        PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                        paymentEtranzactType = paymentEtranzactTypeLogic.GetModelBy(p => p.Fee_Type_Id == feeType.Id);

                        paymentEtz.EtranzactType = paymentEtranzactType;


                        if (payment != null)
                        {
                            OnlinePayment onlinePayment = new OnlinePayment();
                            OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                            onlinePayment = onlinePaymentLogic.GetModelBy(c => c.PAYMENT_CHANNEL.Payment_Channnel_Id == (int)PaymentChannel.Channels.Etranzact && c.Payment_Id == payment.Id);

                            paymentEtz.Payment = onlinePayment;
                        }



                        paymentEtz.CustomerID = payment.InvoiceNumber;

                        base.Create(paymentEtz);
                        transaction.Complete();
                        return true;
                    }
                }


                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Person CreatePerson(Person ps, ApplicationFormSetting AS)
        {
            try
            {
                Role role = new Role() { Id = 6 };
                PersonType personType = new PersonType() { Id = AS.PersonType.Id };
                Nationality nationality = new Nationality() { Id = 1 };

                ps.Role = role;
                ps.PersonType = personType;
                ps.Nationality = nationality;
                ps.DateEntered = DateTime.Now;

                PersonLogic personLogic = new PersonLogic();
                Person person = personLogic.Create(ps);
                if (person != null && person.Id > 0)
                {
                    ps = person;
                }

                return person;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private Payment CreatePayment(Person person, ApplicationFormSetting AS, FeeType ft)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                payment.PaymentMode = new PaymentMode() { Id = AS.PaymentMode.Id };
                payment.PaymentType = new PaymentType() { Id = AS.PaymentType.Id };
                payment.PersonType = new PersonType() { Id = AS.PersonType.Id };
                payment.FeeType = new FeeType() { Id = ft.Id };
                payment.DatePaid = DateTime.Now;
                payment.Person = person;
                payment.Session = AS.Session;

                OnlinePayment newOnlinePayment = null;
                Payment newPayment = paymentLogic.Create(payment);
                if (newPayment != null)
                {
                    PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
                    OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                    OnlinePayment onlinePayment = new OnlinePayment();
                    onlinePayment.Channel = channel;
                    onlinePayment.Payment = newPayment;
                    newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
                }

                return newPayment;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private AppliedCourse CreateAppliedCourse(Programme programme, Department department, Person person)
        {
            try
            {
                AppliedCourse appliedCourse = new AppliedCourse();
                appliedCourse.Programme = programme;
                appliedCourse.Department = department;
                appliedCourse.Person = person;

                AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                return appliedCourseLogic.Create(appliedCourse);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FeeType GetFeeTypeBy(Session session, Programme programme)
        {
            try
            {
                ApplicationProgrammeFeeLogic programmeFeeLogic = new ApplicationProgrammeFeeLogic();
                ApplicationProgrammeFee A = new ApplicationProgrammeFee();
                A = programmeFeeLogic.GetBy(programme, session);

                if (A != null)
                {
                    return A.FeeType;
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PaymentEtranzactType GetPaymentTypeBy(FeeType feeType)
        {
            PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
            PaymentEtranzactType p = new PaymentEtranzactType();
            p = paymentEtranzactTypeLogic.GetBy(feeType);

            if (p != null)
            {
                return p;
            }

            return null;
        }

        public ApplicationFormSetting GetApplicationFormSettingBy(Session session)
        {
            try
            {
                ApplicationFormSettingLogic applicationFormSettingLogic = new ApplicationFormSettingLogic();
                return applicationFormSettingLogic.GetBy(session);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Session GetCurrentSession()
        {
            try
            {
                CurrentSessionSemesterLogic currentSessionLogic = new CurrentSessionSemesterLogic();
                CurrentSessionSemester currentSessionSemester = currentSessionLogic.GetCurrentSessionTerm();

                if (currentSessionSemester != null && currentSessionSemester.SessionSemester != null)
                {
                    return currentSessionSemester.SessionSemester.Session;
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PaymentEtranzact RetrievePinAlternative(string confirmationNo, PaymentTerminal TerminalID)
        {
            try
            {
                string ReceiptNo = "";
                string PaymentCode = "";
                string MerchantCode = "";
                string TransactionAmount = "";
                string TransactionDescription = "";
                string BankCode = "";
                string BankBranchCode = "";
                string CustomerName = "";
                string CustomerAddress = "";
                string CustomerId = "";
                //string Session = "";


                Hashtable hsParams = new Hashtable();
                hsParams.Clear();
                string Result = "";
                string param = "";

                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("TERMINAL_ID",TerminalID.TerminalId),
                    new KeyValuePair<string, string>("CONFIRMATION_NO", confirmationNo)
                };

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string postData = "TERMINAL_ID=" + TerminalID.TerminalId + "&CONFIRMATION_NO=" + confirmationNo;
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bytes.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);

                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);

                Result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();


                PaymentEtranzact paymentEtz = new PaymentEtranzact();
                if (Result != "-1" && Result.Length > 10)
                {
                    String[] RSplit = Result.Replace("\r\n", "").Replace("</html>", "").Replace("%20&", "%20and").Replace("%20", " ").Split('&');
                    String[] Rsplitx;
                    foreach (string s in RSplit)
                    {
                        Rsplitx = s.Split('=');
                        hsParams.Add(Rsplitx[0], Rsplitx[1]);
                    }

                    ReceiptNo = hsParams["RECEIPT_NO"].ToString().Trim();
                    PaymentCode = hsParams["PAYMENT_CODE"].ToString().Trim();
                    MerchantCode = hsParams["MERCHANT_CODE"].ToString().Trim();
                    TransactionAmount = hsParams["TRANS_AMOUNT"].ToString().Trim();
                    TransactionDescription = hsParams["TRANS_DESCR"].ToString().Trim();
                    BankCode = hsParams["BANK_CODE"].ToString().Trim();
                    BankBranchCode = hsParams["BRANCH_CODE"].ToString().Trim();
                    CustomerName = hsParams["CUSTOMER_NAME"].ToString().Trim();
                    CustomerAddress = hsParams["CUSTOMER_ADDRESS"].ToString().Trim();
                    CustomerId = hsParams["CUSTOMER_ID"].ToString().Trim();
                    //Session = "1";
                    hsParams.Clear();

                    paymentEtz.BankCode = BankCode;
                    paymentEtz.BranchCode = BankBranchCode;
                    paymentEtz.ConfirmationNo = PaymentCode;
                    paymentEtz.CustomerAddress = CustomerAddress;
                    paymentEtz.CustomerID = CustomerId;
                    paymentEtz.CustomerName = CustomerName;
                    paymentEtz.MerchantCode = MerchantCode;
                    paymentEtz.PaymentCode = PaymentCode;
                    paymentEtz.ReceiptNo = ReceiptNo;
                    paymentEtz.TransactionAmount = Convert.ToDecimal(TransactionAmount);
                    paymentEtz.TransactionDate = DateTime.Now;
                    paymentEtz.TransactionDescription = TransactionDescription;
                    paymentEtz.Used = false;
                    paymentEtz.Terminal = TerminalID;
                    paymentEtz.UsedBy = 0;

                    PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                    PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                    paymentEtranzactType = paymentEtranzactTypeLogic.GetModelsBy(p => p.Fee_Type_Id == TerminalID.FeeType.Id).LastOrDefault();

                    paymentEtz.EtranzactType = paymentEtranzactType;

                    Payment payment = new Payment();
                    PaymentLogic paymentLogic = new PaymentLogic();
                    //CustomerId
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == CustomerId);

                    if (payment != null)
                    {
                        OnlinePayment onlinePayment = new OnlinePayment();
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        onlinePayment = onlinePaymentLogic.GetModelBy(c => c.PAYMENT_CHANNEL.Payment_Channnel_Id == (int)PaymentChannel.Channels.Etranzact && c.Payment_Id == payment.Id);

                        paymentEtz.Payment = onlinePayment;
                    }

                    base.Create(paymentEtz);
                    return paymentEtz;
                }


                return paymentEtz;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPaymentStatus (string invoice)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT,bool>> selector = p => p.ONLINE_PAYMENT.PAYMENT.Invoice_Number == invoice ;
                PaymentEtranzact etranzactPayments = GetModelBy(selector);
                if(etranzactPayments != null )
                {
                    return "FEE HAS BEEN PAID";
                }
                else
                {
                    return "FEE HAS NOT BEEN PAID";
                }
            }
            catch(Exception)
            {
                throw;
            }
        }

        public bool IsPinOnTable(string confirmationOrderNumber)
        {
            try
            {
                Expression<Func<PAYMENT_ETRANZACT, bool>> selector = p => p.Confirmation_No == confirmationOrderNumber;
                List<PaymentEtranzact> etranzactPayments = GetModelsBy(selector);
                if (etranzactPayments != null && etranzactPayments.Count > 0)
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PaymentEtranzact RetrieveEtranzactWebServicePinDetails(string confirmationNo, PaymentTerminal TerminalID)
        {
            try
            {
                string ReceiptNo = "";
                string PaymentCode = "";
                string MerchantCode = "";
                string TransactionAmount = "";
                string TransactionDescription = "";
                string BankCode = "";
                string BankBranchCode = "";
                string CustomerName = "";
                string CustomerAddress = "";
                string CustomerId = "";
                //string Session = "";

                Hashtable hsParams = new Hashtable();
                hsParams.Clear();
                //queryPayoutletTransaction payoutletTransaction = new queryPayoutletTransaction();
                //queryPayoutletTransactionResponse gateWayResponse = new queryPayoutletTransactionResponse();

                //payoutletTransaction.confirmationNo = confirmationNo.Trim();
                //payoutletTransaction.terminalId = TerminalID.TerminalId;
                //eTranzactWebService.QueryPayoutletTransactionClient ws = new QueryPayoutletTransactionClient();

                //gateWayResponse = ws.queryPayoutletTransaction(payoutletTransaction);
                //string Result = gateWayResponse.@return;
                string Result = "";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(baseUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                string postData = "TERMINAL_ID=" + TerminalID.TerminalId + "&CONFIRMATION_NO=" + confirmationNo;
                byte[] bytes = Encoding.UTF8.GetBytes(postData);
                request.ContentLength = bytes.Length;

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);

                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);

                Result = reader.ReadToEnd();
                stream.Dispose();
                reader.Dispose();


                PaymentEtranzact paymentEtz = new PaymentEtranzact();
                if (Result != "-1" && Result != "\r\n\r\nSUCCESS=-1\r\n\r\n\r\n</html>" && Result.Length > 10)
                {
                    String[] RSplit = Result.Replace("\r\n", "").Replace("</html>", "").Replace("=%20&", "=FPIDEFAULT&").Replace("%20&", "%20and").Replace("%20", " ").Split('&');
                    String[] Rsplitx;
                    foreach (string s in RSplit)
                    {
                        Rsplitx = s.Split('=');
                        hsParams.Add(Rsplitx[0], Rsplitx[1]);
                    }


                    ReceiptNo = hsParams["RECEIPT_NO"].ToString().Trim();
                    PaymentCode = hsParams["PAYMENT_CODE"].ToString().Trim();
                    MerchantCode = hsParams["MERCHANT_CODE"].ToString().Trim();
                    TransactionAmount = hsParams["TRANS_AMOUNT"].ToString().Trim();
                    TransactionDescription = hsParams["TRANS_DESCR"].ToString().Trim();
                    BankCode = hsParams["BANK_CODE"].ToString().Trim();
                    CustomerName = hsParams["CUSTOMER_NAME"].ToString().Trim();
                    BankBranchCode = hsParams["BRANCH_CODE"].ToString().Trim();
                    CustomerId = hsParams["CUSTOMER_ID"].ToString().Trim();
                    CustomerAddress = hsParams["CUSTOMER_ADDRESS"].ToString().Trim();

                    hsParams.Clear();

                    paymentEtz.BankCode = BankCode;
                    paymentEtz.BranchCode = BankBranchCode;
                    paymentEtz.ConfirmationNo = PaymentCode;
                    paymentEtz.CustomerAddress = CustomerAddress;
                    paymentEtz.CustomerID = CustomerId;
                    paymentEtz.CustomerName = CustomerName;
                    paymentEtz.MerchantCode = MerchantCode;
                    paymentEtz.PaymentCode = PaymentCode;
                    paymentEtz.ReceiptNo = ReceiptNo;
                    paymentEtz.TransactionAmount = Convert.ToDecimal(TransactionAmount);
                    paymentEtz.TransactionDate = DateTime.Now;
                    paymentEtz.TransactionDescription = TransactionDescription;
                    paymentEtz.Used = false;
                    paymentEtz.Terminal = TerminalID;
                    paymentEtz.UsedBy = 0;
                    return paymentEtz;
                }


                return paymentEtz;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
