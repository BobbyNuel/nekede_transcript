﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class SessionLogic : BusinessBaseLogic<Session, SESSION>
    {
        public SessionLogic()
        {
            translator = new SessionTranslator();
        }

       public bool Modify(Session session)
        {
            try
            {
                Expression<Func<SESSION, bool>> selector = s => s.Session_Id == session.Id;
                SESSION entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Session_Name = session.Name;
                entity.Start_Date = session.StartDate;
                entity.End_date = session.EndDate;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

       public Session GetCurrentSession()
        {
           try
           {
               Expression<Func<SESSION, bool>> selector = s => s.Activated == true;
               return GetModelBy(selector);
           }
           catch (Exception)
           {
               throw;
           }
        }


    }
}

