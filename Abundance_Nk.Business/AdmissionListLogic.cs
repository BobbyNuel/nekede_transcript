﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;
using System.Data.Entity.Validation;
using System.Web.UI.WebControls;

namespace Abundance_Nk.Business
{
    public class AdmissionListLogic : BusinessBaseLogic<AdmissionList, ADMISSION_LIST>
    {
        public AdmissionListLogic()
        {
            translator = new AdmissionListTranslator();
        }

        public AdmissionList GetBy(long applicationFormId)
        {
            try
            {
                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Application_Form_Id == applicationFormId;

                AdmissionList admission = GetModelBy(selector);
                if (admission != null && admission.Id > 0)
                {

                }

                return admission;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public AdmissionList GetBy(Person person)
        {
            try
            {
                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.APPLICATION_FORM.Person_Id == person.Id;

                AdmissionList admission = GetModelsBy(selector).FirstOrDefault();
                if (admission != null && admission.Id > 0)
                {

                }

                return admission;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsAdmitted(ApplicationForm applicationForm)
        {
            try
            {
                bool admitted = false;

                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Application_Form_Id == applicationForm.Id && a.Activated == true;
                AdmissionList admissionList = GetModelBy(selector);
                if (admissionList != null && admissionList.Id > 0)
                {
                    admitted = true;
                }

                return admitted;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool AdmissionExist(ApplicationForm applicationForm)
        {
            try
            {
                bool admitted = false;

                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Application_Form_Id == applicationForm.Id;
                AdmissionList admissionList = GetModelBy(selector);
                if (admissionList != null && admissionList.Id > 0)
                {
                    admitted = true;
                }

                return admitted;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool IsAdmitted(string ApplicationNumber)
        {
            try
            {
                bool admitted = false;

                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.APPLICATION_FORM.Application_Form_Number == ApplicationNumber;
                AdmissionList admissionList = GetModelBy(selector);
                if (admissionList != null && admissionList.Id > 0)
                {
                    admitted = true;
                }

                return admitted;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public AdmissionList Create(AdmissionList admissionlist, AdmissionListBatch batch, AdmissionListAudit Audit)
        {
            AdmissionList List = new AdmissionList();
            try
            {
                AdmissionListAuditLogic auditLogic = new AdmissionListAuditLogic();
                admissionlist.Batch = batch;
                if (!IsAdmitted(admissionlist.Form))
                {
                    List = base.Create(admissionlist);
                    Audit.AdmissionList = admissionlist;
                    Audit.AdmissionList.Id = List.Id;
                    Audit.Form = admissionlist.Form;
                    Audit.Deprtment = admissionlist.Deprtment;
                    Audit.DepartmentOption = admissionlist.DepartmentOption;
                    auditLogic.Create(Audit);
                }
            }
            catch (DbEntityValidationException e)
            {
                //foreach (var eve in e.EntityValidationErrors)
                //{
                //    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                //        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                //    foreach (var ve in eve.ValidationErrors)
                //    {
                //        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                //            ve.PropertyName, ve.ErrorMessage);
                //    }
                //}
                throw e;
            }
          
            return List;
            
        }

        public bool IsValidApplicationNumberAndPin(string applicationNumber, string pin)
        {
            try
            {
                Expression<Func<APPLICATION_FORM, bool>> selector = af => af.Application_Form_Number == applicationNumber;
                ApplicationForm applicationForm = new ApplicationForm();
                ApplicationFormLogic formLogic = new ApplicationFormLogic();
                applicationForm = formLogic.GetModelBy(selector);
                if (applicationForm == null)
                {
                    throw new Exception("Invalid Application Number!");
                }

                //Payment payment = InvalidConfirmationOrderNumber(pin, applicationForm, (int)FeeTypes.ResultChecking);
                //if (payment == null || payment.Id <= 0)
                //{
                //    throw new Exception("Invalid Pin!");
                //}

                if (!IsAdmitted(applicationForm))
                {
                    throw new Exception("You have not been admitted! Please check again as admission is ongoing");
                }


                //if (payment != null && payment.Id > 0 && applicationForm.Id > 0)
                //{
                //    PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
                //    bool pinUseStatus = etranzactLogic.IsPinUsed(pin, (int)applicationForm.Person.Id);
                //    if (pinUseStatus)
                //    {
                //        throw new Exception("Pin entered does not belong to applicant '" + applicationForm.Person.FullName + "' !");
                  
                //    }
                //    etranzactLogic.UpdatePin(payment, applicationForm.Person);
                //    return true;
                //}

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private Payment InvalidConfirmationOrderNumber(string confirmationOrderNumber, ApplicationForm applicant, int feeType_Id)
        {
            Payment payment = new Payment();
            PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
            PaymentEtranzact etranzactDetails = etranzactLogic.GetModelBy(m => m.Confirmation_No == confirmationOrderNumber);
            if (etranzactDetails == null || etranzactDetails.ReceiptNo == null)
            {
                PaymentTerminal paymentTerminal = new PaymentTerminal();
                PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == feeType_Id && p.Session_Id == applicant.ProgrammeFee.Session.Id);

                etranzactDetails = etranzactLogic.RetrievePinsWithoutInvoice(confirmationOrderNumber, applicant, feeType_Id, paymentTerminal);
                if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                    if (payment != null && payment.Id > 0)
                    {
                        FeeDetail feeDetail = new FeeDetail();
                        FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                        feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);
                        if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                        {
                            throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");

                        }

                    }
                    else
                    {
                        throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");

                    }
                }
                else
                {
                    throw new Exception("Confirmation Order Number entered seems not to be valid! Please cross check and try again.");

                }
            }
            else
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                if (payment != null && payment.Id > 0)
                {
                    FeeDetail feeDetail = new FeeDetail();
                    FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                    feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);

                    if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                    {
                        throw new Exception("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.");

                    }
                }
                else
                {
                    throw new Exception("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.");

                }
            }

            return payment;
        }
        public bool HasStudentCheckedStatus(long fid)
        {
            try
            {
                ApplicationForm appForm = new ApplicationForm();
                ApplicationFormLogic appFormLogic = new ApplicationFormLogic();
                appForm = appFormLogic.GetModelBy(a => a.Application_Form_Id == fid);
                if (appForm != null)
                {
                   PaymentEtranzact eTranzact = new PaymentEtranzact();
                   PaymentEtranzactLogic eTranzactLogic = new PaymentEtranzactLogic();

                   eTranzact = eTranzactLogic.GetModelsBy(p => p.Used_By_Person_Id == appForm.Person.Id && p.Payment_Etranzact_Type_Id ==3).FirstOrDefault();
                   if (eTranzact != null)
                   {
                       return true;
                   }
                }
               
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return false;
        }



        public bool Update(AdmissionList admissionlist, AdmissionListAudit Audit)
        {

            try
            {
                AdmissionListAuditLogic auditLogic = new AdmissionListAuditLogic();
                if (IsAdmitted(admissionlist.Form))
                {
                    Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Admission_List_Id == admissionlist.Id;
                    ADMISSION_LIST List = GetEntityBy(selector);
                    if (List != null && List.Admission_List_Id > 0)
                    {
                        List.Department_Id = admissionlist.Deprtment.Id;
                        List.Programme_Id = admissionlist.Programme.Id;
                        List.Activated = admissionlist.Activated;

                        int modifiedRecordCount = Save();

                        if (modifiedRecordCount > 0)
                        {
                            Audit.AdmissionList = admissionlist;
                            Audit.AdmissionList.Id = List.Admission_List_Id;
                            Audit.Form = admissionlist.Form;
                            Audit.Deprtment = admissionlist.Deprtment;
                            Audit.DepartmentOption = admissionlist.DepartmentOption;
                            Audit.Time = DateTime.Now;
                            auditLogic.Create(Audit);
                            return true;
                        }
                    }

                    return false;
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return false;

        }
        public List<AdmissionListModel> GetAdmissionListBy(Session session, Programme programme, Department department, DateTime dateFrom, DateTime dateTo)
        {
            List<AdmissionListModel> admissionList = new List<AdmissionListModel>();
            try
            {
                admissionList = (from sr in repository.GetBy<VW_ADMISSION_LIST_ALL>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id && a.Date_Uploaded >= dateFrom && a.Date_Uploaded <= dateTo)
                                 select new AdmissionListModel
                                 {
                                     PersonId = sr.Person_Id,
                                     FullName = sr.Full_Name,
                                     ExamNumber = sr.Exam_Number,
                                     ApplicationNumber = sr.Application_Form_Number,
                                     ProgrammeId = sr.Programme_Id,
                                     Programme = sr.Programme_Name,
                                     DepartmentId = sr.Department_Id,
                                     Department = sr.Department_Name,
                                     AdmissionListType = sr.Admission_List_Type_Name,
                                     DateUploaded = sr.Date_Uploaded,
                                     JambNumber = sr.Applicant_Jamb_Registration_Number,
                                     SessionName = sr.Session_Name,
                                     SessionId = sr.Session_Id
                                 }).ToList();



            }
            catch (Exception)
            {
                throw;
            }

            return admissionList.OrderBy(a => a.ApplicationNumber).ToList();
        }
        public List<AdmissionListModel> GetAdmissionListBulk(Session session, DateTime dateFrom, DateTime dateTo)
        {
            List<AdmissionListModel> admissionList = new List<AdmissionListModel>();
            try
            {
                admissionList = (from sr in repository.GetBy<VW_ADMISSION_LIST_ALL>(a => a.Session_Id == session.Id && a.Date_Uploaded >= dateFrom && a.Date_Uploaded <= dateTo)
                                 select new AdmissionListModel
                                 {
                                     PersonId = sr.Person_Id,
                                     FullName = sr.Full_Name,
                                     ExamNumber = sr.Exam_Number,
                                     ApplicationNumber = sr.Application_Form_Number,
                                     ProgrammeId = sr.Programme_Id,
                                     Programme = sr.Programme_Name,
                                     DepartmentId = sr.Department_Id,
                                     Department = sr.Department_Name,
                                     AdmissionListType = sr.Admission_List_Type_Name,
                                     DateUploaded = sr.Date_Uploaded,
                                     JambNumber = sr.Applicant_Jamb_Registration_Number,
                                     SessionName = sr.Session_Name,
                                     SessionId = sr.Session_Id
                                 }).ToList();



            }
            catch (Exception)
            {
                throw;
            }

            return admissionList.OrderBy(a => a.ApplicationNumber).ToList();
        }
        public List<AdmittedStudent> GetAdmissionListResultDetails(Programme programme, Department department, Session session)
        {
            List<AdmittedStudent> admissionList = new List<AdmittedStudent>();
            List<AdmittedStudent> masterAdmissionList = new List<AdmittedStudent>();
            try
            {
                admissionList = (from sr in repository.GetBy<VW_ADMITTED_STUDENTS_RESULT_DETAILS>(a => a.Session_Id == session.Id && a.Programme_Id == programme.Id && a.Department_Id == department.Id )
                                 select new AdmittedStudent
                                 {
                                     PersonId = sr.Person_Id,
                                     Sex = sr.Sex_Name,
                                     LocalGovernment = sr.Local_Government_Name,
                                     State = sr.State_Name,
                                     ExamNumber = sr.Exam_Number,
                                     ApplicationNumber = sr.Application_Form_Number,
                                     ProgrammeId = sr.Programme_Id,
                                     Programme = sr.Programme_Name,
                                     DepartmentId = sr.Department_Id,
                                     Department = sr.Department_Name,
                                     JambRegNumber = sr.Applicant_Jamb_Registration_Number,
                                     JambScore = Convert.ToDecimal(sr.Applicant_Jamb_Score),
                                     AdmissionListType = sr.Admission_List_Type_Name,
                                     DateUploaded = sr.Date_Uploaded.ToLongDateString(),
                                     DateSubmitted = sr.Date_Submitted.ToLongDateString(),
                                     RankedSubjects = sr.Ranked_Subjects,
                                     TotalScore = Convert.ToDecimal(sr.Total),
                                     Reason = sr.Reason,
                                     Session = sr.Session_Name,
                                     SessionId = sr.Session_Id,
                                     OLevelExamNuber = sr.O_Level_Exam_Number,
                                     OLevelYear = Convert.ToString(sr.Exam_Year),
                                     OLevelSittingId = sr.O_Level_Exam_Sitting_Id,
                                     OLevelSitting = sr.O_Level_Exam_Sitting_Name,
                                     OLevelTypeName = sr.O_Level_Type_Name,
                                     OLevelTypeShortName = sr.O_Level_Type_Short_Name,
                                     Name = sr.Name

                                 }).ToList();

                List<long> distinctStudents = admissionList.Select(d => d.PersonId).Distinct().ToList();

                OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();

                for (int i = 0; i < distinctStudents.Count; i++)
                {
                    long studentId = distinctStudents[i];

                    AdmittedStudent firstSittingDetail = admissionList.LastOrDefault(f => f.OLevelSittingId == 1 && f.PersonId == studentId);

                    if (firstSittingDetail != null)
                    {
                        firstSittingDetail.FirstSittingOLevelExamNuber = firstSittingDetail.OLevelExamNuber;
                        firstSittingDetail.FirstSittingOLevelTypeName = firstSittingDetail.OLevelTypeName + " (" + firstSittingDetail.OLevelYear + ") ";

                        List<OLevelResultDetail> firstSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.APPLICANT_O_LEVEL_RESULT.O_Level_Exam_Sitting_Id == 1 && o.APPLICANT_O_LEVEL_RESULT.Person_Id == studentId);

                        for (int j = 0; j < firstSittingResultDetails.Count; j++)
                        {
                            firstSittingDetail.FirstSittingSubjects += firstSittingResultDetails[j].Subject.Name + ": " + firstSittingResultDetails[j].Grade.Name + "; ";
                        }

                        AdmittedStudent secondSittingDetail = admissionList.LastOrDefault(f => f.OLevelSittingId == 2 && f.PersonId == studentId);
                        if (secondSittingDetail != null)
                        {
                            firstSittingDetail.SecondSittingOLevelExamNuber = secondSittingDetail.OLevelExamNuber;
                            firstSittingDetail.SecondSittingOLevelTypeName = secondSittingDetail.OLevelTypeName + " (" + secondSittingDetail.OLevelYear + ") ";

                            List<OLevelResultDetail> secondSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.APPLICANT_O_LEVEL_RESULT.O_Level_Exam_Sitting_Id == 2 && o.APPLICANT_O_LEVEL_RESULT.Person_Id == studentId);

                            for (int j = 0; j < secondSittingResultDetails.Count; j++)
                            {
                                firstSittingDetail.SecondSittingSubjects += secondSittingResultDetails[j].Subject.Name + ": " + secondSittingResultDetails[j].Grade.Name + "; ";
                            }
                        }

                        masterAdmissionList.Add(firstSittingDetail);
                    }
                    else
                    {
                        AdmittedStudent secondSittingDetail = admissionList.LastOrDefault(f => f.OLevelSittingId == 2 && f.PersonId == studentId);
                        if (secondSittingDetail != null)
                        {
                            secondSittingDetail.SecondSittingOLevelExamNuber = secondSittingDetail.OLevelExamNuber;
                            secondSittingDetail.SecondSittingOLevelTypeName = secondSittingDetail.OLevelTypeName + " (" + secondSittingDetail.OLevelYear + ") ";

                            List<OLevelResultDetail> secondSittingResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.APPLICANT_O_LEVEL_RESULT.O_Level_Exam_Sitting_Id == 2 && o.APPLICANT_O_LEVEL_RESULT.Person_Id == studentId);

                            for (int j = 0; j < secondSittingResultDetails.Count; j++)
                            {
                                secondSittingDetail.SecondSittingSubjects += secondSittingResultDetails[j].Subject.Name + ": " + secondSittingResultDetails[j].Grade.Name + "; ";
                            }
                        }

                        masterAdmissionList.Add(secondSittingDetail);
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }

            return masterAdmissionList.OrderBy(a => a.ApplicationNumber).ToList();
        }

        public List<AdmissionSummary> GetSummary(Session session)
        {
            try
            {
                List<AdmissionSummary> admissionSummary = (from a in repository.GetBy<VW_ADMISSION_SUMMARY>(a => a.Session_Id == session.Id)
                                                           select new AdmissionSummary
                                                           {
                                                               Programme_Id = a.Programme_Id,
                                                               Programme_Name = a.Programme_Name,
                                                               Department_Name = a.Department_Name,
                                                               Session_Name = a.Session_Name,
                                                               AdmissionCount = (int)a.AdmissionCount,
                                                           }).ToList();

                return admissionSummary;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(AdmissionList admissionlist, AdmissionListAudit admissionListAudit)
        {

            try
            {
                AdmissionListAuditLogic auditLogic = new AdmissionListAuditLogic();

                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Admission_List_Id == admissionlist.Id;
                ADMISSION_LIST List = GetEntityBy(selector);

                if (List != null && List.Admission_List_Id > 0)
                {
                    if (admissionlist.Deprtment != null)
                    {
                        List.Department_Id = admissionlist.Deprtment.Id;
                    }
                    if (admissionlist.DepartmentOption != null && admissionlist.DepartmentOption.Id > 0)
                    {
                        List.Department_Option_Id = admissionlist.DepartmentOption.Id;
                    }

                    List.Activated = admissionlist.Activated;

                    int modifiedRecordCount = Save();

                    if (modifiedRecordCount > 0)
                    {
                        admissionListAudit.AdmissionList = admissionlist;
                        admissionListAudit.AdmissionList.Id = List.Admission_List_Id;
                        admissionListAudit.Form = admissionlist.Form;
                        admissionListAudit.Deprtment = admissionlist.Deprtment;
                        admissionListAudit.DepartmentOption = admissionlist.DepartmentOption;
                        admissionListAudit.Time = DateTime.Now;
                        auditLogic.Create(admissionListAudit);

                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                throw;
            }

            return false;
        }
        public bool ModifyListOnly(AdmissionList admissionlist)
        {
            try
            {
                Expression<Func<ADMISSION_LIST, bool>> selector = a => a.Admission_List_Id == admissionlist.Id;
                ADMISSION_LIST List = GetEntityBy(selector);

                if (List != null && List.Admission_List_Id > 0)
                {
                    if (admissionlist.Deprtment != null)
                    {
                        List.Department_Id = admissionlist.Deprtment.Id;
                    }
                    if (admissionlist.DepartmentOption != null)
                    {
                        List.Department_Id = admissionlist.DepartmentOption.Id;
                    }
                    if (admissionlist.Form != null)
                    {
                        List.Application_Form_Id = admissionlist.Form.Id;
                    }

                    List.Activated = admissionlist.Activated;

                    int modifiedRecordCount = Save();

                    if (modifiedRecordCount > 0)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
