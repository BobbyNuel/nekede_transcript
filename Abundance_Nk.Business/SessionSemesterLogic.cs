﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Translator;
using System.Linq.Expressions;

namespace Abundance_Nk.Business
{
    public class SessionSemesterLogic : BusinessBaseLogic<SessionSemester, SESSION_SEMESTER>
    {
        private CurrentSessionSemesterLogic currentSessionSemesterLogic;

        public SessionSemesterLogic()
        {
            translator = new SessionSemesterTranslator();
            currentSessionSemesterLogic = new CurrentSessionSemesterLogic();
        }
        public SessionSemester GetBy(int id)
        {
            try
            {
                Expression<Func<SESSION_SEMESTER, bool>> selector = s => s.Session_Semester_Id == id;
                return base.GetModelBy(selector);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Modify(SessionSemester sessionSemester)
        {
            try
            {
                Expression<Func<SESSION_SEMESTER, bool>> selector = s => s.Session_Semester_Id == sessionSemester.Id;
                SESSION_SEMESTER entity = GetEntityBy(selector);

                if (entity == null)
                {
                    throw new Exception(NoItemFound);
                }

                entity.Session_Id = sessionSemester.Session.Id;
                entity.Semester_Id = sessionSemester.Semester.Id;
                entity.Start_Date = sessionSemester.StartDate;
                entity.End_Date = sessionSemester.EndDate;

                int modifiedRecordCount = Save();
                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }





        //public override bool Remove(Func<SESSION_TERM, bool> selector)
        //{
        //    bool suceeded = base.Remove(selector);
        //    repository.SaveChanges();
        //    return suceeded;
        //}

        //public SessionTerm GetCurrent()
        //{
        //    try
        //    {
        //       CurrentSessionTerm currentTerm = currentSessionTermLogic.GetCurrentSessionTerm();
        //       return currentTerm.SessionTerm;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public SessionTerm GetThirdTermBy(Session session)
        //{
        //    try
        //    {
        //        Func<SESSION_TERM, bool> selector = s => s.Session_Id == session.Id && s.Term_Id == 3;
        //        return base.GetModelBy(selector);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public List<SessionTerm> GetBy(Session session)
        //{
        //    try
        //    {
        //        Func<SESSION_TERM, bool> selector = s => s.Session_Id == session.Id;
        //        return base.GetModelsBy(selector);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public SessionTerm GetBy(Session session, Term term)
        //{
        //    try
        //    {
        //        Func<SESSION_TERM, bool> selector = s => s.Session_Id == session.Id && s.Term_Id == term.Id;
        //        return base.GetModelBy(selector);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        //public SessionTerm GetBy(int id)
        //{
        //    try
        //    {
        //        Func<SESSION_TERM, bool> selector = s => s.Session_Term_Id == id;
        //        return base.GetModelBy(selector);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public int GetNoOfTimesSchoolOpenedBy(Session session)
        //{
        //    try
        //    {
        //        Func<SESSION_TERM, bool> selector = s => s.Session_Id == session.Id;
        //        List<SessionTerm> sessionTerms = base.GetModelsBy(selector);

        //        return sessionTerms.Sum(st => st.NoOfTimesSchoolOpened).GetValueOrDefault();
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
               
        //public List<Session> Get()
        //{
        //    try
        //    {
        //        var sessions = (from c in repository.Fetch<VW_SESSION_TERM>()
        //                        select new Session
        //                       {
        //                           Id = c.Session_Term_Id,
        //                           Name = c.Session_Name + " - " + c.Term_Name,
        //                       }).OrderBy(c => c.Name).ToList();

        //        return sessions;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}




    }
}
