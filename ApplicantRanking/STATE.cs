//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApplicantRanking
{
    using System;
    using System.Collections.Generic;
    
    public partial class STATE
    {
        public STATE()
        {
            this.LOCAL_GOVERNMENT = new HashSet<LOCAL_GOVERNMENT>();
            this.OLD_PORTAL_ISSUES = new HashSet<OLD_PORTAL_ISSUES>();
            this.PERSON = new HashSet<PERSON>();
            this.PERSON_AUDIT = new HashSet<PERSON_AUDIT>();
            this.PERSON_AUDIT1 = new HashSet<PERSON_AUDIT>();
            this.TRANSCRIPT_REQUEST = new HashSet<TRANSCRIPT_REQUEST>();
        }
    
        public string State_Id { get; set; }
        public string State_Name { get; set; }
        public int Nationality_Id { get; set; }
    
        public virtual ICollection<LOCAL_GOVERNMENT> LOCAL_GOVERNMENT { get; set; }
        public virtual NATIONALITY NATIONALITY { get; set; }
        public virtual ICollection<OLD_PORTAL_ISSUES> OLD_PORTAL_ISSUES { get; set; }
        public virtual ICollection<PERSON> PERSON { get; set; }
        public virtual ICollection<PERSON_AUDIT> PERSON_AUDIT { get; set; }
        public virtual ICollection<PERSON_AUDIT> PERSON_AUDIT1 { get; set; }
        public virtual ICollection<TRANSCRIPT_REQUEST> TRANSCRIPT_REQUEST { get; set; }
    }
}
