﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;

namespace ApplicantRanking
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRankAll_Click(object sender, EventArgs e)
        {
            try
            {
                AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();

                List<AppliedCourse> appliedCourses = appliedCourseLogic.GetModelsBy(a => a.Department_Id == 2 && a.Programme_Id == 1 && a.APPLICATION_FORM.APPLICATION_FORM_SETTING.Session_Id == 3);

                if (appliedCourses != null && appliedCourses.Count > 0)
                {
                    for (int i = 0; i < appliedCourses.Count; i++)
                    {
                        AppliedCourse appliedCourse = appliedCourses[i];

                        RankingDataLogic rankingDataLogic = new RankingDataLogic();

                        string rejectReason = rankingDataLogic.RankStudent(appliedCourse);
                    }

                    lblMessage.Text = "Success";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
    }
}
