﻿namespace ApplicantRanking
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRankAll = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnRankAll
            // 
            this.btnRankAll.Location = new System.Drawing.Point(211, 133);
            this.btnRankAll.Name = "btnRankAll";
            this.btnRankAll.Size = new System.Drawing.Size(297, 163);
            this.btnRankAll.TabIndex = 0;
            this.btnRankAll.Text = "RANK ALL";
            this.btnRankAll.UseVisualStyleBackColor = true;
            this.btnRankAll.Click += new System.EventHandler(this.btnRankAll_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(113, 359);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(496, 61);
            this.lblMessage.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 501);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnRankAll);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnRankAll;
        private System.Windows.Forms.Label lblMessage;
    }
}

