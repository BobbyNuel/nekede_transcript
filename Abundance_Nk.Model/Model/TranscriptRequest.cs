﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class TranscriptRequest
    {
        public Int64 Id { get; set; }
        public Student student { get; set; }
        public Payment payment {get; set;}
        public OnlinePayment OnlinePayment { get;set; }
        [Display(Name = "Date of Request")]
        public DateTime DateRequested {get; set;}
        [Display(Name="Destination Address")]
        public string DestinationAddress {get; set;}
        [Display(Name = "Destination Address State")]
        public State DestinationState {get; set;}
        [Display(Name = "Destination Address Country")]
        public Country DestinationCountry { get; set; }
        public TranscriptClearanceStatus transcriptClearanceStatus {get; set;}
        public TranscriptStatus transcriptStatus {get; set;}
        public string ConfirmationOrderNumber { get; set; }
        public string Amount { get; set; }
        public RemitaPayment remitaPayment { get; set; }
        public PaymentInterswitch PaymentInterswitch { get; set; }
        public string Receiver { get; set; }
        public string RequestType { get; set; }
        [Required]
        [Display(Name = "Alumni Fee Receipt")]
        public string AlumniReceiptUrl { get; set; }
        [Required]
        [Display(Name = "Statement of Result")]
        public string StatementOfResultUrl { get; set; }
        public string MyFile { get; set; }
        public string Hash { get; set; }
        public string ReturnUrl { get; set; }
        public int AmountInKobo { get; set; }
        public bool? Verified { get; set; }
        public bool? Processed { get; set; }
        public bool VerificationStatus { get; set; }
        public string DepartmentName { get; set; }
        public string ProgrammeName { get; set; }
        public long TransciptIdentifier { get; set; }
    }
}
