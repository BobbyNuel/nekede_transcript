﻿using System.Collections.Generic;
namespace Abundance_Nk.Model.Model
{
    public class Course
    {
        public long Id { get; set; }
        public CourseType Type { get; set; }
        public string Name { get; set; }
        public Level Level { get; set; }
        public Department Department { get; set; }
        public DepartmentOption DepartmentOption { get; set; }
        public Session Session { get; set; }
        public int Unit { get; set; }
        public Semester Semester { get; set; }
        public string Code { get; set; }
        public bool IsRegistered { get; set; }
        public bool isCarryOverCourse { get; set; }
        public bool? Activated { get; set; }
        public decimal TestScore { get; set; }
        public decimal ExamScore { get; set; }
        public string Grade { get; set; }
        public int? SessionId { get; set; }
        

        public bool IsAlreadyRegistered(List<CourseRegistrationDetail> regDetails)
        {
            try
            {
                if (RegId <= 0 || regDetails.Count <= 0)
                {
                    return false;
                }

                bool registered = false;

                regDetails.ForEach(c =>
                {
                    if (c.Id == RegId)
                        registered = true;
                });

                return registered;
            }
            catch (System.Exception)
            {
                throw;
            }
        }

        public long RegId { get; set; }
    }
}
