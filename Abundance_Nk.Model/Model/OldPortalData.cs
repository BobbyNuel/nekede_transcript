﻿using System;

namespace Abundance_Nk.Model.Model
    {
    public class OldPortalData
        {
        public long Id { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string Fullname { get; set; }
        public Department Department { get; set; }
        public Programme Programme { get; set; }
        public string ApplicationNumber { get; set; }
        public string Pin { get; set; }
        public State State { get; set; }
        public LocalGovernment LocalGovernment { get; set; }
        public User User { get; set; }
        public DateTime DateAdded { get; set; }
        public string Session { get; set; }
        }
    }
