﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class StudentAcademicInformation
    {
        public Student Student { get; set; }
        public ModeOfEntry ModeOfEntry { get; set; }
        public ModeOfStudy ModeOfStudy { get; set; }

        [Display(Name = "Year of Admission")]
        public string YearOfAdmission { get; set; }

        [Display(Name = "Year of Graduation")]
        public int YearOfGraduation { get; set; }
        public Level Level { get; set; }
        public DateTime? DateOfEntry { get; set; }
        public string LastSchoolAttended { get; set; }
        public string StudentAward { get; set; }
        public DateTime? AwardDate { get; set; }
        public decimal? CGPA { get; set; }
        public string MonthOfGraduation { get; set; }
        public bool? IsModified { get; set; }
    }
}
