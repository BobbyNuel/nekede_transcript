﻿namespace Abundance_Nk.Model.Model
{
    public class AdmissionList
    {
        public long Id { get; set; }
        public ApplicationForm Form { get; set; }
        public AdmissionListBatch Batch { get; set; }
        public Department Deprtment { get; set; }
        public DepartmentOption DepartmentOption { get; set; }
        public Programme Programme { get; set; }
        public Session Session { get; set; }
        public bool? Activated { get; set; }

        public bool Deactivated { get; set; }

        public bool ActivateAlt { get; set; }
    }

    public class UnregisteredStudent
    {
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Othername { get; set; }
        public string JambNumberNumber { get; set; }
        public Department Deprtment { get; set; }
        public Programme Programme { get; set; }
        public Session Session { get; set; }
    }


}
