﻿using System;

namespace Abundance_Nk.Model.Model
{
   public class venueDesignation
    {
        public int start { get; set; }
        public int stop { get; set; }
        public string venue { get; set; }
        public Department Deprtment { get; set; }
        public DateTime ExamDate { get; set; }
    }
}
