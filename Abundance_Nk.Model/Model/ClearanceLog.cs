﻿using System;

namespace Abundance_Nk.Model.Model
{
    public class ClearanceLog 
    {
        public long ClearanceId { get; set; }
        public ApplicationForm applicationForm { get; set; }
        public User User { get; set; }
        public bool status { get; set; }
        public DateTime Date { get; set; }
    }
}
