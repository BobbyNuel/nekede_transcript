﻿namespace Abundance_Nk.Model.Model
{
    public class Invoice
    {
        public Person Person { get; set; }
        public Payment Payment { get; set; }
        public string JambRegistrationNumber { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
        public PaymentEtranzactType paymentEtranzactType { get; set; }

        public RemitaPayment remitaPayment { get; set; }
        public decimal Amount { get; set; }
        public string MatricNumber { get; set; }
        public bool Paid { get; set; }
    }



}
