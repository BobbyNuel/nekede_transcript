﻿using System.Collections.Generic;

namespace Abundance_Nk.Model.Model
{
    public class RoomSetting
    {
        public HostelRoom HostelRoom { get; set; }
        public List<HostelRoomCorner> HostelRoomCorners { get; set; }
    }
}