﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class SemesterReport
    {
        public string CourseCode { get; set; }
        public string CourseTitle { get; set; }
        public int CourseUnit { get; set; }
        public string GradeScore { get; set; }
        public decimal GradePoint { get; set; }
        public string Grade { get; set; }
        public decimal GPA { get; set; }
        public decimal CGPA { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public int Total { get; set; }
        public decimal GradePoint_CourseUnit { get; set; }
        public string DeptCourse { get; set; }
        public int FirstSemester { get; set; }
        public int SecondSemester { get; set; }
        public int SessionId { get; set; }
        public int SessionSemesterId { get; set; }
    }
}
