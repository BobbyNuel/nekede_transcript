﻿namespace Abundance_Nk.Model.Model
{
    public enum SortOption
    {
        Name = 1,
        ExamNo = 2,
        ApplicationNo = 3
    }
}
