﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class PersonGuardian
    {
        public long Id { get; set; }
        public Person Person { get; set; }
        public string GuardianName { get; set; }
        public string Address { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string Occupation { get; set; }
    }
}
