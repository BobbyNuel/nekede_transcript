﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class SecurityQuestion : Setup
    {
        [Required]
        [Display(Name="Security Question")]
        public override int Id { get; set; }

        public override string Name { get; set; }
    }
}
