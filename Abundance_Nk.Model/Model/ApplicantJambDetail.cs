﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class ApplicantJambDetail
    {
        public Person Person { get; set; }
        [Required]
        [Display(Name = "JAMB Registration No")]
        [RegularExpression("^(7)([0-9]{7}[A-Z]{2})$", ErrorMessage = "JAMB Registration No is not valid")]
        public string JambRegistrationNumber { get; set; }
        [Display(Name = "JAMB Score")]
        public short? JambScore { get; set; }
        public InstitutionChoice InstitutionChoice { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
        [Display(Name = "First Subject")]
        public OLevelSubject JambSubject1 { get; set; }
        [Display(Name = "Second Subject")]
        public OLevelSubject JambSubject2 { get; set; }
        [Display(Name = "Third Subject")]
        public OLevelSubject JambSubject3 { get; set; }
        [Display(Name = "Fourth Subject")]
        public OLevelSubject JambSubject4 { get; set; }
    }



}
