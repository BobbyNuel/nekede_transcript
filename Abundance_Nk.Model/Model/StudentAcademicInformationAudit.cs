﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class StudentAcademicInformationAudit
    {
        public long Id { get; set; }
        public decimal OldCgpa { get; set; }
        public decimal NewCgpa { get; set; }
        public long UserId { get; set; }
        public string Operation { get; set; }
        public string Action { get; set; }
        public string Client { get; set; }
        public System.DateTime Time { get; set; }

        public StudentAcademicInformation StudentAcademicInformation { get; set; }
    }
}
