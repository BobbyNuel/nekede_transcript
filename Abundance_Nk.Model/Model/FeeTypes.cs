﻿namespace Abundance_Nk.Model.Model
{
	public enum FeeTypes
	{
		ApplicationForm = 1,
		AcceptanceFee = 2,
		SchoolFees = 3,
		PutmeForm = 4,
		ResultChecking = 5,
		SupplementaryForm = 9,
		SupplementaryAcceptanceFee = 10,
		TranscriptLocal = 11,
		TranscriptForeign = 12,
		HostelFee = 13
	}

	public enum Programmes
	{
		NDMorning = 5,
		NDEvening = 1,
		NDWeekend = 2,
        HNDFullTime = 3,
        HNDEvening = 4,
        HNDWeekend = 6
	}


}
