﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class FeeType : BasicSetup
    {
        public override int Id { get; set; }

        [Required]
        [Display(Name = "Fee Type")]
        public override string Name { get; set; }

        


    }

}
