﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class ResultGrade : Setup
    {
        [Display(Name = "Result Grade")]
        public override int Id { get; set; }

        [Display(Name = "Result Grade")]
        public override string Name { get; set; }
    }



}
