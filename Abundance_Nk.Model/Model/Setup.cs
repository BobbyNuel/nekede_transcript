﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public abstract class Setup
    {
        [Required]
        
        public virtual int Id { get; set; }
                
        public virtual string Name { get; set; }
    }    
}
