﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class Session : Setup
    {
        [Display(Name = "Session")]
        public override int Id { get; set; }

        [Display(Name = "Session")]
        public override string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Activated { get; set; }

    }
}
