﻿using System.Collections.Generic;

namespace Abundance_Nk.Model.Model
{
    public class PaymentHistory
    {
        public Student Student { get; set; }
        public List<PaymentView> Payments { get; set; }
    }
}
