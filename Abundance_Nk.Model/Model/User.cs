﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class User 
    {
        public long Id { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public SecurityQuestion SecurityQuestion { get; set; }
        [Required]
        [Display (Name="Security Answer")]
        public string SecurityAnswer { get; set; }
        [Required]
        public Role Role { get; set; }
        public DateTime LastLoginDate { get; set; }
        [Required]
        public Person person { get; set; }

        public bool? Activated { get; set; }
    }


}
