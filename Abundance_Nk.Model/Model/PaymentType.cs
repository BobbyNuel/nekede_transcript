﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class PaymentType : BasicSetup
    {
        [Required]
        [Display(Name = "Payment Type")]
        public override string Name { get; set; }
    }
}
