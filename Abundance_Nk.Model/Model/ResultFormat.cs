﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class ResultFormat
    {
        public ResultFormat()
        {
            ResultSpecialCaseMessages = new ResultSpecialCaseMessages();
        }
        public int SN { get; set; }
        public string MATRICNO { get; set; }
        public string NAME { get; set; }
        public decimal T_EX { get; set; }
        public decimal T_CA { get; set; }
        public string fileUploadUrl { get; set; }
        public ResultSpecialCaseMessages ResultSpecialCaseMessages { get; set; }
    }

    public class ResultManageFormat
    {
        public ResultManageFormat()
        {
            ResultSpecialCaseMessages = new ResultSpecialCaseMessages();
        }
        public int SN { get; set; }
        public string MATRICNO { get; set; }
        public decimal Test_Score { get; set; }
        public decimal Exam_Score { get; set; }
        public string Programme { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string Course_Code { get; set; }
         public ResultSpecialCaseMessages ResultSpecialCaseMessages { get; set; }
        public string fileUploadUrl { get; set; }
    }

    public class StudentResultFormat
    {
        public StudentResultFormat()
        {
            ResultSpecialCaseMessages = new ResultSpecialCaseMessages();
        }
        public int SN { get; set; }
        public string NAME { get; set; }
        public string MATRICNO { get; set; }
        public string Grade { get; set; }
        public string Programme { get; set; }
        public string Department { get; set; }
        public string Level { get; set; }
        public string Session { get; set; }
        public string Semester { get; set; }
        public string Course_Code { get; set; }
        public int CourseUnit { get; set; }
        public ResultSpecialCaseMessages ResultSpecialCaseMessages { get; set; }
        public string fileUploadUrl { get; set; }
    }
}
