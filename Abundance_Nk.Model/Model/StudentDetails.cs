﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class StudentDetails
    {
        public string FullName { get; set; }
        public string MatricNumber { get; set; }
        public string CourseCode { get; set; }
        public string Score { get; set; }
        public string Grade { get; set; }
        public string Unit { get; set; }
        public string Level { get; set; }
        public string Department { get; set; }
        public string Program { get; set; }
        public string ApplicationForm { get; set; }
        public string ExamNumber { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string Session { get; set; }
    }
}
