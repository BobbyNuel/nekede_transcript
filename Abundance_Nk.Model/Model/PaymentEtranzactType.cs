﻿using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Model.Model
{
    public class PaymentEtranzactType : Setup
    {
        public override int Id { get; set; }
       
        [Display(Name = "Payment Type")]
        public override string  Name { get; set; }
        public FeeType FeeType { get; set; }
        public Session Session { get; set; }

    }


}
