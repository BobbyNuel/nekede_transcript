﻿using System;

namespace Abundance_Nk.Model.Model
{
    public class SessionSemester
    {
        public int Id { get; set; }
        public Session Session { get; set; }
        public Semester Semester { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public string Name { get; set; }
        public int SequenceNumber { get; set; }
    }



}
