﻿using System;

namespace Abundance_Nk.Model.Model
{
    public class StudentNdResultDetail
    {
        public int Id { get; set; }
        public StudentNdResult Result { get; set; }
        public Course Course { get; set; }
        public ScoreGrade Grade { get; set; }
        public DateTime DateObtained { get; set; }

    }


}
