﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public enum ScreeningSetting
    {
        
        FirstSittingScore = 10,
        SecondSittingScore = 3,
        GradeAScore = 6,
        GradeBScore = 4,
        GradeCScore = 3,
        JambBaseScore = 20,
        JambBaseScoreStart = 180
       
    }
}
