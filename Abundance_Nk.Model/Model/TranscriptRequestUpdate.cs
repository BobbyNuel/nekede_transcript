﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class TranscriptRequestUpdate
    {
        public int Id { get; set; }
        public System.DateTime LastUpdateOn { get; set; }
        public int Count { get; set; }
        public long LastRequestId { get; set; }
    }
}
