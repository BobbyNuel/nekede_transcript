﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class PaymentInterswitch
    {
      public Payment Payment { get; set; }
      public int  Amount { get; set; }
      public string  CardNumber { get; set; }
      public string  MerchantReference { get; set; }
      public string  PaymentReference { get; set; }
      public string  RetrievalReferenceNumber { get; set; }
      public string  LeadBankCbnCode { get; set; }
      public string  LeadBankName { get; set; }
      public string[]  SplitAccounts { get; set; }
      public DateTime  TransactionDate { get; set; }
      [Display(Name = "Response Code")]
      public string  ResponseCode { get; set; }
      [Display(Name = "Response Code Description")]
      public string  ResponseDescription { get; set; }

      public string RemitaOrderId { get; set; }
    }
}
