﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class AdmittedStudent
    {
        public long PersonId { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string LocalGovernment { get; set; }
        public string State { get; set; }
        public string ApplicationNumber { get; set; }
        public string ExamNumber { get; set; }
        public string Department { get; set; }
        public int DepartmentId  { get; set; }
        public string Programme { get; set; }
        public int ProgrammeId { get; set; }
        public string Session { get; set; }
        public int SessionId { get; set; }
        public string DateUploaded { get; set; }
        public string AdmissionListType { get; set; }
        public string DateSubmitted { get; set; }
        public string JambRegNumber { get; set; }
        public decimal JambScore { get; set; }
        public string RankedSubjects { get; set; }
        public decimal TotalScore { get; set; }
        public bool Qualified { get; set; }
        public string Reason { get; set; }
        public string OLevelExamNuber { get; set; }
        public string OLevelYear { get; set; }
        public int OLevelSittingId { get; set; }
        public string OLevelSitting { get; set; }
        public string OLevelTypeName { get; set; }
        public string OLevelTypeShortName { get; set; }
        public string FirstSittingOLevelExamNuber { get; set; }
        public string FirstSittingOLevelTypeName { get; set; }
        public string FirstSittingSubjects { get; set; }
        public string SecondSittingOLevelExamNuber { get; set; }
        public string SecondSittingOLevelTypeName { get; set; }
        public string SecondSittingSubjects { get; set; }
    }
}
