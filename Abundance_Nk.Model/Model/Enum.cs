﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class Enum
    {

    }
    public enum ExamSittings
    {
        FirstSitting = 1,
        SecondSitting = 2
    }
    public enum PaymentTypeEnum
    {
        CardPayment = 1,
        OnlinePayment = 2,
    }
    public enum Levels
    {
        NDI = 1,
        NDII = 2,
        HNDI = 3,
        HNDII = 4
    }
    public enum PersonTypes
    {
        Staff = 1,
        Parent = 2,
        Student = 3,
        Applicant = 4
    }
}
