﻿namespace Abundance_Nk.Model.Model
{
    public class ApplicationFormSummary
    {
        public int ProgrammeId { get; set; }
        public string ProgrammeName { get; set; }
        public string DepartmentName { get; set; }
        public string SessionName { get; set; }
        public int FormCount { get; set; }
    }
}
