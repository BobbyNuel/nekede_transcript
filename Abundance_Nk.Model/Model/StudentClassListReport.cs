﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class StudentClassListReport
    {
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public decimal? CGPA { get; set; }
        public double GradePoint_CourseUnit { get; set; }

    }
}
