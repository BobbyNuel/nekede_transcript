﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Abundance_Nk.Model.Model
{
    public class ApplicantData
    {
        public string Name { get; set; }
        public string JambRegNumber { get; set; }
        public string ApplicationNumber { get; set; }
        public string Department { get; set; }
        public string Session { get; set; }
        public string RankingScore { get; set; }
        public string JambScore { get; set; }
        public string Subject1 { get; set; }
        public string Subject2 { get; set; }
        public string Subject3 { get; set; }
        public string Subject4 { get; set; }
        public string Subject5 { get; set; }
        public int SessionId { get; set; }
        public long DepartmentId { get; set; }
        public long ProgrammeId { get; set; }
        public string ExamNumber { get; set; }
        public string Programme { get; set; }
        public string DepartmentCode { get; set; }
        public int Qualified { get; set; }
        public string QualifiedStatus { get; set; }
        public decimal RankingScoreInt { get; set; }
        public int AcceptancCount { get; set; }
        public int AdmittanceCount { get; set; }
        public int ApplicationCount { get; set; }
        public string JambSubjects { get; set; }
    }
}
