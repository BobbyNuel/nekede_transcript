﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class FeeDetailTranslator : TranslatorBase<FeeDetail, FEE_DETAIL>
    {
        private FeeTranslator feeTranslator;
        private FeeTypeTranslator feeTypeTranslator;
        private SessionTranslator sessionTranslator;

        public FeeDetailTranslator()
        {
            feeTranslator = new FeeTranslator();
            feeTypeTranslator = new FeeTypeTranslator();
            sessionTranslator = new SessionTranslator();
        }

        public override FeeDetail TranslateToModel(FEE_DETAIL entity)
        {
            try
            {
                FeeDetail model = null;
                if (entity != null)
                {
                    model = new FeeDetail();
                    model.Id = entity.Fee_Detail_Id;
                    model.Fee = feeTranslator.Translate(entity.FEE);
                    model.FeeType = feeTypeTranslator.Translate(entity.FEE_TYPE);
                    if (entity.SESSION != null)
                    {
                        model.Session = sessionTranslator.Translate(entity.SESSION);
                    }
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override FEE_DETAIL TranslateToEntity(FeeDetail model)
        {
            try
            {
                FEE_DETAIL entity = null;
                if (model != null)
                {
                    entity = new FEE_DETAIL();
                    entity.Fee_Detail_Id = model.Id;
                    entity.Fee_Id = model.Fee.Id;
                    entity.Fee_Type_Id = model.FeeType.Id;
                    if (model.Session != null)
                    {
                        entity.Session_Id = model.Session.Id;
                    }
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }



    }

}
