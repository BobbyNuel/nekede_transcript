﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class ClearanceLogTranslator : TranslatorBase<ClearanceLog, CLEARANCE_LOG>
    {
        private UserTranslator userTranslator;
        private ApplicationFormTranslator applicationFormTranslator;

        public ClearanceLogTranslator()
        {
            userTranslator = new UserTranslator();
            applicationFormTranslator = new ApplicationFormTranslator();
        }
        public override ClearanceLog TranslateToModel(CLEARANCE_LOG entity)
        {
            try
            {
                ClearanceLog model = null;
                if (entity != null)
                {
                    model = new ClearanceLog();
                    model.ClearanceId = entity.ClearanceId;
                    model.Date = entity.Date;
                    model.status = entity.Status;
                    model.User = userTranslator.Translate(entity.USER);
                    model.applicationForm = applicationFormTranslator.Translate(entity.APPLICATION_FORM);
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override CLEARANCE_LOG TranslateToEntity(ClearanceLog model)
        {
            try
            {
                CLEARANCE_LOG entity = null;
                if (model != null)
                {
                    entity = new CLEARANCE_LOG();
                    entity.Application_Form_Id = model.applicationForm.Id;
                    entity.ClearanceId = model.ClearanceId;
                    entity.Date = model.Date;
                    entity.Status = model.status;
                    entity.User_Id = model.User.Id;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }



    }
}
