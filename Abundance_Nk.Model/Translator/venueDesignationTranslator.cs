﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class venueDesignationTranslator : TranslatorBase<venueDesignation, VENUE_DESIGNATION>
    {
        private DepartmentTranslator departmentTranslator;
        public venueDesignationTranslator()
        {
            departmentTranslator = new DepartmentTranslator();
        }


        public override venueDesignation TranslateToModel(VENUE_DESIGNATION entity)
        {
            try
            {
                venueDesignation model = null;
                if (entity != null)
                {
                    model = new venueDesignation();
                    model.start = entity.start;
                    model.stop = entity.stop;
                    model.venue = entity.venue;
                    model.Deprtment = departmentTranslator.Translate(entity.DEPARTMENT);
                    model.ExamDate = entity.Exam_date;
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override VENUE_DESIGNATION TranslateToEntity(venueDesignation model)
        {
            try
            {
                VENUE_DESIGNATION entity = null;
                if (model != null)
                {
                    entity = new VENUE_DESIGNATION();
                    entity.start = model.start;
                    entity.stop = model.stop;
                    entity.venue = model.venue;
                    entity.Department_Id = model.Deprtment.Id;
                    entity.Exam_date = model.ExamDate;

                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }




    }
}
