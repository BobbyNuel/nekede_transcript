﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class SessionTranslator : TranslatorBase<Session, SESSION>
    {
        public override Session TranslateToModel(SESSION sessionEntity)
        {
            try
            {
                Session session = null;
                if (sessionEntity != null)
                {
                    session = new Session();
                    session.Id = sessionEntity.Session_Id;
                    session.Name = sessionEntity.Session_Name;
                    session.StartDate = sessionEntity.Start_Date;
                    session.EndDate = sessionEntity.End_date;
                }

                return session;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override SESSION TranslateToEntity(Session session)
        {
            try
            {
                SESSION sessionEntity = null;
                if (session != null)
                {
                    sessionEntity = new SESSION();
                    sessionEntity.Session_Name = session.Name;
                    sessionEntity.Start_Date = session.StartDate;
                    sessionEntity.End_date = session.EndDate;
                }

                return sessionEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }





    }
}
