﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
    {
        public class OldportalDataTranslator : TranslatorBase<OldPortalData, OLD_PORTAL_ISSUES>
        {
            private DepartmentTranslator departmentTranslator;
            private ProgrammeTranslator programmeTranslator;
            private StateTranslator stateTranslator;
            private LocalGovernmentTranslator localGovernmentTranslator ;
            private UserTranslator userTranslator;

            public OldportalDataTranslator()
            {
                departmentTranslator = new DepartmentTranslator();
                programmeTranslator =  new ProgrammeTranslator();
                stateTranslator = new StateTranslator();
                localGovernmentTranslator = new LocalGovernmentTranslator();
                userTranslator = new UserTranslator();
            }
            public override OldPortalData TranslateToModel(OLD_PORTAL_ISSUES entity)
            {
                try
                {
                    OldPortalData model = null;
                    if (entity != null)
                    {
                        model = new OldPortalData();
                        model.Id = entity.Id;
                        model.Surname = entity.Surname;
                        model.FirstName = entity.First_Name;
                        model.OtherName = entity.Other_Name;
                        model.ApplicationNumber = entity.Application_Number;
                        model.Pin = entity.Pin_Used;
                        model.LocalGovernment = localGovernmentTranslator.Translate(entity.LOCAL_GOVERNMENT);
                        model.State = stateTranslator.Translate(entity.STATE);
                        model.Department = departmentTranslator.Translate(entity.DEPARTMENT);
                        model.Programme = programmeTranslator.Translate(entity.PROGRAMME);
                        model.User = userTranslator.Translate(entity.USER);
                        model.DateAdded = entity.Date_Added;
                        model.Fullname = entity.Surname + " " + entity.First_Name + " " + entity.Other_Name;
                        model.Session = entity.Session_Name;
                    }

                    return model;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public override OLD_PORTAL_ISSUES TranslateToEntity(OldPortalData model)
            {
                try
                {
                    OLD_PORTAL_ISSUES entity = null;
                    if (model != null)
                    {
                        entity = new OLD_PORTAL_ISSUES ( );
                        entity.Id = model.Id;
                        entity.Surname = model.Surname;
                        entity.First_Name = model.FirstName;
                        entity.Other_Name = model.OtherName;
                        entity.Application_Number = model.ApplicationNumber;
                        entity.Pin_Used = model.Pin;
                        entity.Date_Added = model.DateAdded;
                        entity.Programme_Id = model.Programme.Id;
                        entity.Department_Id = model.Department.Id;
                        entity.State_Id = model.State.Id;
                        entity.LGA_Id = model.LocalGovernment.Id;
                        entity.User_Id = model.User.Id;
                        entity.Session_Name = model.Session;

                    }

                    return entity;
                }
                catch (Exception)
                {
                    throw;
                }
            }

        }
    }
