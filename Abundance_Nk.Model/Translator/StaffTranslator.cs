﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Model.Translator
{
    public class StaffTranslator : TranslatorBase<Staff, STAFF>
    {
        private StaffTypeTranslator staffTypeTranslator;
        private MaritalStatusTranslator maritalStatusTranslator;

        public StaffTranslator()
        {
            staffTypeTranslator = new StaffTypeTranslator();
            maritalStatusTranslator = new MaritalStatusTranslator();
        }

        public override Staff TranslateToModel(STAFF entity)
        {
            try
            {
                Staff model = null;
                if (entity != null)
                {
                    model = new Staff();
                    model.Id = entity.Staff_Id;
                    model.Type = staffTypeTranslator.Translate(entity.STAFF_TYPE);
                    model.MaritalStatus = maritalStatusTranslator.Translate(entity.MARITAL_STATUS);
                    model.ProfileDescription = entity.Profile_Description;
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override STAFF TranslateToEntity(Staff model)
        {
            try
            {
                STAFF entity = null;
                if (model != null)
                {
                    entity = new STAFF();
                    entity.Staff_Id = model.Id;
                    entity.Staff_Type_Id = model.Type.Id;
                    entity.Marital_Status_Id = model.MaritalStatus.Id;
                    entity.Profile_Description = model.ProfileDescription;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }


    }

}
