﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class NdPutmeResultTranslator : TranslatorBase<NdPutmeResult,PUTME_RESULT>
    {
       private PersonTranslator personTranslator;
       private SessionTranslator sessionTranslator;
       public NdPutmeResultTranslator()
       {
           personTranslator = new PersonTranslator();
       }

       public override NdPutmeResult TranslateToModel(PUTME_RESULT entity)
       {
           try
           {
               NdPutmeResult model = null;
               if (entity != null)
               {
                   model = new NdPutmeResult();
                   model.Id = entity.ID;
                   model.Jambscore = entity.JAMBSCORE;
                   model.Course = entity.COURSE;
                   model.ExamNo = entity.EXAMNO;
                   model.Person = personTranslator.Translate(entity.PERSON);
                   model.RegNo = entity.REGNO;
                   model.RawScore =  entity.RAW_SCORE;
                   model.Total = entity.TOTAL;
                   model.FullName = entity.FULLNAME;
                  
                   if (model.Session != null)
                   {
                     model.Session = sessionTranslator.Translate(entity.SESSION);
                   }
                  
               }

               return model;
           }
           catch (Exception)
           {
               throw;
           }
       }

       public override PUTME_RESULT TranslateToEntity(NdPutmeResult model)
       {
           try
           {
              PUTME_RESULT entity = null;
               if (model != null)
               {
                   entity = new PUTME_RESULT();
                   entity.ID = model.Id;
                   entity.JAMBSCORE = model.Jambscore;
                   entity.Person_Id = model.Person.Id;
                   entity.RAW_SCORE = model.RawScore;
                   entity.REGNO = model.RegNo;
                   entity.TOTAL = model.Total;
                   entity.COURSE = model.Course;
                   entity.EXAMNO = model.ExamNo;
                   entity.FULLNAME = model.FullName;
                   entity.SESSION.Session_Id = model.Session.Id;
               }

               return entity;
           }
           catch (Exception)
           {
               throw;
           }
       }



    }
}
