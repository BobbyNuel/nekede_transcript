﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Model.Translator
{
    public class StudentNdResultDetailTranslator : TranslatorBase<StudentNdResultDetail, STUDENT_ND_RESULT_DETAIL>
    {
        private CourseTranslator courseTranslator;
        private ScoreGradeTranslator scoreGradeTranslator;
        private StudentNdResultTranslator studentNdResultTranslator;

        public StudentNdResultDetailTranslator()
        {
            courseTranslator = new CourseTranslator();
            scoreGradeTranslator = new ScoreGradeTranslator();
            studentNdResultTranslator = new StudentNdResultTranslator();
        }

        public override StudentNdResultDetail TranslateToModel(STUDENT_ND_RESULT_DETAIL entity)
        {
            try
            {
                StudentNdResultDetail model = null;
                if (entity != null)
                {
                    model = new StudentNdResultDetail();
                    model.Id = entity.Student_Nd_Result_Detail_Id;
                    model.Result = studentNdResultTranslator.Translate(entity.STUDENT_ND_RESULT);
                    model.Course = courseTranslator.Translate(entity.COURSE);
                    model.Grade = scoreGradeTranslator.Translate(entity.SCORE_GRADE);
                    model.DateObtained = entity.Date_Obtained;
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override STUDENT_ND_RESULT_DETAIL TranslateToEntity(StudentNdResultDetail model)
        {
            try
            {
                STUDENT_ND_RESULT_DETAIL entity = null;
                if (model != null)
                {
                    entity = new STUDENT_ND_RESULT_DETAIL();
                    entity.Student_Nd_Result_Detail_Id = model.Id;
                    entity.Person_Id = model.Result.Student.Id;
                    entity.Course_Id = model.Course.Id;
                    entity.Grade_Id= model.Grade.Id;
                    entity.Date_Obtained = model.DateObtained;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }



    }


}
