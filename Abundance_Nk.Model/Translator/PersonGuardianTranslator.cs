﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Model.Translator
{
    public class PersonGuardianTranslator : TranslatorBase<PersonGuardian, PERSON_GUARDIAN>
    {
        private PersonTranslator personTranslator;

        public PersonGuardianTranslator()
        {
            personTranslator = new PersonTranslator();
        }

        public override PersonGuardian TranslateToModel(PERSON_GUARDIAN entity)
        {
            try
            {
                PersonGuardian model = null;
                if (entity != null)
                {
                    model = new PersonGuardian();
                    model.Id = entity.Person_Guardian_Id;
                    model.Address = entity.Address;
                    model.Email = entity.Email;
                    model.GuardianName = entity.Guardian_Name;
                    model.MobilePhone = entity.Mobile_Phone;
                    model.Occupation = entity.Occupation;
                    model.Person = personTranslator.Translate(entity.PERSON);
                    
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PERSON_GUARDIAN TranslateToEntity(PersonGuardian model)
        {
            try
            {
                PERSON_GUARDIAN entity = null;
                if (model != null)
                {
                    entity = new PERSON_GUARDIAN();
                    entity.Address = model.Address;
                    entity.Email = model.Email;
                    entity.Guardian_Name = model.GuardianName;
                    entity.Mobile_Phone = model.MobilePhone;
                    entity.Occupation = model.Occupation;
                    entity.Person_Guardian_Id = model.Id;
                    entity.Person_Id = model.Person.Id;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
