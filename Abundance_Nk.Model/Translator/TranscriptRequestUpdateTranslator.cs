﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Model.Translator
{
    public class TranscriptRequestUpdateTranslator : TranslatorBase<TranscriptRequestUpdate, TRANSCRIPT_REQUEST_UPDATE>
    {
        public override TranscriptRequestUpdate TranslateToModel(TRANSCRIPT_REQUEST_UPDATE entity)
        {
            try
            {
                TranscriptRequestUpdate model = null;
                if (entity != null)
                {
                    model = new TranscriptRequestUpdate();
                    model.Id = entity.Transcript_Request_Update_Id;
                    model.Count = entity.Count;
                    model.LastRequestId = entity.Last_Request_Id;
                    model.LastUpdateOn = entity.Last_Update_On;
                }

                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override TRANSCRIPT_REQUEST_UPDATE TranslateToEntity(TranscriptRequestUpdate model)
        {
            try
            {
                TRANSCRIPT_REQUEST_UPDATE entity = null;
                if (model != null)
                {
                    entity = new TRANSCRIPT_REQUEST_UPDATE();
                    entity.Last_Request_Id = model.LastRequestId;
                    entity.Count = model.Count;
                    entity.Last_Update_On = model.LastUpdateOn;
                    entity.Transcript_Request_Update_Id = model.Id;
                }

                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
