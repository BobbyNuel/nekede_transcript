﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Model.Translator
{
    public class StudentTranslator : TranslatorBase<Student, STUDENT>
    {
        private GenotypeTranslator genotypeTranslator;
        private TitleTranslator titleTranslator;
        private StudentTypeTranslator studentTypeTranslator;
        private StudentCategoryTranslator studentCategoryTranslator;
        private ApplicationFormTranslator applicationFormTranslator;
        private StudentStatusTranslator studentStatusTranslator;
        private MaritalStatusTranslator maritalStatusTranslator;
        private BloodGroupTranslator bloodGroupTranslator;

        public StudentTranslator()
        {
            titleTranslator = new TitleTranslator();
            studentTypeTranslator = new StudentTypeTranslator();
            studentCategoryTranslator = new StudentCategoryTranslator();
            applicationFormTranslator = new ApplicationFormTranslator();
            studentStatusTranslator = new StudentStatusTranslator();
            maritalStatusTranslator = new MaritalStatusTranslator();
            bloodGroupTranslator = new BloodGroupTranslator();
            genotypeTranslator = new GenotypeTranslator();
        }
 
        public override Student TranslateToModel(STUDENT studentEntity)
        {
            try
            {
                Student student = null;
                if (studentEntity != null)
                {
                    student = new Student();
                    student.Id = studentEntity.Person_Id;
                    student.ApplicationForm = applicationFormTranslator.Translate(studentEntity.APPLICATION_FORM);
                    student.Type = studentTypeTranslator.TranslateToModel(studentEntity.STUDENT_TYPE);
                    student.Category = studentCategoryTranslator.TranslateToModel(studentEntity.STUDENT_CATEGORY);
                    student.Status = studentStatusTranslator.TranslateToModel(studentEntity.STUDENT_STATUS);
                    student.Number = studentEntity.Student_Number;
                    student.MatricNumber = studentEntity.Matric_Number;
                    student.Title = titleTranslator.Translate(studentEntity.TITLE);
                    student.MaritalStatus = maritalStatusTranslator.Translate(studentEntity.MARITAL_STATUS);
                    student.BloodGroup = bloodGroupTranslator.Translate(studentEntity.BLOOD_GROUP);
                    student.Genotype = genotypeTranslator.Translate(studentEntity.GENOTYPE);
                    student.SchoolContactAddress = studentEntity.School_Contact_Address;
                    if (studentEntity.PERSON != null)
                    {
                        student.LastName = studentEntity.PERSON.Last_Name;
                        student.FirstName = studentEntity.PERSON.First_Name;
                        student.OtherName = studentEntity.PERSON.Other_Name;
                        student.Email = studentEntity.PERSON.Email;
                        student.MobilePhone = studentEntity.PERSON.Mobile_Phone;
                        student.ContactAddress = studentEntity.PERSON.Contact_Address;

                    }
                }

                return student;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override STUDENT TranslateToEntity(Student student)
        {
            try
            {
                STUDENT studentEntity = null;
                if (student != null)
                {
                    studentEntity = new STUDENT();
                    studentEntity.Person_Id = student.Id;
                    if (student.ApplicationForm != null)
                    {
                        studentEntity.Application_Form_Id = student.ApplicationForm.Id; 
                    }
                    if (student.Type != null)
                    {
                        studentEntity.Student_Type_Id = student.Type.Id; 
                    }
                    if (student.Category != null)
                    {
                        studentEntity.Student_Category_Id = student.Category.Id; 
                    }
                    if (student.Status != null)
                    {
                        studentEntity.Student_Status_Id = student.Status.Id;  
                    }
                    if (student.Title != null)
                    {
                        studentEntity.Title_Id = student.Title.Id; 
                    }
                    if (student.MaritalStatus != null)
                    {
                        studentEntity.Marital_Status_Id = student.MaritalStatus.Id; 
                    }
                    if (student.BloodGroup != null)
                    {
                        studentEntity.Blood_Group_Id = student.BloodGroup.Id;
                    }
                    if (student.Genotype != null)
                    {
                        studentEntity.Genotype_Id = student.Genotype.Id; 
                    }

                    studentEntity.Student_Number = student.Number;
                    studentEntity.Matric_Number = student.MatricNumber;
                    studentEntity.School_Contact_Address = student.SchoolContactAddress;
                }

                return studentEntity;
            }
            catch (Exception)
            {
                throw;
            }
        }





    }
}
