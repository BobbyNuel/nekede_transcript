﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Model.Translator
{
    public class StudentAcademicInformationAuditTranslator : TranslatorBase<StudentAcademicInformationAudit,STUDENT_ACEDEMIC_INFORMATION_AUDIT>
    {
        private StudentAcademicInformationTranslator studentAcademicInformationTranslator;

        public StudentAcademicInformationAuditTranslator()
        {
            studentAcademicInformationTranslator = new StudentAcademicInformationTranslator();
        }
        public override StudentAcademicInformationAudit TranslateToModel(STUDENT_ACEDEMIC_INFORMATION_AUDIT entity)
        {
            try
            {
                StudentAcademicInformationAudit model = null;
                if (entity != null)
                {
                    model = new StudentAcademicInformationAudit();
                    model.Id = entity.Student_Acedemic_Information_Audit_Id;
                    model.StudentAcademicInformation = studentAcademicInformationTranslator.Translate(entity.STUDENT_ACADEMIC_INFORMATION);
                    model.OldCgpa = entity.Old_CGPA;
                    model.NewCgpa = entity.New_CGPA;
                    model.Operation = entity.Operation;
                    model.Action = entity.Action;
                    model.Client = entity.Client;
                    model.Time = entity.Time;
                    model.UserId = entity.User_Id;

                }
                return model;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }

        public override STUDENT_ACEDEMIC_INFORMATION_AUDIT TranslateToEntity(StudentAcademicInformationAudit model)
        {
            try
            {
                STUDENT_ACEDEMIC_INFORMATION_AUDIT entity = null;
                if (model != null)
                {
                    entity = new STUDENT_ACEDEMIC_INFORMATION_AUDIT();
                    entity.Person_Id = model.StudentAcademicInformation.Student.Id;
                    entity.Old_CGPA = model.OldCgpa;
                    entity.New_CGPA = model.NewCgpa;
                    entity.Operation = model.Operation;
                    entity.Action = model.Action;
                    entity.Client = model.Client;
                    entity.Time = model.Time;
                    entity.User_Id = model.UserId;

                }
                return entity;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
   