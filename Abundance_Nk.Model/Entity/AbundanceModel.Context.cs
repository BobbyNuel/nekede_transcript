﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Abundance_Nk.Model.Entity
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class Abundance_Nk_NekedeEntities : DbContext
    {
        public Abundance_Nk_NekedeEntities()
            : base("name=Abundance_Nk_NekedeEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__appf_1> C__appf_1 { get; set; }
        public virtual DbSet<C__appf_4> C__appf_4 { get; set; }
        public virtual DbSet<C__appf_5> C__appf_5 { get; set; }
        public virtual DbSet<C__Table_1> C__Table_1 { get; set; }
        public virtual DbSet<ABILITY> ABILITY { get; set; }
        public virtual DbSet<ADMISSION_CRITERIA> ADMISSION_CRITERIA { get; set; }
        public virtual DbSet<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT> ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT { get; set; }
        public virtual DbSet<ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT_ALTERNATIVE> ADMISSION_CRITERIA_FOR_O_LEVEL_SUBJECT_ALTERNATIVE { get; set; }
        public virtual DbSet<ADMISSION_CRITERIA_FOR_O_LEVEL_TYPE> ADMISSION_CRITERIA_FOR_O_LEVEL_TYPE { get; set; }
        public virtual DbSet<ADMISSION_LIST_AUDIT> ADMISSION_LIST_AUDIT { get; set; }
        public virtual DbSet<ADMISSION_LIST_BATCH> ADMISSION_LIST_BATCH { get; set; }
        public virtual DbSet<ADMISSION_LIST_TYPE> ADMISSION_LIST_TYPE { get; set; }
        public virtual DbSet<APPLICANT> APPLICANT { get; set; }
        public virtual DbSet<APPLICANT_APPLIED_COURSE> APPLICANT_APPLIED_COURSE { get; set; }
        public virtual DbSet<APPLICANT_APPLIED_COURSE_AUDIT> APPLICANT_APPLIED_COURSE_AUDIT { get; set; }
        public virtual DbSet<APPLICANT_CLEARANCE> APPLICANT_CLEARANCE { get; set; }
        public virtual DbSet<APPLICANT_JAMB_DETAIL> APPLICANT_JAMB_DETAIL { get; set; }
        public virtual DbSet<APPLICANT_LEVEL> APPLICANT_LEVEL { get; set; }
        public virtual DbSet<APPLICANT_O_LEVEL_RESULT> APPLICANT_O_LEVEL_RESULT { get; set; }
        public virtual DbSet<APPLICANT_O_LEVEL_RESULT_DETAIL> APPLICANT_O_LEVEL_RESULT_DETAIL { get; set; }
        public virtual DbSet<APPLICANT_PREVIOUS_EDUCATION> APPLICANT_PREVIOUS_EDUCATION { get; set; }
        public virtual DbSet<APPLICANT_SPONSOR> APPLICANT_SPONSOR { get; set; }
        public virtual DbSet<APPLICANT_STATUS> APPLICANT_STATUS { get; set; }
        public virtual DbSet<APPLICATION_FORM> APPLICATION_FORM { get; set; }
        public virtual DbSet<APPLICATION_FORM_SETTING> APPLICATION_FORM_SETTING { get; set; }
        public virtual DbSet<APPLICATION_PROGRAMME_FEE> APPLICATION_PROGRAMME_FEE { get; set; }
        public virtual DbSet<ATTENDANCE> ATTENDANCE { get; set; }
        public virtual DbSet<ATTENDANCE_STATUS> ATTENDANCE_STATUS { get; set; }
        public virtual DbSet<BANK> BANK { get; set; }
        public virtual DbSet<BANK_BRANCH> BANK_BRANCH { get; set; }
        public virtual DbSet<BLOOD_GROUP> BLOOD_GROUP { get; set; }
        public virtual DbSet<CARD_PAYMENT> CARD_PAYMENT { get; set; }
        public virtual DbSet<CLASS_ASSESSMENT> CLASS_ASSESSMENT { get; set; }
        public virtual DbSet<CLASS_ROOM> CLASS_ROOM { get; set; }
        public virtual DbSet<CLEARANCE_LOG> CLEARANCE_LOG { get; set; }
        public virtual DbSet<COUNTRY> COUNTRY { get; set; }
        public virtual DbSet<COURSE_TEACHER> COURSE_TEACHER { get; set; }
        public virtual DbSet<COURSE_TYPE> COURSE_TYPE { get; set; }
        public virtual DbSet<CURRENT_FEE> CURRENT_FEE { get; set; }
        public virtual DbSet<CURRENT_SESSION_SEMESTER> CURRENT_SESSION_SEMESTER { get; set; }
        public virtual DbSet<DAY> DAY { get; set; }
        public virtual DbSet<DAY_ACTIVITY> DAY_ACTIVITY { get; set; }
        public virtual DbSet<DEPARTMENT> DEPARTMENT { get; set; }
        public virtual DbSet<DEPARTMENT_HEAD> DEPARTMENT_HEAD { get; set; }
        public virtual DbSet<DEPARTMENT_OPTION> DEPARTMENT_OPTION { get; set; }
        public virtual DbSet<EDUCATIONAL_QUALIFICATION> EDUCATIONAL_QUALIFICATION { get; set; }
        public virtual DbSet<FACULTY> FACULTY { get; set; }
        public virtual DbSet<FEE> FEE { get; set; }
        public virtual DbSet<FEE_DETAIL> FEE_DETAIL { get; set; }
        public virtual DbSet<FEE_TYPE> FEE_TYPE { get; set; }
        public virtual DbSet<GENOTYPE> GENOTYPE { get; set; }
        public virtual DbSet<GRADE_LEVEL> GRADE_LEVEL { get; set; }
        public virtual DbSet<ILLNESS> ILLNESS { get; set; }
        public virtual DbSet<INSTITUTION> INSTITUTION { get; set; }
        public virtual DbSet<INSTITUTION_CHOICE> INSTITUTION_CHOICE { get; set; }
        public virtual DbSet<INSTITUTION_TYPE> INSTITUTION_TYPE { get; set; }
        public virtual DbSet<IT_DURATION> IT_DURATION { get; set; }
        public virtual DbSet<JOB_ROLE> JOB_ROLE { get; set; }
        public virtual DbSet<LEVEL> LEVEL { get; set; }
        public virtual DbSet<LEVEL_CLASS_ROOM> LEVEL_CLASS_ROOM { get; set; }
        public virtual DbSet<LOCAL_GOVERNMENT> LOCAL_GOVERNMENT { get; set; }
        public virtual DbSet<MARITAL_STATUS> MARITAL_STATUS { get; set; }
        public virtual DbSet<MODE_OF_ENTRY> MODE_OF_ENTRY { get; set; }
        public virtual DbSet<MODE_OF_FINANCE> MODE_OF_FINANCE { get; set; }
        public virtual DbSet<MODE_OF_STUDY> MODE_OF_STUDY { get; set; }
        public virtual DbSet<NATIONALITY> NATIONALITY { get; set; }
        public virtual DbSet<NEWS> NEWS { get; set; }
        public virtual DbSet<NEXT_OF_KIN> NEXT_OF_KIN { get; set; }
        public virtual DbSet<O_LEVEL_EXAM_SITTING> O_LEVEL_EXAM_SITTING { get; set; }
        public virtual DbSet<O_LEVEL_GRADE> O_LEVEL_GRADE { get; set; }
        public virtual DbSet<O_LEVEL_SUBJECT> O_LEVEL_SUBJECT { get; set; }
        public virtual DbSet<O_LEVEL_TYPE> O_LEVEL_TYPE { get; set; }
        public virtual DbSet<OCCUPATION> OCCUPATION { get; set; }
        public virtual DbSet<OFFENCE> OFFENCE { get; set; }
        public virtual DbSet<OFFENCE_DEGREE> OFFENCE_DEGREE { get; set; }
        public virtual DbSet<OFFENCE_PUNISHMENT> OFFENCE_PUNISHMENT { get; set; }
        public virtual DbSet<OLD_PORTAL_ISSUES> OLD_PORTAL_ISSUES { get; set; }
        public virtual DbSet<ONLINE_PAYMENT> ONLINE_PAYMENT { get; set; }
        public virtual DbSet<PAYMENT> PAYMENT { get; set; }
        public virtual DbSet<PAYMENT_CHANNEL> PAYMENT_CHANNEL { get; set; }
        public virtual DbSet<PAYMENT_ETRANZACT> PAYMENT_ETRANZACT { get; set; }
        public virtual DbSet<PAYMENT_ETRANZACT_TYPE> PAYMENT_ETRANZACT_TYPE { get; set; }
        public virtual DbSet<PAYMENT_MODE> PAYMENT_MODE { get; set; }
        public virtual DbSet<PAYMENT_TERMINAL> PAYMENT_TERMINAL { get; set; }
        public virtual DbSet<PAYMENT_TYPE> PAYMENT_TYPE { get; set; }
        public virtual DbSet<PERIOD> PERIOD { get; set; }
        public virtual DbSet<PERSON> PERSON { get; set; }
        public virtual DbSet<PERSON_AUDIT> PERSON_AUDIT { get; set; }
        public virtual DbSet<PERSON_LOGIN> PERSON_LOGIN { get; set; }
        public virtual DbSet<PERSON_TYPE> PERSON_TYPE { get; set; }
        public virtual DbSet<PROGRAMME> PROGRAMME { get; set; }
        public virtual DbSet<PROGRAMME_DEPARTMENT> PROGRAMME_DEPARTMENT { get; set; }
        public virtual DbSet<PUNISHMENT> PUNISHMENT { get; set; }
        public virtual DbSet<PUNISHMENT_STATUS> PUNISHMENT_STATUS { get; set; }
        public virtual DbSet<PUTME_RESULT> PUTME_RESULT { get; set; }
        public virtual DbSet<PUTME_RESULT_AUDIT> PUTME_RESULT_AUDIT { get; set; }
        public virtual DbSet<RANKING_DATA> RANKING_DATA { get; set; }
        public virtual DbSet<RATING> RATING { get; set; }
        public virtual DbSet<RELATIONSHIP> RELATIONSHIP { get; set; }
        public virtual DbSet<RELIGION> RELIGION { get; set; }
        public virtual DbSet<REMITA_PAYMENT> REMITA_PAYMENT { get; set; }
        public virtual DbSet<REMITA_PAYMENT_SETTINGS> REMITA_PAYMENT_SETTINGS { get; set; }
        public virtual DbSet<REMITA_SPLIT_DETAILS> REMITA_SPLIT_DETAILS { get; set; }
        public virtual DbSet<REPORT_CARD> REPORT_CARD { get; set; }
        public virtual DbSet<REPORT_CARD_COMMENT> REPORT_CARD_COMMENT { get; set; }
        public virtual DbSet<RESULT_GRADE> RESULT_GRADE { get; set; }
        public virtual DbSet<RIGHT> RIGHT { get; set; }
        public virtual DbSet<ROLE> ROLE { get; set; }
        public virtual DbSet<ROLE_RIGHT> ROLE_RIGHT { get; set; }
        public virtual DbSet<SCORE_GRADE> SCORE_GRADE { get; set; }
        public virtual DbSet<SCRATCH_CARD> SCRATCH_CARD { get; set; }
        public virtual DbSet<SCRATCH_CARD_BATCH> SCRATCH_CARD_BATCH { get; set; }
        public virtual DbSet<SCRATCH_CARD_TYPE> SCRATCH_CARD_TYPE { get; set; }
        public virtual DbSet<SECURITY_QUESTION> SECURITY_QUESTION { get; set; }
        public virtual DbSet<SEMESTER> SEMESTER { get; set; }
        public virtual DbSet<SESSION> SESSION { get; set; }
        public virtual DbSet<SESSION_SEMESTER> SESSION_SEMESTER { get; set; }
        public virtual DbSet<SEX> SEX { get; set; }
        public virtual DbSet<STAFF> STAFF { get; set; }
        public virtual DbSet<STAFF_DEPARTMENT> STAFF_DEPARTMENT { get; set; }
        public virtual DbSet<STAFF_JOB_ROLE> STAFF_JOB_ROLE { get; set; }
        public virtual DbSet<STAFF_TYPE> STAFF_TYPE { get; set; }
        public virtual DbSet<STATE> STATE { get; set; }
        public virtual DbSet<STUDENT_ASSESSMENT_CRITERIA> STUDENT_ASSESSMENT_CRITERIA { get; set; }
        public virtual DbSet<STUDENT_CATEGORY> STUDENT_CATEGORY { get; set; }
        public virtual DbSet<STUDENT_EMPLOYMENT_INFORMATION> STUDENT_EMPLOYMENT_INFORMATION { get; set; }
        public virtual DbSet<STUDENT_FINANCE_INFORMATION> STUDENT_FINANCE_INFORMATION { get; set; }
        public virtual DbSet<STUDENT_LEVEL> STUDENT_LEVEL { get; set; }
        public virtual DbSet<STUDENT_LEVEL_HISTORY> STUDENT_LEVEL_HISTORY { get; set; }
        public virtual DbSet<STUDENT_MATRIC_NUMBER_ASSIGNMENT> STUDENT_MATRIC_NUMBER_ASSIGNMENT { get; set; }
        public virtual DbSet<STUDENT_ND_RESULT> STUDENT_ND_RESULT { get; set; }
        public virtual DbSet<STUDENT_PAYMENT> STUDENT_PAYMENT { get; set; }
        public virtual DbSet<STUDENT_SCORE> STUDENT_SCORE { get; set; }
        public virtual DbSet<STUDENT_SEMESTER> STUDENT_SEMESTER { get; set; }
        public virtual DbSet<STUDENT_SPONSOR> STUDENT_SPONSOR { get; set; }
        public virtual DbSet<STUDENT_STATUS> STUDENT_STATUS { get; set; }
        public virtual DbSet<STUDENT_TYPE> STUDENT_TYPE { get; set; }
        public virtual DbSet<TITLE> TITLE { get; set; }
        public virtual DbSet<USER> USER { get; set; }
        public virtual DbSet<C__appf_2> C__appf_2 { get; set; }
        public virtual DbSet<C__Table_2> C__Table_2 { get; set; }
        public virtual DbSet<VENUE_DESIGNATION> VENUE_DESIGNATION { get; set; }
        public virtual DbSet<VW_ACCEPTANCE_REPORT> VW_ACCEPTANCE_REPORT { get; set; }
        public virtual DbSet<VW_ADMISSION_LIST> VW_ADMISSION_LIST { get; set; }
        public virtual DbSet<VW_APPLICANT_PAYMENT> VW_APPLICANT_PAYMENT { get; set; }
        public virtual DbSet<VW_APPLICATION_EXAM_NUMBER> VW_APPLICATION_EXAM_NUMBER { get; set; }
        public virtual DbSet<VW_APPLICATION_FORM_SUMMARY> VW_APPLICATION_FORM_SUMMARY { get; set; }
        public virtual DbSet<VW_ASSIGNED_MATRIC_NUMBER> VW_ASSIGNED_MATRIC_NUMBER { get; set; }
        public virtual DbSet<VW_DEPARTMENT_OPTION> VW_DEPARTMENT_OPTION { get; set; }
        public virtual DbSet<VW_PAYMENT> VW_PAYMENT { get; set; }
        public virtual DbSet<VW_POST_JAMP_APPLICATION> VW_POST_JAMP_APPLICATION { get; set; }
        public virtual DbSet<VW_POST_JAMP_APPLICATION_ALL_PROGRAMME> VW_POST_JAMP_APPLICATION_ALL_PROGRAMME { get; set; }
        public virtual DbSet<VW_PROGRAMME_DEPARTMENT> VW_PROGRAMME_DEPARTMENT { get; set; }
        public virtual DbSet<VW_PROSPECTIVE_STUDENT> VW_PROSPECTIVE_STUDENT { get; set; }
        public virtual DbSet<VW_NDM_RANKING> VW_NDM_RANKING { get; set; }
        public virtual DbSet<VW_RANKING_ALL_PROGRAMME> VW_RANKING_ALL_PROGRAMME { get; set; }
        public virtual DbSet<COURSE_ALLOCATION> COURSE_ALLOCATION { get; set; }
        public virtual DbSet<COURSE_MODE> COURSE_MODE { get; set; }
        public virtual DbSet<STUDENT_COURSE_REGISTRATION> STUDENT_COURSE_REGISTRATION { get; set; }
        public virtual DbSet<STUDENT_COURSE_REGISTRATION_DETAIL> STUDENT_COURSE_REGISTRATION_DETAIL { get; set; }
        public virtual DbSet<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT> STUDENT_EXAM_RAW_SCORE_SHEET_RESULT { get; set; }
        public virtual DbSet<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED> STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED { get; set; }
        public virtual DbSet<STUDENT_RESULT_DETAIL> STUDENT_RESULT_DETAIL { get; set; }
        public virtual DbSet<STUDENT_RESULT_TYPE> STUDENT_RESULT_TYPE { get; set; }
        public virtual DbSet<VW_REGISTERED_COURSES> VW_REGISTERED_COURSES { get; set; }
        public virtual DbSet<VW_STUDENT_REGISTERED_COURSE_CARRYOVER> VW_STUDENT_REGISTERED_COURSE_CARRYOVER { get; set; }
        public virtual DbSet<VW_STUDENT_REGISTERED_COURSES> VW_STUDENT_REGISTERED_COURSES { get; set; }
        public virtual DbSet<VW_STUDENT_RESULT_RAW_SCORE_SHEET> VW_STUDENT_RESULT_RAW_SCORE_SHEET { get; set; }
        public virtual DbSet<VW_STUDENT_RESULT_RAW_SCORE_SHEET_UNREGISTERED> VW_STUDENT_RESULT_RAW_SCORE_SHEET_UNREGISTERED { get; set; }
        public virtual DbSet<VW_UPLOADED_COURSES> VW_UPLOADED_COURSES { get; set; }
        public virtual DbSet<COURSE> COURSE { get; set; }
        public virtual DbSet<STUDENT_ND_RESULT_DETAIL> STUDENT_ND_RESULT_DETAIL { get; set; }
        public virtual DbSet<STUDENT_RESULT> STUDENT_RESULT { get; set; }
        public virtual DbSet<VW_STUDENT_RESULT_SUMMARY> VW_STUDENT_RESULT_SUMMARY { get; set; }
        public virtual DbSet<STUDENT> STUDENT { get; set; }
        public virtual DbSet<TRANSCRIPT_CLEARANCE_STATUS> TRANSCRIPT_CLEARANCE_STATUS { get; set; }
        public virtual DbSet<TRANSCRIPT_REQUEST> TRANSCRIPT_REQUEST { get; set; }
        public virtual DbSet<TRANSCRIPT_STATUS> TRANSCRIPT_STATUS { get; set; }
        public virtual DbSet<PAYMENT_INTERSWITCH> PAYMENT_INTERSWITCH { get; set; }
        public virtual DbSet<MENU> MENU { get; set; }
        public virtual DbSet<MENU_GROUP> MENU_GROUP { get; set; }
        public virtual DbSet<MENU_IN_ROLE> MENU_IN_ROLE { get; set; }
        public virtual DbSet<VW_APPLICANT_DETAILS> VW_APPLICANT_DETAILS { get; set; }
        public virtual DbSet<HOSTEL_ALLOCATION> HOSTEL_ALLOCATION { get; set; }
        public virtual DbSet<HOSTEL_ALLOCATION_COUNT> HOSTEL_ALLOCATION_COUNT { get; set; }
        public virtual DbSet<HOSTEL_BLACKLIST> HOSTEL_BLACKLIST { get; set; }
        public virtual DbSet<HOSTEL_CRITERIA> HOSTEL_CRITERIA { get; set; }
        public virtual DbSet<HOSTEL_FEE> HOSTEL_FEE { get; set; }
        public virtual DbSet<HOSTEL_REQUEST> HOSTEL_REQUEST { get; set; }
        public virtual DbSet<HOSTEL_ROOM> HOSTEL_ROOM { get; set; }
        public virtual DbSet<HOSTEL_ROOM_CORNER> HOSTEL_ROOM_CORNER { get; set; }
        public virtual DbSet<HOSTEL_SERIES> HOSTEL_SERIES { get; set; }
        public virtual DbSet<HOSTEL> HOSTEL { get; set; }
        public virtual DbSet<HOSTEL_TYPE> HOSTEL_TYPE { get; set; }
        public virtual DbSet<VW_ADMISSION_LIST_ALL> VW_ADMISSION_LIST_ALL { get; set; }
        public virtual DbSet<VW_PUTME_RESULT> VW_PUTME_RESULT { get; set; }
        public virtual DbSet<FEE_DETAIL_AUDIT> FEE_DETAIL_AUDIT { get; set; }
        public virtual DbSet<VW_ADMITTED_STUDENTS_RESULT_DETAILS> VW_ADMITTED_STUDENTS_RESULT_DETAILS { get; set; }
        public virtual DbSet<VW_ADMISSION_SUMMARY> VW_ADMISSION_SUMMARY { get; set; }
        public virtual DbSet<ADMISSION_LIST> ADMISSION_LIST { get; set; }
        public virtual DbSet<PERSON_GUARDIAN> PERSON_GUARDIAN { get; set; }
        public virtual DbSet<STUDENT_ACADEMIC_INFORMATION> STUDENT_ACADEMIC_INFORMATION { get; set; }
        public virtual DbSet<STUDENT_ACEDEMIC_INFORMATION_AUDIT> STUDENT_ACEDEMIC_INFORMATION_AUDIT { get; set; }
        public virtual DbSet<TRANSCRIPT_REQUEST_UPDATE> TRANSCRIPT_REQUEST_UPDATE { get; set; }
        public virtual DbSet<VW_STUDENT_RESULT_ALT> VW_STUDENT_RESULT_ALT { get; set; }
        public virtual DbSet<VW_UNRANKED_STUDENT> VW_UNRANKED_STUDENT { get; set; }
    
        public virtual int STP_DELETE_APPLICATION_FORM(Nullable<long> paramApplication_Form_Id)
        {
            var paramApplication_Form_IdParameter = paramApplication_Form_Id.HasValue ?
                new ObjectParameter("paramApplication_Form_Id", paramApplication_Form_Id) :
                new ObjectParameter("paramApplication_Form_Id", typeof(long));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("STP_DELETE_APPLICATION_FORM", paramApplication_Form_IdParameter);
        }
    
        public virtual ObjectResult<STP_INSERT_APPLICATION_FORM_Result> STP_INSERT_APPLICATION_FORM(Nullable<long> paramSerial_Number, string paramApplication_Form_Number, string paramApplication_Exam_Number, Nullable<int> paramApplication_Form_Setting_Id, Nullable<int> paramApplication_Programme_Fee_Id, Nullable<long> paramPayment_Id, Nullable<long> paramPerson_Id, Nullable<int> paramExam_Serial_Number, string paramExam_Number, Nullable<System.DateTime> paramDate_Submitted, Nullable<bool> paramRelease, Nullable<bool> paramRejected, string paramReject_Reason, string paramRemarks)
        {
            var paramSerial_NumberParameter = paramSerial_Number.HasValue ?
                new ObjectParameter("paramSerial_Number", paramSerial_Number) :
                new ObjectParameter("paramSerial_Number", typeof(long));
    
            var paramApplication_Form_NumberParameter = paramApplication_Form_Number != null ?
                new ObjectParameter("paramApplication_Form_Number", paramApplication_Form_Number) :
                new ObjectParameter("paramApplication_Form_Number", typeof(string));
    
            var paramApplication_Exam_NumberParameter = paramApplication_Exam_Number != null ?
                new ObjectParameter("paramApplication_Exam_Number", paramApplication_Exam_Number) :
                new ObjectParameter("paramApplication_Exam_Number", typeof(string));
    
            var paramApplication_Form_Setting_IdParameter = paramApplication_Form_Setting_Id.HasValue ?
                new ObjectParameter("paramApplication_Form_Setting_Id", paramApplication_Form_Setting_Id) :
                new ObjectParameter("paramApplication_Form_Setting_Id", typeof(int));
    
            var paramApplication_Programme_Fee_IdParameter = paramApplication_Programme_Fee_Id.HasValue ?
                new ObjectParameter("paramApplication_Programme_Fee_Id", paramApplication_Programme_Fee_Id) :
                new ObjectParameter("paramApplication_Programme_Fee_Id", typeof(int));
    
            var paramPayment_IdParameter = paramPayment_Id.HasValue ?
                new ObjectParameter("paramPayment_Id", paramPayment_Id) :
                new ObjectParameter("paramPayment_Id", typeof(long));
    
            var paramPerson_IdParameter = paramPerson_Id.HasValue ?
                new ObjectParameter("paramPerson_Id", paramPerson_Id) :
                new ObjectParameter("paramPerson_Id", typeof(long));
    
            var paramExam_Serial_NumberParameter = paramExam_Serial_Number.HasValue ?
                new ObjectParameter("paramExam_Serial_Number", paramExam_Serial_Number) :
                new ObjectParameter("paramExam_Serial_Number", typeof(int));
    
            var paramExam_NumberParameter = paramExam_Number != null ?
                new ObjectParameter("paramExam_Number", paramExam_Number) :
                new ObjectParameter("paramExam_Number", typeof(string));
    
            var paramDate_SubmittedParameter = paramDate_Submitted.HasValue ?
                new ObjectParameter("paramDate_Submitted", paramDate_Submitted) :
                new ObjectParameter("paramDate_Submitted", typeof(System.DateTime));
    
            var paramReleaseParameter = paramRelease.HasValue ?
                new ObjectParameter("paramRelease", paramRelease) :
                new ObjectParameter("paramRelease", typeof(bool));
    
            var paramRejectedParameter = paramRejected.HasValue ?
                new ObjectParameter("paramRejected", paramRejected) :
                new ObjectParameter("paramRejected", typeof(bool));
    
            var paramReject_ReasonParameter = paramReject_Reason != null ?
                new ObjectParameter("paramReject_Reason", paramReject_Reason) :
                new ObjectParameter("paramReject_Reason", typeof(string));
    
            var paramRemarksParameter = paramRemarks != null ?
                new ObjectParameter("paramRemarks", paramRemarks) :
                new ObjectParameter("paramRemarks", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<STP_INSERT_APPLICATION_FORM_Result>("STP_INSERT_APPLICATION_FORM", paramSerial_NumberParameter, paramApplication_Form_NumberParameter, paramApplication_Exam_NumberParameter, paramApplication_Form_Setting_IdParameter, paramApplication_Programme_Fee_IdParameter, paramPayment_IdParameter, paramPerson_IdParameter, paramExam_Serial_NumberParameter, paramExam_NumberParameter, paramDate_SubmittedParameter, paramReleaseParameter, paramRejectedParameter, paramReject_ReasonParameter, paramRemarksParameter);
        }
    
        public virtual ObjectResult<STP_UPDATE_APPLICATION_FORM_Result> STP_UPDATE_APPLICATION_FORM(Nullable<long> paramApplication_Form_Id, Nullable<long> paramSerial_Number, string paramApplication_Form_Number, string paramApplication_Exam_Number, Nullable<int> paramApplication_Form_Setting_Id, Nullable<int> paramApplication_Programme_Fee_Id, Nullable<long> paramPayment_Id, Nullable<long> paramPerson_Id, Nullable<int> paramExam_Serial_Number, string paramExam_Number, Nullable<System.DateTime> paramDate_Submitted, Nullable<bool> paramRelease, Nullable<bool> paramRejected, string paramReject_Reason, string paramRemarks)
        {
            var paramApplication_Form_IdParameter = paramApplication_Form_Id.HasValue ?
                new ObjectParameter("paramApplication_Form_Id", paramApplication_Form_Id) :
                new ObjectParameter("paramApplication_Form_Id", typeof(long));
    
            var paramSerial_NumberParameter = paramSerial_Number.HasValue ?
                new ObjectParameter("paramSerial_Number", paramSerial_Number) :
                new ObjectParameter("paramSerial_Number", typeof(long));
    
            var paramApplication_Form_NumberParameter = paramApplication_Form_Number != null ?
                new ObjectParameter("paramApplication_Form_Number", paramApplication_Form_Number) :
                new ObjectParameter("paramApplication_Form_Number", typeof(string));
    
            var paramApplication_Exam_NumberParameter = paramApplication_Exam_Number != null ?
                new ObjectParameter("paramApplication_Exam_Number", paramApplication_Exam_Number) :
                new ObjectParameter("paramApplication_Exam_Number", typeof(string));
    
            var paramApplication_Form_Setting_IdParameter = paramApplication_Form_Setting_Id.HasValue ?
                new ObjectParameter("paramApplication_Form_Setting_Id", paramApplication_Form_Setting_Id) :
                new ObjectParameter("paramApplication_Form_Setting_Id", typeof(int));
    
            var paramApplication_Programme_Fee_IdParameter = paramApplication_Programme_Fee_Id.HasValue ?
                new ObjectParameter("paramApplication_Programme_Fee_Id", paramApplication_Programme_Fee_Id) :
                new ObjectParameter("paramApplication_Programme_Fee_Id", typeof(int));
    
            var paramPayment_IdParameter = paramPayment_Id.HasValue ?
                new ObjectParameter("paramPayment_Id", paramPayment_Id) :
                new ObjectParameter("paramPayment_Id", typeof(long));
    
            var paramPerson_IdParameter = paramPerson_Id.HasValue ?
                new ObjectParameter("paramPerson_Id", paramPerson_Id) :
                new ObjectParameter("paramPerson_Id", typeof(long));
    
            var paramExam_Serial_NumberParameter = paramExam_Serial_Number.HasValue ?
                new ObjectParameter("paramExam_Serial_Number", paramExam_Serial_Number) :
                new ObjectParameter("paramExam_Serial_Number", typeof(int));
    
            var paramExam_NumberParameter = paramExam_Number != null ?
                new ObjectParameter("paramExam_Number", paramExam_Number) :
                new ObjectParameter("paramExam_Number", typeof(string));
    
            var paramDate_SubmittedParameter = paramDate_Submitted.HasValue ?
                new ObjectParameter("paramDate_Submitted", paramDate_Submitted) :
                new ObjectParameter("paramDate_Submitted", typeof(System.DateTime));
    
            var paramReleaseParameter = paramRelease.HasValue ?
                new ObjectParameter("paramRelease", paramRelease) :
                new ObjectParameter("paramRelease", typeof(bool));
    
            var paramRejectedParameter = paramRejected.HasValue ?
                new ObjectParameter("paramRejected", paramRejected) :
                new ObjectParameter("paramRejected", typeof(bool));
    
            var paramReject_ReasonParameter = paramReject_Reason != null ?
                new ObjectParameter("paramReject_Reason", paramReject_Reason) :
                new ObjectParameter("paramReject_Reason", typeof(string));
    
            var paramRemarksParameter = paramRemarks != null ?
                new ObjectParameter("paramRemarks", paramRemarks) :
                new ObjectParameter("paramRemarks", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<STP_UPDATE_APPLICATION_FORM_Result>("STP_UPDATE_APPLICATION_FORM", paramApplication_Form_IdParameter, paramSerial_NumberParameter, paramApplication_Form_NumberParameter, paramApplication_Exam_NumberParameter, paramApplication_Form_Setting_IdParameter, paramApplication_Programme_Fee_IdParameter, paramPayment_IdParameter, paramPerson_IdParameter, paramExam_Serial_NumberParameter, paramExam_NumberParameter, paramDate_SubmittedParameter, paramReleaseParameter, paramRejectedParameter, paramReject_ReasonParameter, paramRemarksParameter);
        }
    }
}
