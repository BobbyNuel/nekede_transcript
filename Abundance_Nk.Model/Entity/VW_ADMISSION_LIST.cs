//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Abundance_Nk.Model.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class VW_ADMISSION_LIST
    {
        public string Programme_Name { get; set; }
        public string Department_Name { get; set; }
        public string Application_Form_Number { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Other_Name { get; set; }
        public string Mobile_Phone { get; set; }
        public string Email { get; set; }
        public System.DateTime Date_Uploaded { get; set; }
        public string Exam_Number { get; set; }
        public string Admission_List_Type_Name { get; set; }
        public int Application_Form_Setting_Id { get; set; }
    }
}
