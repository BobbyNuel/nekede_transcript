//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Abundance_Nk.Model.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class SESSION
    {
        public SESSION()
        {
            this.APPLICANT_LEVEL = new HashSet<APPLICANT_LEVEL>();
            this.APPLICATION_FORM_SETTING = new HashSet<APPLICATION_FORM_SETTING>();
            this.APPLICATION_PROGRAMME_FEE = new HashSet<APPLICATION_PROGRAMME_FEE>();
            this.PAYMENT = new HashSet<PAYMENT>();
            this.PAYMENT_ETRANZACT_TYPE = new HashSet<PAYMENT_ETRANZACT_TYPE>();
            this.PAYMENT_TERMINAL = new HashSet<PAYMENT_TERMINAL>();
            this.PUTME_RESULT = new HashSet<PUTME_RESULT>();
            this.SESSION_SEMESTER = new HashSet<SESSION_SEMESTER>();
            this.STUDENT_LEVEL = new HashSet<STUDENT_LEVEL>();
            this.STUDENT_MATRIC_NUMBER_ASSIGNMENT = new HashSet<STUDENT_MATRIC_NUMBER_ASSIGNMENT>();
            this.FEE_DETAIL = new HashSet<FEE_DETAIL>();
            this.COURSE_ALLOCATION = new HashSet<COURSE_ALLOCATION>();
            this.STUDENT_COURSE_REGISTRATION = new HashSet<STUDENT_COURSE_REGISTRATION>();
            this.STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED = new HashSet<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED>();
            this.STUDENT_EXAM_RAW_SCORE_SHEET_RESULT = new HashSet<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT>();
            this.HOSTEL_ALLOCATION = new HashSet<HOSTEL_ALLOCATION>();
            this.HOSTEL_BLACKLIST = new HashSet<HOSTEL_BLACKLIST>();
            this.HOSTEL_REQUEST = new HashSet<HOSTEL_REQUEST>();
            this.FEE_DETAIL_AUDIT = new HashSet<FEE_DETAIL_AUDIT>();
            this.ADMISSION_LIST = new HashSet<ADMISSION_LIST>();
            this.COURSE = new HashSet<COURSE>();
        }
    
        public int Session_Id { get; set; }
        public string Session_Name { get; set; }
        public System.DateTime Start_Date { get; set; }
        public System.DateTime End_date { get; set; }
        public bool Activated { get; set; }
    
        public virtual ICollection<APPLICANT_LEVEL> APPLICANT_LEVEL { get; set; }
        public virtual ICollection<APPLICATION_FORM_SETTING> APPLICATION_FORM_SETTING { get; set; }
        public virtual ICollection<APPLICATION_PROGRAMME_FEE> APPLICATION_PROGRAMME_FEE { get; set; }
        public virtual ICollection<PAYMENT> PAYMENT { get; set; }
        public virtual ICollection<PAYMENT_ETRANZACT_TYPE> PAYMENT_ETRANZACT_TYPE { get; set; }
        public virtual ICollection<PAYMENT_TERMINAL> PAYMENT_TERMINAL { get; set; }
        public virtual ICollection<PUTME_RESULT> PUTME_RESULT { get; set; }
        public virtual ICollection<SESSION_SEMESTER> SESSION_SEMESTER { get; set; }
        public virtual ICollection<STUDENT_LEVEL> STUDENT_LEVEL { get; set; }
        public virtual ICollection<STUDENT_MATRIC_NUMBER_ASSIGNMENT> STUDENT_MATRIC_NUMBER_ASSIGNMENT { get; set; }
        public virtual ICollection<FEE_DETAIL> FEE_DETAIL { get; set; }
        public virtual ICollection<COURSE_ALLOCATION> COURSE_ALLOCATION { get; set; }
        public virtual ICollection<STUDENT_COURSE_REGISTRATION> STUDENT_COURSE_REGISTRATION { get; set; }
        public virtual ICollection<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED> STUDENT_EXAM_RAW_SCORE_SHEET_RESULT_NOT_REGISTERED { get; set; }
        public virtual ICollection<STUDENT_EXAM_RAW_SCORE_SHEET_RESULT> STUDENT_EXAM_RAW_SCORE_SHEET_RESULT { get; set; }
        public virtual ICollection<HOSTEL_ALLOCATION> HOSTEL_ALLOCATION { get; set; }
        public virtual ICollection<HOSTEL_BLACKLIST> HOSTEL_BLACKLIST { get; set; }
        public virtual ICollection<HOSTEL_REQUEST> HOSTEL_REQUEST { get; set; }
        public virtual ICollection<FEE_DETAIL_AUDIT> FEE_DETAIL_AUDIT { get; set; }
        public virtual ICollection<ADMISSION_LIST> ADMISSION_LIST { get; set; }
        public virtual ICollection<COURSE> COURSE { get; set; }
    }
}
