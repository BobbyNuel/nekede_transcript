﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace Abundance_Nk.Web.Services
{
    //[IncludeExceptionDetailInFaults]
    [ServiceContract(Namespace = "Abundance_Nk.Web.Services")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class AjaxService
    {                
        public delegate TResult RankingDelegate<TResult>();

        // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
        // To create an operation that returns XML,
        //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
        //     and include the following line in the operation body:
        //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
        [OperationContract]
        public void DoWork()
        {
            // Add your operation implementation here
            return;
        }

        
         private string RankStudentHelper()
         {
             AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
             List<APPLICANT_APPLIED_COURSE> appliedCourses = appliedCourseLogic.GetEntitiesBy(a => a.Application_Form_Id != null);

             int count = appliedCourses.Count;
             RankingDataLogic rankingDataLogic = new RankingDataLogic();

              for (int i = 0; i < count; i++)
              {
                  rankingDataLogic.RankAllStudents(appliedCourses[i]);
              }

              return "";
            // return rankStudent.BeginInvoke(cb, s);
         }
        private RankingDelegate<string> rankStudent;
        [OperationContract(AsyncPattern = true)]
        [WebInvoke]
        protected IAsyncResult BeginRankStudent(AsyncCallback cb, object s)
        {
            RankingDataLogic rankingDataLogic = new RankingDataLogic();
            rankStudent = RankStudentHelper;
            return rankStudent.BeginInvoke(cb, s);
        }

        public string EndRankStudent(IAsyncResult ar)
        {
            //try
            //{

                return rankStudent.EndInvoke(ar);
            //}
            //catch(Exception ex)
            //{
            //    int d = 0;
            //    return null;
            //}
        }
        // Add more operations here and mark them with [OperationContract]
    }
}
