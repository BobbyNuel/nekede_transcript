﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Microsoft.Reporting.WebForms;
using ZXing;

namespace Abundance_Nk.Web.Reports.Presenter
{
    public partial class ResultTranscriptNew : System.Web.UI.Page
    {
        string studentId;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (Request.QueryString["personId"] != null)
                {
                    studentId = Request.QueryString["personId"];

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            BuildStudentTranscript(studentId, false);
        }
        private void BuildStudentTranscript(string sId, bool studentCopy)
        {
            try
            {
                long studentId = Convert.ToInt64(sId);

                PersonLogic personLogic = new PersonLogic();
                PERSON person = new PERSON();
                StudentLogic studentLogic = new StudentLogic();
                STUDENT student = new STUDENT();
                List<PersonReport> personReport = new List<PersonReport>();
                //StudentCourseMarkLogic courseMarkLogic = new StudentCourseMarkLogic();
                StudentResultLogic studentResultLogic = new StudentResultLogic();

                person = personLogic.GetEntityBy(p => p.Person_Id == studentId);
                student = studentLogic.GetEntityBy(p => p.Person_Id == studentId);

                StudentClassListReport CGPA = studentResultLogic.GetStudentWithCGPA(student);
                personReport = personLogic.GetPersonInfromation(student, CGPA);

                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == studentId).LastOrDefault();

                Level firstLevel = new Level();
                Level secondLevel = new Level();

                int[] hndProgrammes = { (int)Programmes.HNDEvening, (int)Programmes.HNDFullTime, (int)Programmes.HNDWeekend };

                if (studentLevel != null && hndProgrammes.Contains(studentLevel.Programme.Id))
                {
                    firstLevel = new Level() { Id = (int)Levels.HNDI };
                    secondLevel = new Level() { Id = (int)Levels.HNDII };
                }
                else
                {
                    firstLevel = new Level() { Id = (int)Levels.NDI };
                    secondLevel = new Level() { Id = (int)Levels.NDII };
                }

                CourseRegistration firstCourseRegistration = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == firstLevel.Id).FirstOrDefault();
                //CourseRegistration secondCourseRegistration = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == secondLevel.Id).FirstOrDefault();
                List<CourseRegistration> otherCourseRegistrations = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == secondLevel.Id);

                CourseRegistration secondCourseRegistration = new CourseRegistration();
                CourseRegistration thirdCourseRegistration = new CourseRegistration();

                if (otherCourseRegistrations.Count > 1)
                {
                    int firstSessionStart = Convert.ToInt32(otherCourseRegistrations[0].Session.Name.Split('/').FirstOrDefault());
                    int secondSessionStart = Convert.ToInt32(otherCourseRegistrations[1].Session.Name.Split('/').FirstOrDefault());

                    if (firstSessionStart < secondSessionStart)
                    {
                        secondCourseRegistration = otherCourseRegistrations[0];
                        thirdCourseRegistration = otherCourseRegistrations[1];
                    }
                    else
                    {
                        secondCourseRegistration = otherCourseRegistrations[1];
                        thirdCourseRegistration = otherCourseRegistrations[0];
                    }
                }
                else if (otherCourseRegistrations.Count > 0)
                {
                    secondCourseRegistration = otherCourseRegistrations[0];
                }

                Semester firstSemester = new Semester() { Id = 1 };
                Semester secondSemester = new Semester() { Id = 2 };
                List<SemesterReport> firstSemesterReport = studentResultLogic.GetSemesterResultInformation(firstCourseRegistration, firstSemester);
                List<SemesterReport> secondSemesterReport = studentResultLogic.GetSemesterResultInformation(firstCourseRegistration, secondSemester);

                var distinctSessions = firstSemesterReport.Select(s => s.SessionId).Distinct().ToList();
                for (int i = 0; i < distinctSessions.Count; i++)
                {
                    int sessionId = distinctSessions[i];
                    if (i <= 1)
                    {
                        List<SemesterReport> firstSemesterResultList = firstSemesterReport.Where(s => s.SessionId == sessionId).ToList();
                        List<SemesterReport> secondSemesterResultList = secondSemesterReport.Where(s => s.SessionId == sessionId).ToList();
                        var gradePointUnit = firstSemesterResultList.Sum(s => s.GradePoint_CourseUnit) + secondSemesterResultList.Sum(s => s.GradePoint_CourseUnit);
                        var courseUnit = firstSemesterResultList.Sum(s => s.CourseUnit) + secondSemesterResultList.Sum(s => s.CourseUnit);
                        decimal semsterCgpa = Math.Round((gradePointUnit / courseUnit), 2);

                        for (int j = 0; j < firstSemesterReport.Count; j++)
                        {
                            if (sessionId == firstSemesterReport[j].SessionId)
                            {
                                firstSemesterReport[j].GPA = semsterCgpa;
                            }

                        }
                        for (int k = 0; k < secondSemesterReport.Count; k++)
                        {
                            if (sessionId == secondSemesterReport[k].SessionId && i != 1)
                            {
                                secondSemesterReport[k].GPA = semsterCgpa;
                            }

                        }
                    }
                }
                string bind_dsPerson = "dsPerson";
                string bind_dsSemester1 = "dsSemester1";
                string bind_dsSemester2 = "dsSemester2";
                string reportPath = null;
                reportPath = studentCopy ? @"Reports\Result\StudentCopyResultTranscript.rdlc" : @"Reports\Result\ResultTranscriptNew.rdlc";

                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DisplayName = "Result Transcript ";
                ReportViewer1.LocalReport.ReportPath = reportPath;
                ReportViewer1.LocalReport.EnableExternalImages = true;

                if (personReport != null && firstSemesterReport != null && secondSemesterReport != null)
                {
                    if (studentCopy)
                    {
                        string waterMarkFilePath = File.Exists(Server.MapPath("~/Content/Images/studentsCopy.jpeg")) ? Server.MapPath("~/Content/Images/studentsCopy.jpeg") : Server.MapPath(GenerateWaterMark());
                        string imageFilePath = GenerateQrCode(studentId.ToString());
                        string imagePath = new Uri(Server.MapPath(imageFilePath)).AbsoluteUri;
                        string waterMarkPath = new Uri(waterMarkFilePath).AbsoluteUri;
                        ReportParameter parameter = new ReportParameter("ImagePath", imagePath);
                        ReportParameter parameter2 = new ReportParameter("WaterMark", waterMarkPath);
                        ReportViewer1.LocalReport.SetParameters(new[] { parameter, parameter2 });

                    }
                    else
                    {
                        //string imageFilePath = "~/Content/QRCodes/" + studentId + ".Jpeg";
                        string imageFilePath = GenerateQrCode(studentId.ToString());
                        string imagePath = new Uri(Server.MapPath(imageFilePath)).AbsoluteUri;
                        ReportParameter parameter = new ReportParameter("ImagePath", imagePath);
                        ReportViewer1.LocalReport.SetParameters(new[] { parameter });
                    }
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsPerson.Trim(), personReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester1.Trim(), firstSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester2.Trim(), secondSemesterReport));
                    ReportViewer1.LocalReport.Refresh();

                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        public string GenerateQrCode(string personId, string alt = "QR code", int height = 100, int width = 100, int margin = 0)
        {
            try
            {
                string folderPath = "~/Content/QRCodes/";
                string imagePath = "~/Content/QRCodes/" + personId + ".Jpeg";
                string verifyUrl = "localhost:2600/Reports/Presenter/ResultTranscript?personId=" + personId;

                string wildCard = personId + "*.*";
                IEnumerable<string> files = Directory.EnumerateFiles(Server.MapPath("~/Content/QRCodes/"), wildCard, SearchOption.TopDirectoryOnly);

                if (files != null && files.Count() > 0)
                {
                    return imagePath;
                }
                // If the directory doesn't exist then create it.
                if (!Directory.Exists(Server.MapPath(folderPath)))
                {
                    Directory.CreateDirectory(folderPath);
                }

                var barcodeWriter = new BarcodeWriter();
                barcodeWriter.Format = BarcodeFormat.QR_CODE;
                var result = barcodeWriter.Write(verifyUrl);

                string barcodePath = Server.MapPath(imagePath);
                var barcodeBitmap = new Bitmap(result);
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(barcodePath, FileMode.Create))
                    {
                        barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }

                return imagePath;
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            BuildStudentTranscript(studentId, true);
        }
        public string GenerateWaterMark()
        {
            string returnImagePath = "~/Content/Junk/defaultImage1.bmp";
            string imagePath = Server.MapPath("~/Content/Junk/defaultImage1.bmp");
            string defultImagePath = Server.MapPath("~/Content/Junk/defaultImage.bmp");
            string reportMark = "STUDENT'S COPY";
            Bitmap bmp = new Bitmap(defultImagePath);
            RectangleF rectf = new RectangleF(0, 0, bmp.Width, bmp.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;
            g.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            StringFormat format = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
            g.DrawString(reportMark, new Font("Tahoma", 16), Brushes.RoyalBlue, rectf, format);
            g.Flush();
            using (MemoryStream memory = new MemoryStream())
            {
                using (FileStream fs = new FileStream(imagePath, FileMode.Create))
                {
                    bmp.Save(memory, ImageFormat.Bmp);
                    byte[] bytes = memory.ToArray();
                    fs.Write(bytes, 0, bytes.Length);
                }


            }
            return returnImagePath;
        }
    }
}