﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ResultTranscriptNew.aspx.cs" Inherits="Abundance_Nk.Web.Reports.Presenter.ResultTranscriptNew" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   
    <title></title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
</head>
<body>


    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="60000">
     
        </asp:ScriptManager>
        <div>
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <p>
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </p>
           
             <div class="form-group">
                  <asp:Button ID="Button1" runat="server" Text="Display Report" class="btn" OnClick="Button1_Click" />
                  <asp:Button ID="Button2" runat="server" Text="Display Student Copy" class="btn" OnClick="Button2_Click" />
                                 <asp:UpdateProgress ID="UpdateProgress1" AssociatedUpdatePanelID="UpdatePanel1" runat="server">
                                <ProgressTemplate>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Content/Images/bx_loader.gif" /> Loading...
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                                 </div>
               <rsweb:ReportViewer ID="ReportViewer1" runat="server" style="margin-left: 0px; margin-right: 0px; margin-top: 15px;" Width="100%" >
        </rsweb:ReportViewer>
        </ContentTemplate>
</asp:UpdatePanel>
</div>
   

    <script src="~/Scripts/jquery-2.1.3.js"></script>
    <script src="../../Scripts/jquery-2.1.3.js"></script>
    <script src="../../Scripts/bootstrap.min.js"></script>
 
     

    </form>
     
    </body>
</html>

 