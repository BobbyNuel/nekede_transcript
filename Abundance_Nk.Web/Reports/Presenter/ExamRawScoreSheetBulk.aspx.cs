﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using Microsoft.AspNet.Identity;
namespace Abundance_Nk.Web.Reports.Presenter
{

    public partial class ExamRawScoreSheetBulk : System.Web.UI.Page
    {
        List<Semester> semesters = new List<Semester>();
        Session session = null;
        Semester semester = null;
        Level level = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                Display_Button.Visible = true;

               if (!IsPostBack)
                {
                    Utility.BindDropdownItem(ddlSession, Utility.GetAllSessions(), Utility.ID, Utility.NAME);

                    ddlSemester.Visible = false;

                    Utility.BindDropdownItem(ddlLevel, Utility.GetAllLevels(), Utility.ID, Utility.NAME);
                }
        
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }

        public Session SelectedSession
        {
            get { return new Session() { Id = Convert.ToInt32(ddlSession.SelectedValue), Name = ddlSession.SelectedItem.Text }; }
            set { ddlSession.SelectedValue = value.Id.ToString(); }
        }
        public Semester SelectedSemester
        {
            get { return new Semester() { Id = Convert.ToInt32(ddlSemester.SelectedValue), Name = ddlSemester.SelectedItem.Text }; }
            set { ddlSemester.SelectedValue = value.Id.ToString(); }
        }
        public Level SelectedLevel
        {
            get { return new Level() { Id = Convert.ToInt32(ddlLevel.SelectedValue), Name = ddlLevel.SelectedItem.Text }; }
            set { ddlLevel.SelectedValue = value.Id.ToString(); }
        }
        protected void Display_Button_Click1(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    lblMessage.Text = "All fields must be selected";
                    return;
                }

                DisplayReportBy(SelectedSession, SelectedSemester, SelectedLevel);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message; ;
            }
        }
        private bool InvalidUserInput()
        {
            try
            {
                if (SelectedSession == null || SelectedSession.Id <= 0 || SelectedLevel == null || SelectedLevel.Id <= 0)
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void DisplayReportBy(Session session, Semester semester, Level level)
        {
            try
            {
                //StudentExamRawScoreSheetResultLogic studentExamRawScoreSheetResultLogic = new StudentExamRawScoreSheetResultLogic();
                //List<ExamRawScoreSheetReport> examRawScoreSheetReportList = studentExamRawScoreSheetResultLogic.GetScoreSheetBy(session, semester, course, department, level, programme);
                //string bind_dsStudentPaymentSummary = "dsExamRawScoreSheet";
                //string reportPath = @"Reports\Result\ExamRawScoreSheet.rdlc";

                //ReportViewer1.Reset();
                //ReportViewer1.LocalReport.DisplayName = "Examination Raw Score Sheet ";
                //ReportViewer1.LocalReport.ReportPath = reportPath;

                //if (examRawScoreSheetReportList != null)
                //{
                //    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                //    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsStudentPaymentSummary.Trim(), examRawScoreSheetReportList));
                //    ReportViewer1.LocalReport.Refresh();
                //    ReportViewer1.Visible = true;
                //}

                LevelLogic levelLogic = new LevelLogic();
                Level selectedLevel = levelLogic.GetModelBy(l => l.Level_Id == level.Id);

                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();

                List<Programme> programmes = programmeLogic.GetAll();

                if (Directory.Exists(Server.MapPath("~/Content/temp")))
                {
                    Directory.Delete(Server.MapPath("~/Content/temp"), true);
                }

                Directory.CreateDirectory(Server.MapPath("~/Content/temp"));

                string zipName = "ScoreSheet";

                for (int i = 0; i < programmes.Count; i++)
                {
                    Programme currentProgramme = programmes[i];
                    List<Department> departments = departmentLogic.GetBy(currentProgramme);

                    for (int j = 0; j < departments.Count; j++)
                    {
                        Department currentDepartment = departments[j];

                        CourseLogic courseLogic = new CourseLogic();
                        List<Course> courses = courseLogic.GetModelsBy(p => p.Department_Id == currentDepartment.Id && p.Level_Id == level.Id && p.Semester_Id == semester.Id);

                        for (int k = 0; k < courses.Count; k++)
                        {
                            Course currentCourse = courses[k];

                            StudentExamRawScoreSheetResultLogic studentExamRawScoreSheetResultLogic = new StudentExamRawScoreSheetResultLogic();
                            List<ExamRawScoreSheetReport> examRawScoreSheetReportList = studentExamRawScoreSheetResultLogic.GetScoreSheetBy(session, semester, currentCourse, currentDepartment, level, currentProgramme);

                            if (examRawScoreSheetReportList.Count > 0)
                            {
                                Warning[] warnings;
                                string[] streamIds;
                                string mimeType = string.Empty;
                                string encoding = string.Empty;
                                string extension = string.Empty;

                                string bind_dsStudentPaymentSummary = "dsExamRawScoreSheet";
                                string reportPath = @"Reports\Result\ExamRawScoreSheet.rdlc";

                                ReportViewer rptViewer = new ReportViewer();
                                rptViewer.Visible = false;
                                rptViewer.Reset();
                                rptViewer.LocalReport.DisplayName = "ScoreSheet";
                                rptViewer.ProcessingMode = ProcessingMode.Local;
                                rptViewer.LocalReport.ReportPath = reportPath;
                                rptViewer.LocalReport.EnableExternalImages = true;
                                rptViewer.LocalReport.DataSources.Add(new ReportDataSource(bind_dsStudentPaymentSummary.Trim(), examRawScoreSheetReportList));

                                byte[] bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                                string path = Server.MapPath("~/Content/temp");
                                string savelocation = Path.Combine(path, currentProgramme.ShortName.Replace("/", "_") + " " + currentDepartment.Name.Replace("/", "_") + " " + selectedLevel.Name.Replace("/", "_") + " " + currentCourse.Code.Replace("/", "_") + ".pdf");
                                File.WriteAllBytes(savelocation, bytes);
                            } 
                        }

                    }
                }
                using (ZipFile zip = new ZipFile())
                {
                    string file = Server.MapPath("~/Content/temp/");
                    zip.AddDirectory(file, "");
                    string zipFileName = zipName;
                    zip.Save(file + zipFileName + ".zip");
                    string export = "~/Content/temp/" + zipFileName + ".zip";

                    Response.Redirect(export, false);
                    return;
                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message; 
            }
        }

        protected void ddlSession_SelectedIndexChanged1(object sender, EventArgs e)
        {
            Session session = new Session() { Id = Convert.ToInt32(ddlSession.SelectedValue) };
            SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
            List<SessionSemester> semesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);
            foreach (SessionSemester sessionSemester in semesterList)
            {

                semesters.Add(sessionSemester.Semester);
            }
            semesters.Insert(0, new Semester() { Name = "-- Select Semester--" });
            Utility.BindDropdownItem(ddlSemester, semesters, Utility.ID, Utility.NAME);
            ddlSemester.Visible = true;
        }

    }
}