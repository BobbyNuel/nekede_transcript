﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using Abundance_Nk.Web.Models;
using Microsoft.Reporting.WebForms;
using Abundance_Nk.Web.Models.Intefaces;

namespace Abundance_Nk.Web.Reports.Presenter.Result
{
    public partial class StatementOfSemesterResult : System.Web.UI.Page, IReport
    {
        private MailMessage mail;
        public string Message
        {
            get { return lblMessage.Text; }
            set { lblMessage.Text = value; }
        }

        public ReportViewer Viewer
        {
            get { return rv; }
            set { rv = value; }
        }

        public int ReportType
        {
            get { throw new NotImplementedException(); }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Message = "";

                if (!IsPostBack)
                {
                    PopulateAllDropDown();
                    ddlStudent.Visible = false;
                    ddlDepartment.Visible = false;
                    ddlDepartment.Visible = false;
                    ddlProgramme.Visible = false;
                    ddlSession.Visible = false;
                    ddlLevel.Visible = false;
                    btnDisplayReport.Visible = false;
                    if (Request["sid"] != null && Request["sesId"] != null)
                    {
                        long sid = Convert.ToInt64(Request["sid"].ToString());
                        int sesid = Convert.ToInt32(Request["sesId"].ToString());
                        // Response.Write(sid);
                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                        SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Session_Semester_Id == sesid);
                        StudentLevel studentLevel = new StudentLevel();
                        StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                        studentLevel = studentLevelLogic.GetModelsBy(sl => sl.Person_Id == sid && sl.Session_Id == sessionSemester.Session.Id).LastOrDefault();
                        if (studentLevel != null)
                        {  
                            DisplayReportBy(sessionSemester, studentLevel.Level, studentLevel.Programme, studentLevel.Department, studentLevel.Student);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        public Level Level
        {
            get { return new Level() { Id = Convert.ToInt32(ddlLevel.SelectedValue), Name = ddlLevel.SelectedItem.Text }; }
            set { ddlLevel.SelectedValue = value.Id.ToString(); }
        }

        public Programme Programme
        {
            get { return new Programme() { Id = Convert.ToInt32(ddlProgramme.SelectedValue), Name = ddlProgramme.SelectedItem.Text }; }
            set { ddlProgramme.SelectedValue = value.Id.ToString(); }
        }

        public Department Department
        {
            get { return new Department() { Id = Convert.ToInt32(ddlDepartment.SelectedValue), Name = ddlDepartment.SelectedItem.Text }; }
            set { ddlDepartment.SelectedValue = value.Id.ToString(); }
        }

        public Student Student
        {
            get { return new Student() { Id = Convert.ToInt32(ddlStudent.SelectedValue), Name = ddlStudent.SelectedItem.Text }; }
            set { ddlStudent.SelectedValue = value.Id.ToString(); }
        }
        public SessionSemester SelectedSession
        {
            get { return new SessionSemester() { Id = Convert.ToInt32(ddlSession.SelectedValue), Name = ddlSession.SelectedItem.Text }; }
            set { ddlSession.SelectedValue = value.Id.ToString(); }
        }

        private void DisplayReportBy(SessionSemester session, Level level, Programme programme, Department department, Student student)
        {
            try
            {
                //ScoreGradeLogic scoreGradeLogic = new ScoreGradeLogic();
                StudentResultLogic resultLogic = new StudentResultLogic();
                List<Model.Model.Result> results = resultLogic.GetStudentResultBy(session, level, programme, department, student);

                List<StatementOfResultSummary> resultSummaries = resultLogic.GetStatementOfResultSummaryBy(session, level, programme, department, student);

                SendMail(results, resultSummaries);

                
                string bind_ds = "dsMasterSheet";
                string bind_resultSummary = "dsResultSummary";
               // string reportPath = @"Reports\Result\StatementOfSemesterResult.rdlc";
                string reportPath = @"Reports\Result\StatementOfResult.rdlc";
                
                if (results != null && results.Count > 0)
                {
                    string appRoot = ConfigurationManager.AppSettings["AppRoot"];
                    foreach (Model.Model.Result result in results)
                    {
                        if (!string.IsNullOrWhiteSpace(result.PassportUrl))
                        {
                            result.PassportUrl = appRoot + result.PassportUrl;
                        }
                        else
                        {
                            result.PassportUrl = appRoot + Utility.DEFAULT_AVATAR;
                        }
                    }
                }

                rv.Reset();
                rv.LocalReport.DisplayName = "Statement of Result";
                rv.LocalReport.ReportPath = reportPath;
                rv.LocalReport.EnableExternalImages = true;

                string programmeName = programme.Id > 2 ? "Higher National Diploma" : "National Diploma";
                //ReportParameter programmeParam = new ReportParameter("Programme", programmeName);
                //ReportParameter departmentParam = new ReportParameter("Department", department.Name);
                //ReportParameter sessionSemesterParam = new ReportParameter("SessionSemester", session.Name);
                //ReportParameter abbreviationsParam = new ReportParameter("Abbreviations", abbreviations);
                //ReportParameter scoreGradingKeysParam = new ReportParameter("ScoreGradingKeys", scoreGradingKeys);
                //ReportParameter[] reportParams = new ReportParameter[] { departmentParam, programmeParam, sessionSemesterParam, abbreviationsParam, scoreGradingKeysParam };

                //rv.LocalReport.SetParameters(reportParams);

                if (results != null && results.Count > 0)
                {
                    rv.ProcessingMode = ProcessingMode.Local;
                    rv.LocalReport.DataSources.Add(new ReportDataSource(bind_ds.Trim(), results));
                    rv.LocalReport.DataSources.Add(new ReportDataSource(bind_resultSummary.Trim(), resultSummaries));
                    
                    rv.LocalReport.Refresh();
                    rv.Visible = true;
                }
                else
                {
                    lblMessage.Text = "No result to display";
                    rv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }

        private void SendMail(List<Model.Model.Result> result, List<StatementOfResultSummary> resultSummary)
        {
            try
            {
                if (result.Count == 0 || result.LastOrDefault().Email == null)
                {
                   return; 
                }

                string resultBody = "Dear " + result.LastOrDefault().Name  + " " + result.LastOrDefault().MatricNumber + "," + "\n";
                resultBody += "Find below your semester result:" + "\n\n";

                string GPA = "";
                if(resultSummary.Count > 0)
                {
                    GPA = resultSummary[2].CurrentSemester;
                }
                    
                //mail = new MailMessage();
                ////mail.To.Add(result.LastOrDefault().Email);
                //mail.To.Add("william.enemali@lloydant.com");
                //mail.From = new MailAddress("lloydant@fpno.edu.ng", "NEKEDEPOLY");

                //mail.Subject = "SEMESTER RESULT";
                for (int i = 0; i < result.Count; i++)
                {
                    resultBody += result[i].CourseCode + ": " + result[i].Grade + "\n";
                }

                resultBody += "\n GPA: " + GPA;

                EmailClient emailClient = new EmailClient("SEMESTER RESULT", resultBody, result.LastOrDefault().Email);
               
                //grading = resultSummary.LastOrDefault().AllSemester;
                //mail.Body = resultBody;
                //mail.IsBodyHtml = true;
                //SmtpClient smtp = new SmtpClient();
                //smtp.Host = "smtp.office365.com";
                //smtp.Port = 25;
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new NetworkCredential("lloydant@fpno.edu.ng", "L44jesus");
                //smtp.EnableSsl = true;
                //smtp.Send(mail);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        long studentId = 39199;// Convert.ToInt64(Request.QueryString["sid"]);
        //        if (studentId > 0)
        //        {
        //            Student student = new Student() { Id = studentId };
        //            StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
        //            SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
        //            SessionSemester sessionSemester = sessionSemesterLogic.GetBy(SelectedSession.Id);
        //            StudentLevel studentLevel = studentLevelLogic.GetBy(student, sessionSemester.Session);
        //            if (studentLevel != null)
        //            {
        //                DisplayReportBy(SelectedSession, studentLevel.Level, studentLevel.Programme, studentLevel.Department, student);
        //            }
        //            else
        //            {
        //                lblMessage.Text = "No result to display";
        //                rv.Visible = false;
        //            }
        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        lblMessage.Text = ex.Message;
        //    }
        //}



        private void PopulateAllDropDown()
        {
            try
            {
                Utility.BindDropdownItem(ddlSession, Utility.GetAllSessionSemesters(), Utility.ID, Utility.NAME);
                Utility.BindDropdownItem(ddlLevel, Utility.GetAllLevels(), Utility.ID, Utility.NAME);
                Utility.BindDropdownItem(ddlProgramme, Utility.GetAllProgrammes(), Utility.ID, Utility.NAME);
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        private bool InvalidUserInput()
        {
            try
            {
                if (Level == null || Level.Id <= 0)
                {
                    lblMessage.Text = "Please select Level";
                    return true;
                }
                else if (Programme == null || Programme.Id <= 0)
                {
                    lblMessage.Text = "Please select Programme";
                    return true;
                }
                else if (Department == null || Department.Id <= 0)
                {
                    lblMessage.Text = "Please select Department";
                    return true;
                }
                else if (SelectedSession == null || SelectedSession.Id <= 0)
                {
                    lblMessage.Text = " Session not set! Please contact your system administrator.";
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void btnDisplayReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    return;
                }
                else if (Student == null || Student.Id <= 0)
                {
                    lblMessage.Text = "Please select Student";
                    return;
                }

                
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                SessionSemester sessionSemester = sessionSemesterLogic.GetBy(SelectedSession.Id);
                StudentLevel studentLevel = studentLevelLogic.GetBy(Student, sessionSemester.Session);
                if (studentLevel != null)
                {
                    DisplayReportBy(SelectedSession, studentLevel.Level, studentLevel.Programme, studentLevel.Department, Student);
                }
                else
                {
                    lblMessage.Text = "No result to display";
                    rv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        protected void ddlProgramme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Programme != null && Programme.Id > 0)
                {
                    PopulateDepartmentDropdownByProgramme(Programme);
                }
                else
                {
                    ddlDepartment.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }


        private void PopulateDepartmentDropdownByProgramme(Programme programme)
        {
            try
            {
                List<Department> departments = Utility.GetDepartmentByProgramme(programme);
                if (departments != null && departments.Count > 0)
                {
                    Utility.BindDropdownItem(ddlDepartment, Utility.GetDepartmentByProgramme(programme), Utility.ID, Utility.NAME);
                    ddlDepartment.Visible = true;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (InvalidUserInput())
                {
                    return;
                }

                rv.LocalReport.DataSources.Clear();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                SessionSemester sessionSemester = sessionSemesterLogic.GetBy(SelectedSession.Id);
                List<Student> students = Utility.GetStudentsBy(Level, Programme, Department, sessionSemester.Session);
                if (students != null && students.Count > 0)
                {
                    Utility.BindDropdownItem(ddlStudent, students, Utility.ID, "FirstName");
                    ddlStudent.Visible = true;
                    rv.Visible = true;
                }
                else
                {
                    ddlStudent.Visible = false;
                    rv.Visible = false;
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }

        
    }
}