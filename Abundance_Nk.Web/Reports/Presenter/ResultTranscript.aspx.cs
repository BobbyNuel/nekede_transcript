﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
//using Abundance_Nk.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZXing;

namespace Abundance_Nk.Web.Reports.Presenter
{

    public partial class ResultTranscript : System.Web.UI.Page
    {
        string studentId;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";
                if (Request.QueryString["personId"] != null)
                {
                    studentId = Request.QueryString["personId"];

                }

            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            BuildStudentTranscript(studentId);
        }
        private void BuildStudentTranscript(string sId)
        {
            try
            {
                long studentId = Convert.ToInt64(sId);

                PersonLogic personLogic = new PersonLogic();
                PERSON person = new PERSON();
                StudentLogic studentLogic = new StudentLogic();
                STUDENT student = new STUDENT();
                List<PersonReport> personReport = new List<PersonReport>();
                //StudentCourseMarkLogic courseMarkLogic = new StudentCourseMarkLogic();
                StudentResultLogic studentResultLogic = new StudentResultLogic();

                person = personLogic.GetEntityBy(p => p.Person_Id == studentId);
                student = studentLogic.GetEntityBy(p => p.Person_Id == studentId);

                StudentClassListReport CGPA = studentResultLogic.GetStudentWithCGPA(student);
                personReport = personLogic.GetPersonInfromation(student, CGPA);

               CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == studentId).LastOrDefault();

                Level firstLevel = new Level(); 
                Level secondLevel = new Level(); 

                int[] hndProgrammes = { (int)Programmes.HNDEvening, (int)Programmes.HNDFullTime, (int)Programmes.HNDWeekend };

                if (studentLevel != null && hndProgrammes.Contains(studentLevel.Programme.Id))
                {
                    firstLevel = new Level(){ Id = (int)Levels.HNDI };
                    secondLevel = new Level() { Id = (int)Levels.HNDII };
                }
                else
                {
                    firstLevel = new Level() { Id = (int)Levels.NDI };
                    secondLevel = new Level() { Id = (int)Levels.NDII };
                }

                CourseRegistration firstCourseRegistration = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == firstLevel.Id).FirstOrDefault();
                //CourseRegistration secondCourseRegistration = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == secondLevel.Id).FirstOrDefault();
                List<CourseRegistration> otherCourseRegistrations = courseRegistrationLogic.GetModelsBy(c => c.Person_Id == studentId && c.Level_Id == secondLevel.Id);

                CourseRegistration secondCourseRegistration = new CourseRegistration();
                CourseRegistration thirdCourseRegistration = new CourseRegistration();

                if (otherCourseRegistrations.Count > 1)
                {
                    int firstSessionStart = Convert.ToInt32(otherCourseRegistrations[0].Session.Name.Split('/').FirstOrDefault());
                    int secondSessionStart = Convert.ToInt32(otherCourseRegistrations[1].Session.Name.Split('/').FirstOrDefault());

                    if (firstSessionStart < secondSessionStart)
                    {
                        secondCourseRegistration = otherCourseRegistrations[0];
                        thirdCourseRegistration = otherCourseRegistrations[1];
                    }
                    else
                    {
                        secondCourseRegistration = otherCourseRegistrations[1];
                        thirdCourseRegistration = otherCourseRegistrations[0];
                    }
                }
                else if (otherCourseRegistrations.Count > 0)
                {
                    secondCourseRegistration = otherCourseRegistrations[0];
                }

                Semester firstSemester = new Semester(){ Id = 1 };
                Semester secondSemester = new Semester(){ Id = 2 };

                List<SemesterReport> firstSemesterReport = studentResultLogic.GetSemesterResultInformation(firstCourseRegistration, firstSemester, firstLevel);
                List<SemesterReport> secondSemesterReport = studentResultLogic.GetSemesterResultInformation(firstCourseRegistration, secondSemester, firstLevel);
                List<SemesterReport> thirdSemesterReport = studentResultLogic.GetSemesterResultInformation(secondCourseRegistration, firstSemester, secondLevel);
                List<SemesterReport> fourthSemesterReport = studentResultLogic.GetSemesterResultInformation(secondCourseRegistration, secondSemester, secondLevel);
                List<SemesterReport> fifthSemesterReport = studentResultLogic.GetSemesterResultInformation(thirdCourseRegistration, firstSemester, secondLevel);
                List<SemesterReport> sixthSemesterReport = studentResultLogic.GetSemesterResultInformation(thirdCourseRegistration, secondSemester, secondLevel);

                string bind_dsPerson = "dsPerson";
                string bind_dsSemester1 = "dsSemester1";
                string bind_dsSemester2 = "dsSemester2";
                string bind_dsSemester3 = "dsSemester3";
                string bind_dsSemester4 = "dsSemester4";
                string bind_dsSemester5 = "dsSemester5";
                string bind_dsSemester6 = "dsSemester6";
                string reportPath = @"Reports\ResultTranscript.rdlc";

                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DisplayName = "Result Transcript ";
                ReportViewer1.LocalReport.ReportPath = reportPath;
                ReportViewer1.LocalReport.EnableExternalImages = true;

                if (personReport != null && firstSemesterReport != null && secondSemesterReport != null && thirdSemesterReport != null && fourthSemesterReport != null)
                {
                    //string imageFilePath = "~/Content/QRCodes/" + studentId + ".Jpeg";
                    string imageFilePath = GenerateQrCode(studentId.ToString());
                    string imagePath = new Uri(Server.MapPath(imageFilePath)).AbsoluteUri;

                    ReportParameter parameter = new ReportParameter("ImagePath", imagePath);
                    ReportViewer1.LocalReport.SetParameters(new[] { parameter });

                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsPerson.Trim(), personReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester1.Trim(), firstSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester2.Trim(), secondSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester3.Trim(), thirdSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester4.Trim(), fourthSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester5.Trim(), fifthSemesterReport));
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsSemester6.Trim(), sixthSemesterReport));
                    ReportViewer1.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message;
            }
        }
        public string GenerateQrCode(string personId, string alt = "QR code", int height = 100, int width = 100, int margin = 0)
        {
            try
            {
                string folderPath = "~/Content/QRCodes/";
                string imagePath = "~/Content/QRCodes/" + personId + ".Jpeg";
                string verifyUrl = "localhost:2600/Reports/Presenter/ResultTranscript?personId=" + personId;

                string wildCard = personId + "*.*";
                IEnumerable<string> files = Directory.EnumerateFiles(Server.MapPath("~/Content/QRCodes/"), wildCard, SearchOption.TopDirectoryOnly);

                if (files != null && files.Count() > 0)
                {
                    return imagePath;
                }
                // If the directory doesn't exist then create it.
                if (!Directory.Exists(Server.MapPath(folderPath)))
                {
                    Directory.CreateDirectory(folderPath);
                }

                var barcodeWriter = new BarcodeWriter();
                barcodeWriter.Format = BarcodeFormat.QR_CODE;
                var result = barcodeWriter.Write(verifyUrl);

                string barcodePath = Server.MapPath(imagePath);
                var barcodeBitmap = new Bitmap(result);
                using (MemoryStream memory = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(barcodePath, FileMode.Create))
                    {
                        barcodeBitmap.Save(memory, ImageFormat.Jpeg);
                        byte[] bytes = memory.ToArray();
                        fs.Write(bytes, 0, bytes.Length);
                    }
                }

                return imagePath;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}