﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;
using Ionic.Zip;
using Microsoft.Reporting.WebForms;

namespace Abundance_Nk.Web.Reports.Presenter
{
    public partial class NDMorningRankingReport : System.Web.UI.Page
    {
        List<Department> departments;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                lblMessage.Text = "";

                if (!IsPostBack)
                {
                    Programme programme = new Programme() { Id = 5};
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    departments = departmentLogic.GetBy(programme);
                    departments.Insert(0, new Department(){ Id = 0, Name = "-- Select Department --"});

                    Utility.BindDropdownItem(ddlDepartment, departments, Utility.ID, Utility.NAME);
                    Utility.BindDropdownItem(ddlSession, Utility.GetAllSessions(), Utility.ID, Utility.NAME);

                    rbnFormType.SelectedValue = "1";
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message;
            }
        }
        public Session SelectedSession
        {
            get { return new Session() { Id = Convert.ToInt32(ddlSession.SelectedValue), Name = ddlSession.SelectedItem.Text }; }
            set { ddlSession.SelectedValue = value.Id.ToString(); }
        }
        public Department SelectedDepartment
        {
            get { return new Department() { Id = Convert.ToInt32(ddlDepartment.SelectedValue), Name = ddlDepartment.SelectedItem.Text }; }
            set { ddlDepartment.SelectedValue = value.Id.ToString(); }
        }

        private void DisplayReport(Session session, Department department)
        {
            try
            {
                ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();

                int formType = Convert.ToInt32(rbnFormType.SelectedValue);
                if (formType > 0)
                {
                    formType = formType == 1 ? (int)FeeTypes.PutmeForm : (int)FeeTypes.SupplementaryForm;
                }

                var report = applicantJambDetailLogic.GetNDMApplicantRanking(department, session, txtBoxDateFrom.Text, txtBoxDateTo.Text, formType);
                
                string bind_dsNDMApplicants = "dsNDMApplicant";
                string reportPath = "";

                int sortOption = Convert.ToInt32(rbnSortOption.SelectedValue);
                if (sortOption == 1)
                {
                    reportPath = @"Reports\NDMApplicantRanking.rdlc";
                }
                if (sortOption == 2)
                {
                    reportPath = @"Reports\NDMApplicantRankingByJamb.rdlc";
                }
                if (sortOption == 3)
                {
                    reportPath = @"Reports\NDMApplicantRankingByName.rdlc";
                }
                if (sortOption == 4)
                {
                    reportPath = @"Reports\NDMApplicantRankingByAggregate.rdlc";
                }

                ReportViewer1.Reset();
                ReportViewer1.LocalReport.DisplayName = "Ranking Report ";
                ReportViewer1.LocalReport.ReportPath = reportPath;

                if (report != null)
                {
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource(bind_dsNDMApplicants.Trim(), report));
                    ReportViewer1.LocalReport.Refresh();
                }
            }
            catch (Exception ex)
            {
                lblMessage.Text = ex.Message + ex.InnerException.Message; ;
            }
        }
        private bool InvalidUserInput()
        {
            try
            {
                if (SelectedSession == null || SelectedSession.Id <= 0 || SelectedDepartment == null || SelectedDepartment.Id <= 0 )
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        protected void Display_Button_Click(object sender, EventArgs e)
        {
            if (InvalidUserInput())
            {
                lblMessage.Text = "All fields must be selected";
                return;
            }  

            DisplayReport(SelectedSession, SelectedDepartment);

        }
        protected void btnBulk_Click(object sender, EventArgs e)
        {
            try
            {
                if (SelectedSession == null || SelectedSession.Id <= 0)
                {
                    lblMessage.Text = "Please Check your selection";
                    return;
                }

                if (Directory.Exists(Server.MapPath("~/Content/temp")))
                {
                    Directory.Delete(Server.MapPath("~/Content/temp"), true);
                }

                Directory.CreateDirectory(Server.MapPath("~/Content/temp"));

                int sortOption = Convert.ToInt32(rbnSortOption.SelectedValue);
                int bulkOption = Convert.ToInt32(rbnBulkOption.SelectedValue);

                string zipName = "Ranking Report ";

                DepartmentLogic departmentLogic = new DepartmentLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();

                Programme programme = programmeLogic.GetModelBy(p => p.Programme_Id == (int)Programmes.NDMorning);
                ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();

                List<Department> departments = departmentLogic.GetBy(programme);

                int formType = Convert.ToInt32(rbnFormType.SelectedValue);
                if (formType > 0)
                {
                    formType = formType == 1 ? (int)FeeTypes.PutmeForm : (int)FeeTypes.SupplementaryForm;
                }

                for (int j = 0; j < departments.Count; j++)
                {
                    Department currentDepartment = departments[j];
                    var report = applicantJambDetailLogic.GetNDMApplicantRanking(currentDepartment, SelectedSession, txtBoxDateFrom.Text, txtBoxDateTo.Text, formType);

                    if (report.Count > 0)
                    {
                        Warning[] warnings;
                        string[] streamIds;
                        string mimeType = string.Empty;
                        string encoding = string.Empty;
                        string extension = string.Empty;

                        string bind_dsApplicants = "dsNDMApplicant";
                        string reportPath = "";
                        if (sortOption == 1)
                        {
                            reportPath = @"Reports\NDMApplicantRanking.rdlc";
                        }
                        if (sortOption == 2)
                        {
                            reportPath = @"Reports\NDMApplicantRankingByJamb.rdlc";
                        }
                        if (sortOption == 3)
                        {
                            reportPath = @"Reports\NDMApplicantRankingByName.rdlc";
                        }
                        if (sortOption == 4)
                        {
                            reportPath = @"Reports\NDMApplicantRankingByAggregate.rdlc";
                        }

                        ReportViewer rptViewer = new ReportViewer();
                        rptViewer.Visible = false;
                        rptViewer.Reset();
                        rptViewer.LocalReport.DisplayName = "Ranking Report";
                        rptViewer.ProcessingMode = ProcessingMode.Local;
                        rptViewer.LocalReport.ReportPath = reportPath;
                        rptViewer.LocalReport.EnableExternalImages = true;
                        rptViewer.LocalReport.DataSources.Add(new ReportDataSource(bind_dsApplicants.Trim(), report));

                        //byte[] bytes = rptViewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

                        byte[] bytes = {};

                        if (bulkOption == 1)
                        {
                            bytes = rptViewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                        }
                        else if (bulkOption == 2)
                        {
                            bytes = rptViewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                        }
                        else
                        {
                            bytes = rptViewer.LocalReport.Render("Excel", null, out mimeType, out encoding, out extension, out streamIds, out warnings);
                        }

                        
                        string path = Server.MapPath("~/Content/temp");

                        string savelocation = "";

                        if (bulkOption == 1)
                        {
                            savelocation = Path.Combine(path, programme.Name.Replace("/", "_") + " " + currentDepartment.Name.Replace("/", "_") + ".xls");
                        }
                        else if (bulkOption == 2)
                        {
                            savelocation = Path.Combine(path, programme.Name.Replace("/", "_") + " " + currentDepartment.Name.Replace("/", "_") + ".pdf");
                        }
                        else
                        {
                            savelocation = Path.Combine(path, programme.Name.Replace("/", "_") + " " + currentDepartment.Name.Replace("/", "_") + ".xls");
                        }

                        
                        File.WriteAllBytes(savelocation, bytes);
                    }
                }
                using (ZipFile zip = new ZipFile())
                {
                    string file = Server.MapPath("~/Content/temp/");
                    zip.AddDirectory(file, "");
                    string zipFileName = zipName;
                    zip.Save(file + zipFileName + ".zip");
                    string export = "~/Content/temp/" + zipFileName + ".zip";

                    Response.Redirect(export, false);
                    return;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}