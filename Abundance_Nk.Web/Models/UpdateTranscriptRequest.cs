﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Web.Models
{
    public static class UpdateTranscriptRequest
    {
        private static string commandString;

        public static bool UpdateTransciptRequest(DateTime dateFrom, DateTime dateTo)
        {
            try
            {
                DataTable transcriptDataTable = GetTranscriptRequest(dateFrom, dateTo);

                List<TranscriptRequestFormat> requestFormats = new List<TranscriptRequestFormat>();
                for (int i = 0; i < transcriptDataTable.Rows.Count; i++)
                {
                    TranscriptRequestFormat requestFormat = new TranscriptRequestFormat();
                    requestFormat.TranscriptRequestId = Convert.ToInt64(transcriptDataTable.Rows[i][0]) > 0 ? Convert.ToInt64(transcriptDataTable.Rows[i][0]) : 0;
                    requestFormat.PaymentId = Convert.ToInt64(transcriptDataTable.Rows[i][3]) > 0 ? Convert.ToInt64(transcriptDataTable.Rows[i][3]) : 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(transcriptDataTable.Rows[i][1])))
                    {
                        //requestFormat.DateRequested = new DateTime(Convert.ToInt32(transcriptDataTable.Rows[i][1].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[2]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][1].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[0]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][1].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[1]));

                        DateTime dateRequested = new DateTime();
                        if (!DateTime.TryParse(transcriptDataTable.Rows[i][1].ToString().Trim(), out dateRequested))
                        {
                            dateRequested = DateTime.Now;
                        }
                        requestFormat.DateOfBirth = dateRequested;
                    }
                    else
                    {
                        requestFormat.DateOfBirth = DateTime.Now;
                    }
                    
                    requestFormat.Receiver = transcriptDataTable.Rows[i][2].ToString().Trim();
                    requestFormat.DestinationAddress = transcriptDataTable.Rows[i][4].ToString().Trim();
                    requestFormat.DestinationStateId = transcriptDataTable.Rows[i][5].ToString().Trim();
                    requestFormat.DestinationCountryId = Convert.ToString(transcriptDataTable.Rows[i][6]);
                    requestFormat.TranscriptStatusId = Convert.ToInt32(transcriptDataTable.Rows[i][7]);
                    requestFormat.AlumniReceiptUrl = Convert.ToString(transcriptDataTable.Rows[i][8]);
                    requestFormat.StatementOfResultUrl = Convert.ToString(transcriptDataTable.Rows[i][9]);
                    requestFormat.DepartmentId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][11].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][11]) : 0;
                    requestFormat.DepartmentOptionId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][12].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][12]) : 0;
                    requestFormat.LevelId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][13].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][13]) : 0;
                    requestFormat.ProgrammeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][14].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][14]) : 0;
                    requestFormat.SessionId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][15].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][15]) : 0;
                    requestFormat.PersonId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][16].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][16]) : 0;
                    requestFormat.PersonTypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][17].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][17]) : 0;
                    requestFormat.FirstName = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][18].ToString()) ? Convert.ToString(transcriptDataTable.Rows[i][18]) : "";
                    requestFormat.LastName = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][19].ToString()) ? Convert.ToString(transcriptDataTable.Rows[i][19]) : "";
                    requestFormat.OtherName = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][20].ToString()) ? Convert.ToString(transcriptDataTable.Rows[i][20]) : "";
                    requestFormat.Initial = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][21].ToString()) ? Convert.ToString(transcriptDataTable.Rows[i][21]) : "";
                    requestFormat.TitleId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][22].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][22]) : 0;
                    requestFormat.ImageFileUrl = Convert.ToString(transcriptDataTable.Rows[i][23]);
                    requestFormat.SignatureFileUrl = Convert.ToString(transcriptDataTable.Rows[i][24]);
                    requestFormat.SexId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][25].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][25]) : 0;
                    requestFormat.ContactAddress = Convert.ToString(transcriptDataTable.Rows[i][26]);
                    requestFormat.Email = Convert.ToString(transcriptDataTable.Rows[i][27]);
                    requestFormat.MobilePhone = Convert.ToString(transcriptDataTable.Rows[i][28]);
                    if (!string.IsNullOrEmpty(Convert.ToString(transcriptDataTable.Rows[i][29])))
                    {
                        //requestFormat.DateOfBirth = new DateTime(Convert.ToInt32(transcriptDataTable.Rows[i][29].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[2]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][29].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[0]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][29].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[1]));

                        DateTime dob = new DateTime();
                        if (!DateTime.TryParse(transcriptDataTable.Rows[i][29].ToString().Trim(), out dob))
                        {
                            dob = DateTime.Now;
                        }
                        requestFormat.DateOfBirth = dob;
                    }
                    else
                    {
                        requestFormat.DateOfBirth = DateTime.Now;
                    }
                    requestFormat.StateId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][30].ToString()) ? Convert.ToString(transcriptDataTable.Rows[i][30]) : "";
                    requestFormat.LocalGovernmentId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][31].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][31]) : 0;
                    requestFormat.HomeTown = Convert.ToString(transcriptDataTable.Rows[i][32]);
                    requestFormat.HomeAddress = Convert.ToString(transcriptDataTable.Rows[i][33]);
                    requestFormat.NationalityId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][34].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][34]) : 0;
                    requestFormat.ReligionId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][35].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][35]) : 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(transcriptDataTable.Rows[i][36])))
                    {
                        //requestFormat.DateEntered = new DateTime(Convert.ToInt32(transcriptDataTable.Rows[i][36].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[2]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][36].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[0]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][36].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[1]));

                        DateTime dateEntered = new DateTime();
                        if (!DateTime.TryParse(transcriptDataTable.Rows[i][36].ToString().Trim(), out dateEntered))
                        {
                            dateEntered = DateTime.Now;
                        }
                        requestFormat.DateEntered = dateEntered;
                    }
                    else
                    {
                        requestFormat.DateEntered = DateTime.Now;
                    }
                    requestFormat.RoleId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][37].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][37]) : 0;
                    requestFormat.PlaceOfBirth = Convert.ToString(transcriptDataTable.Rows[i][38]);
                    requestFormat.ApplicationFormId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][39].ToString()) ? Convert.ToInt64(transcriptDataTable.Rows[i][39]) : 0;
                    requestFormat.StudentTypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][40].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][40]) : 0;
                    requestFormat.StudentCategoryId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][41].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][41]) : 0;
                    requestFormat.StudentStatusId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][42].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][42]) : 0;
                    requestFormat.StudentNumber = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][43].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][43]): 0;
                    requestFormat.MatricNumber = Convert.ToString(transcriptDataTable.Rows[i][44]);
                    requestFormat.MaritalStatusId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][46].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][46]) : 0;
                    requestFormat.BloodGroupId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][47].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][47]) : 0;
                    requestFormat.GenotypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][48].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][48]) : 0;
                    requestFormat.SchoolContactAddress = Convert.ToString(transcriptDataTable.Rows[i][49]);
                    requestFormat.Activated = !string.IsNullOrEmpty(Convert.ToString(transcriptDataTable.Rows[i][50])) ? Convert.ToBoolean(transcriptDataTable.Rows[i][50]) : true;

                    requestFormat.PaymentModeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][54].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][54]) : 0;
                    requestFormat.PaymentTypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][56].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][56]) : 0;
                    requestFormat.PersonTypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][57].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][57]) : 0;
                    requestFormat.FeeTypeId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][52].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][52]) : 0;
                    if (!string.IsNullOrEmpty(Convert.ToString(transcriptDataTable.Rows[i][51])))
                    {
                        //requestFormat.DatePaid = new DateTime(Convert.ToInt32(transcriptDataTable.Rows[i][51].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[2]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][51].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[0]),
                        //                                       Convert.ToInt32(transcriptDataTable.Rows[i][51].ToString().Trim().Split(' ').FirstOrDefault().Split('/')[1]));

                        DateTime datePaid = new DateTime();
                        if (!DateTime.TryParse(transcriptDataTable.Rows[i][51].ToString().Trim(), out datePaid))
                        {
                            datePaid = DateTime.Now;
                        }
                        requestFormat.DatePaid = datePaid;
                    }
                    else
                    {
                        requestFormat.DatePaid = DateTime.Now;
                    }
                    requestFormat.SerialNumber = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][55].ToString()) ? Convert.ToInt64(transcriptDataTable.Rows[i][55]) : 0;
                    requestFormat.PaymentSessionId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][58].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][58]) : 0;
                    requestFormat.InvoiceNumber = Convert.ToString(transcriptDataTable.Rows[i][53]);
                    requestFormat.TranscriptClearanceStatusId = !string.IsNullOrEmpty(transcriptDataTable.Rows[i][59].ToString()) ? Convert.ToInt32(transcriptDataTable.Rows[i][59]) : 0;

                    requestFormats.Add(requestFormat);
                }

                return SaveTranscriptRequest(requestFormats);

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public static DataTable GetTranscriptRequest(DateTime dateFrom, DateTime dateTo)
        {
            DataTable dataTable = new DataTable();

            try
            {
                string connectionString = "data source=97.74.6.243;initial catalog=Abundance_Nk_Nekede;User ID=support;Password=8n4H!Lo65;";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    //if (requestUpdate != null)
                    //{
                    //    string requestIdString = Convert.ToString(requestUpdate.LastRequestId, CultureInfo.InvariantCulture);
                    string dateFromStr = (dateFrom.Year.ToString() + '-' + dateFrom.Month.ToString() + '-' + dateFrom.Day.ToString());
                    string dateToStr = dateTo.Year.ToString() + '-' + dateTo.Month.ToString() + '-' + dateTo.Day.ToString();

                    commandString = String.Format("SELECT TR.Transcript_Request_Id, TR.Date_Requested, TR.Receiver, TR.Payment_Id, TR.Destination_Address, TR.Destination_State_Id, TR.Destination_Country_Id, " +
                    "TR.Transcript_Status_Id, TR.AlumniReceiptUrl, TR.StatementOfResultUrl, SL.Student_Level_Id, SL.Department_Id, SL.Department_Option_Id, SL.Level_Id," +
                    "SL.Programme_Id, SL.Session_Id, PS.Person_Id, PS.Person_Type_Id, PS.First_Name, PS.Last_Name, PS.Other_Name, PS.Initial, PS.Title," +
                    "PS.Image_File_Url, PS.Signature_File_Url, PS.Sex_Id, PS.Contact_Address, PS.Email, PS.Mobile_Phone, PS.Date_Of_Birth, PS.State_Id, PS.Local_Government_Id, PS.Home_Town," +
                    "PS.Home_Address, PS.Nationality_Id, PS.Religion_Id, PS.Date_Entered, PS.Role_Id, PS.Place_Of_Birth, ST.Application_Form_Id, ST.Student_Type_Id, ST.Student_Category_Id," +
                    "ST.Student_Status_Id, ST.Student_Number, ST.Matric_Number, ST.Title_Id, ST.Marital_Status_Id, ST.Blood_Group_Id, ST.Genotype_Id, ST.School_Contact_Address, Activated" +
                    ", P.Date_Paid, P.Fee_Type_Id, P.Invoice_Number, P.Payment_Mode_Id, P.Payment_Serial_Number, P.Payment_Type_Id, P.Person_Type_Id, P.Session_Id, TR.Transcript_clearance_Status_Id " +
                    " FROM  TRANSCRIPT_REQUEST TR INNER JOIN  " +
                    "PERSON PS ON PS.Person_Id = TR.Student_id INNER JOIN " +
                    "STUDENT ST ON PS.Person_Id  = ST.Person_Id INNER JOIN " +
                    "PAYMENT P ON P.Payment_Id = TR.Payment_Id INNER JOIN " +
                    "REMITA_PAYMENT RP ON RP.Payment_Id = TR.Payment_Id INNER JOIN " +
                    "TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TR.Transcript_Status_Id LEFT OUTER JOIN " +
                    "STUDENT_LEVEL SL ON SL.Person_Id = ST.Person_Id " +
                    "WHERE TR.Date_Requested >= '{0}' AND TR.Date_Requested <= '{1}' ", dateFromStr, dateToStr);

                    //SqlDataAdapter dataAdapter = new SqlDataAdapter(CommandString, connection);TR.Date_Requested > '2018-5-1' AND TR.Date_Requested < '2018-6-5'
                    //dataAdapter.Fill(dataTable);
                    //dataTable.Rows.Remove(dataTable.Rows[0]);

                    //}
                    //else
                    //{
                        //commandString = String.Format("SELECT TR.Transcript_Request_Id, TR.Date_Requested, TR.Receiver, TR.Payment_Id, TR.Destination_Address, TR.Destination_State_Id, TR.Destination_Country_Id, " +
                        //"TR.Transcript_Status_Id, TR.AlumniReceiptUrl, TR.StatementOfResultUrl, SL.Student_Level_Id, SL.Department_Id, SL.Department_Option_Id, SL.Level_Id," +
                        //"SL.Programme_Id, SL.Session_Id, PS.Person_Id, PS.Person_Type_Id, PS.First_Name, PS.Last_Name, PS.Other_Name, PS.Initial, PS.Title," +
                        //"PS.Image_File_Url, PS.Signature_File_Url, PS.Sex_Id, PS.Contact_Address, PS.Email, PS.Mobile_Phone, PS.Date_Of_Birth, PS.State_Id, PS.Local_Government_Id, PS.Home_Town," +
                        //"PS.Home_Address, PS.Nationality_Id, PS.Religion_Id, PS.Date_Entered, PS.Role_Id, PS.Place_Of_Birth, ST.Application_Form_Id, ST.Student_Type_Id, ST.Student_Category_Id," +
                        //"ST.Student_Status_Id, ST.Student_Number, ST.Matric_Number, ST.Title_Id, ST.Marital_Status_Id, ST.Blood_Group_Id, ST.Genotype_Id, ST.School_Contact_Address, Activated" +
                        //", P.Date_Paid, P.Fee_Type_Id, P.Invoice_Number, P.Payment_Mode_Id, P.Payment_Serial_Number, P.Payment_Type_Id, P.Person_Type_Id, P.Session_Id, TR.Transcript_clearance_Status_Id " +
                        //" FROM TRANSCRIPT_REQUEST TR INNER JOIN  " +
                        //"PERSON PS ON PS.Person_Id = TR.Student_id INNER JOIN " +
                        //"STUDENT ST ON PS.Person_Id  = ST.Person_Id INNER JOIN " +
                        //"PAYMENT P ON P.Payment_Id = TR.Payment_Id INNER JOIN " +
                        //"REMITA_PAYMENT RP ON RP.Payment_Id = TR.Payment_Id INNER JOIN " +
                        //"TRANSCRIPT_STATUS TRS ON TRS.Transcript_Status_Id = TR.Transcript_Status_Id LEFT OUTER JOIN " +
                        //"STUDENT_LEVEL SL ON SL.Person_Id = ST.Person_Id ");
                        

                    //}

                    using (SqlCommand command = new SqlCommand(commandString, connection))
                    {
                        using (SqlDataAdapter dataAdapter = new SqlDataAdapter(command))
                        {
                            dataAdapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return dataTable;
        }
        public static bool SaveTranscriptRequest(List<TranscriptRequestFormat> format)
        {
            try
            {
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                StudentLogic studentLogic = new StudentLogic();
                TranscriptRequest transcriptRequest = new TranscriptRequest();

                Payment payment = new Payment();
                bool modified = false;
                if (format.Count > 0)
                {
                    long requestId = 0;
                    for (int i = 0; i < format.Count; i++)
                    {
                        TranscriptRequestFormat currentFormat = format[i];

                        using (TransactionScope scope = new TransactionScope())
                        {
                            if (string.IsNullOrEmpty(currentFormat.MatricNumber))
                            {
                                continue;
                            }

                            STUDENT existingStudent = studentLogic.GetEntitiesBy(s => s.Matric_Number == currentFormat.MatricNumber).LastOrDefault();
                            if (existingStudent != null)
                            {
                                currentFormat.PersonId = existingStudent.Person_Id;
                                TRANSCRIPT_REQUEST existingRequest = requestLogic.GetEntitiesBy(r => r.Student_id == currentFormat.PersonId && r.Transcript_Identifier == currentFormat.TranscriptRequestId).LastOrDefault();
                                if (existingRequest != null)
                                {
                                    TranscriptRequest request = new TranscriptRequest();
                                    request.student = new Student(){ Id = existingRequest.Student_id.Value };
                                    request.Id = existingRequest.Transcript_Request_Id;
                                    request.AlumniReceiptUrl = currentFormat.AlumniReceiptUrl;
                                    request.StatementOfResultUrl = currentFormat.StatementOfResultUrl;
                                    request.transcriptClearanceStatus = new TranscriptClearanceStatus(){ TranscriptClearanceStatusId = currentFormat.TranscriptClearanceStatusId};
                                    request.transcriptStatus = new TranscriptStatus(){ TranscriptStatusId = currentFormat.TranscriptStatusId};
                                    request.payment = new Payment(){ Id = existingRequest.Payment_Id.Value};
                                    request.DateRequested = existingRequest.Date_Requested;
                                    request.Receiver = existingRequest.Receiver;
                                    request.DestinationAddress = existingRequest.Destination_Address;
                                    request.DestinationCountry = new Country(){ Id = existingRequest.Destination_Country_Id };
                                    request.DestinationState = new State(){ Id = existingRequest.Destination_State_Id };
                                    request.RequestType = existingRequest.Request_Type;
                                    request.Verified = existingRequest.Verified;
                                    request.Processed = existingRequest.Processed;
                                    request.TransciptIdentifier = (long)existingRequest.Transcript_Identifier;

                                    requestLogic.Modify(request);
                                    //TranscriptRequest request = requestLogic.GetModelBy(t => t.Transcript_Request_Id == existingRequest.Transcript_Request_Id);
                                    //if (request != null)
                                    //{
                                    //    request.transcriptStatus = currentFormat.TranscriptStatusId > 0 ? new TranscriptStatus() { TranscriptStatusId = currentFormat.TranscriptStatusId } : request.transcriptStatus;

                                    //    requestLogic.Modify(request);
                                    //}
                                }
                                else
                                {
                                    payment = CheckAndCreatePayment(currentFormat);
                                    CheckAndCreateStudentLevel(currentFormat);

                                    currentFormat.PaymentId = payment != null ? payment.Id : currentFormat.PaymentId;
                                    currentFormat.PersonId = existingStudent.Person_Id;

                                    transcriptRequest = CreateTranscriptRequest(currentFormat);

                                    if (transcriptRequest != null && transcriptRequest.Id > 0)
                                    {
                                        scope.Complete();
                                    }
                                }
                            }
                            else
                            {
                                Person person = CreatePerson(currentFormat);
                                Student student = CreateStudent(currentFormat, person);

                                currentFormat.PersonId = student != null ? student.Id : 0;
                                payment = CheckAndCreatePayment(currentFormat);
                                currentFormat.PaymentId = payment != null ? payment.Id : 0;

                                CheckAndCreateStudentLevel(currentFormat);

                                TRANSCRIPT_REQUEST existingRequest = requestLogic.GetEntitiesBy(r => r.Student_id == currentFormat.PersonId && r.Transcript_Identifier == currentFormat.TranscriptRequestId).LastOrDefault();
                                if (existingRequest != null)
                                {
                                    TranscriptRequest request = new TranscriptRequest();
                                    request.student = new Student() { Id = existingRequest.Student_id.Value };
                                    request.Id = existingRequest.Transcript_Request_Id;
                                    request.AlumniReceiptUrl = currentFormat.AlumniReceiptUrl;
                                    request.StatementOfResultUrl = currentFormat.StatementOfResultUrl;
                                    request.transcriptClearanceStatus = new TranscriptClearanceStatus() { TranscriptClearanceStatusId = currentFormat.TranscriptClearanceStatusId };
                                    request.transcriptStatus = new TranscriptStatus() { TranscriptStatusId = currentFormat.TranscriptStatusId };
                                    request.payment = new Payment() { Id = existingRequest.Payment_Id.Value };
                                    request.DateRequested = existingRequest.Date_Requested;
                                    request.Receiver = existingRequest.Receiver;
                                    request.DestinationAddress = existingRequest.Destination_Address;
                                    request.DestinationCountry = new Country() { Id = existingRequest.Destination_Country_Id };
                                    request.DestinationState = new State() { Id = existingRequest.Destination_State_Id };
                                    request.RequestType = existingRequest.Request_Type;
                                    request.Verified = existingRequest.Verified;
                                    request.Processed = existingRequest.Processed;
                                    request.TransciptIdentifier = (long) existingRequest.Transcript_Identifier;

                                    modified = requestLogic.Modify(request);
                                    //TranscriptRequest request = requestLogic.GetModelBy(t => t.Transcript_Request_Id == existingRequest.Transcript_Request_Id);
                                    //if (request != null)
                                    //{
                                    //    request.transcriptStatus = currentFormat.TranscriptStatusId > 0 ? new TranscriptStatus() { TranscriptStatusId = currentFormat.TranscriptStatusId } : request.transcriptStatus;

                                    //    requestLogic.Modify(request);
                                    //}
                                }
                                else
                                {
                                    transcriptRequest = CreateTranscriptRequest(currentFormat);
                                }

                                if (transcriptRequest != null && transcriptRequest.Id > 0 || modified)
                                {
                                    scope.Complete();
                                }
                            }
                        }

                        if (currentFormat.TranscriptRequestId > 0)
                        {
                            requestId = currentFormat.TranscriptRequestId; 
                        }
                    }

                    //if (requestId > 0)
                    //{
                    //    TranscriptRequestUpdate requestUpdate = new TranscriptRequestUpdate();
                    //    TranscriptRequestUpdateLogic requestUpdateLogic = new TranscriptRequestUpdateLogic();

                    //    requestUpdate.Count = format.Count;
                    //    requestUpdate.LastRequestId = requestId;
                    //    requestUpdate.LastUpdateOn = DateTime.Now;

                    //    requestUpdateLogic.Create(requestUpdate);

                    //    return true;
                    //}

                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return false;
        }

        private static Student CreateStudent(TranscriptRequestFormat currentFormat, Person person)
        {
            Student student = null;
            try
            {
                if (currentFormat.StudentCategoryId > 0 && currentFormat.StudentStatusId > 0 && currentFormat.StudentTypeId > 0 && person.Id > 0)
                {
                    StudentLogic studentLogic = new StudentLogic();

                    student = new Student();
                    student.Activated = true;
                    student.BloodGroup = currentFormat.BloodGroupId > 0 ? new BloodGroup() { Id = currentFormat.BloodGroupId } : null ;
                    student.Category = new StudentCategory(){ Id = currentFormat.StudentCategoryId };
                    student.Genotype = currentFormat.GenotypeId > 0 ? new Genotype() { Id = currentFormat.GenotypeId } : null;
                    student.MaritalStatus = currentFormat.MaritalStatusId > 0 ? new MaritalStatus() { Id = currentFormat.MaritalStatusId } : null;
                    student.MatricNumber = currentFormat.MatricNumber;
                    student.Number = currentFormat.StudentNumber > 0 ? currentFormat.SerialNumber : null;
                    student.SchoolContactAddress = currentFormat.SchoolContactAddress;
                    student.Status = new StudentStatus(){ Id = currentFormat.StudentStatusId };
                    student.Title = currentFormat.TitleId > 0 ? new Title() { Id = currentFormat.TitleId } : null;
                    student.Type = new StudentType(){ Id = currentFormat.StudentTypeId };
                    student.Id = person.Id;

                    student = studentLogic.CreateStudent(student);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return student;
        }
        private static Person CreatePerson(TranscriptRequestFormat currentFormat)
        {
            Person person = null;
            try
            {
                if (currentFormat.NationalityId > 0 && !string.IsNullOrEmpty(currentFormat.StateId) && currentFormat.PersonTypeId > 0 && currentFormat.RoleId > 0 && !string.IsNullOrEmpty(currentFormat.LastName)
                    && !string.IsNullOrEmpty(currentFormat.FirstName))
                {
                    Role role = new Role() { Id = currentFormat.RoleId };
                    PersonType personType = new PersonType() { Id = currentFormat.PersonTypeId };
                    Nationality nationality = new Nationality() { Id = currentFormat.NationalityId };

                    person = new Person();
                    person.Role = role;
                    person.PersonType = personType;
                    person.Nationality = nationality;
                    person.DateEntered = currentFormat.DateEntered;
                    person.ContactAddress = currentFormat.ContactAddress;
                    person.DateOfBirth = currentFormat.DateOfBirth;
                    person.Email = currentFormat.Email;
                    person.FirstName = currentFormat.FirstName;
                    person.HomeAddress = currentFormat.HomeAddress;
                    person.HomeTown = currentFormat.HomeTown;
                    person.ImageFileUrl = currentFormat.ImageFileUrl;
                    person.Initial = currentFormat.Initial;
                    person.LastName = currentFormat.LastName;
                    person.LocalGovernment = currentFormat.LocalGovernmentId > 0 ? new LocalGovernment() { Id = currentFormat.LocalGovernmentId } : null;
                    person.MobilePhone = currentFormat.MobilePhone;
                    person.OtherName = currentFormat.OtherName;
                    person.PlaceOfBirth = currentFormat.PlaceOfBirth;
                    person.Religion = currentFormat.ReligionId > 0 ? new Religion() { Id = currentFormat.ReligionId } : null;
                    person.Sex = currentFormat.SexId > 0 ? new Sex() {Id = Convert.ToByte(currentFormat.SexId)} : null;
                    person.SignatureFileUrl = currentFormat.SignatureFileUrl;
                    person.State = new State() {Id = currentFormat.StateId};
                    
                    PersonLogic personLogic = new PersonLogic();

                    person = personLogic.Create(person);
                }
                
            }
            catch (Exception)
            {
                throw;
            }

            return person;
        }
        private static TranscriptRequest CreateTranscriptRequest(TranscriptRequestFormat currentFormat)
        {
            TranscriptRequest request = new TranscriptRequest();
            try
            {
                if (!string.IsNullOrEmpty(currentFormat.Receiver) && !string.IsNullOrEmpty(currentFormat.DestinationAddress) && !string.IsNullOrEmpty(currentFormat.StateId) &&
                    !string.IsNullOrEmpty(currentFormat.DestinationCountryId) && currentFormat.TranscriptStatusId > 0 && currentFormat.PersonId > 0)
                {
                    TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                    TranscriptRequest transcriptRequest = new TranscriptRequest();

                    transcriptRequest.payment = currentFormat.PaymentId > 0 ? new Payment() { Id = currentFormat.PaymentId } : null;
                    transcriptRequest.AlumniReceiptUrl = !string.IsNullOrEmpty(currentFormat.AlumniReceiptUrl) ? currentFormat.AlumniReceiptUrl : null;
                    transcriptRequest.DateRequested = currentFormat.DateRequested.Year == 1 ? DateTime.Now : currentFormat.DateRequested;
                    transcriptRequest.DestinationAddress = currentFormat.DestinationAddress;
                    transcriptRequest.DestinationCountry = new Country(){ Id = currentFormat.DestinationCountryId };
                    transcriptRequest.DestinationState = new State() { Id = currentFormat.DestinationStateId };
                    transcriptRequest.payment = currentFormat.PaymentId > 0 ? new Payment() { Id = currentFormat.PaymentId } : null;
                    transcriptRequest.Receiver = currentFormat.Receiver;
                    transcriptRequest.StatementOfResultUrl = !string.IsNullOrEmpty(currentFormat.StatementOfResultUrl) ? currentFormat.StatementOfResultUrl : null;
                    transcriptRequest.student = new Student() { Id = currentFormat.PersonId };
                    transcriptRequest.transcriptClearanceStatus = currentFormat.TranscriptClearanceStatusId > 0 ? new TranscriptClearanceStatus() { TranscriptClearanceStatusId = currentFormat.TranscriptClearanceStatusId } : null;
                    transcriptRequest.transcriptStatus = new TranscriptStatus() { TranscriptStatusId = currentFormat.TranscriptStatusId };
                    transcriptRequest.TransciptIdentifier = currentFormat.TranscriptRequestId;
                    request = requestLogic.Create(transcriptRequest);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return request;
        }

        private static void CheckAndCreateStudentLevel(TranscriptRequestFormat currentFormat)
        {
            try
            {
                if (currentFormat.PersonId > 0 && currentFormat.DepartmentId > 0 && currentFormat.ProgrammeId > 0 && currentFormat.LevelId > 0 && currentFormat.SessionId > 0)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel existingStudentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == currentFormat.PersonId).LastOrDefault();

                    if (existingStudentLevel == null)
                    {
                        existingStudentLevel = new StudentLevel();
                        existingStudentLevel.Department = new Department(){ Id = currentFormat.DepartmentId };
                        existingStudentLevel.Programme = new Programme(){ Id = currentFormat.ProgrammeId };
                        existingStudentLevel.Level = new Level(){ Id = currentFormat.LevelId };
                        existingStudentLevel.Session = new Session(){ Id = currentFormat.SessionId };
                        existingStudentLevel.Student = new Student() { Id = currentFormat.PersonId };
                        existingStudentLevel.DepartmentOption = currentFormat.DepartmentOptionId > 0 ? new DepartmentOption() {Id = currentFormat.DepartmentOptionId } : null;

                        studentLevelLogic.Create(existingStudentLevel);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static Payment CheckAndCreatePayment(TranscriptRequestFormat currentFormat)
        {
            Payment newPayment = new Payment();
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                if (currentFormat.PersonId > 0 && currentFormat.FeeTypeId > 0 && currentFormat.PersonTypeId > 0 && currentFormat.PaymentTypeId > 0 && currentFormat.PaymentModeId > 0 && currentFormat.PaymentId > 0)
                {
                    Payment payment = paymentLogic.GetModelBy(p => p.Payment_Id == currentFormat.PaymentId);
                    if (payment == null)
                    {
                        payment = new Payment();
                        payment.DatePaid = currentFormat.DatePaid;
                        payment.FeeType = new FeeType(){ Id = currentFormat.FeeTypeId };
                        payment.PersonType = new PersonType(){ Id = currentFormat.PersonTypeId };
                        payment.PaymentType = new PaymentType(){ Id = currentFormat.PaymentTypeId };
                        payment.PaymentMode = new PaymentMode(){ Id = currentFormat.PaymentModeId };
                        payment.InvoiceNumber = currentFormat.InvoiceNumber;
                        payment.SerialNumber = currentFormat.SerialNumber;
                        payment.Person = new Person(){ Id = currentFormat.PersonId };
                        payment.Session = currentFormat.PaymentSessionId > 0 ? new Session() { Id = currentFormat.PaymentSessionId } : null;

                        newPayment = paymentLogic.Create(payment);
                    }
                    else
                    {
                        newPayment = payment;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return newPayment;
        }

        public static void UpdateOnlineTranscriptRequest(TranscriptRequest transcriptRequest)
        {
            try
            {
                string connectionString = "data source=97.74.6.243;initial catalog=Abundance_Nk_Nekede;User ID=support;Password=8n4H!Lo65;";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {

                        command.CommandText = "UPDATE TRANSCRIPT_REQUEST SET Transcript_clearance_Status_Id = @clearanceStatus, Transcript_Status_Id = @status, Verified = @verified, " +
                                              "Processed = @processed " +
                                              "FROM	TRANSCRIPT_REQUEST TR, PAYMENT PY WHERE	TR.Payment_Id = PY.Payment_Id AND PY.Invoice_Number = @invoiceNumber";

                        command.Parameters.AddWithValue("@clearanceStatus", transcriptRequest.transcriptClearanceStatus.TranscriptClearanceStatusId);
                        command.Parameters.AddWithValue("@status", transcriptRequest.transcriptStatus.TranscriptStatusId);
                        command.Parameters.AddWithValue("@verified", transcriptRequest.Verified ?? false );
                        command.Parameters.AddWithValue("@processed", transcriptRequest.Processed ?? false);
                        command.Parameters.AddWithValue("@invoiceNumber", transcriptRequest.payment.InvoiceNumber);

                        connection.Open();

                        command.ExecuteNonQuery();

                        connection.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
    public class TranscriptRequestFormat
    {
        public long TranscriptRequestId { get; set; }
        public long PaymentId { get; set; }
        public DateTime DateRequested { get; set; }
        public string DestinationAddress { get; set; }
        public string DestinationStateId { get; set; }
        public string DestinationCountryId { get; set; }
        public int TranscriptClearanceStatusId { get; set; }
        public int TranscriptStatusId { get; set; }
        public string Receiver { get; set; }
        public string RequestType { get; set; }
        public string AlumniReceiptUrl { get; set; }
        public string StatementOfResultUrl { get; set; }
        public int StudentTypeId { get; set; }
        public int StudentCategoryId { get; set; }
        public int StudentStatusId { get; set; }
        public long StudentNumber { get; set; }
        public string MatricNumber { get; set; }
        public long ApplicationFormId { get; set; }
        public int TitleId { get; set; }
        public int MaritalStatusId { get; set; }
        public int BloodGroupId { get; set; }
        public int GenotypeId { get; set; }
        public string SchoolContactAddress { get; set; }
        public bool? Activated { get; set; }
        public int LevelId { get; set; }
        public int ProgrammeId { get; set; }
        public int DepartmentId { get; set; }
        public int DepartmentOptionId { get; set; }
        public int SessionId { get; set; }
        public long PersonId { get; set; }
        public int PersonTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OtherName { get; set; }
        public string ImageFileUrl { get; set; }
        public string SignatureFileUrl { get; set; }
        public int SexId { get; set; }
        public string ContactAddress { get; set; }
        public string Email { get; set; }
        public string MobilePhone { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string StateId { get; set; }
        public string Initial { get; set; }
        public string HomeTown { get; set; }
        public string HomeAddress { get; set; }
        public int NationalityId { get; set; }
        public DateTime DateEntered { get; set; }
        public int RoleId { get; set; }
        public int LocalGovernmentId { get; set; }
        public int ReligionId { get; set; }
        public string PlaceOfBirth { get; set; }
        
        public int PaymentModeId { get; set; }
        public int PaymentTypeId { get; set; }
        public int FeeTypeId { get; set; }
        public DateTime DatePaid { get; set; }
        public long? SerialNumber { get; set; }
        public int PaymentSessionId { get; set; }
        public string InvoiceNumber { get; set; }
    }
}