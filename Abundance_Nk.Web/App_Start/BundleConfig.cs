﻿using System.Web;
using System.Web.Optimization;

namespace Abundance_Nk.Web
{
    public class BundleConfig
    {
       

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                 "~/Content/site.css",
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap-override.css",
                      "~/Content/style.default.css",
                      "~/Content/bootstrap-theme.css",
                         "~/Content/Gridmvc.css",
                         "~/Content/misc.css"

                          ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.tagsinput.min.js",
                        "~/Scripts/jquery.autogrow-textarea.js",
                        "~/Scripts/jquery-migrate-1.2.1.min.js",
                        "~/Scripts/gridmvc.min.js",
                        "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/jquery.print.js",
                        "~/Scripts/jquery.validate.js",
                        "~/Scripts/jquery.validate.unobtrusive.js",
                        "~/Scripts/jquery.print.js"

                        ));
             bundles.Add(new ScriptBundle("~/bundles/FileUpload").Include(
                        "~/Scripts/jquery-ui-1.9.0.js",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-jquery-ui.js",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload.js",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-image.js",
                        "~/Scripts/jQuery.FileUpload/jquery.fileupload-process.js",
                        "~/Scripts/jQuery.FileUpload/jquery.iframe-transport.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/dropzonescripts").Include(

                     "~/Scripts/dropzone/dropzone.js"

                     ));

            bundles.Add(new StyleBundle("~/Content/dropzonescss").Include(
                     "~/Scripts/dropzone/css/basic.css",
                     "~/Scripts/dropzone/css/dropzone.css"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

        }


    }
}
