﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;

namespace Abundance_Nk.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
          //  EmailClient emailClient = new EmailClient("Test", "ujahmycine@gmail.com");
           
            return View();
        }

        public ActionResult About()
        {
            //AppliedCourse appliedCourse = new AppliedCourse();
            //AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
            //appliedCourse = appliedCourseLogic.GetModelBy(a => a.Person_Id == 62145);
            
            //RankingDataLogic rankingDataLogic = new RankingDataLogic();
            //string reason = rankingDataLogic.RankStudent(appliedCourse);
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}