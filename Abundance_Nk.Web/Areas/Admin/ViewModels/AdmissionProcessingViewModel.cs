﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Admin.ViewModels
{
    public class AdmissionProcessingViewModel
    {
        private ApplicationFormLogic applicationFormLogic;

        public AdmissionProcessingViewModel()
        {

            Session = new Session();
            applicationFormLogic = new ApplicationFormLogic();
            ApplicationForms = new List<ApplicationForm>();

            SessionSelectList = Utility.PopulateSessionSelectListItem();
        }

        public Session Session { get; set; }
        public List<ApplicationForm> ApplicationForms { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }

        public List<ApplicationForm> GetApplicationsBy(bool rejected, Session session)
        {
            try
            {
                ApplicationForms = applicationFormLogic.GetModelsBy(af => af.Rejected == rejected && af.APPLICATION_FORM_SETTING.Session_Id == session.Id);
                return ApplicationForms;
            }
            catch (Exception)
            {
                throw;
            }
        }

        



    }




}