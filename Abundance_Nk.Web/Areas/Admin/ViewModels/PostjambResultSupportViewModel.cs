﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.ComponentModel.DataAnnotations;


namespace Abundance_Nk.Web.Areas.Admin.ViewModels
{
    public class PostjambResultSupportViewModel
    {
        public PostjambResultSupportViewModel()
        {
            SessionSelectList = Utility.PopulateAllSessionSelectListItem();
            ProgrammeSelectList = Utility.PopulateAllProgrammeSelectListItem();
        }
        public Abundance_Nk.Model.Model.NdPutmeResult putmeResult { get; set; }
        public List<NdPutmeResult> AllResults { get; set; }
        public ApplicantJambDetail jambDetail { get; set; }
        public ApplicationForm ApplicationDetail { get; set; }

        [Display(Name = "Jamb Number")]
        public string JambNumber { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public List<SelectListItem> ProgrammeSelectList { get; set; }
        public Session Session { get; set; }
        public Programme Programme { get; set; }
        public List<PutmeResultModel> PutmeResults { get; set; }

    }
}