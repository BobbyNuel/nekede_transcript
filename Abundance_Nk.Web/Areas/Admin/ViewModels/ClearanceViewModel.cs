﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Web.Areas.Admin.ViewModels
{
    public class ClearanceViewModel
    {
       

        public ClearanceViewModel()
        {
            applicationFormList = new List<ApplicationForm>();
            appliedCourseList = new List<AppliedCourse>();
            personList = new List<Person>();

            FirstSittingOLevelResult = new OLevelResult();
            FirstSittingOLevelResult.Type = new OLevelType();

            SecondSittingOLevelResult = new OLevelResult();
            SecondSittingOLevelResult.Type = new OLevelType();
            FirstSittingOLevelResultDetailsList = new List<OLevelResultDetail>();
            SecondSittingOLevelResultDetailsList = new List<OLevelResultDetail>();

            AdmissionCriteriaLogic AdmissionCriteriaLogic = new Business.AdmissionCriteriaLogic();
            admissionCriteriaList = AdmissionCriteriaLogic.GetAll();

            InitialiseOLevelResult();
            //InitialiseOLevelSubjects();
            ProgrammeSelectListItem = Utility.PopulateAllProgrammeSelectListItem();
            OLevelSubjectSelectList = Utility.PopulateOLevelSubjectSelectListItem();
            OLevelGradeSelectList = Utility.PopulateOLevelGradeSelectListItem();
            OlevelTypeSelectListItem = Utility.PopulateOLevelTypeSelectListItem();
            SessionSelectList = Utility.PopulateAllSessionSelectListItem();
        }
        public string ApplicationNumber { get; set; }

        [Display (Name="Etranzact Pin")]
        public string ConfrimationOrderNo { get; set; }
        public List<ApplicationForm> applicationFormList { get; set; }
        public List<AppliedCourse> appliedCourseList { get; set; }
        public List<Person> personList { get; set; }
        public List<OLevelType> OLevelTypes { get; set; }
        public List<OLevelGrade> OLevelGrades { get; set; }
        public List<OLevelSubject> OLevelSubjects { get; set; }
        public List<OLevelSubject> OLevelSubjectsAlternatives { get; set; }
        public List<SelectListItem> OLevelSubjectSelectList { get; set; }
        public OLevelResult FirstSittingOLevelResult { get; set; }
        public OLevelResult SecondSittingOLevelResult { get; set; }
        public OLevelResultDetail FirstSittingOLevelResultDetail { get; set; }
        public OLevelResultDetail SecondSittingOLevelResultDetail { get; set; }
        public List<OLevelResultDetail> FirstSittingOLevelResultDetailsList { get; set; }
        public List<OLevelResultDetail> SecondSittingOLevelResultDetailsList { get; set; }
        public OLevelResult ApplicantFirstSittingOLevelResult { get; set; }
        public OLevelResult ApplicantSecondSittingOLevelResult { get; set; }
        public List<OLevelResultDetail> ApplicantFirstSittingOLevelResultDetailsList { get; set; }
        public List<OLevelResultDetail> ApplicantSecondSittingOLevelResultDetailsList { get; set; }
        public List<AdmissionCriteria> admissionCriteriaList { get; set; }
        public List<AdmissionCriteriaForOLevelSubject> admissionCriteriaForOLevelSubject { get; set; }
        public List<AdmissionCriteriaForOLevelSubjectAlternative> admissionCriteriaForOLevelSubjectAlternative { get; set; }
        public List<AdmissionCriteriaForOLevelType> admissionCriteriaForOLevelType { get; set; }
        public bool CheckBox { get; set; }
        public AdmissionCriteria admissionCriteria { get; set; }
        public ApplicationForm applicationForm { get; set; }
        public AppliedCourse appliedCourse { get; set; }
        public Person person { get; set; }
        public Payment payment { get; set; }
        public User ClearanceOfficer { get; set; }
        public ClearanceLog clearanceDetails { get; set; }
        public List<SelectListItem> OlevelTypeSelectListItem { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public void InitialiseOLevelResult()
        {
            try
            {
                List<OLevelResultDetail> oLevelResultDetails = new List<OLevelResultDetail>();
                OLevelResultDetail oLevelResultDetail1 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail2 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail3 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail4 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail5 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail6 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail7 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail8 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail9 = new OLevelResultDetail();

                OLevelResultDetail oLevelResultDetail11 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail22 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail33 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail44 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail55 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail66 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail77 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail88 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail99 = new OLevelResultDetail();

                FirstSittingOLevelResultDetailsList = new List<OLevelResultDetail>();
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail1);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail2);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail3);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail4);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail5);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail6);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail7);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail8);
                FirstSittingOLevelResultDetailsList.Add(oLevelResultDetail9);

                SecondSittingOLevelResultDetailsList = new List<OLevelResultDetail>();
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail11);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail22);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail33);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail44);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail55);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail66);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail77);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail88);
                SecondSittingOLevelResultDetailsList.Add(oLevelResultDetail99);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void InitialiseOLevelSubjects()
        {
            try
            {
                OLevelSubject oLevelResultSubject1 = new OLevelSubject();
                OLevelSubject oLevelResultSubject2 = new OLevelSubject();
                OLevelSubject oLevelResultSubject3 = new OLevelSubject();
                OLevelSubject oLevelResultSubject4 = new OLevelSubject();
                OLevelSubject oLevelResultSubject5 = new OLevelSubject();
                OLevelSubject oLevelResultSubject6 = new OLevelSubject();
                OLevelSubject oLevelResultSubject7 = new OLevelSubject();
                OLevelSubject oLevelResultSubject8 = new OLevelSubject();
                OLevelSubject oLevelResultSubject9 = new OLevelSubject();
                OLevelSubject oLevelResultSubject10 = new OLevelSubject();
                OLevelSubject oLevelResultSubject11 = new OLevelSubject();
                OLevelSubject oLevelResultSubject12 = new OLevelSubject();
                OLevelSubject oLevelResultSubject13 = new OLevelSubject();
                OLevelSubject oLevelResultSubject14 = new OLevelSubject();
                OLevelSubject oLevelResultSubject15 = new OLevelSubject();

                OLevelSubject oLevelResultSubject111 = new OLevelSubject();
                OLevelSubject oLevelResultSubject222 = new OLevelSubject();
                OLevelSubject oLevelResultSubject333 = new OLevelSubject();
                OLevelSubject oLevelResultSubject444 = new OLevelSubject();
                OLevelSubject oLevelResultSubject555 = new OLevelSubject();
                OLevelSubject oLevelResultSubject666 = new OLevelSubject();
                OLevelSubject oLevelResultSubject777 = new OLevelSubject();
                OLevelSubject oLevelResultSubject888 = new OLevelSubject();
                OLevelSubject oLevelResultSubject999 = new OLevelSubject();
                OLevelSubject oLevelResultSubject100 = new OLevelSubject();
                OLevelSubject oLevelResultSubject110 = new OLevelSubject();
                OLevelSubject oLevelResultSubject120 = new OLevelSubject();
                OLevelSubject oLevelResultSubject130 = new OLevelSubject();
                OLevelSubject oLevelResultSubject140 = new OLevelSubject();
                OLevelSubject oLevelResultSubject150 = new OLevelSubject();

                OLevelSubjects = new List<OLevelSubject>();
                OLevelSubjects.Add(oLevelResultSubject1);
                OLevelSubjects.Add(oLevelResultSubject2);
                OLevelSubjects.Add(oLevelResultSubject3);
                OLevelSubjects.Add(oLevelResultSubject4);
                OLevelSubjects.Add(oLevelResultSubject5);
                OLevelSubjects.Add(oLevelResultSubject6);
                OLevelSubjects.Add(oLevelResultSubject7);
                OLevelSubjects.Add(oLevelResultSubject8);
                OLevelSubjects.Add(oLevelResultSubject9);
                OLevelSubjects.Add(oLevelResultSubject10);
                OLevelSubjects.Add(oLevelResultSubject11);
                OLevelSubjects.Add(oLevelResultSubject12);
                OLevelSubjects.Add(oLevelResultSubject13);
                OLevelSubjects.Add(oLevelResultSubject14);
                OLevelSubjects.Add(oLevelResultSubject15);

                OLevelSubjectsAlternatives = new List<OLevelSubject>();
                OLevelSubjectsAlternatives.Add(oLevelResultSubject111);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject222);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject333);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject444);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject555);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject666);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject777);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject888);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject999);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject100);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject110);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject120);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject130);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject140);
                OLevelSubjectsAlternatives.Add(oLevelResultSubject150);
            }
            catch (Exception)
            {
                throw;
            }
        }

    
        public void LoadApplicantResult(Person person)
        {
            try
            {
              
                    OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
                    OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
                    

                    ApplicantFirstSittingOLevelResult = oLevelResultLogic.GetModelBy(p => p.Person_Id == person.Id && p.O_Level_Exam_Sitting_Id == 1);
                    ApplicantSecondSittingOLevelResult = oLevelResultLogic.GetModelBy(p => p.Person_Id == person.Id && p.O_Level_Exam_Sitting_Id == 2);
                   
                    if (ApplicantFirstSittingOLevelResult != null && ApplicantFirstSittingOLevelResult.Id > 0)
                    {
                        ApplicantFirstSittingOLevelResultDetailsList = oLevelResultDetailLogic.GetModelsBy(p => p.Applicant_O_Level_Result_Id == ApplicantFirstSittingOLevelResult.Id);
                    }

                    if (ApplicantSecondSittingOLevelResult != null && ApplicantSecondSittingOLevelResult.Id > 0)
                    {
                        ApplicantSecondSittingOLevelResultDetailsList = oLevelResultDetailLogic.GetModelsBy(p => p.Applicant_O_Level_Result_Id == ApplicantSecondSittingOLevelResult.Id);
                    }

                  
                    SetApplicantFirstSittingOLevelResult(ApplicantFirstSittingOLevelResult, ApplicantFirstSittingOLevelResultDetailsList);
                    SetApplicantSecondSittingOLevelResult(ApplicantSecondSittingOLevelResult, ApplicantSecondSittingOLevelResultDetailsList);
                   
            }
            catch (Exception)
            {
                throw;
            }
        }
      
        public void SetApplicantFirstSittingOLevelResult(OLevelResult oLevelResult, List<OLevelResultDetail> oLevelResultDetails)
        {
            try
            {
                if (oLevelResult != null && oLevelResult.Id > 0)
                {
                    if (oLevelResult.Type != null)
                    {
                        FirstSittingOLevelResult.Type = oLevelResult.Type;
                    }
                    else
                    {
                        FirstSittingOLevelResult.Type = new OLevelType();
                    }

                    FirstSittingOLevelResult.ExamNumber = oLevelResult.ExamNumber;
                    FirstSittingOLevelResult.ExamYear = oLevelResult.ExamYear;

                    if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                    {
                        for (int i = 0; i < oLevelResultDetails.Count; i++)
                        {
                            FirstSittingOLevelResultDetailsList[i].Subject = oLevelResultDetails[i].Subject;
                            FirstSittingOLevelResultDetailsList[i].Grade = oLevelResultDetails[i].Grade;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SetApplicantSecondSittingOLevelResult(OLevelResult oLevelResult, List<OLevelResultDetail> oLevelResultDetails)
        {
            try
            {
                if (oLevelResult != null && oLevelResult.Id > 0)
                {
                    if (oLevelResult.Type != null)
                    {
                        SecondSittingOLevelResult.Type = oLevelResult.Type;
                    }
                    else
                    {
                        SecondSittingOLevelResult.Type = new OLevelType();
                    }

                    SecondSittingOLevelResult.ExamNumber = oLevelResult.ExamNumber;
                    SecondSittingOLevelResult.ExamYear = oLevelResult.ExamYear;

                    if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                    {
                        for (int i = 0; i < oLevelResultDetails.Count; i++)
                        {
                            if (oLevelResultDetails[i].Subject != null)
                            {
                                SecondSittingOLevelResultDetailsList[i].Subject = oLevelResultDetails[i].Subject;
                            }
                            else
                            {
                                SecondSittingOLevelResultDetailsList[i].Subject = new OLevelSubject();
                            }
                            if (oLevelResultDetails[i].Grade != null)
                            {
                                SecondSittingOLevelResultDetailsList[i].Grade = oLevelResultDetails[i].Grade;
                            }
                            else
                            {
                                SecondSittingOLevelResultDetailsList[i].Grade = new OLevelGrade();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SelectListItem> ProgrammeSelectListItem { get; set; }

        public List<SelectListItem> OLevelGradeSelectList { get; set; }
        public OLevelSubject OLevelSubject { get; set; }
        public AdmissionCriteriaForOLevelSubject AdmissionCriteriaForOLevelSubjectModel { get; set; }
    }

    public class CriteriaJsonResult
    {
        public bool IsError { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string Alternatives { get; set; }
    }
}