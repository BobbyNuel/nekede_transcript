﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Admin.ViewModels
{
    public class TranscriptProcessorViewModel
    {
        const int START_YEAR = 1920;
        public TranscriptProcessorViewModel()
        {
            transcriptSelectList = Utility.PopulateTranscriptStatusSelectListItem();
            transcriptClearanceSelectList = Utility.PopulateTranscriptClearanceStatusSelectListItem();
            transcriptRequest = new TranscriptRequest();
            StateSelectList = Utility.PopulateStateSelectListItem();
            CountrySelectList = Utility.PopulateCountrySelectListItem();
            SexSelectList = Utility.PopulateSexSelectListItem();
            DepartmentSelectList = Utility.PopulateAllDepartmentSelectListItem();
            LocalGovernmentSelectList = Utility.PopulateLocalGovernmentSelectListItem();
            ProgrammeSelectList = Utility.PopulateAllProgrammeSelectListItem();
            LevelSelectList = Utility.PopulateLevelSelectListItem();
            SessionSelectList = Utility.PopulateAllSessionSelectListItem();
            ReligionSelectList = Utility.PopulateReligionSelectListItem();
            MaritalStatusSelectList = Utility.PopulateMaritalStatusSelectListItem();
            RelationshipSelectList = Utility.PopulateRelationshipSelectListItem();
            ModeOfEntrySelectList = Utility.PopulateModeOfEntrySelectListItem();
            ModeOfStudySelectList = Utility.PopulateModeOfStudySelectListItem();
            GradeSelectList = Utility.PopulateScoreGradeSelectListItem();
            MonthSelectListItems = Utility.PopulateMonthSelectListItem();
            YearSelectListItems = Utility.PopulateYearSelectListItem(START_YEAR, false);
        }
        public List<SelectListItem> transcriptSelectList { get; set; }
        public List<SelectListItem> transcriptClearanceSelectList { get; set; }
        public List<TranscriptRequest> transcriptRequests { get; set; }
        public TranscriptRequest transcriptRequest { get; set; }
        public TranscriptStatus transcriptStatus { get; set; }
        public TranscriptClearanceStatus transcriptClearanceStatus { get; set; }
        public Person Person { get; set; }
        public Model.Model.Student Student { get; set; }
        public List<SelectListItem> StateSelectList { get; set; }
        public List<SelectListItem> CountrySelectList { get; set; }
        public String RequestDateString { get; set; }
        public bool StudentExist { get; set; }
        public StudentLevel StudentLevel { get; set; }
        public List<SelectListItem> SexSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectList { get; set; }
        public List<SelectListItem> LocalGovernmentSelectList { get; set; }
        public List<SelectListItem> ProgrammeSelectList { get; set; }
        public List<SelectListItem> LevelSelectList { get; set; }
        public List<SelectListItem> SessionSelectList { get; set; }
        public List<SelectListItem> ReligionSelectList { get; set; }
        public List<SelectListItem> MaritalStatusSelectList { get; set; }
        public List<SelectListItem> RelationshipSelectList { get; set; }
        public List<SelectListItem> ModeOfEntrySelectList { get; set; }
        public List<SelectListItem> ModeOfStudySelectList { get; set; }
        public List<SelectListItem> GradeSelectList { get; set; }
        public Semester Semester { get; set; }
        public Session Session { get; set; }
        public Programme Programme { get; set; }
        public Department Department { get; set; }
        public Level Level { get; set; }
        public Course Course { get; set; }
        public StudentSponsor StudentSponsor { get; set; }
        public PersonGuardian PersonGuardian { get; set; }
        public StudentAcademicInformation StudentAcademicInformation { get; set; }
        public List<Course> Courses { get; set; }
        public List<Course> FirstSemesterCourses { get; set; }
        public List<Course> SecondSemesterCourses { get; set; }
        public List<Course> FirstSemesterCoursesNd1 { get; set; }
        public List<Course> SecondSemesterCoursesNd1 { get; set; }
        public List<Course> CarryOverCourses { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public bool SelectCourseInSession { get; set; }
        public List<CourseRegistrationDetail> CourseRegistrationDetails { get; set; }
        public List<SelectListItem> MonthSelectListItems { get; set; }
        public List<SelectListItem> YearSelectListItems { get; set; }
    }

    public class PersonModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string OtherName { get; set; }
        public string SexId { get; set; }
        public string MobilePhone { get; set; }
        public string Email { get; set; }
        public string DOBString { get; set; }
        public string ContactAddress { get; set; }
        public string StateId { get; set; }
        public string LGAId { get; set; }
        public string ReligionId { get; set; }
        public string MaritalStatusId { get; set; }
        public string MatricNumberString { get; set; }
        public string Id { get; set; }
        public string PlaceOfBirth { get; set; }
        public string SponsorRelationship { get; set; }
        public string SponsorName { get; set; }
        public string SponsorAddress { get; set; }
        public string SponsorMobilePhone { get; set; }
        public string SponsorEmail { get; set; }
        public string GuardianName { get; set; }
        public string GuardianAddress { get; set; }
        public string GuardianMobilePhone { get; set; }
        public string GuardianEmail { get; set; }
        public string GuardianOccupation { get; set; }
        public string ModeOfEntry { get; set; }
        public string ModeOfStudy { get; set; }
        public string DateOfEntry { get; set; }
        public string LastSchoolAttended { get; set; }
        public string StudentAward { get; set; }
        public string AwardDate { get; set; }

        public string MonthOfGraduation { get; set; }
        public string YearOfGraduation { get; set; }
    }
}