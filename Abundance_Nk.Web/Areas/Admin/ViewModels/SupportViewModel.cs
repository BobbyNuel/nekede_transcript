﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Abundance_Nk.Web.Areas.Admin.ViewModels
{
    public class SupportViewModel 
    {
        private PaymentLogic paymentLogic;
        private DepartmentLogic departmentLogic;
        private PersonLogic personLogic;

        private OLevelTypeLogic oLevelTypeLogic;
        private OLevelGradeLogic oLevelGradeLogic;
        private OLevelSubjectLogic oLevelSubjectLogic;
        private ResultGradeLogic resultGradeLogic;
        
        public SupportViewModel()
        {
            paymentLogic = new PaymentLogic();
            personLogic = new PersonLogic();
            oLevelTypeLogic = new OLevelTypeLogic();
            oLevelGradeLogic = new OLevelGradeLogic();
            oLevelSubjectLogic = new OLevelSubjectLogic();
            resultGradeLogic = new ResultGradeLogic();

            ProgrammeSelectListItem = Utility.PopulateProgrammeSelectListItem();
            StatusSelectListItem = Utility.PopulateApplicantStatusSelectListItem();

            FeeTypeSelectList = Utility.PopulateFeeTypeSelectListItem();
            ProgrammeSelectListItem = Utility.PopulateAllProgrammeSelectListItem();
            ExamYearSelectList = Utility.PopulateExamYearSelectListItem(1990);
            OLevelTypeSelectList = Utility.PopulateOLevelTypeSelectListItem();
            OLevelGradeSelectList = Utility.PopulateOLevelGradeSelectListItem();
            OLevelSubjectSelectList = Utility.PopulateOLevelSubjectSelectListItem();
            ResultGradeSelectList = Utility.PopulateResultGradeSelectListItem();
            LevelSelectList = Utility.PopulateLevelSelectListItem();
            SessionSelectListItem = Utility.PopulateSessionSelectListItem();
            PaymentEtranzactTypeSelectList = Utility.PopulatePaymentEtranzactTypeSelectListItem();
            PaymentTerminalSelectList = Utility.PopulatePaymentTerminalSelectListItem();
            PaymentModeSelectListItem = Utility.PopulatePaymentModeSelectListItem();


            if (Programme!= null && Programme.Id > 0)
            {
                DepartmentSelectListItem = Utility.PopulateDepartmentSelectListItem(Programme);
            }
            EtranzactPins = new List<PaymentEtranzact>();

        }

        [Display(Name="Invoice Number")]
        public string InvoiceNumber { get; set; }
        [RegularExpression("^0[0-9]{10}$", ErrorMessage = "Phone number is not valid")]
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string Pin { get; set; }
        [Display(Name = "Message Content")]
        public string Body { get; set; }
        public Department Department { get; set; }
        public Programme Programme { get; set; }
        public List<SelectListItem> FeeTypeSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public List<SelectListItem> ProgrammeSelectListItem { get; set; }
        public List<SelectListItem> StatusSelectListItem { get; set; }
        public Abundance_Nk.Model.Model.Payment Payment { get; set; }
        public Abundance_Nk.Model.Model.AppliedCourse AppliedCourse { get; set; }
        public Abundance_Nk.Model.Model.Person Person { get; set; }
        public Abundance_Nk.Model.Model.ApplicantJambDetail ApplicantJambDetail { get; set; }
        public List<PersonAudit> personAudit { get; set; }
        public PersonAudit personAuditDetails { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
        public List<PaymentEtranzact> EtranzactPins { get; set; }
        public Abundance_Nk.Model.Model.Applicant Applicants { get; set; }
        public Abundance_Nk.Model.Model.Student Student { get; set; }
        public FeeType FeeType { get; set; }
        public PaymentEtranzact PaymentEtranzact { get; set; }
        public List<SelectListItem> PaymentEtranzactTypeSelectList { get; set; }
        public List<SelectListItem> PaymentTerminalSelectList { get; set; }
        public List<SelectListItem> PaymentModeSelectListItem { get; set; }
        public List<SelectListItem> ExamYearSelectList { get; set; }
        public List<SelectListItem> OLevelTypeSelectList { get; set; }
        public List<SelectListItem> OLevelGradeSelectList { get; set; }
        public List<SelectListItem> OLevelSubjectSelectList { get; set; }
        public List<SelectListItem> ResultGradeSelectList { get; set; }
        public List<SelectListItem> LevelSelectList { get; set; }

        public OLevelResult FirstSittingOLevelResult { get; set; }
        public OLevelResult SecondSittingOLevelResult { get; set; }
        public OLevelResultDetail FirstSittingOLevelResultDetail { get; set; }
        public OLevelResultDetail SecondSittingOLevelResultDetail { get; set; }
        public List<OLevelResultDetail> FirstSittingOLevelResultDetails { get; set; }
        public List<OLevelResultDetail> SecondSittingOLevelResultDetails { get; set; }
        public HttpPostedFileBase File { get; set; }

        public void InitialiseOLevelResult()
        {
            try
            {
                List<OLevelResultDetail> oLevelResultDetails = new List<OLevelResultDetail>();
                OLevelResultDetail oLevelResultDetail1 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail2 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail3 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail4 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail5 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail6 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail7 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail8 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail9 = new OLevelResultDetail();

                OLevelResultDetail oLevelResultDetail11 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail22 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail33 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail44 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail55 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail66 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail77 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail88 = new OLevelResultDetail();
                OLevelResultDetail oLevelResultDetail99 = new OLevelResultDetail();

                FirstSittingOLevelResultDetails = new List<OLevelResultDetail>();
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail1);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail2);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail3);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail4);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail5);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail6);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail7);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail8);
                FirstSittingOLevelResultDetails.Add(oLevelResultDetail9);

                SecondSittingOLevelResultDetails = new List<OLevelResultDetail>();
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail11);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail22);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail33);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail44);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail55);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail66);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail77);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail88);
                SecondSittingOLevelResultDetails.Add(oLevelResultDetail99);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ApplicationForm GetApplicationFormBy(Person person, Payment payment)
        {
            try
            {
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                return applicationFormLogic.GetModelBy(a => a.Person_Id == person.Id && a.Payment_Id == payment.Id);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void PopulateDropdowns()
        {
            try
            {

                OLevelTypes = oLevelTypeLogic.GetAll();
                OLevelGrades = oLevelGradeLogic.GetAll();
                OLevelSubjects = oLevelSubjectLogic.GetAll();
                ResultGrades = resultGradeLogic.GetAll();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public List<ApplicantJambDetail> ApplicantJambDetailList { get; set; }
        public List<OLevelType> OLevelTypes { get; set; }
        public List<OLevelGrade> OLevelGrades { get; set; }
        public List<OLevelSubject> OLevelSubjects { get; set; }
        public List<ResultGrade> ResultGrades { get; set; }
        public List<SelectListItem> SessionSelectListItem { get; set; }
        public Session Session { get; set; }
        public List<AppliedCourse> AppliedCourseList { get; set; }
    }
}