﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Web.Areas.Admin.ViewModels;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class ReportController : Controller
    {
        public ReportViewModel ReportViewModel;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ApplicationFormSummary()
        {
            return PartialView();
        }

        //
        // GET: /Admin/Report/
        public ActionResult ApplicationSummary()
        {
            return PartialView();
        }

        public ActionResult ListOfApplications()
        {
            return View();
        }

        public ActionResult PhotoCard()
        {
            return View();
        }

        public ActionResult AcceptanceReport()
        {
            return View();
        }
        public ActionResult NDMApplicantReport()
        {
            return View();
        }
        public ActionResult AllProgrammeRanking()
        {
            return View();
        }

        public ActionResult AdmissionProcessing()
        {
            AppliedCourseLogic appliedCourseLogic = new Business.AppliedCourseLogic();
            AdmissionCriteriaLogic admissionCriteriaLogic = new AdmissionCriteriaLogic();

            AppliedCourse appliedCourse = appliedCourseLogic.GetModelBy(m => m.Person_Id == 152);


            string rejectReason = admissionCriteriaLogic.EvaluateApplication(appliedCourse);
            ViewBag.RejectReason = rejectReason;
            return View();
        }
        public ActionResult AttendanceReportBulk()
        {
            return View();
        }

        public ActionResult NotificationOfResult(long personId, int semesterId, int sessionId, int programmeId, int departmentId, int levelId)
        {
            ViewBag.personId = personId;
            ViewBag.semesterId = semesterId;
            ViewBag.sessionId = sessionId;
            ViewBag.programmeId = programmeId;
            ViewBag.departmentId = departmentId;
            ViewBag.semesterId = semesterId;
            ViewBag.levelId = levelId;
            return View();
        }
        public ActionResult NotificationOfResultBulk()
        {
            return View();
        }
        public ActionResult StatementOfResultBulk()
        {
            return View();
        }
        public ActionResult MasterGradeSheet()
        {
            return View();
        }

        public ActionResult ResultSheet()
        {
            ReportViewModel = new ReportViewModel();
            if (TempData["ReportViewModel"] != null)
            {
                ReportViewModel = (ReportViewModel)TempData["ReportViewModel"];
                ViewBag.levelId = ReportViewModel.Level.Id;
                ViewBag.semesterId = ReportViewModel.Semester.Id;
                ViewBag.progId = ReportViewModel.Programme.Id;
                ViewBag.deptId = ReportViewModel.Department.Id;
                ViewBag.sessionId = ReportViewModel.Session.Id;
                ViewBag.courseId = ReportViewModel.Course.Id;
            }

            return View();
        }

        public ActionResult UnregisteredStudentResultSheet(string levelId, string semesterId, string progId, string deptId, string sessionId, string courseId)
        {
            ViewBag.levelId = levelId;
            ViewBag.semesterId = semesterId;
            ViewBag.progId = progId;
            ViewBag.deptId = deptId;
            ViewBag.sessionId = sessionId;
            ViewBag.courseId = courseId;
            return View();
        }
        public ActionResult ExamPassList()
        {
            return View();
        }
        public ActionResult ExamReferenceList()
        {
            return View();
        }
        public ActionResult ApplicantResultDetails()
        {
            return View();
        }
        public ActionResult AdmittedStudentReport()
        {
            return View();
        }
        public ActionResult AdmissionCountSummary()
        {
            return View();
        }
        public ActionResult Transcript(long personId)
        {
            ViewBag.PersonId = personId.ToString();
            return View();
        }
	}
}