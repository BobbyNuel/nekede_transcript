﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;
using System.Linq.Expressions;
using System.IO;

namespace Abundance_Nk.Web.Areas.Admin.Views
{
    public class SupportController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string VALUE = "Value";
        private const string TEXT = "Text";
        private Abundance_Nk_NekedeEntities db = new Abundance_Nk_NekedeEntities();
        private SupportViewModel viewmodel;
        private const string FIRST_SITTING = "FIRST SITTING";
        private const string SECOND_SITTING = "SECOND SITTING";

        // GET: /Admin/Support/
        public ActionResult Index()
        {
            viewmodel = new SupportViewModel();
            try
            {
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            TempData["SupportViewModel"] = viewmodel;
            return View();
        }

        [HttpPost]
        public ActionResult Index(SupportViewModel vModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (vModel.InvoiceNumber != null)
                    {
                        Payment payment = new Payment();
                        PaymentLogic paymentLogic = new PaymentLogic();
                        PaymentEtranzact p = new PaymentEtranzact();
                        PaymentEtranzactLogic pe = new PaymentEtranzactLogic();
                        p = pe.GetModelBy(n => n.Confirmation_No == vModel.InvoiceNumber);
                        if (p != null && p.ConfirmationNo != null)
                        {
                            payment = paymentLogic.GetModelBy(m => m.Invoice_Number == p.CustomerID);
                        }
                        else
                        {

                            payment = paymentLogic.GetModelBy(m => m.Invoice_Number == vModel.InvoiceNumber);

                        }
                        
                        if (payment != null && payment.Id > 0)
                        {
                            AppliedCourse appliedcourse = new AppliedCourse();
                            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                            ApplicantJambDetail applicantjambdetail = new ApplicantJambDetail();
                            ApplicantJambDetailLogic applicantjambdetailLogic = new ApplicantJambDetailLogic();

                            appliedcourse = appliedCourseLogic.GetModelBy(n => n.Person_Id == payment.Person.Id);
                            if (appliedcourse != null && appliedcourse.Department.Id > 0)
                            {
                                applicantjambdetail = applicantjambdetailLogic.GetModelBy(x => x.Person_Id == payment.Person.Id);
                                if (applicantjambdetail != null && applicantjambdetail.Person.Id > 0)
                                {
                                    vModel.ApplicantJambDetail = applicantjambdetail;
                                }


                                vModel.AppliedCourse = appliedcourse;
                                vModel.DepartmentSelectListItem = Utility.PopulateDepartmentSelectListItem(appliedcourse.Programme);
                                vModel.ProgrammeSelectListItem = Utility.PopulateAllProgrammeSelectListItem();

                                if (vModel.DepartmentSelectListItem != null)
                                {
                                    ViewBag.DepartmentId = new SelectList(vModel.DepartmentSelectListItem, VALUE, TEXT, appliedcourse.Department.Id);

                                }
                                else
                                {
                                    ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

                                }
                                if (vModel.ProgrammeSelectListItem != null)
                                {

                                    ViewBag.ProgrammeId = new SelectList(vModel.ProgrammeSelectListItem, VALUE, TEXT, appliedcourse.Programme.Id);

                                }
                                else
                                {
                                    ViewBag.ProgrammeId = new SelectList(new List<Programme>(), ID, NAME);

                                }

                                ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), VALUE, TEXT, payment.FeeType.Id);

                                vModel.Payment = payment;
                                vModel.Person = payment.Person;
                                vModel.InvoiceNumber = payment.InvoiceNumber;
                                TempData["SupportViewModel"] = viewmodel;
                                return View(vModel);
                            }


                        }
                        else
                        {
                            SetMessage("Invoice does not exist! ", Message.Category.Error);
                            return View(vModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(vModel);
        }

        [HttpPost]
        public ActionResult UpdateInvoice(SupportViewModel vModel)
        {
            try
            {
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        error.ErrorMessage.ToString();
                    }
                }

                ModelState.Remove("Person.DateEntered");
                ModelState.Remove("Person.DateOfBirth");
                ModelState.Remove("Person.FirstName");
                ModelState.Remove("Person.LastName");
                ModelState.Remove("Person.MobilePhone");
                ModelState.Remove("Person.Sex.Id");
                ModelState.Remove("Person.Religion.Id");
                ModelState.Remove("Person.LocalGovernment.Id");
                ModelState.Remove("AppliedCourse.Option.Id");
                ModelState.Remove("AppliedCourse.ApplicationForm.Id");
                ModelState.Remove("ApplicantJambDetail.Person.Id");
                ModelState.Remove("ApplicantJambDetail.JambRegistrationNumber");
                ModelState.Remove("FeeType.Name");


                if (ModelState.IsValid)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        string operation = "UPDATE";
                        string action = "MODIFY APPLICANT PERSON AND APPLIED COURSE DETAILS";
                        string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";

                        if (vModel.Person != null && vModel.AppliedCourse != null)
                        {
                            PersonAudit personAudit = new PersonAudit();
                            UserLogic loggeduser = new UserLogic();
                            personAudit.User = loggeduser.GetModelBy(u => u.User_Name == User.Identity.Name);
                            personAudit.Operation = operation;
                            personAudit.Action = action;
                            personAudit.Time = DateTime.Now;
                            personAudit.Client = client;

                            PersonLogic personLogic = new PersonLogic();
                            bool personChanged = personLogic.Modify(vModel.Person, personAudit);


                            AppliedCourseAudit appliedCourseAudit = new AppliedCourseAudit();
                            appliedCourseAudit.User = loggeduser.GetModelBy(u => u.User_Name == User.Identity.Name);
                            appliedCourseAudit.Operation = operation;
                            appliedCourseAudit.Action = action;
                            appliedCourseAudit.Time = DateTime.Now;
                            appliedCourseAudit.Client = client;

                            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                            vModel.AppliedCourse.Person = vModel.Person;
                            bool appliedCourseChanged = appliedCourseLogic.Modify(vModel.AppliedCourse, appliedCourseAudit);

                            if (vModel.AppliedCourse.Department.Id != appliedCourseAudit.OldDepartment.Id)
                            {
                                ApplicationForm appForm = new ApplicationForm();
                                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                                appForm = applicationFormLogic.GetModelBy(m => m.Person_Id == vModel.Person.Id);
                                if (appForm != null)
                                {
                                    Department department = new Department();
                                    DepartmentLogic departmentLogic = new DepartmentLogic();
                                    department = departmentLogic.GetModelBy(d => d.Department_Id == vModel.AppliedCourse.Department.Id);
                                    vModel.AppliedCourse.Department = department;
                                    appForm = applicationFormLogic.SetNextExamNumber(appForm, vModel.AppliedCourse);
                                    applicationFormLogic.Modify(appForm);
                                }
                            }

                            if (vModel.ApplicantJambDetail.JambRegistrationNumber != null && appliedCourseAudit.OldProgramme.Id == 5 && appliedCourseAudit.Programme.Id == 5)
                            {
                                ApplicantJambDetail appDetail = new ApplicantJambDetail();
                                ApplicantJambDetailLogic appJambDetailLogic = new ApplicantJambDetailLogic();
                                appDetail = appJambDetailLogic.GetModelBy(m => m.Person_Id == appliedCourseAudit.AppliedCourse.Person.Id);

                                if (appDetail != null)
                                {
                                    appDetail.JambRegistrationNumber = vModel.ApplicantJambDetail.JambRegistrationNumber;
                                    appJambDetailLogic.Modify(appDetail);
                                }
                                else
                                {
                                    vModel.ApplicantJambDetail.Person = appliedCourseAudit.AppliedCourse.Person;
                                    appJambDetailLogic.Create(vModel.ApplicantJambDetail);
                                }

                            }
                            else if (vModel.ApplicantJambDetail.JambRegistrationNumber != null && appliedCourseAudit.OldProgramme.Id != 5 && appliedCourseAudit.Programme.Id == 5)
                            {
                                ApplicantJambDetailLogic appJambDetailLogic = new ApplicantJambDetailLogic();
                                vModel.ApplicantJambDetail.Person = appliedCourseAudit.AppliedCourse.Person;
                                appJambDetailLogic.Create(vModel.ApplicantJambDetail);

                            }
                            else if (appliedCourseAudit.Programme.Id != 5)
                            {
                                ApplicantJambDetailLogic appJambDetailLogic = new ApplicantJambDetailLogic();
                                Selector = r => r.PERSON.Person_Id == appliedCourseAudit.AppliedCourse.Person.Id;
                                appJambDetailLogic.Delete(Selector);
                            }

                            //ApplicationProgrammeFee ApplicationProgrammeFee = new ApplicationProgrammeFee();
                            //ApplicationProgrammeFeeLogic ApplicationProgrammeFeeLogic = new ApplicationProgrammeFeeLogic();
                            //ApplicationProgrammeFee = ApplicationProgrammeFeeLogic.GetModelBy(z => z.Programme_Id == appliedCourseAudit.Programme.Id && z.Session_Id == 2);

                            Payment payment = new Payment(); 
                            PaymentLogic PaymentLogic = new Business.PaymentLogic();
                            payment = PaymentLogic.GetModelBy(p => p.Person_Id == appliedCourseAudit.AppliedCourse.Person.Id && p.Invoice_Number == vModel.Payment.InvoiceNumber);
                            payment.FeeType = vModel.FeeType;
                            PaymentLogic.Modify(payment);

                            transaction.Complete();
                        }
                    }

                    TempData["Message"] = "Record was successfully updated";
                    return RedirectToAction("Index", "Support");
                }
            }
            catch (Exception ex)
            {
                TempData["Message"] = "System Message :" + ex.Message;
                return RedirectToAction("Index", "Support");
            }

            TempData["Message"] = "Record was not updated! Crosscheck entries and try again";
            return RedirectToAction("Index", "Support");
        }

        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };

                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected Expression<Func<APPLICANT_JAMB_DETAIL, bool>> Selector { get; set; }
        public ActionResult PinError()
        {
            viewmodel = new SupportViewModel();
            try
            {

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        [HttpPost]
        public ActionResult PinError(SupportViewModel vModel)
        {
            try
            {
                if (vModel.InvoiceNumber != null && vModel.Pin != null)
                {
                    Payment payment = new Payment();
                    PaymentLogic paymentLogic = new PaymentLogic();
                    PaymentEtranzact p = new PaymentEtranzact();
                    PaymentEtranzactLogic pe = new PaymentEtranzactLogic();
                    p = pe.GetModelBy(n => n.Confirmation_No == vModel.Pin);
                    if (p == null)
                    {
                        payment = paymentLogic.GetModelBy(m => m.Invoice_Number == vModel.InvoiceNumber);
                        if (payment != null)
                        {
                            if (pe.RetrieveErrorPin(vModel.Pin, "0330920031"))
                            {
                                TempData["Message"] = "Record was successfully updated";
                                return RedirectToAction("PinError", "Support");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        public ActionResult ViewAuditReport()
        {
            try
            {
                SupportViewModel supportModel = new SupportViewModel();

                List<PersonAudit> personAudit = new List<PersonAudit>();
                PersonAuditLogic personAuditLogic = new PersonAuditLogic();

                personAudit = personAuditLogic.GetAll();
                supportModel.personAudit = personAudit;
                return View(supportModel);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public ActionResult ViewAuditReportDetails(long Id)
        {
            try
            {
                SupportViewModel supportModel = new SupportViewModel();
                if (Id > 0)
                {
                    PersonAudit personAudit = new PersonAudit();
                    PersonAuditLogic personAuditLogic = new PersonAuditLogic();

                    personAudit = personAuditLogic.GetModelBy(p => p.Person_Audit_Id == Id);
                    supportModel.personAuditDetails = personAudit;



                    return View(supportModel);
                }

                return View(supportModel);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult PinRetrieval()
        {
            SupportViewModel vModel = new SupportViewModel();
            return View(vModel);
        }
        [HttpPost]
        public ActionResult PinRetrieval(SupportViewModel viewmodel)
        {
            ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
            PersonLogic personLogic = new PersonLogic();
            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
            List<PaymentEtranzact> paymentEtranzact = new List<PaymentEtranzact>();
            
            viewmodel.ApplicationForm = applicationFormLogic.GetModelBy(p => p.Application_Form_Number == viewmodel.ApplicationForm.Number);
            if (viewmodel.ApplicationForm != null && viewmodel.ApplicationForm.Person.Id > 0)
               {
                    paymentEtranzact = paymentEtranzactLogic.GetModelsBy(p => p.Used_By_Person_Id == viewmodel.ApplicationForm.Person.Id);
                    viewmodel.EtranzactPins = paymentEtranzact;
            
               }
           
            return View(viewmodel);

        }
          
        public ActionResult ResetInvoice()
        {
            try
            {

            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Warning);

            }
            return View();
        }

        [HttpPost]
        public ActionResult ResetInvoice(SupportViewModel viewmodel)
        {
            try
            {
                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                payment = paymentLogic.GetModelBy(n => n.Invoice_Number == viewmodel.InvoiceNumber && n.Fee_Type_Id != 1);
                if (payment != null && payment.Id > 0)
                {
                    RemitaPayment remitaPayment = new RemitaPayment();
                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();

                    remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                    if (remitaPayment != null)
                    {
                        if (remitaPayment.Status.Contains("01:"))
                        {
                            SetMessage("This invoice has been paid for and can't be deleted!", Message.Category.Warning);
                            return View(viewmodel);
                        }
                        else
                        {
                            ApplicationFormLogic appFormLogic = new ApplicationFormLogic();
                            viewmodel.ApplicationForm = appFormLogic.GetBy(payment.Person);
                            ApplicantStatus.Status newStatus = new ApplicantStatus.Status();
                            if (payment.FeeType.Id == 2 )
                            {
                                newStatus = ApplicantStatus.Status.SubmittedApplicationForm;
                            }
                           
                            OnlinePayment onlinePayment = new OnlinePayment();
                            OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                            onlinePayment = onlinePaymentLogic.GetBy(payment.Id);
                            if (onlinePayment != null)
                            {
                                using (TransactionScope scope = new TransactionScope())
                                {
                                    onlinePaymentLogic.DeleteBy(onlinePayment.Payment.Id);
                                    remitaPaymentLogic.DeleteBy(remitaPayment.payment.Id);
                                    paymentLogic.DeleteBy(payment.Id);

                                    ApplicantLogic applicantLogic = new ApplicantLogic();
                                    applicantLogic.UpdateStatus(viewmodel.ApplicationForm, newStatus);

                                    scope.Complete();
                                    SetMessage("The " + payment.FeeType.Name + "  invoice  " + payment.InvoiceNumber + "  for " + payment.Person.FullName + " has been deleted! Please log into your profile and generate a new one", Message.Category.Information);
                                    return View(viewmodel);
                                }

                            }

                        }
                    }
                    else
                    {

                        ApplicationFormLogic appFormLogic = new ApplicationFormLogic();
                        viewmodel.ApplicationForm = appFormLogic.GetBy(payment.Person);
                        ApplicantStatus.Status newStatus = new ApplicantStatus.Status();
                        if (payment.FeeType.Id == 2)
                        {
                            newStatus = ApplicantStatus.Status.SubmittedApplicationForm;
                        }
                       
                        OnlinePayment onlinePayment = new OnlinePayment();
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        onlinePayment = onlinePaymentLogic.GetBy(payment.Id);
                        if (onlinePayment != null)
                        {
                            PaymentEtranzact etranzact = new PaymentEtranzact();
                            PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
                            etranzact = etranzactLogic.GetBy(payment);
                            if (etranzact == null)
                            {
                                using (TransactionScope scope = new TransactionScope())
                                {
                                    onlinePaymentLogic.DeleteBy(onlinePayment.Payment.Id);
                                    paymentLogic.DeleteBy(payment.Id);

                                    ApplicantLogic applicantLogic = new ApplicantLogic();
                                    applicantLogic.UpdateStatus(viewmodel.ApplicationForm, newStatus);

                                    scope.Complete();
                                    SetMessage("The " + payment.FeeType.Name + "  invoice  " + payment.InvoiceNumber + "  for " + payment.Person.FullName + " has been deleted! Please log into your profile and generate a new one", Message.Category.Information);
                                    return View(viewmodel);
                                }
                            }
                            else
                            {
                                SetMessage("The " + payment.FeeType.Name + "  invoice  " + payment.InvoiceNumber + "  for " + payment.Person.FullName + " is tied to a pin and cannot be deleted! ", Message.Category.Information);
                                return View(viewmodel);
                            }
                           

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Warning);
            }

            return View(viewmodel);
        }
        public ActionResult ResetStep()
        {
            try
            {
                viewmodel = new SupportViewModel();
                ViewBag.StatusId = viewmodel.StatusSelectListItem;
               
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Warning);

            }
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult ResetStep(SupportViewModel viewmodel)
        {
            try
            {
                if (viewmodel.ApplicationForm.Number != null)
                {
                    ApplicantLogic applicantLogic = new ApplicantLogic();
                    viewmodel.Applicants = applicantLogic.GetApplicantsBy(viewmodel.ApplicationForm.Number);
                   
                    if (viewmodel.Applicants.Status != null)
                    {
                        ViewBag.StatusId = new SelectList(viewmodel.StatusSelectListItem, VALUE, TEXT, viewmodel.Applicants.Status.Id);
                    }
                    else
                    {
                        ViewBag.StatusId = viewmodel.StatusSelectListItem;
               
                    }

                    
                }
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Warning);

            }
            return View(viewmodel);
        }
        [HttpPost]
        public ActionResult UpdateStep(SupportViewModel viewmodel)
        {
            try
            {
                ApplicantLogic applicantLogic = new ApplicantLogic();
                ApplicantStatus.Status status = (ApplicantStatus.Status)viewmodel.Applicants.Status.Id;
                applicantLogic.UpdateStatus(viewmodel.Applicants.ApplicationForm, status); 
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Warning);
            }
            SetMessage("Updated Successfully",Message.Category.Information);
            return RedirectToAction("ResetStep");
        }
        public ActionResult CorrectOlevel()
        {
            viewmodel = new SupportViewModel();
            try
            {
                PopulateOlevelDropdowns(viewmodel);
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);

            }
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult CorrectOlevel(SupportViewModel supportModel)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    if (supportModel.InvoiceNumber != null)
                    {
                        Payment payment = new Payment();
                        PaymentLogic paymentLogic = new PaymentLogic();
                        PaymentEtranzact p = new PaymentEtranzact();
                        PaymentEtranzactLogic pe = new PaymentEtranzactLogic();
                        p = pe.GetModelBy(n => n.Confirmation_No == supportModel.InvoiceNumber);
                        if (p != null && p.ConfirmationNo != null)
                        {
                            payment = paymentLogic.GetModelBy(m => m.Invoice_Number == p.CustomerID);
                        }
                        else
                        {

                            payment = paymentLogic.GetModelBy(m => m.Invoice_Number == supportModel.InvoiceNumber);

                        }

                        if (payment != null && payment.Id > 0)
                        {
                            AppliedCourse appliedcourse = new AppliedCourse();
                            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                            OLevelResultDetail resultDetail = new OLevelResultDetail();

                            appliedcourse = appliedCourseLogic.GetModelBy(n => n.Person_Id == payment.Person.Id);
                            if (appliedcourse != null && appliedcourse.Department.Id > 0)
                            {
                                ApplicationForm applicationform = supportModel.GetApplicationFormBy(payment.Person, payment);
                                if (applicationform != null && applicationform.Id > 0)
                                {


                                    OLevelResult olevelResult = new OLevelResult();
                                    OLevelResultLogic olevelResultLogic = new OLevelResultLogic();
                                    olevelResult = olevelResultLogic.GetModelBy(m => m.Person_Id == payment.Person.Id && m.O_Level_Exam_Sitting_Id == 1 && m.Application_Form_Id != null);
                                    if (olevelResult != null)
                                    {
                                        List<OLevelResultDetail> olevelResultdetails = new List<OLevelResultDetail>();
                                        OLevelResultDetailLogic olevelResultdetailsLogic = new OLevelResultDetailLogic();

                                        olevelResultdetails = olevelResultdetailsLogic.GetModelsBy(m => m.Applicant_O_Level_Result_Id == olevelResult.Id);
                                        supportModel.FirstSittingOLevelResult = olevelResult;
                                        supportModel.FirstSittingOLevelResultDetails = olevelResultdetails;
                                    }

                                    olevelResult = olevelResultLogic.GetModelBy(m => m.Person_Id == payment.Person.Id && m.O_Level_Exam_Sitting_Id == 2 && m.Application_Form_Id != null);
                                    if (olevelResult != null)
                                    {
                                        List<OLevelResultDetail> olevelResultdetails = new List<OLevelResultDetail>();
                                        OLevelResultDetailLogic olevelResultdetailsLogic = new OLevelResultDetailLogic();

                                        olevelResultdetails = olevelResultdetailsLogic.GetModelsBy(m => m.Applicant_O_Level_Result_Id == olevelResult.Id);
                                        supportModel.SecondSittingOLevelResult = olevelResult;
                                        supportModel.SecondSittingOLevelResultDetails = olevelResultdetails;
                                    }
                                }

                                supportModel.AppliedCourse = appliedcourse;
                                supportModel.Payment = payment;
                                supportModel.Person = payment.Person;
                                supportModel.InvoiceNumber = payment.InvoiceNumber;
                                //SetSelectedSittingSubjectAndGrade(supportModel);
                                PopulateOlevelDropdowns(supportModel);
                                supportModel.PopulateDropdowns();


                                TempData["SupportViewModel"] = supportModel;
                                return View(supportModel);
                            }

                        }
                        else
                        {
                            SetMessage("Invoice does not exist! ", Message.Category.Error);
                            return View(supportModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);

            }

            return View(viewmodel);
        }
        private void PopulateOlevelDropdowns(SupportViewModel viewmodel)
        {
            try
            {
                ViewBag.FirstSittingOLevelTypeId = viewmodel.OLevelTypeSelectList;
                ViewBag.SecondSittingOLevelTypeId = viewmodel.OLevelTypeSelectList;
                ViewBag.FirstSittingExamYearId = viewmodel.ExamYearSelectList;
                ViewBag.SecondSittingExamYearId = viewmodel.ExamYearSelectList;
                ViewBag.ResultGradeId = viewmodel.ResultGradeSelectList;

                SetSelectedSittingSubjectAndGrade(viewmodel);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        private void SetSelectedSittingSubjectAndGrade(SupportViewModel existingViewModel)
        {
            try
            {
                if (existingViewModel != null && existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail firstSittingOLevelResultDetail in existingViewModel.FirstSittingOLevelResultDetails)
                    {
                        if (firstSittingOLevelResultDetail.Subject != null && firstSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Subject.Id);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
                        }

                        i++;
                    }
                }

                if (existingViewModel != null && existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail secondSittingOLevelResultDetail in existingViewModel.SecondSittingOLevelResultDetails)
                    {
                        if (secondSittingOLevelResultDetail.Subject != null && secondSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Subject.Id);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
                        }

                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        [HttpPost]
        public ActionResult UpdateOlevel(SupportViewModel viewModel)
        {

            try
            {
                ModelState.Remove("Person.DateEntered");
                ModelState.Remove("Person.DateOfBirth");
                ModelState.Remove("Person.FirstName");
                ModelState.Remove("Person.LastName");
                ModelState.Remove("Person.MobilePhone");
                ModelState.Remove("Person.Sex.Id");
                ModelState.Remove("Person.Religion.Id");
                ModelState.Remove("Person.LocalGovernment.Id");
                ModelState.Remove("AppliedCourse.Option.Id");
                ModelState.Remove("AppliedCourse.ApplicationForm.Id");
                ModelState.Remove("ApplicantJambDetail.Person.Id");
                ModelState.Remove("ApplicationForm.Id");
                ModelState.Remove("ApplicationForm.Person.Id");

                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        //DoSomethingWith(error);
                        SetMessage(error.ErrorMessage, Message.Category.Information);
                    }
                }

                viewModel.PopulateDropdowns();

                if (InvalidOlevelSubjectOrGrade(viewModel.FirstSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, FIRST_SITTING))
                {
                    SetMessage("O-level Subjects contains duplicates! ", Message.Category.Error);
                    return RedirectToAction("CorrectOlevel");
                }

                if (InvalidNumberOfOlevelSubject(viewModel.FirstSittingOLevelResultDetails, viewModel.SecondSittingOLevelResultDetails))
                {
                    SetMessage("Invalid number of O-level Subjects! ", Message.Category.Error);
                    return RedirectToAction("CorrectOlevel");
                }
                if (InvalidOlevelSubjectOrGrade(viewModel.SecondSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, SECOND_SITTING))
                {
                    SetMessage("Second sitting O-level Subjects contains duplicates! ", Message.Category.Error);
                    return RedirectToAction("CorrectOlevel");
                }

                using (TransactionScope transaction = new TransactionScope())
                {
                    OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
                    OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();

                    ApplicationFormLogic appFormLogic = new ApplicationFormLogic();
                    viewModel.ApplicationForm = appFormLogic.GetBy(viewModel.AppliedCourse.ApplicationForm.Id);

                    //get applicant's applied course
                    if (viewModel.FirstSittingOLevelResult == null || viewModel.FirstSittingOLevelResult.Id <= 0)
                    {
                        viewModel.FirstSittingOLevelResult.ApplicationForm = viewModel.ApplicationForm;
                        viewModel.FirstSittingOLevelResult.Person = viewModel.ApplicationForm.Person;
                        viewModel.FirstSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 1 };
                    }

                    if (viewModel.SecondSittingOLevelResult == null || viewModel.SecondSittingOLevelResult.Id <= 0)
                    {
                        viewModel.SecondSittingOLevelResult.ApplicationForm = viewModel.ApplicationForm;
                        viewModel.SecondSittingOLevelResult.Person = viewModel.ApplicationForm.Person;
                        viewModel.SecondSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 2 };
                    }

                    ModifyOlevelResult(viewModel.FirstSittingOLevelResult, viewModel.FirstSittingOLevelResultDetails);
                    ModifyOlevelResult(viewModel.SecondSittingOLevelResult, viewModel.SecondSittingOLevelResultDetails);



                    //get applicant's applied course
                    AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    ApplicantLogic applicantLogic = new ApplicantLogic();
                    AppliedCourse appliedCourse = appliedCourseLogic.GetBy(viewModel.ApplicationForm.Person);

                    //Set department to admitted department since it might vary
                    AdmissionList admissionList = new AdmissionList();
                    admissionList = admissionListLogic.GetBy(viewModel.ApplicationForm.Person);
                    if (admissionList != null)
                    {
                        appliedCourse.Department = admissionList.Deprtment;

                        if (appliedCourse == null)
                        {
                            SetMessage("Your O-Level was successfully verified, but could not be cleared because no Applied Course was not found for your application", Message.Category.Error);
                            return RedirectToAction("CorrectOlevel");
                        }

                        //set reject reason if exist
                        ApplicantStatus.Status newStatus;
                        AdmissionCriteriaLogic admissionCriteriaLogic = new AdmissionCriteriaLogic();
                        string rejectReason = admissionCriteriaLogic.EvaluateApplication(appliedCourse);
                        if (string.IsNullOrWhiteSpace(rejectReason))
                        {

                            Abundance_Nk.Model.Model.Applicant applicant = new Model.Model.Applicant();
                            applicant = applicantLogic.GetModelBy(p => p.Person_Id == appliedCourse.Person.Id);
                            newStatus = (ApplicantStatus.Status)applicant.Status.Id;
                            //newStatus = ApplicantStatus.Status.ClearedAndAccepted;
                            viewModel.ApplicationForm.Rejected = false;
                            viewModel.ApplicationForm.Release = false;
                        }
                        else
                        {
                            newStatus = ApplicantStatus.Status.ClearedAndRejected;
                            viewModel.ApplicationForm.Rejected = true;
                            viewModel.ApplicationForm.Release = true;
                        }

                        viewModel.ApplicationForm.RejectReason = rejectReason;
                        ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                        applicationFormLogic.SetRejectReason(viewModel.ApplicationForm);


                        //set applicant new status
                        //ApplicantLogic applicantLogic = new ApplicantLogic();
                        applicantLogic.UpdateStatus(viewModel.ApplicationForm, newStatus);


                        //save clearance metadata
                        ApplicantClearance applicantClearance = new ApplicantClearance();
                        ApplicantClearanceLogic applicantClearanceLogic = new ApplicantClearanceLogic();
                        applicantClearance = applicantClearanceLogic.GetBy(viewModel.ApplicationForm);
                        if (applicantClearance == null)
                        {
                            applicantClearance = new ApplicantClearance();
                            applicantClearance.ApplicationForm = viewModel.ApplicationForm;
                            applicantClearance.Cleared = string.IsNullOrWhiteSpace(viewModel.ApplicationForm.RejectReason) ? true : false;
                            applicantClearance.DateCleared = DateTime.Now;
                            applicantClearanceLogic.Create(applicantClearance);
                        }
                        else
                        {
                            applicantClearance.Cleared = string.IsNullOrWhiteSpace(viewModel.ApplicationForm.RejectReason) ? true : false;
                            applicantClearance.DateCleared = DateTime.Now;
                            applicantClearanceLogic.Modify(applicantClearance);
                        }
                    }
                    transaction.Complete();
                }
            }


            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);

            }

            SetMessage("O-Level result updated", Message.Category.Information);
            return RedirectToAction("CorrectOlevel");
        }

        private void ModifyOlevelResult(OLevelResult oLevelResult, List<OLevelResultDetail> oLevelResultDetails)
        {
            try
            {
                OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();

                OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
                if (oLevelResult != null && oLevelResult.ExamNumber != null && oLevelResult.Type != null && oLevelResult.ExamYear > 0)
                {
                    if (oLevelResult != null && oLevelResult.Id > 0)
                    {

                        oLevelResultDetailLogic.DeleteBy(oLevelResult);
                        oLevelResultLogic.Modify(oLevelResult);
                    }
                    else
                    {

                        OLevelResult newOLevelResult = oLevelResultLogic.Create(oLevelResult);
                        oLevelResult.Id = newOLevelResult.Id;
                    }

                    if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                    {
                        List<OLevelResultDetail> olevelResultDetails = oLevelResultDetails.Where(m => m.Grade != null && m.Grade.Id > 0 && m.Subject != null && m.Subject.Id > 0).ToList();
                        foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
                        {
                            oLevelResultDetail.Header = oLevelResult;
                            oLevelResultDetailLogic.Create(oLevelResultDetail);
                        }

                        //oLevelResultDetailLogic.Create(olevelResultDetails);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool InvalidOlevelSubjectOrGrade(List<OLevelResultDetail> oLevelResultDetails, List<OLevelSubject> subjects, List<OLevelGrade> grades, string sitting)
        {
            try
            {
                List<OLevelResultDetail> subjectList = null;
                if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                {
                    subjectList = oLevelResultDetails.Where(r => r.Subject.Id > 0 || r.Grade.Id > 0).ToList();
                }

                if (subjectList == null) { return false; }
                foreach (OLevelResultDetail oLevelResultDetail in subjectList)
                {
                    OLevelSubject subject = subjects.Where(s => s.Id == oLevelResultDetail.Subject.Id).SingleOrDefault();
                    OLevelGrade grade = grades.Where(g => g.Id == oLevelResultDetail.Grade.Id).SingleOrDefault();

                    List<OLevelResultDetail> results = subjectList.Where(o => o.Subject.Id == oLevelResultDetail.Subject.Id).ToList();
                    if (results != null && results.Count > 1)
                    {
                        SetMessage("Duplicate " + subject.Name.ToUpper() + " Subject detected in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }
                    else if (oLevelResultDetail.Subject.Id > 0 && oLevelResultDetail.Grade.Id <= 0)
                    {
                        SetMessage("No Grade specified for Subject " + subject.Name.ToUpper() + " in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }
                    else if (oLevelResultDetail.Subject.Id <= 0 && oLevelResultDetail.Grade.Id > 0)
                    {
                        SetMessage("No Subject specified for Grade" + grade.Name.ToUpper() + " in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }

                    //else if (oLevelResultDetail.Grade.Id > 0 && oLevelResultDetail.Subject.Id <= 0)
                    //{
                    //    SetMessage("No Grade specified for " + subject.Name.ToUpper() + " for " + sitting + "! Please modify.", Message.Category.Error);
                    //    return true;
                    //}
                    //else if (oLevelResultDetail.Subject.Id <= 0 && oLevelResultDetail.Grade.Id > 0)
                    //{
                    //    SetMessage("No Subject specified for " + grade.Name.ToUpper() + " for " + sitting + "! Please modify.", Message.Category.Error);
                    //    return true;
                    //}
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool InvalidNumberOfOlevelSubject(List<OLevelResultDetail> firstSittingResultDetails, List<OLevelResultDetail> secondSittingResultDetails)
        {
            const int FIVE = 5;

            try
            {
                int totalNoOfSubjects = 0;

                List<OLevelResultDetail> firstSittingSubjectList = null;
                List<OLevelResultDetail> secondSittingSubjectList = null;

                if (firstSittingResultDetails != null && firstSittingResultDetails.Count > 0)
                {
                    firstSittingSubjectList = firstSittingResultDetails.Where(r => r.Subject.Id > 0).ToList();
                    if (firstSittingSubjectList != null)
                    {
                        totalNoOfSubjects += firstSittingSubjectList.Count;
                    }
                }

                if (secondSittingResultDetails != null && secondSittingResultDetails.Count > 0)
                {
                    secondSittingSubjectList = secondSittingResultDetails.Where(r => r.Subject.Id > 0).ToList();
                    if (secondSittingSubjectList != null)
                    {
                        totalNoOfSubjects += secondSittingSubjectList.Count;
                    }
                }

                if (totalNoOfSubjects == 0)
                {
                    SetMessage("No O-Level Result Details found for both sittings!", Message.Category.Error);
                    return true;
                }
                else if (totalNoOfSubjects < FIVE)
                {
                    SetMessage("O-Level Result cannot be less than " + FIVE + " subjects in both sittings!", Message.Category.Error);
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult UpdatePassport()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdatePassport(SupportViewModel viewmodel)
        {
            try
            {
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                ApplicationForm applicationForm = new ApplicationForm();
                applicationForm = applicationFormLogic.GetModelBy(p => p.Application_Form_Number == viewmodel.ApplicationForm.Number);
                viewmodel.Person = applicationForm.Person;

            }
            catch (Exception ex)
            {
                SetMessage("Error occured! " + ex.Message, Message.Category.Error);

            }

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult SaveStudentPassport(SupportViewModel viewmodel)
        {
            try
            {
                PersonLogic personLogic = new PersonLogic();
                Person person = personLogic.GetModelBy(p => p.Person_Id == viewmodel.Person.Id);
                if (viewmodel.File == null)
                {
                    SetMessage("No file was selected! ", Message.Category.Error);
                    return RedirectToAction("UpdatePassport");
                }

                string extension = Path.GetExtension(viewmodel.File.FileName).ToLower();

                string invalidImage = InvalidFile(viewmodel.File.ContentLength, extension);
                if (string.IsNullOrEmpty(invalidImage))
                {
                    string imageUrl = getImageURL(viewmodel, person);
                    person.ImageFileUrl = imageUrl;
                    personLogic.Modify(person);
                    SetMessage("Correction Successful ", Message.Category.Information);
                }
                else
                {
                    SetMessage("Operation Failed, " + invalidImage, Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Operation Failed, " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("UpdatePassport");
        }
        private string getImageURL(SupportViewModel viewModel, Person person)
        {
            if (viewModel.File != null)
            {
                //Saving image to a folder and saving its url to db
                string[] allowedFIleExtensions = new[] { ".jpg", ".png", ".jpeg", ".Jpeg" };

                string filenameWithExtension = Path.GetFileName(viewModel.File.FileName); // getting filename
                string extension = Path.GetExtension(viewModel.File.FileName).ToLower(); // getting only the extension
                if (allowedFIleExtensions.Contains(extension)) // check extension type
                {

                    string FileNameWithoutExtension = Path.GetFileNameWithoutExtension(filenameWithExtension); //retireves filename without extension
                    FileNameWithoutExtension = person.FullName;
                    string FileNameInServer = FileNameWithoutExtension + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_" + extension; // add reg number after underscore to make the filename unique for each person
                    string pathToFileInServer = Path.Combine(Server.MapPath("/Content/Student/"), FileNameInServer);
                    string passportUrl = "/Content/Student/" + FileNameInServer;
                    viewModel.File.SaveAs(pathToFileInServer);
                    return passportUrl;
                }
                else
                {
                    return person.ImageFileUrl;
                }
            }
            else
            {
                return person.ImageFileUrl;
            }

        }
        private string InvalidFile(decimal uploadedFileSize, string fileExtension)
        {
            try
            {
                string message = null;
                decimal oneKiloByte = 1024;
                decimal maximumFileSize = 50 * oneKiloByte;

                decimal actualFileSizeToUpload = Math.Round(uploadedFileSize / oneKiloByte, 1);
                if (InvalidFileType(fileExtension))
                {
                    message = "File type '" + fileExtension + "' is invalid! File type must be any of the following: .jpg, .jpeg, .png or .jif ";
                }
                else if (actualFileSizeToUpload > (maximumFileSize / oneKiloByte))
                {
                    message = "Your file size of " + actualFileSizeToUpload.ToString("0.#") + " Kb is too large, maximum allowed size is " + (maximumFileSize / oneKiloByte) + " Kb";
                }

                return message;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool InvalidFileType(string extension)
        {
            extension = extension.ToLower();
            switch (extension)
            {
                case ".jpg":
                    return false;
                case ".png":
                    return false;
                case ".gif":
                    return false;
                case ".jpeg":
                    return false;
                default:
                    return true;
            }
        }

        public ActionResult EditApplicantDetailByName()
        {
            SupportViewModel viewModel = null;
            try
            {
                viewModel = new SupportViewModel();
                ViewBag.Programmes = viewModel.ProgrammeSelectListItem;
                ViewBag.Sessions = viewModel.SessionSelectListItem;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditApplicantDetailByName(SupportViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                    List<AppliedCourse> masterList = new List<AppliedCourse>();

                    //List<AppliedCourse> appliedCourseList = appliedCourseLogic.GetModelsBy(a => a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id);

                        string[] nameSplit = viewModel.Person.FirstName.Split(' ');
                        string lastName = "";
                        string firstName = "";
                        string otherNames = "";

                        if (nameSplit.Count() == 1)
                        {
                            lastName = nameSplit[0].ToLower().Trim();
                        }
                        else if (nameSplit.Count() == 2)
                        {
                            lastName = nameSplit[0].ToLower().Trim();
                            firstName = nameSplit[1].ToLower().Trim();
                        }
                        else if (nameSplit.Count() == 3)
                        {
                            lastName = nameSplit[0].ToLower().Trim();
                            firstName = nameSplit[1].ToLower().Trim();
                            otherNames = nameSplit[2].ToLower().Trim();
                        }
                        else if (nameSplit.Count() >= 4)
                        {
                            lastName = nameSplit[0].ToLower().Trim();
                            firstName = nameSplit[1].ToLower().Trim();
                            otherNames = nameSplit[2].ToLower().Trim() + " " + nameSplit[3].ToLower().Trim();
                        }

                        if (nameSplit.Length == 1)
                        {
                            masterList = appliedCourseLogic.GetModelsBy(a => (a.PERSON.Last_Name + a.PERSON.First_Name + a.PERSON.Other_Name).ToLower().Trim().Contains(lastName) && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                        }

                        if (nameSplit.Length == 2)
                        {
                            List<AppliedCourse> totalSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == lastName && a.PERSON.First_Name.ToLower().Trim() == firstName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (totalSearch.Count > 0)
                            {
                                masterList.AddRange(totalSearch);
                            }
                            List<AppliedCourse> totalSearchx = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == firstName && a.PERSON.First_Name.ToLower().Trim() == lastName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (totalSearchx.Count > 0)
                            {
                                masterList.AddRange(totalSearchx);
                            }

                            List<AppliedCourse> lastNameSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == lastName || a.PERSON.First_Name.ToLower().Trim() == lastName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (lastNameSearch.Count > 0)
                            {
                                masterList.AddRange(lastNameSearch);
                            }

                            List<AppliedCourse> firstNameSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == firstName || a.PERSON.First_Name.ToLower().Trim() == firstName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (firstNameSearch.Count > 0)
                            {
                                masterList.AddRange(firstNameSearch);
                            }
                        }

                        if (nameSplit.Length >= 3)
                        {
                            List<AppliedCourse> totalSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == lastName && a.PERSON.First_Name.ToLower().Trim() == firstName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (totalSearch.Count > 0)
                            {
                                masterList.AddRange(totalSearch);
                            }

                            List<AppliedCourse> totalSearchx = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == firstName && a.PERSON.First_Name.ToLower().Trim() == lastName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (totalSearchx.Count > 0)
                            {
                                masterList.AddRange(totalSearchx);
                            }

                            List<AppliedCourse> lastNameSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == lastName || a.PERSON.First_Name.ToLower().Trim() == lastName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (lastNameSearch.Count > 0)
                            {
                                masterList.AddRange(lastNameSearch);
                            }

                            List<AppliedCourse> firstNameSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == firstName || a.PERSON.First_Name.ToLower().Trim() == firstName && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (firstNameSearch.Count > 0)
                            {
                                masterList.AddRange(firstNameSearch);
                            }

                            List<AppliedCourse> perfectSearch = appliedCourseLogic.GetModelsBy(a => a.PERSON.Last_Name.ToLower().Trim() == lastName && a.PERSON.First_Name.ToLower().Trim() == firstName && a.PERSON.Other_Name.ToLower().Trim() == otherNames && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.PAYMENT.Session_Id == viewModel.Session.Id).ToList();
                            if (totalSearch.Count > 0)
                            {
                                masterList.AddRange(perfectSearch);
                            }
                        }

                        viewModel.AppliedCourseList = masterList;
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Programmes = viewModel.ProgrammeSelectListItem;
            ViewBag.Sessions = viewModel.SessionSelectListItem;
            return View(viewModel);
        }
        public ActionResult EditApplicantByName(long personId)
        {
            try
            {
                AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                Payment payment = new Payment();
                        PaymentLogic paymentLogic = new PaymentLogic();

                AppliedCourse appliedCourse = appliedCourseLogic.GetModelsBy(a => a.Person_Id == personId).LastOrDefault();

                if (appliedCourse != null)
                {
                    SupportViewModel vModel = new SupportViewModel();
                    vModel.InvoiceNumber = appliedCourse.ApplicationForm.Payment.InvoiceNumber;

                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == vModel.InvoiceNumber);

                    if (payment != null && payment.Id > 0)
                    {
                        AppliedCourse appliedcourse = new AppliedCourse();
                        ApplicantJambDetail applicantjambdetail = new ApplicantJambDetail();
                        ApplicantJambDetailLogic applicantjambdetailLogic = new ApplicantJambDetailLogic();

                        appliedcourse = appliedCourseLogic.GetModelBy(n => n.Person_Id == payment.Person.Id);
                        if (appliedcourse != null && appliedcourse.Department.Id > 0)
                        {
                            applicantjambdetail =
                                applicantjambdetailLogic.GetModelBy(x => x.Person_Id == payment.Person.Id);
                            if (applicantjambdetail != null && applicantjambdetail.Person.Id > 0)
                            {
                                vModel.ApplicantJambDetail = applicantjambdetail;
                            }


                            vModel.AppliedCourse = appliedcourse;
                            vModel.DepartmentSelectListItem =
                                Utility.PopulateDepartmentSelectListItem(appliedcourse.Programme);
                            vModel.ProgrammeSelectListItem = Utility.PopulateAllProgrammeSelectListItem();

                            if (vModel.DepartmentSelectListItem != null)
                            {
                                ViewBag.DepartmentId = new SelectList(vModel.DepartmentSelectListItem, VALUE, TEXT,
                                    appliedcourse.Department.Id);

                            }
                            else
                            {
                                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

                            }
                            if (vModel.ProgrammeSelectListItem != null)
                            {

                                ViewBag.ProgrammeId = new SelectList(vModel.ProgrammeSelectListItem, VALUE, TEXT,
                                    appliedcourse.Programme.Id);

                            }
                            else
                            {
                                ViewBag.ProgrammeId = new SelectList(new List<Programme>(), ID, NAME);

                            }

                            ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), VALUE,
                                TEXT, payment.FeeType.Id);

                            vModel.Payment = payment;
                            vModel.Person = payment.Person;
                            vModel.InvoiceNumber = payment.InvoiceNumber;
                            TempData["SupportViewModel"] = viewmodel;
                            return View("Index", vModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("EditApplicantDetailByName");
        }
        public ActionResult UpdatePayment()
        {
            SupportViewModel viewmodel = new SupportViewModel();
            ViewBag.FeeTypeId = viewmodel.FeeTypeSelectList;
            ViewBag.SessionId = viewmodel.SessionSelectListItem;
            return View();
        }

        [HttpPost]
        public ActionResult UpdatePayment(SupportViewModel viewmodel)
        {
            try
            {
                PaymentEtranzact paymentEtranzact = new PaymentEtranzact();
                PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
                PaymentTerminal paymentTerminal = new PaymentTerminal();
                PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == viewmodel.FeeType.Id && p.Session_Id == viewmodel.Session.Id);
                paymentEtranzact = paymentEtranzactLogic.RetrieveEtranzactWebServicePinDetails(viewmodel.PaymentEtranzact.ConfirmationNo, paymentTerminal);
                viewmodel.PaymentEtranzact = paymentEtranzact;
                viewmodel.PaymentEtranzact.Terminal = paymentTerminal;

            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.FeeTypeId = viewmodel.FeeTypeSelectList;
            ViewBag.SessionId = viewmodel.SessionSelectListItem;
            ViewBag.PaymentEtranzactId = viewmodel.PaymentEtranzactTypeSelectList;
            ViewBag.PaymentTerminalId = viewmodel.PaymentTerminalSelectList;

            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult SavePayment(SupportViewModel viewmodel)
        {
            try
            {
                PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
                PaymentEtranzact paymetEtranzact = new PaymentEtranzact();
                paymetEtranzact = viewmodel.PaymentEtranzact;
                PaymentLogic paymentLogic = new PaymentLogic();
                Payment payment = new Payment();
                payment = paymentLogic.GetModelBy(p => p.Invoice_Number == viewmodel.PaymentEtranzact.CustomerID);
                if (payment != null)
                {
                    PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                    PaymentTerminal paymentTerminal = new PaymentTerminal();
                    if (viewmodel.PaymentEtranzact.Terminal != null && viewmodel.PaymentEtranzact.Terminal.Id > 0)
                    {
                        paymentTerminal = viewmodel.PaymentEtranzact.Terminal;
                    }
                    else
                    {
                        paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == payment.FeeType.Id);
                    }

                    PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                    PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                    if (viewmodel.PaymentEtranzact.EtranzactType != null && viewmodel.PaymentEtranzact.EtranzactType.Id > 0)
                    {
                        paymentEtranzactType = viewmodel.PaymentEtranzact.EtranzactType;
                    }
                    else
                    {
                        paymentEtranzactType = paymentEtranzactTypeLogic.GetModelsBy(p => p.Fee_Type_Id == payment.FeeType.Id).LastOrDefault();
                    }

                    paymetEtranzact.EtranzactType = paymentEtranzactType;
                    paymetEtranzact.Terminal = paymentTerminal;
                    if (payment != null)
                    {
                        OnlinePayment onlinePayment = new OnlinePayment();
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        onlinePayment = onlinePaymentLogic.GetModelBy(c => c.PAYMENT_CHANNEL.Payment_Channnel_Id == (int)PaymentChannel.Channels.Etranzact && c.Payment_Id == payment.Id);
                        paymetEtranzact.Payment = onlinePayment;
                    }

                    if (viewmodel.Student.MatricNumber != null)
                    {
                        StudentLogic studentLogic = new StudentLogic();
                        viewmodel.Student = studentLogic.GetModelsBy(s => s.Matric_Number.Trim() == viewmodel.Student.MatricNumber.Trim()).LastOrDefault();

                        if (viewmodel.Student != null)
                        {
                            payment.Person = new Person() { Id = viewmodel.Student.Id };
                            paymentLogic.Modify(payment);
                            paymetEtranzact.CustomerName = viewmodel.Student.FullName;
                        }
                    }

                    if (paymentEtranzactLogic.GetBy(paymetEtranzact.ConfirmationNo) == null)
                    {
                        paymetEtranzact = paymentEtranzactLogic.Create(paymetEtranzact);
                        SetMessage("Payment Updated Successfully! ", Message.Category.Information);
                    }
                    else
                    {
                        if (paymentEtranzactLogic.Modify(paymetEtranzact))
                        {
                            SetMessage("Payment already exists and was modified succesfully! ", Message.Category.Information);
                        }
                        else
                        {
                            SetMessage("Payment already exists and was not modified! ", Message.Category.Information);
                        }

                    }

                }


            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);

            }
            ViewBag.FeeTypeId = viewmodel.FeeTypeSelectList;
            ViewBag.SessionId = viewmodel.SessionSelectListItem;
            ViewBag.PaymentEtranzactId = viewmodel.PaymentEtranzactTypeSelectList;
            ViewBag.PaymentTerminalId = viewmodel.PaymentTerminalSelectList;
            return View("UpdatePayment", viewmodel);
        }
        public ActionResult Rank()
        {
            SupportViewModel viewModel = new SupportViewModel();
            try
            {
                ViewBag.Sessions = viewModel.SessionSelectListItem;
                ViewBag.Programmes = viewModel.ProgrammeSelectListItem;
                ViewBag.Departments = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Rank(SupportViewModel viewModel)
        {
            try
            {
                AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();

                if (viewModel.Programme != null && viewModel.Programme.Id > 0 && viewModel.Department != null && viewModel.Department.Id > 0 && viewModel.Session != null && viewModel.Session.Id > 0)
                {
                    List<AppliedCourse> appliedCourses = appliedCourseLogic.GetModelsBy(a => a.Department_Id == viewModel.Department.Id && a.Programme_Id == viewModel.Programme.Id && a.APPLICATION_FORM.APPLICATION_FORM_SETTING.Session_Id == viewModel.Session.Id);

                    if (appliedCourses != null && appliedCourses.Count > 0)
                    {
                        for (int i = 0; i < appliedCourses.Count; i++)
                        {
                            AppliedCourse appliedCourse = appliedCourses[i];

                            RankingDataLogic rankingDataLogic = new RankingDataLogic();

                            string rejectReason = rankingDataLogic.RankStudent(appliedCourse);
                        }

                        SetMessage("Operation Successful! ", Message.Category.Information);
                    }
                }
                else
                {
                    SetMessage("One of the fields was not selected. Check and try again.", Message.Category.Error);

                    ViewBag.Sessions = viewModel.SessionSelectListItem;
                    ViewBag.Programmes = viewModel.ProgrammeSelectListItem;

                    if (viewModel.Programme != null && viewModel.Programme.Id > 0 && viewModel.Department != null && viewModel.Department.Id > 0)
                    {
                        DepartmentLogic departmentLogic = new DepartmentLogic();
                        List<Department> departments = departmentLogic.GetBy(viewModel.Programme);

                        ViewBag.Departments = new SelectList(departments, ID, NAME, viewModel.Department.Id);
                    }
                    else
                    {
                        ViewBag.Departments = new SelectList(new List<Department>(), ID, NAME);
                    }
                    
                    return View(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);

            }

            ViewBag.Sessions = viewModel.SessionSelectListItem;
            ViewBag.Programmes = viewModel.ProgrammeSelectListItem;

            if (viewModel.Programme != null && viewModel.Programme.Id > 0 && viewModel.Department != null && viewModel.Department.Id > 0)
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(viewModel.Programme);

                ViewBag.Departments = new SelectList(departments, ID, NAME, viewModel.Department.Id);
            }
            else
            {
                ViewBag.Departments = new SelectList(new List<Department>(), ID, NAME);
            }

            return View(viewModel);
        }
    }
}
