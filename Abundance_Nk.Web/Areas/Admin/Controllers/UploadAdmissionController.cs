﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;
using System.IO;
using System.Data.OleDb;
using System.Web.UI.WebControls;

namespace Abundance_Nk.Web.Areas.Admin.Views
{
    public class UploadAdmissionController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string VALUE = "Value";
        private const string TEXT = "Text";
        private Abundance_Nk_NekedeEntities db = new Abundance_Nk_NekedeEntities();
        private UploadAdmissionViewModel viewmodel;
        //
        // GET: /Admin/UploadAdmission/
        public ActionResult UploadAdmission()
        {

            try
            {


                viewmodel = new UploadAdmissionViewModel();
                viewmodel.ProgrammeSelectListItem = Utility.PopulateProgrammeSelectListItem();
                viewmodel.SessionSelectListItem = Utility.PopulateSessionSelectListItem();
                viewmodel.AdmissionListTypeSelectListItem = Utility.PopulateAdmissionListTypeSelectListItem();
                ViewBag.ProgrammeId = viewmodel.ProgrammeSelectListItem;
                ViewBag.SessionId = viewmodel.SessionSelectListItem;
                ViewBag.AdmissionListTypeId = viewmodel.AdmissionListTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }

        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };

                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult UploadAdmission(UploadAdmissionViewModel vmodel)
        {
            try
            {
                KeepApplicationFormInvoiceGenerationDropDownState(vmodel);

                List<AppliedCourse> applicants = new List<AppliedCourse>();
                string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                string savedFileName = "";
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        FileInfo fileInfo = new FileInfo(hpf.FileName);
                        string fileExtension = fileInfo.Extension;
                        string newFile = "Admission" + "__";
                        string newFileName = newFile + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + fileExtension;

                        savedFileName = Path.Combine(pathForSaving,newFileName);
                        hpf.SaveAs(savedFileName);
                    }
                    DataSet dsAdmissionList = ReadExcelFile(savedFileName);
                    if (dsAdmissionList != null && dsAdmissionList.Tables[0].Rows.Count > 0)
                    {
                        string Application_Number = "";
                        AppliedCourse appliedCourse = new AppliedCourse();
                        AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                        for (int i = 0; i < dsAdmissionList.Tables[0].Rows.Count; i++)
                        {
                            Application_Number = dsAdmissionList.Tables[0].Rows[i][0].ToString();
                            appliedCourse = appliedCourseLogic.GetModelBy(m => m.APPLICATION_FORM.Application_Form_Number == Application_Number);
                            if (appliedCourse != null)
                            {
                                applicants.Add(appliedCourse);
                            }
                        }

                        vmodel.AppliedCourseList = applicants;
                        TempData["UploadAdmissionViewModel"] = vmodel;
                        return View(vmodel);

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View();


        }

        [HttpPost]
        public ActionResult SaveAdmissionList(UploadAdmissionViewModel vmodel)
        {
            try
            {
                int UploadCount = 0;
                string operation = "INSERT";
                string action = "UPLOADING OF ADMISSION LIST";
                string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";

                vmodel = (UploadAdmissionViewModel)TempData["UploadAdmissionViewModel"];
                if (vmodel.AppliedCourseList != null && vmodel.AppliedCourseList.Count > 0)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        AdmissionListBatch batch = new AdmissionListBatch();
                        AdmissionListBatchLogic batchLogic = new AdmissionListBatchLogic();
                        AdmissionListType AdmissionType = new AdmissionListType();
                        AdmissionListAudit AdmissionListAudit = new Model.Model.AdmissionListAudit();
                        UserLogic loggeduser = new UserLogic();
                        AdmissionType = vmodel.AdmissionListType;
                        batch.DateUploaded = DateTime.Now;
                        batch.IUploadedFilePath = "NAN";
                        batch.Type = AdmissionType;
                        batch = batchLogic.Create(batch);

                        AdmissionListAudit.Action = action;
                        AdmissionListAudit.Client = client;
                        AdmissionListAudit.Operation = operation;
                        AdmissionListAudit.Time = DateTime.Now;
                        AdmissionListAudit.User = loggeduser.GetModelBy(u => u.User_Name == User.Identity.Name);
                        UploadCount = 0;
                        for (int i = 0; i < vmodel.AppliedCourseList.Count; i++)
                        {
                            
                            AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                            if (!admissionListLogic.AdmissionExist(vmodel.AppliedCourseList[i].ApplicationForm))
                            {
                                AdmissionList admissionlist = new AdmissionList();
                                admissionlist.Form = vmodel.AppliedCourseList[i].ApplicationForm;
                                admissionlist.Deprtment = vmodel.AdmissionListDetail.Deprtment;
                                admissionlist.Programme = vmodel.AdmissionListDetail.Form.ProgrammeFee.Programme;
                                admissionlist.Session = vmodel.CurrentSession;
                                admissionlist.Activated = true;

                                admissionListLogic.Create(admissionlist, batch, AdmissionListAudit);
                                UploadCount++;
                            }
                            else
                            {
                                AdmissionList list = admissionListLogic.GetBy(vmodel.AppliedCourseList[i].ApplicationForm.Id);
                                if (list != null)
                                {
                                    list.Deprtment = vmodel.AdmissionListDetail.Deprtment;
                                    list.Programme = vmodel.AdmissionListDetail.Form.ProgrammeFee.Programme;
                                    admissionListLogic.Update(list, AdmissionListAudit);
                                    UploadCount++;
                                }
                            }

                            
                           

                        }
                        transaction.Complete();
                    }

                      SetMessage( UploadCount.ToString() + " out of "+ vmodel.AppliedCourseList.Count.ToString() + " names List was uploaded successfully", Message.Category.Information);
                  
                    return RedirectToAction("UploadAdmission");
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            SetMessage("List was not uploaded successfully, Please check for duplicates and try again", Message.Category.Information);
                  
            return View();
        }

        public ActionResult ViewAdmission()
        {

            try
            {

                viewmodel = new UploadAdmissionViewModel();
                viewmodel.ProgrammeSelectListItem = Utility.PopulateProgrammeSelectListItem();
                viewmodel.SessionSelectListItem = Utility.PopulateSessionSelectListItem();
                viewmodel.AdmissionListTypeSelectListItem = Utility.PopulateAdmissionListTypeSelectListItem();
                ViewBag.ProgrammeId = viewmodel.ProgrammeSelectListItem;
                ViewBag.SessionId = viewmodel.SessionSelectListItem;
                ViewBag.AdmissionListTypeId = viewmodel.AdmissionListTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
       
        [HttpPost]
        public ActionResult ViewAdmission(UploadAdmissionViewModel vmodel)
        {

            try
            {
                KeepApplicationFormInvoiceGenerationDropDownState(vmodel);
                if (vmodel.AdmissionListDetail.Deprtment != null && vmodel.AdmissionListDetail.Form.ProgrammeFee.Programme != null && vmodel.CurrentSession != null && vmodel.AdmissionListType != null)
                {
                    List<AdmissionList> list = new List<AdmissionList>();
                    AdmissionListLogic ListLogic = new AdmissionListLogic();
                    list = ListLogic.GetModelsBy(a => a.Department_Id == vmodel.AdmissionListDetail.Deprtment.Id && a.APPLICATION_FORM.APPLICATION_PROGRAMME_FEE.PROGRAMME.Programme_Id == vmodel.AdmissionListDetail.Form.ProgrammeFee.Programme.Id && a.APPLICATION_FORM.APPLICATION_FORM_SETTING.SESSION.Session_Id == vmodel.CurrentSession.Id && a.ADMISSION_LIST_BATCH.ADMISSION_LIST_TYPE.Admission_List_Type_Id == vmodel.AdmissionListType.Id);
                    if (list != null)
                    {
                        vmodel.AdmissionList = list;
                        return View(vmodel);
                    }
                }
               
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }

        private string InvalidFile(decimal uploadedFileSize, string fileExtension)
        {
            try
            {
                string message = null;
                decimal oneKiloByte = 1024;
                decimal maximumFileSize = 50 * oneKiloByte;

                decimal actualFileSizeToUpload = Math.Round(uploadedFileSize / oneKiloByte, 1);
                if (InvalidFileType(fileExtension))
                {
                    message = "File type '" + fileExtension + "' is invalid! File type must be any of the following: .jpg, .jpeg, .png or .jif ";
                }
                else if (actualFileSizeToUpload > (maximumFileSize / oneKiloByte))
                {
                    message = "Your file size of " + actualFileSizeToUpload.ToString("0.#") + " Kb is too large, maximum allowed size is " + (maximumFileSize / oneKiloByte) + " Kb";
                }

                return message;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool InvalidFileType(string extension)
        {
            extension = extension.ToLower();
            switch (extension)
            {
                case ".xls":
                    return false;
                case ".xlsx":
                    return false;
                default:
                    return true;
            }
        }

        private void DeleteFileIfExist(string folderPath, string fileName)
        {
            try
            {
                string wildCard = fileName + "*.*";
                IEnumerable<string> files = Directory.EnumerateFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

                if (files != null && files.Count() > 0)
                {
                    foreach (string file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CreateFolderIfNeeded(string path)
        {
            try
            {
                bool result = true;
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch (Exception)
                    {
                        /*TODO: You must process this exception.*/
                        result = false;
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" +"Extended Properties=Excel 8.0;";
                var connection = new OleDbConnection(xConnStr);
                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,null);
                foreach(DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'","");
                    var command = new OleDbCommand("Select * FROM [" + sheetName + "]",connection);
                    // Create DbDataReader to Data Worksheet

                    var MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    var ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        public static DataSet ReadExcelFile(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);
                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$]", connection);
                connection.Open();
                // Create DbDataReader to Data Worksheet

                OleDbDataAdapter MyData = new OleDbDataAdapter();
                MyData.SelectCommand = command;
                DataSet ds = new DataSet();
                ds.Clear();
                MyData.Fill(ds);
                connection.Close();

                Result = ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;
        }

        private void KeepApplicationFormInvoiceGenerationDropDownState(UploadAdmissionViewModel viewModel)
        {
            try
            {


                if (viewModel.AdmissionListDetail.Form.ProgrammeFee.Programme.Id != null && viewModel.AdmissionListDetail.Form.ProgrammeFee.Programme.Id > 0)
                {
                    viewModel.DepartmentSelectListItem = Utility.PopulateDepartmentSelectListItem(viewModel.AdmissionListDetail.Form.ProgrammeFee.Programme);
                    ViewBag.ProgrammeId = new SelectList(viewModel.ProgrammeSelectListItem, VALUE, TEXT, viewModel.AdmissionListDetail.Form.ProgrammeFee.Programme.Id);
                    ViewBag.SessionId = Utility.PopulateSessionSelectListItem();
                    ViewBag.AdmissionListTypeId = Utility.PopulateAdmissionListTypeSelectListItem();

                    if (viewModel.AdmissionListDetail.Deprtment != null && viewModel.AdmissionListDetail.Deprtment.Id > 0)
                    {

                        ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, VALUE, TEXT, viewModel.AdmissionListDetail.Deprtment.Id);
                       
                    }
                    else
                    {
                        ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, VALUE, TEXT);
                    }
                }
                else
                {
                    ViewBag.ProgrammeId = new SelectList(viewModel.ProgrammeSelectListItem, VALUE, TEXT);
                    ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
                    ViewBag.SessionId = new SelectList(viewmodel.SessionSelectListItem, VALUE, TEXT);
                    ViewBag.AdmissionListTypeId = viewmodel.AdmissionListTypeSelectListItem;

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

       
        public ActionResult SearchAdmittedStudents()
        {

            return View();
        }
       
        [HttpPost]
        public ActionResult SearchAdmittedStudents(UploadAdmissionViewModel vModel)
        {
            try
            {

                List<AdmissionList> admissionList = new List<AdmissionList>();
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                if (vModel.SearchString != null)
                {
                    string search = vModel.SearchString.ToString();
                    admissionList = admissionListLogic.GetModelsBy(p => p.APPLICATION_FORM.Exam_Number.Contains(search) || p.APPLICATION_FORM.Application_Form_Number.Contains(search));
                    if (admissionList.Count > 0)
                    {
                        vModel.AdmissionList = admissionList;
                    }
                    else
                    {
                        TempData["Action"] = "Student does not have admission";
                        return RedirectToAction("SearchAdmittedStudents");
                    }

                }

                return View(vModel);
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult EditAdmittedStudentDepartment(long id)
        {
            try
            {
                AdmissionList admissionList = new AdmissionList();
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                admissionList = admissionListLogic.GetModelBy(p => p.Admission_List_Id == id);
                if (admissionList == null)
                {
                    TempData["Action"] = "Student does not have admission";
                    return RedirectToAction("SearchAdmittedStudents");
                }
                UploadAdmissionViewModel vModel = new UploadAdmissionViewModel();
                vModel.AdmissionListDetail = admissionList;
                ViewBag.ProgrammeId = vModel.ProgrammeSelectListItem;
                //ViewBag.Departments = new SelectList(new List<Department>(), Utility.ID, Utility.NAME);
                KeepApplicationFormInvoiceGenerationDropDownState(vModel);
                return View(vModel);
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpPost]
        public ActionResult EditAdmittedStudentDepartment(UploadAdmissionViewModel vModel)
        {
            try
            {
                if (vModel.AdmissionListDetail.Id > 0)
                {
                    AdmissionList admissionList = new AdmissionList();
                    AdmissionList admissionListOld = new AdmissionList();
                    ProgrammeLogic programmeLogic = new ProgrammeLogic();
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    admissionList = admissionListLogic.GetBy(vModel.AdmissionListDetail.Form.Id);
                    if (admissionList != null)
                    {
                        admissionList.Deprtment.Id = vModel.AdmissionListDetail.Deprtment.Id;
                        admissionList.Programme.Id = vModel.AdmissionListDetail.Programme.Id;
                        User user = new User();
                        UserLogic userLogic = new UserLogic();
                        user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                        string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";

                        AdmissionListAudit Audit = new AdmissionListAudit();
                        Audit.Client = client;
                        Audit.Action = "UPDATE";
                        Audit.Operation = "UPDATING ADMISSION LIST";
                        Audit.User = user;


                        bool isUpdate = admissionListLogic.Update(admissionList, Audit);
                        if (isUpdate)
                        {
                            TempData["UpdateSuccess"] = "Student Admission Details Updated Successfully";
                            return RedirectToAction("SearchAdmittedStudents");
                        }
                        TempData["UpdateFailure"] = "Student Admission Details Update Failed";
                        return RedirectToAction("SearchAdmittedStudents");
                    }

                }
                return RedirectToAction("SearchAdmittedStudents");
            }
            catch (Exception)
            {

                throw;
            }

        }
        [HttpGet]
        public ActionResult GetDepartmentAndLevelByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                // List<Level> levels = null;
                List<Department> departments = null;
                Programme programme = new Programme() { Id = Convert.ToInt32(id) };
                if (programme.Id > 0)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    departments = departmentLogic.GetBy(programme);

                }
                return Json(new { Departments = departments }, "json", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult UnregisteredStudent()
        {
          try
            {


                viewmodel = new UploadAdmissionViewModel();
                viewmodel.ProgrammeSelectListItem = Utility.PopulateProgrammeSelectListItem();
                viewmodel.SessionSelectListItem = Utility.PopulateSessionSelectListItem();
                viewmodel.AdmissionListTypeSelectListItem = Utility.PopulateAdmissionListTypeSelectListItem();
                ViewBag.ProgrammeId = viewmodel.ProgrammeSelectListItem;
                ViewBag.SessionId = viewmodel.SessionSelectListItem;
                ViewBag.AdmissionListTypeId = viewmodel.AdmissionListTypeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult UnregisteredStudent(UploadAdmissionViewModel vmodel)
        {
            try
            {
                KeepApplicationFormInvoiceGenerationDropDownState(vmodel);
                vmodel.UnregisteredAdmissionList = new List<UnregisteredStudent>();
                List<AppliedCourse> applicants = new List<AppliedCourse>();
                string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                string savedFileName = "";
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    if (hpf.ContentLength == 0)
                        continue;
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        FileInfo fileInfo = new FileInfo(hpf.FileName);
                        string fileExtension = fileInfo.Extension;
                        string newFile = "Admission" + "__";
                        string newFileName = newFile + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + fileExtension;

                        savedFileName = Path.Combine(pathForSaving,newFileName);
                        hpf.SaveAs(savedFileName);
                    }
                    DataSet dsAdmissionList = ReadExcel(savedFileName);
                    if (dsAdmissionList != null && dsAdmissionList.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsAdmissionList.Tables[0].Rows.Count; i++)
                        {

                           UnregisteredStudent student = new UnregisteredStudent();
                           student.Surname = dsAdmissionList.Tables[0].Rows[i][1].ToString();
                           student.Firstname = dsAdmissionList.Tables[0].Rows[i][2].ToString();
                           student.Othername = dsAdmissionList.Tables[0].Rows[i][3].ToString();
                           student.JambNumberNumber = dsAdmissionList.Tables[0].Rows[i][4].ToString();
                           student.Deprtment = new Department() {Id = vmodel.AdmissionListDetail.Deprtment.Id};
                           student.Programme = new Programme(){Id = vmodel.AdmissionListDetail.Form.ProgrammeFee.Programme.Id};
                           student.Session = new Session(){Id = vmodel.CurrentSession.Id};
                           vmodel.UnregisteredAdmissionList.Add(student);

                        }

                       
                        TempData["UploadAdmissionViewModel"] = vmodel;
                        return View(vmodel);

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View();


        }
        [HttpPost]
        public ActionResult SaveUnRegisteredAdmissionList(UploadAdmissionViewModel vmodel)
        {
            try
            {
                int UploadCount = 0;
                string operation = "INSERT";
                string action = "UPLOADING OF ADMISSION LIST";
                string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

                vmodel = (UploadAdmissionViewModel)TempData["UploadAdmissionViewModel"];
                if (vmodel.UnregisteredAdmissionList != null && vmodel.UnregisteredAdmissionList.Count > 0)
                {
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        AdmissionListBatch batch = new AdmissionListBatch();
                        AdmissionListBatchLogic batchLogic = new AdmissionListBatchLogic();
                        AdmissionListType AdmissionType = new AdmissionListType();
                        AdmissionListAudit AdmissionListAudit = new Model.Model.AdmissionListAudit();
                        UserLogic loggeduser = new UserLogic();
                        AdmissionType = vmodel.AdmissionListType;
                        batch.DateUploaded = DateTime.Now;
                        batch.IUploadedFilePath = "NAN";
                        batch.Type = AdmissionType;
                        batch = batchLogic.Create(batch);

                        AdmissionListAudit.Action = action;
                        AdmissionListAudit.Client = client;
                        AdmissionListAudit.Operation = operation;
                        AdmissionListAudit.Time = DateTime.Now;
                        AdmissionListAudit.User = loggeduser.GetModelBy(u => u.User_Name == User.Identity.Name);
                       
                        UploadCount = 0;
                        for (int i = 0; i < vmodel.UnregisteredAdmissionList.Count; i++)
                        {

                            if (vmodel.UnregisteredAdmissionList[i].Programme.Id == 5 && vmodel.UnregisteredAdmissionList[i].JambNumberNumber != null)
                            {
                                string jambNo = vmodel.UnregisteredAdmissionList[i].JambNumberNumber;
                                ApplicantJambDetailLogic jambDetailLogic = new ApplicantJambDetailLogic();
                                var jambDetail = jambDetailLogic.GetModelsBy(a => a.Applicant_Jamb_Registration_Number == jambNo && a.Application_Form_Id != null).FirstOrDefault();
                                if (jambDetail != null)
                                {
                                    if (!admissionListLogic.IsAdmitted(jambDetail.ApplicationForm))
                                    {
                                        AdmissionList list = new AdmissionList();
                                        list.Form = jambDetail.ApplicationForm;
                                        list.Deprtment =  new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                                        admissionListLogic.Create(list, batch, AdmissionListAudit);
                                        UploadCount++;
                                    }
                                    else
                                    {
                                        AdmissionList list = admissionListLogic.GetBy(jambDetail.ApplicationForm.Id);
                                        if (list != null)
                                        {
                                            list.Deprtment = new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                                            list.Programme = new Programme {Id = vmodel.UnregisteredAdmissionList[i].Programme.Id};
                                            admissionListLogic.Update(list, AdmissionListAudit);
                                             UploadCount++;
                                        }
                                    }

                                    continue;
                                }
                            }
                            else if ((vmodel.UnregisteredAdmissionList[i].Programme.Id == 1 && vmodel.UnregisteredAdmissionList[i].JambNumberNumber != null) || (vmodel.UnregisteredAdmissionList[i].Programme.Id == 2 && vmodel.UnregisteredAdmissionList[i].JambNumberNumber != null))
                            {
                               string formNo = vmodel.UnregisteredAdmissionList[i].JambNumberNumber;
                               ApplicationForm form =  applicationFormLogic.GetBy(formNo);
                                if (form != null)
                                {
                                    if (admissionListLogic.IsAdmitted(form))
                                    {
                                        AdmissionList list = admissionListLogic.GetBy(form.Id);
                                        if (list != null)
                                        {
                                            list.Deprtment = new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                                            list.Programme = new Programme {Id = vmodel.UnregisteredAdmissionList[i].Programme.Id};
                                            admissionListLogic.Update(list, AdmissionListAudit);
                                            UploadCount++;
                                        }
                                    }
                                }
                            }


                            //create person
                            var applicantSex = new Sex {Id = 1};
                            string mobile = "NAN";
                            string hometown = "NAN";
                            string permanentAddress = "NAN";
                            string contact_address = "NAN";
                            string email = "NAN";
                            var role = new Role {Id = 6};
                            var personType = new PersonType {Id = 4};
                            var nationality = new Nationality {Id = 1};
                            var religion = new Religion {Id = 1};
                            var applicantPerson = new Person();
                            applicantPerson.DateEntered = DateTime.Now;
                            applicantPerson.MobilePhone = mobile;
                            applicantPerson.Religion = religion;
                            applicantPerson.HomeTown = hometown;
                            applicantPerson.HomeAddress = permanentAddress;
                            applicantPerson.Role = role;
                            applicantPerson.PersonType = personType;
                            applicantPerson.Nationality = nationality;
                            applicantPerson.LastName = vmodel.UnregisteredAdmissionList[i].Surname;
                            applicantPerson.FirstName = vmodel.UnregisteredAdmissionList[i].Firstname;
                            applicantPerson.OtherName = vmodel.UnregisteredAdmissionList[i].Othername;
                            applicantPerson.State = new State(){Id = "IM"};
                            applicantPerson.LocalGovernment = new LocalGovernment(){Id = 1};
                            applicantPerson.ContactAddress = contact_address;
                            applicantPerson.Sex = applicantSex;
                            applicantPerson.Email = email;
                            var personLogic = new PersonLogic();
                            applicantPerson = personLogic.Create(applicantPerson);

                            //Create Payment
                            PaymentLogic paymentLogic = new PaymentLogic();
                            Payment payment = new Payment();
                            payment.Person = applicantPerson;
                            payment.PaymentMode = new PaymentMode() { Id = 1 };
                            payment.PaymentType = new PaymentType() { Id = 2 };
                            payment.PersonType = new PersonType() { Id = 4 };
                            payment.FeeType = new FeeType() { Id = 1 };
                            payment.DatePaid = DateTime.Now;
                            payment.Session = new Session() { Id = 2};

                            OnlinePayment newOnlinePayment = null;
                            Payment newPayment = paymentLogic.Create(payment);
                            if (newPayment != null)
                            {
                                PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
                                OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                                OnlinePayment onlinePayment = new OnlinePayment();
                                onlinePayment.Channel = channel;
                                onlinePayment.Payment = newPayment;
                                newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
                            }

                             //Create Applied Course
                            var applicantAppliedCourse = new AppliedCourse();
                            var appliedCourseLogic = new AppliedCourseLogic();
                            applicantAppliedCourse.Programme = new Programme {Id = vmodel.UnregisteredAdmissionList[i].Programme.Id};
                            applicantAppliedCourse.Department = new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                            applicantAppliedCourse.Person = applicantPerson;
                            applicantAppliedCourse = appliedCourseLogic.Create(applicantAppliedCourse);


                            //create application form
                            var applicationForm = new ApplicationForm();
                            
                            applicationForm.DateSubmitted = DateTime.Now;
                            applicationForm.Payment = newPayment;
                            applicationForm.Person = applicantPerson;
                            applicationForm.ProgrammeFee = new ApplicationProgrammeFee {Id = 1};
                            applicationForm.RejectReason = "";
                            applicationForm.Remarks = "-UNREGISTERED STUDENT-";
                            applicationForm.Rejected = false;
                            applicationForm.Release = true;
                            applicationForm.Setting = new ApplicationFormSetting {Id = 2};
                            applicationForm = applicationFormLogic.Create(applicationForm);

                            
                            applicantAppliedCourse.Programme = new Programme {Id = vmodel.UnregisteredAdmissionList[i].Programme.Id};
                            applicantAppliedCourse.Department = new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                            applicantAppliedCourse.Person = applicantPerson;
                            applicantAppliedCourse.ApplicationForm = applicationForm;
                            appliedCourseLogic.Modify(applicantAppliedCourse);
                            
                            
                           
                            //create Applicant
                            Model.Model.Applicant applicant = new Model.Model.Applicant();
                            ApplicantLogic applicantLogic = new ApplicantLogic();
                            applicant.Ability = new Ability(){Id = 1};
                            applicant.ApplicationForm = applicationForm;
                            applicant.ExtraCurricullarActivities = "None";
                            applicant.OtherAbility = "None";
                            applicant.Person = applicantPerson;
                            applicant.Status = new ApplicantStatus(){Id = 1};
                            applicantLogic.Create(applicant);


                            //create applicant jamb detail
                            if (vmodel.UnregisteredAdmissionList[i].JambNumberNumber != null && vmodel.UnregisteredAdmissionList[i].JambNumberNumber.Length > 5)
                            {
                                if (vmodel.UnregisteredAdmissionList[i].Programme.Id == 5)
                                {
                                    var applicantJambDetail = new ApplicantJambDetail();
                                    var applicantJambDetailLogic = new ApplicantJambDetailLogic();
                                    applicantJambDetail.Person = applicantPerson;
                                    applicantJambDetail.ApplicationForm = applicationForm;
                                    applicantJambDetail.JambRegistrationNumber = vmodel.UnregisteredAdmissionList[i].JambNumberNumber;
                                    applicantJambDetail = applicantJambDetailLogic.Create(applicantJambDetail);
                                }
                            }
                           

                            //add to admissionList
                            AdmissionList admissionlist = new AdmissionList();
                            admissionlist.Form = applicationForm;
                            admissionlist.Deprtment =  new Department(){Id = vmodel.UnregisteredAdmissionList[i].Deprtment.Id};
                            admissionlist.Programme = new Programme {Id = vmodel.UnregisteredAdmissionList[i].Programme.Id};
                            admissionListLogic.Create(admissionlist, batch, AdmissionListAudit);
                             UploadCount++;
                        }
                        transaction.Complete();
                    }

                     SetMessage( UploadCount.ToString() + " out of "+ vmodel.UnregisteredAdmissionList.Count.ToString() + " names List was uploaded successfully", Message.Category.Information);
                  
                    return RedirectToAction("UnregisteredStudent");
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            SetMessage("List was not uploaded successfully, Please check for duplicates and try again", Message.Category.Information);
                  
            return View();
        }
        public ActionResult ApplicantList()
        {
            try
            {
                viewmodel = new UploadAdmissionViewModel();
                ViewBag.SessionId = viewmodel.AllSessionSelectListItem;
                ViewBag.Programme = viewmodel.ProgrammeSelectListItem;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }


        [HttpPost]
        public ActionResult ApplicantList(UploadAdmissionViewModel viewModel)
        {
            try
            {
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

                if (viewModel.CurrentSession != null)
                {
                    viewModel.Applicants = applicationFormLogic.GetApplicantList(viewModel.CurrentSession, viewModel.Programme, viewModel.Department, Convert.ToDateTime(viewModel.DateFrom), Convert.ToDateTime(viewModel.DateTo));
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.SessionId = viewModel.AllSessionSelectListItem;
            ViewBag.Programme = viewModel.ProgrammeSelectListItem;
            if (viewModel.Department != null && viewModel.Department.Id > 0 && viewModel.Programme != null && viewModel.Programme.Id > 0)
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(viewModel.Programme);

                ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.Department.Id);
            }
            else
            {
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
            }

            return View(viewModel);
        }
        public ActionResult ApplicantListBulk()
        {
            try
            {
                viewmodel = new UploadAdmissionViewModel();
                ViewBag.SessionId = viewmodel.AllSessionSelectListItem;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View();
        }


        [HttpPost]
        public ActionResult ApplicantListBulk(UploadAdmissionViewModel viewModel)
        {
            try
            {
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

                if (viewModel.CurrentSession != null)
                {
                    viewModel.Applicants = applicationFormLogic.GetApplicantListBulk(viewModel.CurrentSession, Convert.ToDateTime(viewModel.DateFrom), Convert.ToDateTime(viewModel.DateTo));

                    var gv = new GridView();

                    if (viewModel.Applicants != null && viewModel.Applicants.Count > 0)
                    {
                        gv.DataSource = viewModel.Applicants;

                        gv.Caption = "Applicants";
                        gv.DataBind();
                        string filename = "Applicants";

                        ViewBag.SessionId = viewModel.AllSessionSelectListItem;

                        return new DownloadFileActionResult(gv, filename + ".xls");
                    }

                    Response.Write("No data available for download");
                    Response.End();

                    ViewBag.SessionId = viewModel.AllSessionSelectListItem;

                    return new JavaScriptResult();
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.SessionId = viewModel.AllSessionSelectListItem;

            return View(viewModel);
        }
        public ActionResult AdmissionList()
        {
            UploadAdmissionViewModel viewModel = new UploadAdmissionViewModel();
            try
            {
                ViewBag.Session = viewModel.AllSessionSelectListItem;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectListItem;


            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AdmissionList(UploadAdmissionViewModel viewModel)
        {
            try
            {
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();

                if (viewModel.CurrentSession != null && viewModel.CurrentSession.Id > 0 && viewModel.Programme != null && viewModel.Programme.Id > 0 && viewModel.Department != null && viewModel.Department.Id > 0)
                {
                    viewModel.AdmissionModelList = admissionListLogic.GetAdmissionListBy(viewModel.CurrentSession, viewModel.Programme, viewModel.Department, Convert.ToDateTime(viewModel.DateFrom), Convert.ToDateTime(viewModel.DateTo));
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Session = viewModel.AllSessionSelectListItem;

            ViewBag.Programme = viewModel.ProgrammeSelectListItem;

            if (viewModel.Department != null && viewModel.Department.Id > 0 && viewModel.Programme != null && viewModel.Programme.Id > 0)
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(viewModel.Programme);

                ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.Department.Id);
            }
            else
            {
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
            }

            return View(viewModel);
        }
        public ActionResult AdmissionListBulk()
        {
            UploadAdmissionViewModel viewModel = new UploadAdmissionViewModel();
            try
            {
                ViewBag.Session = viewModel.AllSessionSelectListItem;

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AdmissionListBulk(UploadAdmissionViewModel viewModel)
        {
            try
            {
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();

                if (viewModel.CurrentSession != null && viewModel.CurrentSession.Id > 0)
                {
                    viewModel.AdmissionModelList = admissionListLogic.GetAdmissionListBulk(viewModel.CurrentSession, Convert.ToDateTime(viewModel.DateFrom), Convert.ToDateTime(viewModel.DateTo));

                    var gv = new GridView();

                    if (viewModel.AdmissionModelList != null && viewModel.AdmissionModelList.Count > 0)
                    {
                        gv.DataSource = viewModel.AdmissionModelList;

                        gv.Caption = "Admission List";
                        gv.DataBind();
                        string filename = "Admission List";

                        ViewBag.SessionId = viewModel.AllSessionSelectListItem;

                        return new DownloadFileActionResult(gv, filename + ".xls");
                    }

                    Response.Write("No data available for download");
                    Response.End();

                    ViewBag.Session = viewModel.AllSessionSelectListItem;

                    return new JavaScriptResult();
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Session = viewModel.AllSessionSelectListItem;

            return View(viewModel);
        }
        public ActionResult EditAdmission()
        {
            UploadAdmissionViewModel viewModel = new UploadAdmissionViewModel();
            try
            {
                ViewBag.ProgrammeId = viewModel.ProgrammeSelectListItem;
                ViewBag.SessionId = Utility.PopulateSessionSelectListItem();
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditAdmission(UploadAdmissionViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    List<AdmissionList> admissionList = new List<AdmissionList>();

                    //admissionList = admissionListLogic.GetModelsBy(a => a.Session_Id == viewModel.CurrentSession.Id && a.Programme_Id == viewModel.Programme.Id && a.Department_Id == viewModel.Department.Id);
                    admissionList = admissionListLogic.GetModelsBy(a => a.Programme_Id == viewModel.Programme.Id && a.Department_Id == viewModel.Department.Id);
                    viewModel.AdmissionList = admissionList.OrderBy(a => a.Form.Person.FullName).ToList();
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            ViewBag.ProgrammeId = viewModel.ProgrammeSelectListItem;
            ViewBag.SessionId = Utility.PopulateSessionSelectListItem();

            if (viewModel.Department != null && viewModel.Programme != null)
            {
                ViewBag.DepartmentId = new SelectList(Utility.PopulateDepartmentSelectListItem(viewModel.Programme), "Value", "Text", viewModel.Department.Id);
            }
            else
            {
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult SaveEditedAdmission(UploadAdmissionViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    AdmissionListAudit audit = new AdmissionListAudit();
                    User user = new User();
                    UserLogic userLogic = new UserLogic();

                    for (int i = 0; i < viewModel.AdmissionList.Count; i++)
                    {
                        if (viewModel.AdmissionList[i].Deactivated)
                        {
                            long currentAdmissionListId = viewModel.AdmissionList[i].Id;
                            AdmissionList admissionList = admissionListLogic.GetModelBy(a => a.Admission_List_Id == currentAdmissionListId);
                            admissionList.Activated = false;

                            user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                            string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";

                            audit.Client = client;
                            audit.Action = "MODIFY";
                            audit.Operation = "MODIFY ADMISSION";
                            audit.User = user;

                            admissionListLogic.Modify(admissionList, audit);
                        }
                        if (viewModel.AdmissionList[i].ActivateAlt)
                        {
                            long currentAdmissionListId = viewModel.AdmissionList[i].Id;
                            AdmissionList admissionList = admissionListLogic.GetModelBy(a => a.Admission_List_Id == currentAdmissionListId);
                            admissionList.Activated = true;

                            user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                            string client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")";

                            audit.Client = client;
                            audit.Action = "MODIFY";
                            audit.Operation = "MODIFY ADMISSION";
                            audit.User = user;

                            admissionListLogic.Modify(admissionList, audit);
                        }
                    }

                    SetMessage("Operation Successful! ", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("EditAdmission");
        }

        public ActionResult RemoveAdmission(long id)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (id > 0)
                {
                    AdmissionListAuditLogic admissionListAuditLogic = new AdmissionListAuditLogic();
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();

                        long admissionListId = id;

                        using (TransactionScope scope = new TransactionScope())
                        {
                            admissionListAuditLogic.Delete(a => a.Admission_List_Id == admissionListId);
                            admissionListLogic.Delete(a => a.Admission_List_Id == admissionListId);

                            scope.Complete();
                        }
                    
                    result.IsError = false;
                    result.Message = "Operation Successful!";
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Parameter was not set";
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}