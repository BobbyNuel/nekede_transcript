﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class ResultManagerController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private StaffCourseAllocationViewModel viewModel;
        private string FileUploadURL = null;
        // GET: Admin/ResultManager
        public ActionResult Index()
        {
             try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult Index(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocation courseAllocation = new CourseAllocation();
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id && p.Department_Id == viewModel.Department.Id && p.Level_Id == viewModel.Level.Id && p.Programme_Id == viewModel.Programme.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                    if (courseAllocation == null)
                    {
                        if (!User.IsInRole("Admin"))
                        {
                            SetMessage("You are not allocated to this course, with this Programme-Department combination", Message.Category.Error);
                            KeepDropDownState(viewModel);
                            return RedirectToAction("Index");                            
                        }
                    }
                    GridView gv = new GridView();

                    DataTable ds = new DataTable();
                    List<ResultManageFormat> resultFormatList = new List<ResultManageFormat>();

                    SessionLogic sessionLogic = new SessionLogic();
                    Session session = sessionLogic.GetModelBy(p => p.Activated == true);
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                    List<CourseRegistrationDetail> courseRegistrationDetailList = new List<CourseRegistrationDetail>();
                    List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    courseRegistrationList = courseRegistrationLogic.GetModelsBy(p => p.Session_Id == session.Id && p.Level_Id == viewModel.Level.Id && p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id);
                    if (viewModel.Course != null && viewModel.Semester != null && courseRegistrationList.Count > 0)
                    {
                        int count = 1;
                        foreach (CourseRegistration courseRegistration in courseRegistrationList)
                        {
                            courseRegistrationDetailList = courseRegistrationDetailLogic.GetModelsBy(p => p.Course_Id == viewModel.Course.Id && p.Semester_Id == viewModel.Semester.Id && p.Student_Course_Registration_Id == courseRegistration.Id);
                            if (courseRegistrationDetailList.Count > 0)
                            {

                                foreach (CourseRegistrationDetail courseRegistrationDetailItem in courseRegistrationDetailList)
                                {
                                    ResultManageFormat resultFormat = new ResultManageFormat();
                                    // resultFormat.SN = count;
                                    resultFormat.MATRICNO = courseRegistrationDetailItem.CourseRegistration.Student.MatricNumber;
                                    resultFormat.Course_Code = courseRegistrationDetailItem.Course.Code;
                                    resultFormat.Department = courseRegistrationDetailItem.Course.Department.Name;
                                    resultFormat.Level = courseRegistrationDetailItem.CourseRegistration.Level.Name;
                                    resultFormat.Programme = courseRegistrationDetailItem.CourseRegistration.Programme.Name;
                                    resultFormat.Semester = courseRegistrationDetailItem.Semester.Name;
                                    resultFormat.Session = courseRegistrationDetailItem.CourseRegistration.Session.Name;
                                    resultFormatList.Add(resultFormat);
                                    count++;
                                }
                            }
                        }
                    }

                    if (resultFormatList.Count > 0)
                    {
                        List<ResultManageFormat> list = resultFormatList.OrderBy(p => p.MATRICNO).ToList();
                        List<ResultManageFormat> sort = new List<ResultManageFormat>();
                        for (int i = 0; i < list.Count; i++)
                        {
                            list[i].SN = (i + 1);
                            sort.Add(list[i]);
                        }

                        gv.DataSource = sort;// resultFormatList.OrderBy(p => p.MATRICNO);
                        CourseLogic courseLogic = new CourseLogic();
                        Course course = courseLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id);
                        gv.Caption = course.Name.ToUpper() + " " +course.Code + " " + " DEPARTMENT OF " + " " + course.Department.Name.ToUpper() + " " + course.Unit + " " + "Units";
                        gv.DataBind();

                        string filename = course.Code.Replace("/", "").Replace("\\", "") + course.Department.Code + ".xls";
                        return new DownloadFileActionResult(gv, filename);
                    }
                    else
                    {
                        Response.Write("No data available for download");
                        Response.End();
                        return new JavaScriptResult();
                    }
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Error);

                    KeepDropDownState(viewModel);

                    return RedirectToAction("Index");
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }


            return RedirectToAction("Index");
        }
        public void KeepDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.User = viewModel.UserSelectList;
                if (viewModel.Semester != null)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.Semester.Id);
                }
                if (viewModel.Department != null && viewModel.Department.Id > 0)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = new List<Department>();
                    departments = departmentLogic.GetBy(viewModel.Programme);

                    ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.Department.Id);
                }
                if (viewModel.Course != null && viewModel.Course.Id > 0)
                {
                    List<Course> courseList = new List<Course>();
                    courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, viewModel.Department, viewModel.Semester);

                    ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Course.Id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void KeepCourseDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.User = viewModel.UserSelectList;
                if (viewModel.CourseAllocation.Semester != null)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.CourseAllocation.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.CourseAllocation.Semester.Id);
                }
                if (viewModel.CourseAllocation.Department != null)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = new List<Department>();
                    departments = departmentLogic.GetBy(viewModel.CourseAllocation.Programme);

                    ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.CourseAllocation.Department.Id);
                }
                if (viewModel.CourseAllocation.Course != null)
                {
                    List<Course> courseList = new List<Course>();
                    courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.CourseAllocation.Level, viewModel.CourseAllocation.Department, viewModel.CourseAllocation.Semester);

                    ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.CourseAllocation.Course.Id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult UploadResultSheet()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);

            }
            return View();
        }
        [HttpPost]
        public ActionResult UploadResultSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                List<ResultManageFormat> resultFormatList = new List<ResultManageFormat>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcel(savedFileName);

                    if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 1; i < studentSet.Tables[0].Rows.Count; i++)
                        {
                            ResultManageFormat resultFormat = new ResultManageFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.MATRICNO = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.Test_Score = Convert.ToDecimal(studentSet.Tables[0].Rows[i][2].ToString().Trim());
                            resultFormat.Exam_Score = Convert.ToDecimal(studentSet.Tables[0].Rows[i][3].ToString().Trim());
                            if (resultFormat.Test_Score + resultFormat.Exam_Score > 100)
                            {
                                resultFormat.Programme = "Sum is greater than 100, please cross-check before saving";
                            }
                            if (resultFormat.MATRICNO != "")
                            {
                                resultFormatList.Add(resultFormat);
                            }

                        }
                        resultFormatList.OrderBy(p => p.MATRICNO);
                        viewModel.ResultManageFormats = resultFormatList;
                        TempData["staffCourseAllocationViewModel"] = viewModel;

                    }

                }
            }
            catch (Exception ex)
            {

                SetMessage("Error occured! " + ex.Message, Message.Category.Error);
            }
            RetainDropdownState(viewModel);
            return View(viewModel);
        }
        public void RetainDropdownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                LevelLogic levelLogic = new LevelLogic();
                CourseModeLogic courseModeLogic = new CourseModeLogic();
                if (viewModel != null)
                {
                    if (viewModel.Session != null)
                    {

                        ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectList;
                    }
                    if (viewModel.Semester != null)
                    {
                        ViewBag.Semester = new SelectList(semesterLogic.GetAll(), ID, NAME, viewModel.Semester.Id);
                    }
                    else
                    {
                        ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                    }
                    if (viewModel.Programme != null)
                    {
                        ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "value", "Text", viewModel.Programme.Id);
                    }
                    else
                    {
                        ViewBag.Programme = viewModel.ProgrammeSelectList;
                    }
                    if (viewModel.Department != null && viewModel.Programme != null)
                    {
                        ViewBag.Department = new SelectList(departmentLogic.GetBy(viewModel.Programme), ID, NAME, viewModel.Department.Id);
                    }
                    else
                    {
                        ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                    }
                    if (viewModel.Level != null)
                    {
                        ViewBag.Level = new SelectList(levelLogic.GetAll(), ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                    }
                    if (viewModel.Course != null && viewModel.Level != null && viewModel.Semester != null && viewModel.Department != null)
                    {
                        List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, viewModel.Department, viewModel.Semester);
                        for (int i = 1; i < courseList.Count; i++)
                        {
                            courseList[i].Name += ", " + courseList[i].Code;
                        }
                        ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                    }
                    if (viewModel.CourseMode != null)
                    {
                        ViewBag.CourseMode = new SelectList(courseModeLogic.GetAll(), ID, NAME, viewModel.CourseMode.Id);
                    }
                    else
                    {
                        ViewBag.CourseMode = new SelectList(viewModel.CourseModeSelectList, ID, NAME);
                    }
                    ViewBag.DepartmentOption = new SelectList(new List<DepartmentOption>(), ID, NAME);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public enum SpeicalCaseCodes
        {
            Sick = 101,
            Absent = 201,
            Other = 301
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);
                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }
        public ActionResult SaveUploadedResultSheet()
        {
            StaffCourseAllocationViewModel viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
            try
            {
                if (viewModel != null)
                {
                    int status = validateFields(viewModel.ResultManageFormats);

                    if (status > 0)
                    {
                        ResultManageFormat format = viewModel.ResultManageFormats.ElementAt((status - 1));
                        SetMessage("Validation Error for" + " " + format.MATRICNO, Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("UploadResultSheet");
                    }

                    bool resultAdditionStatus = addStudentResult(viewModel);

                    SetMessage("Upload successful", Message.Category.Information);
                }


            }
            catch (Exception ex)
            {
                SetMessage("Error occured " + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return RedirectToAction("UploadResultSheet");
        }
        private int validateFields(List<ResultManageFormat> list)
        {
            try
            {
                int failedReason = 0;

                if (list != null && list.Count > 0)
                {

                    for (int i = 0; i < list.Count; i++)
                    {
                        bool testStatus;
                        bool examStatus;
                        bool totalStatus;
                        decimal testScore = list[i].Test_Score;
                        decimal inputExamScore = list[i].Exam_Score;
                        if (testScore > 100 || inputExamScore > 100)
                        {
                            AssignSpecialCaseRemarks(list, i, testScore, inputExamScore);
                        }
                        else
                        {
                            decimal calculatedExamScore = list[i].Exam_Score;
                            decimal inputTotalScore = list[i].Test_Score + list[i].Exam_Score;
                            decimal calculatedTotalScore = list[i].Test_Score + list[i].Exam_Score;
                            if (testScore >= 0 && testScore <= 30)
                            {
                                testStatus = true;

                            }
                            else
                            {
                                testStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }
                            }

                            if ((inputExamScore >= 0 && inputExamScore <= 70) && (calculatedExamScore >= 0 && calculatedExamScore <= 70))
                            {
                                examStatus = true;

                            }
                            else
                            {
                                examStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }

                            }

                            if ((calculatedTotalScore == inputTotalScore) && calculatedTotalScore <= 100)
                            {
                                totalStatus = true;
                            }
                            else
                            {
                                totalStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }
                            }
                            if (calculatedExamScore != inputExamScore)
                            {
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }
                            }


                        }

                    }

                }
                if (failedReason > 0)
                {
                    return failedReason;
                }
                else
                {
                    return 0;
                }



            }
            catch (Exception)
            {

                throw;
            }
        }
        private static void AssignSpecialCaseRemarks(List<ResultManageFormat> list, int i, decimal testScore, decimal inputExamScore)
        {
            try
            {
                if (testScore == (decimal)StaffCourseAllocationController.SpeicalCaseCodes.Sick)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "SICK: TEST";
                    list[i].ResultSpecialCaseMessages.TestSpecialCaseMessage = "SICK: TEST";
                    list[i].Test_Score = 0;
                }
                else if (inputExamScore == (decimal)StaffCourseAllocationController.SpeicalCaseCodes.Sick)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "SICK";
                    list[i].ResultSpecialCaseMessages.ExamSpecialCaseMessage = "SICK";
                    list[i].Exam_Score = 0;
                }
                else if (inputExamScore == (decimal)StaffCourseAllocationController.SpeicalCaseCodes.Absent)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "ABSENT";
                    list[i].ResultSpecialCaseMessages.ExamSpecialCaseMessage = "ABSENT";
                    list[i].Exam_Score = 0;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
  
        }
        private bool addStudentResult(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
                SessionSemesterLogic sessionSemesterLogicc = new SessionSemesterLogic();
                SessionSemester sessionSemester = new SessionSemester();
                UserLogic userLogic = new UserLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentResultType testResultType = new StudentResultType() { Id = 1 };
                StudentResultType examResultType = new StudentResultType() { Id = 2 };
                User user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                sessionSemester = sessionSemesterLogicc.GetModelBy(p => p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id);
                if (viewModel != null && viewModel.ResultManageFormats.Count > 0)
                {
                    CourseRegistration courseRegistration = new CourseRegistration();
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                    StudentResultDetail studentResultDetailTest;
                    StudentResultDetail studentResultDetailExam;
                    List<StudentResultDetail> studentResultDetailTestList;
                    List<StudentResultDetail> studentResultDetailExamList;
                    StudentResultLogic studentResultLogic;
                    StudentResult studentResultTest;
                    StudentResult studentResultExam;
                    StudentExamRawScoreSheet studentExamRawScoreSheet = new StudentExamRawScoreSheet();
                    StudentExamRawScoreSheetResultLogic StudentExamRawScoreSheetLogic = new StudentExamRawScoreSheetResultLogic();

                    StudentResultDetailLogic studentResultDetailLogic = new StudentResultDetailLogic();
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new System.TimeSpan(0, 15, 0)))
                    {
                        foreach (ResultManageFormat resultFormat in viewModel.ResultManageFormats)
                        {
                            resultFormat.fileUploadUrl = "xxx";
                            InitializeStudentResult(viewModel,resultFormat.fileUploadUrl ,sessionSemester, testResultType, examResultType, user, out studentResultDetailTest, out studentResultDetailExam, out studentResultDetailTestList, out studentResultDetailExamList, out studentResultLogic, out studentResultTest, out studentResultExam);
                            studentResultDetailTest.Course = viewModel.Course;

                            courseRegistration = courseRegistrationLogic.GetModelsBy(c => c.STUDENT.Matric_Number == resultFormat.MATRICNO.Trim() && c.Session_Id == viewModel.Session.Id).LastOrDefault();
                            if (courseRegistration != null)
                            {
                                  studentResultDetailTest.Student = studentLogic.GetModelBy(p => p.Matric_Number == resultFormat.MATRICNO.Trim() && p.Person_Id == courseRegistration.Student.Id);
                                    if (studentResultDetailTest.Student != null)
                                    {
                                        studentResultDetailTest.Score = resultFormat.Test_Score;
                                        studentResultDetailTest.SpecialCaseMessage = resultFormat.ResultSpecialCaseMessages.TestSpecialCaseMessage;
                                        studentResultDetailTestList.Add(studentResultDetailTest);
                                        studentResultTest.Results = studentResultDetailTestList;
                                        studentResultLogic.Add(studentResultTest);

                                        studentResultDetailExam.Course = viewModel.Course;
                                        studentResultDetailExam.Student = studentResultDetailTest.Student;
                                        studentResultDetailExam.Score = resultFormat.Exam_Score;
                                        studentResultDetailExam.SpecialCaseMessage = resultFormat.ResultSpecialCaseMessages.ExamSpecialCaseMessage;
                                        studentResultDetailExamList.Add(studentResultDetailExam);
                                        studentResultExam.Results = studentResultDetailExamList;
                                        studentResultLogic.Add(studentResultExam);

                                        studentExamRawScoreSheet = StudentExamRawScoreSheetLogic.GetModelBy(p => p.Student_Id == studentResultDetailExam.Student.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Course_Id == viewModel.Course.Id);
                                        List<StudentExamRawScoreSheet> a =StudentExamRawScoreSheetLogic.GetModelsBy(p => p.Student_Id == studentResultDetailExam.Student.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Course_Id == viewModel.Course.Id);
                                       
                                    }
                            }
                        }
                        scope.Complete();
                    }


                }

                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private static void InitializeStudentResult(StaffCourseAllocationViewModel viewModel,string FileUploadURL, SessionSemester sessionSemester, StudentResultType testResultType, StudentResultType examResultType, User user, out StudentResultDetail studentResultDetailTest, out StudentResultDetail studentResultDetailExam, out List<StudentResultDetail> studentResultDetailTestList, out List<StudentResultDetail> studentResultDetailExamList, out StudentResultLogic studentResultLogic, out StudentResult studentResultTest, out StudentResult studentResultExam)
        {
            studentResultDetailTest = new StudentResultDetail();
            studentResultDetailExam = new StudentResultDetail();
            studentResultDetailTestList = new List<StudentResultDetail>();
            studentResultDetailExamList = new List<StudentResultDetail>();
            studentResultLogic = new StudentResultLogic();
            studentResultTest = new StudentResult();
            studentResultExam = new StudentResult();

            studentResultTest.MaximumObtainableScore = 30;
            studentResultTest.DateUploaded = DateTime.Now;
            studentResultTest.Department = viewModel.Department;
            studentResultTest.Level = viewModel.Level;
            studentResultTest.Programme = viewModel.Programme;
            studentResultTest.SessionSemester = sessionSemester;
            studentResultTest.UploadedFileUrl = FileUploadURL;
            studentResultTest.Uploader = user;
            studentResultTest.Type = testResultType;

            studentResultExam.MaximumObtainableScore = 70;
            studentResultExam.DateUploaded = DateTime.Now;
            studentResultExam.Department = viewModel.Department;
            studentResultExam.Level = viewModel.Level;
            studentResultExam.Programme = viewModel.Programme;
            studentResultExam.SessionSemester = sessionSemester;
            studentResultExam.UploadedFileUrl = FileUploadURL;
            studentResultExam.Uploader = user;
            studentResultExam.Type = examResultType;
        }
        public ActionResult ViewResults()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult ViewResults(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null && viewModel.Course != null && viewModel.Semester != null && viewModel.Session != null &&viewModel.Department != null && viewModel.Programme != null && viewModel.Level != null)
                {
                
                }
            }
            catch (Exception)
            {
                
                throw;
            }
            KeepDropDownState(viewModel);
            return View();
        }
        

        public ActionResult UploadStudentAndResult()
        {
            try
            { 
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                ViewBag.CourseMode = viewModel.CourseModeSelectList;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult UploadStudentAndResult(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                List<StudentResultFormat> resultFormatList = new List<StudentResultFormat>();
                List<string> invalidNameSN = new List<string>();
                List<string> invalidRegNumberSN = new List<string>();
                List<string> invalidGradeSN = new List<string>();
                string[] gradeList = {"A", "AB", "B", "BC", "C", "CD", "D", "E", "F"};
                string message = "";
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcel(savedFileName);

                    if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < studentSet.Tables[0].Rows.Count; i++)
                        {
                            StudentResultFormat resultFormat = new StudentResultFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.MATRICNO = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.NAME = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                            //resultFormat.Practical = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                            resultFormat.Grade = studentSet.Tables[0].Rows[i][3].ToString().Trim();
                            resultFormat.Course_Code = studentSet.Tables[0].Rows[i][4].ToString().Trim();
                            resultFormat.CourseUnit = Convert.ToInt32(studentSet.Tables[0].Rows[i][5].ToString().Trim());

                            if (resultFormat.MATRICNO == "" || resultFormat.MATRICNO == " ")
                            {
                                resultFormat.MATRICNO = null;
                                invalidRegNumberSN.Add(resultFormat.SN.ToString());
                            }

                            if (resultFormat.NAME == null || resultFormat.NAME == "")
                            {
                                invalidNameSN.Add(resultFormat.SN.ToString());
                            }
                            if (!gradeList.Contains(resultFormat.Grade))
                            {
                                invalidGradeSN.Add(resultFormat.SN.ToString());
                            }

                            resultFormatList.Add(resultFormat);
                        }

                        resultFormatList.OrderBy(p => p.SN);
                        viewModel.StudentResultFormatList = resultFormatList;
                        TempData["staffCourseAllocationViewModel"] = viewModel;

                        if (invalidGradeSN.Count > 0 || invalidNameSN.Count > 0 || invalidRegNumberSN.Count > 0)
                        {
                            if (invalidNameSN.Count > 0)
                            {
                                message += "No Names in the following records, SN: ";
                                for (int i = 0; i < invalidNameSN.Count; i++)
                                {
                                    message += invalidNameSN[i] + ", ";
                                } 
                            }
                            if (invalidRegNumberSN.Count > 0)
                            {
                                message += " | No Matric NO in the following records, SN: ";
                                for (int i = 0; i < invalidRegNumberSN.Count; i++)
                                {
                                    message += invalidRegNumberSN[i] + ", ";
                                } 
                            }
                            if (invalidGradeSN.Count > 0)
                            {
                                message += " | Invalid Grade in the following records, SN: ";
                                for (int i = 0; i < invalidGradeSN.Count; i++)
                                {
                                    message += invalidGradeSN[i] + ", ";
                                } 
                            }
                            
                            SetMessage(message + ". This can prevent the affected results from being saved", Message.Category.Warning); 
                        }
                    }
                }
            }
            catch (Exception ex)
            { 
                SetMessage("Error occured! " + ex.Message, Message.Category.Error);
            }

            TempData["StaffCourseAllocationViewModel"] = viewModel;
            RetainDropdownState(viewModel);
            return View(viewModel);
        }

        public ActionResult SaveUploadedStudentResult()
        {
            StaffCourseAllocationViewModel viewModel = (StaffCourseAllocationViewModel)TempData["StaffCourseAllocationViewModel"];

            PersonLogic personLogic = new PersonLogic();
            StudentLogic studentLogic = new StudentLogic();
            StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
            CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
            CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
            CourseLogic courseLogic = new CourseLogic();
            ProgrammeLogic programmeLogic = new ProgrammeLogic();
            DepartmentLogic departmentLogic = new DepartmentLogic();
            LevelLogic levelLogic = new LevelLogic();
            SessionLogic sessionLogic = new SessionLogic();
            SemesterLogic semesterLogic = new SemesterLogic();

            List<StudentResultFormat> failedRecords = new List<StudentResultFormat>();
            
            try
            {
                string stateId = "IM";
                PersonType personType = new PersonType(){ Id = 3};
                State state = new State() { Id = stateId };
                Nationality nationality = new Nationality() {Id = 1};
                DateTime date = DateTime.Now;
                Role role = new Role(){ Id = 5};
                StudentType studentType = new StudentType();
                
                StudentCategory studentCategory = new StudentCategory() {Id = 2};
                StudentStatus studentStatus = new StudentStatus() {Id = 1};
                
                if (viewModel != null)
                {
                    if (viewModel.Programme.Id == 3 || viewModel.Programme.Id == 4 || viewModel.Programme.Id == 6)
                    {
                        studentType = new StudentType() { Id = 2 };
                    }
                    else if (viewModel.Programme.Id == 1 || viewModel.Programme.Id == 2 || viewModel.Programme.Id == 5)
                    {
                        studentType = new StudentType() { Id = 1 };
                    }

                    Programme programme = programmeLogic.GetModelBy(p => p.Programme_Id == viewModel.Programme.Id);
                    Department department = departmentLogic.GetModelBy(d => d.Department_Id == viewModel.Department.Id);
                    Session session = sessionLogic.GetModelBy(s => s.Session_Id == viewModel.Session.Id);
                    Semester semester = semesterLogic.GetModelBy(s => s.Semester_Id == viewModel.Semester.Id);
                    Level level = levelLogic.GetModelBy(l => l.Level_Id == viewModel.Level.Id);
                    Course course = courseLogic.GetModelBy(c => c.Course_Id == viewModel.Course.Id);
                    CourseMode courseMode = viewModel.CourseMode;

                    string message = "Unsuccessful Records SN: ";
                    for (int i = 0; i < viewModel.StudentResultFormatList.Count; i++)
                    {
                        string matricNumber = viewModel.StudentResultFormatList[i].MATRICNO;
                        string name = viewModel.StudentResultFormatList[i].NAME;
                        string[] names = name.Split(' ');
                        string grade = viewModel.StudentResultFormatList[i].Grade;

                        if (matricNumber == null || grade == null || grade == "" || grade == " ")
                        {
                            failedRecords.Add(viewModel.StudentResultFormatList[i]);
                            continue;
                        }

                        using (TransactionScope scope = new TransactionScope())
                        {
                            Person personCheck1 = personLogic.GetModelBy(p => p.STUDENT.Matric_Number == matricNumber);
                            Person personCheck = personLogic.GetModelBy(p => (p.Last_Name + p.First_Name + p.Other_Name).Trim() == name.Replace(" ", "").Trim());
                            Person person = new Person();
                            Person newPerson = new Person();
                            if (personCheck == null && personCheck1 == null)
                            {
                                person.PersonType = personType;
                                if (names.Count() == 1)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[0];
                                }
                                else if (names.Count() == 2)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                }
                                else if (names.Count() == 3)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                    person.OtherName = names[2];
                                }
                                else if (names.Count() >= 4)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                    person.OtherName = names[2] + " " + names[3];
                                }

                                person.State = state;
                                person.Nationality = nationality;
                                person.DateEntered = date;
                                person.Role = role;

                                newPerson = personLogic.Create(person);
                            }
                            else
                            {
                                if (personCheck != null)
                                {
                                    newPerson = personCheck; 
                                }
                                else
                                {
                                    newPerson = personCheck1; 
                                }
                                
                            }
                            
                            Model.Model.Student studentCheck = studentLogic.GetModelBy(s => (s.Matric_Number == matricNumber || s.Person_Id == newPerson.Id) && s.Matric_Number != null);
                            Model.Model.Student student = new Model.Model.Student();
                            Model.Model.Student newStudent = new Model.Model.Student();
                            if (studentCheck == null)
                            {
                                student.Id = newPerson.Id;
                                student.Type = studentType;
                                student.Category = studentCategory;
                                student.Status = studentStatus;
                                student.MatricNumber = matricNumber;

                                newStudent = studentLogic.CreateStudent(student);
                            }
                            else
                            {
                                newStudent = studentCheck;
                            }

                            StudentLevel studentLevelCheck = studentLevelLogic.GetModelBy(sl => sl.Department_Id == department.Id && sl.Level_Id == level.Id && sl.Person_Id == newStudent.Id && sl.Programme_Id == programme.Id && sl.Session_Id == session.Id);
                            StudentLevel studentLevel = new StudentLevel();
                            StudentLevel newStudentLevel = new StudentLevel();
                            if (studentLevelCheck == null)
                            {
                                studentLevel.Student = newStudent;
                                studentLevel.Level = level;
                                studentLevel.Programme = programme;
                                studentLevel.Department = department;
                                studentLevel.Session = session;

                                newStudentLevel = studentLevelLogic.Create(studentLevel);
                            }
                            else
                            {
                                newStudentLevel = studentLevelCheck;
                            }

                            CourseRegistration courseRegistrationCheck = courseRegistrationLogic.GetModelBy(cr => cr.Department_Id == department.Id && cr.Level_Id == level.Id && cr.Person_Id == newStudent.Id && cr.Programme_Id == programme.Id && cr.Session_Id == session.Id);
                            CourseRegistration courseRegistration = new CourseRegistration();
                            CourseRegistration newCourseRegistration = new CourseRegistration();
                            if (courseRegistrationCheck == null)
                            {
                                courseRegistration.Student = newStudent;
                                courseRegistration.Level = level;
                                courseRegistration.Programme = programme;
                                courseRegistration.Department = department;
                                courseRegistration.Session = session;

                                newCourseRegistration = courseRegistrationLogic.CreateCourseRegOnly(courseRegistration);
                            }
                            else
                            {
                                newCourseRegistration = courseRegistrationCheck;
                            }

                            CourseRegistrationDetail courseRegistrationDetailCheck = courseRegistrationDetailLogic.GetModelBy(c => c.Course_Id == course.Id && c.Semester_Id == semester.Id && c.Student_Course_Registration_Id == newCourseRegistration.Id);
                            CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                            CourseRegistrationDetail newCourseRegistrationDetail = new CourseRegistrationDetail();
                            if (courseRegistrationDetailCheck == null)
                            {
                                courseRegistrationDetail.CourseRegistration = newCourseRegistration;
                                courseRegistrationDetail.Course = course;
                                courseRegistrationDetail.Mode = courseMode;
                                courseRegistrationDetail.Semester = semester;
                                courseRegistrationDetail.CourseUnit = course.Unit;
                                courseRegistrationDetail.Grade = grade;

                                newCourseRegistrationDetail = courseRegistrationDetailLogic.Create(courseRegistrationDetail);
                            }
                             
                            scope.Complete();
                        }
                    }

                    if (failedRecords.Count == 0)
                    {
                        SetMessage("Operation Successful! ", Message.Category.Information);
                    }
                    else
                    {
                        for (int i = 0; i < failedRecords.Count; i++)
                        {
                            message += failedRecords[i].SN + ", ";
                        }

                        SetMessage("Done! " + message + " Details can be seen below.", Message.Category.Warning); 
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error occured! " + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            viewModel.StudentResultFormatList = failedRecords;
            return View("UploadStudentAndResult", viewModel);
        }
        public ActionResult UploadCompositeSheeet()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.DepartmentOption = new SelectList(new List<DepartmentOption>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.CourseMode = viewModel.CourseModeSelectList;  
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);  
            }
            return View();
        }
        [HttpPost]
        public ActionResult UploadCompositeSheeet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                List<StudentResultFormat> resultFormatList = new List<StudentResultFormat>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);

                    DataSet studentSet = ReadExcelComposite(savedFileName);
                    DataTable dtCompositeSheet = GetCompositeDataSet(studentSet);

                    viewModel.CompositeSheetDataTable = dtCompositeSheet;

                    List<string> invalidNameSN = new List<string>();
                    List<string> invalidRegNumberSN = new List<string>();
                    List<string> invalidGradeSN = new List<string>();
                    string[] gradeList = { "A", "AB", "B", "BC", "C", "CD", "D", "E", "F" };
                    string message = "";

                    if (dtCompositeSheet != null && dtCompositeSheet.Rows.Count > 0)
                    {
                            for (int i = 0; i < dtCompositeSheet.Rows.Count; i++)
                            {
                                StudentResultFormat resultFormat = new StudentResultFormat();

                                string MatricNo = dtCompositeSheet.Rows[i][1].ToString();
                                string Name = dtCompositeSheet.Rows[i][2].ToString();
                                int SubjectCount = dtCompositeSheet.Columns.Count - 3;

                                resultFormat.MATRICNO = MatricNo;
                                resultFormat.NAME = Name;
                                resultFormat.Course_Code = "";

                                for (int k = 3; k < dtCompositeSheet.Columns.Count; k++)
                                {
                                    if (dtCompositeSheet.Rows[i][k].ToString() != "")
                                    {
                                        var CourseCode = dtCompositeSheet.Columns[k].ToString();
                                        var Grade = dtCompositeSheet.Rows[i][k].ToString();

                                        if (Grade == "")
                                        {
                                            continue;
                                        }

                                        resultFormat.Course_Code += " | " + CourseCode + ": " + Grade;

                                        if (!gradeList.Contains(Grade.Trim()))
                                        {
                                            invalidGradeSN.Add(Name);
                                        }
                                    }
                                }

                                if (MatricNo == "" && Name == "")
                                {
                                    continue;
                                }

                                if (MatricNo == "" || MatricNo == " ")
                                {
                                    MatricNo = null;
                                    invalidRegNumberSN.Add(Name);
                                }

                                if (Name == null || Name == "")
                                {
                                    invalidNameSN.Add(MatricNo);
                                }

                                resultFormatList.Add(resultFormat);
                            }

                            resultFormatList.OrderBy(p => p.NAME);
                            viewModel.StudentResultFormatList = resultFormatList;
                            TempData["staffCourseAllocationViewModel"] = viewModel;
                            TempData["DataTable"] = dtCompositeSheet;

                            if (invalidGradeSN.Count > 0 || invalidNameSN.Count > 0 || invalidRegNumberSN.Count > 0)
                            {
                                if (invalidNameSN.Count > 0)
                                {
                                    message += "No Names in the following records, SN: ";
                                    for (int i = 0; i < invalidNameSN.Count; i++)
                                    {
                                        StudentResultFormat resultFormat = resultFormatList.Where( r => r.MATRICNO == invalidNameSN[i]).FirstOrDefault();
                                        int index = resultFormatList.IndexOf(resultFormat) + 1;
                                        message += index.ToString() + ", ";
                                    }
                                }
                                if (invalidRegNumberSN.Count > 0)
                                {
                                    message += "======> | No Matric NO in the following records, SN: ";
                                    for (int i = 0; i < invalidRegNumberSN.Count; i++)
                                    {
                                        StudentResultFormat resultFormat = resultFormatList.Where(r => r.NAME == invalidRegNumberSN[i]).FirstOrDefault();
                                        int index = resultFormatList.IndexOf(resultFormat) + 1;
                                        message += index.ToString() + ", ";
                                    }
                                }
                                if (invalidGradeSN.Count > 0)
                                {
                                    message += "======> | Invalid Grade in the following records, SN: ";
                                    for (int i = 0; i < invalidGradeSN.Count; i++)
                                    {
                                        StudentResultFormat resultFormat = resultFormatList.Where(r => r.NAME == invalidGradeSN[i]).FirstOrDefault();
                                        int index = resultFormatList.IndexOf(resultFormat) + 1;
                                        message += index.ToString() + ", ";
                                    }
                                }

                                SetMessage(message + ". This can prevent the affected results from being saved.", Message.Category.Warning);
                            }
                    }
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return View(viewModel);
        }
        public ActionResult UploadCompositeSheeetFromFolder()
        {
            try
            {
                List<StudentResultFormat> resultFormatList = new List<StudentResultFormat>();
                PersonLogic personLogic = new PersonLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                CourseLogic courseLogic = new CourseLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                LevelLogic levelLogic = new LevelLogic();
                SessionLogic sessionLogic = new SessionLogic();
                SemesterLogic semesterLogic = new SemesterLogic();
                string[] gradeList = { "A", "AB", "B", "BC", "C", "CD", "D", "E", "F" };
                
                string stateId = "IM";
                PersonType personType = new PersonType() { Id = 3 };
                State state = new State() { Id = stateId };
                Nationality nationality = new Nationality() { Id = 1 };
                DateTime date = DateTime.Now;
                Role role = new Role() { Id = 5 };
                StudentType studentType = new StudentType();

                StudentCategory studentCategory = new StudentCategory() { Id = 2 };
                StudentStatus studentStatus = new StudentStatus() { Id = 1 };

                
                Session session = new Session(){Id = 1};
                Semester semester = new Semester(){Id = 1};
                CourseMode courseMode = new CourseMode(){Id = 1};
                Programme programme = new Programme();
                Department department = new Department();
                Level level = new Level();

                
                string resultPath = Server.MapPath("~/Content/Result");
                string[] programmeNames = Directory.GetDirectories(resultPath);

                for (int z = 0; z < programmeNames.Length; z++)
                {
                    string programmeName = programmeNames[z].Split('\\').LastOrDefault();
                    programme = programmeLogic.GetModelsBy(p => p.Programme_Name.Trim() == programmeName.Trim()).LastOrDefault();

                    if (programme == null)
                    {
                        continue;
                    }

                    if (programme.Id == 3 || programme.Id == 4 || programme.Id == 6)
                    {
                        studentType = new StudentType() { Id = 2 };
                    }
                    else if (programme.Id == 1 || programme.Id == 2 || programme.Id == 5)
                    {
                        studentType = new StudentType() { Id = 1 };
                    }

                    string[] departmentNames = Directory.GetDirectories(programmeNames[z]);

                    for (int y = 0; y < departmentNames.Length; y++)
                    {
                        string departmentName = departmentNames[y].Split('\\').LastOrDefault();
                        department = departmentLogic.GetModelsBy(d => d.Department_Name.Trim().Replace("/", "").Replace(" ", "") == departmentName.Trim().Replace(" ", "")).LastOrDefault();

                        if (department == null)
                        {
                            continue;
                        }

                        string[] levelNames = Directory.GetFiles(departmentNames[y]);

                        for (int x = 0; x < levelNames.Length; x++)
                        {
                            string path = levelNames[x];
                            string levelName = levelNames[x].Split('\\').LastOrDefault();
                            string extractedLevelName = levelName.Split('.').FirstOrDefault();
                            level = levelLogic.GetModelBy(l => l.Level_Name == extractedLevelName);

                            if (level == null)
                            {
                                continue;
                            }

                            //string[] courseNa

                            //for (int w = 0; w < UPPER; i++)
                            //{
                                
                            //}

                            DataSet studentSet = ReadExcelComposite(path);
                            DataTable dtCompositeSheet = GetCompositeDataSet(studentSet);

                            if (dtCompositeSheet != null && dtCompositeSheet.Rows.Count > 0)
                            {

                                for (int i = 0; i < dtCompositeSheet.Rows.Count; i++)
                                {
                                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                                    {
                                        string MatricNo = dtCompositeSheet.Rows[i][1].ToString().Trim();
                                        string Name = dtCompositeSheet.Rows[i][2].ToString();
                                        int SubjectCount = dtCompositeSheet.Columns.Count - 3;

                                        if (MatricNo == "" && Name == "")
                                        {
                                            continue;
                                        }

                                        if (MatricNo == "" || MatricNo.Length < 6)
                                        {
                                            MatricNo = null;
                                        }

                                        string[] names = Name.Split(' ');

                                        Person personCheck = null;

                                        if (MatricNo != null)
                                        {
                                            personCheck = personLogic.GetModelsBy(p => p.STUDENT.Matric_Number.Trim().Replace(" ", "") == MatricNo.Trim().Replace(" ", "")).LastOrDefault();
 
                                        }
                                        
                                        Person person = new Person();
                                        Person newPerson = new Person();
                                        if (personCheck == null)
                                        {
                                            person.PersonType = personType;
                                            if (names.Count() == 1)
                                            {
                                                person.LastName = names[0];
                                                person.FirstName = names[0];
                                            }
                                            else if (names.Count() == 2)
                                            {
                                                person.LastName = names[0];
                                                person.FirstName = names[1];
                                            }
                                            else if (names.Count() == 3)
                                            {
                                                person.LastName = names[0];
                                                person.FirstName = names[1];
                                                person.OtherName = names[2];
                                            }
                                            else if (names.Count() >= 4)
                                            {
                                                person.LastName = names[0];
                                                person.FirstName = names[1];
                                                person.OtherName = names[2] + " " + names[3];
                                            }

                                            person.State = state;
                                            person.Nationality = nationality;
                                            person.DateEntered = date;
                                            person.Role = role;

                                            newPerson = personLogic.Create(person);
                                        }
                                        else
                                        {
                                            newPerson = personCheck;
                                        }

                                        Model.Model.Student studentCheck = null;

                                        if (MatricNo != null)
                                        {
                                            studentCheck = studentLogic.GetModelsBy(s => s.Matric_Number.Trim().Replace(" ", "") == MatricNo.Trim().Replace(" ", "")).LastOrDefault(); 
                                        }
                                        
                                        Model.Model.Student student = new Model.Model.Student();
                                        Model.Model.Student newStudent = new Model.Model.Student();
                                        if (studentCheck == null)
                                        {
                                            student.Id = newPerson.Id;
                                            student.Type = studentType;
                                            student.Category = studentCategory;
                                            student.Status = studentStatus;
                                            if (MatricNo != null)
                                            {
                                                student.MatricNumber = MatricNo.Replace(" ", ""); 
                                            }
                                            else
                                            {
                                                student.MatricNumber = null;
                                            }

                                            newStudent = studentLogic.CreateStudent(student);
                                        }
                                        else
                                        {
                                            newStudent = studentCheck;
                                        }
                                        
                                        StudentLevel studentLevelCheck = null;
                                        StudentLevel studentLevel = new StudentLevel();
                                        StudentLevel newStudentLevel = new StudentLevel();
                                        if (studentLevelCheck == null)
                                        {
                                            studentLevel.Student = newStudent;
                                            studentLevel.Level = level;
                                            studentLevel.Programme = programme;
                                            studentLevel.Department = department;
                                            
                                            studentLevel.Session = session;

                                            newStudentLevel = studentLevelLogic.Create(studentLevel);
                                        }
                                        else
                                        {
                                            newStudentLevel = studentLevelCheck;
                                        }

                                        List<CourseRegistrationDetail> RegDetails = new List<CourseRegistrationDetail>();

                                        for (int k = 3; k < dtCompositeSheet.Columns.Count; k++)
                                        {
                                            if (dtCompositeSheet.Rows[i][k].ToString() != "")
                                            {
                                                var CourseCode = dtCompositeSheet.Columns[k].ToString().Trim();
                                                var Grade = dtCompositeSheet.Rows[i][k].ToString().Trim();

                                                if (string.IsNullOrEmpty(CourseCode) || !gradeList.Contains(Grade))
                                                {
                                                    continue;
                                                }

                                                Course newCourse = courseLogic.GetModelsBy(c => c.Course_Code.Trim().Replace(" ", "") == CourseCode.Trim().Replace(" ", "") && c.Department_Id == department.Id && c.Level_Id == level.Id && c.Semester_Id == semester.Id).LastOrDefault();
                                                
                                                Course course = new Course();

                                                if (newCourse == null)
                                                {
                                                    newCourse = new Course();
                                                    newCourse.Activated = true;
                                                    newCourse.Code = CourseCode.Trim().Replace(" ", "");
                                                    newCourse.Department = department;
                                                    
                                                    newCourse.Level = level;
                                                    newCourse.Name = CourseCode;
                                                    newCourse.Semester = semester;
                                                    newCourse.Type = new CourseType() { Id = 1 };
                                                    newCourse.Unit = 2;

                                                    course = courseLogic.Create(newCourse);
                                                }
                                                else
                                                {
                                                    course = newCourse;
                                                }

                                                CourseRegistrationDetail regDetail = new CourseRegistrationDetail();
                                                if (Grade != "")
                                                {
                                                    regDetail.Grade = Grade.Trim();
                                                }
                                                else
                                                {
                                                    regDetail.Grade = Grade;
                                                }

                                                regDetail.Course = course;
                                                regDetail.CourseUnit = course.Unit;
                                                regDetail.Mode = courseMode;
                                                regDetail.Semester = semester;

                                                RegDetails.Add(regDetail);
                                            }
                                        }

                                        CourseRegistration courseRegistration = new CourseRegistration();
                                        CourseRegistration newCourseRegistration = new CourseRegistration();
                                       
                                        courseRegistration.Student = newStudent;
                                        courseRegistration.Level = level;
                                        courseRegistration.Programme = programme;
                                        courseRegistration.Department = department;
                                        courseRegistration.Session = session;
                                        courseRegistration.Details = RegDetails;

                                        newCourseRegistration = courseRegistrationLogic.Create(courseRegistration);

                                        using (StreamWriter log = new StreamWriter(Server.MapPath("~/Content/Result/log.txt"), true))
                                        {
                                            log.WriteLine("Inserted " + " " + Name + " " + "Matric NO.: " + MatricNo + " " + programme.Name + " " + department.Name + " " + level.Name + " " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                                        }

                                        scope.Complete();
                                    }
                                }
                            }
                        }
                    }
                }

                SetMessage("Operation Successful!", Message.Category.Information);
            }
            catch (Exception ex)
            { 
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("UploadCompositeSheeet");
        }
        public ActionResult UploadCompositeSheeetByCourseFromFolder()
        {
            try
            {
                List<StudentResultFormat> resultFormatList = new List<StudentResultFormat>();
                PersonLogic personLogic = new PersonLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                CourseLogic courseLogic = new CourseLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                LevelLogic levelLogic = new LevelLogic();
                SessionLogic sessionLogic = new SessionLogic();
                SemesterLogic semesterLogic = new SemesterLogic();
                string[] gradeList = { "A", "AB", "B", "BC", "C", "CD", "D", "E", "F" };

                string stateId = "IM";
                PersonType personType = new PersonType() { Id = 3 };
                State state = new State() { Id = stateId };
                Nationality nationality = new Nationality() { Id = 1 };
                DateTime date = DateTime.Now;
                Role role = new Role() { Id = 5 };
                StudentType studentType = new StudentType();

                StudentCategory studentCategory = new StudentCategory() { Id = 2 };
                StudentStatus studentStatus = new StudentStatus() { Id = 1 };


                Session session = new Session() { Id = 1 };
                Semester semester = new Semester() { Id = 1 };
                CourseMode courseMode = new CourseMode() { Id = 1 };
                Programme programme = new Programme();
                Department department = new Department();
                Level level = new Level();
                Course course = new Course();


                string resultPath = Server.MapPath("~/Content/Result");
                string[] programmeNames = Directory.GetDirectories(resultPath);

                for (int z = 0; z < programmeNames.Length; z++)
                {
                    string programmeName = programmeNames[z].Split('\\').LastOrDefault();
                    programme = programmeLogic.GetModelsBy(p => p.Programme_Name.Trim() == programmeName.Trim()).LastOrDefault();

                    if (programme == null)
                    {
                        continue;
                    }

                    if (programme.Id == 3 || programme.Id == 4 || programme.Id == 6)
                    {
                        studentType = new StudentType() { Id = 2 };
                    }
                    else if (programme.Id == 1 || programme.Id == 2 || programme.Id == 5)
                    {
                        studentType = new StudentType() { Id = 1 };
                    }

                    string[] departmentNames = Directory.GetDirectories(programmeNames[z]);

                    for (int y = 0; y < departmentNames.Length; y++)
                    {
                        string departmentName = departmentNames[y].Split('\\').LastOrDefault();
                        department = departmentLogic.GetModelsBy(d => d.Department_Name.Trim().Replace("/", "").Replace(" ", "") == departmentName.Trim().Replace(" ", "")).LastOrDefault();

                        if (department == null)
                        {
                            continue;
                        }

                        string[] levelNames = Directory.GetDirectories(departmentNames[y]);

                        for (int x = 0; x < levelNames.Length; x++)
                        {
                            //string path = levelNames[x];
                            string levelName = levelNames[x].Split('\\').LastOrDefault();
                            //string extractedLevelName = levelName.Split('.').FirstOrDefault();
                            level = levelLogic.GetModelBy(l => l.Level_Name.Trim() == levelName.Trim());

                            if (level == null)
                            {
                                continue;
                            }

                            string[] courseNames = Directory.GetFiles(levelNames[x]);

                            for (int w = 0; w < courseNames.Length; w++)
                            {
                                string path = courseNames[w];
                                string courseCodeExt = courseNames[w].Split('\\').LastOrDefault();
                                string courseCode = courseCodeExt.Split('.').FirstOrDefault();
                                Course newCourse = courseLogic.GetModelsBy(c => c.Course_Code.Trim().Replace(" ", "") == courseCode.Trim().Replace(" ", "") && c.Department_Id == department.Id && c.Level_Id == level.Id && c.Semester_Id == semester.Id).LastOrDefault();

                                if (newCourse == null)
                                {
                                    newCourse = new Course();
                                    newCourse.Activated = true;
                                    newCourse.Code = courseCode.Trim().Replace(" ", "");
                                    newCourse.Department = department;

                                    newCourse.Level = level;
                                    newCourse.Name = courseCode;
                                    newCourse.Semester = semester;
                                    newCourse.Type = new CourseType() { Id = 1 };
                                    newCourse.Unit = 2;

                                    course = courseLogic.Create(newCourse);
                                }
                                else
                                {
                                    course = newCourse;
                                }

                                DataSet studentSet = ReadExcel(path);
                                if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < studentSet.Tables[0].Rows.Count; i++)
                                    {
                                         using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                                         {
                                             string MatricNo = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                                             string Name = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                                             Decimal Practical = 0M;
                                             Decimal CA = 0M;
                                             Decimal Exam = 0M;
                                             if (!string.IsNullOrEmpty(studentSet.Tables[0].Rows[i][3].ToString().Trim()))
                                             {
                                                 Practical = Convert.ToDecimal(studentSet.Tables[0].Rows[i][3].ToString().Trim()); 
                                             }
                                             if (!string.IsNullOrEmpty(studentSet.Tables[0].Rows[i][4].ToString().Trim()))
                                             {
                                                CA = Convert.ToDecimal(studentSet.Tables[0].Rows[i][4].ToString().Trim());  
                                             }
                                             if (!string.IsNullOrEmpty(studentSet.Tables[0].Rows[i][5].ToString().Trim()))
                                             {
                                                Exam = Convert.ToDecimal(studentSet.Tables[0].Rows[i][5].ToString().Trim()); 
                                             } 
                                             string Grade = studentSet.Tables[0].Rows[i][6].ToString().Trim();

                                             if ((string.IsNullOrEmpty(MatricNo) && string.IsNullOrEmpty(Name)) || string.IsNullOrEmpty(Name) || !gradeList.Contains(Grade))
                                             {
                                                 continue;
                                             }

                                             if (MatricNo == "" || MatricNo.Length < 6)
                                             {
                                                 MatricNo = null;
                                             }

                                             string[] names = Name.Split(' ');

                                             Person personCheck = null;

                                             if (MatricNo != null)
                                             {
                                                 personCheck = personLogic.GetModelsBy(p => p.STUDENT.Matric_Number.Trim().Replace(" ", "") == MatricNo.Trim().Replace(" ", "")).LastOrDefault();
                                                 
                                             }
                                             if (personCheck == null)
                                             {
                                                 personCheck = personLogic.GetModelsBy(p => p.Last_Name.Trim().Replace(" ", "") + p.First_Name.Trim().Replace(" ", "") + p.Other_Name.Trim().Replace(" ", "") == Name.Trim().Replace(" ", "")).LastOrDefault();
                                                 if (personCheck != null)
                                                 {
                                                    StudentLevel checkPersonStudentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == personCheck.Id && s.Programme_Id == programme.Id && s.Department_Id == department.Id).LastOrDefault() ;
                                                    if (checkPersonStudentLevel == null)
                                                    {
                                                        personCheck = null;
                                                    }
                                                 }
                                             }

                                             Person person = new Person();
                                             Person newPerson = new Person();
                                             if (personCheck == null)
                                             {
                                                 person.PersonType = personType;
                                                 if (names.Count() == 1)
                                                 {
                                                     person.LastName = names[0];
                                                     person.FirstName = names[0];
                                                 }
                                                 else if (names.Count() == 2)
                                                 {
                                                     person.LastName = names[0];
                                                     person.FirstName = names[1];
                                                 }
                                                 else if (names.Count() == 3)
                                                 {
                                                     person.LastName = names[0];
                                                     person.FirstName = names[1];
                                                     person.OtherName = names[2];
                                                 }
                                                 else if (names.Count() >= 4)
                                                 {
                                                     person.LastName = names[0];
                                                     person.FirstName = names[1];
                                                     person.OtherName = names[2];
                                                 }

                                                 person.State = state;
                                                 person.Nationality = nationality;
                                                 person.DateEntered = date;
                                                 person.Role = role;

                                                 newPerson = personLogic.Create(person);
                                             }
                                             else
                                             {
                                                 newPerson = personCheck;
                                             }

                                             Model.Model.Student studentCheck = null;

                                             if (MatricNo != null)
                                             {
                                                 studentCheck = studentLogic.GetModelsBy(s => s.Matric_Number.Trim().Replace(" ", "") == MatricNo.Trim().Replace(" ", "")).LastOrDefault();
                                             }
                                             if (studentCheck == null)
                                             {
                                                 studentCheck = studentLogic.GetModelsBy(s => s.Person_Id == newPerson.Id).LastOrDefault();
                                             }

                                             Model.Model.Student student = new Model.Model.Student();
                                             Model.Model.Student newStudent = new Model.Model.Student();
                                             if (studentCheck == null)
                                             {
                                                 student.Id = newPerson.Id;
                                                 student.Type = studentType;
                                                 student.Category = studentCategory;
                                                 student.Status = studentStatus;
                                                 if (MatricNo != null)
                                                 {
                                                     student.MatricNumber = MatricNo.Replace(" ", "");
                                                 }
                                                 else
                                                 {
                                                     student.MatricNumber = null;
                                                 }

                                                 newStudent = studentLogic.CreateStudent(student);
                                             }
                                             else
                                             {
                                                 newStudent = studentCheck;
                                             }

                                             StudentLevel studentLevelCheck = null;
                                             studentLevelCheck = studentLevelLogic.GetModelBy(sl => sl.Person_Id == newStudent.Id);
                                             StudentLevel studentLevel = new StudentLevel();
                                             StudentLevel newStudentLevel = new StudentLevel();
                                             if (studentLevelCheck == null)
                                             {
                                                 studentLevel.Student = newStudent;
                                                 studentLevel.Level = level;
                                                 studentLevel.Programme = programme;
                                                 studentLevel.Department = department;

                                                 studentLevel.Session = session;

                                                 newStudentLevel = studentLevelLogic.Create(studentLevel);
                                             }
                                             else
                                             {
                                                 newStudentLevel = studentLevelCheck;
                                             }

                                             CourseRegistration courseRegistration = new CourseRegistration();
                                             CourseRegistration newCourseRegistration = new CourseRegistration();
                                             CourseRegistration courseRegistrationCheck = new CourseRegistration();
                                             courseRegistrationCheck = courseRegistrationLogic.GetModelsBy(c => c.Session_Id == session.Id && c.Person_Id == newStudent.Id && c.Department_Id == department.Id && c.Programme_Id == programme.Id && c.Level_Id == level.Id).LastOrDefault();
                                             if (courseRegistrationCheck == null)
                                             {
                                                 courseRegistration.Student = newStudent;
                                                 courseRegistration.Level = level;
                                                 courseRegistration.Programme = programme;
                                                 courseRegistration.Department = department;
                                                 courseRegistration.Session = session;

                                                 newCourseRegistration = courseRegistrationLogic.Create(courseRegistration);
                                             }
                                             else
                                             {
                                                 newCourseRegistration = courseRegistrationCheck;
                                             }

                                             CourseRegistrationDetail regDetail = new CourseRegistrationDetail();
                                             CourseRegistrationDetail regDetailCheck = new CourseRegistrationDetail();
                                             regDetailCheck = courseRegistrationDetailLogic.GetModelsBy(c => c.Course_Id == course.Id && c.Student_Course_Registration_Id == newCourseRegistration.Id).LastOrDefault();
                                             if (regDetailCheck == null)
                                             {
                                                 regDetail.Course = course;
                                                 regDetail.CourseUnit = course.Unit;
                                                 regDetail.Mode = courseMode;
                                                 regDetail.Semester = semester;
                                                 regDetail.PracticalScore = Practical;
                                                 regDetail.ExamScore = Exam;
                                                 regDetail.TestScore = CA;
                                                 regDetail.Grade = Grade;
                                                 regDetail.CourseRegistration = newCourseRegistration;

                                                 courseRegistrationDetailLogic.Create(regDetail); 
                                             } 

                                             using (StreamWriter log = new StreamWriter(Server.MapPath("~/Content/Result/log.txt"), true))
                                             {
                                                 log.WriteLine("Inserted " + " " + Name + " " + "Matric NO.: " + MatricNo + " " + programme.Name + " " + department.Name + " " + level.Name + " " + course.Code + " " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());
                                             }

                                             scope.Complete();
                                         }
                                    }
                                }

                            }
                        }
                    }
                }

                SetMessage("Operation Successful!", Message.Category.Information);
            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("UploadCompositeSheeet");
        }
        public ActionResult GetUploadLog()
        {
            try
            {
                return File(Server.MapPath("~/Content/Result/log.txt"), MediaTypeNames.Text.Plain, "log.txt");
            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("UploadCompositeSheeet"); ;
        }
        private DataSet ReadExcelComposite(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties='Excel 8.0;HDR=NO;'";
                OleDbConnection connection = new OleDbConnection(xConnStr);
                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Result;
        }
        private DataTable GetCompositeDataSet(DataSet studentSet)
        {
            //Remove unwanted rows and columns in dataset
            DataTable dtCompositeSheet = new DataTable();

            //Define Header properties
            List<string> headerNames = new List<string>();
            //get headers
            for (int k = 0; k < studentSet.Tables[0].Columns.Count; k++)
            {
                if (k == 0 && studentSet.Tables[0].Rows[0][k].ToString() == "")
                {
                    headerNames.Add("SN");
                    dtCompositeSheet.Columns.Add("SN");
                }
                if (studentSet.Tables[0].Rows[0][k].ToString() != "")
                {
                    headerNames.Add(studentSet.Tables[0].Rows[0][k].ToString().Trim());
                    dtCompositeSheet.Columns.Add(studentSet.Tables[0].Rows[0][k].ToString().Trim());
                }
            }

            int r = 0;
            for (int j = 1; j < studentSet.Tables[0].Rows.Count; j++)
            {
                dtCompositeSheet.Rows.Add();
                int t = 0;
                for (int k = 0; k < studentSet.Tables[0].Columns.Count; k++)
                {
                    if (studentSet.Tables[0].Rows[j][k].ToString() != "" || k < headerNames.Count)
                    {
                        var x = studentSet.Tables[0].Rows[j][k].ToString();
                        dtCompositeSheet.Rows[r][t] = studentSet.Tables[0].Rows[j][k].ToString();
                        t++;
                    }
                }
                r++;
            }
            return dtCompositeSheet;
        }

        public ActionResult SaveUploadedCompositeSheet()
        {
            try
            {
                DataTable dtCompositeSheet = (DataTable) TempData["DataTable"];
                viewModel = (StaffCourseAllocationViewModel) TempData["staffCourseAllocationViewModel"];

                PersonLogic personLogic = new PersonLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                CourseLogic courseLogic = new CourseLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                LevelLogic levelLogic = new LevelLogic();
                SessionLogic sessionLogic = new SessionLogic();
                SemesterLogic semesterLogic = new SemesterLogic();
                
                string stateId = "IM";
                PersonType personType = new PersonType() { Id = 3 };
                State state = new State() { Id = stateId };
                Nationality nationality = new Nationality() { Id = 1 };
                DateTime date = DateTime.Now;
                Role role = new Role() { Id = 5 };
                StudentType studentType = new StudentType();

                StudentCategory studentCategory = new StudentCategory() { Id = 2 };
                StudentStatus studentStatus = new StudentStatus() { Id = 1 };

                if (viewModel.Programme.Id == 3 || viewModel.Programme.Id == 4 || viewModel.Programme.Id == 6)
                {
                    studentType = new StudentType() { Id = 2 };
                }
                else if (viewModel.Programme.Id == 1 || viewModel.Programme.Id == 2 || viewModel.Programme.Id == 5)
                {
                    studentType = new StudentType() { Id = 1 };
                }

                Programme programme = programmeLogic.GetModelBy(p => p.Programme_Id == viewModel.Programme.Id);
                Department department = departmentLogic.GetModelBy(d => d.Department_Id == viewModel.Department.Id);
                DepartmentOption departmentOption = null;
                if (viewModel.DepartmentOption.Id > 0)
                {
                    departmentOption = departmentOptionLogic.GetModelBy(d => d.Department_Option_Id == viewModel.DepartmentOption.Id); 
                } 
                Session session = sessionLogic.GetModelBy(s => s.Session_Id == viewModel.Session.Id);
                Semester semester = semesterLogic.GetModelBy(s => s.Semester_Id == viewModel.Semester.Id);
                Level level = levelLogic.GetModelBy(l => l.Level_Id == viewModel.Level.Id);
                CourseMode courseMode = viewModel.CourseMode;

                List<StudentResultFormat> failedRecords = new List<StudentResultFormat>();
                string message = "Unsuccessful Records SN: "; 
                
                if (dtCompositeSheet != null && dtCompositeSheet.Rows.Count > 0)
                {
                    
                        for (int i = 0; i < dtCompositeSheet.Rows.Count; i++)
                        {
                            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                            {
                            string MatricNo = dtCompositeSheet.Rows[i][1].ToString();
                            string Name = dtCompositeSheet.Rows[i][2].ToString();
                            int SubjectCount = dtCompositeSheet.Columns.Count - 3;

                            if (MatricNo == "" && Name == "")
                            {
                                continue;
                            }

                            if (MatricNo == "")
                            {
                                //failedRecords.Add(new StudentResultFormat()
                                //{
                                //    Department = department.Name,
                                //    Level = level.Name,
                                //    MATRICNO = MatricNo,
                                //    NAME = Name,
                                //    fileUploadUrl = "",
                                //    Programme = programme.Name,
                                //    Semester = semester.Name,
                                //    Session = session.Name
                                //});

                                //continue;

                                MatricNo = null;
                            }

                            string[] names = Name.Split(' '); 

                            Person personCheck = null;
                            Person personCheck1 = null;
                            if (MatricNo != null)
                            {
                                personCheck1 = personLogic.GetModelsBy(p => p.STUDENT.Matric_Number == MatricNo.Trim()).FirstOrDefault(); 
                            }

                            personCheck = personLogic.GetModelsBy(p => (p.Last_Name + p.First_Name + p.Other_Name).Trim() == Name.Replace(" ", "").Trim()).FirstOrDefault();
                            if (personCheck == null && personCheck1 == null)
                            {
                                personCheck = FindPerson(personLogic, Name, programme, department, level); 
                            }

                            Person person = new Person();
                            Person newPerson = new Person();
                            if (personCheck == null && personCheck1 == null)
                            {
                                person.PersonType = personType;
                                if (names.Count() == 1)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[0];
                                }
                                else if (names.Count() == 2)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                }
                                else if (names.Count() == 3)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                    person.OtherName = names[2];
                                }
                                else if (names.Count() >= 4)
                                {
                                    person.LastName = names[0];
                                    person.FirstName = names[1];
                                    person.OtherName = names[2] + " " + names[3];
                                }

                                person.State = state;
                                person.Nationality = nationality;
                                person.DateEntered = date;
                                person.Role = role;

                                newPerson = personLogic.Create(person);
                            }
                            else
                            {
                                if (personCheck != null)
                                {
                                    newPerson = personCheck;
                                }
                                else
                                {
                                    newPerson = personCheck1;
                                }

                            }

                            Model.Model.Student studentCheck = null;
                            if (MatricNo == null)
                            {
                                studentCheck = studentLogic.GetModelsBy(s => s.Person_Id == newPerson.Id).FirstOrDefault();
                            }
                            else
                            {
                                studentCheck = studentLogic.GetModelsBy(s => s.Matric_Number == MatricNo.Trim() || s.Person_Id == newPerson.Id).FirstOrDefault(); 
                            } 
                            Model.Model.Student student = new Model.Model.Student();
                            Model.Model.Student newStudent = new Model.Model.Student();
                            if (studentCheck == null)
                            {
                                student.Id = newPerson.Id;
                                student.Type = studentType;
                                student.Category = studentCategory;
                                student.Status = studentStatus;
                                student.MatricNumber = MatricNo;

                                newStudent = studentLogic.CreateStudent(student);
                            }
                            else
                            {
                                newStudent = studentCheck;
                            }

                            StudentLevel studentLevelCheck = studentLevelLogic.GetModelsBy(sl => sl.Department_Id == department.Id && sl.Level_Id == level.Id && sl.Person_Id == newStudent.Id && sl.Programme_Id == programme.Id && sl.Session_Id == session.Id).FirstOrDefault();
                            StudentLevel studentLevel = new StudentLevel();
                            StudentLevel newStudentLevel = new StudentLevel();
                            if (studentLevelCheck == null)
                            {
                                studentLevel.Student = newStudent;
                                studentLevel.Level = level;
                                studentLevel.Programme = programme;
                                studentLevel.Department = department;
                                if (departmentOption != null)
                                {
                                    studentLevel.DepartmentOption = departmentOption;
                                }
                                studentLevel.Session = session;

                                newStudentLevel = studentLevelLogic.Create(studentLevel);
                            }
                            else
                            {
                                newStudentLevel = studentLevelCheck;
                            }  

                            List<CourseRegistrationDetail> RegDetails = new List<CourseRegistrationDetail>();

                            for (int k = 3; k < dtCompositeSheet.Columns.Count; k++)
                            {
                                if (dtCompositeSheet.Rows[i][k].ToString() != "")
                                {
                                    var CourseCode = dtCompositeSheet.Columns[k].ToString();
                                    var Grade = dtCompositeSheet.Rows[i][k].ToString();

                                    if (CourseCode == null || CourseCode == "" || CourseCode == " ")
                                    {
                                        continue;
                                    }

                                    Course newCourse = null;
                                    if (departmentOption != null)
                                    {
                                        newCourse = courseLogic.GetModelsBy(c => c.Course_Code.Trim().Replace(" ", "") == CourseCode.Trim().Replace(" ", "") && c.Department_Id == department.Id && c.Department_Option_Id == departmentOption.Id && c.Level_Id == level.Id && c.Semester_Id == semester.Id).FirstOrDefault(); 
                                    }
                                    else
                                    {
                                        newCourse = courseLogic.GetModelsBy(c => c.Course_Code.Trim().Replace(" ", "") == CourseCode.Trim().Replace(" ", "") && c.Department_Id == department.Id && c.Level_Id == level.Id && c.Semester_Id == semester.Id).FirstOrDefault(); 
                                    }

                                    Course course = new Course();

                                    if (newCourse == null)
                                    {
                                        newCourse = new Course();
                                        newCourse.Activated = true;
                                        newCourse.Code = CourseCode.Trim().Replace(" ", "");
                                        newCourse.Department = department;
                                        if (departmentOption != null)
                                        {
                                            newCourse.DepartmentOption = departmentOption; 
                                        }
                                        else
                                        {
                                            newCourse.DepartmentOption = null;
                                        }
                                        newCourse.Level = level;
                                        newCourse.Name = CourseCode;
                                        newCourse.Semester = semester;
                                        newCourse.Type = new CourseType() { Id = 1 };
                                        newCourse.Unit = 2;

                                        course = courseLogic.Create(newCourse);
                                    }
                                    else
                                    {
                                        course = newCourse;
                                    }

                                    CourseRegistrationDetail regDetail = new CourseRegistrationDetail();
                                    if (Grade != "")
                                    {
                                        regDetail.Grade = Grade.Trim();  
                                    }
                                    else
                                    {
                                        regDetail.Grade = Grade;
                                    }
                                    regDetail.Course = course;
                                    regDetail.CourseUnit = course.Unit;
                                    regDetail.Mode = viewModel.CourseMode;
                                    regDetail.Semester = semester;
                                    
                                    RegDetails.Add(regDetail);
                                }
                            }

                            CourseRegistration courseRegistrationCheck = courseRegistrationLogic.GetModelsBy(cr => cr.Department_Id == department.Id && cr.Level_Id == level.Id && cr.Person_Id == newStudent.Id && cr.Programme_Id == programme.Id && cr.Session_Id == session.Id).FirstOrDefault();
                            CourseRegistration courseRegistration = new CourseRegistration();
                            CourseRegistration newCourseRegistration = new CourseRegistration();
                            if (courseRegistrationCheck == null)
                            {
                                courseRegistration.Student = newStudent;
                                courseRegistration.Level = level;
                                courseRegistration.Programme = programme;
                                courseRegistration.Department = department;
                                courseRegistration.Session = session;
                                courseRegistration.Details = RegDetails;

                                newCourseRegistration = courseRegistrationLogic.Create(courseRegistration);
                            }
                            else
                            {
                                newCourseRegistration = courseRegistrationCheck;
                                for (int j = 0; j < RegDetails.Count; j++)
                                {
                                    CourseRegistrationDetail currentRegDetail = RegDetails[j];
                                    CourseRegistrationDetail regDetailExist = courseRegistrationDetailLogic.GetModelsBy(c => c.Course_Id == currentRegDetail.Course.Id && c.Semester_Id == currentRegDetail.Semester.Id && c.STUDENT_COURSE_REGISTRATION.Session_Id == newCourseRegistration.Session.Id && c.STUDENT_COURSE_REGISTRATION.Person_Id == newCourseRegistration.Student.Id && c.Student_Course_Registration_Id == newCourseRegistration.Id).FirstOrDefault();
                                    if (regDetailExist != null)
                                    {
                                        regDetailExist.Grade = currentRegDetail.Grade;
                                        courseRegistrationDetailLogic.Modify(regDetailExist);
                                    }
                                    else
                                    {
                                        currentRegDetail.CourseRegistration = newCourseRegistration;
                                        courseRegistrationDetailLogic.Create(currentRegDetail);
                                    }
                                }
                            }

                            scope.Complete();       
                        }

                        if (failedRecords.Count == 0)
                        {
                            SetMessage("Operation Successful! ", Message.Category.Information);
                        }
                        else
                        {
                            viewModel.StudentResultFormatList = failedRecords;
                            SetMessage("Done! Details of unsuccessful records can be seen below.", Message.Category.Warning);
                        }

                    }
                }
            }
            catch (Exception ex)
            {   
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return View("UploadCompositeSheeet", viewModel);
        }

        private static Person FindPerson(PersonLogic personLogic, string Name, Programme programme, Department department, Level level)
        {
            Person person = null;
            try
            {
                string[] names = Name.Trim().Split(' ');
                string LastName = "";
                string FirstName = "";
                string OtherName = "";
                if (names.Count() == 1)
                {
                    LastName = names[0].Trim();
                    FirstName = names[0].Trim();
                }
                else if (names.Count() == 2)
                {
                    LastName = names[0].Trim();
                    FirstName = names[1].Trim();
                }
                else if (names.Count() == 3)
                {
                    LastName = names[0].Trim();
                    FirstName = names[1].Trim();
                    OtherName = names[2].Trim();
                }
                else if (names.Count() >= 4)
                {
                    LastName = names[0].Trim();
                    FirstName = names[1].Trim();
                    OtherName = names[2].Trim();
                }

                List<Person> LastNameMatches = personLogic.GetModelsBy(p => p.Last_Name.Contains(LastName) || p.First_Name.Contains(LastName) || p.Other_Name.Contains(LastName));
                List<Person> SecondLevelMatches = new List<Person>();
                List<Person> TopLevelMatches = new List<Person>();
                if (FirstName != "" && FirstName != LastName)
                {
                    SecondLevelMatches = LastNameMatches.Where(p => (p.LastName + p.FirstName + p.OtherName).Contains(FirstName)).ToList();
                }
                else if(OtherName != "")
                {
                    SecondLevelMatches = LastNameMatches.Where(p => (p.LastName + p.FirstName + p.OtherName).Contains(OtherName)).ToList();
                    
                }
                if (FirstName != "" && OtherName != "")
                {
                    TopLevelMatches = LastNameMatches.Where(p => (p.LastName + p.FirstName + p.OtherName).Contains(FirstName) && (p.LastName + p.FirstName + p.OtherName).Contains(OtherName)).ToList();
                }    

                if (TopLevelMatches.Count > 0)
                {
                    person = TopLevelMatches.FirstOrDefault();
                }
                else if (SecondLevelMatches.Count > 0)
                {
                    person = SecondLevelMatches.FirstOrDefault();
                }

                if (person != null)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    List<StudentLevel> studentLevels = new List<StudentLevel>();
                    studentLevels = studentLevelLogic.GetModelsBy(s => s.Department_Id == department.Id && s.Programme_Id == programme.Id);
                    List<long> departmentalPersonIds = studentLevels.Select(s => s.Student.Id).Distinct().ToList();
                    if (!departmentalPersonIds.Contains(person.Id))
                    {
                        return null;
                    }
                    else
                    {
                        return person;
                    }
                }

            }
            catch (Exception)
            {   
                throw;
            }

            return null;
        }

    }
}