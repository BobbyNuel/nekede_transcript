﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Transactions;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class StudentCourseRegistrationController : BaseController
    {
        public const string ID = "Id";
        public const string NAME = "Name";
        // GET: Admin/StudentCourseRegistration
        public ActionResult RegisterCourse()
        {
            try
            {
                StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                return View();
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return View();
            }

        }

        public void RetainDropdownState(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                LevelLogic levelLogic = new LevelLogic();
                if (viewModel != null)
                {
                    if (viewModel.Session != null)
                    {

                        ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectList;
                    }
                    if (viewModel.Semester != null)
                    {
                        ViewBag.Semester = new SelectList(semesterLogic.GetAll(), ID, NAME, viewModel.Semester.Id);
                    }
                    else
                    {
                        ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                    }
                    if (viewModel.Programme != null)
                    {
                        ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.Programme.Id);
                    }
                    else
                    {
                        ViewBag.Programme = viewModel.ProgrammeSelectList;
                    }
                    if (viewModel.Department != null && viewModel.Programme != null)
                    {
                        ViewBag.Department = new SelectList(departmentLogic.GetBy(viewModel.Programme), ID, NAME, viewModel.Department.Id);
                    }
                    else
                    {
                        ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                    }
                    if (viewModel.Level != null)
                    {
                        ViewBag.Level = new SelectList(levelLogic.GetAll(), ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                    }
                    if (viewModel.Course != null && viewModel.Level != null && viewModel.Semester != null && viewModel.Department != null)
                    {
                        List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, viewModel.Department, viewModel.Semester);
                        ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult RegisterCourse(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                List<CourseRegistration> courseRegistrationListCount = new List<CourseRegistration>();
                int courseRegDetailCheckCount = 0;
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                List<StudentLevel> studentLevelList = new List<StudentLevel>();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                CourseMode courseMode = new CourseMode() {Id = 1};
                studentLevelList =
                    studentLevelLogic.GetModelsBy(
                        p =>
                            p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id &&
                            p.Session_Id == viewModel.Session.Id && p.Level_Id == viewModel.Level.Id);
                foreach (StudentLevel studentLevel in studentLevelList)
                {
                    List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();
                    courseRegistrationList =
                        courseRegistrationLogic.GetModelsBy(
                            p =>
                                p.Person_Id == studentLevel.Student.Id && p.Programme_Id == studentLevel.Programme.Id &&
                                p.Department_Id == studentLevel.Department.Id && p.Level_Id == studentLevel.Level.Id &&
                                p.Session_Id == studentLevel.Session.Id);
                    if (courseRegistrationList != null && courseRegistrationList.Count() > 0)
                    {
                        foreach (CourseRegistration item in courseRegistrationList)
                        {
                            CourseRegistrationDetail courseRegistrationDetailCheck = new CourseRegistrationDetail();
                            courseRegistrationDetailCheck =
                                courseRegistrationDetailLogic.GetModelBy(
                                    p =>
                                        p.Student_Course_Registration_Id == item.Id &&
                                        p.Course_Id == viewModel.Course.Id && p.Semester_Id == viewModel.Semester.Id &&
                                        p.Course_Mode_Id == 1);
                            if (courseRegistrationDetailCheck != null)
                            {
                                courseRegDetailCheckCount += 1;

                            }
                        }
                    }
                    if (courseRegDetailCheckCount == 0)
                    {
                        using (TransactionScope transaction = new TransactionScope())
                        {
                            CourseRegistration courseRegistration = new CourseRegistration();
                            courseRegistration.Student = studentLevel.Student;
                            courseRegistration.Session = studentLevel.Session;
                            courseRegistration.Programme = studentLevel.Programme;
                            courseRegistration.Department = studentLevel.Department;
                            courseRegistration.Level = studentLevel.Level;
                            courseRegistration = courseRegistrationLogic.CreateCourseRegistration(courseRegistration);
                            courseRegistrationDetail.CourseRegistration = courseRegistration;
                            courseRegistrationDetail.Course = viewModel.Course;
                            courseRegistrationDetail.Mode = courseMode;
                            courseRegistrationDetail.Semester = viewModel.Semester;
                            courseRegistrationDetail = courseRegistrationDetailLogic.Create(courseRegistrationDetail);
                            courseRegistrationListCount.Add(courseRegistration);
                            transaction.Complete();
                        }
                    }
                    courseRegDetailCheckCount = 0;
                }
                TempData["Action"] = courseRegistrationListCount.Count + " Students registered  successfully";
                return RedirectToAction("RegisterCourse", new {controller = "StudentCourseRegistration", area = "Admin"});
            }
            catch (Exception ex)
            {
                RetainDropdownState(viewModel);
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return View();
            }

        }

        public ActionResult UnRegisterCourse()
        {
            try
            {
                StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                return View();
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return View();
            }

        }

        [HttpPost]
        public ActionResult UnRegisterCourse(StudentCourseRegistrationViewModel viewModel)
        {

            try
            {
                List<CourseRegistration> courseRegistrationListCount = new List<CourseRegistration>();
                int courseRegistrationDeleteCount = 0;
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                List<StudentLevel> studentLevelList = new List<StudentLevel>();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                CourseMode courseMode = new CourseMode() {Id = 1};
                studentLevelList =
                    studentLevelLogic.GetModelsBy(
                        p =>
                            p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id &&
                            p.Session_Id == viewModel.Session.Id && p.Level_Id == viewModel.Level.Id);
                foreach (StudentLevel studentLevel in studentLevelList)
                {
                    List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();
                    courseRegistrationList =
                        courseRegistrationLogic.GetModelsBy(
                            p =>
                                p.Person_Id == studentLevel.Student.Id && p.Programme_Id == studentLevel.Programme.Id &&
                                p.Department_Id == studentLevel.Department.Id && p.Level_Id == studentLevel.Level.Id &&
                                p.Session_Id == studentLevel.Session.Id);
                    if (courseRegistrationList != null && courseRegistrationList.Count() > 0)
                    {
                        foreach (CourseRegistration item in courseRegistrationList)
                        {
                            List<CourseRegistrationDetail> courseRegistrationDetailCheckList =
                                new List<CourseRegistrationDetail>();
                            courseRegistrationDetailCheckList =
                                courseRegistrationDetailLogic.GetModelsBy(
                                    p =>
                                        p.Student_Course_Registration_Id == item.Id && p.Test_Score == 0.00M &&
                                        p.Exam_Score == 0.00M);
                            using (TransactionScope scope = new TransactionScope())
                            {
                                if (courseRegistrationDetailCheckList.Count == 0)
                                {
                                    bool isCourseRegistrationDetailDeleted =
                                        courseRegistrationDetailLogic.Delete(
                                            p =>
                                                p.Student_Course_Registration_Id == item.Id &&
                                                p.Course_Id == viewModel.Course.Id &&
                                                p.Semester_Id == viewModel.Semester.Id && p.Course_Mode_Id == 1);
                                    if (isCourseRegistrationDetailDeleted)
                                    {
                                        courseRegistrationDeleteCount += 1;
                                    }
                                    scope.Complete();
                                }

                            }


                        }
                    }


                }
                TempData["Action"] = courseRegistrationDeleteCount + " Students unregistered  successfully";
                return RedirectToAction("UnRegisterCourse",
                    new {controller = "StudentCourseRegistration", area = "Admin"});
            }
            catch (Exception ex)
            {
                RetainDropdownState(viewModel);
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return View();
            }

        }

        public ActionResult AddExtraCourse()
        {
            StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddExtraCourse(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    StudentLogic studentLogic = new StudentLogic();
                    CourseLogic courseLogic = new CourseLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();

                    List<Model.Model.Student> students =
                        studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber);
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View(viewModel);
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    List<StudentLevel> studentLevels =
                        studentLevelLogic.GetModelsBy(
                            sl =>
                                sl.Person_Id == student.Id && sl.Department_Id == viewModel.Department.Id &&
                                sl.Programme_Id == viewModel.Programme.Id);
                    if (studentLevels.Count <= 0)
                    {
                        SetMessage("Student is not in this Programme, Department!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return View(viewModel);
                    }

                    viewModel.Courses =
                        courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && (c.Activated == true || c.Activated == null));
                    viewModel.Courses.OrderBy(c => c.Level);

                    RetainDropdownState(viewModel);
                    return View(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return View(viewModel);
        }

        public ActionResult SaveAddedCourse(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    CourseMode carryOverCourseMode = new CourseMode() {Id = 2};
                    CourseMode firstAttemprCourseMode = new CourseMode() {Id = 1};

                    List<StudentLevel> studentLevelList = new List<StudentLevel>();
                    List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();

                    List<Model.Model.Student> students =
                        studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber);
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("AddExtraCourse");
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    studentLevelList =
                        studentLevelLogic.GetModelsBy(
                            p =>
                                p.Person_Id == student.Id && p.Department_Id == viewModel.Department.Id &&
                                p.Programme_Id == viewModel.Programme.Id && p.Session_Id == viewModel.Session.Id &&
                                p.Level_Id == viewModel.Level.Id);
                    StudentLevel studentLevel = studentLevelList.LastOrDefault();
                    if (studentLevelList.Count == 0)
                    {
                        SetMessage("Student has not been registered in this level for this session!",
                            Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("AddExtraCourse");
                    }

                    courseRegistrationList =
                        courseRegistrationLogic.GetModelsBy(
                            p =>
                                p.Person_Id == studentLevel.Student.Id && p.Programme_Id == studentLevel.Programme.Id &&
                                p.Department_Id == studentLevel.Department.Id && p.Level_Id == studentLevel.Level.Id &&
                                p.Session_Id == studentLevel.Session.Id);

                    if (courseRegistrationList.Count() != 1)
                    {
                        SetMessage("Student has not registered course for this session!", Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("AddExtraCourse");
                    }

                    CourseRegistration courseRegistration = courseRegistrationList.FirstOrDefault();

                    for (int i = 0; i < viewModel.Courses.Count; i++)
                    {
                        long courseId = viewModel.Courses[i].Id;
                        CourseRegistrationDetail courseRegistrationDetailCheck = courseRegistrationDetailLogic.GetModelBy(crd => crd.Course_Id == courseId && crd.Student_Course_Registration_Id == courseRegistration.Id);
                        if (courseRegistrationDetailCheck == null)
                        {
                            if (viewModel.Courses[i].IsRegistered || viewModel.Courses[i].isCarryOverCourse)
                            {
                                courseRegistrationDetail.CourseRegistration = courseRegistration;
                                courseRegistrationDetail.Course = viewModel.Courses[i];
                                if (viewModel.Courses[i].isCarryOverCourse)
                                {
                                    courseRegistrationDetail.Mode = carryOverCourseMode;
                                }
                                else
                                {
                                    courseRegistrationDetail.Mode = firstAttemprCourseMode;
                                }

                                courseRegistrationDetail.Semester = viewModel.Semester;
                                courseRegistrationDetail.CourseUnit = viewModel.Courses[i].Unit;
                                courseRegistrationDetail = courseRegistrationDetailLogic.Create(courseRegistrationDetail);

                            }
                        }


                    }

                    SetMessage("Operation Successful!", Message.Category.Information);
                    return RedirectToAction("AddExtraCourse");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View("AddExtraCourse", viewModel);
        }

        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() {Id = Convert.ToInt32(id)};
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetDepartmentOptions(string ProgId, string DeptId)
        {
            try
            {
                if (string.IsNullOrEmpty(ProgId) || string.IsNullOrEmpty(DeptId))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(ProgId) };
                Department department = new Department() { Id = Convert.ToInt32(DeptId) };
                DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                List<DepartmentOption> departmentOptions = new List<DepartmentOption>();
                
                departmentOptions = departmentOptionLogic.GetBy(department, programme);

                return Json(new SelectList(departmentOptions, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() {Id = Convert.ToInt32(id)};
                SemesterLogic semesterLogic = new SemesterLogic();
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetCourses(int[] ids)
        {
            try
            {
                if (ids.Count() == 0)
                {
                    return null;
                }
                Level level = new Level() {Id = Convert.ToInt32(ids[1])};
                Department department = new Department() {Id = Convert.ToInt32(ids[0])};
                Semester semester = new Semester() {Id = Convert.ToInt32(ids[2])};
                List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(level, department, semester);

                return Json(new SelectList(courseList, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult RegisterAll()
        {
            StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                return View(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return View(viewModel);
            }

        }

        //[HttpPost]
        //public ActionResult RegisterAll(StudentCourseRegistrationViewModel viewModel)
        //{
        //    try
        //    {
        //        CourseLogic courseLogic = new CourseLogic();
        //        CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();

        //        viewModel.CourseRegistrations = courseRegistrationLogic.GetUnregisteredStudents(viewModel.Session,
        //            viewModel.Programme, viewModel.Department, viewModel.Level);
        //        TempData["viewModel"] = viewModel;
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
        //    }

        //    RetainDropdownState(viewModel);
        //    return View(viewModel);
        //}

        public ActionResult SaveAllRegisteredStudents()
        {
            StudentCourseRegistrationViewModel viewModel = (StudentCourseRegistrationViewModel) TempData["viewModel"];
            try
            {
                CourseLogic courseLogic = new CourseLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();

                List<CourseRegistration> courseRegistrations = viewModel.CourseRegistrations;

                if (courseRegistrations != null && courseRegistrations.Count > 0)
                {
                    List<Course> SemesterCourses = courseLogic.GetBy(viewModel.Department, viewModel.Level,
                        viewModel.Semester);

                    List<CourseRegistrationDetail> courseRegistrationDetails = new List<CourseRegistrationDetail>();

                    foreach (Course SemesterCourse in SemesterCourses)
                    {
                        CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                        courseRegistrationDetail.Course = SemesterCourse;
                        courseRegistrationDetail.CourseUnit = SemesterCourse.Unit;
                        courseRegistrationDetail.Mode = new CourseMode() {Id = 1};
                        courseRegistrationDetail.Semester = viewModel.Semester;
                        courseRegistrationDetails.Add(courseRegistrationDetail);
                    }



                    if (courseRegistrationDetails.Count > 0)
                    {
                        foreach (CourseRegistration courseRegistration in courseRegistrations)
                        {
                            CourseRegistration registeredCourse = new CourseRegistration();
                            registeredCourse = courseRegistrationLogic.GetBy(courseRegistration.Student,
                                courseRegistration.Level, courseRegistration.Programme, courseRegistration.Department,
                                courseRegistration.Session);
                            if (registeredCourse == null)
                            {
                                courseRegistration.Details = courseRegistrationDetails;
                                registeredCourse = courseRegistrationLogic.Create(courseRegistration);

                            }
                        }
                    }

                    SetMessage("Operation Succesful!", Message.Category.Information);
                }

                RetainDropdownState(viewModel);
                return RedirectToAction("RegisterAll");
            }
            catch (Exception ex)
            {
                RetainDropdownState(viewModel);
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
                return RedirectToAction("RegisterAll");
            }

        }

        public ActionResult StudentsToRegister()
        {
            StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);

                return View(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult StudentsToRegister(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    List<StudentLevel> studentLevelList = new List<StudentLevel>();

                    List<CourseRegistration> courseRegistrations =
                        courseRegistrationLogic.GetModelsBy(
                            s =>
                                s.Session_Id == viewModel.Session.Id && s.Department_Id == viewModel.Department.Id &&
                                s.Programme_Id == viewModel.Programme.Id && s.Level_Id == viewModel.Level.Id);
                    List<StudentLevel> studentLevels =
                        studentLevelLogic.GetModelsBy(
                            s =>
                                s.Session_Id == viewModel.Session.Id && s.Department_Id == viewModel.Department.Id &&
                                s.Programme_Id == viewModel.Programme.Id && s.Level_Id == viewModel.Level.Id);

                    List<long> courseRegPersonIdList = courseRegistrations.Select(c => c.Student.Id).ToList();

                    for (int i = 0; i < studentLevels.Count; i++)
                    {
                        if (!courseRegPersonIdList.Contains(studentLevels[i].Student.Id))
                        {
                            string matricNumber = studentLevels[i].Student.MatricNumber;
                            string session = studentLevels[i].Session.Name;
                            string[] splitRegNumber = matricNumber.Split('/');
                            string matricYearNumber = splitRegNumber[2];
                            string[] sessionSplit = session.Split('/');
                            string sessionYear = sessionSplit[0].Substring(2, 2);
                            string prevSessionYear = (Convert.ToInt32(sessionYear) - 1).ToString();
                            string[] matricNumberYearsToPull = { sessionYear, prevSessionYear };
                            if (matricNumberYearsToPull.Contains(matricYearNumber))
                            {
                                studentLevelList.Add(studentLevels[i]);
                            }

                            //studentLevelList.Add(studentLevels[i]);
                        }
                    }

                    viewModel.StudentLevelList = studentLevelList;
                    TempData["viewModel"] = viewModel;
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return View(viewModel);
        }

        public ActionResult GetPayments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                StudentCourseRegistrationViewModel viewModel =
                    (StudentCourseRegistrationViewModel) TempData["viewModel"];
                TempData.Keep("viewModel");
                long personId = Convert.ToInt64(id);

                PaymentLogic paymentLogic = new PaymentLogic();
                PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                List<Payment> paymentList = new List<Payment>();

                List<Payment> payments = paymentLogic.GetModelsBy(p => p.Person_Id == personId);

                for (int i = 0; i < payments.Count; i++)
                {
                    Payment currentPayment = payments[i];
                    PaymentEtranzact paymentEtranzact =
                        paymentEtranzactLogic.GetModelBy(p => p.Payment_Id == currentPayment.Id);
                    RemitaPayment remitaPayment = remitaPaymentLogic.GetModelBy(r => r.Payment_Id == currentPayment.Id);
                    if (paymentEtranzact != null)
                    {
                        paymentList.Add(currentPayment);
                    }
                    if (remitaPayment != null)
                    {
                        paymentList.Add(currentPayment);
                    }
                }

                viewModel.Payments = paymentList;
                return PartialView("_StudentPayment", viewModel);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult RegisteStudent(string id)
        {
            StudentCourseRegistrationViewModel viewModel = (StudentCourseRegistrationViewModel) TempData["viewModel"];
            TempData.Keep("viewModel");
            try
            {
                long personId = Convert.ToInt64(id);
                Model.Model.Student student = new Model.Model.Student() {Id = personId};

                CourseLogic courseLogic = new CourseLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();

                Semester firstSemester = new Semester() {Id = 1};
                Semester secondSemester = new Semester() {Id = 2};

                List<Course> firstSemesterCourses = courseLogic.GetBy(viewModel.Department, viewModel.Level,
                    firstSemester);
                List<Course> secondSemesterCourses = courseLogic.GetBy(viewModel.Department, viewModel.Level,
                    secondSemester);

                List<CourseRegistrationDetail> courseRegistrationDetails = new List<CourseRegistrationDetail>();

                foreach (Course SemesterCourse in firstSemesterCourses)
                {
                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    courseRegistrationDetail.Course = SemesterCourse;
                    courseRegistrationDetail.CourseUnit = SemesterCourse.Unit;
                    courseRegistrationDetail.Mode = new CourseMode() {Id = 1};
                    courseRegistrationDetail.Semester = firstSemester;
                    courseRegistrationDetails.Add(courseRegistrationDetail);
                }

                foreach (Course SemesterCourse in secondSemesterCourses)
                {
                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    courseRegistrationDetail.Course = SemesterCourse;
                    courseRegistrationDetail.CourseUnit = SemesterCourse.Unit;
                    courseRegistrationDetail.Mode = new CourseMode() {Id = 1};
                    courseRegistrationDetail.Semester = secondSemester;
                    courseRegistrationDetails.Add(courseRegistrationDetail);
                }


                if (courseRegistrationDetails.Count > 0)
                {
                    CourseRegistration registeredCourse = new CourseRegistration();
                    registeredCourse = courseRegistrationLogic.GetBy(student, viewModel.Level, viewModel.Programme,
                        viewModel.Department, viewModel.Session);
                    if (registeredCourse == null)
                    {
                        registeredCourse = new CourseRegistration();
                        registeredCourse.Student = student;
                        registeredCourse.Department = viewModel.Department;
                        registeredCourse.Details = courseRegistrationDetails;
                        registeredCourse.Level = viewModel.Level;
                        registeredCourse.Programme = viewModel.Programme;
                        registeredCourse.Session = viewModel.Session;
                        courseRegistrationLogic.Create(registeredCourse);
                    }
                }

                return Json(new {result = "Success"});
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult StudentDetails()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StudentDetails(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                List<Model.Model.Student> students = new List<Model.Model.Student>();
                students = studentLogic.GetModelsBy(x => x.Matric_Number == viewModel.Student.MatricNumber);

                if (students.Count == 0)
                {
                    students = studentLogic.GetModelsBy(x => x.PERSON.Last_Name + x.PERSON.First_Name + x.PERSON.Other_Name == viewModel.Student.MatricNumber);
                }

                Model.Model.Student myStudent = new Model.Model.Student();
                if (students.Count > 1)
                {
                    SetMessage("Matric Number is duplicate", Message.Category.Error);
                    return View();
                }
                if (students.Count == 0)
                {
                    Model.Model.Student appliedStudent = studentLogic.GetModelsBy(s => s.APPLICATION_FORM.Application_Form_Number == viewModel.Student.MatricNumber).LastOrDefault();
                    if (appliedStudent == null)
                    {
                        SetMessage("No record found", Message.Category.Error);
                        return View();
                    }
                    else
                    {
                        myStudent = appliedStudent;
                    }
                } 
                
                if (students.Count == 1)
                {
                    myStudent = students.FirstOrDefault(); 
                }
                
                List<CourseRegistration> courseRegistrationlist = courseRegistrationLogic.GetModelsBy(x => x.Person_Id == myStudent.Id);

                for (int i = 0; i < courseRegistrationlist.Count; i++)
                {
                    long courseRegId = courseRegistrationlist[i].Id;
                    List<CourseRegistrationDetail> courseRegistrationDetaillist = courseRegistrationDetailLogic.GetModelsBy(x => x.Student_Course_Registration_Id == courseRegId);
                    if (courseRegistrationDetaillist.Count != 0)
                    {
                        courseRegistrationlist[i].Details = courseRegistrationDetaillist.OrderBy(c => c.Semester.Id).ToList();
                    }
                }
                

                viewModel.StudentLevelList = studentLevelLogic.GetModelsBy(s => s.STUDENT.Person_Id == myStudent.Id);
                viewModel.Student = myStudent;
                viewModel.CourseRegistrations = courseRegistrationlist;

                return View(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("StudentDetails");
        }
        public JsonResult DeleteStudentLevel(string id)
        {   
            try
            {
                long studentLevelId = Convert.ToInt64(id);
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();

                studentLevelLogic.Delete(s => s.Student_Level_Id == studentLevelId);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult DeleteCourseRegistration(string id)
        {
            try
            {
                long courseRegId = Convert.ToInt64(id);
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                courseRegistrationDetailLogic.Delete(c => c.Student_Course_Registration_Id == courseRegId);
                courseRegistrationLogic.Delete(c => c.Student_Course_Registration_Id == courseRegId);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult RemoveCourse(string id)
        {
            try
            {
                long courseRegdetailId = Convert.ToInt64(id);
                CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                courseRegistrationDetailLogic.Delete(s => s.Student_Course_Registration_Detail_Id == courseRegdetailId);
                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult EditStudentLevel(int sid)
        {
            StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
            try
            {
                if (sid > 0)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelBy(sl => sl.Student_Level_Id == sid);

                    viewModel.StudentLevel = studentLevel;
                    ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME, studentLevel.Level.Id);
                    ViewBag.Session = viewModel.SessionSelectList;
                    ViewBag.Programme = viewModel.ProgrammeSelectList;
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = departmentLogic.GetBy(studentLevel.Programme);
                    ViewBag.Department = new SelectList(departments, "Id", "Name", studentLevel.Department.Id);
                    
                    if (studentLevel.DepartmentOption != null)
                    {
                        DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                        List<DepartmentOption> options = departmentOptionLogic.GetBy(studentLevel.Department, studentLevel.Programme);

                        ViewBag.DepartmentOption = new SelectList(options, "Id", "Name", studentLevel.DepartmentOption.Id);  
                    }
                    else
                    {
                        ViewBag.DepartmentOption = new SelectList(new List<DepartmentOption>(), "Id", "Name"); 
                    }  
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditStudentLevel(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel.StudentLevel != null)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelBy(sl => sl.Student_Level_Id == viewModel.StudentLevel.Id);

                    studentLevel.Session = viewModel.StudentLevel.Session;
                    studentLevel.Level = viewModel.StudentLevel.Level;
                    studentLevel.Programme = viewModel.StudentLevel.Programme;
                    if (viewModel.OptionId > 0)
                    {
                       DepartmentOption departmentOption = new DepartmentOption(){Id = viewModel.OptionId};
                       studentLevel.DepartmentOption = departmentOption;
                    }
                    
                    studentLevel.Department = viewModel.StudentLevel.Department;

                    studentLevelLogic.Modify(studentLevel, studentLevel.Id);

                    SetMessage("Operation Successful! ", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("StudentDetails");
        }
        public ActionResult EditCourseRegistration(int cid)
        {
            StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
            try
            {
                if (cid > 0)
                {
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                    CourseRegistration courseRegistration = courseRegistrationLogic.GetModelBy(c => c.Student_Course_Registration_Id == cid);
                    List<CourseRegistrationDetail> courseRegistrationDetails = courseRegistrationDetailLogic.GetModelsBy(c => c.Student_Course_Registration_Id == cid);

                    viewModel.CourseRegistration = courseRegistration;
                    viewModel.CourseRegistration.Details = courseRegistrationDetails;

                    ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME, courseRegistration.Level.Id);
                    ViewBag.Session = viewModel.SessionSelectList;
                    ViewBag.Programme = viewModel.ProgrammeSelectList;
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = departmentLogic.GetBy(courseRegistration.Programme);
                    ViewBag.Department = new SelectList(departments, "Id", "Name", courseRegistration.Department.Id);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditCourseRegistration(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel.CourseRegistration != null)
                {
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    PersonLogic personLogic = new PersonLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    CourseRegistration courseRegistration = courseRegistrationLogic.GetModelBy(c => c.Student_Course_Registration_Id == viewModel.CourseRegistration.Id);

                    if (viewModel.Student.LastName == null && viewModel.Student.FirstName == null)
                    {
                        courseRegistration.Session = viewModel.CourseRegistration.Session;
                        courseRegistration.Level = viewModel.CourseRegistration.Level;
                        courseRegistration.Programme = viewModel.CourseRegistration.Programme;
                        courseRegistration.Department = viewModel.CourseRegistration.Department;
                        courseRegistration.Details = viewModel.CourseRegistration.Details;

                        courseRegistrationLogic.ModifyRegOnly(courseRegistration); 
                    }
                    else
                    {
                        
                    }

                    SetMessage("Operation Successful! ", Message.Category.Information);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("StudentDetails");
        }

        public ActionResult SearchAndMergeDuplicateResult()
        {
            StudentCourseRegistrationViewModel viewModel = null;
            try
            {
                viewModel = new StudentCourseRegistrationViewModel();

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult SearchAndMergeDuplicateResult(StudentCourseRegistrationViewModel viewModel)
        {
            try
            {
                if (viewModel.Student.MatricNumber != null)
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    List<StudentLevel> studentLevels = new List<StudentLevel>();

                    studentLevels = studentLevelLogic.GetModelsBy(s => s.STUDENT.Matric_Number == viewModel.Student.MatricNumber);

                    if (studentLevels.Count <= 0)
                    {
                        studentLevels = studentLevelLogic.GetModelsBy(s => (s.STUDENT.PERSON.Last_Name + s.STUDENT.PERSON.First_Name + s.STUDENT.PERSON.Other_Name) == viewModel.Student.MatricNumber);

                    }

                    viewModel.StudentLevelList = studentLevels;
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            TempData["ViewModel"] = viewModel;
            return View(viewModel);
        }

        public JsonResult MergeResult()
        {
            try
            {
                StudentCourseRegistrationViewModel viewModel = (StudentCourseRegistrationViewModel) TempData["ViewModel"];
                List<StudentLevel> studentLevels = viewModel.StudentLevelList;

                StudentLevel firstStudentLevel = studentLevels.FirstOrDefault();
                long firstPersonId = studentLevels.FirstOrDefault().Student.Id;

                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLogic studentLogic = new StudentLogic();
                PersonLogic personLogic = new PersonLogic();

                for (int i = 0; i < studentLevels.Count; i++)
                {
                    StudentLevel currentLevel = studentLevels[i];

                    //CourseRegistration courseRegistration = courseRegistrationLogic.GetModelsBy()
                }

                return Json(new { result = "Success" });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public ActionResult ViewConfirmedpayments()
        //{
        //    StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
        //    try
        //    {
               
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    return View(viewModel);
        //}

        //[HttpPost]
        //public ActionResult ViewConfirmedpayments(StudentCourseRegistrationViewModel viewModel)
        //{
        //    try
        //    {
        //        if (viewModel.Student.MatricNumber != null)
        //        {
        //            StudentLogic studentLogic = new StudentLogic();
        //            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();

        //            Model.Model.Student student = studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber).LastOrDefault();
        //            if (student == null)
        //            {
        //                SetMessage("Error! Student does not exist", Message.Category.Error); 
        //            }

        //            viewModel.PaymentEtranzacts = paymentEtranzactLogic.GetModelsBy(p => p.ONLINE_PAYMENT.PAYMENT.Person_Id == student.Id);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    return View(viewModel);
        //}
        //public ActionResult EditConfirmedPayment(int pid)
        //{   
        //    StudentCourseRegistrationViewModel viewModel = new StudentCourseRegistrationViewModel();
        //    try
        //    {
        //        if (pid > 0)
        //        {
        //            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
        //            viewModel.PaymentEtranzact = paymentEtranzactLogic.GetModelBy(p => p.Payment_Id == pid);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    return View(viewModel);
        //}
        //[HttpPost]
        //public ActionResult EditConfirmedPayment(StudentCourseRegistrationViewModel viewModel)
        //{
        //    try
        //    {
        //        if (viewModel.PaymentEtranzact != null)
        //        {
        //            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
        //            viewModel.PaymentEtranzact = paymentEtranzactLogic.GetModelBy(p => p.ONLINE_PAYMENT.PAYMENT.Payment_Id == viewModel.PaymentEtranzact.Payment.Payment.Id);
        //            viewModel.PaymentEtranzact.TransactionAmount = Convert.ToDecimal(viewModel.Amount);

        //            paymentEtranzactLogic.Modify(viewModel.PaymentEtranzact);
        //            SetMessage("Operation Successful! ", Message.Category.Information);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error! " + ex.Message, Message.Category.Error);
        //    }

        //    return RedirectToAction("ViewConfirmedpayments");
        //}
    }
}