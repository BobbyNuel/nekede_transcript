﻿using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class StaffCourseAllocationController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private StaffCourseAllocationViewModel viewModel;
        private string FileUploadURL = null;
        public ActionResult AllocatedCourses()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AllocatedCourses(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    viewModel.CourseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.CourseAllocation.Level.Id && p.Programme_Id == viewModel.CourseAllocation.Programme.Id && p.Session_Id == viewModel.CourseAllocation.Session.Id);
                }

                KeepCourseDropDownState(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }

            return View("AllocatedCourses", viewModel);
        }
        public ActionResult AllocateCourse()
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.User = viewModel.UserSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult AllocateCourse(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    CourseAllocation courseAllocationList = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.CourseAllocation.Level.Id && p.Programme_Id == viewModel.CourseAllocation.Programme.Id  && p.Session_Id == viewModel.CourseAllocation.Session.Id && p.Course_Id == viewModel.CourseAllocation.Course.Id && p.Department_Id == viewModel.CourseAllocation.Department.Id).FirstOrDefault();
                    if (courseAllocationList == null && viewModel.CourseAllocation.Course != null && viewModel.CourseAllocation.Department != null && viewModel.CourseAllocation.Level != null && viewModel.CourseAllocation.Programme != null  && viewModel.CourseAllocation.Session != null && viewModel.CourseAllocation.User != null)
                    {
                        courseAllocationLogic.Create(viewModel.CourseAllocation);
                        SetMessage("Course Allocated", Message.Category.Information);
                        KeepCourseDropDownState(viewModel);
                        return View(viewModel);
                    }
                    else
                    {
                        courseAllocationList.User.Id = viewModel.CourseAllocation.User.Id;
                        courseAllocationLogic.Modify(courseAllocationList);
                        SetMessage("Course allocation updated", Message.Category.Error);
                        KeepCourseDropDownState(viewModel);
                        return View("AllocateCourse");
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error: " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        public ActionResult DownloadResultSheet()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult DownloadResultSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    CourseAllocation courseAllocation = new CourseAllocation();
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id && p.Department_Id == viewModel.Department.Id && p.Level_Id == viewModel.Level.Id && p.Programme_Id == viewModel.Programme.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                    if (courseAllocation == null)
                    {
                        if (!User.IsInRole("Record"))
                        {
                            SetMessage("You are not allocated to this course, with this Programme-Department combination", Message.Category.Error);
                            KeepDropDownState(viewModel);
                            return RedirectToAction("DownloadResultSheet");                            
                        }
                    }
                    GridView gv = new GridView();

                    DataTable ds = new DataTable();
                    List<ResultFormat> resultFormatList = new List<ResultFormat>();

                    SessionLogic sessionLogic = new SessionLogic();
                    Session session = viewModel.Session;
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                    List<CourseRegistrationDetail> courseRegistrationDetailList = new List<CourseRegistrationDetail>();
                    List<CourseRegistration> courseRegistrationList = new List<CourseRegistration>();
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    courseRegistrationList = courseRegistrationLogic.GetModelsBy(p => p.Session_Id == session.Id && p.Level_Id == viewModel.Level.Id && p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id);
                    if (viewModel.Course != null && viewModel.Semester != null && courseRegistrationList.Count > 0)
                    {
                        int count = 1;
                        foreach (CourseRegistration courseRegistration in courseRegistrationList)
                        {
                            courseRegistrationDetailList = courseRegistrationDetailLogic.GetModelsBy(p => p.Course_Id == viewModel.Course.Id && p.Semester_Id == viewModel.Semester.Id && p.Student_Course_Registration_Id == courseRegistration.Id);
                            if (courseRegistrationDetailList.Count > 0)
                            {

                                foreach (CourseRegistrationDetail courseRegistrationDetailItem in courseRegistrationDetailList)
                                {
                                    ResultFormat resultFormat = new ResultFormat();
                                    // resultFormat.SN = count;
                                    resultFormat.MATRICNO = courseRegistrationDetailItem.CourseRegistration.Student.MatricNumber;
                                    resultFormatList.Add(resultFormat);
                                    count++;
                                }
                            }
                        }
                    }

                    if (resultFormatList.Count > 0)
                    {
                        List<ResultFormat> list = resultFormatList.Where(p => p.MATRICNO != null).OrderBy(p => p.MATRICNO).ToList();
                        List<ResultFormat> sort = new List<ResultFormat>();
                        for (int i = 0; i < list.Count; i++)
                        {
                            list[i].SN = (i + 1);
                            sort.Add(list[i]);
                        }

                        gv.DataSource = sort;// resultFormatList.OrderBy(p => p.MATRICNO);
                        CourseLogic courseLogic = new CourseLogic();
                        Course course = courseLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id);
                        gv.Caption = course.Name.ToUpper() + " " +course.Code + " " + " DEPARTMENT OF " + " " + course.Department.Name.ToUpper() + " " + course.Unit + " " + "Units";
                        gv.DataBind();

                        string filename = course.Code.Replace("/", "").Replace("\\", "") + course.Department.Code + ".xls";
                        return new DownloadFileActionResult(gv, filename);
                    }
                    else
                    {
                        Response.Write("No data available for download");
                        Response.End();
                        return new JavaScriptResult();
                    }
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Error);

                    KeepDropDownState(viewModel);

                    return RedirectToAction("DownloadResultSheet");
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }


            return RedirectToAction("DownloadResultSheet");
        }
        public ActionResult UploadResultSheet()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);

            }
            return View();
        }
        [HttpPost]
        public ActionResult UploadResultSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                //courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id && p.Department_Id == viewModel.Department.Id && p.Level_Id == viewModel.Level.Id && p.Programme_Id == viewModel.Programme.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                //if (courseAllocation == null)
                //{
                //    if (!User.IsInRole("Admin"))
                //    {
                //        SetMessage("You are not allocated to this course, with this Programme-Department combination", Message.Category.Error);
                //        RetainDropdownState(viewModel);
                //        return View(viewModel);
                //    }
                //}
                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcel(savedFileName);

                    if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 1; i < studentSet.Tables[0].Rows.Count; i++)
                        {
                            ResultFormat resultFormat = new ResultFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.MATRICNO = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.NAME = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                            resultFormat.T_EX = Convert.ToDecimal(studentSet.Tables[0].Rows[i][11].ToString().Trim());
                            resultFormat.T_CA = Convert.ToDecimal(studentSet.Tables[0].Rows[i][12].ToString().Trim());

                            if (resultFormat.MATRICNO != "")
                            {
                                resultFormatList.Add(resultFormat);
                            }

                        }
                        resultFormatList.OrderBy(p => p.MATRICNO);
                        viewModel.resultFormatList = resultFormatList;
                        TempData["staffCourseAllocationViewModel"] = viewModel;

                    }

                }
            }
            catch (Exception ex)
            {

                SetMessage("Error occured! " + ex.Message, Message.Category.Error);
            }
            RetainDropdownState(viewModel);
            return View(viewModel);
        }
        public ActionResult SaveUploadedResultSheet()
        {
            StaffCourseAllocationViewModel viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
            try
            {
                if (viewModel != null)
                {

                    int status = validateFields(viewModel.resultFormatList);

                    if (status > 0)
                    {
                        ResultFormat format = viewModel.resultFormatList.ElementAt((status - 1));
                        SetMessage("Validation Error for" + " " + format.MATRICNO, Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("StaffResultSheet");
                    }

                    bool resultAdditionStatus = addStudentResult(viewModel);

                    SetMessage("Upload successful", Message.Category.Information);
                }


            }
            catch (Exception ex)
            {
                SetMessage("Error occured " + ex.Message, Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return RedirectToAction("StaffResultSheet");
        }
        public ActionResult DownloadAttendanceSheet()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);

            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }
            return View();
        }
        [HttpPost]
        public ActionResult AttendanceReport(StaffCourseAllocationViewModel viewModel)
        {
            CourseAllocation courseAllocation = new CourseAllocation();
            CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
            // courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Id == viewModel.Course.Id && p.Department_Id == viewModel.Department.Id && p.Level_Id == viewModel.Level.Id && p.Programme_Id == viewModel.Programme.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
            //if (courseAllocation == null)
            //{
            //if (!User.IsInRole("Admin"))
            //{
            //    SetMessage("You are not allocated to this course, with this Programme-Department combination", Message.Category.Error);
            //    KeepDropDownState(viewModel);
            //    return RedirectToAction("DownloadResultSheet");                    
            //}
            //}

            ViewBag.SessionId = viewModel.Session.Id.ToString();
            ViewBag.SemesterId = viewModel.Semester.Id.ToString();
            ViewBag.ProgrammeId = viewModel.Programme.Id.ToString();
            ViewBag.DepartmentId = viewModel.Department.Id.ToString();
            ViewBag.LevelId = viewModel.Level.Id.ToString();
            ViewBag.CourseId = viewModel.Course.Id.ToString();

            return View();
        }
        public ActionResult StaffResultSheet()
        {
            StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
            ViewBag.Session = viewModel.SessionSelectList;
            ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
            ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult StaffResultSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.COURSE.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                KeepDropDownState(viewModel);
                TempData["vModel"] = viewModel;
            }
            catch (Exception)
            {

                throw;
            }
            return View(viewModel);
        }

        public ActionResult ResultUploadSheet(long cid)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Allocation_Id == cid);
                if (courseAllocation == null)
                {
                    SetMessage("You are not allocated to this course, with this programme-department combination", Message.Category.Error);
                    return RedirectToAction("StaffResultSheet");
                }
                GridView gv = new GridView();
                DataTable ds = new DataTable();
                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                resultFormatList = courseRegistrationLogic.GetDownloadResultFormats(courseAllocation);
                if (resultFormatList.Count > 0)
                {
                    List<ResultFormat> list = resultFormatList.Where(p => p.MATRICNO != null).OrderBy(p => p.MATRICNO).ToList();
                    List<ResultFormat> sort = new List<ResultFormat>();
                    
                    for (int i = 0; i < list.Count; i++)
                    {
                        list[i].SN = (i + 1); 
                        sort.Add(list[i]);
                    }

                    gv.DataSource = sort;// resultFormatList.OrderBy(p => p.MATRICNO);
                    CourseLogic courseLogic = new CourseLogic();
                    Course course = courseLogic.GetModelBy(p => p.Course_Id == courseAllocation.Course.Id);
                    gv.Caption = course.Name + " "+ course.Code + " " + " DEPARTMENT OF " + " " + course.Department.Name.ToUpper() + " " + course.Unit + " " + "Units";
                   
                    gv.DataBind();

                    string filename = course.Code.Replace("/", "").Replace("\\", "") + course.Department.Code + ".xls";
                    return new DownloadFileActionResult(gv, filename);
                }
                else
                {
                    Response.Write("No data available for download");
                    Response.End();
                    return new JavaScriptResult();
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error occured! " + ex.Message, Message.Category.Error);
            }
            return RedirectToAction("StaffResultSheet");
        }
        [HttpPost]
        public ActionResult ResultUploadSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Allocation_Id == viewModel.cid);
                CourseLogic courseLogic = new CourseLogic();
                Course course = courseLogic.GetModelBy(p => p.Course_Id == courseAllocation.Course.Id);
                if (courseAllocation == null)
                {
                    SetMessage("You are not allocated to this course, with this programme-department combination", Message.Category.Error);
                    return RedirectToAction("StaffResultSheet");
                }
                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    FileUploadURL = savedFileName;
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcel(savedFileName);

                    if (studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 1; i < studentSet.Tables[0].Rows.Count; i++)
                        {
                            ResultFormat resultFormat = new ResultFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.MATRICNO = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.NAME = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                            resultFormat.T_EX = Convert.ToDecimal(studentSet.Tables[0].Rows[i][3].ToString().Trim());
                            resultFormat.T_CA = Convert.ToDecimal(studentSet.Tables[0].Rows[i][4].ToString().Trim());
                            resultFormat.fileUploadUrl = studentSet.Tables[0].Rows[i][5].ToString().Trim();
                            if (resultFormat.MATRICNO != "")
                            {
                                resultFormatList.Add(resultFormat);
                            }

                        }
                        resultFormatList.OrderBy(p => p.MATRICNO);
                        viewModel.resultFormatList = resultFormatList;
                        viewModel.Course = course;
                        StaffCourseAllocationViewModel vModel = (StaffCourseAllocationViewModel)TempData["vModel"];
                        vModel.Course = course;
                        vModel.resultFormatList = resultFormatList;
                        vModel.CourseAllocation = courseAllocation;
                        TempData["staffCourseAllocationViewModel"] = vModel;

                    }

                }
            }
            catch (Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message, Message.Category.Error);
            }
            RetainDropdownState(viewModel);
            return View(viewModel);
        }
        public ActionResult DownloadZip(string downloadName)
        {
            TempData["downloadName"] = downloadName + ".zip";
            return View();
        }
        public ActionResult GetZip()
        {
            try
            {
                string downloadName = (string)TempData["downloadName"];
                TempData.Keep("downloadName");
                string path = "~/Content/temp/" + downloadName;
                Response.Redirect(path, false);
            }
            catch (Exception ex)
            {
                SetMessage("Error, " + ex.Message, Message.Category.Error);
            }

            return View("DownloadZip");
        }
        private bool addStudentResult(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
                SessionSemesterLogic sessionSemesterLogicc = new SessionSemesterLogic();
                SessionSemester sessionSemester = new SessionSemester();
                UserLogic userLogic = new UserLogic();
                StudentLogic studentLogic = new StudentLogic();
                StudentResultType testResultType = new StudentResultType() { Id = 1 };
                StudentResultType examResultType = new StudentResultType() { Id = 2 };
                User user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                sessionSemester = sessionSemesterLogicc.GetModelBy(p => p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id);
                if (viewModel != null && viewModel.resultFormatList.Count > 0)
                {
                    CourseRegistration courseRegistration = new CourseRegistration();
                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();
                    CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                    StudentResultDetail studentResultDetailTest;
                    StudentResultDetail studentResultDetailExam;
                    List<StudentResultDetail> studentResultDetailTestList;
                    List<StudentResultDetail> studentResultDetailExamList;
                    StudentResultLogic studentResultLogic;
                    StudentResult studentResultTest;
                    StudentResult studentResultExam;
                    StudentExamRawScoreSheet studentExamRawScoreSheet = new StudentExamRawScoreSheet();
                    StudentExamRawScoreSheetResultLogic StudentExamRawScoreSheetLogic = new StudentExamRawScoreSheetResultLogic();

                    if(!User.IsInRole("Teacher"))
                    {
                        viewModel.CourseAllocation = courseAllocationLogic.GetModelsBy(c => c.Course_Id == viewModel.Course.Id && c.Session_Id == viewModel.Session.Id).LastOrDefault();
                    }                     

                    StudentResultDetailLogic studentResultDetailLogic = new StudentResultDetailLogic();
                    using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new System.TimeSpan(0, 15, 0)))
                    {
                        foreach (ResultFormat resultFormat in viewModel.resultFormatList)
                        {
                            InitializeStudentResult(viewModel,resultFormat.fileUploadUrl ,sessionSemester, testResultType, examResultType, user, out studentResultDetailTest, out studentResultDetailExam, out studentResultDetailTestList, out studentResultDetailExamList, out studentResultLogic, out studentResultTest, out studentResultExam);
                            studentResultDetailTest.Course = viewModel.Course;

                            courseRegistration =
                                courseRegistrationLogic.GetModelsBy(
                                    c => c.STUDENT.Matric_Number == resultFormat.MATRICNO.Trim() && c.Session_Id == viewModel.Session.Id).LastOrDefault();
                            if (courseRegistration != null)
                            {
                                  studentResultDetailTest.Student = studentLogic.GetModelBy(p => p.Matric_Number == resultFormat.MATRICNO.Trim() && p.Person_Id == courseRegistration.Student.Id);
                                    if (studentResultDetailTest.Student != null)
                                    {
                                        studentResultDetailTest.Score = resultFormat.T_CA;
                                        studentResultDetailTest.SpecialCaseMessage = resultFormat.ResultSpecialCaseMessages.TestSpecialCaseMessage;
                                        studentResultDetailTestList.Add(studentResultDetailTest);
                                        studentResultTest.Results = studentResultDetailTestList;
                                        studentResultLogic.Add(studentResultTest);

                                        studentResultDetailExam.Course = viewModel.Course;
                                        studentResultDetailExam.Student = studentResultDetailTest.Student;
                                        studentResultDetailExam.Score = resultFormat.T_EX;
                                        studentResultDetailExam.SpecialCaseMessage = resultFormat.ResultSpecialCaseMessages.ExamSpecialCaseMessage;
                                        studentResultDetailExamList.Add(studentResultDetailExam);
                                        studentResultExam.Results = studentResultDetailExamList;
                                        studentResultLogic.Add(studentResultExam);

                                        studentExamRawScoreSheet = StudentExamRawScoreSheetLogic.GetModelBy(p => p.Student_Id == studentResultDetailExam.Student.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Course_Id == viewModel.Course.Id);
                                        List<StudentExamRawScoreSheet> a =StudentExamRawScoreSheetLogic.GetModelsBy(p => p.Student_Id == studentResultDetailExam.Student.Id && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Course_Id == viewModel.Course.Id);
                                        if (a.Count > 1)
                                        {
                                            Response.Write("o");
                                        }
                                        
                                        
                                        if (studentExamRawScoreSheet == null)
                                        {
                                            studentExamRawScoreSheet = CreateExamRawScoreSheet(viewModel,user, studentResultDetailTest, studentExamRawScoreSheet, StudentExamRawScoreSheetLogic, resultFormat);
                                        }
                                        else
                                        {
                                            ModifyExamRawScoreSheet(viewModel, user, studentResultDetailTest, studentExamRawScoreSheet, StudentExamRawScoreSheetLogic, resultFormat);
                                        }
                                    }
                            
                            }
                          
                        
                        }
                        scope.Complete();
                    }


                }

                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        private static void ModifyExamRawScoreSheet(StaffCourseAllocationViewModel viewModel, User user, StudentResultDetail studentResultDetailTest, StudentExamRawScoreSheet studentExamRawScoreSheet, StudentExamRawScoreSheetResultLogic StudentExamRawScoreSheetLogic, ResultFormat resultFormat)
        {
            studentExamRawScoreSheet.Course = viewModel.Course;
            studentExamRawScoreSheet.Level = viewModel.Level;
            studentExamRawScoreSheet.T_CA = (double)resultFormat.T_CA;
            studentExamRawScoreSheet.T_EX = (double)resultFormat.T_EX;
            studentExamRawScoreSheet.Semester = viewModel.Semester;
            studentExamRawScoreSheet.Session = viewModel.Session;
            studentExamRawScoreSheet.Special_Case = resultFormat.ResultSpecialCaseMessages.SpecialCaseMessage;
            studentExamRawScoreSheet.Student = studentResultDetailTest.Student;
            studentExamRawScoreSheet.Uploader = user;
            bool isScoreSheetModified = StudentExamRawScoreSheetLogic.Modify(studentExamRawScoreSheet);
        }
        private static StudentExamRawScoreSheet CreateExamRawScoreSheet(StaffCourseAllocationViewModel viewModel, User user, StudentResultDetail studentResultDetailTest, StudentExamRawScoreSheet studentExamRawScoreSheet, StudentExamRawScoreSheetResultLogic StudentExamRawScoreSheetLogic, ResultFormat resultFormat)
        {
            studentExamRawScoreSheet = new StudentExamRawScoreSheet();
            studentExamRawScoreSheet.Course = viewModel.Course;
            studentExamRawScoreSheet.Level = viewModel.Level;
            studentExamRawScoreSheet.T_CA = (double)resultFormat.T_CA;
            studentExamRawScoreSheet.T_EX = (double)resultFormat.T_EX;
            studentExamRawScoreSheet.Semester = viewModel.Semester;
            studentExamRawScoreSheet.Session = viewModel.Session;
            studentExamRawScoreSheet.Special_Case = resultFormat.ResultSpecialCaseMessages.SpecialCaseMessage;
            studentExamRawScoreSheet.Student = studentResultDetailTest.Student;
            studentExamRawScoreSheet.Uploader = user;
            StudentExamRawScoreSheetLogic.Create(studentExamRawScoreSheet);
            return studentExamRawScoreSheet;
        }
        private static void InitializeStudentResult(StaffCourseAllocationViewModel viewModel,string FileUploadURL, SessionSemester sessionSemester, StudentResultType testResultType, StudentResultType examResultType, User user, out StudentResultDetail studentResultDetailTest, out StudentResultDetail studentResultDetailExam, out List<StudentResultDetail> studentResultDetailTestList, out List<StudentResultDetail> studentResultDetailExamList, out StudentResultLogic studentResultLogic, out StudentResult studentResultTest, out StudentResult studentResultExam)
        {
            studentResultDetailTest = new StudentResultDetail();
            studentResultDetailExam = new StudentResultDetail();
            studentResultDetailTestList = new List<StudentResultDetail>();
            studentResultDetailExamList = new List<StudentResultDetail>();
            studentResultLogic = new StudentResultLogic();
            studentResultTest = new StudentResult();
            studentResultExam = new StudentResult();

            studentResultTest.MaximumObtainableScore = 30;
            studentResultTest.DateUploaded = DateTime.Now;
            studentResultTest.Department = viewModel.CourseAllocation.Department;
            studentResultTest.Level = viewModel.CourseAllocation.Level;
            studentResultTest.Programme = viewModel.CourseAllocation.Programme;
            studentResultTest.SessionSemester = sessionSemester;
            studentResultTest.UploadedFileUrl = FileUploadURL;
            studentResultTest.Uploader = user;
            studentResultTest.Type = testResultType;

            studentResultExam.MaximumObtainableScore = 70;
            studentResultExam.DateUploaded = DateTime.Now;
            studentResultExam.Department = viewModel.CourseAllocation.Department;
            studentResultExam.Level = viewModel.CourseAllocation.Level;
            studentResultExam.Programme = viewModel.CourseAllocation.Programme;
            studentResultExam.SessionSemester = sessionSemester;
            studentResultExam.UploadedFileUrl = FileUploadURL;
            studentResultExam.Uploader = user;
            studentResultExam.Type = examResultType;
        }
        private List<ResultHolder> RetrieveCourseRegistrationInformation(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                if (viewModel != null && viewModel.resultFormatList != null)
                {
                    List<ResultHolder> results = new List<ResultHolder>();
                    foreach (ResultFormat resultFormat in viewModel.resultFormatList)
                    {
                        List<Abundance_Nk.Model.Model.Student> students = new List<Model.Model.Student>();
                        StudentLogic studentLogic = new StudentLogic();
                        students = studentLogic.GetModelsBy(p => p.Matric_Number == resultFormat.MATRICNO);

                        CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                        if (students.Count == 1)
                        {
                            CourseRegistration courseRegistration = new CourseRegistration();
                            ResultHolder result = new ResultHolder();
                            long studentId = students[0].Id;
                            courseRegistration = courseRegistrationLogic.GetModelBy(p => p.Person_Id == studentId && p.Level_Id == viewModel.Level.Id && p.Session_Id == viewModel.Session.Id && p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id);

                            if (courseRegistration != null)
                            {
                                result.CourseRegistration = courseRegistration;
                                result.ResultFormat = resultFormat;
                                results.Add(result);
                            }
                        }

                    }
                    return results;

                }
                else
                {
                    return new List<ResultHolder>();
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        private int validateFields(List<ResultFormat> list)
        {
            try
            {
                int failedReason = 0;

                if (list != null && list.Count > 0)
                {

                    for (int i = 0; i < list.Count; i++)
                    {
                        bool testStatus;
                        bool examStatus;
                        bool totalStatus;
                        decimal testScore = list[i].T_CA;
                        decimal inputExamScore = list[i].T_EX;
                        if (testScore > 100 || inputExamScore > 100)
                        {
                            AssignSpecialCaseRemarks(list, i, testScore, inputExamScore);
                        }
                        else
                        {
                            decimal calculatedTotalScore = list[i].T_CA + list[i].T_EX;
                            if (testScore >= 0 && testScore <= 30)
                            {
                                testStatus = true;
                            }
                            else
                            {
                                testStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }
                            }

                            if (inputExamScore >= 0 && inputExamScore <= 70)
                            {
                                examStatus = true;
                            }
                            else
                            {
                                examStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }  
                            }

                            if (calculatedTotalScore <= 100)
                            {
                                totalStatus = true;
                            }
                            else
                            {
                                totalStatus = false;
                                if (failedReason == 0)
                                {
                                    failedReason += (i + 1);
                                }
                            } 
                        }    
                    }       
                }
                if (failedReason > 0)
                {
                    return failedReason;
                }
                else
                {
                    return 0;
                }      
            }
            catch (Exception)
            {          
                throw;
            }
        }
        private static void AssignSpecialCaseRemarks(List<ResultFormat> list, int i, decimal testScore, decimal inputExamScore)
        {
            try
            {
                if (testScore == (decimal)SpeicalCaseCodes.Sick)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "SICK: TEST";
                    list[i].ResultSpecialCaseMessages.TestSpecialCaseMessage = "SICK: TEST";
                    list[i].T_CA = 0;
                }
                else if (inputExamScore == (decimal)SpeicalCaseCodes.Sick)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "SICK";
                    list[i].ResultSpecialCaseMessages.ExamSpecialCaseMessage = "SICK";
                    list[i].T_EX = 0;
                }
                else if (inputExamScore == (decimal)SpeicalCaseCodes.Absent)
                {
                    list[i].ResultSpecialCaseMessages.SpecialCaseMessage = "ABSENT";
                    list[i].ResultSpecialCaseMessages.ExamSpecialCaseMessage = "ABSENT";
                    list[i].T_EX = 0;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
  
        }
        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() { Id = Convert.ToInt32(id) };
                SemesterLogic semesterLogic = new SemesterLogic();
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetCourses(int[] ids)
        {
            try
            {
                if (ids.Count() == 0)
                {
                    return null;
                }
                Level level = new Level() { Id = Convert.ToInt32(ids[1]) };
                Department department = new Department() { Id = Convert.ToInt32(ids[0]) };
                Semester semester = new Semester() { Id = Convert.ToInt32(ids[2]) };
                List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(level, department, semester);

                for (int i = 1; i < courseList.Count; i++)
                {
                    courseList[i].Name += ", " + courseList[i].Code;
                }

                return Json(new SelectList(courseList, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void KeepDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.User = viewModel.UserSelectList;
                if (viewModel.Semester != null)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.Semester.Id);
                }
                if (viewModel.Department != null && viewModel.Department.Id > 0)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = new List<Department>();
                    departments = departmentLogic.GetBy(viewModel.Programme);

                    ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.Department.Id);
                }
                if (viewModel.Course != null && viewModel.Course.Id > 0)
                {
                    List<Course> courseList = new List<Course>();
                    courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, viewModel.Department, viewModel.Semester);

                    ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Course.Id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void KeepCourseDropDownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.User = viewModel.UserSelectList;
                if (viewModel.CourseAllocation.Semester != null)
                {
                    List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.CourseAllocation.Session.Id);

                    List<Semester> semesters = new List<Semester>();
                    foreach (SessionSemester sessionSemester in sessionSemesterList)
                    {
                        semesters.Add(sessionSemester.Semester);
                    }

                    ViewBag.Semester = new SelectList(semesters, ID, NAME, viewModel.CourseAllocation.Semester.Id);
                }
                if (viewModel.CourseAllocation.Department != null)
                {
                    DepartmentLogic departmentLogic = new DepartmentLogic();
                    List<Department> departments = new List<Department>();
                    departments = departmentLogic.GetBy(viewModel.CourseAllocation.Programme);

                    ViewBag.Department = new SelectList(departments, ID, NAME, viewModel.CourseAllocation.Department.Id);
                }
                if (viewModel.CourseAllocation.Course != null)
                {
                    List<Course> courseList = new List<Course>();
                    courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.CourseAllocation.Level, viewModel.CourseAllocation.Department, viewModel.CourseAllocation.Semester);

                    ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.CourseAllocation.Course.Id);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private DataSet ReadExcel(string filepath)
        {
            DataSet Result = null;
            try
            {
                string xConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source=" + filepath + ";" + "Extended Properties=Excel 8.0;";
                OleDbConnection connection = new OleDbConnection(xConnStr);

                connection.Open();
                DataTable sheet = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow dataRow in sheet.Rows)
                {
                    string sheetName = dataRow[2].ToString().Replace("'", "");
                    OleDbCommand command = new OleDbCommand("Select * FROM [" + sheetName + "]", connection);
                    // Create DbDataReader to Data Worksheet

                    OleDbDataAdapter MyData = new OleDbDataAdapter();
                    MyData.SelectCommand = command;
                    DataSet ds = new DataSet();
                    ds.Clear();
                    MyData.Fill(ds);
                    connection.Close();

                    Result = ds;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Result;

        }
        public void RetainDropdownState(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                SemesterLogic semesterLogic = new SemesterLogic();
                DepartmentLogic departmentLogic = new DepartmentLogic();
                SessionLogic sessionLogic = new SessionLogic();
                ProgrammeLogic programmeLogic = new ProgrammeLogic();
                LevelLogic levelLogic = new LevelLogic();
                if (viewModel != null)
                {
                    if (viewModel.Session != null)
                    {

                        ViewBag.Session = new SelectList(sessionLogic.GetModelsBy(p => p.Activated == true), ID, NAME, viewModel.Session.Id);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectList;
                    }
                    if (viewModel.Semester != null)
                    {
                        ViewBag.Semester = new SelectList(semesterLogic.GetAll(), ID, NAME, viewModel.Semester.Id);
                    }
                    else
                    {
                        ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                    }
                    if (viewModel.Programme != null)
                    {
                        ViewBag.Programme = new SelectList(programmeLogic.GetModelsBy(p => p.Activated == true), ID, NAME, viewModel.Programme.Id);
                    }
                    else
                    {
                        ViewBag.Programme = viewModel.ProgrammeSelectList;
                    }
                    if (viewModel.Department != null && viewModel.Programme != null)
                    {
                        ViewBag.Department = new SelectList(departmentLogic.GetBy(viewModel.Programme), ID, NAME, viewModel.Department.Id);
                    }
                    else
                    {
                        ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
                    }
                    if (viewModel.Level != null)
                    {
                        ViewBag.Level = new SelectList(levelLogic.GetAll(), ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                    }
                    if (viewModel.Course != null && viewModel.Level != null && viewModel.Semester != null && viewModel.Department != null)
                    {
                        List<Course> courseList = Utility.GetCoursesByLevelDepartmentAndSemester(viewModel.Level, viewModel.Department, viewModel.Semester);
                        ViewBag.Course = new SelectList(courseList, ID, NAME, viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Course = new SelectList(new List<Course>(), ID, NAME);
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        public enum SpeicalCaseCodes
        {
            Sick = 101,
            Absent = 201,
            Other = 301
        }
        public ActionResult StaffReportSheet()
        {
            StaffCourseAllocationViewModel viewModel = null;
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(),ID,NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList,ID,NAME);
            }
            catch(Exception)
            {

                throw;
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult StaffReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                KeepDropDownState(viewModel);
            }
            catch(Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message,Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult StaffDownloadReportSheet(string cid)
        {
            try
            {
                long Id = Convert.ToInt64(Utility.Decrypt(cid));
                viewModel = new StaffCourseAllocationViewModel();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocation = courseAllocationLogic.GetModelsBy(p => p.Course_Allocation_Id == Id).LastOrDefault();
                if (viewModel.CourseAllocation != null)
                {
                    ReportViewModel reportViewModel = new ReportViewModel();
                    reportViewModel.Department = viewModel.CourseAllocation.Department;
                    reportViewModel.Course = viewModel.CourseAllocation.Course;
                    reportViewModel.Faculty = viewModel.CourseAllocation.Department.Faculty;
                    reportViewModel.Level = viewModel.CourseAllocation.Level;
                    reportViewModel.Programme = viewModel.CourseAllocation.Programme;
                    reportViewModel.Semester = viewModel.CourseAllocation.Semester;
                    reportViewModel.Session = viewModel.CourseAllocation.Session;
                    TempData["ReportViewModel"] = reportViewModel;
                    return RedirectToAction("ResultSheet","Report", new { area = "admin"});
                }
                
                  return RedirectToAction("StaffReportSheet","StaffCourseAllocation", new { area = "admin"});
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        public ActionResult DownloadBlankResultSheet()
        {
            try
            {
                StaffCourseAllocationViewModel viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(),ID,NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList,ID,NAME);
                return View(viewModel);

            }
            catch(Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message,Message.Category.Error);
            }
            return View();
        }

        [HttpPost]
        public ActionResult DownloadBlankResultSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.COURSE.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
                KeepDropDownState(viewModel);
                TempData["vModel"] = viewModel;
            }
            catch(Exception)
            {

                throw;
            }
            return View(viewModel);
        }

        public ActionResult BlankResultUploadSheet(long cid)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Allocation_Id == cid);
                if(courseAllocation == null)
                {
                    SetMessage("You are not allocated to this course, with this programme-department combination",Message.Category.Error);
                    return RedirectToAction("StaffResultSheet");
                }
                GridView gv = new GridView();
                DataTable ds = new DataTable();
                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                ResultFormat sampleFormat = new ResultFormat();
                sampleFormat.MATRICNO = "N/XX/15/12345";
                 resultFormatList.Add(sampleFormat); 
                gv.DataSource = resultFormatList;// resultFormatList.OrderBy(p => p.MATRICNO);
                CourseLogic courseLogic = new CourseLogic();
                Course course = courseLogic.GetModelBy(p => p.Course_Id == courseAllocation.Course.Id);
                gv.Caption = course.Name + " " + course.Code + " " + " DEPARTMENT OF " + " " + course.Department.Name.ToUpper() + " " + course.Unit + " " + "Units";
                gv.DataBind();

                  string filename = course.Code.Replace("/", "").Replace("\\", "") + course.Department.Code + ".xls";
                    return new DownloadFileActionResult(gv, filename);
                

            }
            catch(Exception ex)
            {
                SetMessage("Error occured! " + ex.Message,Message.Category.Error);
            }
            return RedirectToAction("StaffResultSheet");
        }

        [HttpPost]
        public ActionResult BlankResultUploadSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocation courseAllocation = new CourseAllocation();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                courseAllocation = courseAllocationLogic.GetModelBy(p => p.Course_Allocation_Id == viewModel.cid);
                CourseLogic courseLogic = new CourseLogic();
                Course course = courseLogic.GetModelBy(p => p.Course_Id == courseAllocation.Course.Id);
                if(courseAllocation == null)
                {
                    SetMessage("You are not allocated to this course, with this programme-department combination",Message.Category.Error);
                    return RedirectToAction("StaffResultSheet");
                }
                List<ResultFormat> resultFormatList = new List<ResultFormat>();
                foreach(string file in Request.Files)
                {
                    HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                    string pathForSaving = Server.MapPath("~/Content/ExcelUploads");
                    string savedFileName = Path.Combine(pathForSaving, hpf.FileName);
                    FileUploadURL = savedFileName;
                    hpf.SaveAs(savedFileName);
                    DataSet studentSet = ReadExcel(savedFileName);

                    if(studentSet != null && studentSet.Tables[0].Rows.Count > 0)
                    {
                        for(int i = 1;i < studentSet.Tables[0].Rows.Count;i++)
                        {
                            ResultFormat resultFormat = new ResultFormat();
                            resultFormat.SN = Convert.ToInt32(studentSet.Tables[0].Rows[i][0].ToString().Trim());
                            resultFormat.MATRICNO = studentSet.Tables[0].Rows[i][1].ToString().Trim();
                            resultFormat.NAME = studentSet.Tables[0].Rows[i][2].ToString().Trim();
                            resultFormat.T_EX = Convert.ToDecimal(studentSet.Tables[0].Rows[i][3].ToString().Trim());
                            resultFormat.T_CA = Convert.ToDecimal(studentSet.Tables[0].Rows[i][4].ToString().Trim());
                            resultFormat.fileUploadUrl = savedFileName;
                            if(resultFormat.MATRICNO != "")
                            {
                                resultFormatList.Add(resultFormat);
                            }          
                        }
                        resultFormatList.OrderBy(p => p.MATRICNO);
                        viewModel.resultFormatList = resultFormatList;
                        viewModel.Course = course;
                        StaffCourseAllocationViewModel vModel = (StaffCourseAllocationViewModel)TempData["vModel"];
                        vModel.Course = course;
                        vModel.resultFormatList = resultFormatList;
                        vModel.CourseAllocation = courseAllocation;
                        TempData["staffCourseAllocationViewModel"] = vModel;

                    }

                }
            }
            catch(Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message,Message.Category.Error);
            }
            RetainDropdownState(viewModel);
            return View(viewModel);
        }

        public ActionResult SaveUploadedBlankResultSheet()
        {
            StaffCourseAllocationViewModel viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
            try
            {
                if(viewModel != null)
                {

                    int status = validateFields(viewModel.resultFormatList);

                    if(status > 0)
                    {
                        ResultFormat format = viewModel.resultFormatList.ElementAt((status - 1));
                        SetMessage("Validation Error for" + " " + format.MATRICNO,Message.Category.Error);
                        RetainDropdownState(viewModel);
                        return RedirectToAction("DownloadBlankResultSheet");
                    }

                    bool resultAdditionStatus = AddUnregisteredStudentResult(viewModel);

                    SetMessage("Upload successful",Message.Category.Information);
                }


            }
            catch(Exception ex)
            {
                SetMessage("Error occured " + ex.Message,Message.Category.Error);
            }

            RetainDropdownState(viewModel);
            return RedirectToAction("DownloadBlankResultSheet");
        }

        private bool AddUnregisteredStudentResult(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                viewModel = (StaffCourseAllocationViewModel)TempData["staffCourseAllocationViewModel"];
                SessionSemesterLogic sessionSemesterLogicc = new SessionSemesterLogic();
                SessionSemester sessionSemester = new SessionSemester();
                UserLogic userLogic = new UserLogic();
                StudentResultType testResultType = new StudentResultType() { Id = 1 };
                StudentResultType examResultType = new StudentResultType() { Id = 2 };
                User user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                sessionSemester = sessionSemesterLogicc.GetModelBy(p => p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id);
                if(viewModel != null && viewModel.resultFormatList.Count > 0)
                {
                    
                    StudentExamRawScoreSheet studentExamRawScoreSheet = new StudentExamRawScoreSheet();
                    StudentExamRawScoreSheetNotRegisteredLogic StudentExamRawScoreSheetLogic = new StudentExamRawScoreSheetNotRegisteredLogic();

                    StudentResultDetailLogic studentResultDetailLogic = new StudentResultDetailLogic();
                    using(TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,new System.TimeSpan(0,15,0)))
                    {
                        foreach(ResultFormat resultFormat in viewModel.resultFormatList)
                        {
                            studentExamRawScoreSheet = StudentExamRawScoreSheetLogic.GetModelBy(p => p.Student_Matric_Number == resultFormat.MATRICNO && p.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Course_Id == viewModel.Course.Id);
                            if(studentExamRawScoreSheet == null)
                            {
                                studentExamRawScoreSheet = CreateUnregisteredStudentExamRawScoreSheet(viewModel,user,studentExamRawScoreSheet,StudentExamRawScoreSheetLogic,resultFormat,resultFormat.fileUploadUrl);
                            }
                            else
                            {
                                ModifyUnregisteredStudentExamRawScoreSheet(viewModel,user,studentExamRawScoreSheet,StudentExamRawScoreSheetLogic,resultFormat,resultFormat.fileUploadUrl);
                            }

                        }
                        scope.Complete();
                    }


                }

                return true;
            }
            catch(Exception)
            {

                throw;
            }
        }

        private static StudentExamRawScoreSheet CreateUnregisteredStudentExamRawScoreSheet(StaffCourseAllocationViewModel viewModel,User user,StudentExamRawScoreSheet studentExamRawScoreSheet,StudentExamRawScoreSheetNotRegisteredLogic studentExamRawScoreSheetNotRegisteredLogic,ResultFormat resultFormat,string fileURL)
        {
            studentExamRawScoreSheet = new StudentExamRawScoreSheet();
            studentExamRawScoreSheet.Course = viewModel.Course;
            studentExamRawScoreSheet.Level = viewModel.Level;
            studentExamRawScoreSheet.T_CA = (double)resultFormat.T_CA;
            studentExamRawScoreSheet.T_EX = (double)resultFormat.T_EX;
            studentExamRawScoreSheet.Semester = viewModel.Semester;
            studentExamRawScoreSheet.Session = viewModel.Session;
            studentExamRawScoreSheet.Special_Case = resultFormat.ResultSpecialCaseMessages.SpecialCaseMessage;
            studentExamRawScoreSheet.MatricNumber = resultFormat.MATRICNO;
            studentExamRawScoreSheet.Uploader = user;
            studentExamRawScoreSheet.FileUploadURL = fileURL;
            studentExamRawScoreSheetNotRegisteredLogic.Create(studentExamRawScoreSheet);
            return studentExamRawScoreSheet;
        }

        private static void ModifyUnregisteredStudentExamRawScoreSheet(StaffCourseAllocationViewModel viewModel,User user,StudentExamRawScoreSheet studentExamRawScoreSheet,StudentExamRawScoreSheetNotRegisteredLogic studentExamRawScoreSheetNotRegisteredLogic,ResultFormat resultFormat,string fileURL)
        {
            studentExamRawScoreSheet.Course = viewModel.Course;
            studentExamRawScoreSheet.Level = viewModel.Level;
            studentExamRawScoreSheet.T_CA = (double)resultFormat.T_CA;
            studentExamRawScoreSheet.T_EX = (double)resultFormat.T_EX;
            studentExamRawScoreSheet.Semester = viewModel.Semester;
            studentExamRawScoreSheet.Session = viewModel.Session;
            studentExamRawScoreSheet.Special_Case = resultFormat.ResultSpecialCaseMessages.SpecialCaseMessage;
            studentExamRawScoreSheet.MatricNumber = resultFormat.MATRICNO;
            studentExamRawScoreSheet.Uploader = user;
            studentExamRawScoreSheet.FileUploadURL = fileURL;
            bool isScoreSheetModified = studentExamRawScoreSheetNotRegisteredLogic.Modify(studentExamRawScoreSheet);
        }

        public ActionResult StaffBlankReportSheet()
        {
            StaffCourseAllocationViewModel viewModel = null;
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(),ID,NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList,ID,NAME);
            }
            catch(Exception)
            {

                throw;
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult StaffBlankReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.COURSE.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name);
               
                KeepDropDownState(viewModel);
            }
            catch(Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message,Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult AdminBlankReportSheet()
        {
            StaffCourseAllocationViewModel viewModel = null;
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AdminBlankReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.COURSE.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Programme_Id == viewModel.Programme.Id && p.Department_Id == viewModel.Department.Id);

                KeepDropDownState(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult StaffDownloadBlankReportSheet(int Session_Id,int Semester_Id,int Programme_Id,int Department_Id,int Level_Id,int Course_Id)
        {
            try
            {

                return RedirectToAction("UnregisteredStudentResultSheet",new { area = "admin",controller = "Report",levelId = Level_Id,semesterId = Semester_Id,progId = Programme_Id,deptId = Department_Id,sessionId = Session_Id,courseId = Course_Id });

            }
            catch(Exception)
            {

                throw;
            }

        }
        public ActionResult ViewUploadedCourses()
        {
            try
            {
               viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ViewUploadedCourses(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                StudentResultLogic studentResultLogic = new StudentResultLogic();
                viewModel.UploadedCourses = studentResultLogic.GetUploadedCourses(viewModel.Session, viewModel.Semester);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            TempData["UploadedCourses"] = viewModel.UploadedCourses;
            TempData.Keep("UploadedCourses");
            RetainSessionSemesterDropDown(viewModel);

            return View(viewModel);
        }
        private void RetainSessionSemesterDropDown(StaffCourseAllocationViewModel viewModel)
        {
            List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
            SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
            sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id);

            List<Semester> semesters = new List<Semester>();
            foreach (SessionSemester sessionSemester in sessionSemesterList)
            {
                semesters.Add(sessionSemester.Semester);
            }
            ViewBag.Session = viewModel.SessionSelectList;
            if (viewModel.Semester != null)
            {
                ViewBag.Semester = new SelectList(semesters, "Id", "Name", viewModel.Semester.Id);
            }
            else
            {
                ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
            }
        }
        public ActionResult ViewScoreSheet(string index)
        {
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                viewModel.UploadedCourses = (List<UploadedCourseFormat>) TempData["UploadedCourses"];
                UploadedCourseFormat uploadedCourseFormat = viewModel.UploadedCourses[Convert.ToInt32(index)];
                ReportViewModel reportViewModel = new ReportViewModel();
                reportViewModel.Department.Name = uploadedCourseFormat.Department;
                reportViewModel.Department.Id = uploadedCourseFormat.DepartmentId;
                reportViewModel.Course.Id = uploadedCourseFormat.CourseId;
                reportViewModel.Course.Name = uploadedCourseFormat.CourseTitle;
                reportViewModel.Level.Id = uploadedCourseFormat.LevelId;
                reportViewModel.Level.Name = uploadedCourseFormat.Level;
                reportViewModel.Programme.Id = uploadedCourseFormat.ProgrammeId;
                reportViewModel.Semester.Id = uploadedCourseFormat.SemesterId;
                reportViewModel.Session.Id = uploadedCourseFormat.SessionId;


                TempData["ReportViewModel"] = reportViewModel;
                return RedirectToAction("ResultSheet", new { area = "admin", controller = "Report"});
            }
            catch (Exception)
            {       
                throw;
            }    
        }
        public ActionResult DepartmentReportSheet()
        {
            StaffCourseAllocationViewModel viewModel = null;
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(),ID,NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList,ID,NAME);
            }
            catch(Exception)
            {

                throw;
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult DepartmentReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Session_Id == viewModel.Session.Id && p.USER.User_Name == User.Identity.Name  && p.Is_HOD == true);
                if (viewModel.CourseAllocations != null && viewModel.CourseAllocations.Count > 0)
                {
                    int HodDepartmentId = viewModel.CourseAllocations[0].HodDepartment.Id;
                     viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Department_Id == HodDepartmentId && p.Level_Id == viewModel.Level.Id && p.Session_Id == viewModel.Session.Id);
              
                }
                KeepDropDownState(viewModel);
            }
            catch(Exception ex)
            {

                SetMessage("Error occurred! " + ex.Message,Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult ViewAlternateScoreSheet()
        {
            try
            {
               viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ViewAlternateScoreSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                StudentResultLogic studentResultLogic = new StudentResultLogic();
                viewModel.UploadedCourses = studentResultLogic.GetUploadedAlternateCourses(viewModel.Session, viewModel.Semester);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
            TempData["UploadedCourses"] = viewModel.UploadedCourses;
            TempData.Keep("UploadedCourses");
            RetainSessionSemesterDropDown(viewModel);

            return View(viewModel);
        }
        public ActionResult DownloadViewAlternateScoreSheet(int Session_Id,int Semester_Id,int Programme_Id,int Department_Id,int Level_Id,int Course_Id)
        {
            try
            {

                return RedirectToAction("UnregisteredStudentResultSheet",new { area = "admin",controller = "Report",levelId = Level_Id,semesterId = Semester_Id,progId = Programme_Id,deptId = Department_Id,sessionId = Session_Id,courseId = Course_Id });

            }
            catch(Exception)
            {

                throw;
            }

        }
      
        public ActionResult AdminDownloadBlankReportSheet()
        {
            StaffCourseAllocationViewModel viewModel = null;
            try
            {
                viewModel = new StaffCourseAllocationViewModel();
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), ID, NAME);
                ViewBag.Level = new SelectList(viewModel.LevelList, ID, NAME);
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult AdminDownloadBlankReportSheet(StaffCourseAllocationViewModel viewModel)
        {
            try
            {
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                viewModel.CourseAllocations = courseAllocationLogic.GetModelsBy(p => p.Level_Id == viewModel.Level.Id && p.COURSE.Semester_Id == viewModel.Semester.Id && p.Session_Id == viewModel.Session.Id && p.Programme_Id == viewModel.Programme.Id && p.Department_Id == viewModel.Department.Id);

                KeepDropDownState(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult AdminBlankResultUploadSheet(long cid, int deptId,int progId, int levelId, int sessionId)
        {
            try
            {
                StudentExamRawScoreSheetNotRegisteredLogic scoreSheetNotRegisteredLogic = new StudentExamRawScoreSheetNotRegisteredLogic();
                
                GridView gv = new GridView();
                DataTable ds = new DataTable();
                List<ResultFormat> resultFormatList = scoreSheetNotRegisteredLogic.GetDownloadResultFormats(cid, deptId, progId, levelId,sessionId);
               
                gv.DataSource = resultFormatList;// resultFormatList.OrderBy(p => p.MATRICNO);
                CourseLogic courseLogic = new CourseLogic();
                Course course = courseLogic.GetModelBy(p => p.Course_Id == cid);
                gv.Caption = course.Name + " " + course.Code + " " + " DEPARTMENT OF " + " " + course.Department.Name.ToUpper() + " " + course.Unit + " " + "Units";
                gv.DataBind();

                  string filename = course.Code.Replace("/", "").Replace("\\", "") + course.Department.Code + ".xls";
                    return new DownloadFileActionResult(gv, filename);
                

            }
            catch(Exception ex)
            {
                SetMessage("Error occured! " + ex.Message,Message.Category.Error);
            }
            return RedirectToAction("AdminDownloadBlankReportSheet");
        }
        
      

    }
}