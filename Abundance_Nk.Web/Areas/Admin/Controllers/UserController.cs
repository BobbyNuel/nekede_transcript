﻿using System.EnterpriseServices.Internal;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Business;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : BaseController
    {
        private UserViewModel viewModel;
        private User user;
        private Person person;
        private PersonType personType;
        private UserLogic userLogic;
        private PersonLogic personLogic;
        private PersonTypeLogic personTypeLogic;
        public UserController()
        {
            viewModel = new UserViewModel();
            user = new User();
            person = new Person();
            personType = new PersonType();
            personTypeLogic = new PersonTypeLogic();
            userLogic = new UserLogic();
            personLogic = new PersonLogic();
        }

        public ActionResult Index()
        {
            try
            {
                List<User> userList = new List<User>();
                userList = userLogic.GetAll();
                viewModel.Users = userList;
            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }
            return View(viewModel);
        }
        public ActionResult CreateUser()
        {

            ViewBag.SexList = viewModel.SexSelectList;
            ViewBag.RoleList = viewModel.RoleSelectList;
            ViewBag.SecurityQuestionList = viewModel.SecurityQuestionSelectList;

            return View();
        }
        [HttpPost]
        public ActionResult CreateUser(UserViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    List<User> users = new List<User>();
                    UserLogic userLogic = new UserLogic();
                    users = userLogic.GetModelsBy(p => p.User_Name == viewModel.User.Username);
                    if (users.Count > 0)
                    {
                        SetMessage("Error: Staff with this Name already exist", Message.Category.Error);
                        return RedirectToAction("CreateUser");
                    }

                    viewModel.User.LastLoginDate = DateTime.Now;

                    userLogic.Create(viewModel.User);

                    SetMessage("User Created Succesfully", Message.Category.Information);

                    return RedirectToAction("Index", "User");
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Error);
                    return RedirectToAction("CreateUser");
                }

            }
            catch (Exception e)
            {
                SetMessage("Error Occured " + e.Message, Message.Category.Error);
            }

            ViewBag.SexList = viewModel.SexSelectList;
            ViewBag.RoleList = viewModel.RoleSelectList;
            ViewBag.SecurityQuestionList = viewModel.SecurityQuestionSelectList;
           
            return View("CreateUser", viewModel);
        }

        public ActionResult GetUserDetails()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetUserDetails(UserViewModel viewModel)
        {
            try
            {
                if (viewModel.User.Username != null)
                {
                    userLogic = new UserLogic();
                    List<User> users = userLogic.GetModelsBy(u => u.User_Name == viewModel.User.Username);
                    if (users.Count > 1)
                    {
                        SetMessage("There are more than one user with this username!", Message.Category.Error);
                        return View(viewModel);
                    }
                    if (users.Count == 0)
                    {
                        SetMessage("There is no user with this username!", Message.Category.Error);
                        return View(viewModel);
                    }

                    Model.Model.User user = users.FirstOrDefault();
                    return RedirectToAction("ViewUserDetails", new { @id = user.Id });
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }

            return View();
        }
        public ActionResult ViewUserDetails(int? id)
        {
            try
            {
                viewModel = null;
                if (id != null)
                {
                    user = userLogic.GetModelBy(p => p.User_Id == id);
                    viewModel = new UserViewModel();
                    viewModel.User = user;
                    return View(viewModel);
                }
                else
                {
                    SetMessage("Error Occured: Select a User", Message.Category.Error);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return View();
        }
        public ActionResult EditUser(int? id)
        {
            viewModel = null;
            try
            {
                if (id != null)
                {
                    TempData["userId"] = id;
                    user = userLogic.GetModelBy(p => p.User_Id == id);
                    viewModel = new UserViewModel();
                    viewModel.User = user;
                }
                else
                {
                    SetMessage("Select a User", Message.Category.Error);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }

            ViewBag.RoleList = viewModel.RoleSelectList;
            ViewBag.SecurityQuestionList = viewModel.SecurityQuestionSelectList;
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditUser(UserViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    //Role role = new Role() { Id = 8 };
                    //viewModel.User.Role = role;
                    viewModel.User.Id = (int)TempData["userId"];
                    userLogic.Update(viewModel.User);
                    SetMessage("User Edited Successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Input is null", Message.Category.Warning);
                    return RedirectToAction("EditUser");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured " + ex.Message, Message.Category.Error);
            }
            return RedirectToAction("Index");
        }
        public ActionResult EditUserRole()
        {
            try
            {
                viewModel = new UserViewModel();
                ViewBag.Role = viewModel.RoleSelectList;
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditUserRole(UserViewModel viewModel)
        {
            try
            {
                if (viewModel.User.Role != null)
                {
                    UserLogic userLogic = new UserLogic();
                    viewModel.Users = userLogic.GetModelsBy(u => u.Role_Id == viewModel.User.Role.Id);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Role = viewModel.RoleSelectList;
            return View(viewModel);
        }
        public ActionResult EditRole(string id)
        {
            try
            {
                int userId = Convert.ToInt32(id);
                UserLogic userLogic = new UserLogic();

                viewModel = new UserViewModel();
                viewModel.User = userLogic.GetModelBy(u => u.User_Id == userId);

                ViewBag.Role = new SelectList(viewModel.RoleSelectList, "Value", "Text", viewModel.User.Role.Id);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditRole(UserViewModel viewModel)
        {
            try
            {
                if (viewModel.User != null)
                {
                    UserLogic userLogic = new UserLogic();
                    bool isUserModified = userLogic.Modify(viewModel.User);

                    if (isUserModified)
                    {
                        SetMessage("Operation Successful!", Message.Category.Information);
                        return RedirectToAction("EditRoleByUserName");
                    }
                    else
                    {
                        SetMessage("No item was Modified", Message.Category.Information);
                        return RedirectToAction("EditRoleByUserName");
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("EditRoleByUserName");
        }

        public ActionResult EditRoleByUserName()
        {
            try
            {
                viewModel = new UserViewModel();
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult EditRoleByUserName(UserViewModel viewModel)
        {
            try
            {
                if (viewModel.User.Username != null)
                {
                    UserLogic userLogic = new UserLogic();
                    User user = userLogic.GetModelBy(u => u.User_Name == viewModel.User.Username);
                    viewModel.User = user;
                    if (user == null)
                    {
                        SetMessage("User does not exist!", Message.Category.Error);
                        return View(viewModel);
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Role = viewModel.RoleSelectList;
            return View(viewModel);
        }

        public void RetainDropDownState(UserViewModel viewModel)
        {
            try
            {
                if (viewModel.CourseAllocation != null)
                {
                    if (viewModel.CourseAllocation.Programme != null)
                    {
                        ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.CourseAllocation.Programme.Id);
                    }
                    else
                    {
                        ViewBag.Programme = viewModel.ProgrammeSelectList;
                    }

                    if (viewModel.CourseAllocation.Department != null && viewModel.CourseAllocation.Programme != null)
                    {
                        DepartmentLogic departmentLogic = new DepartmentLogic();
                        ViewBag.Department = new SelectList(departmentLogic.GetBy(viewModel.CourseAllocation.Programme), "Id", "Name", viewModel.CourseAllocation.Department.Id);
                    }
                    else
                    {
                        ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    }

                    if (viewModel.CourseAllocation.Semester != null && viewModel.CourseAllocation.Session != null)
                    {
                        SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                        List<SessionSemester> sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == viewModel.CourseAllocation.Session.Id);

                        List<Semester> semesters = new List<Semester>();
                        foreach (SessionSemester sessionSemester in sessionSemesterList)
                        {
                            semesters.Add(sessionSemester.Semester);
                        }

                        ViewBag.Semester = new SelectList(semesters, "Id", "Name", viewModel.CourseAllocation.Session.Id);
                    }
                    else
                    {
                        ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    }

                    if (viewModel.CourseAllocation.Session != null)
                    {
                        ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.CourseAllocation.Session.Id);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectList;
                    }

                    if (viewModel.CourseAllocation.Level != null)
                    {
                        ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.CourseAllocation.Level.Id);
                    }
                    else
                    {
                        ViewBag.Level = viewModel.LevelSelectList;
                    }
                }
                if (viewModel.User != null)
                {
                    if (viewModel.User.Role != null)
                    {
                        ViewBag.Role = new SelectList(viewModel.RoleSelectList, "Value", "Text", viewModel.User.Role.Id);
                    }
                    else
                    {
                        ViewBag.Role = viewModel.RoleSelectList;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JsonResult GetDepartments(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() { Id = Convert.ToInt32(id) };
                SemesterLogic semesterLogic = new SemesterLogic();
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult DeleteStaff()
        {
            try
            {
                viewModel = new UserViewModel();
            }
            catch (Exception ex)
            {
                SetMessage("Error" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult DeleteStaff(UserViewModel viewModel)
        {
            try
            {
                UserLogic userLogic = new UserLogic();
                List<User> users = new List<User>();
                if (viewModel.User != null)
                {

                    users = userLogic.GetModelsBy(x => x.User_Name.Contains(viewModel.User.Username) && x.Role_Id != 1).OrderBy(x => x.Username).ToList();
                    viewModel.Users = users;
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public ActionResult DeleteStaffAction(int? id)
        {
            try
            {
                UserLogic userLogic = new UserLogic();
                Model.Model.User user = new User();
                List<CourseAllocation> courseAllocations = new List<CourseAllocation>();
                CourseAllocationLogic courseAllocationLogic = new CourseAllocationLogic();
                bool isDeleted = false;
                if (id != null)
                {
                    user = userLogic.GetModelBy(x => x.User_Id == id);
                    courseAllocations = courseAllocationLogic.GetModelsBy(x => x.User_Id == user.Id);
                    if (courseAllocations.Count > 0)
                    {
                        SetMessage("Cannot Delete User. Staff has course allocated to him/her", Message.Category.Error);
                        return RedirectToAction("DeleteStaff");
                    }

                    isDeleted = userLogic.Delete(u => u.User_Id == user.Id);
                }
                if (isDeleted == true)
                {
                    SetMessage("Deleted Successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Error", Message.Category.Error);
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error" + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("DeleteStaff");
        }
    }
}