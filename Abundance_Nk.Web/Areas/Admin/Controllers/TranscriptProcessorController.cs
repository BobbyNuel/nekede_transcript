﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Models;
using Abundance_Nk.Web.Controllers;
using System.Transactions;
using System.Web.Script.Serialization;
using Abundance_Nk.Model.Entity;
using System.Globalization;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class TranscriptProcessorController : BaseController
    {
        TranscriptProcessorViewModel viewModel;
        // GET: Admin/TranscriptProcessor
        public ActionResult Index()
        {
            viewModel = new TranscriptProcessorViewModel();

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult Index(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                DateTime DateFrom = new DateTime(Convert.ToInt32(viewModel.DateFrom.Split('-')[0]), Convert.ToInt32(viewModel.DateFrom.Split('-')[1]), Convert.ToInt32(viewModel.DateFrom.Split('-')[2]));
                DateTime DateTo = new DateTime(Convert.ToInt32(viewModel.DateTo.Split('-')[0]), Convert.ToInt32(viewModel.DateTo.Split('-')[1]), Convert.ToInt32(viewModel.DateTo.Split('-')[2]));
                
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();
                if (viewModel.transcriptRequest.VerificationStatus)
                {
                    transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified == true);
                }
                else
                {
                    transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified != true);
                }
                viewModel.transcriptRequests = new List<TranscriptRequest>();
                for (int i = 0; i < transcriptRequests.Count; i++)
                {
                    TranscriptRequest transcriptRequest = transcriptRequests[i];

                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == transcriptRequest.student.Id).LastOrDefault();
                    if (studentLevel != null)
                    {
                        transcriptRequests[i].DepartmentName = studentLevel.Department.Name;
                        transcriptRequests[i].ProgrammeName = studentLevel.Programme.Name;
                    }

                    transcriptRequests[i].student.FullName = transcriptRequests[i].student.FullName.ToUpper();
                    transcriptRequests[i].student.MatricNumber = transcriptRequests[i].student.MatricNumber.ToUpper();
                    transcriptRequests[i].DestinationAddress = transcriptRequests[i].DestinationAddress.ToUpper();
                    transcriptRequests[i].transcriptStatus.TranscriptStatusName = transcriptRequests[i].transcriptStatus.TranscriptStatusName.ToUpper();

                    if (transcriptRequests[i].student.MatricNumber.Split('/').Length > 0 && transcriptRequests[i].student.MatricNumber.Split('/').FirstOrDefault().Contains('H'))
                    {
                        //do nothing
                    }
                    else
                    {
                        viewModel.transcriptRequests.Add(transcriptRequests[i]);
                    }
                }

                viewModel.transcriptRequests = viewModel.transcriptRequests.OrderBy(t => t.DateRequested).ToList();
                PopulateDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public ActionResult TranscripRequestsHnd()
        {
            viewModel = new TranscriptProcessorViewModel();

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult TranscripRequestsHnd(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                DateTime DateFrom = new DateTime(Convert.ToInt32(viewModel.DateFrom.Split('-')[0]), Convert.ToInt32(viewModel.DateFrom.Split('-')[1]), Convert.ToInt32(viewModel.DateFrom.Split('-')[2]));
                DateTime DateTo = new DateTime(Convert.ToInt32(viewModel.DateTo.Split('-')[0]), Convert.ToInt32(viewModel.DateTo.Split('-')[1]), Convert.ToInt32(viewModel.DateTo.Split('-')[2]));
                
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();
                if (viewModel.transcriptRequest.VerificationStatus)
                {
                    transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified == true);
                }
                else
                {
                    transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified != true);
                }
                viewModel.transcriptRequests = new List<TranscriptRequest>();
                for (int i = 0; i < transcriptRequests.Count; i++)
                {
                    TranscriptRequest transcriptRequest = transcriptRequests[i];

                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == transcriptRequest.student.Id).LastOrDefault();
                    if (studentLevel != null)
                    {
                        transcriptRequests[i].DepartmentName = studentLevel.Department.Name;
                        transcriptRequests[i].ProgrammeName = studentLevel.Programme.Name;
                    }

                    transcriptRequests[i].student.FullName = transcriptRequests[i].student.FullName.ToUpper();
                    transcriptRequests[i].student.MatricNumber = transcriptRequests[i].student.MatricNumber.ToUpper();
                    transcriptRequests[i].DestinationAddress = transcriptRequests[i].DestinationAddress.ToUpper();
                    transcriptRequests[i].transcriptStatus.TranscriptStatusName = transcriptRequests[i].transcriptStatus.TranscriptStatusName.ToUpper();

                    if (transcriptRequests[i].student.MatricNumber.Split('/').Length > 0 && transcriptRequests[i].student.MatricNumber.Split('/').FirstOrDefault().Contains('H'))
                    {
                        viewModel.transcriptRequests.Add(transcriptRequests[i]);
                        
                    }
                    else
                    {
                        //do nothing
                    }
                }

                viewModel.transcriptRequests = viewModel.transcriptRequests.OrderBy(t => t.DateRequested).ToList();
                PopulateDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public ActionResult VerifiedTranscriptRequestNd()
        {
            viewModel = new TranscriptProcessorViewModel();

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult VerifiedTranscriptRequestNd(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                DateTime DateFrom = new DateTime(Convert.ToInt32(viewModel.DateFrom.Split('-')[0]), Convert.ToInt32(viewModel.DateFrom.Split('-')[1]), Convert.ToInt32(viewModel.DateFrom.Split('-')[2]));
                DateTime DateTo = new DateTime(Convert.ToInt32(viewModel.DateTo.Split('-')[0]), Convert.ToInt32(viewModel.DateTo.Split('-')[1]), Convert.ToInt32(viewModel.DateTo.Split('-')[2]));

                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();
               
                transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified == true);
                
                viewModel.transcriptRequests = new List<TranscriptRequest>();
                for (int i = 0; i < transcriptRequests.Count; i++)
                {
                    TranscriptRequest transcriptRequest = transcriptRequests[i];

                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == transcriptRequest.student.Id).LastOrDefault();
                    if (studentLevel != null)
                    {
                        transcriptRequests[i].DepartmentName = studentLevel.Department.Name;
                        transcriptRequests[i].ProgrammeName = studentLevel.Programme.Name;
                    }

                    transcriptRequests[i].student.FullName = transcriptRequests[i].student.FullName.ToUpper();
                    transcriptRequests[i].student.MatricNumber = transcriptRequests[i].student.MatricNumber.ToUpper();
                    transcriptRequests[i].DestinationAddress = transcriptRequests[i].DestinationAddress.ToUpper();
                    transcriptRequests[i].transcriptStatus.TranscriptStatusName = transcriptRequests[i].transcriptStatus.TranscriptStatusName.ToUpper();

                    if (transcriptRequests[i].student.MatricNumber.Split('/').Length > 0 && transcriptRequests[i].student.MatricNumber.Split('/').FirstOrDefault().Contains('H'))
                    {
                        //do nothing
                    }
                    else
                    {
                        viewModel.transcriptRequests.Add(transcriptRequests[i]);
                    }
                }

                viewModel.transcriptRequests = viewModel.transcriptRequests.OrderBy(t => t.DateRequested).ToList();
                PopulateDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public ActionResult VerifiedTranscriptRequestHnd()
        {
            viewModel = new TranscriptProcessorViewModel();

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult VerifiedTranscriptRequestHnd(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                DateTime DateFrom = new DateTime(Convert.ToInt32(viewModel.DateFrom.Split('-')[0]), Convert.ToInt32(viewModel.DateFrom.Split('-')[1]), Convert.ToInt32(viewModel.DateFrom.Split('-')[2]));
                DateTime DateTo = new DateTime(Convert.ToInt32(viewModel.DateTo.Split('-')[0]), Convert.ToInt32(viewModel.DateTo.Split('-')[1]), Convert.ToInt32(viewModel.DateTo.Split('-')[2]));

                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                List<TranscriptRequest> transcriptRequests = new List<TranscriptRequest>();

                transcriptRequests = requestLogic.GetModelsBy(t => t.Date_Requested <= DateTo && t.Date_Requested >= DateFrom && t.Transcript_Status_Id > 1 && t.Verified == true);

                viewModel.transcriptRequests = new List<TranscriptRequest>();
                for (int i = 0; i < transcriptRequests.Count; i++)
                {
                    TranscriptRequest transcriptRequest = transcriptRequests[i];

                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == transcriptRequest.student.Id).LastOrDefault();
                    if (studentLevel != null)
                    {
                        transcriptRequests[i].DepartmentName = studentLevel.Department.Name;
                        transcriptRequests[i].ProgrammeName = studentLevel.Programme.Name;
                    }

                    transcriptRequests[i].student.FullName = transcriptRequests[i].student.FullName.ToUpper();
                    transcriptRequests[i].student.MatricNumber = transcriptRequests[i].student.MatricNumber.ToUpper();
                    transcriptRequests[i].DestinationAddress = transcriptRequests[i].DestinationAddress.ToUpper();
                    transcriptRequests[i].transcriptStatus.TranscriptStatusName = transcriptRequests[i].transcriptStatus.TranscriptStatusName.ToUpper();

                    if (transcriptRequests[i].student.MatricNumber.Split('/').Length > 0 && transcriptRequests[i].student.MatricNumber.Split('/').FirstOrDefault().Contains('H'))
                    {
                        viewModel.transcriptRequests.Add(transcriptRequests[i]);

                    }
                    else
                    {
                        //do nothing
                    }
                }

                viewModel.transcriptRequests = viewModel.transcriptRequests.OrderBy(t => t.DateRequested).ToList();
                PopulateDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        private void PopulateDropDown(TranscriptProcessorViewModel viewModel)
        {
            int i = 0;
            foreach (TranscriptRequest t in viewModel.transcriptRequests)
            {
                ViewData["status" + i] = new SelectList(viewModel.transcriptSelectList, Utility.VALUE, Utility.TEXT, t.transcriptStatus.TranscriptStatusId);
                ViewData["clearanceStatus" + i] = new SelectList(viewModel.transcriptClearanceSelectList, Utility.VALUE, Utility.TEXT, t.transcriptClearanceStatus.TranscriptClearanceStatusId);
                i++;
            }
        }
    
        //public ActionResult UpdateStatus(long tid, long stat)
        //{
        //    viewModel = new TranscriptProcessorViewModel();
        //    try
        //    {
        //        TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
        //        TranscriptRequest tRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == tid);
        //        tRequest.transcriptStatus = new TranscriptStatus { TranscriptStatusId = (int)stat };
        //        transcriptRequestLogic.Modify(tRequest);

        //        Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateOnlineTranscriptRequest(tRequest);

        //        SetMessage("Task Completed! ", Message.Category.Information);
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
        //    }
        //    return RedirectToAction("Index");
        //}
        public JsonResult UpdateStatus(long tid, long stat)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (tid > 0 && stat > 0)
                {
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest transcriptRequest = new TranscriptRequest();
                    transcriptRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == tid);

                    if (transcriptRequest != null)
                    {
                        transcriptRequest.transcriptStatus = new TranscriptStatus { TranscriptStatusId = (int)stat };
                        transcriptRequestLogic.Modify(transcriptRequest);

                        Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateOnlineTranscriptRequest(transcriptRequest);

                        result.IsError = false;
                        result.Message = "Operation Successful!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    
        public ActionResult Clearance ()
        {
            try
            {
                 viewModel = new TranscriptProcessorViewModel();
                 TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                 viewModel.transcriptRequests = requestLogic.GetAll();
                 PopulateDropDown(viewModel);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View(viewModel);

        }
        public ActionResult UpdateClearance(long tid, long stat)
        {
            viewModel = new TranscriptProcessorViewModel();
            try
            {
                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                TranscriptRequest tRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == tid);
                tRequest.transcriptClearanceStatus = new TranscriptClearanceStatus { TranscriptClearanceStatusId = (int)stat };
                transcriptRequestLogic.Modify(tRequest);

                Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateOnlineTranscriptRequest(tRequest);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return RedirectToAction("Clearance");
        }
    
        public ActionResult ViewTranscriptDetails()
        {
            viewModel = new TranscriptProcessorViewModel();

            return View(viewModel); 
        }
        [HttpPost]
        public ActionResult ViewTranscriptDetails(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel.transcriptRequest.student.MatricNumber != null)
                {
                    PersonLogic personLogic = new PersonLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();

                    Model.Model.Student student = studentLogic.GetModelBy(s => s.Matric_Number == viewModel.transcriptRequest.student.MatricNumber);
                    if (student != null)
                    {
                        Person person = personLogic.GetModelBy(p => p.Person_Id == student.Id);
                        List<TranscriptRequest> transcriptRequests = transcriptRequestLogic.GetModelsBy(tr => tr.Student_id == student.Id);
                        if (transcriptRequests == null)
                        {
                            SetMessage("The student has not made a transcript request", Message.Category.Error);
                        }
                        else
                        {
                            viewModel.RequestDateString = transcriptRequests.LastOrDefault().DateRequested.ToShortDateString();
                            viewModel.transcriptRequests = transcriptRequests;
                            viewModel.Person = person;
                        }
                    }
                    else
                    {
                        SetMessage("Matric Number is not valid, or the student has not made a transcript request", Message.Category.Error);
                    }
                }
                else
                {
                    SetMessage("Enter Matric Number!", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }
                        
            return View(viewModel);
        }
        public ActionResult EditTranscriptDetails(long Id)
        {
            try
            {
                viewModel = new TranscriptProcessorViewModel();
                if (Id > 0)
                {
                    PersonLogic personLogic = new PersonLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest transcriptRequest = transcriptRequestLogic.GetModelBy(a => a.Transcript_Request_Id == Id);
                    Model.Model.Student student = transcriptRequest.student;
                    if (student != null)
                    {
                        Person person = personLogic.GetModelBy(p => p.Person_Id == student.Id);
                        viewModel.RequestDateString = transcriptRequest.DateRequested.ToShortDateString();
                        viewModel.transcriptRequest = transcriptRequest;
                        viewModel.Person = person;
                       
                    }
                    else
                    {
                        SetMessage("Matric Number is not valid, or the student has not made a transcript request", Message.Category.Error);
                    }
                }
                else
                {
                    SetMessage("Enter Matric Number!", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            RetainDropDownList(viewModel);
            return View(viewModel);
        }

        public ActionResult SaveTranscriptDetails(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel.transcriptRequest != null)
                {
                    PersonLogic personLogic = new PersonLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
 
                    Person person = new Person();
                    Model.Model.Student student = new Model.Model.Student();

                    person.Id = viewModel.transcriptRequest.student.Id;
                    person.LastName = viewModel.transcriptRequest.student.LastName;
                    person.FirstName = viewModel.transcriptRequest.student.FirstName;
                    person.OtherName = viewModel.transcriptRequest.student.OtherName;
                    bool isPersonModified = personLogic.Modify(person);

                    student.Id = viewModel.transcriptRequest.student.Id;
                    student.MatricNumber = viewModel.transcriptRequest.student.MatricNumber;
                    bool isStudentModified = studentLogic.Modify(student);

                    if (viewModel.transcriptRequest.DestinationCountry.Id == "OTH")
                    {
                        viewModel.transcriptRequest.DestinationState.Id = "OT"; 
                    }
                    bool isTranscriptRequestModified = transcriptRequestLogic.Modify(viewModel.transcriptRequest);

                    if (isTranscriptRequestModified && isStudentModified)
                    {
                        SetMessage("Operation Successful!", Message.Category.Information);
                        return RedirectToAction("ViewTranscriptDetails");
                    }
                    if (isTranscriptRequestModified && !isStudentModified)
                    {
                        SetMessage("Request details were modified!", Message.Category.Information);
                        return RedirectToAction("ViewTranscriptDetails");
                    }
                    if (!isTranscriptRequestModified && isStudentModified)
                    {
                        SetMessage("Student details were modified!", Message.Category.Information);
                        return RedirectToAction("ViewTranscriptDetails");
                    }
                    if (!isTranscriptRequestModified && !isStudentModified)
                    {
                        SetMessage("No changes were made!", Message.Category.Information);
                        return RedirectToAction("ViewTranscriptDetails");
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("EditTranscriptDetails",new{Id = viewModel.transcriptRequest.Id});
        }
        public ActionResult DeleteTranscriptDetails(long Id)
        {
            try
            {
                if (Id > 0)
                {
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                    PaymentLogic paymentLogic = new PaymentLogic();

                    TranscriptRequest transcriptRequest = transcriptRequestLogic.GetModelBy(tr => tr.Transcript_Request_Id == Id);
                    TranscriptRequest transcriptRequestAlt = transcriptRequest;
                    if (transcriptRequest != null)
                    {
                        using (TransactionScope scope = new TransactionScope())
                        {
                            transcriptRequestLogic.Delete(tr => tr.Transcript_Request_Id == transcriptRequest.Id);

                            //if (transcriptRequest.payment != null)
                            //{
                            //    OnlinePayment onlinePayment = onlinePaymentLogic.GetModelBy(op => op.Payment_Id == transcriptRequestAlt.payment.Id);
                            //    if (onlinePayment != null)
                            //    {
                            //        onlinePaymentLogic.Delete(op => op.Payment_Id == transcriptRequestAlt.payment.Id);
                            //    }

                            //    paymentLogic.Delete(p => p.Payment_Id == transcriptRequestAlt.payment.Id); 
                            //}
                            

                            SetMessage("Operation Successful!", Message.Category.Information);
                            scope.Complete();
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("ViewTranscriptDetails");
        }
        private void RetainDropDownList(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel.transcriptRequest != null)
                {
                    if (viewModel.transcriptRequest.DestinationCountry != null)
                    {
                        ViewBag.Country = new SelectList(viewModel.CountrySelectList, "Value", "Text", viewModel.transcriptRequest.DestinationCountry.Id);
                    }
                    else
                    {
                        ViewBag.Country = viewModel.CountrySelectList;
                    }

                    if (viewModel.transcriptRequest.DestinationCountry != null)
                    {
                        ViewBag.State = new SelectList(viewModel.StateSelectList, "Value", "Text", viewModel.transcriptRequest.DestinationState.Id);
                    }
                    else
                    {
                        ViewBag.State = viewModel.StateSelectList;
                    }

                    if (viewModel.transcriptRequest.transcriptClearanceStatus != null)
                    {
                        ViewBag.TranscriptClearanceStatus = new SelectList(viewModel.transcriptClearanceSelectList, "Value", "Text", viewModel.transcriptRequest.transcriptClearanceStatus.TranscriptClearanceStatusId);
                    }
                    else
                    {
                        ViewBag.TranscriptClearanceStatus = viewModel.transcriptClearanceSelectList;
                    }

                    if (viewModel.transcriptRequest.transcriptStatus != null)
                    {
                        ViewBag.TranscriptStatus = new SelectList(viewModel.transcriptSelectList, "Value", "Text", viewModel.transcriptRequest.transcriptStatus.TranscriptStatusId);
                    }
                    else
                    {
                        ViewBag.TranscriptStatus = viewModel.transcriptSelectList;
                    } 
                }
                else
                {
                    ViewBag.Country = viewModel.CountrySelectList;
                    ViewBag.State = viewModel.StateSelectList;
                    ViewBag.TranscriptClearanceStatus = viewModel.transcriptClearanceSelectList;
                    ViewBag.TranscriptStatus = viewModel.transcriptSelectList;
                }
                
            }
            catch (Exception)
            {
                throw;
            }
        }
        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);
                return Json(new SelectList(departments, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetDepartmentOptionByDepartment(string id, string programmeid)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Department department = new Department() { Id = Convert.ToInt32(id) };
                Programme programme = new Programme() { Id = Convert.ToInt32(programmeid) };
                DepartmentOptionLogic departmentLogic = new DepartmentOptionLogic();
                List<DepartmentOption> departmentOptions = departmentLogic.GetBy(department, programme);

                return Json(new SelectList(departmentOptions, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public JsonResult GetLocalGovernmentsByState(string id)
        {
            try
            {
                LocalGovernmentLogic lgaLogic = new LocalGovernmentLogic();

                Expression<Func<LOCAL_GOVERNMENT, bool>> selector = l => l.State_Id == id;
                List<LocalGovernment> lgas = lgaLogic.GetModelsBy(selector);

                return Json(new SelectList(lgas, "Id", "Name"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ActionResult ProcessTranscript()
        {
            viewModel = new TranscriptProcessorViewModel();
            try
            {
                ViewBag.Sex = viewModel.SexSelectList;
                ViewBag.Department = viewModel.DepartmentSelectList;
                ViewBag.State = viewModel.StateSelectList;
                ViewBag.LGA = viewModel.LocalGovernmentSelectList;
                ViewBag.Religion = viewModel.ReligionSelectList;
                ViewBag.MaritalStatus = viewModel.MaritalStatusSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = viewModel.LevelSelectList;
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Relationship = viewModel.RelationshipSelectList;
                ViewBag.ModeOfEntry = viewModel.ModeOfEntrySelectList;
                ViewBag.ModeOfStudy = viewModel.ModeOfStudySelectList;
                ViewBag.MonthOfYearGraduation = viewModel.MonthSelectListItems;
                ViewBag.YearOfGraduation = viewModel.YearSelectListItems;
                ViewBag.DepartmentOption = new SelectList(new List<Value>(), Utility.ID, Utility.TEXT);
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        [HttpPost]
        public ActionResult ProcessTranscript(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(viewModel.Student.MatricNumber))
                {
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    PersonLogic personLogic = new PersonLogic();
                    StudentLogic studentLogic = new StudentLogic();
                    Model.Model.Student student = new Model.Model.Student();
                    StudentSponsorLogic studentSponsorLogic = new StudentSponsorLogic();
                    StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                    PersonGuardianLogic personGuardianLogic = new PersonGuardianLogic();

                    student = studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber).LastOrDefault();

                    if (student != null)
                    {
                        viewModel.Student = student;
                        viewModel.StudentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                        viewModel.Person = personLogic.GetModelBy(p => p.Person_Id == student.Id);
                        viewModel.StudentSponsor = studentSponsorLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                        viewModel.StudentAcademicInformation = academicInformationLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                        viewModel.PersonGuardian = personGuardianLogic.GetModelsBy(p => p.Person_Id == student.Id).LastOrDefault();
                    }
                }

                ViewBag.Sex = viewModel.SexSelectList;
                ViewBag.Department = viewModel.DepartmentSelectList;
                ViewBag.DepartmentOption = new SelectList(new List<Value>(), Utility.ID, Utility.TEXT);
                ViewBag.State = viewModel.StateSelectList;
                ViewBag.LGA = viewModel.LocalGovernmentSelectList;
                ViewBag.Religion = viewModel.ReligionSelectList;
                ViewBag.MaritalStatus = viewModel.MaritalStatusSelectList;
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Level = viewModel.LevelSelectList;
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Relationship = viewModel.RelationshipSelectList;
                ViewBag.ModeOfEntry = viewModel.ModeOfEntrySelectList;
                ViewBag.ModeOfStudy = viewModel.ModeOfStudySelectList;
                if (viewModel.StudentLevel != null && viewModel.StudentLevel.DepartmentOption != null &&  viewModel.StudentLevel.DepartmentOption.Id > 0)
                {
                    ViewBag.DepartmentOption = new SelectList(Utility.PopulateDepartmentOptionSelectListItem(viewModel.StudentLevel.Department,viewModel.StudentLevel.Programme),Utility.VALUE,Utility.TEXT,viewModel.StudentLevel.DepartmentOption.Id);
                }
                
                if (viewModel.StudentAcademicInformation != null && !string.IsNullOrEmpty(viewModel.StudentAcademicInformation.MonthOfGraduation))
                {
                    SelectListItem monthListItem = viewModel.MonthSelectListItems.LastOrDefault(m => m.Text == viewModel.StudentAcademicInformation.MonthOfGraduation);
                    if (monthListItem != null)
                    {
                        ViewBag.MonthOfYearGraduation = new SelectList(viewModel.MonthSelectListItems, "Value", "Text", monthListItem.Value);
                    }
                    else
                    {
                        ViewBag.MonthOfYearGraduation = viewModel.MonthSelectListItems;
                    }
                    SelectListItem yearListItem = viewModel.YearSelectListItems.LastOrDefault(m => m.Value == viewModel.StudentAcademicInformation.YearOfGraduation.ToString());
                    if (yearListItem != null)
                    {
                        ViewBag.YearOfGraduation = new SelectList(viewModel.YearSelectListItems, "Value", "Text", yearListItem.Value);
                    }
                    else
                    {
                        ViewBag.YearOfGraduation = viewModel.MonthSelectListItems;
                    }
                }
                else
                {
                    ViewBag.MonthOfYearGraduation = viewModel.MonthSelectListItems;
                    ViewBag.YearOfGraduation = viewModel.YearSelectListItems;
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        public JsonResult SavePersonalDetails(string myRecordArray)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                PersonModel personDetails = serializer.Deserialize<PersonModel>(myRecordArray);

                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();

                person.FirstName = personDetails.FirstName;
                person.LastName = personDetails.LastName;
                person.OtherName = personDetails.OtherName;
                person.MobilePhone = personDetails.MobilePhone;
                person.PlaceOfBirth = personDetails.PlaceOfBirth;
                person.ContactAddress = personDetails.ContactAddress;
                person.Email = personDetails.Email;
                person.Id = !string.IsNullOrEmpty(personDetails.Id) ? Convert.ToInt32(personDetails.Id) : person.Id;

                if (!string.IsNullOrEmpty(personDetails.DOBString))
                {
                    DateTime date;
                    if (DateTime.TryParse(personDetails.DOBString, out date))
                    {
                        person.DateOfBirth = date;
                    }
                    else
                    {
                        person.DateOfBirth = DateTime.Now;
                    }
                    //string day, month, year;
                    //if (personDetails.DOBString.Contains('-'))
                    //{
                    //    day = personDetails.DOBString.Split('-')[2];
                    //    month = personDetails.DOBString.Split('-')[1];
                    //    year = personDetails.DOBString.Split('-')[0];
                    //    person.DateOfBirth = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                    //}
                    //else if(personDetails.DOBString.Contains(' '))
                    //{
                    //    day = personDetails.DOBString.Split(' ')[1];
                    //    month = GetMonthNumber(personDetails.DOBString.Split(' ')[2]);
                    //    year = personDetails.DOBString.Split(' ')[3];
                    //    person.DateOfBirth = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                    //}
                }

                person.LocalGovernment = new LocalGovernment(){ Id = Convert.ToInt32(personDetails.LGAId) };
                person.State = new State(){ Id = personDetails.StateId };
                person.Nationality = new Nationality(){ Id = 1 };
                person.Religion = new Religion() { Id = Convert.ToInt32(personDetails.ReligionId) };
                person.DateEntered = DateTime.Now;
                person.Sex = new Sex(){ Id = Convert.ToByte(personDetails.SexId) };
                person.PersonType = new PersonType(){ Id = (int)PersonTypes.Student };
                person.Role = new Role(){ Id = 5 };

                if (person.Id <= 0)
                {
                    person = personLogic.Create(person);
                }
                else
                {
                    int mStatusId;
                    if (int.TryParse(personDetails.MaritalStatusId, out mStatusId))
                    {
                        StudentLogic studentLogic = new StudentLogic();
                        Model.Model.Student student = studentLogic.GetModelBy(s => s.Person_Id == person.Id);
                        student.MaritalStatus = mStatusId > 0 ? new MaritalStatus() { Id = mStatusId } : student.MaritalStatus;

                        studentLogic.Modify(student);
                    }
                    personLogic.Modify(person);
                }

                result.Id = person.Id.ToString();
                result.IsError = false;
                result.Message = "Operation Successful!";
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private string GetMonthNumber(string monthName)
        {
            try
            {
                string[] monthNames = DateTimeFormatInfo.CurrentInfo.MonthNames;
                for (int i = 0; i < monthNames.Length; i++)
                {
                    if (monthName == monthNames[i])
                    {
                        return (i + 1).ToString();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return "0";
        }

        public JsonResult SaveStudentData(long personId, string matricNumber, int progId, int deptId, int levelId, int sessionId, int maritalStatusId, int option)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                StudentLogic studentLogic = new StudentLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                PersonLogic personLogic = new PersonLogic();

                Model.Model.Student student = studentLogic.GetModelBy(s => s.Person_Id == personId);
                if (student == null)
                {
                    student = studentLogic.GetModelBy(s => s.Matric_Number == matricNumber);
                }

                if (student == null && personLogic.GetModelBy(p => p.Person_Id == personId) != null)
                {
                    student = new Model.Model.Student();
                    student.Id = personId;
                    student.MaritalStatus = student.MaritalStatus ?? new MaritalStatus(){ Id = maritalStatusId };
                    student.MatricNumber = matricNumber;
                    student.Activated = true;
                    student.BloodGroup = student.BloodGroup ?? new BloodGroup(){ Id = 1 };
                    student.Genotype = student.Genotype ?? new Genotype(){ Id = 1 };
                    int[] hndProgrammes = { (int)Programmes.HNDEvening, (int)Programmes.HNDFullTime, (int)Programmes.HNDWeekend };
                    if (hndProgrammes.Contains(progId))
                    {
                        student.Type = new StudentType(){ Id = 2 };
                    }
                    else
                    {
                        student.Type = new StudentType() { Id = 1 };
                    }
                    student.Category = new StudentCategory(){ Id = 2 };
                    student.Status = new StudentStatus(){ Id = 1 };

                    student = studentLogic.CreateStudent(student);
                }
                else if (student != null)
                {
                    student.MatricNumber = matricNumber;
                    student.MaritalStatus = maritalStatusId <= 0 ? student.MaritalStatus : new MaritalStatus() { Id = maritalStatusId };
                    studentLogic.Modify(student);
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Parameter not set!";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

                StudentLevel studentLevel = new StudentLevel();
                studentLevel.Student = student;
                studentLevel.Department = new Department(){ Id = deptId };
                studentLevel.DepartmentOption = option > 0 ? new DepartmentOption(){ Id = option } : null;
                studentLevel.Programme = new Programme(){ Id = progId };
                studentLevel.Level = new Level(){ Id = levelId };
                studentLevel.Session = new Session(){ Id = sessionId };

                if (studentLevel.DepartmentOption != null)
                {
                    List<StudentLevel> studentLevels = studentLevelLogic.GetModelsBy(s => s.Person_Id == student.Id);
                    for (int i = 0; i < studentLevels.Count; i++)
                    {
                        studentLevels[i].DepartmentOption = new DepartmentOption() {Id = option};
                        studentLevelLogic.ModifyByStudentLevelId(studentLevels[i]);
                    }
                }

                StudentLevel existingStudentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                if (existingStudentLevel != null)
                {
                    studentLevelLogic.Modify(studentLevel);
                }
                else
                {
                    studentLevelLogic.Create(studentLevel);
                }

                result.Id = personId.ToString();
                result.IsError = false;
                result.Message = "Operation Successful!";
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddExtraCourse(long personId)
        {
            viewModel = new TranscriptProcessorViewModel();
            try
            {
                StudentLevelLogic studentLeveLogic = new StudentLevelLogic();
                StudentLogic studentLogic = new StudentLogic();

                viewModel.Student = studentLogic.GetModelBy(s => s.Person_Id == personId);
                viewModel.StudentLevel = studentLeveLogic.GetModelsBy(s => s.Person_Id == personId).LastOrDefault();

                if (viewModel.Student != null && viewModel.StudentLevel != null)
                {
                    ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.StudentLevel.Session.Id);
                    ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.StudentLevel.Programme.Id);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectList, "Value", "Text", viewModel.StudentLevel.Department.Id);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.StudentLevel.Level.Id);

                    viewModel.Programme = viewModel.StudentLevel.Programme;
                    viewModel.Department = viewModel.StudentLevel.Department;
                    viewModel.Session = viewModel.StudentLevel.Session;
                    viewModel.Level = viewModel.StudentLevel.Level;
                }
                else
                {
                    ViewBag.Session = viewModel.SessionSelectList;
                    ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = viewModel.ProgrammeSelectList;
                    ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    ViewBag.Level = viewModel.LevelSelectList;
                }

                ViewBag.ScoreGrade = viewModel.GradeSelectList;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult AddExtraCourse(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel != null)
                {
                    StudentLogic studentLogic = new StudentLogic();
                    CourseLogic courseLogic = new CourseLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();

                    List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber);
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        //RetainDropdownState(viewModel);
                        KeepDropdownState(viewModel);
                        return View(viewModel);
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    viewModel.Student = student;
                    List<StudentLevel> studentLevels = studentLevelLogic.GetModelsBy( sl => sl.Person_Id == student.Id && sl.Department_Id == viewModel.Department.Id && sl.Programme_Id == viewModel.Programme.Id);
                    if (studentLevels.Count <= 0)
                    {
                        SetMessage("Student is not in this Programme, Department!", Message.Category.Error);
                        //RetainDropdownState(viewModel);
                        KeepDropdownState(viewModel);
                        return View(viewModel);
                    }

                    StudentLevel studentLevel = studentLevels.FirstOrDefault();

                    CourseRegistrationDetailLogic registrationDetailLogic = new CourseRegistrationDetailLogic();
                    CourseRegistrationLogic registrationLogic = new CourseRegistrationLogic();

                    List<Course> departmentalCourses = new List<Course>();

                    int[] hndProgrammes = { (int)Programmes.HNDEvening, (int)Programmes.HNDFullTime, (int)Programmes.HNDWeekend };

                    if (hndProgrammes.Contains(viewModel.Programme.Id))
                    {
                        if (viewModel.Level.Id == (int)Levels.HNDI)
                        {
                            if (studentLevel.DepartmentOption != null && studentLevel.DepartmentOption.Id > 0)
                            {
                                if (!viewModel.SelectCourseInSession)
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI && c.COURSE.Session_Id == viewModel.Session.Id);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Session_Id == viewModel.Session.Id && c.Department_Option_Id == studentLevel.DepartmentOption.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI);

                                }
                                else
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Department_Option_Id == studentLevel.DepartmentOption.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI);
                                }
                                //viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI);
                            }
                            else
                            {
                                if (!viewModel.SelectCourseInSession)
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI && c.COURSE.Session_Id == viewModel.Session.Id);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI && c.Session_Id == viewModel.Session.Id);
                                }
                                else
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI);

                                }
                                //viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.HNDI);
                            }
                        }
                        else
                        {
                            if (studentLevel.DepartmentOption != null && studentLevel.DepartmentOption.Id > 0)
                            {
                                if (!viewModel.SelectCourseInSession)
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDII) && c.COURSE.Session_Id == viewModel.Session.Id);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id  && c.Session_Id == viewModel.Session.Id && c.Department_Option_Id == studentLevel.DepartmentOption.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII));

                                }
                                else
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDII));

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Department_Option_Id == studentLevel.DepartmentOption.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII));

                                }//viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII));
                            }
                            else
                            {
                                if (!viewModel.SelectCourseInSession)
                                {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDII) && c.COURSE.Session_Id == viewModel.Session.Id);

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII) && c.Session_Id == viewModel.Session.Id);

                                }
                                else {
                                    viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.HNDII));

                                    departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII));

                                }
                                //viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.HNDI || c.Level_Id == (int)Levels.HNDII));
                            }
                            
                        }
                    }
                    else
                    {
                        if (viewModel.Level.Id == (int)Levels.NDI)
                        {
                            if (!viewModel.SelectCourseInSession)
                            {
                                viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDI && c.COURSE.Session_Id == viewModel.Session.Id);

                                departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.NDI && c.Session_Id == viewModel.Session.Id);
                            }
                            else
                            {
                                viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDI);

                                departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.NDI);
                            }

                        }//viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && c.Level_Id == (int)Levels.NDI);
                    
                        else
                        {
                            
                            if (!viewModel.SelectCourseInSession)
                            {
                                viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDII) && c.COURSE.Session_Id == viewModel.Session.Id);

                                departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Session_Id == viewModel.Session.Id && c.Activated == true && (c.Level_Id == (int)Levels.NDI || c.Level_Id == (int)Levels.NDII));

                            }
                            else
                            {
                                viewModel.CourseRegistrationDetails = registrationDetailLogic.GetModelsBy(c => c.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id && c.STUDENT_COURSE_REGISTRATION.Department_Id == viewModel.Department.Id && (c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDI || c.STUDENT_COURSE_REGISTRATION.Level_Id == (int)Levels.NDII));

                                departmentalCourses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.NDI || c.Level_Id == (int)Levels.NDII));

                            }//viewModel.Courses = courseLogic.GetModelsBy(c => c.Department_Id == viewModel.Department.Id && c.Activated == true && (c.Level_Id == (int)Levels.NDI || c.Level_Id == (int)Levels.NDII));
                        }
                    }

                    if (viewModel.CourseRegistrationDetails != null && viewModel.CourseRegistrationDetails.Count > 0)
                    {
                        for (int i = 0; i < viewModel.CourseRegistrationDetails.Count; i++)
                        {
                            viewModel.CourseRegistrationDetails[i].Course.Grade = viewModel.CourseRegistrationDetails[i].Grade;
                            viewModel.CourseRegistrationDetails[i].Course.RegId = viewModel.CourseRegistrationDetails[i].Id;
                            //viewModel.CourseRegistrationDetails[i].Course.IsRegistered = true;
                            //viewModel.CourseRegistrationDetails[i].Course.IsRegistered = viewModel.CourseRegistrationDetails[i].Mode.Id == 1;
                            //viewModel.CourseRegistrationDetails[i].Course.isCarryOverCourse = viewModel.CourseRegistrationDetails[i].Mode.Id == 2;
                        }

                        //if (viewModel.Session.Id > 0)
                        //{
                        //    viewModel.Courses = viewModel.CourseRegistrationDetails.Where(c => c.Course.SessionId == viewModel.Session.Id).Select(s => s.Course).ToList();

                        //}
                        //else
                        //{
                            viewModel.Courses = viewModel.CourseRegistrationDetails.Select(s => s.Course).ToList();

                        //}
                        for (int i = 0; i < departmentalCourses.Count; i++)
                        {
                            Course departmentalCourse = departmentalCourses[i];
                            if (viewModel.CourseRegistrationDetails.LastOrDefault(c => c.Course.Id == departmentalCourse.Id && c.CourseRegistration.Level.Id == departmentalCourse.Level.Id) == null)
                            {
                                viewModel.Courses.Add(departmentalCourse);
                            }
                        }
                    }
                    else
                    {
                        viewModel.Courses = departmentalCourses;
                    }

                    //get and assign grades
                    //List<CourseRegistrationDetail> courseRegistrationDetails = registrationDetailLogic.GetModelsBy(r => r.STUDENT_COURSE_REGISTRATION.Person_Id == student.Id);

                    //for (int i = 0; i < viewModel.Courses.Count; i++)
                    //{
                    //    Course course = viewModel.Courses[i];
                    //    CourseRegistrationDetail courseRegistrationDetail = courseRegistrationDetails.LastOrDefault(c => c.Course.Id == course.Id && c.CourseRegistration.Level.Id == course.Level.Id);
                    //    if (courseRegistrationDetail != null)
                    //    {
                    //        course.Grade = courseRegistrationDetail.Grade.Trim();
                    //        course.IsRegistered = courseRegistrationDetail.Mode.Id == 1;
                    //        course.isCarryOverCourse = courseRegistrationDetail.Mode.Id == 2;
                    //    }
                    //}

                    SetCoursesDropDown(viewModel);
                    
                    //RetainDropdownState(viewModel);
                    KeepDropdownState(viewModel);

                    return View(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            //RetainDropdownState(viewModel);
            KeepDropdownState(viewModel);
            return View(viewModel);
        }

        public void SetCoursesDropDown(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel.Level.Id == (int)Levels.HNDII || viewModel.Level.Id == (int)Levels.NDII)
                {
                    if (viewModel.Courses != null && viewModel.Courses.Count > 0)
                    {
                        string[] carryOverGrades = { "CD", "D", "E", "F", "I" };
                        //viewModel.CarryOverCourses = new List<Course>();
                        viewModel.CarryOverCourses =
                            viewModel.Courses.Where(
                                c =>
                                    carryOverGrades.Contains(c.Grade) &&
                                    (c.Level.Id == (int)Levels.HNDI || c.Level.Id == (int)Levels.NDI)).ToList();
                        if (viewModel.CarryOverCourses != null && viewModel.CarryOverCourses.Count > 0)
                        {
                            //for (int i = 0; i < viewModel.CarryOverCourses.Count; i++)
                            //{
                            //    viewModel.Courses.Remove(viewModel.CarryOverCourses[i]);
                            //}

                            int cIndex = 0;
                            foreach (Course fCourse in viewModel.CarryOverCourses)
                            {
                                //fCourse.isCarryOverCourse = true;
                                ViewData["cCourse" + cIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                    fCourse.Grade);
                                cIndex++;
                            }
                        }
                    }
                      
                        if (viewModel.Level.Id == (int)Levels.HNDII)
                        {
                            if (viewModel.Courses != null && viewModel.Courses.Count > 0)
                            {
                                viewModel.Courses = viewModel.Courses.OrderBy(c => c.Level.Id).ThenBy(c => c.Semester.Id).ToList();

                                CourseRegistrationDetailLogic regDetailLogic = new CourseRegistrationDetailLogic();
                                List<CourseRegistrationDetail> regDetails = regDetailLogic.GetModelsBy(r => r.STUDENT_COURSE_REGISTRATION.Person_Id == viewModel.Student.Id &&
                                                                            r.STUDENT_COURSE_REGISTRATION.Level_Id == viewModel.Level.Id && r.STUDENT_COURSE_REGISTRATION.Session_Id == viewModel.Session.Id);

                                viewModel.FirstSemesterCourses =
                                    viewModel.Courses.Where(s => s.Semester.Id == 1 && (s.Level.Id == (int)Levels.HNDII || s.IsAlreadyRegistered(regDetails)))
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();
                                viewModel.SecondSemesterCourses =
                                    viewModel.Courses.Where(s => s.Semester.Id == 2 && (s.Level.Id == (int)Levels.HNDII || s.IsAlreadyRegistered(regDetails)))
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();

                                viewModel.FirstSemesterCoursesNd1 =
                                    viewModel.Courses.Where(s => s.Semester.Id == 1 && s.Level.Id == (int)Levels.HNDI)
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();
                                viewModel.SecondSemesterCoursesNd1 =
                                    viewModel.Courses.Where(s => s.Semester.Id == 2 && s.Level.Id == (int)Levels.HNDI)
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();

                                int fIndex = 0;
                                foreach (Course fCourse in viewModel.FirstSemesterCourses)
                                {
                                    fCourse.isCarryOverCourse = false;
                                    ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                        fCourse.Grade);
                                    fIndex++;
                                }
                                int sIndex = 0;
                                foreach (Course sCourse in viewModel.SecondSemesterCourses)
                                {
                                    sCourse.isCarryOverCourse = false;
                                    ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                        sCourse.Grade);
                                    sIndex++;
                                }

                                int fNd1Index = 0;
                                foreach (Course fCourse in viewModel.FirstSemesterCoursesNd1)
                                {
                                    ViewData["fNd1Course" + fNd1Index] = new SelectList(viewModel.GradeSelectList, Utility.VALUE,
                                        Utility.TEXT, fCourse.Grade);
                                    fNd1Index++;
                                }
                                int sNd1Index = 0;
                                foreach (Course sCourse in viewModel.SecondSemesterCoursesNd1)
                                {
                                    ViewData["sNd1Course" + sNd1Index] = new SelectList(viewModel.GradeSelectList, Utility.VALUE,
                                        Utility.TEXT, sCourse.Grade);
                                    sNd1Index++;
                                }
                            }
                        }
                        else
                        {
                            if (viewModel.Courses != null && viewModel.Courses.Count > 0)
                            {
                                viewModel.Courses = viewModel.Courses.OrderBy(c => c.Level.Id).ThenBy(c => c.Semester.Id).ToList();

                                CourseRegistrationDetailLogic regDetailLogic = new CourseRegistrationDetailLogic();
                                List<CourseRegistrationDetail> regDetails = regDetailLogic.GetModelsBy(r => r.STUDENT_COURSE_REGISTRATION.Person_Id == viewModel.Student.Id &&
                                                                            r.STUDENT_COURSE_REGISTRATION.Level_Id == viewModel.Level.Id && r.STUDENT_COURSE_REGISTRATION.Session_Id == viewModel.Session.Id);



                                viewModel.FirstSemesterCourses =
                                    viewModel.Courses.Where(s => s.Semester.Id == 1 && (s.Level.Id == (int)Levels.NDII || s.IsAlreadyRegistered(regDetails)))
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();
                                viewModel.SecondSemesterCourses =
                                    viewModel.Courses.Where(s => s.Semester.Id == 2 && (s.Level.Id == (int)Levels.NDII || s.IsAlreadyRegistered(regDetails)))
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();

                                viewModel.FirstSemesterCoursesNd1 =
                                    viewModel.Courses.Where(s => s.Semester.Id == 1 && s.Level.Id == (int)Levels.NDI)
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();
                                viewModel.SecondSemesterCoursesNd1 =
                                    viewModel.Courses.Where(s => s.Semester.Id == 2 && s.Level.Id == (int)Levels.NDI)
                                        .OrderBy(c => c.Level.Id)
                                        .ThenBy(c => c.Semester.Id)
                                        .ToList();

                                int fIndex = 0;
                                foreach (Course fCourse in viewModel.FirstSemesterCourses)
                                {
                                    ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                        fCourse.Grade);
                                    fIndex++;
                                }
                                int sIndex = 0;
                                foreach (Course sCourse in viewModel.SecondSemesterCourses)
                                {
                                    ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                        sCourse.Grade);
                                    sIndex++;
                                }

                                int fNd1Index = 0;
                                foreach (Course fCourse in viewModel.FirstSemesterCoursesNd1)
                                {
                                    ViewData["fNd1Course" + fNd1Index] = new SelectList(viewModel.GradeSelectList, Utility.VALUE,
                                        Utility.TEXT, fCourse.Grade);
                                    fNd1Index++;
                                }
                                int sNd1Index = 0;
                                foreach (Course sCourse in viewModel.SecondSemesterCoursesNd1)
                                {
                                    ViewData["sNd1Course" + sNd1Index] = new SelectList(viewModel.GradeSelectList, Utility.VALUE,
                                        Utility.TEXT, sCourse.Grade);
                                    sNd1Index++;
                                }
                            }
                        }

                    }
                
                else
                {
                    if (viewModel.Courses != null && viewModel.Courses.Count > 0)
                    {
                        viewModel.Courses = viewModel.Courses.OrderBy(c => c.Level.Id).ThenBy(c => c.Semester.Id).ToList();

                        viewModel.FirstSemesterCourses =
                            viewModel.Courses.Where(s => s.Semester.Id == 1)
                                .OrderBy(c => c.Level.Id)
                                .ThenBy(c => c.Semester.Id)
                                .ToList();
                        viewModel.SecondSemesterCourses =
                            viewModel.Courses.Where(s => s.Semester.Id == 2)
                                .OrderBy(c => c.Level.Id)
                                .ThenBy(c => c.Semester.Id)
                                .ToList();

                        int fIndex = 0;
                        foreach (Course fCourse in viewModel.FirstSemesterCourses)
                        {
                            ViewData["fCourse" + fIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                fCourse.Grade);
                            fIndex++;
                        }
                        int sIndex = 0;
                        foreach (Course sCourse in viewModel.SecondSemesterCourses)
                        {
                            ViewData["sCourse" + sIndex] = new SelectList(viewModel.GradeSelectList, Utility.VALUE, Utility.TEXT,
                                sCourse.Grade);
                            sIndex++;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void KeepDropdownState(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                if (viewModel.Session != null && viewModel.Programme != null && viewModel.Department != null &&
                viewModel.Level != null)
                {
                    ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                    //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.Programme.Id);
                    ViewBag.Department = new SelectList(viewModel.DepartmentSelectList, "Value", "Text", viewModel.Department.Id);
                    ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.Level.Id);
                }
                else
                {
                    ViewBag.Session = viewModel.SessionSelectList;
                    //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    ViewBag.Programme = viewModel.ProgrammeSelectList;
                    ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                    ViewBag.Level = viewModel.LevelSelectList;
                }
                ViewBag.ScoreGrade = viewModel.GradeSelectList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void RetainDropdownState(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                ViewBag.Session = viewModel.SessionSelectList;
                ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                ViewBag.Level = viewModel.LevelSelectList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult SaveAddedCourse(TranscriptProcessorViewModel viewModel)
        {
            StudentLogic studentLogic = new StudentLogic();
            List<Model.Model.Student> students = studentLogic.GetModelsBy(s => s.Matric_Number == viewModel.Student.MatricNumber);
            try
            {
                if (viewModel != null)
                {
                    viewModel.Courses = new List<Course>();
                    if (viewModel.FirstSemesterCourses != null)
                    {
                        viewModel.Courses.AddRange(viewModel.FirstSemesterCourses);
                    }
                    if (viewModel.SecondSemesterCourses != null)
                    {
                        viewModel.Courses.AddRange(viewModel.SecondSemesterCourses);
                    }
                    if (viewModel.FirstSemesterCoursesNd1 != null)
                    {
                        viewModel.Courses.AddRange(viewModel.FirstSemesterCoursesNd1);
                    }
                    if (viewModel.SecondSemesterCoursesNd1 != null)
                    {
                        viewModel.Courses.AddRange(viewModel.SecondSemesterCoursesNd1);
                    }
                    if (viewModel.CarryOverCourses != null)
                    {
                        viewModel.Courses.AddRange(viewModel.CarryOverCourses);
                    }
                    
                    //viewModel.Courses.AddRange(viewModel.CarryOverCourses);

                    CourseRegistrationLogic courseRegistrationLogic = new CourseRegistrationLogic();
                    StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                    CourseRegistrationDetailLogic courseRegistrationDetailLogic = new CourseRegistrationDetailLogic();

                    CourseRegistrationDetail courseRegistrationDetail = new CourseRegistrationDetail();
                    CourseMode carryOverCourseMode = new CourseMode() { Id = 2 };
                    CourseMode firstAttemprCourseMode = new CourseMode() { Id = 1 };

                    List<StudentLevel> studentLevelList = new List<StudentLevel>();

                    
                    if (students.Count != 1)
                    {
                        SetMessage("Duplicate Matric Number OR Matric Number does not exist!", Message.Category.Error);
                        KeepDropdownState(viewModel);

                        //return View("AddExtraCourse", viewModel);
                        return RedirectToAction("AddExtraCourse", new { personId = students.LastOrDefault().Id });
                    }

                    Model.Model.Student student = students.FirstOrDefault();
                    studentLevelList = studentLevelLogic.GetModelsBy( p => p.Person_Id == student.Id && p.Department_Id == viewModel.Department.Id && p.Programme_Id == viewModel.Programme.Id && 
                                                                        p.Session_Id == viewModel.Session.Id && p.Level_Id == viewModel.Level.Id);
                    StudentLevel studentLevel = studentLevelList.LastOrDefault();
                    if (studentLevelList.Count == 0)
                    {
                        studentLevel = new StudentLevel();
                        studentLevel.Student = student;
                        studentLevel.Department = viewModel.Department;
                        studentLevel.Programme = viewModel.Programme;
                        studentLevel.Level = viewModel.Level;
                        studentLevel.Session = viewModel.Session;

                        StudentLevel newStudentLevel = studentLevelLogic.Create(studentLevel);
                        studentLevel.Id = newStudentLevel.Id;
                    }

                    CourseRegistration courseRegistration = courseRegistrationLogic.GetModelsBy( p => p.Person_Id == studentLevel.Student.Id && p.Programme_Id == studentLevel.Programme.Id &&
                                                       p.Department_Id == studentLevel.Department.Id && p.Session_Id == studentLevel.Session.Id && p.Level_Id == viewModel.Level.Id).LastOrDefault();

                    if (courseRegistration == null)
                    {
                        courseRegistration = new CourseRegistration();
                        courseRegistration.Level = studentLevel.Level;
                        courseRegistration.Session = studentLevel.Session;
                        courseRegistration.Programme = studentLevel.Programme;
                        courseRegistration.Department = studentLevel.Department;
                        courseRegistration.Student = studentLevel.Student;
                        courseRegistration = courseRegistrationLogic.Create(courseRegistration);
                    }

                    for (int i = 0; i < viewModel.Courses.Count; i++)
                    {
                        long courseId = viewModel.Courses[i].Id;
                        CourseRegistrationDetail courseRegistrationDetailCheck = courseRegistrationDetailLogic.GetModelsBy( crd => crd.Course_Id == courseId && 
                                                                                    crd.Student_Course_Registration_Id == courseRegistration.Id).LastOrDefault();
                        if (courseRegistrationDetailCheck == null)
                        {
                            if (viewModel.Courses[i].IsRegistered || viewModel.Courses[i].isCarryOverCourse)
                            {
                                courseRegistrationDetail.CourseRegistration = courseRegistration;
                                courseRegistrationDetail.Course = viewModel.Courses[i];
                                if (viewModel.Courses[i].isCarryOverCourse)
                                {
                                    courseRegistrationDetail.Mode = carryOverCourseMode;
                                }
                                else
                                {
                                    courseRegistrationDetail.Mode = firstAttemprCourseMode;
                                }

                                if (string.IsNullOrEmpty(viewModel.Courses[i].Grade))
                                {
                                    continue;
                                }

                                AssignScores(courseRegistrationDetail, viewModel.Courses[i].Grade);

                                courseRegistrationDetail.Semester = viewModel.Courses[i].Semester;
                                courseRegistrationDetail.CourseUnit = viewModel.Courses[i].Unit;
                                //courseRegistrationDetail.TestScore = viewModel.Courses[i].TestScore;
                                //courseRegistrationDetail.ExamScore = viewModel.Courses[i].ExamScore;
                                courseRegistrationDetail.Grade = viewModel.Courses[i].Grade.Trim();
                                courseRegistrationDetail = courseRegistrationDetailLogic.Create(courseRegistrationDetail);

                            }
                        }
                        else
                        {
                            if (viewModel.Courses[i].IsRegistered || viewModel.Courses[i].isCarryOverCourse)
                            {
                                if (viewModel.Courses[i].isCarryOverCourse)
                                {
                                    courseRegistrationDetailCheck.Mode = carryOverCourseMode;
                                }
                                else
                                {
                                    courseRegistrationDetailCheck.Mode = firstAttemprCourseMode;
                                }

                                if (string.IsNullOrEmpty(viewModel.Courses[i].Grade))
                                {
                                    continue;
                                }

                                AssignScores(courseRegistrationDetailCheck, viewModel.Courses[i].Grade);

                                courseRegistrationDetailCheck.Semester = viewModel.Courses[i].Semester;
                                courseRegistrationDetailCheck.CourseUnit = viewModel.Courses[i].Unit;
                                //courseRegistrationDetail.TestScore = viewModel.Courses[i].TestScore;
                                //courseRegistrationDetail.ExamScore = viewModel.Courses[i].ExamScore;
                                courseRegistrationDetailCheck.Grade = viewModel.Courses[i].Grade.Trim();
                                courseRegistrationDetailLogic.Modify(courseRegistrationDetailCheck);

                            }
                        }
                    }

                    SetMessage("Operation Successful!", Message.Category.Information);

                    //RetainDropdownState(viewModel);
                    if (viewModel.Session != null && viewModel.Programme != null && viewModel.Department != null && viewModel.Level != null)
                    {
                        ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                        //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                        ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.Programme.Id);
                        ViewBag.Department = new SelectList(viewModel.DepartmentSelectList, "Value", "Text", viewModel.Department.Id);
                        ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.Level.Id);
                    }
                    else
                    {
                        ViewBag.Session = viewModel.SessionSelectList;
                        //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                        ViewBag.Programme = viewModel.ProgrammeSelectList;
                        ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                        ViewBag.Level = viewModel.LevelSelectList;
                    }
                    ViewBag.ScoreGrade = viewModel.GradeSelectList;

                    //SetDropdown(viewModel);

                    //return View("AddExtraCourse", viewModel);
                    return RedirectToAction("AddExtraCourse", new {personId = student.Id});
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occured!" + ex.Message, Message.Category.Error);
            }

            //RetainDropdownState(viewModel);
            if (viewModel.Session != null && viewModel.Programme != null && viewModel.Department != null && viewModel.Level != null)
            {
                ViewBag.Session = new SelectList(viewModel.SessionSelectList, "Value", "Text", viewModel.Session.Id);
                //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                ViewBag.Programme = new SelectList(viewModel.ProgrammeSelectList, "Value", "Text", viewModel.Programme.Id);
                ViewBag.Department = new SelectList(viewModel.DepartmentSelectList, "Value", "Text", viewModel.Department.Id);
                ViewBag.Level = new SelectList(viewModel.LevelSelectList, "Value", "Text", viewModel.Level.Id);
            }
            else
            {
                ViewBag.Session = viewModel.SessionSelectList;
                //ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                ViewBag.Programme = viewModel.ProgrammeSelectList;
                ViewBag.Department = new SelectList(new List<Department>(), "Id", "Name");
                ViewBag.Level = viewModel.LevelSelectList;
            }
            ViewBag.ScoreGrade = viewModel.GradeSelectList;

            //SetDropdown(viewModel);

            //return View("AddExtraCourse", viewModel);
            return RedirectToAction("AddExtraCourse", new { personId = students.LastOrDefault().Id });
        }

        private void AssignScores(CourseRegistrationDetail courseRegistrationDetail, string grade)
        {
            try
            {
                switch (grade)
                {
                    case "A":
                        courseRegistrationDetail.TestScore = 30M;
                        courseRegistrationDetail.ExamScore = 50M;
                        break;
                    case "AB":
                        courseRegistrationDetail.TestScore = 30M;
                        courseRegistrationDetail.ExamScore = 40M;
                        break;
                    case "B":
                        courseRegistrationDetail.TestScore = 20M;
                        courseRegistrationDetail.ExamScore = 40M;
                        break;
                    case "BC":
                        courseRegistrationDetail.TestScore = 20M;
                        courseRegistrationDetail.ExamScore = 30M;
                        break;
                    case "C":
                        courseRegistrationDetail.TestScore = 20M;
                        courseRegistrationDetail.ExamScore = 20M;
                        break;
                    case "CD":
                        courseRegistrationDetail.TestScore = 10M;
                        courseRegistrationDetail.ExamScore = 20M;
                        break;
                    case "D":
                        courseRegistrationDetail.TestScore = 10M;
                        courseRegistrationDetail.ExamScore = 10M;
                        break;
                    case "E":
                        courseRegistrationDetail.TestScore = 5M;
                        courseRegistrationDetail.ExamScore = 5M;
                        break;
                    case "F":
                        courseRegistrationDetail.TestScore = 0M;
                        courseRegistrationDetail.ExamScore = 0M;
                        break;
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public string GetGrade(decimal score)
        {
            string grade = null;
            try
            {
                if (score >= 0M && score  <= 9M)
                {
                    grade = "F";
                }
                else if (score >= 10M && score <= 19M)
                {
                    grade = "E";
                }
                else if (score >= 20M && score <= 29M)
                {
                    grade = "D";
                }
                else if (score >= 30M && score <= 39M)
                {
                    grade = "CD";
                }
                else if (score >= 40M && score <= 49M)
                {
                    grade = "C";
                }
                else if (score >= 50M && score <= 59M)
                {
                    grade = "BC";
                }
                else if (score >= 60M && score <= 69M)
                {
                    grade = "B";
                }
                else if (score >= 70M && score <= 79M)
                {
                    grade = "AB";
                }
                else if (score >= 80M && score <= 100M)
                {
                    grade = "A";
                }
                else
                {
                    grade = null;
                }
            }
            catch (Exception)
            {
                throw;
            }

            return grade;
        }

        public JsonResult SaveSponsorDetails(string myRecordArray)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                PersonModel personDetails = serializer.Deserialize<PersonModel>(myRecordArray);

                StudentSponsor studentSponsor = new StudentSponsor();
                PersonGuardian personGuardian = new PersonGuardian();

                StudentSponsorLogic studentSponsorLogic = new StudentSponsorLogic();
                PersonGuardianLogic personGuardianLogic = new PersonGuardianLogic();
                
                long personId = !string.IsNullOrEmpty(personDetails.Id) ? Convert.ToInt32(personDetails.Id)  : 0;

                studentSponsor.ContactAddress = personDetails.SponsorAddress;
                studentSponsor.Email = personDetails.SponsorEmail;
                studentSponsor.MobilePhone = personDetails.SponsorMobilePhone;
                studentSponsor.Name = personDetails.SponsorName;
                studentSponsor.Relationship = !string.IsNullOrEmpty(personDetails.SponsorRelationship) ? new Relationship(){ Id = Convert.ToInt32(personDetails.SponsorRelationship)} : null;
                studentSponsor.Student = !string.IsNullOrEmpty(personDetails.Id) ? new Model.Model.Student() { Id = Convert.ToInt32(personDetails.Id) } : null;

                StudentSponsor existingStudentSponsor = studentSponsorLogic.GetModelsBy(s => s.Person_Id == personId).LastOrDefault();

                if (existingStudentSponsor == null)
                {
                    studentSponsorLogic.Create(studentSponsor);
                }
                else
                {
                    studentSponsorLogic.Modify(studentSponsor);
                }

                personGuardian.Address = personDetails.GuardianAddress;
                personGuardian.Email = personDetails.GuardianEmail;
                personGuardian.GuardianName = personDetails.GuardianName;
                personGuardian.MobilePhone = personDetails.GuardianMobilePhone;
                personGuardian.Occupation = personDetails.GuardianOccupation;
                personGuardian.Person = !string.IsNullOrEmpty(personDetails.Id) ? new Model.Model.Person() { Id = Convert.ToInt32(personDetails.Id) } : null;

                PersonGuardian existingPersonGuardian = personGuardianLogic.GetModelsBy(p => p.Person_Id == personId).LastOrDefault();

                if (existingPersonGuardian == null)
                {
                    personGuardianLogic.Create(personGuardian);
                }
                else
                {
                    personGuardian.Id = existingPersonGuardian.Id;
                    personGuardianLogic.Modify(personGuardian);
                }

                result.Id = personId.ToString();
                result.IsError = false;
                result.Message = "Operation Successful!";
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveAcademicDetails(string myRecordArray)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                PersonModel personDetails = serializer.Deserialize<PersonModel>(myRecordArray);

                long personId = !string.IsNullOrEmpty(personDetails.Id) ? Convert.ToInt32(personDetails.Id) : 0;

                StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();

                StudentLevel studentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == personId).LastOrDefault();
                StudentAcademicInformation academicInformation = new StudentAcademicInformation();

                if (!string.IsNullOrEmpty(personDetails.AwardDate))
                {
                    DateTime date;
                    if (DateTime.TryParse(personDetails.AwardDate, out date))
                    {
                        academicInformation.AwardDate = date;
                    }
                    else
                    {
                        academicInformation.AwardDate = DateTime.Now;
                    }
                    //academicInformation.AwardDate = new DateTime(Convert.ToInt32(personDetails.AwardDate.Split('-')[0]), Convert.ToInt32(personDetails.AwardDate.Split('-')[1]), Convert.ToInt32(personDetails.AwardDate.Split('-')[2]));
                    //string day, month, year;
                    //if (personDetails.AwardDate.Contains('-'))
                    //{
                    //    day = personDetails.AwardDate.Split('-')[2];
                    //    month = personDetails.AwardDate.Split('-')[1];
                    //    year = personDetails.AwardDate.Split('-')[0];

                    //    academicInformation.AwardDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                    //}
                    //else if (personDetails.AwardDate.Contains(' '))
                    //{
                    //    day = personDetails.AwardDate.Split(' ')[1];
                    //    month = GetMonthNumber(personDetails.AwardDate.Split(' ')[2]);
                    //    year = personDetails.AwardDate.Split(' ')[3];

                    //    academicInformation.AwardDate = new DateTime(Convert.ToInt32(year), Convert.ToInt32(month), Convert.ToInt32(day));
                    //}
                }
                //if (!string.IsNullOrEmpty(personDetails.DateOfEntry))
                //{
                //    //academicInformation.DateOfEntry = new DateTime(Convert.ToInt32(personDetails.DateOfEntry.Split('-')[0]), Convert.ToInt32(personDetails.DateOfEntry.Split('-')[1]), Convert.ToInt32(personDetails.DateOfEntry.Split('-')[2]));

                //}
                academicInformation.LastSchoolAttended = personDetails.LastSchoolAttended;
                academicInformation.ModeOfEntry = !string.IsNullOrEmpty(personDetails.ModeOfEntry) ? new ModeOfEntry() { Id = Convert.ToInt32(personDetails.ModeOfEntry) } : null;
                academicInformation.ModeOfStudy = !string.IsNullOrEmpty(personDetails.ModeOfStudy) ? new ModeOfStudy() { Id = Convert.ToInt32(personDetails.ModeOfStudy) } : null;
                academicInformation.StudentAward = personDetails.StudentAward;
                academicInformation.Student = new Model.Model.Student() { Id = personId };
                academicInformation.Level = studentLevel != null ? studentLevel.Level : null;
                if (!string.IsNullOrEmpty(personDetails.DateOfEntry))
                {
                    academicInformation.YearOfAdmission = personDetails.DateOfEntry;
                }
                //academicInformation.YearOfGraduation = Convert.ToInt32(personDetails.DateOfEntry.Split('/')[0]) + 2;
                academicInformation.YearOfGraduation = Convert.ToInt32(personDetails.YearOfGraduation);
                academicInformation.MonthOfGraduation = personDetails.MonthOfGraduation;

                StudentAcademicInformation existingAcademicInformation = academicInformationLogic.GetModelsBy(a => a.Person_Id == personId).LastOrDefault();

                if (existingAcademicInformation == null)
                {
                    academicInformationLogic.Create(academicInformation);
                }
                else
                {
                    existingAcademicInformation.AwardDate = academicInformation.AwardDate;
                    existingAcademicInformation.LastSchoolAttended = academicInformation.LastSchoolAttended;
                    existingAcademicInformation.ModeOfEntry = academicInformation.ModeOfEntry;
                    existingAcademicInformation.ModeOfStudy = academicInformation.ModeOfStudy;
                    existingAcademicInformation.StudentAward = academicInformation.StudentAward;
                    existingAcademicInformation.Student = academicInformation.Student;
                    existingAcademicInformation.Level = academicInformation.Level;
                    existingAcademicInformation.YearOfAdmission = academicInformation.YearOfAdmission;
                    existingAcademicInformation.YearOfGraduation = academicInformation.YearOfGraduation;
                    existingAcademicInformation.MonthOfGraduation = academicInformation.MonthOfGraduation;

                    academicInformationLogic.Modify(existingAcademicInformation);
                }

                SetProcessedTranscriptRequest(personId);

                result.Id = personId.ToString();
                result.IsError = false;
                result.Message = "Operation Successful!";
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void SetProcessedTranscriptRequest(long personId)
        {
            try
            {
                if (personId > 0)
                {
                    TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                    List<TranscriptRequest> requests = requestLogic.GetModelsBy(t => t.Student_id == personId);

                    for (int i = 0; i < requests.Count; i++)
                    {
                        TranscriptRequest request = requests[i];
                        request.Processed = true;

                        requestLogic.Modify(request);

                        Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateOnlineTranscriptRequest(request);
                    }
                }
            }
            catch (Exception)
            {
                return;
            }
        }

        public JsonResult VerifyTranscriptRequest(long requestId)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (requestId > 0)
                {
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest transcriptRequest = new TranscriptRequest();
                    transcriptRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == requestId);

                    if (transcriptRequest != null)
                    {
                        transcriptRequest.Verified = true;

                        transcriptRequestLogic.Modify(transcriptRequest);

                        Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateOnlineTranscriptRequest(transcriptRequest);

                        result.IsError = false;
                        result.Message = "Operation Successful!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetUrl(long requestId, string urlType)
        {
            JsonResultModel result = new JsonResultModel();
            try
            {
                if (requestId > 0)
                {
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest transcriptRequest = new TranscriptRequest();
                    transcriptRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == requestId);

                    if (transcriptRequest != null)
                    {
                        result.Url = urlType == "statement" ? transcriptRequest.StatementOfResultUrl : transcriptRequest.AlumniReceiptUrl;
                        result.IsError = false;
                        result.Message = "Operation Successful!";
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = "Error! " + ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateTranscriptRequest()
        {
            viewModel = new TranscriptProcessorViewModel();
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult UpdateTranscriptRequest(TranscriptProcessorViewModel viewModel)
        {
            try
            {
                DateTime dateFrom = new DateTime();
                DateTime dateTo = new DateTime();

                if (!DateTime.TryParse(viewModel.DateFrom, out dateFrom))
                {
                    dateFrom = DateTime.Now;
                }
                if (!DateTime.TryParse(viewModel.DateTo, out dateTo))
                {
                    dateTo = DateTime.Now;
                }

                //TranscriptRequestUpdateLogic requestUpdateLogic = new TranscriptRequestUpdateLogic();
                //TranscriptRequestUpdate requestUpdate = requestUpdateLogic.GetAll().LastOrDefault();
                bool status = Abundance_Nk.Web.Models.UpdateTranscriptRequest.UpdateTransciptRequest(dateFrom, dateTo);

                if (status)
                {
                    SetMessage("Operation Successful!", Message.Category.Information);
                }
                else
                {
                    SetMessage("Update failed!", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View();
        }
        public ActionResult UpdateRRRBulk()
        {
            try
            {
                BackgroundWorker m = new BackgroundWorker();
                m.DoWork += m_DoWork;
                var task1 = Task.Run(() => m.RunWorkerAsync());
                RedirectToAction("Index");
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("Payments", "Transcripts", new { Area = "Common"});
        }

        void m_DoWork(object sender, DoWorkEventArgs e)
        {
            RemitaSettings settings = new RemitaSettings();
            RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
            settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 1);
            RemitaResponse remitaResponse = new RemitaResponse();
            RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
            string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
            RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
            List<RemitaPayment> remitaPayments = remitaPaymentLogic.GetModelsBy(m => m.Status.Contains("025"));
            foreach (RemitaPayment remitaPayment in remitaPayments)
            {
                remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
                if (remitaResponse != null && remitaResponse.Status != null)
                {
                    remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                    remitaPaymentLogic.Modify(remitaPayment);
                }
            }

        }
        public ActionResult UpdateTranscriptRemitaPayment()
        {
            try
            {
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();

                List<TranscriptRequest> requests = requestLogic.GetModelsBy(t => t.Transcript_Status_Id == 1);

                for (int i = 0; i < requests.Count; i++)
                {
                    TranscriptRequest request = requests[i];
                    PaymentLogic paymentLogic = new PaymentLogic();
                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();

                    var payment = paymentLogic.GetBy(request.payment.InvoiceNumber);
                    if (payment != null)
                    {
                        RemitaPayment remitaPayment = remitaPaymentLogic.GetModelBy(p => p.Payment_Id == payment.Id);
                        if (remitaPayment != null)
                        {
                            RemitaPayementProcessor remitaPayementProcessor = new RemitaPayementProcessor("587460");
                            RemitaPayment remita = remitaPayementProcessor.GetStatus(remitaPayment.OrderId);

                            if (remita != null && (remita.Status.Contains("00") || remita.Status.Contains("01")) && remita.payment != null && remita.payment.Id > 0)
                            {
                                if (request.transcriptStatus.TranscriptStatusId == 1)
                                {
                                    OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();

                                    onlinePaymentLogic.UpdateTransactionNumber(payment, remitaPayment.OrderId);

                                    request.transcriptStatus.TranscriptStatusId = 2;

                                    requestLogic.Modify(request);
                                }
                            }
                        }
                    }
                }
                
                SetMessage("Task Completed.", Message.Category.Information);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("Index");
        }
        public JsonResult GetStudentCgpa(string matricNumber)
        {
            string studentMatricNumber = null;
            try
            {
                if (!string.IsNullOrEmpty(matricNumber))
                {

                    StudentLogic studentLogic = new StudentLogic();
                    Model.Model.Student student = new Model.Model.Student();
                    StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                    student = studentLogic.GetModelsBy(s => s.Matric_Number == matricNumber).LastOrDefault();
                    if (student != null)
                    {
                        var studentAcademicInformation = academicInformationLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                        if (studentAcademicInformation != null)
                        {
                            studentMatricNumber = studentAcademicInformation.CGPA.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(studentMatricNumber, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SaveEditedCgpa(string cgpa, string matricNumber)
        {
            var modifiedCgpa = false;
            try
            {
                if (!string.IsNullOrEmpty(matricNumber) && !string.IsNullOrEmpty(cgpa))
                {

                    StudentLogic studentLogic = new StudentLogic();
                    Model.Model.Student student = new Model.Model.Student();
                    StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                    var userLogic = new UserLogic();
                    student = studentLogic.GetModelsBy(s => s.Matric_Number == matricNumber).LastOrDefault();
                    if (student != null)
                    {
                        var studentAcademicInformation = academicInformationLogic.GetModelsBy(s => s.Person_Id == student.Id).LastOrDefault();
                        if (studentAcademicInformation != null)
                        {
                            decimal studentcgpa = (decimal) 0.00;
                            if (studentAcademicInformation.CGPA != null)
                            {
                               studentcgpa = (decimal)studentAcademicInformation.CGPA;
                            }
                            studentAcademicInformation.CGPA = Convert.ToDecimal(cgpa);
                            studentAcademicInformation.IsModified = true;
                            modifiedCgpa = academicInformationLogic.Modify(studentAcademicInformation);
                            StudentAcademicInformationAuditLogic studentAcademicInformationAuditLogic = new StudentAcademicInformationAuditLogic();
                            User user = userLogic.GetModelBy(p => p.User_Name == User.Identity.Name);
                            StudentAcademicInformationAudit studentAcademicInformationAudit =
                                new StudentAcademicInformationAudit
                                {
                                    OldCgpa = studentcgpa,
                                    NewCgpa = Convert.ToDecimal(cgpa),
                                    Operation = "Modify",
                                    Action = "Modified Student Payment (StudentPayment controller)",
                                    Client = Request.LogonUserIdentity.Name + " (" + HttpContext.Request.UserHostAddress + ")",
                                    UserId = user.Id,
                                    Time = DateTime.Now,
                                    StudentAcademicInformation = studentAcademicInformation
                                };
                            studentAcademicInformationAuditLogic.Create(studentAcademicInformationAudit);



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return Json(modifiedCgpa, JsonRequestBehavior.AllowGet);
        }
    }
}
