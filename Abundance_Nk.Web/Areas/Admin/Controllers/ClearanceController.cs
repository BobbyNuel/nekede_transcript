﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;

namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class ClearanceController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string VALUE = "Value";
        private const string TEXT = "Text";
        private Abundance_Nk_NekedeEntities db = new Abundance_Nk_NekedeEntities();
        private ClearanceViewModel viewmodel;

        public ActionResult Index(string sortOrder)
        {
            viewmodel = new ClearanceViewModel();
            if (TempData["ClearanceViewModel"] != null)
            {
                viewmodel = (ClearanceViewModel)TempData["ClearanceViewModel"];
            }
            string sortDesc = sortOrder;
            ViewBag.CurrentSort = sortOrder;
            ViewBag.FullName = String.IsNullOrEmpty(sortDesc) ? "name_desc" : "";
            ViewBag.Number = String.IsNullOrEmpty(sortDesc) ? "Number_desc" : "";
            ViewBag.Programme = String.IsNullOrEmpty(sortDesc) ? "Programme_desc" : "";
            ViewBag.Department = String.IsNullOrEmpty(sortDesc) ? "Department_desc" : "";

            try
            {
                switch (sortDesc)
                {
                    case "name_desc":
                        viewmodel.appliedCourseList = viewmodel.appliedCourseList.OrderByDescending(s => s.Person.FullName).ToList();
                        break;
                    case "Number_desc":
                        viewmodel.appliedCourseList = viewmodel.appliedCourseList.OrderByDescending(s => s.ApplicationForm.Number).ToList();
                        break;
                    case "Programme_desc":
                        viewmodel.appliedCourseList = viewmodel.appliedCourseList.OrderByDescending(s => s.Programme.Name).ToList();
                        break;
                    case "Department_desc":
                        viewmodel.appliedCourseList = viewmodel.appliedCourseList.OrderByDescending(s => s.Department.Name).ToList();
                        break;
                    default:
                        viewmodel.appliedCourseList = viewmodel.appliedCourseList.OrderByDescending(s => s.ApplicationForm.Id).ToList();
                        break;
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }



            TempData["ClearanceViewModel"] = viewmodel;
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult Index(ClearanceViewModel vModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (vModel.ApplicationNumber != null)
                    {
                        ClearanceViewModel viewModel = new ClearanceViewModel();

                        List<ApplicationForm> applicationForm = new List<ApplicationForm>();
                        ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

                        List<Person> personList = new List<Person>();
                        PersonLogic personLogic = new PersonLogic();

                        personList = personLogic.GetModelsBy(n => n.Last_Name == vModel.ApplicationNumber || n.First_Name == vModel.ApplicationNumber || n.Other_Name == vModel.ApplicationNumber || n.Mobile_Phone == vModel.ApplicationNumber);
                        if (personList != null && personList.Count > 0)
                        {
                            foreach (Person applicant in personList)
                            {
                                ApplicationForm appForm = applicationFormLogic.GetModelBy(m => m.Person_Id == applicant.Id);
                                applicationForm.Add(appForm);
                            }

                        }
                        else
                        {

                            ApplicationForm appForm = applicationFormLogic.GetModelBy(m => m.Application_Form_Number == vModel.ApplicationNumber || m.Application_Exam_Number == vModel.ApplicationNumber);
                            applicationForm.Add(appForm);
                        }




                        if (applicationForm != null && applicationForm.Count > 0)
                        {
                            List<AppliedCourse> appCourse = new List<AppliedCourse>();
                            List<OLevelResult> result = new List<OLevelResult>();
                            List<OLevelResultDetail> resultdetail = new List<OLevelResultDetail>();
                            List<ApplicationForm> applicantForm = new List<ApplicationForm>();

                            AppliedCourse appliedcourse = new AppliedCourse();
                            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                            foreach (ApplicationForm applicant in applicationForm)
                            {
                                if (applicant != null && applicant.Person.Id != null)
                                {
                                    appliedcourse = appliedCourseLogic.GetModelBy(n => n.Person_Id == applicant.Person.Id);
                                    if (appliedcourse != null && appliedcourse.Department.Id > 0)
                                    {
                                        applicantForm.Add(applicant);
                                        appCourse.Add(appliedcourse);

                                    }
                                }

                            }

                            viewModel.applicationFormList = applicantForm;
                            viewModel.appliedCourseList = appCourse;
                            TempData["ClearanceViewModel"] = viewModel;


                            return View(viewModel);


                        }
                        else
                        {
                            SetMessage("Record does not exist! ", Message.Category.Error);
                            return View(vModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(vModel);
        }

        public ActionResult View(long Id)
        {
            viewmodel = new ClearanceViewModel();
            try
            {
                AppliedCourse appliedCourse = new AppliedCourse();
                AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();

                long personid = Id;
                if (personid > 0)
                {
                    appliedCourse = appliedCourseLogic.GetModelBy(x => x.Person_Id == personid);
                    if (appliedCourse != null && appliedCourse.Person.Id > 0)
                    {
                        viewmodel.appliedCourse = appliedCourse;
                        viewmodel.person = appliedCourse.Person;
                        viewmodel.LoadApplicantResult(appliedCourse.Person);

                        //Check if applicant has been cleared previously
                        ClearanceLog clearance = new ClearanceLog();
                        ClearancelogLogic clearanceLogic = new ClearancelogLogic();
                        clearance = clearanceLogic.GetModelsBy(a => a.Application_Form_Id == appliedCourse.ApplicationForm.Id).FirstOrDefault();
                        if (clearance != null && clearance.ClearanceId > 0)
                        {
                            viewmodel.clearanceDetails = clearance;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }



            TempData["ClearanceViewModel"] = viewmodel;
            return View(viewmodel);
        }

        public ActionResult Clear()
        {
            viewmodel = new ClearanceViewModel();
            if (TempData["ClearanceViewModel"] != null)
            {
                viewmodel = (ClearanceViewModel)TempData["ClearanceViewModel"];
            }
            try
            {
                //Check if applicant met requirements
                ApplicationForm newApplicationForm = new ApplicationForm();
                newApplicationForm = viewmodel.appliedCourse.ApplicationForm;
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                AdmissionCriteriaLogic admissionCriteriaLogic = new AdmissionCriteriaLogic();
                string rejectReason = admissionCriteriaLogic.ClearStudent(viewmodel.appliedCourse);
                if (string.IsNullOrEmpty(rejectReason))
                {
                    newApplicationForm.Rejected = false;
                    if (!applicationFormLogic.SetRejectReason(newApplicationForm))
                    {
                        throw new Exception("Reject Reason not set! Please try again.");
                    }

                }
                else
                {
                    newApplicationForm.Rejected = true;
                    newApplicationForm.RejectReason = rejectReason;
                    if (!applicationFormLogic.SetRejectReason(newApplicationForm))
                    {
                        throw new Exception("Reject Reason not set! Please try again.");
                    }

                }

                Abundance_Nk.Model.Model.User loggedInUser = new User();
                UserLogic userLogic = new UserLogic();
                loggedInUser = userLogic.GetModelBy(m => m.User_Name == User.Identity.Name);
                ClearanceLog clearance = new ClearanceLog();
                ClearancelogLogic clearanceLogic = new ClearancelogLogic();

                clearance.applicationForm = newApplicationForm;
                clearance.Date = DateTime.Now;
                if (newApplicationForm.Rejected == false)
                {
                    clearance.status = true;
                }
                else
                {
                    clearance.status = false;
                }
                clearance.User = loggedInUser;
                clearanceLogic.Create(clearance);



                viewmodel.applicationForm = newApplicationForm;
                viewmodel.ClearanceOfficer = loggedInUser;
                viewmodel.clearanceDetails = clearance;
                TempData["ClearanceViewModel"] = viewmodel;
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                return RedirectToAction("index");
            }
            return View(viewmodel);
        }

        [HttpPost]
        public ActionResult VerifyPayment(ClearanceViewModel vModel)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    if (vModel.ConfrimationOrderNo != null)
                    {
                        Payment payment = InvalidConfirmationOrderNumber(vModel.ConfrimationOrderNo);
                        if (payment == null || payment.Id <= 0)
                        {
                            SetMessage("Pin is invalid or doesn't exist!", Message.Category.Information);
                            return View(vModel);
                        }
                        else
                        {
                            vModel.payment = payment;
                            TempData["ClearanceViewModel"] = vModel;
                            return RedirectToAction("clear2");

                        }

                    }
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            TempData["ClearanceViewModel"] = viewmodel;
            return View(viewmodel);
        }
        public ActionResult Clear2()
        {

            try
            {
                viewmodel = new ClearanceViewModel();
                if (TempData["ClearanceViewModel"] != null)
                {
                    viewmodel = (ClearanceViewModel)TempData["ClearanceViewModel"];
                }

            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return View(viewmodel);
        }

        public ActionResult AdmissionCriteria(string sortOrder)
        {
            viewmodel = new ClearanceViewModel();
            //if (TempData["ClearanceViewModel"] != null)
            //{
            //    viewmodel = (ClearanceViewModel)TempData["ClearanceViewModel"];
            //}
            try
            {
                string sortDesc = sortOrder;
                ViewBag.CurrentSort = sortOrder;
                ViewBag.Department = String.IsNullOrEmpty(sortDesc) ? "Department_desc" : "";
                ViewBag.Programme = String.IsNullOrEmpty(sortDesc) ? "Programme_desc" : "";
                ViewBag.Minimum = String.IsNullOrEmpty(sortDesc) ? "Minimum_desc" : "";
                ViewBag.Date = String.IsNullOrEmpty(sortDesc) ? "Date_desc" : "";


                switch (sortDesc)
                {
                    case "Department_desc":
                        viewmodel.admissionCriteriaList = viewmodel.admissionCriteriaList.OrderByDescending(s => s.Department.Name).ToList();
                        break;
                    case "Programme_desc":
                        viewmodel.admissionCriteriaList = viewmodel.admissionCriteriaList.OrderByDescending(s => s.Programme.Name).ToList();
                        break;
                    case "Minimum_desc":
                        viewmodel.admissionCriteriaList = viewmodel.admissionCriteriaList.OrderByDescending(s => s.MinimumRequiredNumberOfSubject).ToList();
                        break;
                    case "Date_desc":
                        viewmodel.admissionCriteriaList = viewmodel.admissionCriteriaList.OrderByDescending(s => s.DateEntered).ToList();
                        break;
                    default:
                        viewmodel.admissionCriteriaList = viewmodel.admissionCriteriaList.OrderByDescending(s => s.Department.Name).ToList();
                        break;


                }
                TempData["ClearanceViewModel"] = viewmodel;
                return View(viewmodel);
            }




            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View();
        }

        public ActionResult ViewCriteria(long Id)
        {
            viewmodel = new ClearanceViewModel();
            try
            {
                AdmissionCriteriaForOLevelSubjectLogic OlevelLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                List<AdmissionCriteriaForOLevelSubjectAlternative> olevelAltList = new List<AdmissionCriteriaForOLevelSubjectAlternative>();
                AdmissionCriteriaForOLevelSubjectAlternative olevelAlt = new AdmissionCriteriaForOLevelSubjectAlternative();
                AdmissionCriteriaForOLevelSubjectAlternativeLogic olevelAltLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();
                AdmissionCriteriaForOLevelTypeLogic olevelTypeLogic = new AdmissionCriteriaForOLevelTypeLogic();

                viewmodel.admissionCriteriaForOLevelSubject = OlevelLogic.GetModelsBy(a => a.Admission_Criteria_Id == Id);
                foreach (AdmissionCriteriaForOLevelSubject subject in viewmodel.admissionCriteriaForOLevelSubject)
                {
                    subject.Alternatives = olevelAltLogic.GetModelsBy(o => o.Admission_Criteria_For_O_Level_Subject_Id == subject.Id);
                    if (subject.Alternatives.Count > 1)
                    {
                        for (int i = 0; i < subject.Alternatives.Count; i++)
                        {
                            if (i != 0)
                            {
                                subject.Alternatives[0].OLevelSubject.Name += ", " + subject.Alternatives[i].OLevelSubject.Name;
                            }
                        }
                    }
                }

                viewmodel.admissionCriteriaForOLevelType = olevelTypeLogic.GetModelsBy(n => n.Admission_Criteria_Id == Id);

                TempData["ClearanceViewModel"] = viewmodel;
                return View(viewmodel);

            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View();
        }
        [HttpPost]
        public ActionResult EditCriteria(ClearanceViewModel criteriaModel)
        {
            try
            {
                //Add subjects
                AdmissionCriteriaForOLevelSubjectLogic criteriaSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                AdmissionCriteriaForOLevelSubjectAlternativeLogic criteriaSubjectAlternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();
                criteriaSubjectLogic.Modify(criteriaModel.admissionCriteriaForOLevelSubject);

                SetMessage("Criteria was updated successfully", Message.Category.Information);
                return RedirectToAction("AdmissionCriteria");
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("AdmissionCriteria");
        }
        public ActionResult EditCriteria(long Id)
        {
            viewmodel = new ClearanceViewModel();
            try
            {
                AdmissionCriteriaForOLevelSubjectLogic OlevelLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                List<AdmissionCriteriaForOLevelSubjectAlternative> olevelAltList = new List<AdmissionCriteriaForOLevelSubjectAlternative>();
                AdmissionCriteriaForOLevelSubjectAlternative olevelAlt = new AdmissionCriteriaForOLevelSubjectAlternative();
                AdmissionCriteriaForOLevelSubjectAlternativeLogic olevelAltLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();
                AdmissionCriteriaForOLevelTypeLogic olevelTypeLogic = new AdmissionCriteriaForOLevelTypeLogic();


                for (int i = 0; i < 25; i++)
                {
                    ViewData["FirstSittingOLevelSubjectId" + i] = viewmodel.OLevelSubjectSelectList;
                    ViewData["SecondSittingOLevelSubjectId" + i] = viewmodel.OLevelSubjectSelectList;
                    ViewData["OtherOLevelSubjectId" + i] = viewmodel.OLevelSubjectSelectList;
                    ViewData["FirstSittingOLevelGradeId" + i] = viewmodel.OLevelGradeSelectList;

                }

                viewmodel.admissionCriteriaForOLevelSubject = OlevelLogic.GetModelsBy(a => a.Admission_Criteria_Id == Id);
                foreach (AdmissionCriteriaForOLevelSubject subject in viewmodel.admissionCriteriaForOLevelSubject)
                {
                    subject.Alternatives = olevelAltLogic.GetModelsBy(o => o.Admission_Criteria_For_O_Level_Subject_Id == subject.Id);

                    if (subject.Alternatives.Count > 1)
                    {
                        List<AdmissionCriteriaForOLevelSubjectAlternative> alternativeList = new List<AdmissionCriteriaForOLevelSubjectAlternative>();
                        alternativeList.Add(subject.Alternatives[1]);
                        subject.OtherAlternatives = alternativeList;
                    }
                }

                int count = viewmodel.admissionCriteriaForOLevelSubject.Count;

                for (int i = count; i < count + 6; i++)
                {
                    viewmodel.admissionCriteriaForOLevelSubject.Add(new AdmissionCriteriaForOLevelSubject()
                    {
                        Alternatives = new List<AdmissionCriteriaForOLevelSubjectAlternative>(),
                        IsCompulsory = false,
                        MainCriteria = viewmodel.admissionCriteriaForOLevelSubject[0].MainCriteria,
                        MinimumGrade = viewmodel.admissionCriteriaForOLevelSubject[0].MinimumGrade,
                        Subject = new OLevelSubject(),
                        OtherAlternatives = new List<AdmissionCriteriaForOLevelSubjectAlternative>()
                    });
                }

                viewmodel.admissionCriteriaForOLevelType = olevelTypeLogic.GetModelsBy(n => n.Admission_Criteria_Id == Id);
                SetSelectedSittingSubjectAndGrade(viewmodel);


                TempData["ClearanceViewModel"] = viewmodel;
                ViewBag.OLevelSubjects = viewmodel.OLevelSubjectSelectList;
                return View(viewmodel);

            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.OLevelSubjects = viewmodel.OLevelSubjectSelectList;
            return View();
        }
        private void SetSelectedSittingSubjectAndGrade(ClearanceViewModel existingViewModel)
        {
            try
            {
                if (existingViewModel != null && existingViewModel.admissionCriteriaForOLevelSubject != null && existingViewModel.admissionCriteriaForOLevelSubject.Count > 0)
                {
                    int i = 0;
                    foreach (AdmissionCriteriaForOLevelSubject subject in existingViewModel.admissionCriteriaForOLevelSubject)
                    {
                        if (subject.Subject.Name != null)
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, subject.Subject.Id);
                            if (subject.Alternatives.Count > 0)
                            {
                                ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, subject.Alternatives[0].OLevelSubject.Id);
                            }
                            if (subject.OtherAlternatives != null && subject.OtherAlternatives.Count > 0)
                            {
                                ViewData["OtherOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, subject.OtherAlternatives[0].OLevelSubject.Id);
                            }
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, subject.MinimumGrade.Id);

                        }
                        else
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(viewmodel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(viewmodel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["OtherOLevelSubjectId" + i] = new SelectList(viewmodel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);

                        }

                        i++;
                    }

                    AdmissionCriteriaForOLevelSubject sub = new AdmissionCriteriaForOLevelSubject();
                    sub.Id = -1;
                    sub.MainCriteria = existingViewModel.admissionCriteriaForOLevelSubject[0].MainCriteria;
                    sub.Alternatives[0].OLevelSubject.Id = -1;
                    //sub.Alternatives[1].OLevelSubject.Id = -1;


                    for (int u = 0; u < 5; u++)
                    {
                        existingViewModel.admissionCriteriaForOLevelSubject.Add(sub);
                    }


                }


            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        public ActionResult AddAdmissionCriteria()
        {
           ClearanceViewModel Viewmodel = new ClearanceViewModel();

            try
            {
                ViewBag.ProgrammeId = Viewmodel.ProgrammeSelectListItem;
                ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

                for (int i = 0; i < 15; i++)
                {
                    ViewData["FirstSittingOLevelSubjectId" + i] = Viewmodel.OLevelSubjectSelectList;
                    ViewData["SecondSittingOLevelSubjectId" + i] = Viewmodel.OLevelSubjectSelectList;
                }
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            TempData["viewModel"] = Viewmodel;
            return View(Viewmodel);
        }

        [HttpPost]
        public ActionResult AddAdmissionCriteria(ClearanceViewModel crieriaModel, FormCollection formValues)
       {
          
           try
           {
               var m = new ClearanceViewModel();
               if (ModelState.IsValid)
               {
                   //Add team and redirect
               }

              using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                      if (crieriaModel.admissionCriteria.Department != null && crieriaModel.admissionCriteria.Programme != null && crieriaModel.OLevelSubjects != null && crieriaModel.OLevelSubjects.Count > 0)
                      {
                          AdmissionCriteria criteria = new AdmissionCriteria();
                          AdmissionCriteria criteria2 = new AdmissionCriteria();
                          AdmissionCriteriaLogic criteriaLogic = new AdmissionCriteriaLogic();
                          criteria = criteriaLogic.GetModelBy(c => c.Department_Id == crieriaModel.admissionCriteria.Department.Id && c.Programme_Id == crieriaModel.admissionCriteria.Programme.Id);
                           if (criteria == null)
                           {
                               criteria2.DateEntered = DateTime.Now;
                               criteria2.Department = crieriaModel.admissionCriteria.Department;
                               criteria2.Programme = crieriaModel.admissionCriteria.Programme;
                               criteria2.MinimumRequiredNumberOfSubject = 5;
                               criteriaLogic.Create(criteria2);
                               criteria = criteriaLogic.GetModelBy(c => c.Department_Id == crieriaModel.admissionCriteria.Department.Id && c.Programme_Id == crieriaModel.admissionCriteria.Programme.Id);
                          
                               //Add subjects
                               AdmissionCriteriaForOLevelSubjectLogic criteriaSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                               AdmissionCriteriaForOLevelSubject criteriaSubject = new AdmissionCriteriaForOLevelSubject();
                               AdmissionCriteriaForOLevelSubjectAlternativeLogic criteriaSubjectAlternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();
                               AdmissionCriteriaForOLevelSubjectAlternative criteriaSubjectAlternative = new AdmissionCriteriaForOLevelSubjectAlternative();
                               OLevelGrade grade = new OLevelGrade();
                               OLevelGradeLogic gradeLogic = new OLevelGradeLogic();
                               grade = gradeLogic.GetModelBy(g => g.O_Level_Grade_Id == 6);

                               int count = 0;
                               foreach(OLevelSubject subject in crieriaModel.OLevelSubjects )
                               {
                                   if (subject != null && subject.Id > 0)
                                   {
                                       criteriaSubject.MainCriteria = criteria;
                                       criteriaSubject.Subject = (OLevelSubject)subject;
                                       criteriaSubject.IsCompulsory = subject.IsChecked;
                                       criteriaSubject.MinimumGrade = grade;
                                       criteriaSubject = criteriaSubjectLogic.Create(criteriaSubject);
                                       if (crieriaModel.OLevelSubjectsAlternatives[count].Id > 0)
                                       {
                                           criteriaSubjectAlternative.OLevelSubject = crieriaModel.OLevelSubjectsAlternatives[count];
                                           criteriaSubjectAlternative.Alternative = criteriaSubject;
                                           criteriaSubjectAlternativeLogic.Create(criteriaSubjectAlternative);
                                       }
                                       

                                       
                                   }
                                   count++;
                               }
                           }
                      }
                      transaction.Complete();


                      ViewBag.ProgrammeId = crieriaModel.ProgrammeSelectListItem;
                      ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);

                      for (int i = 0; i < 15; i++)
                      {
                          ViewData["FirstSittingOLevelSubjectId" + i] = crieriaModel.OLevelSubjectSelectList;
                          ViewData["SecondSittingOLevelSubjectId" + i] = crieriaModel.OLevelSubjectSelectList;
                      }
              }

           }
           catch (Exception ex)
           {

               SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
           }
           TempData["Message"] = "Successfully Added criteria";
           return View(crieriaModel);
       }


        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };

                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private Payment InvalidConfirmationOrderNumber(string confirmationOrderNumber)
        {
            Payment payment = new Payment();
            PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
            PaymentEtranzact etranzactDetails = etranzactLogic.GetModelBy(m => m.Confirmation_No == confirmationOrderNumber);
            if (etranzactDetails == null || etranzactDetails.ReceiptNo == null)
            {
                PaymentTerminal paymentTerminal = new PaymentTerminal();
                PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
                paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == 1 && p.Session_Id == 1);

                etranzactDetails = etranzactLogic.RetrievePinAlternative(confirmationOrderNumber, paymentTerminal);
                if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
                {
                    PaymentLogic paymentLogic = new PaymentLogic();
                    payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                    if (payment != null && payment.Id > 0)
                    {
                        FeeDetail feeDetail = new FeeDetail();
                        FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                        feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id);
                        //if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                        //{
                        //    SetMessage("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.", Message.Category.Error);
                        //    payment = null;
                        //    return payment;
                        //}

                    }
                    else
                    {
                        SetMessage("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.", Message.Category.Error);

                    }
                }
                else
                {
                    SetMessage("Confirmation Order Number entered seems not to be valid! Please cross check and try again.", Message.Category.Error);

                }
            }
            else
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
                if (payment != null && payment.Id > 0)
                {
                    FeeDetail feeDetail = new FeeDetail();
                    FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                    feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id);

                    if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
                    {
                        SetMessage("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.", Message.Category.Error);
                        payment = null;
                        return payment;
                    }
                }
                else
                {
                    SetMessage("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.", Message.Category.Error);
                    //return View(viewModel);
                }
            }

            return payment;
        }
        
        [AllowAnonymous]
        public ActionResult AsyncStudentRanking()
        {

            return View();
        }

        public JsonResult GetSubjectAlternatives(long criteriaForOLevelSubjectId)
        {
            CriteriaJsonResult result = new CriteriaJsonResult();
            try
            {
                if (criteriaForOLevelSubjectId > 0)
                {
                    AdmissionCriteriaForOLevelSubjectLogic criteriaForOLevelSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                    AdmissionCriteriaForOLevelSubjectAlternativeLogic alternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();

                    AdmissionCriteriaForOLevelSubject criteriaForOLevelSubject = criteriaForOLevelSubjectLogic.GetModelsBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId).LastOrDefault();
                    if (criteriaForOLevelSubject != null)
                    {
                        result.Subject = criteriaForOLevelSubject.Subject.Name;
                    }
                    List<AdmissionCriteriaForOLevelSubjectAlternative> alternatives = alternativeLogic.GetModelsBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                    if (alternatives.Count > 0)
                    {
                        for (int i = 0; i < alternatives.Count; i++)
                        {
                            result.Alternatives += alternatives[i].OLevelSubject.Name + ", ";
                        }
                    }
                    else
                    {
                        result.Alternatives = "";
                    }

                    result.IsError = false;
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Criteria for this subject does not exist";
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddSubjectAlternative(long criteriaForOLevelSubjectId, int oLevelSubjectIdToAdd)
        {
            CriteriaJsonResult result = new CriteriaJsonResult();
            try
            {
                if (criteriaForOLevelSubjectId > 0 && oLevelSubjectIdToAdd > 0)
                {
                    AdmissionCriteriaForOLevelSubjectLogic criteriaForOLevelSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                    AdmissionCriteriaForOLevelSubjectAlternativeLogic alternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();

                    AdmissionCriteriaForOLevelSubject criteriaForOLevelSubject = criteriaForOLevelSubjectLogic.GetModelsBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId).LastOrDefault();
                    if (criteriaForOLevelSubject != null)
                    {
                        result.Subject = criteriaForOLevelSubject.Subject.Name;

                        AdmissionCriteriaForOLevelSubjectAlternative existingAlternative = alternativeLogic.GetModelsBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId && a.O_Level_Subject_Id == oLevelSubjectIdToAdd).LastOrDefault();
                        if (existingAlternative == null)
                        {
                            existingAlternative = new AdmissionCriteriaForOLevelSubjectAlternative();
                            existingAlternative.OLevelSubject = new OLevelSubject(){ Id = oLevelSubjectIdToAdd };
                            existingAlternative.Alternative = criteriaForOLevelSubject;

                            alternativeLogic.Create(existingAlternative);

                            List<AdmissionCriteriaForOLevelSubjectAlternative> alternatives = alternativeLogic.GetModelsBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                            if (alternatives.Count > 0)
                            {
                                for (int i = 0; i < alternatives.Count; i++)
                                {
                                    if (alternatives[i].OLevelSubject != null)
                                    {
                                        result.Alternatives += alternatives[i].OLevelSubject.Name + ", ";
                                    }
                                    else
                                    {
                                        OLevelSubjectLogic oLevelSubjectLogic = new OLevelSubjectLogic();
                                        OLevelSubject oLevelSubject = oLevelSubjectLogic.GetModelBy(o => o.O_Level_Subject_Id == oLevelSubjectIdToAdd);
                                        if (oLevelSubject != null)
                                        {
                                            result.Alternatives += oLevelSubject.Name + ", ";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            result.IsError = true;
                            result.Message = "Alternative exist.";
                        }
                    }
                    else
                    {
                        result.IsError = true;
                        result.Message = "Criteria for this OLevel Subject does not exist.";
                    }
                }
                else
                {
                    result.IsError = true;
                    result.Message = "No OLevel Subject was selected.";
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ClearSubjectAlternatives(long criteriaForOLevelSubjectId)
        {
            CriteriaJsonResult result = new CriteriaJsonResult();
            try
            {
                if (criteriaForOLevelSubjectId > 0)
                {
                    AdmissionCriteriaForOLevelSubjectLogic criteriaForOLevelSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                    AdmissionCriteriaForOLevelSubjectAlternativeLogic alternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();

                    AdmissionCriteriaForOLevelSubject criteriaForOLevelSubject = criteriaForOLevelSubjectLogic.GetModelBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                    if (criteriaForOLevelSubject != null)
                    {
                        alternativeLogic.Delete(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                    }

                    result.Message = "Operation Successful!";
                    result.IsError = false;
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Criteria for this subject does not exist";
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteAdmissionCriteria(long criteriaForOLevelSubjectId)
        {
            CriteriaJsonResult result = new CriteriaJsonResult();
            try
            {
                if (criteriaForOLevelSubjectId > 0)
                {
                    AdmissionCriteriaForOLevelSubjectLogic criteriaForOLevelSubjectLogic = new AdmissionCriteriaForOLevelSubjectLogic();
                    AdmissionCriteriaForOLevelSubjectAlternativeLogic alternativeLogic = new AdmissionCriteriaForOLevelSubjectAlternativeLogic();

                    AdmissionCriteriaForOLevelSubject criteriaForOLevelSubject = criteriaForOLevelSubjectLogic.GetModelBy(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                    if (criteriaForOLevelSubject != null)
                    {
                        alternativeLogic.Delete(a => a.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                        criteriaForOLevelSubjectLogic.Delete(c => c.Admission_Criteria_For_O_Level_Subject_Id == criteriaForOLevelSubjectId);
                    }

                    result.Message = "Operation Successful!";
                    result.IsError = false;
                }
                else
                {
                    result.IsError = true;
                    result.Message = "Criteria for this subject does not exist";
                }
            }
            catch (Exception ex)
            {
                result.IsError = true;
                result.Message = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}