﻿using Abundance_Nk.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Applicant.Controllers
{
        [AllowAnonymous]
        public class TranscriptController : BaseController
        {

            TranscriptViewModel viewModel;
            public ActionResult Index(string type)
            {
                try
                {
                    viewModel = new TranscriptViewModel();
                    ViewBag.StateId = viewModel.StateSelectList;
                    ViewBag.CountryId = viewModel.CountrySelectList;

                    if (type == "")
                    {
                        type = null;
                    }

                    viewModel.RequestType = type;
                    TempData["RequestType"] = type;
                    TempData.Keep("RequestType");
                }
                catch (Exception ex)
                {
                    SetMessage("Error! " + ex.Message, Message.Category.Error);
                }

                return View(viewModel);
            }

            [HttpPost]
            public ActionResult Index(TranscriptViewModel transcriptViewModel)
            {
                try
                {
                    if (transcriptViewModel.transcriptRequest.student.MatricNumber != null)
                    {
                        TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                        TranscriptRequest tRequest = transcriptRequestLogic.GetModelsBy(t => t.STUDENT.Matric_Number == transcriptViewModel.transcriptRequest.student.MatricNumber).LastOrDefault();

                        if (tRequest != null)
                        {
                            PersonLogic personLogic = new PersonLogic();
                            Abundance_Nk.Model.Model.Person person = personLogic.GetModelBy(p => p.Person_Id == tRequest.student.Id);
                            tRequest.student.FirstName = person.FirstName;
                            tRequest.student.LastName = person.LastName;
                            tRequest.student.OtherName = person.OtherName;
                            transcriptViewModel.transcriptRequest = tRequest;

                            if (tRequest.payment != null && tRequest.payment.Id > 0)
                            {
                                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                                tRequest.remitaPayment = remitaPaymentLogic.GetBy(tRequest.payment.Id);
                                if (tRequest.remitaPayment != null && tRequest.remitaPayment.Status.Contains("01"))
                                {

                                    GetStudentDetails(transcriptViewModel);
                                    transcriptViewModel.transcriptRequest.payment = null;
                                }
                            }
                        }
                        else
                        {
                            GetStudentDetails(transcriptViewModel);
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }
                return View(transcriptViewModel);
            }
            public ActionResult IndexAlt(TranscriptViewModel transcriptViewModel)
            {
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }
                TempData.Keep("RequestType");

                try
                {

                    if (transcriptViewModel.transcriptRequest.student == null)
                    {
                        SetMessage("Enter Your Matriculation Number", Message.Category.Error);
                        return RedirectToAction("Index", new { type = type });
                    }

                    if (transcriptViewModel.transcriptRequest.student.MatricNumber != null)
                    {
                        TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                        List<TranscriptRequest> tRequests = transcriptRequestLogic.GetModelsBy(t => t.STUDENT.Matric_Number == transcriptViewModel.transcriptRequest.student.MatricNumber && t.Request_Type == type);

                        if (tRequests.Count > 0)
                        {
                            PersonLogic personLogic = new PersonLogic();
                            long sid = tRequests.FirstOrDefault().student.Id;
                            Abundance_Nk.Model.Model.Person person = personLogic.GetModelBy(p => p.Person_Id == sid);

                            for (int i = 0; i < tRequests.Count; i++)
                            {
                                tRequests[i].student.FirstName = person.FirstName;
                                tRequests[i].student.LastName = person.LastName;
                                tRequests[i].student.OtherName = person.OtherName;
                                //transcriptViewModel.transcriptRequest = tRequests[i];

                                if (tRequests[i].payment != null && tRequests[i].payment.Id > 0)
                                {
                                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                                    tRequests[i].remitaPayment = remitaPaymentLogic.GetBy(tRequests[i].payment.Id);
                                    if (tRequests[i].remitaPayment != null && tRequests[i].remitaPayment.Status.Contains("01"))
                                    {
                                        GetStudentDetails(transcriptViewModel);
                                        tRequests[i].payment = null;
                                    }
                                }
                            }

                            transcriptViewModel.TranscriptRequests = tRequests;
                            transcriptViewModel.RequestType = type;

                            return View(transcriptViewModel);
                        }
                        else
                        {
                            StudentLogic studentLogic = new StudentLogic();
                            Model.Model.Student student = new Model.Model.Student();
                            student = studentLogic.GetBy(transcriptViewModel.transcriptRequest.student.MatricNumber);

                            if (student != null)
                            {
                                return RedirectToAction("Request", new { sid = student.Id });
                            }
                            else
                            {
                                return RedirectToAction("Request", new { sid = 0 });
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }

                transcriptViewModel.RequestType = type;
                return View(transcriptViewModel);
            }
            private static void GetStudentDetails(TranscriptViewModel transcriptViewModel)
            {
                StudentLogic studentLogic = new StudentLogic();
                Model.Model.Student student = new Model.Model.Student();
                string MatricNumber = transcriptViewModel.transcriptRequest.student.MatricNumber;
                student = studentLogic.GetBy(MatricNumber);
                if (student != null)
                {
                    PersonLogic personLogic = new PersonLogic();
                    Abundance_Nk.Model.Model.Person person = personLogic.GetModelBy(p => p.Person_Id == student.Id);
                    student.FirstName = person.FirstName;
                    student.LastName = person.LastName;
                    student.OtherName = person.OtherName;
                    transcriptViewModel.transcriptRequest.student = student;

                }
            }

            public ActionResult Request(long sid)
            {
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }
                TempData.Keep("RequestType");

                try
                {
                    viewModel = new TranscriptViewModel();
                    TranscriptRequest transcriptRequest = new TranscriptRequest();
                    StudentLogic studentLogic = new StudentLogic();
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    if (sid > 0)
                    {
                        // transcriptRequest = transcriptRequestLogic.GetBy(sid);
                        transcriptRequest = transcriptRequestLogic.GetModelsBy(tr => tr.Student_id == sid).FirstOrDefault();
                        if (transcriptRequest != null)
                        {
                            viewModel.transcriptRequest = transcriptRequest;
                        }
                        else
                        {
                            Abundance_Nk.Model.Model.Student student = studentLogic.GetBy(sid);
                            viewModel.transcriptRequest = new TranscriptRequest();
                            viewModel.transcriptRequest.student = student;
                        }
                    }

                    viewModel.RequestType = type;
                    if (type == "Convocation Fee")
                    {
                        ViewBag.StateId = new SelectList(viewModel.StateSelectList, "Value", "Text", "OG");
                        ViewBag.CountryId = new SelectList(viewModel.CountrySelectList, "Value", "Text", "NIG");

                        if (sid > 0)
                        {
                            viewModel.transcriptRequest.DestinationAddress = "FEDPOLY ILARO";
                            viewModel.transcriptRequest.DestinationState = new State() { Id = "OG" };
                            viewModel.transcriptRequest.DestinationCountry = new Country() { Id = "NIG" };
                        }
                        else
                        {
                            viewModel.transcriptRequest = new TranscriptRequest();
                            viewModel.transcriptRequest.DestinationAddress = "FEDPOLY ILARO";
                            viewModel.transcriptRequest.DestinationState = new State() { Id = "OG" };
                            viewModel.transcriptRequest.DestinationCountry = new Country() { Id = "NIG" };
                        }
                    }
                    else
                    {
                        ViewBag.StateId = viewModel.StateSelectList;
                        ViewBag.CountryId = viewModel.CountrySelectList;
                        if (sid > 0)
                        {
                            //Do nothing
                        }
                        else
                        {
                            viewModel.transcriptRequest = new TranscriptRequest();
                        }
                    }


                }
                catch (Exception ex)
                {
                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }

                viewModel.RequestType = type;
                TempData["TranscriptViewModel"] = viewModel;
                return View(viewModel);
            }

            [HttpPost]
            public ActionResult Request(TranscriptViewModel transcriptViewModel)
            {
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }

                TempData.Keep("RequestType");

                //if (type == null && transcriptViewModel.transcriptRequest != null && transcriptViewModel.transcriptRequest.DestinationCountry != null && transcriptViewModel.transcriptRequest.DestinationCountry.Id == "NIG")
                //{
                //    SetMessage("Local transcript request temporarily closed! Check back later.", Message.Category.Error);
                //    return RedirectToAction("Index");
                //}

                try
                {
                    viewModel = new TranscriptViewModel();
                    ReloadDropdown(transcriptViewModel);
                    if (transcriptViewModel.transcriptRequest != null && transcriptViewModel.transcriptRequest.Id <= 0 && transcriptViewModel.transcriptRequest.student != null && transcriptViewModel.transcriptRequest.DestinationCountry != null)
                    {
                        if (transcriptViewModel.transcriptRequest.student.Id < 1)
                        {
                            Model.Model.Student student = new Model.Model.Student();
                            Person person = new Person();
                            StudentLevel studentLevel = new StudentLevel();
                            StudentLogic stduentLogic = new StudentLogic();
                            StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                            PersonLogic personLogic = new PersonLogic();

                            student = transcriptViewModel.transcriptRequest.student;

                            Role role = new Role() { Id = 5 };
                            Nationality nationality = new Nationality() { Id = 1 };

                            person.LastName = student.LastName;
                            person.FirstName = student.FirstName;
                            person.OtherName = student.OtherName;
                            person.State = new State() { Id = "IM" };
                            person.Role = role;
                            person.Nationality = nationality;
                            person.DateEntered = DateTime.Now;
                            person.PersonType = new PersonType() { Id = 3 };
                            person = personLogic.Create(person);
                            if (person != null && person.Id > 0)
                            {
                                string Type = student.MatricNumber.Substring(0, 1);
                                if (Type == "H")
                                {
                                    student.Type = new StudentType() { Id = 2 };
                                }
                                else
                                {
                                    student.Type = new StudentType() { Id = 1 };
                                }
                                student.Id = person.Id;
                                student.Category = new StudentCategory() { Id = 2 };
                                student.Status = new StudentStatus() { Id = 1 };
                                student = stduentLogic.Create(student);
                            }

                        }

                        TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                        TranscriptRequest transcriptRequest = new TranscriptRequest();
                        transcriptRequest = transcriptViewModel.transcriptRequest;
                        transcriptRequest.DateRequested = DateTime.Now;
                        //transcriptRequest.DestinationCountry = new Country { Id = "NIG" };
                        if (transcriptRequest.DestinationState.Id == null)
                        {
                            transcriptRequest.DestinationState = new State() { Id = "OT" };
                        }
                        transcriptRequest.transcriptClearanceStatus = new TranscriptClearanceStatus { TranscriptClearanceStatusId = 4 };
                        transcriptRequest.transcriptStatus = new TranscriptStatus { TranscriptStatusId = 1 };
                        transcriptRequest.RequestType = type;
                        if (transcriptRequest.DestinationCountry.Id == "OTH")
                        {
                            transcriptRequest.DestinationState = new State() { Id = "OT" };
                        }
                        transcriptRequest = transcriptRequestLogic.Create(transcriptRequest);

                        return RedirectToAction("TranscriptPayment", new { tid = transcriptRequest.Id });
                    }
                    else
                    {
                        TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();

                        if (transcriptViewModel.transcriptRequest.payment == null)
                        {
                            TranscriptRequest transcriptRequest = new TranscriptRequest();
                            transcriptRequest = transcriptViewModel.transcriptRequest;
                            transcriptRequest.DateRequested = DateTime.Now;
                            //transcriptRequest.DestinationCountry = new Country { Id = "NIG" };
                            if (transcriptRequest.DestinationState.Id == null)
                            {
                                transcriptRequest.DestinationState = new State() { Id = "OT" };
                            }
                            transcriptRequest.transcriptClearanceStatus = new TranscriptClearanceStatus { TranscriptClearanceStatusId = 4 };
                            transcriptRequest.transcriptStatus = new TranscriptStatus { TranscriptStatusId = 1 };
                            transcriptRequest.RequestType = type;
                            if (transcriptRequest.DestinationCountry.Id == "OTH")
                            {
                                transcriptRequest.DestinationState = new State() { Id = "OT" };
                            }
                            transcriptRequest = transcriptRequestLogic.Create(transcriptRequest);
                            return RedirectToAction("TranscriptPayment", new { tid = transcriptRequest.Id });

                        }
                        if (transcriptViewModel.transcriptRequest.DestinationCountry.Id == "OTH")
                        {
                            transcriptViewModel.transcriptRequest.DestinationState = new State() { Id = "OT" };
                        }
                        transcriptRequestLogic.Modify(transcriptViewModel.transcriptRequest);
                        return RedirectToAction("TranscriptPayment", new { tid = transcriptViewModel.transcriptRequest.Id });
                    }

                }
                catch (Exception ex)
                {
                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }

                viewModel.RequestType = type;
                TempData["TranscriptViewModel"] = viewModel;
                return View(viewModel);
            }

            public ActionResult TranscriptPayment(long tid)
            {
                viewModel = new TranscriptViewModel();
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }
                TempData.Keep("RequestType");

                try
                {
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest tRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == tid);
                    PersonLogic personLogic = new PersonLogic();
                    Person person = new Person();
                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();

                    person = personLogic.GetModelBy(p => p.Person_Id == tRequest.student.Id);
                    if (person != null)
                    {
                        tRequest.student.ImageFileUrl = person.ImageFileUrl;
                        tRequest.student.FirstName = person.FirstName;
                        tRequest.student.LastName = person.LastName;
                        tRequest.student.OtherName = person.OtherName;

                        if (tRequest.payment != null)
                        {
                            tRequest.remitaPayment = remitaPaymentLogic.GetBy(tRequest.payment.Id);

                        }
                        viewModel.transcriptRequest = tRequest;
                        viewModel.RequestType = type;
                    }

                }
                catch (Exception ex)
                {

                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }
                viewModel.RequestType = type;
                TempData["TranscriptViewModel"] = viewModel;
                return View(viewModel);
            }

            public ActionResult ProcessPayment(long tid)
            {
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }
                TempData.Keep("RequestType");

                try
                {
                    TranscriptViewModel viewModel = new TranscriptViewModel();
                    TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                    TranscriptRequest tRequest = transcriptRequestLogic.GetModelBy(t => t.Transcript_Request_Id == tid);
                    if (tRequest != null)
                    {
                        Decimal Amt = 0;
                        Abundance_Nk.Model.Model.Student student = tRequest.student;
                        PersonLogic personLogic = new PersonLogic();
                        Abundance_Nk.Model.Model.Person person = personLogic.GetModelBy(t => t.Person_Id == tRequest.student.Id);
                        Payment payment = new Payment();


                        using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                        {
                            FeeType feeType = new FeeType();

                            feeType = new FeeType() { Id = (int)FeeTypes.TranscriptLocal };
                            
                            payment = CreatePayment(student, feeType);

                            if (payment != null)
                            {
                                Fee fee = null;
                                PaymentLogic paymentLogic = new PaymentLogic();
                                //Get Payment Specific Setting
                                RemitaSettings settings = new RemitaSettings();
                                RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();

                                if (tRequest.DestinationCountry.Id == "NIG")
                                {
                                    if (type == "Certificate Verification" || type == "Certificate Collection")
                                    {
                                        fee = new Fee() { Id = 46 };
                                        settings = settingsLogic.GetBy(8);
                                    }
                                    else
                                    {
                                        if (type == "Convocation Fee")
                                        {
                                            fee = new Fee() { Id = 60 };
                                        }
                                        else
                                        {
                                            fee = new Fee() { Id = 46 };
                                        }

                                        settings = settingsLogic.GetBy(4);
                                    }

                                }
                                else
                                {
                                    fee = new Fee() { Id = 47 };
                                    settings = settingsLogic.GetBy(5);
                                }

                                SetSettingsDescription(settings, type);

                                payment.FeeDetails = paymentLogic.SetFeeDetails(fee.Id);
                                Amt = payment.FeeDetails.Sum(a => a.Fee.Amount);


                                //Get Split Specific details;
                                List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
                                RemitaSplitItems singleItem = new RemitaSplitItems();
                                RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
                                if (type == "Certificate Verification" || type == "Certificate Collection")
                                {
                                    singleItem = splitItemLogic.GetBy(5);
                                    singleItem.deductFeeFrom = "1";
                                    singleItem.beneficiaryAmount = "300";
                                    splitItems.Add(singleItem);
                                    singleItem = splitItemLogic.GetBy(1);
                                    singleItem.deductFeeFrom = "0";
                                    singleItem.beneficiaryAmount = Convert.ToString(Amt - Convert.ToDecimal(splitItems[0].beneficiaryAmount));
                                    splitItems.Add(singleItem);
                                }
                                else
                                {
                                    singleItem = splitItemLogic.GetBy(5);
                                    singleItem.deductFeeFrom = "1";
                                    singleItem.beneficiaryAmount = "0";
                                    splitItems.Add(singleItem);
                                    singleItem = splitItemLogic.GetBy(1);
                                    singleItem.deductFeeFrom = "0";
                                    singleItem.beneficiaryAmount = Convert.ToString(Amt - Convert.ToDecimal(splitItems[0].beneficiaryAmount));
                                    splitItems.Add(singleItem);
                                }

                                //Get BaseURL
                                string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
                                RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
                                RemitaPayment remitaPayment = null;
                                if (type == null)
                                {
                                    remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "TRANSCRIPT", splitItems, settings, Amt);
                                }
                                if (type == "Transcript Verification")
                                {
                                    remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "TRANSCRIPT VERIFICATION", splitItems, settings, Amt);
                                }
                                if (type == "Certificate Collection")
                                {
                                    remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "CERTIFICATE COLLECTION", splitItems, settings, Amt);
                                }
                                if (type == "Certificate Verification")
                                {
                                    remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "CERTIFICATE VERIFICATION", splitItems, settings, Amt);
                                }
                                if (type == "Convocation Fee")
                                {
                                    remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "CONVOCATION FEE", splitItems, settings, Amt);
                                }

                                if (remitaPayment != null)
                                {
                                    transaction.Complete();
                                }
                                else
                                {
                                    SetMessage("Error Occurred! Remita Response: 'Invalid Request' ", Message.Category.Error);
                                    return RedirectToAction("TranscriptPayment", new { tid = tid });
                                }


                                viewModel.Hash = GenerateHash(settings.Api_key, remitaPayment);
                                viewModel.RemitaBaseUrl = remitaBaseUrl;
                                viewModel.RemitaPayment = remitaPayment;
                                //  viewModel.RemitaPayementProcessor = remitaProcessor;
                                TempData["TranscriptViewModel"] = viewModel;

                            }
                        }
                        tRequest.payment = payment;
                        tRequest.transcriptStatus = new TranscriptStatus { TranscriptStatusId = 3 };
                        transcriptRequestLogic.Modify(tRequest);

                        return RedirectToAction("TranscriptInvoice", new { controller = "Credential", area = "Common", pmid = payment.Id });
                        //move payment to invoiceGeneration
                    }

                }
                catch (Exception ex)
                {
                    SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
                }
                return RedirectToAction("Index", new { type = type });
            }

            private void SetSettingsDescription(RemitaSettings settings, string type)
            {
                try
                {
                    if (settings.Payment_SettingId == 4)
                    {
                        if (type == null)
                        {
                            settings.Description = "TRANSCRIPT LOCAL";
                        }
                        if (type == "Transcript Verification")
                        {
                            settings.Description = "TRANSCRIPT VERIFICATION LOCAL";
                        }
                        if (type == "Certificate Collection")
                        {
                            settings.Description = "CERTIFICATE LOCAL";
                        }
                        if (type == "Certificate Verification")
                        {
                            settings.Description = "CERTIFICATE VERIFICATION LOCAL";
                        }
                    }
                    if (settings.Payment_SettingId == 5)
                    {
                        if (type == null)
                        {
                            settings.Description = "TRANSCRIPT INTERNATIONAL";
                        }
                        if (type == "Transcript Verification")
                        {
                            settings.Description = "TRANSCRIPT VERIFICATION INTERNATIONAL";
                        }
                        if (type == "Certificate Collection")
                        {
                            settings.Description = "CERTIFICATE INTERNATIONAL";
                        }
                        if (type == "Certificate Verification")
                        {
                            settings.Description = "CERTIFICATE VERIFICATION INTERNATIONAL";
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }

            private string GenerateHash(string apiKey, RemitaPayment remitaPayment)
            {
                try
                {
                    string hash = remitaPayment.MerchantCode + remitaPayment.RRR + apiKey;
                    RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(hash);
                    string hashConcatenate = remitaProcessor.HashPaymentDetailToSHA512(hash);
                    return hashConcatenate;
                }
                catch (Exception)
                {

                    throw;
                }
            }

            private Payment CreatePayment(Abundance_Nk.Model.Model.Student student, FeeType feeType)
            {
                try
                {
                    Payment payment = new Payment();
                    PaymentLogic paymentLogic = new PaymentLogic();
                    payment.PaymentMode = new PaymentMode() { Id = 1 };
                    payment.PaymentType = new PaymentType() { Id = 2 };
                    payment.PersonType = new PersonType() { Id = 4 };
                    payment.FeeType = feeType;
                    payment.DatePaid = DateTime.Now;
                    payment.Person = student;
                    payment.Session = new Session { Id = 7 };

                    OnlinePayment newOnlinePayment = null;
                    Payment newPayment = paymentLogic.Create(payment);
                    if (newPayment != null)
                    {
                        PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        OnlinePayment onlinePayment = new OnlinePayment();
                        onlinePayment.Channel = channel;
                        onlinePayment.Payment = newPayment;
                        newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
                    }

                    return newPayment;
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public JsonResult GetState(string id)
            {
                try
                {
                    if (string.IsNullOrEmpty(id))
                    {
                        return null;
                    }

                    var country = id;
                    return Json(country, JsonRequestBehavior.AllowGet);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            private void ReloadDropdown(TranscriptViewModel transcriptViewModel)
            {
                if (transcriptViewModel.transcriptRequest.DestinationState != null && !string.IsNullOrEmpty(transcriptViewModel.transcriptRequest.DestinationState.Id))
                {
                    ViewBag.StateId = new SelectList(viewModel.StateSelectList, Utility.VALUE, Utility.TEXT, transcriptViewModel.transcriptRequest.DestinationState.Id);
                    ViewBag.CountryId = new SelectList(viewModel.CountrySelectList, Utility.VALUE, Utility.TEXT, transcriptViewModel.transcriptRequest.DestinationCountry.Id);

                }
                else
                {
                    ViewBag.StateId = new SelectList(viewModel.StateSelectList, Utility.VALUE, Utility.TEXT);
                    ViewBag.CountryId = new SelectList(viewModel.CountrySelectList, Utility.VALUE, Utility.TEXT, transcriptViewModel.transcriptRequest.DestinationCountry.Id);
                }



            }
        }
}