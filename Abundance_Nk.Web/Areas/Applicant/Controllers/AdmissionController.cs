﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;
using System.Configuration;
using System.Web.Script.Serialization;
using System.Net;
using Newtonsoft.Json;

namespace Abundance_Nk.Web.Areas.Applicant.Controllers
{
    [AllowAnonymous]
    public class AdmissionController : BaseController
    {
        private AdmissionViewModel viewModel;
        public AdmissionController()
        {
            viewModel = new AdmissionViewModel();
        }
        public ActionResult CheckStatus()
        {
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult CheckStatus(AdmissionViewModel vModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    bool signIn = admissionListLogic.IsValidApplicationNumberAndPin(vModel.ApplicationForm.Number, vModel.etranzactPin.ConfirmationNo);

                    if (signIn)
                    {
                        AdmissionList admission = viewModel.GetAdmissionListBy(vModel.ApplicationForm.Number);
                        return RedirectToAction("Index", new { fid = Abundance_Nk.Web.Models.Utility.Encrypt(admission.Form.Id.ToString()) });
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Error);
            }

            return View(vModel);
        }
        public ActionResult CheckStatusByName()
        {
            viewModel = new AdmissionViewModel();
            try
            {
                ViewBag.ProgrammeId = new SelectList(Utility.PopulateProgrammeSelectListItem(), "Value", "Text");
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            
            return View(viewModel);
        }
        [HttpPost]
        public ActionResult CheckStatusByName(AdmissionViewModel viewModel)
        {
            try
            {
                if (viewModel.Programme != null && viewModel.Programme.Id > 0 && viewModel.Name != null)
                {
                    AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                    viewModel.AdmissionLists = admissionListLogic.GetModelsBy(a => (a.APPLICATION_FORM.PERSON.Last_Name + a.APPLICATION_FORM.PERSON.First_Name + a.APPLICATION_FORM.PERSON.Other_Name).Contains(viewModel.Name) && a.Programme_Id == viewModel.Programme.Id);

                    viewModel.AdmissionListDropDownModels = new List<AdmissionListDropDownModel>();

                    for (int i = 0; i < viewModel.AdmissionLists.Count; i++)
                    {
                        viewModel.AdmissionListDropDownModels.Add(new AdmissionListDropDownModel(){FormId = viewModel.AdmissionLists[i].Form.Id, Name = viewModel.AdmissionLists[i].Form.Person.FullName.ToUpper()});
                    }

                    ViewBag.AdmissionListId = new SelectList(viewModel.AdmissionListDropDownModels, "FormId", "Name" );
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            if (viewModel.Programme != null)
            {
                ViewBag.ProgrammeId = new SelectList(Utility.PopulateProgrammeSelectListItem(), "Value", "Text", viewModel.Programme.Id);
            }
            else
            {
                ViewBag.ProgrammeId = new SelectList(Utility.PopulateProgrammeSelectListItem(), "Value", "Text");
            }
            
            return View(viewModel);
        }
        public ActionResult CheckAdmissionByName(AdmissionViewModel viewModel)
        {
            try
            {
                if (viewModel.ApplicationForm != null && viewModel.ApplicationForm.Id > 0)
                {
                    return RedirectToAction("Index", new { fid = Abundance_Nk.Web.Models.Utility.Encrypt(viewModel.ApplicationForm.Id.ToString()) });
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("CheckStatusByName");
        }
        public ActionResult Index(string fid)
        {
            try
            {
                TempData["FormViewModel"] = null;
                Int64 formId = Convert.ToInt64(Abundance_Nk.Web.Models.Utility.Decrypt(fid));
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                //if (admissionListLogic.HasStudentCheckedStatus(formId))
                //{
                    viewModel.GetApplicationBy(formId);
                    PopulateAllDropDowns(viewModel);
                    viewModel.GetOLevelResultBy(formId);
                    SetSelectedSittingSubjectAndGrade(viewModel);
                //}
                //mark scratch card as used
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        private void PopulateAllDropDowns(AdmissionViewModel existingViewModel)
        {
            //AdmissionViewModel existingViewModel = (AdmissionViewModel)TempData["viewModel"];
            //PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];

            try
            {
                if (existingViewModel.Loaded)
                {
                    if (existingViewModel.FirstSittingOLevelResult.Type == null) { existingViewModel.FirstSittingOLevelResult.Type = new OLevelType(); }

                    ViewBag.FirstSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, Utility.VALUE, Utility.TEXT, existingViewModel.FirstSittingOLevelResult.Type.Id);
                    ViewBag.FirstSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, Utility.VALUE, Utility.TEXT, existingViewModel.FirstSittingOLevelResult.ExamYear);
                    ViewBag.SecondSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, Utility.VALUE, Utility.TEXT, existingViewModel.SecondSittingOLevelResult.ExamYear);

                    if (existingViewModel.SecondSittingOLevelResult.Type != null)
                    {
                        ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, Utility.VALUE, Utility.TEXT, existingViewModel.SecondSittingOLevelResult.Type.Id);
                    }
                    else
                    {
                        ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, Utility.VALUE, Utility.TEXT, 0);
                    }

                    SetSelectedSittingSubjectAndGrade(existingViewModel);
                }
                else
                {
                    viewModel = new AdmissionViewModel();

                    ViewBag.FirstSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
                    ViewBag.SecondSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
                    ViewBag.FirstSittingExamYearId = viewModel.ExamYearSelectList;
                    ViewBag.SecondSittingExamYearId = viewModel.ExamYearSelectList;

                    //SetDefaultSelectedSittingSubjectAndGrade(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        //[HttpPost]
        //public ActionResult GenerateAcceptanceInvoice(string applicationFormNo)
        //{
        //    Payment payment = null;
        //    try
        //    {
        //        AdmissionListLogic admissionListLogic = new AdmissionListLogic();
        //        ApplicationForm form = viewModel.GetApplicationFormBy(applicationFormNo);
        //        AdmissionList applicationForm = admissionListLogic.GetBy(form.Id);
        //        if (form != null && form.Id > 0)
        //        {
        //            FeeType feeType = null;
        //            if (applicationForm.Batch.Type.Id == 2)
        //            {
        //                feeType = new FeeType() { Id = (int)FeeTypes.SupplementaryAcceptanceFee };   
        //            }
        //            else
        //            {
        //                feeType = new FeeType() { Id = (int)FeeTypes.AcceptanceFee };
        //            }

        //            ApplicantStatus.Status status = ApplicantStatus.Status.GeneratedAcceptanceInvoice;
        //            //if (applicationForm.Programme.Id == 5)
        //           // {
        //                using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
        //                {
        //                    payment = GenerateInvoiceHelper(form, feeType, status);
        //                    Decimal Amt = 0;
        //                    RemitaSettings settings = new RemitaSettings();
        //                    RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
        //                    settings = settingsLogic.GetBy(1);

        //                    Amt = payment.FeeDetails.Sum(a => a.Fee.Amount);
        //                    //Get Split Specific details;
        //                    List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
        //                    RemitaSplitItems singleItem = new RemitaSplitItems();
        //                    RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
        //                    if (feeType.Id == (int)FeeTypes.SupplementaryAcceptanceFee)
        //                    {
        //                        singleItem = splitItemLogic.GetBy(1);
        //                        singleItem.deductFeeFrom = "0";
        //                        singleItem.beneficiaryAmount = Amt.ToString();
        //                        splitItems.Add(singleItem); 
        //                    }
        //                    else
        //                    {
        //                        singleItem = splitItemLogic.GetBy(1);
        //                        singleItem.deductFeeFrom = "0";
        //                        singleItem.beneficiaryAmount = Amt.ToString();
        //                        splitItems.Add(singleItem);
        //                    }

        //                    singleItem = splitItemLogic.GetBy(2);
        //                    singleItem.deductFeeFrom = "1";
        //                    singleItem.beneficiaryAmount = "0";
        //                    splitItems.Add(singleItem);
        //                    singleItem = splitItemLogic.GetBy(7);
        //                    singleItem.lineItemsId = "3";
        //                    singleItem.deductFeeFrom = "0";
        //                    singleItem.beneficiaryAmount = "0";
        //                    splitItems.Add(singleItem);
        //                    //Get BaseURL
        //                    string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
        //                    RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
        //                    viewModel.remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "ACCEPTANCE", splitItems, settings, Amt);
        //                    if (viewModel.remitaPayment == null)
        //                    {
        //                        transaction.Dispose();
        //                    }

        //                    transaction.Complete(); 
        //                }  
        //           // }
        //            //else
        //           // {
        //               // payment = GenerateInvoiceHelper(form, feeType, status);
        //           // }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
        //    }

        //    return Json(new { InvoiceNumber = payment.InvoiceNumber }, "text/html", JsonRequestBehavior.AllowGet);
        //}
        
        public ActionResult GenerateAcceptanceInvoice(string applicationFormNo)
        {
            Payment payment = null;
            AdmissionViewModel vm = new AdmissionViewModel();
            try
            {
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                ApplicationForm form = viewModel.GetApplicationFormBy(applicationFormNo);
                AdmissionList applicationForm = admissionListLogic.GetBy(form.Id);
                if (form != null && form.Id > 0)
                {
                    FeeType feeType = null;
                    if (applicationForm.Batch.Type.Id == 2)
                    {
                        feeType = new FeeType() { Id = (int)FeeTypes.SupplementaryAcceptanceFee };
                    }
                    else
                    {
                        feeType = new FeeType() { Id = (int)FeeTypes.AcceptanceFee };
                    }

                    ApplicantStatus.Status status = ApplicantStatus.Status.GeneratedAcceptanceInvoice;
                    //if (applicationForm.Programme.Id == 5)
                    // {
                    //using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    //{
                        payment = GenerateInvoiceHelper(form, feeType, status);
                        Decimal Amt = 0;
                        RemitaSettings settings = new RemitaSettings();
                        RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                        settings = settingsLogic.GetBy(1);

                        Amt = payment.FeeDetails.Sum(a => a.Fee.Amount);
                    //Get BaseURL
                    string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();

                    Remita remita = new Remita()
                    {
                        merchantId = settings.MarchantId,
                        serviceTypeId = settings.serviceTypeId,
                        orderId = payment.InvoiceNumber.ToString(),
                        totalAmount = (decimal)Amt,
                        payerName = form.Person.FullName,
                        payerEmail = !string.IsNullOrEmpty(form.Person.Email) ? form.Person.Email : "test@lloydant.com",
                        payerPhone = form.Person.MobilePhone,
                        responseurl = "http://ndapplication.fpno.edu.ng/Applicant/Admission/RemitaResponse",
                        paymenttype = "RRRGEN",
                        amt = Amt.ToString()
                    };

                    remita.amt = remita.amt.Split('.')[0];

                    string hash_string = remita.merchantId + remita.serviceTypeId + remita.orderId + remita.amt + remita.responseurl + settings.Api_key;
                    System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                    Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                    sha512.Clear();
                    string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();

                    remita.hash = hashed;

                    vm.remita = remita;
                    //}
                    // }
                    //else
                    // {
                    // payment = GenerateInvoiceHelper(form, feeType, status);
                    // }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(vm);
        }
        public ActionResult RemitaResponse(string orderID)
        {
            RemitaResponse remitaResponse = new RemitaResponse();
            AdmissionViewModel viewModel = new AdmissionViewModel();

            try
            {
                string merchant_id = "532776942";
                string apiKey = "587460";
                string hashed;
                string checkstatusurl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                string url;

                if (Request.QueryString["orderID"] != null)
                {

                    PaymentLogic paymentLogic = new PaymentLogic();
                    ApplicationFormLogic formLogic = new ApplicationFormLogic();
                    RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();

                    orderID = Request.QueryString["orderID"].ToString();

                    Payment payment = paymentLogic.GetBy(orderID);
                    RemitaPayment remitaPyament = payment != null ? remitaLogic.GetModelBy(p => p.Payment_Id == payment.Id) : null;

                    string hash_string = orderID + apiKey + merchant_id;
                    System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                    Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                    sha512.Clear();
                    hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
                    url = checkstatusurl + "/" + merchant_id + "/" + orderID + "/" + hashed + "/" + "orderstatus.reg";

                    if (remitaPyament == null)
                    {
                        string jsondata = new WebClient().DownloadString(url);
                        remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
                    }
                    
                    if (remitaResponse != null && remitaResponse.Status != null)
                    {
                        if (payment != null)
                        {

                            if (remitaPyament != null)
                            {
                                remitaPyament.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                                remitaLogic.Modify(remitaPyament);
                            }
                            else
                            {
                                remitaPyament = new RemitaPayment();
                                remitaPyament.payment = payment;
                                remitaPyament.RRR = remitaResponse.rrr;
                                remitaPyament.OrderId = remitaResponse.orderId;
                                remitaPyament.Status = remitaResponse.Status;
                                remitaPyament.TransactionAmount = remitaResponse.amount;
                                remitaPyament.TransactionDate = DateTime.Now;
                                remitaPyament.MerchantCode = merchant_id;
                                remitaPyament.Description = "ACCEPTANCE";
                                if (remitaLogic.GetBy(payment.Id) == null)
                                {
                                    remitaLogic.Create(remitaPyament);
                                }
                            }

                            viewModel.remitaPayment = remitaPyament;
                            viewModel.Payment = payment;
                            viewModel.Person = payment.Person;
                            if (viewModel.Person != null)
                            {
                                viewModel.ApplicationForm = formLogic.GetModelBy(f => f.Person_Id == viewModel.Person.Id);
                            }
                        }
                        else
                        {
                            RemitaPayment remitaPayment = remitaLogic.GetBy(orderID);
                            string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                            RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(apiKey);
                            remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
                            if (remitaResponse != null && remitaResponse.Status != null)
                            {
                                remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                                remitaLogic.Modify(remitaPayment);

                                payment = paymentLogic.GetBy(remitaPayment.payment.Id);
                                viewModel.remitaPayment = remitaPayment;
                                if (payment != null)
                                {
                                    viewModel.Payment = payment;
                                    viewModel.Person = payment.Person;
                                    if (viewModel.Person != null)
                                    {
                                        viewModel.ApplicationForm = formLogic.GetModelBy(f => f.Person_Id == viewModel.Person.Id);
                                    }
                                }
                            }
                            else
                            {
                                SetMessage("Payment does not exist!", Message.Category.Error);
                            }

                            viewModel.remitaPayment = remitaPayment;
                        }
                    }
                    else if (remitaPyament != null && remitaPyament.payment != null)
                    {
                        return RedirectToAction("Invoice", new {ivn = remitaPyament.payment.InvoiceNumber});
                    }
                    else
                    {
                        SetMessage("Order ID was not generated from this system", Message.Category.Error);
                    }
                }
                else
                {
                    remitaResponse.Message = "No data was received!";
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }
        public ActionResult MakePayment(string PaymentId, string formId)
        {
            try
            {
                Int64 fid = Convert.ToInt64(Abundance_Nk.Web.Models.Utility.Decrypt(formId));
                string invoice = Abundance_Nk.Web.Models.Utility.Decrypt(PaymentId);
                RemitaPayment remitaPyament = new RemitaPayment();
                RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();
                Payment payment = new Payment();
                PaymentLogic pL = new PaymentLogic();
                payment = pL.GetModelBy(p => p.Invoice_Number == invoice);


                decimal Amount = pL.GetPaymentAmount(payment);

                List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
                RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
                splitItems = splitItemLogic.GetAll();
                splitItems[0].deductFeeFrom = "0";
                splitItems[1].deductFeeFrom = "1";


                string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
                RemitaSettings settings = new RemitaSettings();
                RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 1);
                long milliseconds = DateTime.Now.Ticks;
                RemitaPayementProcessor remitaPayementProcessor = new RemitaPayementProcessor(settings.Api_key);
                Remita remitaObj = new Remita()
                {

                    merchantId = settings.MarchantId,
                    serviceTypeId = settings.serviceTypeId,
                    totalAmount = Amount,
                    payerName = payment.Person.FullName,
                    payerEmail = "me@gmail.com",
                    payerPhone = payment.Person.MobilePhone,
                    responseurl = settings.Response_Url,
                    lineItems = splitItems,
                    orderId = payment.InvoiceNumber,

                };

                string toHash = remitaObj.merchantId + remitaObj.serviceTypeId + remitaObj.orderId + remitaObj.totalAmount + remitaObj.responseurl + settings.Api_key;

                string json = "";
                string jsondata = "";
                if (remitaObj != null)
                {
                    remitaObj.hash = HashPaymentDetailToSHA512(toHash);
                    json = new JavaScriptSerializer().Serialize(remitaObj);
                    using (var request = new WebClient())
                    {
                        request.Headers[HttpRequestHeader.Accept] = "application/json";
                        request.Headers[HttpRequestHeader.ContentType] = "application/json";
                        jsondata = request.UploadString(remitaBaseUrl, "POST", json);

                    }
                    jsondata = jsondata.Replace("jsonp(", "");
                    jsondata = jsondata.Replace(")", "");

                    var remitaResponse = new JavaScriptSerializer().Deserialize<RemitaResponse>(jsondata);

                    return View(viewModel);
                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred:" + ex.Message, Message.Category.Error);
                return View(viewModel);
            }
            return View(viewModel);
        }
        public ActionResult TestRedirect()
        {
            try
            {
                
                //RemitaPayment remitaPyament = new RemitaPayment();
                //RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();   

                //decimal Amount = 11500;

                //List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
                //RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
                //splitItems = splitItemLogic.GetAll();
                //splitItems[0].deductFeeFrom = "0";
                //splitItems[1].deductFeeFrom = "1";


                //string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
                //RemitaSettings settings = new RemitaSettings();
                //RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                //settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 1);
                //long milliseconds = DateTime.Now.Ticks;
                //RemitaPayementProcessor remitaPayementProcessor = new RemitaPayementProcessor(settings.Api_key);
                //Remita remitaObj = new Remita()
                //{

                //    merchantId = settings.MarchantId,
                //    serviceTypeId = settings.serviceTypeId,
                //    totalAmount = Amount,
                //    payerName ="Jack",
                //    payerEmail = "me@gmail.com",
                //    payerPhone = "08046107232",
                //    responseurl = settings.Response_Url,
                //    lineItems = splitItems,
                //    paymenttype = "ACCEPTANCE",
                //    orderId = milliseconds.ToString(),

                //};

                //string toHash = remitaObj.merchantId + remitaObj.serviceTypeId + remitaObj.orderId + remitaObj.totalAmount + remitaObj.responseurl + settings.Api_key;

                //string json = "";
                //string jsondata = "";
                //if (remitaObj != null)
                //{
                //    remitaObj.hash = HashPaymentDetailToSHA512(toHash);
                //    json = new JavaScriptSerializer().Serialize(remitaObj);
                //    using (var request = new WebClient())
                //    {
                //        request.Headers[HttpRequestHeader.Accept] = "application/json";
                //        request.Headers[HttpRequestHeader.ContentType] = "application/json";
                //        jsondata = request.UploadString(remitaBaseUrl, "POST", json);

                //    }
                //    jsondata = jsondata.Replace("jsonp(", "");
                //    jsondata = jsondata.Replace(")", "");

                //    var remitaResponse = new JavaScriptSerializer().Deserialize<RemitaResponse>(jsondata);

                //    return View(viewModel);
                //}

                //string remitaFinal = "https://login.remita.net/remita/ecomm/split/init.reg";
                //string json = "";
                //string jsondata = "";
                // using (var request = new WebClient())
                // {
                //     request.Headers[HttpRequestHeader.Accept] = "application/json";
                //     request.Headers[HttpRequestHeader.ContentType] = "application/json";
                //     jsondata = request.UploadString(remitaFinal, "POST", json);

                // }
                // jsondata = jsondata.Replace("jsonp(", "");
                // jsondata = jsondata.Replace(")", "");


            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred:" + ex.Message, Message.Category.Error);
                return View(viewModel);
            }
            return View(viewModel);
        }
        public string HashPaymentDetailToSHA512(string hash_string)
        {
            System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
            Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
            sha512.Clear();
            string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
            return hashed;

        }
        [HttpPost]
        public ActionResult GenerateSchoolFeesInvoice(string formNo)
        {
            Payment payment = null;

            try
            {
                ApplicationForm form = viewModel.GetApplicationFormBy(formNo);
                if (form != null && form.Id > 0)
                {
                    FeeType feeType = new FeeType() { Id = (int)FeeTypes.SchoolFees };
                    ApplicantStatus.Status status = ApplicantStatus.Status.GeneratedSchoolFeesInvoice;

                    payment = GenerateInvoiceHelper(form, feeType, status);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return Json(new { InvoiceNumber = payment.InvoiceNumber }, "text/html", JsonRequestBehavior.AllowGet);
        }
        private Payment GenerateInvoiceHelper(ApplicationForm form, FeeType feeType, ApplicantStatus.Status status)
        {
            try
            {
                Payment payment = viewModel.GenerateInvoice(form, status, feeType);
                if (payment == null)
                {
                    SetMessage("Operation Failed! Invoice could not be generated.", Message.Category.Error);
                }

                viewModel.Invoice.Payment = payment;
                viewModel.Invoice.Person = form.Person;
                viewModel.Invoice.JambRegistrationNumber = "";
                viewModel.Invoice.ApplicationForm = form;

                if (payment.FeeType.Id == (int)FeeTypes.AcceptanceFee)
                {
                    viewModel.AcceptanceInvoiceNumber = payment.InvoiceNumber;
                }
                else if (payment.FeeType.Id == (int)FeeTypes.SchoolFees)
                {
                    viewModel.SchoolFeesInvoiceNumber = payment.InvoiceNumber;
                }

                return payment;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GenerateAcceptanceReceipt(long fid, string ivn, string con, int st)
        {
            try
            {
                string successMeassge = "Acceptance Receipt has been successfully generated and ready for printing. Print the Acceptance Receipt or Admission Letter by clicking on the Print Receipt or Print Admission Letter button.";
                GenerateReceiptHelper(fid, ivn, con, st, successMeassge);
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Error);
            }

            return PartialView("_Message", TempData["Message"]);
        }
        public static string UpdateSupplementaryFeeType(long fid, string ivn)
        {
            string deleteStatus = "NotDeleted";
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                
                AdmissionList admissionList = admissionListLogic.GetModelsBy(a => a.Application_Form_Id == fid).LastOrDefault();
                Payment payment = paymentLogic.GetModelsBy(p => p.Invoice_Number == ivn).LastOrDefault();
                if (admissionList.Batch.Type.Id == 2 && payment != null && payment.FeeType.Id != (int)FeeTypes.SupplementaryAcceptanceFee && payment.Session.Id == 2)
                {
                    payment.FeeType = new FeeType(){ Id = (int)FeeTypes.SupplementaryAcceptanceFee };

                    paymentLogic.Modify(payment);
                }

                if (admissionList.Batch.Type.Id == 2 && admissionList.Programme.Id == 5 && payment != null && payment.Session.Id == 2)
                {
                    RemitaPayment remitaPayment = remitaPaymentLogic.GetModelBy(r => r.Payment_Id == payment.Id);
                    if (remitaPayment != null && remitaPayment.TransactionAmount == 20000)
                    {
                        if (remitaPaymentLogic.Delete(r => r.Payment_Id == payment.Id))
                        {
                            deleteStatus = "Deleted";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return deleteStatus;
        }
        private bool GenerateReceiptHelper(long fid, string ivn, string con, int st, string successMeassge)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                Model.Model.Session session = new Session();

                if (!string.IsNullOrEmpty(ivn))
                {
                    Payment paymentSession = paymentLogic.GetModelsBy(p => p.Invoice_Number == ivn).LastOrDefault();
                    if (paymentSession != null)
                    {
                        session = paymentSession.Session;
                    }
                    else
                    {
                        session = new Session(){Id = 2};
                    }
                }
                else
                {
                    session = new Session() { Id = 2 };
                }

                Payment payment = new Payment();
                if (con.Length > 15)
                {
                    
                    payment = paymentLogic.InvalidConfirmationOrderNumber(con, session);
                    if (payment != null && payment.Id > 0)
                    {
                        if (payment.InvoiceNumber == ivn)
                        {
                            Receipt receipt = GetReceipt(ivn, fid, st);
                            if (receipt != null)
                            {
                                SetMessage(successMeassge, Message.Category.Information);
                                return true;
                            }
                        }
                        else
                        {
                            SetMessage("Your Acceptance Receipt generation failed because the Confirmation Order Number (" + con + ") entered belongs to another invoice number! Please enter your Confirmation Order Number correctly.", Message.Category.Error);
                        }
                    }
                }
                else
                {
                    RemitaPayment remitaPayment = new RemitaPayment();
                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                    remitaPayment = remitaPaymentLogic.GetModelsBy(a => a.RRR == con).FirstOrDefault();
                    if (remitaPayment != null && remitaPayment.Status.Contains("01:"))
                    {

                        if (remitaPayment.Description.Contains("MANUAL PAYMENT ACCEPTANCE"))
                        {
                            Receipt receipt = GetReceipt(ivn, fid, st);
                            if (receipt != null)
                            {
                                SetMessage(successMeassge, Message.Category.Information);
                                return true;
                            }
                        }

                        if (remitaPayment.payment.InvoiceNumber == ivn)
                        {
                            Receipt receipt = GetReceipt(ivn, fid, st);
                            if (receipt != null)
                            {
                                SetMessage(successMeassge, Message.Category.Information);
                                return true;
                            }
                        }
                        else
                        {
                            SetMessage("Your Receipt generation failed because the Confirmation Order Number (" + con + ") entered belongs to another invoice number! Please enter your Confirmation Order Number correctly.", Message.Category.Error);
                        }
                    }

                    //Get status of transaction
                    RemitaSettings settings = new RemitaSettings();
                    RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                    int settingsId = 0;
                    if ((remitaPayment.payment.FeeType.Id == 2))
                    {
                        settingsId = 1;
                        settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == settingsId);
                    }

                   

                    string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                    RemitaPayementProcessor remitaPayementProcessor = new RemitaPayementProcessor(settings.Api_key);
                    remitaPayment = remitaPayementProcessor.GetStatus(remitaPayment.OrderId);
                    if (remitaPayment != null && remitaPayment.Status.Contains("01:"))
                    {
                        if (remitaPayment.payment.InvoiceNumber == ivn)
                        {
                            Receipt receipt = GetReceipt(ivn, fid, st);
                            if (receipt != null)
                            {
                                SetMessage(successMeassge, Message.Category.Information);
                                return true;
                            }
                        }
                        else
                        {
                            SetMessage("Your Receipt generation failed because the Confirmation Order Number (" + con + ") entered belongs to another invoice number! Please enter your Confirmation Order Number correctly.", Message.Category.Error);
                        }
                    }


                }



                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult GenerateSchoolFeesReceipt(long fid, string ivn, string con, int st)
        {
            try
            {
                string successMeassge = "School Fees Receipt has been successfully generated and ready for printing. Click on the Print Receipt button to print receipt.";

                using (TransactionScope transaction = new TransactionScope())
                {
                    bool isSuccessfull = GenerateReceiptHelper(fid, ivn, con, st, successMeassge);
                    if (isSuccessfull)
                    {
                        //assign matric number
                        ApplicantLogic applicantLogic = new ApplicantLogic();
                        ApplicationFormView applicant = applicantLogic.GetBy(fid);
                        if (applicant != null)
                        {
                            StudentLogic studentLogic = new StudentLogic();
                            bool matricNoAssigned = studentLogic.AssignMatricNumber(applicant);
                            if (matricNoAssigned)
                            {
                                transaction.Complete();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return PartialView("_Message", TempData["Message"]);
        }
        public ActionResult Invoice(string ivn)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                ApplicationForm applicationForm = new ApplicationForm();

                Payment payment = paymentLogic.GetModelBy(p => p.Invoice_Number == ivn);
                if (payment != null)
                {
                    applicationForm = applicationFormLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();
                }

                if (CheckAndUpadteSupplementaryAcceptance(ivn) == "Deleted")
                {
                    if (applicationForm.Number != null)
                    {
                        GenerateAcceptanceInvoice(applicationForm.Number);
                    }
                }

                if (string.IsNullOrEmpty(ivn))
                {
                    SetMessage("Invoice Not Set! ", Message.Category.Error);
                }

                viewModel.GetInvoiceBy(ivn);

                if (viewModel.Invoice.remitaPayment != null)
                {
                    string hash = "532776942" + viewModel.Invoice.remitaPayment.RRR + "587460";
                    RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(hash);
                    viewModel.Hash = remitaProcessor.HashPaymentDetailToSHA512(hash);
                }
                

                TempData["AdmissionViewModel"] = viewModel;
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel.Invoice);
        }
        public static string CheckAndUpadteSupplementaryAcceptance(string ivn)
        {
            string deleteStatus = "";
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

                Payment payment = paymentLogic.GetModelBy(p => p.Invoice_Number == ivn);
                if (payment != null)
                {
                    ApplicationForm applicationForm = applicationFormLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();
                    if (applicationForm != null)
                    {
                        deleteStatus = UpdateSupplementaryFeeType(applicationForm.Id, ivn);
                    }
                }
                
            }
            catch (Exception)
            {
                throw;
            }

            return deleteStatus;
        }
        public ActionResult Receipt(string ivn, long fid, int st)
        {
            Receipt receipt = null;

            try
            {
                receipt = GetReceipt(ivn, fid, st);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(receipt);
        }
        private Receipt GetReceipt(string ivn, long fid, int st)
        {
            Receipt receipt = null;

            try
            {
                ApplicantStatus.Status status = (ApplicantStatus.Status)st;
                if (IsNextApplicationStatus(fid, st))
                {
                    receipt = viewModel.GenerateReceipt(ivn, fid, status);
                }
                else
                {
                    receipt = viewModel.GetReceiptBy(ivn);
                }

                if (receipt == null)
                {
                    SetMessage("No receipt found for Invoice No " + ivn, Message.Category.Error);
                }
                return receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool IsNextApplicationStatus(long formId, int status)
        {
            try
            {
                ApplicationForm form = new ApplicationForm() { Id = formId };

                ApplicantLogic applicantLogic = new ApplicantLogic();
                Model.Model.Applicant applicant = applicantLogic.GetBy(form);
                if (applicant != null)
                {
                    if (applicant.Status.Id < (int)status)
                    {
                        return true;
                    }
                }
                else
                {
                    throw new Exception("Applicant Status not found!");
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void SetSelectedSittingSubjectAndGrade(AdmissionViewModel olevelViewModel)
        {
            try
            {
                if (olevelViewModel != null && olevelViewModel.FirstSittingOLevelResultDetails != null && olevelViewModel.FirstSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail firstSittingOLevelResultDetail in olevelViewModel.FirstSittingOLevelResultDetails)
                    {
                        if (firstSittingOLevelResultDetail.Subject != null && firstSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(olevelViewModel.OLevelSubjectSelectList, Utility.VALUE, Utility.TEXT, firstSittingOLevelResultDetail.Subject.Id);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(olevelViewModel.OLevelGradeSelectList, Utility.VALUE, Utility.TEXT, firstSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(olevelViewModel.OLevelSubjectSelectList, Utility.VALUE, Utility.TEXT, 0);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(olevelViewModel.OLevelGradeSelectList, Utility.VALUE, Utility.TEXT, 0);
                        }

                        i++;
                    }
                }

                if (olevelViewModel != null && olevelViewModel.SecondSittingOLevelResultDetails != null && olevelViewModel.SecondSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail secondSittingOLevelResultDetail in olevelViewModel.SecondSittingOLevelResultDetails)
                    {
                        if (secondSittingOLevelResultDetail.Subject != null && secondSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(olevelViewModel.OLevelSubjectSelectList, Utility.VALUE, Utility.TEXT, secondSittingOLevelResultDetail.Subject.Id);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(olevelViewModel.OLevelGradeSelectList, Utility.VALUE, Utility.TEXT, secondSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(olevelViewModel.OLevelSubjectSelectList, Utility.VALUE, Utility.TEXT, 0);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(olevelViewModel.OLevelGradeSelectList, Utility.VALUE, Utility.TEXT, 0);
                        }

                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Error);
            }
        }
        private bool InvalidOlevelResult(AdmissionViewModel viewModel)
        {
            try
            {
                if (InvalidNumberOfOlevelSubject(viewModel.FirstSittingOLevelResultDetails, viewModel.SecondSittingOLevelResultDetails))
                {
                    return true;
                }

                if (InvalidOlevelSubjectOrGrade(viewModel.FirstSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, Utility.FIRST_SITTING))
                {
                    return true;
                }

                if (viewModel.SecondSittingOLevelResult != null)
                {
                    if (viewModel.SecondSittingOLevelResult.ExamNumber != null && viewModel.SecondSittingOLevelResult.Type != null && viewModel.SecondSittingOLevelResult.Type.Id > 0 && viewModel.SecondSittingOLevelResult.ExamYear > 0)
                    {
                        if (InvalidOlevelSubjectOrGrade(viewModel.SecondSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, Utility.SECOND_SITTING))
                        {
                            return true;
                        }
                    }
                }

                if (InvalidOlevelResultHeaderInformation(viewModel.FirstSittingOLevelResultDetails, viewModel.FirstSittingOLevelResult, Utility.FIRST_SITTING))
                {
                    return true;
                }

                if (InvalidOlevelResultHeaderInformation(viewModel.SecondSittingOLevelResultDetails, viewModel.SecondSittingOLevelResult, Utility.SECOND_SITTING))
                {
                    return true;
                }

                if (NoOlevelSubjectSpecified(viewModel.FirstSittingOLevelResultDetails, viewModel.FirstSittingOLevelResult, Utility.FIRST_SITTING))
                {
                    return true;
                }
                if (NoOlevelSubjectSpecified(viewModel.SecondSittingOLevelResultDetails, viewModel.SecondSittingOLevelResult, Utility.SECOND_SITTING))
                {
                    return true;
                }

                if (InvalidOlevelType(viewModel.FirstSittingOLevelResult.Type, viewModel.SecondSittingOLevelResult.Type))
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool InvalidOlevelSubjectOrGrade(List<OLevelResultDetail> oLevelResultDetails, List<OLevelSubject> subjects, List<OLevelGrade> grades, string sitting)
        {
            try
            {
                List<OLevelResultDetail> subjectList = null;
                if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                {
                    subjectList = oLevelResultDetails.Where(r => r.Subject.Id > 0 || r.Grade.Id > 0).ToList();
                }

                foreach (OLevelResultDetail oLevelResultDetail in subjectList)
                {
                    OLevelSubject subject = subjects.Where(s => s.Id == oLevelResultDetail.Subject.Id).SingleOrDefault();
                    OLevelGrade grade = grades.Where(g => g.Id == oLevelResultDetail.Grade.Id).SingleOrDefault();

                    List<OLevelResultDetail> results = subjectList.Where(o => o.Subject.Id == oLevelResultDetail.Subject.Id).ToList();
                    if (results != null && results.Count > 1)
                    {
                        SetMessage("Duplicate " + subject.Name.ToUpper() + " Subject detected in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }
                    else if (oLevelResultDetail.Subject.Id > 0 && oLevelResultDetail.Grade.Id <= 0)
                    {
                        SetMessage("No Grade specified for Subject " + subject.Name.ToUpper() + " in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }
                    else if (oLevelResultDetail.Subject.Id <= 0 && oLevelResultDetail.Grade.Id > 0)
                    {
                        SetMessage("No Subject specified for Grade" + grade.Name.ToUpper() + " in " + sitting + "! Please modify.", Message.Category.Error);
                        return true;
                    }
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool InvalidOlevelResultHeaderInformation(List<OLevelResultDetail> resultDetails, OLevelResult oLevelResult, string sitting)
        {
            try
            {
                if (resultDetails != null && resultDetails.Count > 0)
                {
                    List<OLevelResultDetail> subjectList = resultDetails.Where(r => r.Subject.Id > 0).ToList();

                    if (subjectList != null && subjectList.Count > 0)
                    {
                        if (string.IsNullOrEmpty(oLevelResult.ExamNumber))
                        {
                            SetMessage("O-Level Exam Number not set for " + sitting + " ! Please modify", Message.Category.Error);
                            return true;
                        }
                        else if (oLevelResult.Type == null || oLevelResult.Type.Id <= 0)
                        {
                            SetMessage("O-Level Exam Type not set for " + sitting + " ! Please modify", Message.Category.Error);
                            return true;
                        }
                        else if (oLevelResult.ExamYear <= 0)
                        {
                            SetMessage("O-Level Exam Year not set for " + sitting + " ! Please modify", Message.Category.Error);
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool NoOlevelSubjectSpecified(List<OLevelResultDetail> oLevelResultDetails, OLevelResult oLevelResult, string sitting)
        {
            try
            {
                if (!string.IsNullOrEmpty(oLevelResult.ExamNumber) || (oLevelResult.Type != null && oLevelResult.Type.Id > 0) || (oLevelResult.ExamYear > 0))
                {
                    if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                    {
                        List<OLevelResultDetail> oLevelResultDetailsEntered = oLevelResultDetails.Where(r => r.Subject.Id > 0).ToList();
                        if (oLevelResultDetailsEntered == null || oLevelResultDetailsEntered.Count <= 0)
                        {
                            SetMessage("No O-Level Subject specified for " + sitting + "! At least one subject must be specified when Exam Number, O-Level Type and Year are all specified for the sitting.", Message.Category.Error);
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool InvalidNumberOfOlevelSubject(List<OLevelResultDetail> firstSittingResultDetails, List<OLevelResultDetail> secondSittingResultDetails)
        {
            const int FIVE = 5;

            try
            {
                int totalNoOfSubjects = 0;

                List<OLevelResultDetail> firstSittingSubjectList = null;
                List<OLevelResultDetail> secondSittingSubjectList = null;

                if (firstSittingResultDetails != null && firstSittingResultDetails.Count > 0)
                {
                    firstSittingSubjectList = firstSittingResultDetails.Where(r => r.Subject.Id > 0).ToList();
                    if (firstSittingSubjectList != null)
                    {
                        totalNoOfSubjects += firstSittingSubjectList.Count;
                    }
                }

                if (secondSittingResultDetails != null && secondSittingResultDetails.Count > 0)
                {
                    secondSittingSubjectList = secondSittingResultDetails.Where(r => r.Subject.Id > 0).ToList();
                    if (secondSittingSubjectList != null)
                    {
                        totalNoOfSubjects += secondSittingSubjectList.Count;
                    }
                }

                if (totalNoOfSubjects == 0)
                {
                    SetMessage("No O-Level Result Details found for both sittings!", Message.Category.Error);
                    return true;
                }
                else if (totalNoOfSubjects < FIVE)
                {
                    SetMessage("O-Level Result cannot be less than " + FIVE + " subjects in both sittings!", Message.Category.Error);
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        private bool InvalidOlevelType(OLevelType firstSittingOlevelType, OLevelType secondSittingOlevelType)
        {
            try
            {
                if (firstSittingOlevelType != null && secondSittingOlevelType != null)
                {
                    if ((firstSittingOlevelType.Id != secondSittingOlevelType.Id) && firstSittingOlevelType.Id > 0 && secondSittingOlevelType.Id > 0)
                    {
                        if (firstSittingOlevelType.Id == (int)OLevelTypes.Nabteb)
                        {
                            SetMessage("NABTEB O-Level Type in " + Utility.FIRST_SITTING + " cannot be combined with any other O-Level Type! Please modify.", Message.Category.Error);
                            return true;
                        }
                        else if (secondSittingOlevelType.Id == (int)OLevelTypes.Nabteb)
                        {
                            SetMessage("NABTEB O-Level Type in " + Utility.SECOND_SITTING + " cannot be combined with any other O-Level Type! Please modify.", Message.Category.Error);
                            return true;
                        }
                    }
                }

                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult VerifyOlevelResult(AdmissionViewModel viewModel)
        {
            try
            {

                //validate o-level result entry
                if (InvalidOlevelResult(viewModel))
                {
                    return PartialView("_Message", TempData["Message"]);
                }

                using (TransactionScope transaction = new TransactionScope())
                {
                    //get applicant's applied course
                    if (viewModel.FirstSittingOLevelResult == null || viewModel.FirstSittingOLevelResult.Id <= 0)
                    {
                        viewModel.FirstSittingOLevelResult.ApplicationForm = viewModel.ApplicationForm;
                        viewModel.FirstSittingOLevelResult.Person = viewModel.ApplicationForm.Person;
                        viewModel.FirstSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 1 };
                    }

                    if (viewModel.SecondSittingOLevelResult == null || viewModel.SecondSittingOLevelResult.Id <= 0)
                    {
                        viewModel.SecondSittingOLevelResult.ApplicationForm = viewModel.ApplicationForm;
                        viewModel.SecondSittingOLevelResult.Person = viewModel.ApplicationForm.Person;
                        viewModel.SecondSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 2 };
                    }

                    ModifyOlevelResult(viewModel.FirstSittingOLevelResult, viewModel.FirstSittingOLevelResultDetails);
                    ModifyOlevelResult(viewModel.SecondSittingOLevelResult, viewModel.SecondSittingOLevelResultDetails);


                    transaction.Complete();
                    SetMessage("O-Level result has been successfully updated!", Message.Category.Information);

                }



            }
            catch (Exception ex)
            {
                SetMessage(ex.Message, Message.Category.Error);
            }

            return PartialView("_Message", TempData["Message"]);
        }
        private void ModifyOlevelResult(OLevelResult oLevelResult, List<OLevelResultDetail> oLevelResultDetails)
        {
            try
            {
                OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
                if (oLevelResult != null && oLevelResult.ExamNumber != null && oLevelResult.Type != null && oLevelResult.ExamYear > 0)
                {
                    if (oLevelResult != null && oLevelResult.Id > 0)
                    {
                        oLevelResultDetailLogic.DeleteBy(oLevelResult);
                    }
                    else
                    {
                        OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
                        OLevelResult newOLevelResult = oLevelResultLogic.Create(oLevelResult);
                        oLevelResult.Id = newOLevelResult.Id;
                    }

                    if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
                    {
                        List<OLevelResultDetail> olevelResultDetails = oLevelResultDetails.Where(m => m.Grade != null && m.Grade.Id > 0 && m.Subject != null && m.Subject.Id > 0).ToList();
                        foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
                        {
                            oLevelResultDetail.Header = oLevelResult;
                        }

                        oLevelResultDetailLogic.Create(olevelResultDetails);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult CardPayment()
        {
            AdmissionViewModel viewModel = (AdmissionViewModel)TempData["AdmissionViewModel"];
            TempData.Keep("PostJAMBFormPaymentViewModel");

            return View(viewModel);
        }
    }
}