﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using System.Linq.Expressions;
using System.Web.Routing;
using System.IO;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;
using System.Transactions;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using System.Configuration;
using System.Drawing;
using System.Net;
using BarcodeLib;
using MailerApp.Business;
using Newtonsoft.Json;

namespace Abundance_Nk.Web.Areas.Applicant.Controllers
{
	[AllowAnonymous]
	public class FormController : BaseController
	{
		private const string ID = "Id";
		private const string NAME = "Name";
		private const string VALUE = "Value";
		private const string TEXT = "Text";
		private const string DEFAULT_PASSPORT = "/Content/Images/default_avatar.png";

		private PostJambViewModel viewModel;
		public ActionResult PostJambProgramme()
		{
			PostJAMBProgrammeViewModel viewModel = new PostJAMBProgrammeViewModel();
		   
			try
			{
				TempData["viewModel"] = null;
				TempData["imageUrl"] = null;
				TempData["ProgrammeViewModel"] = null;
				TempData["PostJAMBFormPaymentViewModel"] = null;
				ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), "Value", "Text");
			}
			catch(Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			return View(viewModel);
		}
		[HttpPost]
		public ActionResult PostJambProgramme(PostJAMBProgrammeViewModel viewModel)
		{
			try
			{
				
				RemitaPayment remitaPayment = new RemitaPayment();
				RemitaPaymentLogic remitaPyamentLogic = new RemitaPaymentLogic();

				ModelState.Remove("FeeType.Name");

				if (ModelState.IsValid)
				{
					//live code -- to uncomment later
					Payment payment = InvalidConfirmationOrderNumber(viewModel.ConfirmationOrderNumber, viewModel.FeeType);
					if (payment == null || payment.Id <= 0)
					{
						remitaPayment = remitaPyamentLogic.GetModelsBy(a => a.RRR == viewModel.ConfirmationOrderNumber && a.Status.Contains("01:")  && a.PAYMENT.Fee_Type_Id == viewModel.FeeType.Id).FirstOrDefault();
						if(remitaPayment == null || remitaPayment.payment.Id <= 0)
						{
							SetMessage("Confirmation Order Number/RRR entered was not found or valid for this payment!",Message.Category.Error);
							ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), "Value", "Text");
							return View(viewModel);
						}
						payment = remitaPayment.payment;
						//SetMessage("Confirmation Order Number entered was not found!", Message.Category.Error);
						//return View(viewModel);
					}

					PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
					if(remitaPayment.RRR == null)
					{
						bool pinUseStatus = etranzactLogic.IsPinUsed(viewModel.ConfirmationOrderNumber,(int)payment.Person.Id);
						if(pinUseStatus)
						{
							SetMessage("Pin has been used by another applicant! Please cross check and Try again.",Message.Category.Error);
							ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), "Value", "Text");
							return View(viewModel);
						}
					}

					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

					ApplicationForm formWithoutRegistration = applicationFormLogic.GetModelsBy(a => a.Payment_Id == payment.Id).LastOrDefault();

					if (formWithoutRegistration != null && formWithoutRegistration.Remarks != null && formWithoutRegistration.Remarks.Contains("REGISTERED"))
					{
					   //Do nothing
					}
					else
					{
						//SetMessage("Application has closed!...", Message.Category.Error);
						//return RedirectToAction("PostJambProgramme");
					}

					//PaymentLogic paymentLogic = new PaymentLogic();
					//Payment payment = paymentLogic.GetModelBy(p => p.Invoice_Number == "FPN150000008011");

					AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
					AppliedCourse appliedCourse = appliedCourseLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

					ApplicantJambDetailLogic studentJambDetailLogic = new ApplicantJambDetailLogic();
					ApplicantJambDetail studentJambDetail = studentJambDetailLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

					PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = new PostJAMBFormPaymentViewModel();
					if (studentJambDetail != null)
					{
						postJAMBFormPaymentViewModel.JambRegistrationNumber = studentJambDetail.JambRegistrationNumber;
					}

					postJAMBFormPaymentViewModel.Payment = payment;
					if (appliedCourse != null)
					{
						postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
						postJAMBFormPaymentViewModel.AppliedCourse = appliedCourse;
						postJAMBFormPaymentViewModel.Person = payment.Person;
						postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
						postJAMBFormPaymentViewModel.FeeType = payment.FeeType;
						postJAMBFormPaymentViewModel.CurrentSession = payment.Session;
						postJAMBFormPaymentViewModel.Initialise();
					}
					else
					{
						SetMessage("The Confirmation Order Number entered does not have a corresponding applied course! Please contact your system administrator", Message.Category.Error);
						ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), "Value", "Text");
						return View(viewModel);
					}

					TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
					return RedirectToAction("PostJambForm", "Form");

					//ApplicationForm form = applicationFormLogic.GetModelsBy(a => a.Payment_Id == payment.Id).LastOrDefault();

					//if (form != null && form.Number != null && formWithoutRegistration != null && formWithoutRegistration.Remarks == null)
					//{
					//    PostJambViewModel existingViewModel = new PostJambViewModel();
					//    //ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					//    ApplicantLogic applicantLogic = new ApplicantLogic();
					//    SponsorLogic sponsorLogic = new SponsorLogic();
					//    OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
					//    OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
					//    ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();

					//    existingViewModel.ApplicationAlreadyExist = true;
					//    existingViewModel.AppliedCourse = appliedCourseLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();
					//    existingViewModel.Person = payment.Person;
					//    existingViewModel.ApplicationForm = applicationFormLogic.GetModelsBy(a => a.Payment_Id == payment.Id).LastOrDefault();
					//    existingViewModel.Session = payment.Session;
					//    if (existingViewModel.ApplicationForm != null)
					//    {
					//        existingViewModel.ApplicationProgrammeFee = existingViewModel.ApplicationForm.ProgrammeFee;
					//        existingViewModel.ApplicationFormSetting = existingViewModel.ApplicationFormSetting;
					//        existingViewModel.Applicant = applicantLogic.GetModelsBy(a => a.Application_Form_Id == existingViewModel.ApplicationForm.Id).LastOrDefault();
							
					//    }
					//    if (existingViewModel.AppliedCourse != null)
					//    {
					//        existingViewModel.Programme = existingViewModel.AppliedCourse.Programme;
					//    }
					//    existingViewModel.Payment = payment;
					//    existingViewModel.Sponsor = sponsorLogic.GetModelsBy(s => s.Person_Id == payment.Person.Id).LastOrDefault();

					//    existingViewModel.FirstSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Person_Id == payment.Person.Id && o.O_Level_Exam_Sitting_Id == 1).LastOrDefault();
					//    if (existingViewModel.FirstSittingOLevelResult != null && existingViewModel.FirstSittingOLevelResult.Id > 0)
					//    {
					//        existingViewModel.FirstSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == existingViewModel.FirstSittingOLevelResult.Id);
					//    }
						
					//    existingViewModel.SecondSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Person_Id == payment.Person.Id && o.O_Level_Exam_Sitting_Id == 2).LastOrDefault();
					//    if (existingViewModel.SecondSittingOLevelResult != null && existingViewModel.SecondSittingOLevelResult.Id > 0)
					//    {
					//        existingViewModel.SecondSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == existingViewModel.SecondSittingOLevelResult.Id);
					//    }

					//    existingViewModel.ApplicantJambDetail = applicantJambDetailLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();

					//    TempData["viewModel"] = existingViewModel;

					//    return RedirectToAction("PostJAMBSlip");
					//}
					//else if (formWithoutRegistration != null && formWithoutRegistration.Remarks != null && formWithoutRegistration.Remarks.Contains("REGISTERED"))
					//{
					//    TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
					//    return RedirectToAction("PostJambForm", "Form");
					//}
					//else
					//{
					//    SetMessage("Application has closed!...", Message.Category.Error);
					//    return RedirectToAction("PostJambProgramme");
					//}
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			ViewBag.FeeTypeId = new SelectList(Utility.PopulateFormFeeTypeSelectListItem(), "Value", "Text");
			return View(viewModel);
		}
		private Payment InvalidConfirmationOrderNumber(string confirmationOrderNumber, FeeType feeType)
		{
			Payment payment = new Payment();
			PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();
			PaymentEtranzact etranzactDetails = etranzactLogic.GetModelBy(m => m.Confirmation_No == confirmationOrderNumber);
			if (etranzactDetails == null)
			{


				RemitaPayment remitaPayment = new RemitaPayment();
				RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
				RemitaPayementProcessor r = new RemitaPayementProcessor("918567");
				remitaPayment = remitaPaymentLogic.GetModelsBy(a => a.RRR == confirmationOrderNumber).FirstOrDefault();
				if(remitaPayment != null && remitaPayment.Description.Contains("SCREENING FORM"))
				{
					remitaPayment = r.GetStatus(remitaPayment.OrderId);
					//remitaPayment.TransactionAmount == 2500 &
					if( remitaPayment.Status.Contains("01:"))
					{
						payment = remitaPayment.payment;
						return payment;
					}
					else
					{
						SetMessage("Payment could not be verified, Try again in a few minuteds",Message.Category.Error);
						payment = null;
						return payment;
					}
				}
				else if (remitaPayment != null)
				{
					remitaPayment = r.GetStatus(remitaPayment.OrderId);
					//remitaPayment.TransactionAmount == 2500 &
					if (remitaPayment.Status.Contains("01:"))
					{
						payment = remitaPayment.payment;
						return payment;
					}
					else
					{
						SetMessage("Payment could not be verified, Try again in a few minuteds", Message.Category.Error);
						payment = null;
						return payment;
					}
				}
				else if(remitaPayment == null && confirmationOrderNumber.Length > 12)
				{
					PaymentTerminal paymentTerminal = new PaymentTerminal();
					PaymentTerminalLogic paymentTerminalLogic = new PaymentTerminalLogic();
					//paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == 1 && p.Session_Id == 2);
					paymentTerminal = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == feeType.Id && p.Session_Id == 3); //::BS

					etranzactDetails = etranzactLogic.RetrievePinAlternative(confirmationOrderNumber, paymentTerminal);
					if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
					{
						PaymentLogic paymentLogic = new PaymentLogic();
						payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
						if (payment != null && payment.Id > 0)
						{
							FeeDetail feeDetail = new FeeDetail();
							FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
							feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);
						   if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
						   {
							   SetMessage("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.", Message.Category.Error);
							   payment = null;
							   return payment;
						   }
					   
						}
						else
						{

							SetMessage("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.", Message.Category.Error);
					   
						}
					}
					else
					{
						PaymentTerminal paymentTerminal2 = paymentTerminalLogic.GetModelBy(p => p.Fee_Type_Id == 9 && p.Session_Id == 2);
						etranzactDetails = etranzactLogic.RetrievePinAlternative(confirmationOrderNumber, paymentTerminal2);

						if (etranzactDetails != null && etranzactDetails.ReceiptNo != null)
						{
							PaymentLogic paymentLogic = new PaymentLogic();
							payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);

							if (payment != null && payment.Id > 0)
							{
								FeeDetail feeDetail = new FeeDetail();
								FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
								feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);
								if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
								{
									SetMessage("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.", Message.Category.Error);
									payment = null;
									return payment;
								}

							}
							else
							{

								SetMessage("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.", Message.Category.Error);

							}
						}
						else
						{
						   SetMessage("Confirmation Order Number entered seems not to be valid! Please cross check and try again.", Message.Category.Error); 
						}
					}
				}
				
			}
			else
			{
				PaymentLogic paymentLogic = new PaymentLogic();
				payment = paymentLogic.GetModelBy(m => m.Invoice_Number == etranzactDetails.CustomerID);
				if (payment != null && payment.Id > 0)
				{
					FeeDetail feeDetail = new FeeDetail();
					FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
					feeDetail = feeDetailLogic.GetModelBy(a => a.Fee_Type_Id == payment.FeeType.Id && a.Session_Id == payment.Session.Id);
				  
					if (!etranzactLogic.ValidatePin(etranzactDetails, payment, feeDetail.Fee.Amount))
					{
						SetMessage("The pin amount tied to the pin is not correct. Please contact support@lloydant.com.", Message.Category.Error);
						payment = null;
						return payment;
					}
				}
				else
				{
					SetMessage("The invoice number attached to the pin doesn't belong to you! Please cross check and try again.", Message.Category.Error);
					//return View(viewModel);
				}
			}

			return payment;
		}
		
		//[HttpPost]
		//public ActionResult PostJambProgramme(PostJAMBProgrammeViewModel viewModel)
		//{
		//    try
		//    {
		//        if (ModelState.IsValid)
		//        {
		//            PaymentLogic paymentLogic = new PaymentLogic();
		//            Payment payment = paymentLogic.GetModelBy(m => m.Invoice_Number == viewModel.ConfirmationOrderNumber);
		//            if (payment == null || payment.Id <= 0)
		//            {
		//                SetMessage("Confirmation Order Number enetered was not found!", Message.Category.Error);
		//                return View(viewModel);
		//            }

		//            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
		//            AppliedCourse appliedCourse = appliedCourseLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

		//            ApplicantJambDetailLogic studentJambDetailLogic = new ApplicantJambDetailLogic();
		//            ApplicantJambDetail studentJambDetail = studentJambDetailLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

		//            PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = new PostJAMBFormPaymentViewModel();
		//            if (studentJambDetail != null)
		//            {
		//                postJAMBFormPaymentViewModel.JambRegistrationNumber = studentJambDetail.JambRegistrationNumber;
		//            }

		//            postJAMBFormPaymentViewModel.Payment = payment;
		//            if (appliedCourse != null)
		//            {
		//                postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
		//                postJAMBFormPaymentViewModel.AppliedCourse = appliedCourse;
		//                postJAMBFormPaymentViewModel.Person = payment.Person;
		//                postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
		//                postJAMBFormPaymentViewModel.Initialise();
		//            }
		//            else
		//            {
		//                SetMessage("The Confirmation Order Number enetered do not have a corresponding applied course! Please contact your system administrator", Message.Category.Error);
		//                return View(viewModel);
		//            }

		//            TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
		//            return RedirectToAction("PostJambForm", "Form");
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
		//    }

		//    //MaintainAllDropDownSelection(viewModel);
		//    return View(viewModel);
		//}

		//[HttpPost]
		//public ActionResult PostJambProgramme(PostJAMBProgrammeViewModel viewModel)
		//{
		//    try
		//    {
		//        if (viewModel.YouWantToPay)
		//        {
					
		//            if (InvalidProgrammeSelection(viewModel))
		//            {
		//                MaintainAllDropDownSelection(viewModel);
		//                return View(viewModel);
		//            }

		//            if (viewModel.Programme.Id != 3)
		//            {
		//                if (InvalidJambRegistrationNumber(viewModel))
		//                {
		//                    MaintainAllDropDownSelection(viewModel);
		//                    return View(viewModel);
		//                }
		//            }

		//            TempData["ProgrammeViewModel"] = viewModel;
		//            return RedirectToAction("PostJambFormInvoiceGeneration", "Form");
		//        }
		//        else
		//        {
		//            if (InvalidConfirmationNumber(viewModel))
		//            {
		//                MaintainAllDropDownSelection(viewModel);
		//                return View(viewModel);
		//            }

		//            PaymentLogic paymentLogic = new PaymentLogic();
		//            Payment payment = paymentLogic.GetModelBy(m => m.Invoice_Number == viewModel.ConfirmationOrderNumber);
		//            if (payment == null || payment.Id <= 0)
		//            {
		//                SetMessage("Confirmation Order Number enetered was not found!", Message.Category.Error);
		//                MaintainAllDropDownSelection(viewModel);
		//                return View(viewModel);
		//            }

		//            AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
		//            AppliedCourse appliedCourse = appliedCourseLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

		//            StudentJambDetailLogic studentJambDetailLogic = new StudentJambDetailLogic();
		//            StudentJambDetail studentJambDetail = studentJambDetailLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);

		//            PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = new PostJAMBFormPaymentViewModel();
		//            //postJAMBFormPaymentViewModel.PostJAMBProgrammeViewModel = new PostJAMBProgrammeViewModel();
		//            if (studentJambDetail != null)
		//            {
		//                postJAMBFormPaymentViewModel.JambRegistrationNumber = studentJambDetail.JambRegistrationNumber;
		//            }

		//            postJAMBFormPaymentViewModel.Payment = payment;
		//            if (appliedCourse != null)
		//            {
		//                postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
		//                postJAMBFormPaymentViewModel.AppliedCourse = appliedCourse;
		//                postJAMBFormPaymentViewModel.Student = payment.Person as Abundance_Nk.Model.Model.Student;
		//                postJAMBFormPaymentViewModel.Programme = appliedCourse.Programme;
		//                postJAMBFormPaymentViewModel.Initialise();

		//                postJAMBFormPaymentViewModel.Student.Id = payment.Person.Id;
		//                postJAMBFormPaymentViewModel.Student.FirstName = payment.Person.FirstName;
		//                postJAMBFormPaymentViewModel.Student.LastName = payment.Person.LastName;
		//                postJAMBFormPaymentViewModel.Student.OtherName = payment.Person.OtherName;
		//                postJAMBFormPaymentViewModel.Student.FullName = payment.Person.FullName;
		//                postJAMBFormPaymentViewModel.Student.Email = payment.Person.Email;
		//                postJAMBFormPaymentViewModel.Student.MobilePhone = payment.Person.MobilePhone;
		//                postJAMBFormPaymentViewModel.Student.DateEntered = payment.Person.DateEntered;
		//            }
		//            else
		//            {
		//                SetMessage("The Confirmation Order Number enetered do not have a corresponding applied course! Please contact your system administrator", Message.Category.Error);
		//                MaintainAllDropDownSelection(viewModel);
		//                return View(viewModel);
		//            }

		//            TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
		//            return RedirectToAction("PostJambForm", "Form");
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
		//    }

		//    MaintainAllDropDownSelection(viewModel);
		//    return View(viewModel);
		//}

		//private bool InvalidConfirmationNumber(PostJAMBProgrammeViewModel viewModel)
		//{
		//    try
		//    {
		//        if (string.IsNullOrEmpty(viewModel.ConfirmationOrderNumber))
		//        {
		//            SetMessage("Please enter Confirmation Order Number!", Message.Category.Error);
		//            return true;
		//        }

		//        return false;
		//    }
		//    catch(Exception)
		//    {
		//        throw;
		//    }
		//}
		
		private void CreateStudentJambDetail(string jambRegNo, Person person)
		{
			try
			{
				 if (string.IsNullOrEmpty(jambRegNo))
				 {
					 return;
				 }

				ApplicantJambDetail jambDetail = new ApplicantJambDetail();
				jambDetail.JambRegistrationNumber = jambRegNo;
				jambDetail.Person = person;
				jambDetail.JambSubject1 = new OLevelSubject(){Id = 1};

				ApplicantJambDetailLogic studentJambDetailLogic = new ApplicantJambDetailLogic();
				studentJambDetailLogic.Create(jambDetail);
			}
			catch (Exception)
			{
				throw;
			}
		}
		private bool InvalidJambRegistrationNumber(PostJAMBFormPaymentViewModel viewModel)
		{
			const int TEN = 10;
			const string SEVEN = "7";

			try
			{
				if (string.IsNullOrEmpty(viewModel.JambRegistrationNumber))
				{
					SetMessage("Please enter your JAMB Registration Number!", Message.Category.Error);
					return true;
				}
				else if (viewModel.JambRegistrationNumber.Length != TEN)
				{
					SetMessage("JAMB Registration Number must be equal to " + TEN + " digits!", Message.Category.Error);
					return true;
				}

				int lastAlphabet;
				int secondTolastAlphabet;
				int firstEightNumbers;

				string firstEightCharacters = viewModel.JambRegistrationNumber.Substring(0, 8);
				string lastCharacter = viewModel.JambRegistrationNumber.Substring(9, 1);
				string secondToLastCharacter = viewModel.JambRegistrationNumber.Substring(8, 1);
				string firstCharacter = viewModel.JambRegistrationNumber.Substring(0, 1);
				
				bool lastCharacterIsAlphabet = int.TryParse(lastCharacter, out lastAlphabet);
				bool secondToLastCharacterIsAlphabet = int.TryParse(secondToLastCharacter, out secondTolastAlphabet);
				bool firstEightCharacterIsNumber = int.TryParse(firstEightCharacters, out firstEightNumbers);

				if (firstCharacter != SEVEN)
				{
					SetMessage("The JAMB Registration Number must start with a 7", Message.Category.Error);
					return true;
				}

				if (firstEightCharacterIsNumber == false)
				{
					SetMessage("The first eight characters of JAMB Registration Number must all be numbers! Please modify.", Message.Category.Error);
					return true;
				}

				if (secondToLastCharacterIsAlphabet && lastCharacterIsAlphabet)
				{
					SetMessage("The last two characters of JAMB Registration Number must be alphabets! Please modify.", Message.Category.Error);
					return true;
				}
				else if (lastCharacterIsAlphabet)
				{
					SetMessage("The last character of JAMB Registration Number must be an alphabet! Please modify.", Message.Category.Error);
					return true;
				}
				else if (secondToLastCharacterIsAlphabet)
				{
					SetMessage("The second to the last character of JAMB Registration Number must be an alphabet! Please modify.", Message.Category.Error);
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}
	   
		public ActionResult PostJambFormInvoiceGeneration()
		{
			PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = new PostJAMBFormPaymentViewModel();
			try
			{
				ViewBag.StateId = postJAMBFormPaymentViewModel.StateSelectList;
				ViewBag.ProgrammeId = postJAMBFormPaymentViewModel.ProgrammeSelectListItem;
				ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
				ViewBag.DepartmentOptionId = new SelectList(new List<DepartmentOption>(), ID, NAME);
				ViewBag.FeeTypeId = postJAMBFormPaymentViewModel.FeeTypeSelectListItem;
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
			return View(postJAMBFormPaymentViewModel);
		}

		//[HttpPost]
		//public ActionResult PostJambFormInvoiceGeneration(PostJAMBFormPaymentViewModel viewModel)
		//{
		//	try
		//	{
		//		int[] currentProgrammes = {(int)Programmes.NDEvening, (int)Programmes.NDWeekend, (int)Programmes.NDMorning};
		//		if (!currentProgrammes.Contains(viewModel.Programme.Id))
		//		{
		//			SetMessage("Application has not started for the selected Programme!...", Message.Category.Error);
		//			return RedirectToAction("PostJambFormInvoiceGeneration"); 
		//		}
		//		//if (viewModel.FeeType.Id != (int)FeeTypes.ApplicationForm)
		//		//{
		//		//    SetMessage("The selected form is not on sale!", Message.Category.Error);
		//		//    return RedirectToAction("PostJambFormInvoiceGeneration"); 
		//		//}
				
		//		ModelState.Remove("Person.DateOfBirth");
		//		ModelState.Remove("FeeType.Name");
		//		if (ModelState.IsValid)
		//		{

		//			viewModel.CurrentSession = new Session(){ Id = 3};

		//			viewModel.Initialise();

		//			if (InvalidDepartmentSelection(viewModel))
		//			{
		//				KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//				return View(viewModel);
		//			}

		//			//supplementary courses
		//			int[] coursesToApply = {17, 24, 29, 26, 32, 30, 23, 14, 12, 21, 22, 35, 10, 19};
		//			//if (viewModel.Programme.Id != 5 && viewModel.FeeType.Id != (int)FeeTypes.SupplementaryForm)
		//			//{
		//			//    SetMessage("Application has closed for the selected Programme.", Message.Category.Error);
		//			//    KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//    return View(viewModel); 
		//			//}
		//			//if (!coursesToApply.Contains(viewModel.AppliedCourse.Department.Id) && viewModel.FeeType.Id != (int)FeeTypes.SupplementaryForm)
		//			//{
		//			//    SetMessage("Application has closed for the selected course.", Message.Category.Error);
		//			//    KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//    return View(viewModel);  
		//			//}
		//			//if (viewModel.Programme.Id == 5 && viewModel.FeeType.Id == (int)FeeTypes.SupplementaryForm)
		//			//{
		//			//    SetMessage("No Supplementary application for the selected Programme.", Message.Category.Error);
		//			//    KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//    return View(viewModel);
		//			//}
		//			//if (viewModel.Programme.Id == (int)Programmes.NDMorning)
		//			//{
		//			//	if (viewModel.FeeType.Id == (int)FeeTypes.SupplementaryForm)
		//			//	{
		//			//		if (!coursesToApply.Contains(viewModel.AppliedCourse.Department.Id))
		//			//		{
		//			//			SetMessage("Supplementary Application is not open for the selected course.", Message.Category.Error);
		//			//			KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//			return View(viewModel);
		//			//		}
		//			//	}
		//			//	else
		//			//	{
		//			//		SetMessage("The selected application is not on sale for the selected programme.", Message.Category.Error);
		//			//		KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//		return View(viewModel);
		//			//	}
		//			//}

		//			//if (viewModel.Programme.Id != (int)Programmes.NDMorning && viewModel.FeeType.Id == (int)FeeTypes.SupplementaryForm)
		//			//{
		//			//	SetMessage("The selected application is not on sale for the selected programme.", Message.Category.Error);
		//			//	KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//	return View(viewModel);
		//			//}

		//			//if (viewModel.Programme.Id == (int)Programmes.NDEvening )
		//			//{
		//			//	SetMessage("Application is not on sale for the selected programme.", Message.Category.Error);
		//			//	KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//			//	return View(viewModel);
		//			//}
					
		//			//using (TransactionScope transaction = new TransactionScope())
		//			using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
		//			{
		//				//if (viewModel.Programme.Id == 5)
		//				//{
		//				//    viewModel.FeeType = new FeeType() {Id = 4};
		//				//}
		//				Person person = CreatePerson(viewModel);
		//				Payment payment = CreatePayment(viewModel);

		//				viewModel.Payment = new Payment() { Id = payment.Id, InvoiceNumber = payment.InvoiceNumber, PaymentType = payment.PaymentType, Person = person, FeeDetails = payment.FeeDetails };

		//				AppliedCourse appliedCourse = CreateAppliedCourse(viewModel);
		//				CreateStudentJambDetail(viewModel.JambRegistrationNumber, person);

		//				//transaction.Complete();
		//				//All payments goes to remita
		//				if (payment != null)
		//				{
		//					//Get Payment Specific Setting
		//					decimal Amt = 0;
		//					Amt = payment.FeeDetails.Sum(p => p.Fee.Amount);
		//					RemitaSettings settings = new RemitaSettings();
		//					RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
		//					settings = settingsLogic.GetBy(2);

		//					//Get Split Specific details;
		//					List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
		//					RemitaSplitItems singleItem = new RemitaSplitItems();
		//					RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
		//					singleItem = splitItemLogic.GetBy(6);
		//					singleItem.deductFeeFrom = "1";
		//					singleItem.beneficiaryAmount = "0";
		//					splitItems.Add(singleItem);
		//					singleItem = splitItemLogic.GetBy(5);
		//					singleItem.deductFeeFrom = "0";
		//					singleItem.beneficiaryAmount = Convert.ToString(Amt - Convert.ToDecimal(splitItems[0].beneficiaryAmount));
		//					splitItems.Add(singleItem);

		//					string transactionDescription = "SCREENING FORM";
		//					if (viewModel.FeeType.Id == (int)FeeTypes.SupplementaryForm)
		//					{
		//						transactionDescription = "SUPPLEMENTARY FORM";
		//					}
		//					else if (viewModel.FeeType.Id == (int)FeeTypes.ApplicationForm && viewModel.Programme.Id == (int)Programmes.NDMorning)
		//					{
		//						transactionDescription = "SCREENING FORM";
		//					}
		//					else
		//					{
		//						transactionDescription = "APPLICATION FORM";
		//					}

		//					//Get BaseURL
		//					string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
		//					Amt = payment.FeeDetails.Sum(a => a.Fee.Amount);

		//					RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
		//					viewModel.remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, transactionDescription, splitItems, settings, Amt);
		//					if (viewModel.remitaPayment != null)
		//					{
		//						transaction.Complete();
		//					}

		//				}
		//				else
		//				{
		//					  transaction.Complete();
							
		//				}
		//			}
					  
		//			TempData["PostJAMBFormPaymentViewModel"] = viewModel;
		//			return RedirectToAction("Invoice", "Form" );
					
		//		}
		//	}
		//	catch (Exception ex)
		//	{
		//		SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
		//	}

		//	KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
		//	return View(viewModel);
		//}
		[HttpPost]
		public ActionResult PostJambFormInvoiceGeneration(PostJAMBFormPaymentViewModel viewModel)
		{
			try
			{
				int[] currentProgrammes = { (int)Programmes.NDEvening, (int)Programmes.NDWeekend, (int)Programmes.NDMorning };
				if (!currentProgrammes.Contains(viewModel.Programme.Id))
				{
					SetMessage("Application has not started for the selected Programme!...", Message.Category.Error);
					return RedirectToAction("PostJambFormInvoiceGeneration");
				}

				ModelState.Remove("Person.DateOfBirth");
				ModelState.Remove("FeeType.Name");
				if (ModelState.IsValid)
				{
					SessionLogic sessionLogic = new SessionLogic();

					viewModel.CurrentSession = sessionLogic.GetCurrentSession();

					viewModel.Initialise();

					if (InvalidDepartmentSelection(viewModel))
					{
						KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
						return View(viewModel);
					}
					
					using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
					{
						Person person = CreatePerson(viewModel);
						Payment payment = CreatePayment(viewModel);

						viewModel.Payment = new Payment() { Id = payment.Id, InvoiceNumber = payment.InvoiceNumber, PaymentType = payment.PaymentType, Person = person, FeeDetails = payment.FeeDetails };

						AppliedCourse appliedCourse = CreateAppliedCourse(viewModel);
						CreateStudentJambDetail(viewModel.JambRegistrationNumber, person);
						
						transaction.Complete();
					}

					TempData["PostJAMBFormPaymentViewModel"] = viewModel;
					
					return RedirectToAction("ProcessRemitaPayment");
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			KeepApplicationFormInvoiceGenerationDropDownState(viewModel);
			return View(viewModel);
		}

		public ActionResult ProcessRemitaPayment()
		{
			PostJAMBFormPaymentViewModel viewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];
			try
			{
				if (viewModel != null)
				{
					PaymentLogic paymentLogic = new PaymentLogic();
					Payment payment = paymentLogic.GetBy(viewModel.Payment.Id);
					if (payment != null)
					{
						//Get Payment Specific Setting
						decimal Amt = 0;
						Amt = payment.FeeDetails.Sum(p => p.Fee.Amount);
						RemitaSettings settings = new RemitaSettings();
						RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
						settings = settingsLogic.GetBy(2);

						Remita remita = new Remita()
						{
							merchantId = settings.MarchantId,
							serviceTypeId = settings.serviceTypeId,
							orderId = payment.InvoiceNumber.ToString(),
							totalAmount = (decimal)Amt,
							payerName = payment.Person.FullName,
							payerEmail = !string.IsNullOrEmpty(payment.Person.Email) ? payment.Person.Email : "test@lloydant.com",
							payerPhone = payment.Person.MobilePhone,
							responseurl = "http://ndapplication.fpno.edu.ng/Applicant/Form/RemitaResponse",
							paymenttype = "RRRGEN",
							amt = Amt.ToString()
						};

						remita.amt = remita.amt.Split('.')[0];

						string hash_string = remita.merchantId + remita.serviceTypeId + remita.orderId + remita.amt + remita.responseurl + settings.Api_key;
						System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
						Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
						sha512.Clear();
						string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();

						remita.hash = hashed;

						viewModel.Remita = remita;
					}
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			TempData["PostJAMBFormPaymentViewModel"] = viewModel;

			return View(viewModel);
		}
		public ActionResult RemitaResponse(string orderID)
		{
			RemitaResponse remitaResponse = new RemitaResponse();
			PostJAMBFormPaymentViewModel viewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];
			if (viewModel == null)
			{
				viewModel = new PostJAMBFormPaymentViewModel();
			}

			try
			{
				string merchant_id = "532776942";
				string apiKey = "587460";
				string hashed;
				string checkstatusurl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
				string url;
				
				if (Request.QueryString["orderID"] != null)
				{
					PaymentLogic paymentLogic = new PaymentLogic();
					RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();
					AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
					ApplicationProgrammeFeeLogic programmeFeeLogic = new ApplicationProgrammeFeeLogic();
					AppliedCourse appliedCourse = new AppliedCourse();
					ApplicationProgrammeFee programmeFee = new ApplicationProgrammeFee();
					RemitaPayment remitaPyament = new RemitaPayment();

					orderID = Request.QueryString["orderID"].ToString();

					Payment payment = paymentLogic.GetBy(orderID);

					if (payment != null)
					{
						remitaPyament = remitaLogic.GetModelBy(p => p.Payment_Id == payment.Id);
						appliedCourse = appliedCourseLogic.GetModelsBy(a => a.Person_Id == payment.Person.Id).LastOrDefault();
						if (appliedCourse != null)
						{
							programmeFee = programmeFeeLogic.GetModelBy(p => p.Fee_Type_Id == payment.FeeType.Id && p.Session_Id == payment.Session.Id && p.Programme_Id == appliedCourse.Programme.Id);
						}
					}
					else
					{
						remitaPyament = null;
					}

					string hash_string = orderID + apiKey + merchant_id;
					System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
					Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
					sha512.Clear();
					hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
					url = checkstatusurl + "/" + merchant_id + "/" + orderID + "/" + hashed + "/" + "orderstatus.reg";
					
					
					//viewModel.Hash = hashed;

					if (remitaPyament == null)
					{
						string jsondata = new WebClient().DownloadString(url);
						remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
					}

					if (remitaResponse != null && remitaResponse.Status != null)
					{
						if (payment != null)
						{
							if (remitaPyament != null)
							{
								remitaPyament.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
								remitaLogic.Modify(remitaPyament);
							}
							else
							{
								remitaPyament = new RemitaPayment();
								remitaPyament.payment = payment;
								remitaPyament.RRR = remitaResponse.rrr;
								remitaPyament.OrderId = remitaResponse.orderId;
								remitaPyament.Status = remitaResponse.Status;
								remitaPyament.TransactionAmount = remitaResponse.amount;
								remitaPyament.TransactionDate = DateTime.Now;
								remitaPyament.MerchantCode = merchant_id;
								remitaPyament.Description = "APPLICATION FORM";
								if (remitaLogic.GetBy(payment.Id) == null)
								{
									remitaLogic.Create(remitaPyament);
								}
							}

							viewModel.remitaPayment = remitaPyament;
							viewModel.Payment = payment;
							viewModel.Person = payment.Person;

						}
						else
						{
							RemitaPayment remitaPayment = remitaLogic.GetBy(orderID);
							string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
							RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(apiKey);
							remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
							if (remitaResponse != null && remitaResponse.Status != null)
							{
								remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
								remitaLogic.Modify(remitaPayment);

								payment = paymentLogic.GetBy(remitaPayment.payment.Id);
								viewModel.remitaPayment = remitaPayment;
								if (payment != null)
								{
									viewModel.Payment = payment;
									viewModel.Person = payment.Person;
								}
							}
							else
							{
								SetMessage("Payment does not exist!", Message.Category.Error);
							}

							viewModel.remitaPayment = remitaPayment;
						}


						string hash = "532776942" + remitaResponse.rrr + "587460";
						RemitaPayementProcessor myRemitaProcessor = new RemitaPayementProcessor(hash);
						viewModel.Hash = myRemitaProcessor.HashPaymentDetailToSHA512(hash);
					}
					else
					{
						SetMessage("Order ID was not generated from this system", Message.Category.Error);
					}

					viewModel.Department = appliedCourse != null ? appliedCourse.Department : new Department();
					viewModel.ApplicationProgrammeFee = programmeFee != null ? programmeFee : new ApplicationProgrammeFee();
					viewModel.AppliedCourse = appliedCourse;
				}
				else
				{
					SetMessage("No data was received!", Message.Category.Error);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error! " + ex.Message, Message.Category.Error);
			}
			
			TempData["PostJAMBFormPaymentViewModel"] = viewModel;
			return View(viewModel);
		}
		private bool InvalidDepartmentSelection(PostJAMBFormPaymentViewModel viewModel)
		{
			try
			{
				if (viewModel.AppliedCourse.Department == null || viewModel.AppliedCourse.Department.Id <= 0)
				{
					SetMessage("Please select Department!", Message.Category.Error);
					return true;
				}
			   

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void KeepApplicationFormInvoiceGenerationDropDownState(PostJAMBFormPaymentViewModel viewModel)
		{
			try
			{
				if (viewModel.Person.State != null && !string.IsNullOrEmpty(viewModel.Person.State.Id))
				{
					ViewBag.StateId = new SelectList(viewModel.StateSelectList, VALUE, TEXT, viewModel.Person.State.Id);
				}
				else
				{
					ViewBag.StateId = new SelectList(viewModel.StateSelectList, VALUE, TEXT);
				}

				if (viewModel.Programme != null && viewModel.Programme.Id > 0)
				{
					viewModel.DepartmentSelectListItem = Utility.PopulateDepartmentSelectListItem(viewModel.Programme);
					ViewBag.ProgrammeId = new SelectList(viewModel.ProgrammeSelectListItem, VALUE, TEXT, viewModel.Programme.Id);
					if (viewModel.AppliedCourse.Department != null && viewModel.AppliedCourse.Department.Id > 0)
					{
						
						viewModel.DepartmentOptionSelectListItem = Utility.PopulateDepartmentOptionSelectListItem(viewModel.AppliedCourse.Department, viewModel.Programme);

						ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, VALUE, TEXT, viewModel.AppliedCourse.Department.Id);
						if (viewModel.AppliedCourse.Option != null && viewModel.AppliedCourse.Option.Id > 0)
						{
							ViewBag.DepartmentOptionId = new SelectList(viewModel.DepartmentOptionSelectListItem, VALUE, TEXT, viewModel.AppliedCourse.Option.Id);
						}
						else
						{
							if (viewModel.DepartmentOptionSelectListItem != null && viewModel.DepartmentOptionSelectListItem.Count > 0)
							{
								ViewBag.DepartmentOptionId = new SelectList(viewModel.DepartmentOptionSelectListItem, VALUE, TEXT);
							}
							else
							{
								ViewBag.DepartmentOptionId = new SelectList(new List<DepartmentOption>(), ID, NAME);
							}
						}
					}
					else
					{
						ViewBag.DepartmentId = new SelectList(viewModel.DepartmentSelectListItem, VALUE, TEXT);
						ViewBag.DepartmentOptionId = new SelectList(new List<DepartmentOption>(), ID, NAME);
					}
				}
				else
				{
					ViewBag.ProgrammeId = new SelectList(viewModel.ProgrammeSelectListItem, VALUE, TEXT);
					ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
					ViewBag.DepartmentOptionId = new SelectList(new List<DepartmentOption>(), ID, NAME);
				}
				if (viewModel.FeeType != null && viewModel.FeeType.Id > 0)
				{
					ViewBag.FeeTypeId = new SelectList(viewModel.FeeTypeSelectListItem, VALUE, TEXT, viewModel.FeeType.Id); 
				}
				else
				{
					ViewBag.FeeTypeId = new SelectList(viewModel.FeeTypeSelectListItem, VALUE, TEXT);
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public ActionResult Invoice()
		{
			PostJAMBFormPaymentViewModel viewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];
			DepartmentLogic departmentLogic = new DepartmentLogic();
			PaymentLogic paymentLogic = new PaymentLogic();
			viewModel.Department = departmentLogic.GetModelBy(m => m.Department_Id == viewModel.AppliedCourse.Department.Id);

			List<FeeDetail> FeeDetails = viewModel.Payment.FeeDetails;
			viewModel.Payment = paymentLogic.GetModelBy(p => p.Payment_Id == viewModel.Payment.Id);
			viewModel.Payment.FeeDetails = FeeDetails;

			if (viewModel.Programme.Id == 5)
			{
				string hash = "532776942" + viewModel.remitaPayment.RRR + "587460";
				RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(hash);
				viewModel.Hash = remitaProcessor.HashPaymentDetailToSHA512(hash); 
			} 
			
			TempData["PostJAMBFormPaymentViewModel"] = viewModel;
			return View(viewModel);
		}
		public ActionResult CardPayment()
		{
			PostJAMBFormPaymentViewModel viewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];
			TempData.Keep("PostJAMBFormPaymentViewModel");

			return View(viewModel);
		}

		public ActionResult PostJambForm()
		{
			PostJambViewModel existingViewModel = (PostJambViewModel)TempData["viewModel"];
			PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];

			UpdateApplicationFormRemarks(postJAMBFormPaymentViewModel.Person);

			try
			{
				PopulateAllDropDowns();
								
				if (existingViewModel != null)
				{
					viewModel = existingViewModel;
					SetStudentUploadedPassport(viewModel);
				}

				if (postJAMBFormPaymentViewModel != null)
				{
					if (postJAMBFormPaymentViewModel != null && postJAMBFormPaymentViewModel.Programme != null)
					{
						viewModel.Session = postJAMBFormPaymentViewModel.CurrentSession;
						viewModel.Programme = postJAMBFormPaymentViewModel.Programme;

						//if (viewModel.Programme.Id == 1 || viewModel.Programme.Id == 2 || viewModel.Programme.Id == 3 || viewModel.Programme.Id == 4)
						//{
						//    viewModel.ApplicantJambDetail = null;
						//}
						//else 

						if (viewModel.Programme.Id == 5)
						{
							viewModel.PreviousEducation = null;
							viewModel.ApplicantJambDetail.JambRegistrationNumber = postJAMBFormPaymentViewModel.JambRegistrationNumber;
							viewModel.ApplicantJambDetail.JambSubject1 = new OLevelSubject(){Id = 1};
						}
					   
						viewModel.Payment = postJAMBFormPaymentViewModel.Payment;
						viewModel.AppliedCourse = postJAMBFormPaymentViewModel.AppliedCourse;
						viewModel.ApplicationFormSetting = postJAMBFormPaymentViewModel.ApplicationFormSetting;
						viewModel.ApplicationProgrammeFee = postJAMBFormPaymentViewModel.ApplicationProgrammeFee;

						viewModel.Person.Id = postJAMBFormPaymentViewModel.Person.Id;
						viewModel.Person.FirstName = postJAMBFormPaymentViewModel.Person.FirstName;
						viewModel.Person.LastName = postJAMBFormPaymentViewModel.Person.LastName;
						viewModel.Person.OtherName = postJAMBFormPaymentViewModel.Person.OtherName;
						viewModel.Person.FullName = postJAMBFormPaymentViewModel.Person.FullName;
						viewModel.Person.Email = postJAMBFormPaymentViewModel.Person.Email;
						viewModel.Person.MobilePhone = postJAMBFormPaymentViewModel.Person.MobilePhone;
						viewModel.Person.ContactAddress = postJAMBFormPaymentViewModel.Person.ContactAddress;
						viewModel.Person.DateEntered = postJAMBFormPaymentViewModel.Person.DateEntered;
						viewModel.Person.State = postJAMBFormPaymentViewModel.Person.State;

						SetLgaIfExist(viewModel);
					}
				}

				ApplicationForm applicationform = viewModel.GetApplicationFormBy(viewModel.Person, viewModel.Payment);
				//if (applicationform != null)
				//{
				//    viewModel.ApplicationFormSetting = applicationform.Setting;
				//    viewModel.ApplicationProgrammeFee = applicationform.ProgrammeFee;
				//    postJAMBFormPaymentViewModel.ApplicationFormSetting = applicationform.Setting;
				//    postJAMBFormPaymentViewModel.ApplicationProgrammeFee = applicationform.ProgrammeFee;
				//}
				if ((applicationform != null && applicationform.Id > 0) && viewModel.ApplicationAlreadyExist == false)
				{
					viewModel.ApplicationAlreadyExist = true;

					viewModel.LoadApplicantionFormBy(viewModel.Person, viewModel.Payment);
					SetSelectedSittingSubjectAndGrade(viewModel);

					//To troubleshoot
					if (postJAMBFormPaymentViewModel != null && postJAMBFormPaymentViewModel.ApplicantJambDetail != null)
					{
						postJAMBFormPaymentViewModel.ApplicantJambDetail.ApplicationForm = applicationform;
					}
					if (postJAMBFormPaymentViewModel != null && postJAMBFormPaymentViewModel.AppliedCourse != null)
					{
						postJAMBFormPaymentViewModel.AppliedCourse.ApplicationForm = applicationform;
					}

					SetLgaIfExist(viewModel);

					SetDateOfBirth();
					if (viewModel.AppliedCourse.Programme.Id == 3 || viewModel.AppliedCourse.Programme.Id == 4)
					{
						SetPreviousEducationEndDate();
						SetPreviousEducationStartDate();
					}
				   
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			TempData["viewModel"] = viewModel;
			TempData["imageUrl"] = viewModel.Person.ImageFileUrl;
			TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
			return View(viewModel);
		}
		private void UpdateApplicationFormRemarks(Person person)
		{
			try
			{
				if (person != null)
				{
					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					ApplicationForm applicationForm = applicationFormLogic.GetModelBy(a => a.Person_Id == person.Id);

					if (applicationForm != null && applicationForm.Remarks != null && applicationForm.Remarks.Contains("UNREGISTERED"))
					{
						applicationForm.Remarks = "REGISTERED";
						applicationFormLogic.Modify(applicationForm);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}
		private void SetLgaIfExist(PostJambViewModel viewModel)
		{
			try
			{
				if (viewModel.Person.State != null && !string.IsNullOrEmpty(viewModel.Person.State.Id))
				{
					LocalGovernmentLogic LocalGovernmentLogic = new LocalGovernmentLogic();
					List<LocalGovernment> lgas = LocalGovernmentLogic.GetModelsBy(l => l.State_Id == viewModel.Person.State.Id);
					if (viewModel.Person.LocalGovernment != null && viewModel.Person.LocalGovernment.Id > 0)
					{
						ViewBag.LgaId = new SelectList(lgas, ID, NAME, viewModel.Person.LocalGovernment.Id);
					}
					else
					{
						ViewBag.LgaId = new SelectList(lgas, ID, NAME);
					}
				}
				else
				{
					ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), ID, NAME);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
				//SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}

		private void SetDateOfBirth()
		{
			try
			{
				if (viewModel.Person.DateOfBirth.HasValue)
				{
					int noOfDays = DateTime.DaysInMonth(viewModel.Person.YearOfBirth.Id, viewModel.Person.MonthOfBirth.Id);
					List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
					if (days != null && days.Count > 0)
					{
						days.Insert(0, new Value() { Name = "--DD--" });
					}

					if (viewModel.Person.DayOfBirth != null && viewModel.Person.DayOfBirth.Id > 0)
					{
						ViewBag.DayOfBirthId = new SelectList(days, ID, NAME, viewModel.Person.DayOfBirth.Id);
					}
					else
					{
						ViewBag.DayOfBirthId = new SelectList(days, ID, NAME);
					}
				}
				else
				{
					ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
				//SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}
		
		private void SetPreviousEducationStartDate()
		{
			try
			{
				if (viewModel.PreviousEducation.StartDate != null)
				{
					int noOfDays = DateTime.DaysInMonth(viewModel.PreviousEducation.StartYear.Id, viewModel.PreviousEducation.StartMonth.Id);
					List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
					if (days != null && days.Count > 0)
					{
						days.Insert(0, new Value() { Name = "--DD--" });
					}

					if (viewModel.PreviousEducation.StartDay != null && viewModel.PreviousEducation.StartDay.Id > 0)
					{
						ViewBag.PreviousEducationStartDayId = new SelectList(days, ID, NAME, viewModel.PreviousEducation.StartDay.Id);
					}
					else
					{
						ViewBag.PreviousEducationStartDayId = new SelectList(days, ID, NAME);
					}
				}
				else
				{
					ViewBag.PreviousEducationStartDayId = new SelectList(new List<Value>(), ID, NAME);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
				//SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}

		private void SetPreviousEducationEndDate()
		{
			try
			{
				if (viewModel.PreviousEducation.EndDate != null)
				{
					int noOfDays = DateTime.DaysInMonth(viewModel.PreviousEducation.EndYear.Id, viewModel.PreviousEducation.EndMonth.Id);
					List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
					if (days != null && days.Count > 0)
					{
						days.Insert(0, new Value() { Name = "--DD--" });
					}

					if (viewModel.PreviousEducation.EndDay != null && viewModel.PreviousEducation.EndDay.Id > 0)
					{
						ViewBag.PreviousEducationEndDayId = new SelectList(days, ID, NAME, viewModel.PreviousEducation.EndDay.Id);
					}
					else
					{
						ViewBag.PreviousEducationEndDayId = new SelectList(days, ID, NAME);
					}
				}
				else
				{
					ViewBag.PreviousEducationEndDayId = new SelectList(new List<Value>(), ID, NAME);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
				//SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}
		
		[HttpPost]
		public ActionResult PostJambForm(PostJambViewModel viewModel)
		{
			const string FIRST_SITTING = "FIRST SITTING";
			const string SECOND_SITTING = "SECOND SITTING";
		   
			PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];

			try
			{
				SetStudentUploadedPassport(viewModel);

				//ModelState["ApplicationProgrammeFee.FeeType.Name"].Errors.Clear();

				//ModelState["ApplicationProgrammeFee.FeeType.Name"].Errors.Clear();
				//ModelState["ApplicationFormSetting.PaymentType.Name"].Errors.Clear();
				////ModelState["ApplicantJambDetail.JambSubject1.Id"].Errors.Clear();
				//viewModel.ApplicantJambDetail.JambSubject1 = new OLevelSubject() { Id = 1 };

				var errors = from modelstate in ModelState.AsQueryable().Where(f => f.Value.Errors.Count > 0) select new { Title = modelstate.Key };

				ModelState.Remove("ApplicantJambDetail.JambSubject1.Id");
				ModelState.Remove("ApplicationFormSetting.PaymentType.Name");
				ModelState.Remove("ApplicationProgrammeFee.FeeType.Name");

				if (ModelState.IsValid)
				{
					if (InvalidDateOfBirth(viewModel))
					{
						SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
						return View(viewModel);
					}

					if (InvalidOlevelResultSubject(viewModel.FirstSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, FIRST_SITTING))
					{
						SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
						return View(viewModel);
					}
				   
					if (viewModel.AppliedCourse.Programme.Id == 5)
					{
						if (InvalidJambSubject(viewModel.ApplicantJambDetail))
						{
							SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
							return View(viewModel);
						}
					}
					

					if (viewModel.SecondSittingOLevelResult != null)
					{
						if (viewModel.SecondSittingOLevelResult.ExamNumber != null && viewModel.SecondSittingOLevelResult.Type != null && viewModel.SecondSittingOLevelResult.Type.Id > 0 && viewModel.SecondSittingOLevelResult.ExamYear > 0)
						{
							if (InvalidOlevelResultSubject(viewModel.SecondSittingOLevelResultDetails, viewModel.OLevelSubjects, viewModel.OLevelGrades, SECOND_SITTING))
							{
								SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
								return View(viewModel);
							}
						}
						else if (viewModel.SecondSittingOLevelResult.ExamNumber != null || viewModel.SecondSittingOLevelResult.Type.Id > 0 || viewModel.SecondSittingOLevelResult.ExamYear > 0)
						{
							SetMessage("One or more fields not set in " + SECOND_SITTING + " header! Please modify and try again.", Message.Category.Error);

							SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
							return View(viewModel);
						}
					}

					if (InvalidOlevelResult(viewModel.SecondSittingOLevelResultDetails, viewModel.SecondSittingOLevelResult, SECOND_SITTING))
					{
						SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
						return View(viewModel);
					}

				   

					if (string.IsNullOrEmpty(viewModel.Person.ImageFileUrl) || viewModel.Person.ImageFileUrl == DEFAULT_PASSPORT)
					{
						SetMessage("No Passport uploaded! Please upload your passport to continue.", Message.Category.Error);
						SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
						return View(viewModel);
					}
					
					TempData["viewModel"] = viewModel;
					return RedirectToAction("PostJambPreview", "Form");
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			SetPostJAMBStateVariables(viewModel, postJAMBFormPaymentViewModel);
			return View(viewModel);
		}

		private bool InvalidPreviousEducationStartAndEndDate(PreviousEducation previousEducation)
		{
			const int ONE_YEAR = 365;

			try
			{
				if (InvalidPreviousEducationStartDate(previousEducation))
				{
					return true;
				}
				else if (InvalidPreviousEducationEndDate(previousEducation))
				{
					return true;
				}

				DateTime previousEducationStartDate = new DateTime(previousEducation.StartYear.Id, previousEducation.StartMonth.Id, previousEducation.StartDay.Id);
				DateTime previousEducationEndDate = new DateTime(previousEducation.EndYear.Id, previousEducation.EndMonth.Id, previousEducation.EndDay.Id);

				bool isStartDateInTheFuture = Utility.IsDateInTheFuture(previousEducationStartDate);
				bool isEndDateInTheFuture = Utility.IsDateInTheFuture(previousEducationEndDate);

				if (isStartDateInTheFuture)
				{
					SetMessage("Previous Education Start Date cannot be a future date!", Message.Category.Error);
					return true;
				}
				else if (isEndDateInTheFuture)
				{
					SetMessage("Previous Education End Date cannot be a future date!", Message.Category.Error);
					return true;
				}
				else if (Utility.IsStartDateGreaterThanEndDate(previousEducationStartDate, previousEducationEndDate))
				{
					SetMessage("Previous Education Start Date '" + previousEducationStartDate.ToShortDateString() + "' cannot be greater than End Date '" + previousEducationEndDate.ToShortDateString() + "'! Please modify and try again.", Message.Category.Error);
					return true;
				}
				else if (Utility.IsDateOutOfRange(previousEducationStartDate, previousEducationEndDate, ONE_YEAR))
				{
					SetMessage("Previous Education duration must not be less than one year, twelve months or 365 days to be qualified!", Message.Category.Error);
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void SetStudentUploadedPassport(PostJambViewModel viewModel)
		{
			if (viewModel != null && viewModel.Person != null && !string.IsNullOrEmpty((string)TempData["imageUrl"]))
			{
				viewModel.Person.ImageFileUrl = (string)TempData["imageUrl"];
			}
			else
			{
				viewModel.Person.ImageFileUrl = DEFAULT_PASSPORT;
			}
		}

		private bool InvalidPreviousEducationStartDate(PreviousEducation previousEducation)
		{
			try
			{
				if (previousEducation.StartYear == null || previousEducation.StartYear.Id <= 0)
				{
					SetMessage("Please select Previous Education Start Year!", Message.Category.Error);
					return true;
				}
				else if (previousEducation.StartMonth == null || previousEducation.StartMonth.Id <= 0)
				{
					SetMessage("Please select Previous Education Start Month!", Message.Category.Error);
					return true;
				}
				else if (previousEducation.StartDay == null || previousEducation.StartDay.Id <= 0)
				{
					SetMessage("Please select Previous Education Start Day!", Message.Category.Error);
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool InvalidPreviousEducationEndDate(PreviousEducation previousEducation)
		{
			try
			{
				if (previousEducation.EndYear == null || previousEducation.EndYear.Id <= 0)
				{
					SetMessage("Please select Previous Education End Year!", Message.Category.Error);
					return true;
				}
				else if (previousEducation.EndMonth == null || previousEducation.EndMonth.Id <= 0)
				{
					SetMessage("Please select Previous Education End Month!", Message.Category.Error);
					return true;
				}
				else if (previousEducation.EndDay == null || previousEducation.EndDay.Id <= 0)
				{
					SetMessage("Please select Previous Education End Day!", Message.Category.Error);
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool InvalidDateOfBirth(PostJambViewModel viewModel)
		{
			try
			{
				if (viewModel.Person.YearOfBirth == null || viewModel.Person.YearOfBirth.Id <= 0)
				{
					SetMessage("Please select Year of Birth!", Message.Category.Error);
					return true;
				}
				else if (viewModel.Person.MonthOfBirth == null || viewModel.Person.MonthOfBirth.Id <= 0)
				{
					SetMessage("Please select Month of Birth!", Message.Category.Error);
					return true;
				}
				else if (viewModel.Person.DayOfBirth == null || viewModel.Person.DayOfBirth.Id <= 0)
				{
					SetMessage("Please select Day of Birth!", Message.Category.Error);
					return true;
				}

				viewModel.Person.DateOfBirth = new DateTime(viewModel.Person.YearOfBirth.Id, viewModel.Person.MonthOfBirth.Id, viewModel.Person.DayOfBirth.Id);
				if (viewModel.Person.DateOfBirth == null)
				{
					SetMessage("Please enter Date of Birth!", Message.Category.Error);
					return true;
				}

				TimeSpan difference = DateTime.Now - (DateTime)viewModel.Person.DateOfBirth;
				if (difference.Days == 0)
				{
					SetMessage("Date of Birth cannot be todays date!", Message.Category.Error);
					return true;
				}
				else if (difference.Days == -1)
				{
					SetMessage("Date of Birth cannot be yesterdays date date!", Message.Category.Error);
					return true;
				}

				if (difference.Days < 4380)
				{
					SetMessage("Applicant cannot be less than twelve years!", Message.Category.Error);
					return true;
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void SetPostJAMBStateVariables(PostJambViewModel viewModel, PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel)
		{
			try
			{
				TempData["viewModel"] = viewModel;
				TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
				TempData["imageUrl"] = viewModel.Person.ImageFileUrl;

				PopulateAllDropDowns();
				TempData["PostJAMBFormPaymentViewModel"] = postJAMBFormPaymentViewModel;
			}
			catch(Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}
		}

		private bool InvalidOlevelResult(List<OLevelResultDetail> resultDetails, OLevelResult oLevelResult, string sitting)
		{
			try
			{
				if (resultDetails != null && resultDetails.Count > 0)
				{
					List<OLevelResultDetail> subjectList = resultDetails.Where(r => r.Subject.Id > 0).ToList();

					if (subjectList != null && subjectList.Count > 0)
					{
						if (string.IsNullOrEmpty(oLevelResult.ExamNumber) || oLevelResult.Type == null || oLevelResult.Type.Id <= 0 || oLevelResult.ExamYear <= 0)
						{
							SetMessage("Header Information not set for " + sitting + " O-Level Result Details! ", Message.Category.Error);
							return true;
						}
					}
				}
				
				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool InvalidOlevelResultSubject(List<OLevelResultDetail> resultDetails, List<OLevelSubject> subjects, List<OLevelGrade> grades, string sitting)
		{
			try
			{
				List<OLevelResultDetail>  subjectList = resultDetails.Where(r => r.Subject.Id > 0).ToList();

				if (subjectList == null || subjectList.Count == 0)
				{
					SetMessage("No O-Level Result Details found!", Message.Category.Error);
					return true;
				}
				else if (subjectList.Count < 5)
				{
					SetMessage("O-Level Result cannot be less than less 5 subjects in " + sitting + "!", Message.Category.Error);
					return true;
				}

				foreach (OLevelResultDetail oLevelResultDetail in subjectList)
				{
					OLevelSubject subject = subjects.Where(s => s.Id == oLevelResultDetail.Subject.Id).SingleOrDefault();
					OLevelGrade grade = grades.Where(g => g.Id == oLevelResultDetail.Grade.Id).SingleOrDefault();

					List<OLevelResultDetail> results = subjectList.Where(o => o.Subject.Id == oLevelResultDetail.Subject.Id).ToList();
					if (results != null && results.Count > 1)
					{
						SetMessage("Duplicate " + subject.Name.ToUpper() + " Subject detected in " + sitting + "! Please modify.", Message.Category.Error);
						return true;
					}
					else if (oLevelResultDetail.Subject.Id > 0 && oLevelResultDetail.Grade.Id <= 0)
					{
						SetMessage("No Grade specified for " + subject.Name.ToUpper() + " for " + sitting + "! Please modify.", Message.Category.Error);
						return true;
					}
					else if (oLevelResultDetail.Subject.Id <= 0 && oLevelResultDetail.Grade.Id > 0)
					{
						SetMessage("No Subject specified for " + grade.Name.ToUpper() + " for " + sitting + "! Please modify.", Message.Category.Error);
						return true;
					}
				}

				return false;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool InvalidJambSubject(ApplicantJambDetail jambDetail)
		{
			OLevelSubjectLogic oLevelSubjectLogic = new OLevelSubjectLogic();
			List<OLevelSubject> selectedSubjects = new List<OLevelSubject>();
			List<OLevelSubject> subjects = oLevelSubjectLogic.GetModelsBy(a => a.Activated_For_Jamb == true);
			if (subjects != null && subjects.Count > 1)
			{
				selectedSubjects.Add(jambDetail.JambSubject1);
				selectedSubjects.Add(jambDetail.JambSubject2);
				selectedSubjects.Add(jambDetail.JambSubject3);
				selectedSubjects.Add(jambDetail.JambSubject4);

				var dups = selectedSubjects.GroupBy(x => x.Id).Where(x => x.Count() > 1).Select(x => x.Key).ToList();
				if (dups != null && dups.Count() > 0)
				foreach (int subjectId in dups)
				{
					OLevelSubject subject = subjects.Where(a => a.Id == subjectId).SingleOrDefault();
					SetMessage("Duplicate " + subject.Name.ToUpper() + " Subject detected in jamb courses! Please modify.", Message.Category.Error);
					return true;
				}
			   
			}
		   
			  
				return false;
		}

		public ActionResult PostJambPreview()
		{
			PostJambViewModel viewModel = (PostJambViewModel)TempData["viewModel"];

			try
			{
				if (viewModel != null)
				{
					viewModel.Person.DateOfBirth = new DateTime(viewModel.Person.YearOfBirth.Id, viewModel.Person.MonthOfBirth.Id, viewModel.Person.DayOfBirth.Id);
					viewModel.Person.State = viewModel.States.Where(m => m.Id == viewModel.Person.State.Id).SingleOrDefault();
					viewModel.Person.LocalGovernment = viewModel.Lgas.Where(m => m.Id == viewModel.Person.LocalGovernment.Id).SingleOrDefault();
					viewModel.Person.Sex = viewModel.Genders.Where(m => m.Id == viewModel.Person.Sex.Id).SingleOrDefault();
					viewModel.Applicant.Ability = viewModel.Abilities.Where(m => m.Id == viewModel.Applicant.Ability.Id).SingleOrDefault();
					viewModel.Sponsor.Relationship = viewModel.Relationships.Where(m => m.Id == viewModel.Sponsor.Relationship.Id).SingleOrDefault();
					viewModel.Person.Religion = viewModel.Religions.Where(m => m.Id == viewModel.Person.Religion.Id).SingleOrDefault();

					viewModel.FirstSittingOLevelResult.Type = viewModel.OLevelTypes.Where(m => m.Id == viewModel.FirstSittingOLevelResult.Type.Id).SingleOrDefault();
					if (viewModel.SecondSittingOLevelResult.Type != null)
					{
						viewModel.SecondSittingOLevelResult.Type = viewModel.OLevelTypes.Where(m => m.Id == viewModel.SecondSittingOLevelResult.Type.Id).SingleOrDefault();
					}

					if (viewModel.AppliedCourse.Programme.Id == 5)
					{
						viewModel.ApplicantJambDetail.InstitutionChoice = viewModel.InstitutionChoices.Where(m => m.Id == viewModel.ApplicantJambDetail.InstitutionChoice.Id).SingleOrDefault();
						viewModel.ApplicantJambDetail.JambSubject1 = viewModel.OLevelSubjects.Where(m => m.Id == viewModel.ApplicantJambDetail.JambSubject1.Id).SingleOrDefault();
						viewModel.ApplicantJambDetail.JambSubject2 = viewModel.OLevelSubjects.Where(m => m.Id == viewModel.ApplicantJambDetail.JambSubject2.Id).SingleOrDefault();
						viewModel.ApplicantJambDetail.JambSubject3 = viewModel.OLevelSubjects.Where(m => m.Id == viewModel.ApplicantJambDetail.JambSubject3.Id).SingleOrDefault();
						viewModel.ApplicantJambDetail.JambSubject4 = viewModel.OLevelSubjects.Where(m => m.Id == viewModel.ApplicantJambDetail.JambSubject4.Id).SingleOrDefault();
					}
					//else
					//{
					//}

					UpdateOLevelResultDetail(viewModel);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			TempData["viewModel"] = viewModel;
			return View(viewModel);
		}

		[HttpPost]
		public ActionResult PostJambPreview(PostJambViewModel viewModel)
		{
			ApplicationForm newApplicationForm = null;
			PostJambViewModel existingViewModel = (PostJambViewModel)TempData["viewModel"];


			try
			{
				if (viewModel.ApplicationAlreadyExist == false)
				{
					using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
					{
						ApplicationForm applicationForm = new ApplicationForm();
						ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
						applicationForm.ProgrammeFee = new ApplicationProgrammeFee() { Id = existingViewModel.ApplicationProgrammeFee.Id };
						applicationForm.Setting = new ApplicationFormSetting() { Id = existingViewModel.ApplicationFormSetting.Id };
						applicationForm.DateSubmitted = DateTime.Now;
						applicationForm.Person = viewModel.Person;
						applicationForm.Payment = viewModel.Payment;
						applicationForm.ProgrammeFee.Programme = existingViewModel.Programme;

						newApplicationForm = applicationFormLogic.Create(applicationForm);
						existingViewModel.ApplicationFormNumber = newApplicationForm.Number;
						newApplicationForm.Person = existingViewModel.Person;
						existingViewModel.ApplicationForm = newApplicationForm;
						
						existingViewModel.Applicant.Person = viewModel.Person;
						existingViewModel.Applicant.ApplicationForm = newApplicationForm;
						existingViewModel.Applicant.Status = new ApplicantStatus() { Id = (int)ApplicantStatus.Status.SubmittedApplicationForm };
						ApplicantLogic applicantLogic = new ApplicantLogic();
						applicantLogic.Create(existingViewModel.Applicant);


						//update application no in applied course object
						existingViewModel.AppliedCourse.Person = viewModel.Person;
						existingViewModel.AppliedCourse.ApplicationForm = newApplicationForm;
						AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
						appliedCourseLogic.Modify(existingViewModel.AppliedCourse);


						SponsorLogic sponsorLogic = new SponsorLogic();

						Sponsor sponsor = sponsorLogic.GetModelBy(s => s.Person_Id == viewModel.Person.Id);
						if (sponsor != null)
						{
							sponsor.ApplicationForm = newApplicationForm;
							sponsorLogic.Modify(sponsor);
						}
						else
						{
							existingViewModel.Sponsor.ApplicationForm = newApplicationForm;
							existingViewModel.Sponsor.Person = viewModel.Person;
							sponsorLogic.Create(existingViewModel.Sponsor);
						}


						OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
						OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
						if (existingViewModel.FirstSittingOLevelResult != null && existingViewModel.FirstSittingOLevelResult.ExamNumber != null && existingViewModel.FirstSittingOLevelResult.Type != null && existingViewModel.FirstSittingOLevelResult.ExamYear > 0)
						{
							existingViewModel.FirstSittingOLevelResult.ApplicationForm = newApplicationForm;
							existingViewModel.FirstSittingOLevelResult.Person = viewModel.Person;
							existingViewModel.FirstSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 1 };

							OLevelResult existingFirstSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Person_Id == viewModel.Person.Id && o.O_Level_Exam_Sitting_Id == 1).LastOrDefault();
							
							if (existingFirstSittingOLevelResult != null)
							{
								ResolveOLevelResult(existingFirstSittingOLevelResult);
							}

							OLevelResult firstSittingOLevelResult = oLevelResultLogic.Create(existingViewModel.FirstSittingOLevelResult);

							if (existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0 && firstSittingOLevelResult != null)
							{
								List<OLevelResultDetail> olevelResultDetails = existingViewModel.FirstSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
								foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
								{
									oLevelResultDetail.Header = firstSittingOLevelResult;
								}

								oLevelResultDetailLogic.Create(olevelResultDetails);
							}
						}

						if (existingViewModel.SecondSittingOLevelResult != null && existingViewModel.SecondSittingOLevelResult.ExamNumber != null && existingViewModel.SecondSittingOLevelResult.Type != null && existingViewModel.SecondSittingOLevelResult.ExamYear > 0)
						{
							List<OLevelResultDetail> olevelResultDetails = existingViewModel.SecondSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
							if (olevelResultDetails != null && olevelResultDetails.Count > 0)
							{
								existingViewModel.SecondSittingOLevelResult.ApplicationForm = newApplicationForm;
								existingViewModel.SecondSittingOLevelResult.Person = viewModel.Person;
								existingViewModel.SecondSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 2 };

								OLevelResult existingSecondSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Person_Id == viewModel.Person.Id && o.O_Level_Exam_Sitting_Id == 2).LastOrDefault();

								if (existingSecondSittingOLevelResult != null)
								{
									ResolveOLevelResult(existingSecondSittingOLevelResult);
								}

								OLevelResult secondSittingOLevelResult = oLevelResultLogic.Create(existingViewModel.SecondSittingOLevelResult);

								if (existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0 && secondSittingOLevelResult != null)
								{
									foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
									{
										oLevelResultDetail.Header = secondSittingOLevelResult;
									}

									oLevelResultDetailLogic.Create(olevelResultDetails);
								}
							}
						}

						if (existingViewModel.Programme.Id == 5)
						{
							if (existingViewModel.ApplicantJambDetail != null)
							{
								ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();
								existingViewModel.ApplicantJambDetail.ApplicationForm = newApplicationForm;
								existingViewModel.ApplicantJambDetail.Person = viewModel.Person;
								existingViewModel.ApplicantJambDetail.JambSubject1 = viewModel.ApplicantJambDetail.JambSubject1;
								existingViewModel.ApplicantJambDetail.JambSubject2 = viewModel.ApplicantJambDetail.JambSubject2;
								existingViewModel.ApplicantJambDetail.JambSubject3 = viewModel.ApplicantJambDetail.JambSubject3;
								existingViewModel.ApplicantJambDetail.JambSubject4 = viewModel.ApplicantJambDetail.JambSubject4;
								applicantJambDetailLogic.Modify(existingViewModel.ApplicantJambDetail);

							}
						}
					   
						existingViewModel.AppliedCourse = appliedCourseLogic.GetBy(existingViewModel.AppliedCourse.Person);
						RankingDataLogic rankingDataLogic = new RankingDataLogic();
						RankingData rankingData = new RankingData();
						string rejectReason = rankingDataLogic.RankStudent(existingViewModel.AppliedCourse);

						//if(string.IsNullOrEmpty(rejectReason))
						//{
						//    newApplicationForm.Rejected = false;
						//}
						//else
						//{
						//    newApplicationForm.Rejected = true;
						//    newApplicationForm.RejectReason = rejectReason;

						//    if(!applicationFormLogic.SetRejectReason(newApplicationForm))
						//    {
						//        throw new Exception("Reject Reason not set! Please try again.");
						//    }
						//}
						//existingViewModel.ApplicationForm = newApplicationForm;


						PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
						paymentEtranzactLogic.UpdatePin(existingViewModel.Payment, existingViewModel.Person);


						string junkFilePath;
						string destinationFilePath;
						SetPersonPassportDestination(existingViewModel, out junkFilePath, out destinationFilePath);

						PersonLogic personLogic = new PersonLogic();
						bool personModified = personLogic.Modify(existingViewModel.Person);
						if (personModified)
						{
							SavePersonPassport(junkFilePath, destinationFilePath, existingViewModel.Person);
							transaction.Complete();
							SendSms(existingViewModel.ApplicationForm, existingViewModel.Programme);
						}
						else
						{
							throw new Exception("Passport save operation failed! Please try again.");
						}
					}
				}
				else
				{
					ApplicationFormLogic formLogic = new ApplicationFormLogic();

					existingViewModel.ApplicationForm = formLogic.GetModelsBy(a => a.Application_Form_Number == viewModel.ApplicationForm.Number).LastOrDefault();

					existingViewModel.ApplicationFormNumber = viewModel.ApplicationForm.Number;
					using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
					{
						if (existingViewModel.Programme.Id == 5)
						{
							if (existingViewModel.ApplicantJambDetail != null )
							{
								ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();
								existingViewModel.ApplicantJambDetail.ApplicationForm = newApplicationForm;
								existingViewModel.ApplicantJambDetail.Person = viewModel.Person;
								
								if (viewModel.ApplicantJambDetail.JambSubject1.Id == 0)
								{
									existingViewModel.ApplicantJambDetail.JambSubject1 = new OLevelSubject() { Id = 1 }; 
								}
								else
								{
									existingViewModel.ApplicantJambDetail.JambSubject1 = viewModel.ApplicantJambDetail.JambSubject1;
								}
								existingViewModel.ApplicantJambDetail.JambSubject2 = viewModel.ApplicantJambDetail.JambSubject2;
								existingViewModel.ApplicantJambDetail.JambSubject3 = viewModel.ApplicantJambDetail.JambSubject3;
								existingViewModel.ApplicantJambDetail.JambSubject4 = viewModel.ApplicantJambDetail.JambSubject4;
								applicantJambDetailLogic.Modify(existingViewModel.ApplicantJambDetail);
							}
						}

						SponsorLogic sponsorLogic = new SponsorLogic();

						if (sponsorLogic.GetModelsBy(s => s.Person_Id == viewModel.Person.Id).LastOrDefault() == null)
						{
							Sponsor sponsor = new Sponsor();
							sponsor.ApplicationForm = existingViewModel.ApplicationForm;
							sponsor.ContactAddress = existingViewModel.Sponsor.ContactAddress;
							sponsor.MobilePhone = existingViewModel.Sponsor.MobilePhone;
							sponsor.Name = existingViewModel.Sponsor.Name;
							sponsor.Person = existingViewModel.Person;
							sponsor.Relationship = existingViewModel.Sponsor.Relationship;

							sponsorLogic.Create(sponsor);
						}
						else
						{
							existingViewModel.Sponsor.Person = viewModel.Person;
							sponsorLogic.Modify(existingViewModel.Sponsor);
						}
						
						PersonLogic personLogic = new PersonLogic();
						bool personModified = personLogic.Modify(existingViewModel.Person);

						if (existingViewModel.Programme.Id != 7)
						{

							 //MODIFY O-LEVEL
							existingViewModel.FirstSittingOLevelResult.ApplicationForm = existingViewModel.ApplicationForm;
							existingViewModel.FirstSittingOLevelResult.Person = existingViewModel.Person;
							existingViewModel.FirstSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 1 };

							existingViewModel.SecondSittingOLevelResult.ApplicationForm = existingViewModel.ApplicationForm;
							existingViewModel.SecondSittingOLevelResult.Person = existingViewModel.Person;
							existingViewModel.SecondSittingOLevelResult.Sitting =new OLevelExamSitting(){Id = 2};

							ModifyOlevelResult(existingViewModel.FirstSittingOLevelResult,existingViewModel.FirstSittingOLevelResultDetails);
							ModifyOlevelResult(existingViewModel.SecondSittingOLevelResult,existingViewModel.SecondSittingOLevelResultDetails);
						   
						


							//OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
							//OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
							//if (existingViewModel.FirstSittingOLevelResult != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0 && existingViewModel.FirstSittingOLevelResult.ExamNumber != null && existingViewModel.FirstSittingOLevelResult.Type != null && existingViewModel.FirstSittingOLevelResult.ExamYear > 0)
							//{

							//    oLevelResultLogic.Modify(existingViewModel.FirstSittingOLevelResult);

							//    if (existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0 && existingViewModel.FirstSittingOLevelResult != null)
							//    {
							//        List<OLevelResultDetail> olevelResultDetails = existingViewModel.FirstSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
							//        foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
							//        {
							//            oLevelResultDetail.Header = existingViewModel.FirstSittingOLevelResult;
							//            oLevelResultDetailLogic.Modify(oLevelResultDetail);
							//        }


							//    }
							//}

							//if (existingViewModel.SecondSittingOLevelResult != null && existingViewModel.SecondSittingOLevelResult.ExamNumber != null && existingViewModel.SecondSittingOLevelResult.Type != null && existingViewModel.SecondSittingOLevelResult.ExamYear > 0)
							//{
							//    List<OLevelResultDetail> olevelResultDetails = existingViewModel.SecondSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
							//    if (olevelResultDetails != null && olevelResultDetails.Count > 0)
							//    {
							//        oLevelResultLogic.Modify(existingViewModel.SecondSittingOLevelResult);

							//        if (existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0)
							//        {
							//            foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
							//            {
							//                oLevelResultDetail.Header = existingViewModel.SecondSittingOLevelResult;
							//                oLevelResultDetailLogic.Modify(oLevelResultDetail);
							//            }

							//        }
							//    }
							//}

							AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
							existingViewModel.AppliedCourse = appliedCourseLogic.GetBy(existingViewModel.Person);
							RankingDataLogic rankingDataLogic = new RankingDataLogic();
							RankingData rankingData = new RankingData();
							string rejectReason = rankingDataLogic.RankStudent(existingViewModel.AppliedCourse);

						}

						transaction.Complete();
					}

					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();

					existingViewModel.ApplicationForm = applicationFormLogic.GetModelsBy(a => a.Application_Form_Number == viewModel.ApplicationForm.Number).LastOrDefault();
				}


				TempData["viewModel"] = existingViewModel;
				return RedirectToAction("PostJambSlip", "Form");
			}
			catch (Exception ex)
			{
				newApplicationForm = null;
				viewModel.ApplicationForm = null;
				existingViewModel.ApplicationForm = null;

				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}

			TempData["viewModel"] = existingViewModel;
			return View(existingViewModel);
		}

		private void ResolveOLevelResult(OLevelResult oLevelResult)
		{
			try
			{
				if (oLevelResult != null)
				{
					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					ApplicationForm form = applicationFormLogic.GetModelBy(a => a.Application_Form_Id == oLevelResult.ApplicationForm.Id);
					if (form != null)
					{
						OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
						oLevelResult.Person = form.Person;

						oLevelResultLogic.Modify(oLevelResult);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		public ActionResult PostJAMBSlip(PostJambViewModel viewModel)
		{
			PostJambViewModel existingViewModel = (PostJambViewModel)TempData["viewModel"];

			TempData["viewModel"] = existingViewModel;

			BarcodeLib.Barcode barcode = new BarcodeLib.Barcode(existingViewModel.ApplicationForm.Id.ToString(), TYPE.CODE39);
			Image image = barcode.Encode(TYPE.CODE39, existingViewModel.ApplicationForm.Id.ToString());
			byte[] imageByteData = imageToByteArray(image);
			string imageBase64Data = Convert.ToBase64String(imageByteData);
			string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
			existingViewModel.BarcodeUrl = imageDataURL;

			return View(existingViewModel);
		}
		public ActionResult ReprintPostJAMBSlip(string id)
		{
			PostJambViewModel viewModel = new PostJambViewModel();
			try
			{
				if (!string.IsNullOrEmpty(id))
				{
					long formId = Convert.ToInt64(Utility.Decrypt(id));
					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
					ApplicantLogic applicantLogic = new ApplicantLogic();
					SponsorLogic sponsorLogic = new SponsorLogic();
					OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
					OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
					ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();

					viewModel.ApplicationForm = applicationFormLogic.GetModelBy(a => a.Application_Form_Id == formId);
					if (viewModel.ApplicationForm != null)
					{
						viewModel.AppliedCourse = appliedCourseLogic.GetModelsBy(a => a.Application_Form_Id == formId).LastOrDefault();
						viewModel.ApplicationAlreadyExist = true;
						viewModel.Applicant = applicantLogic.GetModelsBy(a => a.Application_Form_Id == formId).LastOrDefault();
						viewModel.Sponsor = sponsorLogic.GetModelsBy(s => s.Application_Form_Id == formId).LastOrDefault();
						viewModel.FirstSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Application_Form_Id == formId && o.O_Level_Exam_Sitting_Id == (int)ExamSittings.FirstSitting).LastOrDefault();
						if (viewModel.FirstSittingOLevelResult != null)
						{
							viewModel.FirstSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == viewModel.FirstSittingOLevelResult.Id);
						}

						viewModel.SecondSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Application_Form_Id == formId && o.O_Level_Exam_Sitting_Id == (int)ExamSittings.SecondSitting).LastOrDefault();
						if (viewModel.SecondSittingOLevelResult != null)
						{
							viewModel.SecondSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == viewModel.SecondSittingOLevelResult.Id);
						}

						viewModel.ApplicantJambDetail = applicantJambDetailLogic.GetModelsBy(a => a.Application_Form_Id == formId).LastOrDefault();
						viewModel.ApplicationProgrammeFee = viewModel.ApplicationForm.ProgrammeFee;
						viewModel.Session = viewModel.ApplicationForm.Setting.Session;
						viewModel.ApplicationFormSetting = viewModel.ApplicationForm.Setting;
						if (viewModel.AppliedCourse != null)
						{
							viewModel.Programme = viewModel.AppliedCourse.Programme;
							
						}

						viewModel.Person = viewModel.ApplicationForm.Person;
						viewModel.Payment = viewModel.ApplicationForm.Payment;

						BarcodeLib.Barcode barcode = new BarcodeLib.Barcode(viewModel.ApplicationForm.Id.ToString(), TYPE.CODE39);
						Image image = barcode.Encode(TYPE.CODE39, viewModel.ApplicationForm.Id.ToString());
						byte[] imageByteData = imageToByteArray(image);
						string imageBase64Data = Convert.ToBase64String(imageByteData);
						string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
						viewModel.BarcodeUrl = imageDataURL;
					}
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error! " + ex.Message, Message.Category.Error);
			}

			return View("PostJambSlip", viewModel);
		}
		public byte[] imageToByteArray(System.Drawing.Image imageIn)
		{
			MemoryStream ms = new MemoryStream();
			imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
			return ms.ToArray();
		}
		public ActionResult ExamSlip()
		{
			PostJambViewModel existingViewModel = (PostJambViewModel)TempData["viewModel"];

			venueDesignation venue = new venueDesignation();
			VenueDesignationLogic venueLogic = new VenueDesignationLogic();

			ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
			ApplicationForm applicationForm = applicationFormLogic.GetModelBy(f => f.Application_Form_Id == existingViewModel.ApplicationForm.Id);
			if (applicationForm != null)
			{
				existingViewModel.ApplicationForm.ExamNumber = applicationForm.ExamNumber;
				existingViewModel.ApplicationForm.ExamSerialNumber = applicationForm.ExamSerialNumber;
			}

			venue = venueLogic.GetModelsBy(v => v.Department_Id == existingViewModel.AppliedCourse.Department.Id  && v.stop >= existingViewModel.ApplicationForm.ExamSerialNumber).FirstOrDefault();
			existingViewModel.VenueDesignation = venue;

			TempData["viewModel"] = existingViewModel;
			return View(existingViewModel);
		}

		private void SendSms(ApplicationForm applicationForm, Programme programme)
		{
			try
			{
				//send sms to applicant
				Sms textMessage = new Sms();
				textMessage.Sender = "NEKEDE";
				string message = "Hello " + applicationForm.Person.LastName + ", Your application for Admission into the " + programme.ShortName + " programme has been received. Your application no is " + applicationForm.Number + " Thanks";
				string number = applicationForm.Person.MobilePhone;
				string startNumber = number.Substring(0,1);
				if (startNumber == "0")
				{
					number = "234" + number.Substring(1);
				}
				else
				{
					number = number.Substring(0);
				}

				InfoBipSMS smsClient = new InfoBipSMS();
				InfoSMS smsMessage = new InfoSMS();
				smsMessage.from = "NEKEDE";
				smsMessage.to = number;
				smsMessage.text = message;
				smsClient.SendSMS(smsMessage);    
			}
			catch (Exception)
			{
				//do nothing
			}
		}

		private void SavePersonPassport(string junkFilePath, string pathForSaving, Person person)
		{
			try
			{
				string folderPath = Path.GetDirectoryName(pathForSaving);
				string mainFileName = person.Id.ToString() + "__";

				DeleteFileIfExist(folderPath, mainFileName);

				System.IO.File.Move(junkFilePath, pathForSaving);
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void SetPersonPassportDestination(PostJambViewModel existingViewModel, out string junkFilePath, out string destinationFilePath)
		{
			const string TILDA = "~";

			try
			{
				string passportUrl = existingViewModel.Person.ImageFileUrl;
				junkFilePath = Server.MapPath(TILDA + existingViewModel.Person.ImageFileUrl);
				destinationFilePath = junkFilePath.Replace("Junk", "Student");
				existingViewModel.Person.ImageFileUrl = passportUrl.Replace("Junk", "Student");
			}
			catch (Exception)
			{
				throw;
			}
		}

		private Person CreatePerson(PostJAMBFormPaymentViewModel existingViewModel)
		{
			try
			{
				Role role = new Role() { Id = 6 };
				PersonType personType = new PersonType() { Id = existingViewModel.ApplicationFormSetting.PersonType.Id };
				Nationality nationality = new Nationality() { Id = 1 };

				existingViewModel.Person.Role = role;
				existingViewModel.Person.PersonType = personType;
				existingViewModel.Person.Nationality = nationality;
				existingViewModel.Person.DateEntered = DateTime.Now;

				PersonLogic personLogic = new PersonLogic();
				Person person = personLogic.Create(existingViewModel.Person);
				if (person != null && person.Id > 0)
				{
					existingViewModel.Person = person;
				}

				return person;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private Payment CreatePayment(PostJAMBFormPaymentViewModel existingViewModel)
		{
			//const string CHANNEL = "eTransazt";

			try
			{
				SessionLogic sessionLogic = new SessionLogic();
				Payment payment = new Payment();
				PaymentLogic paymentLogic = new PaymentLogic();
				payment.PaymentMode = new PaymentMode() { Id = existingViewModel.ApplicationFormSetting.PaymentMode.Id };
				payment.PaymentType = new PaymentType() { Id = existingViewModel.ApplicationFormSetting.PaymentType.Id };
				payment.PersonType = new PersonType() { Id = existingViewModel.ApplicationFormSetting.PersonType.Id };
				payment.FeeType = new FeeType() { Id = existingViewModel.FeeType.Id };
				payment.DatePaid = DateTime.Now;
				payment.Person = existingViewModel.Person;
				if(existingViewModel != null && existingViewModel.ApplicationFormSetting != null && existingViewModel.ApplicationFormSetting.Session != null)
				{
					payment.Session = existingViewModel.ApplicationFormSetting.Session;
				}
				else
				{
					payment.Session = sessionLogic.GetCurrentSession();
				}
				

				OnlinePayment newOnlinePayment = null;
				Payment newPayment = paymentLogic.Create(payment);
				if (newPayment != null)
				{
					PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
					OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
					OnlinePayment onlinePayment = new OnlinePayment();
					onlinePayment.Channel = channel;
					onlinePayment.Payment = newPayment;
					//onlinePayment.TransactionNumber = newPayment.Id.ToString();

					newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
				}

				return newPayment;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private AppliedCourse CreateAppliedCourse(PostJAMBFormPaymentViewModel viewModel)
		{
			try
			{
				AppliedCourse appliedCourse = new AppliedCourse();
				appliedCourse.Programme = viewModel.Programme;
				appliedCourse.Department = viewModel.AppliedCourse.Department;
				appliedCourse.Person = viewModel.Person;

				AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
				return appliedCourseLogic.Create(appliedCourse);
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void UpdateOLevelResultDetail(PostJambViewModel viewModel)
		{
			try
			{
				if (viewModel != null && viewModel.FirstSittingOLevelResultDetails != null && viewModel.FirstSittingOLevelResultDetails.Count > 0)
				{
					foreach (OLevelResultDetail firstSittingOLevelResultDetail in viewModel.FirstSittingOLevelResultDetails)
					{
						if (firstSittingOLevelResultDetail.Subject != null)
						{
							firstSittingOLevelResultDetail.Subject = viewModel.OLevelSubjects.Where(m => m.Id == firstSittingOLevelResultDetail.Subject.Id).SingleOrDefault();
						   
						}
						if (firstSittingOLevelResultDetail.Grade != null)
						{
							firstSittingOLevelResultDetail.Grade = viewModel.OLevelGrades.Where(m => m.Id == firstSittingOLevelResultDetail.Grade.Id).SingleOrDefault();
						}
					}
				}

				if (viewModel != null && viewModel.SecondSittingOLevelResultDetails != null && viewModel.SecondSittingOLevelResultDetails.Count > 0)
				{
					foreach (OLevelResultDetail secondSittingOLevelResultDetail in viewModel.SecondSittingOLevelResultDetails)
					{
						if (secondSittingOLevelResultDetail.Subject != null)
						{
							secondSittingOLevelResultDetail.Subject = viewModel.OLevelSubjects.Where(m => m.Id == secondSittingOLevelResultDetail.Subject.Id).SingleOrDefault();
						}
						if (secondSittingOLevelResultDetail.Grade != null)
						{
							secondSittingOLevelResultDetail.Grade = viewModel.OLevelGrades.Where(m => m.Id == secondSittingOLevelResultDetail.Grade.Id).SingleOrDefault();
						}
					}
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}
		}
					   
		private void PopulateAllDropDowns()
		{
			PostJambViewModel existingViewModel = (PostJambViewModel)TempData["viewModel"];
			PostJAMBFormPaymentViewModel postJAMBFormPaymentViewModel = (PostJAMBFormPaymentViewModel)TempData["PostJAMBFormPaymentViewModel"];
			
			try
			{
				if (existingViewModel == null)
				{
					viewModel = new PostJambViewModel();

					ViewBag.StateId = viewModel.StateSelectList;
					ViewBag.SexId = viewModel.SexSelectList;
					ViewBag.FirstChoiceFacultyId = viewModel.FacultySelectList;
					ViewBag.SecondChoiceFacultyId = viewModel.FacultySelectList;
					ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), ID, NAME);
					ViewBag.RelationshipId = viewModel.RelationshipSelectList;
					ViewBag.FirstSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
					ViewBag.SecondSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
					ViewBag.FirstSittingExamYearId = viewModel.ExamYearSelectList;
					ViewBag.SecondSittingExamYearId = viewModel.ExamYearSelectList;
					ViewBag.ReligionId = viewModel.ReligionSelectList;
					ViewBag.AbilityId = viewModel.AbilitySelectList;

					ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
					ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
					ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
				   
					if (postJAMBFormPaymentViewModel.Programme.Id == 5)
					{
						ViewBag.JambScoreId = viewModel.JambScoreSelectList;
						ViewBag.InstitutionChoiceId = viewModel.InstitutionChoiceSelectList;
						ViewBag.JambSubject1 = new SelectList(viewModel.JambSubjectSelectList, VALUE, TEXT, 1);
						ViewBag.JambSubject2 = viewModel.JambSubjectSelectList;
						ViewBag.JambSubject3 = viewModel.JambSubjectSelectList;
						ViewBag.JambSubject4 = viewModel.JambSubjectSelectList;
					}
				   

					SetDefaultSelectedSittingSubjectAndGrade(viewModel);
				}
				else
				{
					if (existingViewModel.Person.Religion == null) { existingViewModel.Person.Religion = new Religion(); }
					if (existingViewModel.Person.State == null) { existingViewModel.Person.State = new State(); }
					if (existingViewModel.Person.Sex == null) { existingViewModel.Person.Sex = new Sex(); }
					if (existingViewModel.AppliedCourse.Programme == null) { existingViewModel.AppliedCourse.Programme = new Programme(); }
					if (existingViewModel.Sponsor.Relationship == null) { existingViewModel.Sponsor.Relationship = new Relationship(); }
					if (existingViewModel.FirstSittingOLevelResult.Type == null) { existingViewModel.FirstSittingOLevelResult.Type = new OLevelType(); }
					if (existingViewModel.SecondSittingOLevelResult.Type == null) { existingViewModel.SecondSittingOLevelResult.Type = new OLevelType(); }
					if (existingViewModel.Applicant.Ability == null) { existingViewModel.Applicant.Ability = new Ability(); }
					if (existingViewModel.Person.YearOfBirth == null) { existingViewModel.Person.YearOfBirth = new Value(); }
					if (existingViewModel.Person.MonthOfBirth == null) { existingViewModel.Person.MonthOfBirth = new Value(); }
					if (existingViewModel.Person.DayOfBirth == null) { existingViewModel.Person.DayOfBirth = new Value(); }

					ViewBag.ReligionId = new SelectList(existingViewModel.ReligionSelectList, VALUE, TEXT, existingViewModel.Person.Religion.Id);
					ViewBag.StateId = new SelectList(existingViewModel.StateSelectList, VALUE, TEXT, existingViewModel.Person.State.Id);
					ViewBag.SexId = new SelectList(existingViewModel.SexSelectList, VALUE, TEXT, existingViewModel.Person.Sex.Id);
					ViewBag.ProgrammeId = new SelectList(existingViewModel.FacultySelectList, VALUE, TEXT, existingViewModel.AppliedCourse.Programme.Id);
					ViewBag.RelationshipId = new SelectList(existingViewModel.RelationshipSelectList, VALUE, TEXT, existingViewModel.Sponsor.Relationship.Id);
					ViewBag.FirstSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, existingViewModel.FirstSittingOLevelResult.Type.Id);
					ViewBag.FirstSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, VALUE, TEXT, existingViewModel.FirstSittingOLevelResult.ExamYear);
					ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, existingViewModel.SecondSittingOLevelResult.Type.Id);
					ViewBag.SecondSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, VALUE, TEXT, existingViewModel.SecondSittingOLevelResult.ExamYear);
					ViewBag.AbilityId = new SelectList(existingViewModel.AbilitySelectList, VALUE, TEXT, existingViewModel.Applicant.Ability.Id);

					SetDateOfBirthDropDown(existingViewModel);
					if (postJAMBFormPaymentViewModel.Programme.Id == 5)
					{
						ViewBag.InstitutionChoiceId = new SelectList(existingViewModel.InstitutionChoiceSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.InstitutionChoice.Id);
						ViewBag.JambScoreId = new SelectList(existingViewModel.JambScoreSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambScore);
						if (existingViewModel.ApplicantJambDetail.JambSubject1.Id == 0)
						{
							ViewBag.JambSubject1 = new SelectList(existingViewModel.JambSubjectSelectList, VALUE, TEXT, 1);
						}
						else
						{
							ViewBag.JambSubject1 = new SelectList(existingViewModel.JambSubjectSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambSubject1.Id);
						}
						
						ViewBag.JambSubject2 = new SelectList(existingViewModel.JambSubjectSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambSubject2.Id);
						ViewBag.JambSubject3 = new SelectList(existingViewModel.JambSubjectSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambSubject3.Id);
						ViewBag.JambSubject4 = new SelectList(existingViewModel.JambSubjectSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambSubject4.Id);
					}

					if (existingViewModel.Person.LocalGovernment != null && existingViewModel.Person.LocalGovernment.Id > 0)
					{
						ViewBag.LgaId = new SelectList(existingViewModel.LocalGovernmentSelectList, VALUE, TEXT, existingViewModel.Person.LocalGovernment.Id);
					}
					else
					{
						ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), VALUE, TEXT);
					}

					if (existingViewModel.SecondSittingOLevelResult.Type != null)
					{
						ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, existingViewModel.SecondSittingOLevelResult.Type.Id);
					}
					else
					{
						ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, 0);
					}

					SetSelectedSittingSubjectAndGrade(existingViewModel);
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}

		private void SetDateOfBirthDropDown(PostJambViewModel existingViewModel)
		{
			try
			{
				ViewBag.MonthOfBirthId = new SelectList(existingViewModel.MonthOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.MonthOfBirth.Id);
				ViewBag.YearOfBirthId = new SelectList(existingViewModel.YearOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.YearOfBirth.Id);
				if ((existingViewModel.DayOfBirthSelectList == null || existingViewModel.DayOfBirthSelectList.Count == 0) && (existingViewModel.Person.MonthOfBirth.Id > 0 && existingViewModel.Person.YearOfBirth.Id > 0))
				{
					existingViewModel.DayOfBirthSelectList = Utility.PopulateDaySelectListItem(existingViewModel.Person.MonthOfBirth, existingViewModel.Person.YearOfBirth);
				}
				else
				{
					if (existingViewModel.DayOfBirthSelectList != null && existingViewModel.Person.DayOfBirth.Id > 0)
					{
						ViewBag.DayOfBirthId = new SelectList(existingViewModel.DayOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.DayOfBirth.Id);
					}
					else if (existingViewModel.DayOfBirthSelectList != null && existingViewModel.Person.DayOfBirth.Id <= 0)
					{
						ViewBag.DayOfBirthId = existingViewModel.DayOfBirthSelectList;
					}
					else if (existingViewModel.DayOfBirthSelectList == null)
					{
						existingViewModel.DayOfBirthSelectList = new List<SelectListItem>();
						ViewBag.DayOfBirthId = new List<SelectListItem>();
					}
				}

				if (existingViewModel.Person.DayOfBirth != null && existingViewModel.Person.DayOfBirth.Id > 0)
				{
					ViewBag.DayOfBirthId = new SelectList(existingViewModel.DayOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.DayOfBirth.Id);
				}
				else
				{
					ViewBag.DayOfBirthId = existingViewModel.DayOfBirthSelectList;
				}
			}
			catch (Exception)
			{
				throw;
			}
		}
		
		private void SetPreviousEducationStartDateDropDowns(PostJambViewModel existingViewModel)
		{
			try
			{
				ViewBag.PreviousEducationStartMonthId = new SelectList(existingViewModel.PreviousEducationStartMonthSelectList, VALUE, TEXT, existingViewModel.PreviousEducation.StartMonth.Id);
				ViewBag.PreviousEducationStartYearId = new SelectList(existingViewModel.PreviousEducationStartYearSelectList, VALUE, TEXT, existingViewModel.PreviousEducation.StartYear.Id);
				if ((existingViewModel.PreviousEducationStartDaySelectList == null || existingViewModel.PreviousEducationStartDaySelectList.Count == 0) && (existingViewModel.PreviousEducation.StartMonth.Id > 0 && existingViewModel.PreviousEducation.StartYear.Id > 0))
				{
					existingViewModel.PreviousEducationStartDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.PreviousEducation.StartMonth, existingViewModel.PreviousEducation.StartYear);
				}
				else
				{
					if (existingViewModel.PreviousEducationStartDaySelectList != null && existingViewModel.PreviousEducation.StartDay.Id > 0)
					{
						ViewBag.PreviousEducationStartDayId = new SelectList(existingViewModel.PreviousEducationStartDaySelectList, VALUE, TEXT, existingViewModel.PreviousEducation.StartDay.Id);
					}
					else if (existingViewModel.PreviousEducationStartDaySelectList != null && existingViewModel.PreviousEducation.StartDay.Id <= 0)
					{
						ViewBag.PreviousEducationStartDayId = existingViewModel.PreviousEducationStartDaySelectList;
					}
					else if (existingViewModel.PreviousEducationStartDaySelectList == null)
					{
						existingViewModel.PreviousEducationStartDaySelectList = new List<SelectListItem>();
						ViewBag.PreviousEducationStartDayId = new List<SelectListItem>();
					}
				}

				if (existingViewModel.PreviousEducation.StartDay != null && existingViewModel.PreviousEducation.StartDay.Id > 0)
				{
					ViewBag.PreviousEducationStartDayId = new SelectList(existingViewModel.PreviousEducationStartDaySelectList, VALUE, TEXT, existingViewModel.PreviousEducation.StartDay.Id);
				}
				else
				{
					ViewBag.PreviousEducationStartDayId = existingViewModel.PreviousEducationStartDaySelectList;
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void SetPreviousEducationEndDateDropDowns(PostJambViewModel existingViewModel)
		{
			try
			{
				ViewBag.PreviousEducationEndMonthId = new SelectList(existingViewModel.PreviousEducationEndMonthSelectList, VALUE, TEXT, existingViewModel.PreviousEducation.EndMonth.Id);
				ViewBag.PreviousEducationEndYearId = new SelectList(existingViewModel.PreviousEducationEndYearSelectList, VALUE, TEXT, existingViewModel.PreviousEducation.EndYear.Id);
				if ((existingViewModel.PreviousEducationEndDaySelectList == null || existingViewModel.PreviousEducationEndDaySelectList.Count == 0) && (existingViewModel.PreviousEducation.EndMonth.Id > 0 && existingViewModel.PreviousEducation.EndYear.Id > 0))
				{
					existingViewModel.PreviousEducationEndDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.PreviousEducation.EndMonth, existingViewModel.PreviousEducation.EndYear);
				}
				else
				{
					if (existingViewModel.PreviousEducationEndDaySelectList != null && existingViewModel.PreviousEducation.EndDay.Id > 0)
					{
						ViewBag.PreviousEducationEndDayId = new SelectList(existingViewModel.PreviousEducationEndDaySelectList, VALUE, TEXT, existingViewModel.PreviousEducation.EndDay.Id);
					}
					else if (existingViewModel.PreviousEducationEndDaySelectList != null && existingViewModel.PreviousEducation.EndDay.Id <= 0)
					{
						ViewBag.PreviousEducationEndDayId = existingViewModel.PreviousEducationEndDaySelectList;
					}
					else if (existingViewModel.PreviousEducationEndDaySelectList == null)
					{
						existingViewModel.PreviousEducationEndDaySelectList = new List<SelectListItem>();
						ViewBag.PreviousEducationEndDayId = new List<SelectListItem>();
					}
				}

				if (existingViewModel.PreviousEducation.EndDay != null && existingViewModel.PreviousEducation.EndDay.Id > 0)
				{
					ViewBag.PreviousEducationEndDayId = new SelectList(existingViewModel.PreviousEducationEndDaySelectList, VALUE, TEXT, existingViewModel.PreviousEducation.EndDay.Id);
				}
				else
				{
					ViewBag.PreviousEducationEndDayId = existingViewModel.PreviousEducationEndDaySelectList;
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void SetDefaultSelectedSittingSubjectAndGrade(PostJambViewModel viewModel)
		{
			try
			{
				if (viewModel != null && viewModel.FirstSittingOLevelResultDetails != null)
				{
					for (int i = 0; i < 9; i++ )
					{
						ViewData["FirstSittingOLevelSubjectId" + i] = viewModel.OLevelSubjectSelectList;
						ViewData["FirstSittingOLevelGradeId" + i] = viewModel.OLevelGradeSelectList;
					}
				}

				if (viewModel != null && viewModel.SecondSittingOLevelResultDetails != null)
				{
					for (int i = 0; i < 9; i++)
					{
						ViewData["SecondSittingOLevelSubjectId" + i] = viewModel.OLevelSubjectSelectList;
						ViewData["SecondSittingOLevelGradeId" + i] = viewModel.OLevelGradeSelectList;
					}
				}
			}
			catch (Exception ex)
			{
				SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
			}
		}
	   
		private void SetSelectedSittingSubjectAndGrade(PostJambViewModel existingViewModel)
		{
			try
			{
				if (existingViewModel != null && existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0 )
				{
					int i = 0;
					foreach (OLevelResultDetail firstSittingOLevelResultDetail in existingViewModel.FirstSittingOLevelResultDetails )
					{
						if (firstSittingOLevelResultDetail.Subject != null && firstSittingOLevelResultDetail.Grade != null)
						{
							ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Subject.Id);
							ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Grade.Id);
						}
						else
						{
							ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
							ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
						}

						i++;
					}
				}

				if (existingViewModel != null && existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0)
				{
					int i = 0;
					foreach (OLevelResultDetail secondSittingOLevelResultDetail in existingViewModel.SecondSittingOLevelResultDetails)
					{
						if (secondSittingOLevelResultDetail.Subject != null && secondSittingOLevelResultDetail.Grade != null)
						{
							ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Subject.Id);
							ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Grade.Id);
						}
						else
						{
							ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
							ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
						}

						i++;
					}
				}
			}
			catch (Exception ex)
			{
				//SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
				SetMessage("Error Occurred! " + ex.Message + " --- " + ex.InnerException.Message, Message.Category.Error);
			}
		}

		public JsonResult GetLocalGovernmentsByState(string id)
		{
			try
			{
				LocalGovernmentLogic lgaLogic = new LocalGovernmentLogic();

				Expression<Func<LOCAL_GOVERNMENT, bool>> selector = l => l.State_Id == id;
				List<LocalGovernment> lgas = lgaLogic.GetModelsBy(selector);

				return Json(new SelectList(lgas, "Id", "Name"), JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public JsonResult GetDayOfBirthBy(string monthId, string yearId)
		{
			try
			{
				if (string.IsNullOrEmpty(monthId) || string.IsNullOrEmpty(yearId))
				{
					return null;
				}

				Value month = new Value() { Id = Convert.ToInt32(monthId) };
				Value year = new Value() { Id = Convert.ToInt32(yearId) };
				List<Value> days = Utility.GetNumberOfDaysInMonth(month, year);

				return Json(new SelectList(days, ID, NAME), JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public JsonResult GetDepartmentByProgrammeId(string id)
		{
			try
			{
				if (string.IsNullOrEmpty(id))
				{
					return null;
				}

				Programme programme = new Programme() { Id = Convert.ToInt32(id) };

				DepartmentLogic departmentLogic = new DepartmentLogic();
				List<Department> departments = departmentLogic.GetBy(programme);

				//if (programme.Id == 2)
				//{
				//    Department mechanicalEngineering = departments.LastOrDefault(m => m.Id == 15);
				//    Department chemicalEngineering = departments.LastOrDefault(m => m.Id == 31);

				//    departments.Remove(mechanicalEngineering);
				//    departments.Remove(chemicalEngineering);
				//} 

				return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet); 
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		public JsonResult GetDepartmentOptionByDepartment(string id, string programmeid)
		{
			try
			{
				if (string.IsNullOrEmpty(id))
				{
					return null;
				}

				Department department = new Department() { Id = Convert.ToInt32(id) };
				Programme programme = new Programme() { Id = Convert.ToInt32(programmeid) };
				DepartmentOptionLogic departmentLogic = new DepartmentOptionLogic();
				List<DepartmentOption> departmentOptions = departmentLogic.GetBy(department, programme);

				return Json(new SelectList(departmentOptions, ID, NAME), JsonRequestBehavior.AllowGet);

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		[HttpPost]
		public virtual ActionResult UploadFile(FormCollection form)
		{
			HttpPostedFileBase file = Request.Files["MyFile"];

			bool isUploaded = false;
			string personId = form["Person.Id"].ToString();
			string passportUrl = form["Person.ImageFileUrl"].ToString();
			string message = "File upload failed";

			string path = null;
			string imageUrl = null;
			string imageUrlDisplay = null;

			try
			{
				if (file != null && file.ContentLength != 0)
				{
					FileInfo fileInfo = new FileInfo(file.FileName);
					string fileExtension = fileInfo.Extension;
					string newFile = personId + "__";
					string newFileName = newFile + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + fileExtension;

					string invalidFileMessage = InvalidFile(file.ContentLength, fileExtension);
					if (!string.IsNullOrEmpty(invalidFileMessage))
					{
						isUploaded = false;
						TempData["imageUrl"] = null;
						return Json(new { isUploaded = isUploaded, message = invalidFileMessage, imageUrl = passportUrl }, "text/html", JsonRequestBehavior.AllowGet);
					}

					string pathForSaving = Server.MapPath("~/Content/Junk");
					if (this.CreateFolderIfNeeded(pathForSaving))
					{
						DeleteFileIfExist(pathForSaving, personId);

						file.SaveAs(Path.Combine(pathForSaving, newFileName));

						isUploaded = true;
						message = "File uploaded successfully!";

						path = Path.Combine(pathForSaving, newFileName);
						if (path != null)
						{
							string appRoot = ConfigurationManager.AppSettings["AppRoot"];
							imageUrl = "/Content/Junk/" + newFileName;
							imageUrlDisplay = appRoot + imageUrl + "?t=" + DateTime.Now;

							TempData["imageUrl"] = imageUrl;
						}
					}
				}
			}
			catch (Exception ex)
			{
				message = string.Format("File upload failed: {0}", ex.Message);
			}

			return Json(new { isUploaded = isUploaded, message = message, imageUrl = imageUrlDisplay }, "text/html", JsonRequestBehavior.AllowGet);
		}

		private string InvalidFile(decimal uploadedFileSize, string fileExtension)
		{
			try
			{
				string message = null;
				decimal oneKiloByte = 1024;
				decimal maximumFileSize = 50 * oneKiloByte;
				
				decimal actualFileSizeToUpload = Math.Round(uploadedFileSize/oneKiloByte, 1);
				if (InvalidFileType(fileExtension))
				{
					message = "File type '" + fileExtension + "' is invalid! File type must be any of the following: .jpg, .jpeg, .png or .jif ";
				}
				else if (actualFileSizeToUpload > (maximumFileSize / oneKiloByte))
				{
					message = "Your file size of " + actualFileSizeToUpload.ToString("0.#") + " Kb is too large, maximum allowed size is " + (maximumFileSize / oneKiloByte) + " Kb";
				}

				return message;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool InvalidFileType(string extension)
		{
			extension = extension.ToLower();
			switch (extension)
			{
				case ".jpg":
					return false;
				case ".png":
					return false;
				case ".gif":
					return false;
				case ".jpeg":
					return false;
				default:
					return true;
			}
		}
	   
		private void DeleteFileIfExist(string folderPath, string fileName)
		{
			try
			{
				string wildCard = fileName + "*.*";
				IEnumerable<string> files = Directory.EnumerateFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

				if (files != null && files.Count() > 0)
				{
					foreach (string file in files)
					{
						System.IO.File.Delete(file);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		private bool CreateFolderIfNeeded(string path)
		{
			try
			{
				bool result = true;
				if (!Directory.Exists(path))
				{
					try
					{
						Directory.CreateDirectory(path);
					}
					catch (Exception)
					{
						/*TODO: You must process this exception.*/
						result = false;
					}
				}

				return result;
			}
			catch (Exception)
			{
				throw;
			}
		}

		private void ModifyOlevelResult(OLevelResult oLevelResult, List<OLevelResultDetail> oLevelResultDetails)
		{
			try
			{
				OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
				if (oLevelResult != null && oLevelResult.ExamNumber != null && oLevelResult.Type != null && oLevelResult.ExamYear > 0)
				{
					if (oLevelResult != null && oLevelResult.Id > 0)
					{
						oLevelResultDetailLogic.DeleteBy(oLevelResult);
						OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
						oLevelResultLogic.Modify(oLevelResult);
						
					}
					else
					{
						OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
						OLevelResult newOLevelResult = oLevelResultLogic.Create(oLevelResult);
						oLevelResult.Id = newOLevelResult.Id;
					}

					if (oLevelResultDetails != null && oLevelResultDetails.Count > 0)
					{
						List<OLevelResultDetail> olevelResultDetails = oLevelResultDetails.Where(m => m.Grade != null && m.Grade.Id > 0 && m.Subject != null && m.Subject.Id > 0).ToList();
						foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
						{
							oLevelResultDetail.Header = oLevelResult;
							oLevelResultDetailLogic.Create(oLevelResultDetail);
						}

						//oLevelResultDetailLogic.Create(olevelResultDetails);
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}

		public ActionResult RegenerateRemitaPayment()
		{
			try
			{
				PaymentLogic paymentLogic = new PaymentLogic();
				FeeDetailLogic feeDetailLogic = new FeeDetailLogic();

				List<Payment> payments = paymentLogic.GetMissingRemitaPayments();

				for (int i = 0; i < payments.Count; i++)
				{
					Payment payment = payments[i];

					payment.FeeDetails = feeDetailLogic.GetModelsBy(f => f.Fee_Type_Id == payment.FeeType.Id && f.Session_Id == payment.Session.Id);

					decimal Amt = 0;
					Amt = payment.FeeDetails.Sum(p => p.Fee.Amount);
					RemitaSettings settings = new RemitaSettings();
					RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
					settings = settingsLogic.GetBy(2);

					//Get Split Specific details;
					List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
					RemitaSplitItems singleItem = new RemitaSplitItems();
					RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();
					singleItem = splitItemLogic.GetBy(6);
					singleItem.deductFeeFrom = "1";
					splitItems.Add(singleItem);
					singleItem = splitItemLogic.GetBy(5);
					singleItem.deductFeeFrom = "0";
					singleItem.beneficiaryAmount = Convert.ToString(Amt - Convert.ToDecimal(splitItems[0].beneficiaryAmount));
					splitItems.Add(singleItem);

					//Get BaseURL
					string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
					Amt = payment.FeeDetails.Sum(a => a.Fee.Amount);

					RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
					RemitaPayment remitaPayment = remitaProcessor.GenerateRRR(payment.InvoiceNumber, remitaBaseUrl, "SCREENING FORM", splitItems, settings, Amt);
				}

				SetMessage("Operation Successful! ", Message.Category.Information);
			}
			catch (Exception ex)
			{
				SetMessage("Error! ", Message.Category.Error);
			}

			return View();
		}
		public ActionResult UnregisteredPrintInvoice(string fid)
		{
			PostJAMBFormPaymentViewModel paymentViewModel = new PostJAMBFormPaymentViewModel();
			try
			{
				long formId = Convert.ToInt64(Utility.Decrypt(fid));
				if (formId > 0)
				{
					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
					ApplicationForm applicationForm = applicationFormLogic.GetBy(formId);
					if (applicationForm != null)
					{
						AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
						AppliedCourse appliedCourse = appliedCourseLogic.GetModelBy(a => a.Application_Form_Id == applicationForm.Id);

						paymentViewModel.ApplicationFormSetting = applicationForm.Setting;
						paymentViewModel.AppliedCourse = appliedCourse;
						paymentViewModel.Payment = applicationForm.Payment;
						paymentViewModel.Person = applicationForm.Person;
						paymentViewModel.Department = appliedCourse.Department;
						paymentViewModel.Programme = appliedCourse.Programme;
						paymentViewModel.ApplicationProgrammeFee = applicationForm.ProgrammeFee;
						paymentViewModel.PaymentEtranzactType = paymentViewModel.GetPaymentTypeBy(applicationForm.Payment.FeeType, applicationForm.Payment.Session);
						//viewModel.FeeType = applicationForm.Payment.FeeType;
						//viewModel.PaymentType = applicationForm.Setting.PaymentType;
						if (appliedCourse.Programme.Id == (int)Programmes.NDMorning)
						{
							paymentViewModel.remitaPayment = remitaPaymentLogic.GetModelBy(r => r.Payment_Id == applicationForm.Payment.Id);
						}
					}
				}
			}
			catch (Exception)
			{
				throw;
			}

			TempData["PostJAMBFormPaymentViewModel"] = paymentViewModel;
			return RedirectToAction("Invoice", "Form");
		}

		public ActionResult ReprintPostJAMBSlipByForm()
		{
			try
			{
				viewModel = new PostJambViewModel();
			}
			catch (Exception ex)
			{
				SetMessage("Error! " + ex.Message, Message.Category.Error);
			}

			return View();
		}
		[HttpPost]
		public ActionResult ReprintPostJAMBSlipByForm(PostJambViewModel viewModel)
		{
			try
			{
					ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
					AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
					ApplicantLogic applicantLogic = new ApplicantLogic();
					SponsorLogic sponsorLogic = new SponsorLogic();
					OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
					OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
					ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();

					viewModel.ApplicationForm = applicationFormLogic.GetModelBy(a => a.Application_Form_Number == viewModel.ApplicationForm.Number.Trim());
					if (viewModel.ApplicationForm != null)
					{
						viewModel.AppliedCourse = appliedCourseLogic.GetModelsBy(a => a.Application_Form_Id == viewModel.ApplicationForm.Id).LastOrDefault();
						viewModel.ApplicationAlreadyExist = true;
						viewModel.Applicant = applicantLogic.GetModelsBy(a => a.Application_Form_Id == viewModel.ApplicationForm.Id).LastOrDefault();
						viewModel.Sponsor = sponsorLogic.GetModelsBy(s => s.Application_Form_Id == viewModel.ApplicationForm.Id).LastOrDefault();
						viewModel.FirstSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Application_Form_Id == viewModel.ApplicationForm.Id && o.O_Level_Exam_Sitting_Id == (int)ExamSittings.FirstSitting).LastOrDefault();
						if (viewModel.FirstSittingOLevelResult != null)
						{
							viewModel.FirstSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == viewModel.FirstSittingOLevelResult.Id);
						}

						viewModel.SecondSittingOLevelResult = oLevelResultLogic.GetModelsBy(o => o.Application_Form_Id == viewModel.ApplicationForm.Id && o.O_Level_Exam_Sitting_Id == (int)ExamSittings.SecondSitting).LastOrDefault();
						if (viewModel.SecondSittingOLevelResult != null)
						{
							viewModel.SecondSittingOLevelResultDetails = oLevelResultDetailLogic.GetModelsBy(o => o.Applicant_O_Level_Result_Id == viewModel.SecondSittingOLevelResult.Id);
						}

						viewModel.ApplicantJambDetail = applicantJambDetailLogic.GetModelsBy(a => a.Application_Form_Id == viewModel.ApplicationForm.Id).LastOrDefault();
						viewModel.ApplicationProgrammeFee = viewModel.ApplicationForm.ProgrammeFee;
						viewModel.Session = viewModel.ApplicationForm.Setting.Session;
						viewModel.ApplicationFormSetting = viewModel.ApplicationForm.Setting;
						if (viewModel.AppliedCourse != null)
						{
							viewModel.Programme = viewModel.AppliedCourse.Programme;

						}

						viewModel.Person = viewModel.ApplicationForm.Person;
						viewModel.Payment = viewModel.ApplicationForm.Payment;

						BarcodeLib.Barcode barcode = new BarcodeLib.Barcode(viewModel.ApplicationForm.Id.ToString(), TYPE.CODE39);
						Image image = barcode.Encode(TYPE.CODE39, viewModel.ApplicationForm.Id.ToString());
						byte[] imageByteData = imageToByteArray(image);
						string imageBase64Data = Convert.ToBase64String(imageByteData);
						string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
						viewModel.BarcodeUrl = imageDataURL;
					}
			}
			catch (Exception ex)
			{
				SetMessage("Error! " + ex.Message, Message.Category.Error);
			}

			return View("PostJambSlip", viewModel);
		}
	}
}