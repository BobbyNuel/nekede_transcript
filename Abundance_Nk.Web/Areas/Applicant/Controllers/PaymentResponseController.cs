﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;
using System.Configuration;


namespace Abundance_Nk.Web.Areas.Admin.Controllers
{
    public class PaymentResponseController : Controller
    {
        // GET: Admin/PaymentResponse
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string orderID)
        {

            RemitaSettings settings = new RemitaSettings();
            RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
            settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 1);
            RemitaResponse remitaResponse = new RemitaResponse();
            RemitaPayment remitaPayment = new RemitaPayment();
            RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
            remitaPayment = remitaPaymentLogic.GetModelBy(m => m.OrderId == orderID);
            string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
            RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
            remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
            remitaPayment.Status = remitaResponse.StatusCode + ":" + remitaResponse.Status;
            remitaPaymentLogic.Modify(remitaPayment);
            return View(remitaResponse);

        }
    }
}