﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using Abundance_Nk.Web.Models;
using System.Transactions;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using System.Net;
using Abundance_Nk.Model.Entity;

namespace Abundance_Nk.Web.Areas.Applicant.Controllers
{
    public class OldPortalController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string VALUE = "Value";
        private const string TEXT = "Text";
        public OldPortalDataViewModel OldPortalDataViewModel;
        // GET: Applicant/OldPortal
        public ActionResult Index()
        {
            OldPortalDataViewModel = new OldPortalDataViewModel();
            return View (OldPortalDataViewModel);
        }
    
        [HttpPost]
        public ActionResult Index(OldPortalDataViewModel vModel)
        {
            try
            {
                if(vModel.OldPortalData.Pin != null)
                {
                    OldPortalDataLogic portalDataLogic = new OldPortalDataLogic ();
                    vModel.OldPortalData = portalDataLogic.GetModelsBy (a => a.Pin_Used == vModel.OldPortalData.Pin).FirstOrDefault ();
                }
            }
            catch(Exception ex)
            {
                SetMessage ("Error Occurred" + ex.Message, Message.Category.Error);
                throw;
            }
           
            return View(vModel);
        }

        public ActionResult AddData()
        {
            OldPortalDataViewModel = new OldPortalDataViewModel ();
            try
            {
                ViewBag.StateId = OldPortalDataViewModel.StateSelectList;
                ViewBag.ProgrammeId = OldPortalDataViewModel.ProgrammeSelectListItem;
                ViewBag.DepartmentId = new SelectList (new List<Department> (), ID, NAME);
                ViewBag.LGAId = new SelectList (new List<LocalGovernment> (), ID, NAME);
            }
            catch (Exception ex)
            {
                SetMessage ("Error Occurred" + ex.Message, Message.Category.Error);      
                throw;
            }
            return View(OldPortalDataViewModel);
        }
      
        [HttpPost]
        public ActionResult AddData(OldPortalDataViewModel vModel)
        {
            try
            {
                OldPortalDataLogic portalDataLogic = new OldPortalDataLogic ();
                UserLogic loggeduser = new UserLogic ();
                vModel.OldPortalData.User = loggeduser.GetModelBy (u => u.User_Name == User.Identity.Name);
                vModel.OldPortalData.DateAdded = DateTime.Now;
                portalDataLogic.Create (vModel.OldPortalData);

                vModel = new OldPortalDataViewModel ();
                ViewBag.StateId = vModel.StateSelectList;
                ViewBag.ProgrammeId = vModel.ProgrammeSelectListItem;
                ViewBag.DepartmentId = new SelectList (new List<Department> (), ID, NAME);
                ViewBag.LGAId = new SelectList (new List<LocalGovernment> (), ID, NAME);
                SetMessage ("Student added Successfully!", Message.Category.Information);
            }
            //catch (Exception ex)
            //{
            //    SetMessage ("Error Occurred" + ex.Message, Message.Category.Error);    
            //    throw;
            //}
            catch(DbEntityValidationException dbEx)
            {
                foreach(var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach(var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation ("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            return View (vModel);
        }
        public ActionResult AcceptanceLetter(string id)
        {
            OldPortalData acceptanceData = new OldPortalData ();
            try
            {
                long Id = Convert.ToInt64 (Utility.Decrypt (id));
                OldPortalDataLogic acceptanceDataLogic = new OldPortalDataLogic ();
                acceptanceData = acceptanceDataLogic.GetModelBy (a => a.Id == Id);
            }
            catch(Exception ex)
            {
                SetMessage ("Error Occurred" + ex.Message, Message.Category.Error);
                throw;
            }
            
            return View(acceptanceData);
        }
        public ActionResult AdmissionLetter (string id)
        {

            OldPortalData acceptanceData = new OldPortalData ();
            try
            {
                long Id = Convert.ToInt64 (Utility.Decrypt (id));
                OldPortalDataLogic acceptanceDataLogic = new OldPortalDataLogic ();
                acceptanceData = acceptanceDataLogic.GetModelBy (a => a.Id == Id);
            }
            catch(Exception ex)
            {
                SetMessage ("Error Occurred" + ex.Message, Message.Category.Error);
                throw;
            }
            return View (acceptanceData);
        }

        public JsonResult GetLocalGovernmentsByState (string id)
        {
            try
            {
                LocalGovernmentLogic lgaLogic = new LocalGovernmentLogic ();

                Expression<Func<LOCAL_GOVERNMENT, bool>> selector = l => l.State_Id == id;
                List<LocalGovernment> lgas = lgaLogic.GetModelsBy (selector);

                return Json (new SelectList (lgas, "Id", "Name"), JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetDepartmentByProgrammeId (string id)
        {
            try
            {
                if(string.IsNullOrEmpty (id))
                {
                    return null;
                }

                Programme programme = new Programme () { Id = Convert.ToInt32 (id) };
                DepartmentLogic departmentLogic = new DepartmentLogic ();
                List<Department> departments = departmentLogic.GetBy (programme);

                return Json (new SelectList (departments, ID, NAME), JsonRequestBehavior.AllowGet);

            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

      
    }
}