﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using System.Linq.Expressions;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;
using System.Web.Routing;
using System.IO;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Applicant.Controllers
{
    [AllowAnonymous]
    public class PostUtmeResultController : BaseController
    {
        private Abundance_Nk_NekedeEntities db = new Abundance_Nk_NekedeEntities();

        // GET: /Student/PostUtmeResult/
        public ActionResult Index()
        {
            PostUtmeResultViewModel viewModel = new PostUtmeResultViewModel();
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(PostUtmeResultViewModel viewModel)
        {
            try
            {
            if (viewModel.JambRegistrationNumber != null && viewModel.PinNumber != null)
            {
                PaymentEtranzact payment = new PaymentEtranzact();
                PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();
                PaymentTerminal terminalid = new PaymentTerminal();
                PaymentTerminalLogic terminalLogic = new PaymentTerminalLogic();
                terminalid = terminalLogic.GetModelBy(a => a.Fee_Type_Id == 5);
                NdPutmeResult result = new NdPutmeResult();
                NdPutmeResultLogic PostUtmeResultLogic = new NdPutmeResultLogic();

                ApplicationForm appForm = new ApplicationForm();
                ApplicationFormLogic appFormLogic = new ApplicationFormLogic();
                ApplicantJambDetail jambDetail = new ApplicantJambDetail();
                ApplicantJambDetailLogic jambDetailLogic = new ApplicantJambDetailLogic();

                jambDetail = jambDetailLogic.GetModelsBy(j => j.Applicant_Jamb_Registration_Number == viewModel.JambRegistrationNumber && j.APPLICATION_FORM.Application_Form_Id != null).FirstOrDefault();
                if (jambDetail == null)
                {
                    jambDetail = jambDetailLogic.GetModelsBy(j => j.Applicant_Jamb_Registration_Number == viewModel.JambRegistrationNumber).FirstOrDefault();
                    SetMessage("Unable to find Jamb Number, Please Check and try again! ", Message.Category.Error);
                    //return View(viewModel);
                }
                if (jambDetail != null)
                {
                    appForm = appFormLogic.GetModelBy(a => a.Person_Id == jambDetail.Person.Id);
                    if (appForm == null)
                    {
                        SetMessage("You may not have filled the application form, Please check and try again.", Message.Category.Error);
                        return View(viewModel);
                    }
                    payment = paymentEtranzactLogic.GetModelsBy(p => p.Confirmation_No == viewModel.PinNumber).FirstOrDefault();
                    if (payment == null)
                    {
                        payment = paymentEtranzactLogic.RetrievePinsWithoutInvoice(viewModel.PinNumber, appForm, 5, terminalid);
                        if (payment != null && payment.Payment != null)
                        {
                            decimal Amount = 2700;
                            if (paymentEtranzactLogic.ValidatePin(payment, payment.Payment.Payment, Amount))
                            {
                                result = PostUtmeResultLogic.GetModelsBy(m => m.REGNO == viewModel.JambRegistrationNumber).FirstOrDefault();
                                if (result == null || result.Id <= 0)
                                {
                                    SetMessage("Registration Number / Jamb No was not found! Please check that you have typed in the correct detail", Message.Category.Error);
                                    return View(viewModel);
                                }
                                else
                                {
                                    paymentEtranzactLogic.UpdatePin(payment.Payment.Payment, appForm.Person);
                                    viewModel.Result = result;
                                    viewModel.jambDetail = jambDetail;
                                    viewModel.ApplicationDetail = appForm;
                                    TempData["PostUtmeResultViewModel"] = viewModel;
                                    return RedirectToAction("PostUtmeResultSlip");

                                }


                            }
                        }
                        else
                        {
                            SetMessage("Pin is not valid. Please check that you have typed in the correct detail", Message.Category.Error);
                            return View(viewModel);
                        }

                    }
                    else
                    {
                        decimal Amount = 2700;
                        if (paymentEtranzactLogic.ValidatePin(payment, payment.Payment.Payment, Amount))
                        {
                            if (!paymentEtranzactLogic.IsPinUsed(viewModel.PinNumber, (int)appForm.Person.Id))
                            {
                                result = PostUtmeResultLogic.GetModelsBy(m => m.REGNO == viewModel.JambRegistrationNumber).FirstOrDefault();
                                if (result == null || result.Id <= 0)
                                {
                                    SetMessage("Registration Number / Jamb No was not found! Please check that you have typed in the correct detail", Message.Category.Error);
                                    return View(viewModel);
                                }
                                else
                                {
                                    paymentEtranzactLogic.UpdatePin(payment.Payment.Payment, appForm.Person);
                                    viewModel.Result = result;
                                    viewModel.jambDetail = jambDetail;
                                    viewModel.ApplicationDetail = appForm;
                                    TempData["PostUtmeResultViewModel"] = viewModel;
                                    return RedirectToAction("PostUtmeResultSlip");

                                }

                            }
                            else
                            {
                                SetMessage("Pin has been used by another applicant! Please cross check and Try again.", Message.Category.Error);
                                return View(viewModel);
                            }

                        }
                        else
                        {
                            SetMessage("This Pin is not for result checking! Please cross check and Try again.", Message.Category.Error);
                            return View(viewModel);
                        }
                    }
                    
                }
                else
                {
                    SetMessage("The Jamb Registration number was not found in the database. Please use the jamb number used during the application process", Message.Category.Error);
                    return View(viewModel);
                }
            }

           
            }
            catch (Exception ex)
            {

                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        //Load PUTME Slip view
        public ActionResult PostUtmeResultSlip(PostUtmeResultViewModel viewModel)
        {

            PostUtmeResultViewModel existingViewModel = (PostUtmeResultViewModel)TempData["PostUtmeResultViewModel"];
            TempData["viewModel"] = existingViewModel;
            viewModel = existingViewModel;
            if (viewModel != null)
            {
                return View(viewModel);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }

        public  ActionResult TestStp()
        {
            try
            {
                
                    //APPLICATION_FORM form = new APPLICATION_FORM();
                    //form.Application_Form_Setting_Id = 2;
                    //form.Application_Programme_Fee_Id = 1;
                    //form.Payment_Id = 173662;
                    //form.Person_Id = 120530;
                    //form.Date_Submitted = DateTime.Now;
                    //form.Rejected = false;
                    //form.Release = true;
                    //db.APPLICATION_FORM.Add(form);
                    //int formid =  db.SaveChanges();
             
                //ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                //ApplicationForm applicationForm = new ApplicationForm();
                //applicationForm = applicationFormLogic.GetBy(113451);
                //applicationForm.Remarks = "Just making sure";
                //applicationForm.Release = false;
                //applicationForm.RejectReason = "It works";
                //applicationFormLogic.Modify(applicationForm);
                // applicationForm = applicationFormLogic.GetBy(113451);

                //ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                //ApplicationForm applicationForm = new ApplicationForm();
                //applicationForm.Setting = new ApplicationFormSetting(){Id = 2}; 
                //applicationForm.ProgrammeFee = new ApplicationProgrammeFee() {Id = 1};
                //applicationForm.Payment = new Payment() {Id = 173662};
                //applicationForm.Person = new Person() {Id = 120530};
                //applicationForm.DateSubmitted = DateTime.Now;
                //applicationForm.Rejected = false;
                //applicationForm.Release = true;
                //var outputdata = applicationFormLogic.Create(applicationForm);

            }
            catch (Exception ex)
            {
                    
                throw ex;
            }
            return View();
        }
    }
}
