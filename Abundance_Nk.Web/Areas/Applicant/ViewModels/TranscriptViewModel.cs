﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Applicant.ViewModels
{
    public class TranscriptViewModel
    {
        public TranscriptViewModel()
        {
            StateSelectList = Utility.PopulateStateSelectListItem();
            CountrySelectList = Utility.PopulateCountrySelectListItem();
            transcriptRequest = new TranscriptRequest();
        }
        public List<SelectListItem> FeesTypeSelectList { get; set; }
        public List<SelectListItem> StateSelectList { get; set; }
        public List<SelectListItem> CountrySelectList { get; set; }
        public TranscriptRequest transcriptRequest { get; set; }
        public List<TranscriptRequest> TranscriptRequests { get; set; }
        public TranscriptStatus transcriptStatus { get; set; }
        public TranscriptClearanceStatus transcriptClearanceStatus { get; set; }
        public PaymentEtranzact PaymentEtranzact { get; set; }
        public bool Paymentstatus { get; set; }
        public RemitaPayment RemitaPayment { get; set; }
        public RemitaPayementProcessor RemitaPayementProcessor { get; set; }
        public string  RemitaBaseUrl { get; set; }
        public string Hash { get; set; }
        public Fee Fee { get; set; }
        public FeeType FeeType { get; set; }
        public string RequestType { get; set; }
    }
}