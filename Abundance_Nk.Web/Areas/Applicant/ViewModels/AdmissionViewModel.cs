﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.Transactions;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Web.Areas.Applicant.ViewModels
{
    public class AdmissionViewModel : OLevelResultViewModel
    {
        private PaymentLogic paymentLogic;
        private PaymentEtranzactLogic paymentEtranzactLogic;
        private ApplicantLogic applicantLogic;
        private AppliedCourseLogic appliedCourseLogic;
        private ApplicationFormLogic applicationFormLogic;
        private OnlinePaymentLogic onlinePaymentLogic;
        private AdmissionListLogic admissionListLogic;
        private RemitaPaymentLogic remitaPaymentLogic;
        public AdmissionViewModel()
        {
            ApplicationForm = new ApplicationForm();
            ApplicationForm.Person = new Person();

            Applicant = new Abundance_Nk.Model.Model.Applicant();
            Applicant.Status = new ApplicantStatus();
            
            AppliedCourse = new AppliedCourse();
            AppliedCourse.Programme = new Programme();
            AppliedCourse.Department = new Department();

            Payment = new Payment();
            paymentEtranzactLogic = new PaymentEtranzactLogic();

            Invoice = new Invoice();
            Invoice.Payment = new Payment();
            Invoice.Payment.FeeType = new FeeType();
            //Invoice.Payment.Fee.Type = new FeeType();
            Invoice.Person = new Person();

            paymentLogic = new PaymentLogic();
            appliedCourseLogic = new AppliedCourseLogic();
            applicationFormLogic = new ApplicationFormLogic();
            applicantLogic = new ApplicantLogic();
            onlinePaymentLogic = new OnlinePaymentLogic();
            admissionListLogic = new AdmissionListLogic();

            etranzactPin = new PaymentEtranzact();
        }

        public bool Loaded { get; set; }
        public PaymentEtranzact etranzactPin { get; set; }
        public Remita remita { get; set; }
        public RemitaPayment remitaPayment { get; set; }
        public RemitaResponse remitaResponse { get; set; }
        public Receipt Receipt { get; set; }
        public Invoice Invoice { get; set; }
        public Abundance_Nk.Model.Model.Applicant Applicant { get; set; }
        public ApplicationForm ApplicationForm { get; set; }
        public AppliedCourse AppliedCourse { get; set; }
        public AdmissionList AdmissionList { get; set; }
        public Payment Payment { get; set; }
        public Programme Programme { get; set; }
        public string Name { get; set; }
        public List<AdmissionList> AdmissionLists { get; set; }
        public List<AdmissionListDropDownModel> AdmissionListDropDownModels { get; set; }
        public bool IsAdmitted { get; set; }
        public int ApplicantStatusId { get; set; }

        [Display(Name = "Acceptance Confirmation Order Number / RRR Number")]
        public string AcceptanceConfirmationOrderNumber { get; set; }

        [Display(Name = "School Fees Confirmation Order Number")]
        public string SchoolFeesConfirmationOrderNumber { get; set; }

        [Display(Name = "Acceptance Invoice Number")]
        public string AcceptanceInvoiceNumber { get; set; }

        [Display(Name = "Acceptance Receipt Number")]
        public string AcceptanceReceiptNumber { get; set; }

        [Display(Name = "School Fees Invoice Number")]
        public string SchoolFeesInvoiceNumber { get; set; }

        [Display(Name = "School Fees Receipt Number")]
        public string SchoolFeesReceiptNumber { get; set; }       
        public void GetApplicationBy(long formId)
        {
            try
            {
                ApplicationForm = applicationFormLogic.GetModelBy(a => a.Application_Form_Id == formId);
                if (ApplicationForm != null && ApplicationForm.Id > 0)
                {
                    AppliedCourse = appliedCourseLogic.GetModelBy(f => f.Application_Form_Id == ApplicationForm.Id);
                    Payment = paymentLogic.GetModelBy(p => p.Payment_Id == ApplicationForm.Payment.Id);
                    Applicant = applicantLogic.GetModelBy(a => a.Application_Form_Id == ApplicationForm.Id);
                    IsAdmitted = admissionListLogic.IsAdmitted(ApplicationForm);
                    AdmissionList = admissionListLogic.GetBy(formId);

                    if (Applicant != null && Applicant.Status != null)
                    {
                        ApplicantStatusId = Applicant.Status.Id;
                    }

                    

                    //get acceptance payment
                    FeeType acceptanceFee = new FeeType() { Id = (int)FeeTypes.AcceptanceFee };
                    Payment acceptancePayment = paymentLogic.GetBy(ApplicationForm.Person, acceptanceFee);
                    if (acceptancePayment != null)
                    {
                        AcceptanceInvoiceNumber =  acceptancePayment.InvoiceNumber;
                        PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetBy(acceptancePayment);
                        if (paymentEtranzact != null)
                        {
                            AcceptanceReceiptNumber = paymentEtranzact.ReceiptNo;
                            if (ApplicantStatusId >= (int)ApplicantStatus.Status.ClearedAndAccepted)
                            {
                                AcceptanceConfirmationOrderNumber = paymentEtranzact.ConfirmationNo;
                            }
                        }

                       
                    }
                    else
                    {
                        FeeType supplementrayAcceptanceFeeType = new FeeType() { Id = 10 };
                        Payment supplementrayAcceptanceFee = paymentLogic.GetBy(ApplicationForm.Person, supplementrayAcceptanceFeeType);
                        if (supplementrayAcceptanceFee != null)
                        {
                            AcceptanceInvoiceNumber =  supplementrayAcceptanceFee.InvoiceNumber;
                            PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetBy(supplementrayAcceptanceFee);
                            if (paymentEtranzact != null)
                            {
                                AcceptanceReceiptNumber = paymentEtranzact.ReceiptNo;
                                if (ApplicantStatusId >= (int)ApplicantStatus.Status.ClearedAndAccepted)
                                {
                                    AcceptanceConfirmationOrderNumber = paymentEtranzact.ConfirmationNo;
                                }
                            }

                       
                        }
                    }

                    //get school fees payment
                    FeeType schoolFees = new FeeType() { Id = (int)FeeTypes.SchoolFees };
                    Payment schoolFeesPayment = paymentLogic.GetBy(ApplicationForm.Person, schoolFees);
                    if (schoolFeesPayment != null)
                    {
                        SchoolFeesInvoiceNumber = schoolFeesPayment.InvoiceNumber;
                        PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetBy(schoolFeesPayment);
                        if (paymentEtranzact != null)
                        {
                            SchoolFeesReceiptNumber = paymentEtranzact.ReceiptNo;
                            if (ApplicantStatusId >= (int)ApplicantStatus.Status.GeneratedSchoolFeesReceipt)
                            {
                                SchoolFeesConfirmationOrderNumber = paymentEtranzact.ConfirmationNo;
                            }
                        }

                       
                    }

                    Loaded = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ApplicationForm GetApplicationFormBy(string formNumber)
        {
            try
            {
                return applicationFormLogic.GetModelBy(f => f.Application_Form_Number == formNumber);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public AdmissionList GetAdmissionListBy(string AppformNumber)
        {
            try
            {
                return admissionListLogic.GetModelBy(f => f.APPLICATION_FORM.Application_Form_Number == AppformNumber);
            }
            catch(Exception)
            {
                throw;
            }
        }
        public void GetInvoiceBy(string invoiceNumber)
        {
            try
            {
                Payment payment = paymentLogic.GetBy(invoiceNumber);
                PaymentEtranzactType eTranzactpaymentType = new PaymentEtranzactType();
                PaymentEtranzactTypeLogic eTranzactpaymentTypeLogic = new PaymentEtranzactTypeLogic();
                //eTranzactpaymentType = eTranzactpaymentTypeLogic.GetModelBy(et => et.Fee_Type_Id == payment.FeeType.Id);
                eTranzactpaymentType = eTranzactpaymentTypeLogic.GetModelBy(et => et.Fee_Type_Id == payment.FeeType.Id && et.Session_Id == payment.Session.Id);
                Invoice = new Invoice();
                Invoice.Payment = payment;
                Invoice.Person = payment.Person;
                Invoice.ApplicationForm = applicationFormLogic.GetModelBy(f => f.Person_Id == payment.Person.Id);
                Invoice.paymentEtranzactType = eTranzactpaymentType;
                Invoice.JambRegistrationNumber = Invoice.ApplicationForm.Number;
               
                remitaPayment = new RemitaPayment();
                remitaPaymentLogic = new RemitaPaymentLogic();
                remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                if (remitaPayment != null)
                {
                    Invoice.remitaPayment = remitaPayment;
                }

                AcceptanceInvoiceNumber = payment.InvoiceNumber;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Receipt GetReceiptBy(string invoiceNumber)
        {
            try
            {
                Payment payment = paymentLogic.GetBy(invoiceNumber);
                if (payment == null ||payment.Id <= 0)
                {
                    return null;
                }

                PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetModelBy(o => o.Payment_Id == payment.Id);
                if (paymentEtranzact != null)
                {
                    if (payment.FeeDetails == null || payment.FeeDetails.Count <= 0)
                    {
                        throw new Exception("Fee Details for " + payment.FeeType.Name + " not set! please contact your system administrator.");
                    }

                    decimal amount = payment.FeeDetails.Sum(p => p.Fee.Amount);
                    Receipt = BuildReceipt(payment.Person.FullName, payment.InvoiceNumber, paymentEtranzact, amount, payment.FeeType.Name,"","");
                }
                else
                {
                    remitaPayment = new RemitaPayment();
                    remitaPaymentLogic = new RemitaPaymentLogic();
                    remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                    if (remitaPayment != null && remitaPayment.Status.Contains("01:"))
                    {
                        decimal amount = payment.FeeDetails.Sum(p => p.Fee.Amount);
                        Receipt = BuildReceipt(payment.Person.FullName, payment.InvoiceNumber, remitaPayment, amount, payment.FeeType.Name, "", "");
              
                    }
                }
                return Receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Payment GenerateInvoice(ApplicationForm applicationForm, ApplicantStatus.Status status, FeeType feeType)
        {
            try
            {
                Payment payment = new Payment();
                payment.PaymentMode = new PaymentMode() { Id = applicationForm.Setting.PaymentMode.Id };
                payment.PaymentType = new PaymentType() { Id = applicationForm.Setting.PaymentType.Id };
                payment.PersonType = new PersonType() { Id = applicationForm.Setting.PersonType.Id };
                payment.Person = applicationForm.Person;
                payment.DatePaid = DateTime.Now;
                payment.FeeType = feeType;
                payment.Session = applicationForm.Setting.Session;
                
                if (paymentLogic.PaymentAlreadyMade(payment))
                {
                    applicantLogic.UpdateStatus(applicationForm, status);
                    return paymentLogic.GetBy(applicationForm.Person, feeType);
                }
                else
                {
                    Payment newPayment = null;
                    OnlinePayment newOnlinePayment = null;
                    using (TransactionScope transaction = new TransactionScope())
                    {
                        newPayment = paymentLogic.Create(payment);
                        if (newPayment != null)
                        {
                            PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
                            OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                            OnlinePayment onlinePayment = new OnlinePayment();
                            onlinePayment.Channel = channel;
                            onlinePayment.Payment = newPayment;

                            newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
                        }

                        applicantLogic.UpdateStatus(applicationForm, status);
                        transaction.Complete();
                    }

                    return newPayment;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Receipt GenerateReceipt(string invoiceNumber, long formId, ApplicantStatus.Status status)
        {
            try
            {
                Payment payment = paymentLogic.GetBy(invoiceNumber);
                if (payment == null || payment.Id <= 0)
                {
                    return null;
                }

                Receipt receipt = null;
                AdmissionList admissionList = admissionListLogic.GetBy(formId);
                if (admissionList != null && admissionList.Id > 0)
                {
                   
                        remitaPayment = new RemitaPayment();
                        remitaPaymentLogic = new RemitaPaymentLogic();
                        remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                        if (remitaPayment != null && remitaPayment.Status.Contains("01:"))
                        {
                            using (TransactionScope transaction = new TransactionScope())
                            {
                                bool updated = onlinePaymentLogic.UpdateTransactionNumber(payment, remitaPayment.RRR);
                                applicantLogic.UpdateStatus(admissionList.Form, status);

                                transaction.Complete();
                            }

                            decimal amount = payment.FeeDetails.Sum(p => p.Fee.Amount);
                            receipt = BuildReceipt(admissionList.Form.Person.FullName, invoiceNumber, remitaPayment, amount, payment.FeeType.Name, "", admissionList.Form.Number);
                        }
                   
                
              
                    PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetBy(payment);
                    if (paymentEtranzact != null)
                    {
                        using (TransactionScope transaction = new TransactionScope())
                        {
                            bool updated = onlinePaymentLogic.UpdateTransactionNumber(payment, paymentEtranzact.ConfirmationNo);
                            applicantLogic.UpdateStatus(admissionList.Form, status);
                            paymentEtranzactLogic.UpdatePin(payment, admissionList.Form.Person);
                            transaction.Complete();
                        }

                        decimal amount = payment.FeeDetails.Sum(p => p.Fee.Amount);
                        receipt = BuildReceipt(admissionList.Form.Person.FullName, invoiceNumber, paymentEtranzact, amount, payment.FeeType.Name, "", admissionList.Form.Number);
                    }
                
                  

                   
                }

                return receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public Receipt BuildReceipt(string name, string invoiceNumber, PaymentEtranzact paymentEtranzact, decimal amount, string purpose, string MatricNumber, string ApplicationFormNumber)
        {
            try
            {
                Receipt receipt = new Receipt();
                receipt.Number = paymentEtranzact.ReceiptNo;
                receipt.Name = name;
                receipt.ConfirmationOrderNumber = paymentEtranzact.ConfirmationNo;
                receipt.Amount = amount;
                receipt.AmountInWords = "";
                receipt.Purpose = purpose;
                receipt.Date = DateTime.Now;
                receipt.ApplicationFormNumber = ApplicationFormNumber;
                receipt.MatricNumber = MatricNumber;
                return receipt;
            }
            catch(Exception)
            {
                throw;
            }
        }
        public Receipt BuildReceipt(string name, string invoiceNumber, RemitaPayment remitaPayment, decimal amount, string purpose, string MatricNumber, string ApplicationFormNumber)
        {
            try
            {
                Receipt receipt = new Receipt();
                receipt.Number = remitaPayment.OrderId;
                receipt.Name = name;
                receipt.ConfirmationOrderNumber = remitaPayment.RRR;
                receipt.Amount = amount;
                receipt.AmountInWords = "";
                receipt.Purpose = purpose;
                receipt.Date = DateTime.Now;
                receipt.ApplicationFormNumber = ApplicationFormNumber;
                receipt.MatricNumber = MatricNumber;
                return receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string Hash { get; set; }
        public Person Person { get; set; }
    }
    public class AdmissionListDropDownModel
    {
        public long FormId { get; set; }
        public string Name { get; set; }
    }

}