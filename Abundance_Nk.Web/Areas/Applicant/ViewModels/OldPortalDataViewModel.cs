﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Applicant.ViewModels
{
    public class OldPortalDataViewModel
    {
        public OldPortalDataViewModel()
        {
            OldPortalData = new OldPortalData();
            ProgrammeSelectListItem = Utility.PopulateAllProgrammeSelectListItem ();
            StateSelectList = Utility.PopulateStateSelectListItem ();
            if(OldPortalData.Programme != null && OldPortalData.Programme.Id > 0)
            {
                DepartmentSelectListItem = Utility.PopulateDepartmentSelectListItem (OldPortalData.Programme);
            }
        }
        public OldPortalData OldPortalData { get; set; }
        public List<SelectListItem> StateSelectList { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public List<SelectListItem> LocalGovernmentSelectListItems { get; set; }
        public List<SelectListItem> ProgrammeSelectListItem { get; set; }
    }
}