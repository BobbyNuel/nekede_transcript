﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using Abundance_Nk.Model.Model;
using System.ComponentModel.DataAnnotations;
using Abundance_Nk.Business;
using Abundance_Nk.Web.Models;
using System.Web.Mvc;

namespace Abundance_Nk.Web.Areas.Applicant.ViewModels
{
    public class PostJAMBFormPaymentViewModel
    {
        public PostJAMBFormPaymentViewModel()
        {
            Programme = new Programme();
            AppliedCourse = new AppliedCourse();
            AppliedCourse.Programme = new Programme();
            AppliedCourse.Department = new Department();

            Person = new Person();
            Person.State = new State();

            StateSelectList = Utility.PopulateStateSelectListItem();
            ProgrammeSelectListItem = Utility.PopulateProgrammeSelectListItem();
            FeeTypeSelectListItem = Utility.PopulateFormFeeTypeSelectListItem();
        }

        [Display(Name = "JAMB Reg. No")]
        [RegularExpression("^(8)([0-9]{7}[A-Z]{2})$", ErrorMessage = "JAMB Registration No is not valid")]
       
        public string JambRegistrationNumber { get; set; }
        public AppliedCourse AppliedCourse { get; set; }
        public Department Department { get; set; }
        public Programme Programme { get; set; }
        public List<SelectListItem> StateSelectList { get; set; }
        public List<SelectListItem> ProgrammeSelectListItem { get; set; }
        public List<SelectListItem> DepartmentSelectListItem { get; set; }
        public List<SelectListItem> DepartmentOptionSelectListItem { get; set; }
        public List<SelectListItem> FeeTypeSelectListItem { get; set; }
        public FeeType FeeType { get; set; }
        public Person Person { get; set; }
        public ApplicantJambDetail ApplicantJambDetail { get; set; }
        public decimal Amount { get; set; }
        public Session CurrentSession { get; set; }
        public PaymentType PaymentType { get; set; }
        public Payment Payment { get; set; }
        public ApplicationFormSetting ApplicationFormSetting { get; set; }
        public ApplicationProgrammeFee ApplicationProgrammeFee { get; set; }
        public PaymentEtranzactType PaymentEtranzactType { get; set; }
        public RemitaPayment remitaPayment { get; set; }
        public string Hash { get; set; }
        public Remita Remita { get; set; }

        public void Initialise()
        {
            try
            {
                //CurrentSession = GetCurrentSession();

                if (CurrentSession != null && Programme.Id > 0)
                {
                    if (FeeType == null || FeeType.Id <= 0)
                    {
                        FeeType = GetFeeTypeBy(CurrentSession, Programme); 
                    }

                    if (FeeType != null && FeeType.Id == (int)FeeTypes.ApplicationForm && Programme.Id == 5)
                    {
                        FeeType = new FeeType(){Id = (int)FeeTypes.PutmeForm};
                    }

                    ApplicationProgrammeFeeLogic applicationProgrammeFeeLogic = new ApplicationProgrammeFeeLogic();
                    ApplicationProgrammeFee = applicationProgrammeFeeLogic.GetModelsBy(ap => ap.Session_Id == CurrentSession.Id && ap.Programme_Id == Programme.Id && ap.Fee_Type_Id == FeeType.Id).LastOrDefault();
                    
                    PaymentEtranzactType = GetPaymentTypeBy(FeeType,CurrentSession);
                    ApplicationFormSetting = GetApplicationFormSettingBy(CurrentSession);
                   
                    if (ApplicationFormSetting != null)
                    {
                        PaymentType = ApplicationFormSetting.PaymentType;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Session GetCurrentSession()
        {
            try
            {
                Session currentSession = new Session();
                SessionLogic sessionLogic = new SessionLogic();
                currentSession = sessionLogic.GetCurrentSession();
                if(currentSession != null && currentSession.Id > 0)
                {
                    return currentSession;
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public FeeType GetFeeTypeBy(Session session, Programme programme)
        {
            try
            {
                ApplicationProgrammeFeeLogic programmeFeeLogic = new ApplicationProgrammeFeeLogic();
                ApplicationProgrammeFee = programmeFeeLogic.GetBy(programme, session);

                if (ApplicationProgrammeFee != null)
                {
                    return ApplicationProgrammeFee.FeeType;
                }

                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public PaymentEtranzactType GetPaymentTypeBy(FeeType feeType, Session session)
        {
            PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
            PaymentEtranzactType = paymentEtranzactTypeLogic.GetBy(feeType,session);

            if (PaymentEtranzactType != null)
            {
                return PaymentEtranzactType;
            }

            return null;
        }

        public ApplicationFormSetting GetApplicationFormSettingBy(Session session)
        {
            try
            {
                ApplicationFormSettingLogic applicationFormSettingLogic = new ApplicationFormSettingLogic();
                return applicationFormSettingLogic.GetBy(session);
            }
            catch (Exception)
            {
                throw;
            }
        }

        
    }
}