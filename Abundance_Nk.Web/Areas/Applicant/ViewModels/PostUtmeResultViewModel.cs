﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abundance_Nk.Web.Models;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.ComponentModel.DataAnnotations;

namespace Abundance_Nk.Web.Areas.Applicant.ViewModels
{
    public class PostUtmeResultViewModel
    {
        public NdPutmeResult Result { get; set; }

        public ApplicantJambDetail jambDetail { get; set; }

        public ApplicationForm ApplicationDetail { get; set; }

        [Required(ErrorMessage = "Please Enter your Jamb Number or Application Number")]
        public string JambRegistrationNumber { get; set; }
        
        [Required(ErrorMessage = "Please Enter your Etranzact Confirmation Number")]
        public string PinNumber { get; set; }
        public bool ValidatePin()
        {
            return true;
        }
        public PostUtmeResultViewModel()
        {
            Result = new NdPutmeResult();
        }
    }
}