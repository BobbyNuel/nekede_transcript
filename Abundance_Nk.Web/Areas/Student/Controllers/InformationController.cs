﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Web.Areas.Student.Models;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;
using Abundance_Nk.Business;
using System.Transactions;


namespace Abundance_Nk.Web.Areas.Student.Controllers 
{
    [AllowAnonymous]
    public class InformationController : BaseController
    {
        private const string ID = "Id";
        private const string NAME = "Name";
        private const string VALUE = "Value";
        private const string TEXT = "Text";
        private const string DEFAULT_PASSPORT = "/Content/Images/default_avatar.png";

        private FormViewModel viewModel;

        public InformationController()
        {
            viewModel = new FormViewModel();
        }

        public ActionResult Form(long? fid, int? pid)
        {
            FormViewModel existingViewModel = (FormViewModel)TempData["FormViewModel"];

            try
            {
                PopulateAllDropDowns((int)pid);

                if (existingViewModel != null)
                {
                    viewModel = existingViewModel;
                    SetStudentUploadedPassport(viewModel);
                }

                SetLgaIfExist(viewModel);

                ApplicationForm applicationform = viewModel.GetApplicationFormBy((long)fid);
                if ((applicationform != null && applicationform.Id > 0) && viewModel.ApplicationAlreadyExist == false)
                {
                    viewModel.ApplicationAlreadyExist = true;
                    viewModel.LoadApplicantionFormBy((long)fid);

                    SetSelectedSittingSubjectAndGrade(viewModel);
                    SetLgaIfExist(viewModel);
                    SetDepartmentIfExist(viewModel);
                    SetDepartmentOptionIfExist(viewModel);

                    SetDateOfBirth();

                    SetEntryAndStudyMode(viewModel);

                    viewModel.Student.Type = new StudentType() { Id = 3 };
                    if (viewModel.AppliedCourse.Programme.Id == 3 || viewModel.AppliedCourse.Programme.Id == 4)
                    {
                        SetPreviousEducationStartDate();
                        SetPreviousEducationEndDate();

                        viewModel.Student.Category = new StudentCategory() { Id = 2 };
                    }
                    else
                    {
                        viewModel.Student.Category = new StudentCategory() { Id = 1 };
                    }
                    
                     Model.Model.Student student = viewModel.GetStudentBy(applicationform.Person.Id);
                     if (student != null && student.Id > 0)
                     {
                         viewModel.LoadStudentInformationFormBy(applicationform.Person.Id);
                     }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            TempData["FormViewModel"] = viewModel;
            TempData["imageUrl"] = viewModel.Person.ImageFileUrl;

            return View(viewModel);
        }

        private void SetEntryAndStudyMode(FormViewModel vModel)
        {
            try
            {
                //set mode of entry
                vModel.StudentAcademicInformation.ModeOfEntry = new ModeOfEntry() { Id = vModel.AppliedCourse.Programme.Id };

                //set mode of study
                switch (vModel.AppliedCourse.Programme.Id)
                {
                    case 1:
                    case 3:
                        {
                            vModel.StudentAcademicInformation.ModeOfStudy = new ModeOfStudy() { Id = 1 };
                            break;
                        }
                    case 2:
                    case 4:
                        {
                            vModel.StudentAcademicInformation.ModeOfStudy = new ModeOfStudy() { Id = 2 };
                            break;
                        }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Form(FormViewModel viewModel)
        {
            //const string FIRST_SITTING = "FIRST SITTING";
            //const string SECOND_SITTING = "SECOND SITTING";

            try
            {
                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        error.ErrorMessage.ToString();
                    }
                }

                SetStudentUploadedPassport(viewModel);

                ModelState.Remove("Student.FirstName");
                ModelState.Remove("Student.LastName");
                ModelState.Remove("Student.MobilePhone");
                //ModelState["Student.BloodGroup.Id"].Errors.Clear();
                //ModelState["Student.Genotype.Id"].Errors.Clear();

                if (viewModel.AppliedCourse.Programme.Id != 5)
                {
                    ModelState.Remove("ApplicantJambDetail.JambRegistrationNumber");
                }
                                
                if (ModelState.IsValid)
                {
                    if (string.IsNullOrEmpty(viewModel.Person.ImageFileUrl) || viewModel.Person.ImageFileUrl == DEFAULT_PASSPORT)
                    {
                        SetMessage("No Passport uploaded! Please upload your passport to continue.", Message.Category.Error);
                        SetPostJAMBStateVariables(viewModel);
                        return View(viewModel);
                    }

                    TempData["FormViewModel"] = viewModel;
                    return RedirectToAction("FormPreview", "Information");
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            SetPostJAMBStateVariables(viewModel);
            return View(viewModel);
        }

        private void SetPostJAMBStateVariables(FormViewModel viewModel)
        {
            try
            {
                TempData["FormViewModel"] = viewModel;
                TempData["imageUrl"] = viewModel.Person.ImageFileUrl;

                PopulateAllDropDowns(viewModel.AppliedCourse.Programme.Id);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        
        public ActionResult FormPreview()
        {
            FormViewModel viewModel = (FormViewModel)TempData["FormViewModel"];

            try
            {
                if (viewModel != null)
                {
                    viewModel.Person.DateOfBirth = new DateTime(viewModel.Person.YearOfBirth.Id, viewModel.Person.MonthOfBirth.Id, viewModel.Person.DayOfBirth.Id);
                    viewModel.Person.State = viewModel.States.Where(m => m.Id == viewModel.Person.State.Id).SingleOrDefault();
                    viewModel.Person.LocalGovernment = viewModel.Lgas.Where(m => m.Id == viewModel.Person.LocalGovernment.Id).SingleOrDefault();
                    viewModel.Person.Sex = viewModel.Genders.Where(m => m.Id == viewModel.Person.Sex.Id).SingleOrDefault();
                    viewModel.Sponsor.Relationship = viewModel.Relationships.Where(m => m.Id == viewModel.Sponsor.Relationship.Id).SingleOrDefault();
                    viewModel.Person.Religion = viewModel.Religions.Where(m => m.Id == viewModel.Person.Religion.Id).SingleOrDefault();
                    viewModel.Student.Title = viewModel.Titles.Where(m => m.Id == viewModel.Student.Title.Id).SingleOrDefault();
                    viewModel.Student.MaritalStatus = viewModel.MaritalStatuses.Where(m => m.Id == viewModel.Student.MaritalStatus.Id).SingleOrDefault();

                    if (viewModel.Student.BloodGroup != null && viewModel.Student.BloodGroup.Id > 0)
                    {
                        viewModel.Student.BloodGroup = viewModel.BloodGroups.Where(m => m.Id == viewModel.Student.BloodGroup.Id).SingleOrDefault();
                    }
                    if (viewModel.Student.Genotype != null && viewModel.Student.Genotype.Id > 0)
                    {
                        viewModel.Student.Genotype = viewModel.Genotypes.Where(m => m.Id == viewModel.Student.Genotype.Id).SingleOrDefault();
                    }

                    viewModel.StudentAcademicInformation.ModeOfEntry = viewModel.ModeOfEntries.Where(m => m.Id == viewModel.StudentAcademicInformation.ModeOfEntry.Id).SingleOrDefault();
                    viewModel.StudentAcademicInformation.ModeOfStudy = viewModel.ModeOfStudies.Where(m => m.Id == viewModel.StudentAcademicInformation.ModeOfStudy.Id).SingleOrDefault();
                    viewModel.Student.Category = viewModel.StudentCategories.Where(m => m.Id == viewModel.Student.Category.Id).SingleOrDefault();
                    viewModel.Student.Type = viewModel.StudentTypes.Where(m => m.Id == viewModel.Student.Type.Id).SingleOrDefault();
                    viewModel.StudentAcademicInformation.Level = viewModel.Levels.Where(m => m.Id == viewModel.StudentAcademicInformation.Level.Id).SingleOrDefault();
                    viewModel.StudentFinanceInformation.Mode = viewModel.ModeOfFinances.Where(m => m.Id == viewModel.StudentFinanceInformation.Mode.Id).SingleOrDefault();
                    viewModel.StudentSponsor.Relationship = viewModel.Relationships.Where(m => m.Id == viewModel.StudentSponsor.Relationship.Id).SingleOrDefault();
                    
                    viewModel.FirstSittingOLevelResult.Type = viewModel.OLevelTypes.Where(m => m.Id == viewModel.FirstSittingOLevelResult.Type.Id).SingleOrDefault();
                    if (viewModel.SecondSittingOLevelResult.Type != null)
                    {
                        viewModel.SecondSittingOLevelResult.Type = viewModel.OLevelTypes.Where(m => m.Id == viewModel.SecondSittingOLevelResult.Type.Id).SingleOrDefault();
                    }

                    if (viewModel.AppliedCourse.Programme.Id == 3 || viewModel.AppliedCourse.Programme.Id == 4)
                    {
                        viewModel.ApplicantPreviousEducation.StartDate = new DateTime(viewModel.ApplicantPreviousEducation.StartYear.Id, viewModel.ApplicantPreviousEducation.StartMonth.Id, viewModel.ApplicantPreviousEducation.StartDay.Id);
                        viewModel.ApplicantPreviousEducation.EndDate = new DateTime(viewModel.ApplicantPreviousEducation.EndYear.Id, viewModel.ApplicantPreviousEducation.EndMonth.Id, viewModel.ApplicantPreviousEducation.EndDay.Id);
                        viewModel.StudentEmploymentInformation.StartDate = new DateTime(viewModel.StudentEmploymentInformation.StartYear.Id, viewModel.StudentEmploymentInformation.StartMonth.Id, viewModel.StudentEmploymentInformation.StartDay.Id);
                        viewModel.StudentEmploymentInformation.EndDate = new DateTime(viewModel.StudentEmploymentInformation.EndYear.Id, viewModel.StudentEmploymentInformation.EndMonth.Id, viewModel.StudentEmploymentInformation.EndDay.Id);
                        viewModel.StudentNdResult.DateAwarded = new DateTime(viewModel.StudentNdResult.YearAwarded.Id, viewModel.StudentNdResult.MonthAwarded.Id, viewModel.StudentNdResult.DayAwarded.Id);
                        viewModel.ApplicantPreviousEducation.ResultGrade = viewModel.ResultGrades.Where(m => m.Id == viewModel.ApplicantPreviousEducation.ResultGrade.Id).SingleOrDefault();
                    }
                    //else
                    //{
                    //    viewModel.ApplicantPreviousEducation.ResultGrade = viewModel.ResultGrades.Where(m => m.Id == viewModel.ApplicantPreviousEducation.ResultGrade.Id).SingleOrDefault();
                    //    viewModel.ApplicantPreviousEducation.ResultGrade = viewModel.ResultGrades.Where(m => m.Id == viewModel.ApplicantPreviousEducation.ResultGrade.Id).SingleOrDefault();

                    //    ViewBag.JambScoreId = viewModel.JambScoreSelectList;
                    //    ViewBag.InstitutionChoiceId = viewModel.InstitutionChoiceSelectList;
                    //}

                    UpdateOLevelResultDetail(viewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            TempData["FormViewModel"] = viewModel;
            return View(viewModel);
        }

        [HttpPost]
        public ActionResult FormPreview(FormViewModel vm)
        {
            FormViewModel viewModel = (FormViewModel)TempData["FormViewModel"];

            try
            {
                if (viewModel.ApplicationForm != null && viewModel.ApplicationForm.Id > 0 && viewModel.Student != null && viewModel.Student.Id <= 0)
                {
                    //using (TransactionScope transaction = new TransactionScope())
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        viewModel.Student.ApplicationForm = viewModel.ApplicationForm;
                        viewModel.Student.Id = viewModel.Person.Id;
                        viewModel.Student.Status = new StudentStatus() { Id = 1 };
                        StudentLogic studentLogic = new StudentLogic();
                        Abundance_Nk.Model.Model.Student newStudent = studentLogic.Create(viewModel.Student);
                        
                        
                        viewModel.StudentSponsor.Student = newStudent;
                        StudentSponsorLogic sponsorLogic = new StudentSponsorLogic();
                        sponsorLogic.Create(viewModel.StudentSponsor);

                                                
                        viewModel.StudentAcademicInformation.Student = newStudent;
                        StudentAcademicInformationLogic academicInformationLogic = new StudentAcademicInformationLogic();
                        academicInformationLogic.Create(viewModel.StudentAcademicInformation);

                                                
                        viewModel.StudentFinanceInformation.Student = newStudent;
                        StudentFinanceInformationLogic financeInformationLogic = new StudentFinanceInformationLogic();
                        financeInformationLogic.Create(viewModel.StudentFinanceInformation);

                        if (viewModel.AppliedCourse.Programme.Id == 3 || viewModel.AppliedCourse.Programme.Id == 4)
                        {
                            viewModel.StudentEmploymentInformation.Student = newStudent;
                            StudentEmploymentInformationLogic employmentInformationLogic = new StudentEmploymentInformationLogic();
                            employmentInformationLogic.Create(viewModel.StudentEmploymentInformation);


                            viewModel.StudentNdResult.Student = newStudent;
                            StudentNdResultLogic ndResultLogic = new StudentNdResultLogic();
                            ndResultLogic.Create(viewModel.StudentNdResult);
                        }

                        //update applicant status
                        ApplicantLogic applicantLogic = new ApplicantLogic();
                        applicantLogic.UpdateStatus(viewModel.ApplicationForm, ApplicantStatus.Status.CompletedStudentInformationForm);


                        transaction.Complete();


                        //OLevelResultLogic oLevelResultLogic = new OLevelResultLogic();
                        //OLevelResultDetailLogic oLevelResultDetailLogic = new OLevelResultDetailLogic();
                        //if (existingViewModel.FirstSittingOLevelResult != null && existingViewModel.FirstSittingOLevelResult.ExamNumber != null && existingViewModel.FirstSittingOLevelResult.Type != null && existingViewModel.FirstSittingOLevelResult.ExamYear > 0)
                        //{
                        //    existingViewModel.FirstSittingOLevelResult.ApplicationForm = newApplicationForm;
                        //    existingViewModel.FirstSittingOLevelResult.Person = viewModel.Person;
                        //    existingViewModel.FirstSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 1 };
                        //    OLevelResult firstSittingOLevelResult = oLevelResultLogic.Create(existingViewModel.FirstSittingOLevelResult);

                        //    if (existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0 && firstSittingOLevelResult != null)
                        //    {
                        //        List<OLevelResultDetail> olevelResultDetails = existingViewModel.FirstSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
                        //        foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
                        //        {
                        //            oLevelResultDetail.Header = firstSittingOLevelResult;
                        //        }

                        //        oLevelResultDetailLogic.Create(olevelResultDetails);
                        //    }
                        //}

                        //if (existingViewModel.SecondSittingOLevelResult != null && existingViewModel.SecondSittingOLevelResult.ExamNumber != null && existingViewModel.SecondSittingOLevelResult.Type != null && existingViewModel.SecondSittingOLevelResult.ExamYear > 0)
                        //{
                        //    List<OLevelResultDetail> olevelResultDetails = existingViewModel.SecondSittingOLevelResultDetails.Where(m => m.Grade != null && m.Subject != null).ToList();
                        //    if (olevelResultDetails != null && olevelResultDetails.Count > 0)
                        //    {
                        //        existingViewModel.SecondSittingOLevelResult.ApplicationForm = newApplicationForm;
                        //        existingViewModel.SecondSittingOLevelResult.Person = viewModel.Person;
                        //        existingViewModel.SecondSittingOLevelResult.Sitting = new OLevelExamSitting() { Id = 2 };
                        //        OLevelResult secondSittingOLevelResult = oLevelResultLogic.Create(existingViewModel.SecondSittingOLevelResult);

                        //        if (existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0 && secondSittingOLevelResult != null)
                        //        {
                        //            foreach (OLevelResultDetail oLevelResultDetail in olevelResultDetails)
                        //            {
                        //                oLevelResultDetail.Header = secondSittingOLevelResult;
                        //            }

                        //            oLevelResultDetailLogic.Create(olevelResultDetails);
                        //        }
                        //    }
                        //}


                        ////set resject reason
                        //if (existingViewModel.Programme.Id == 3)
                        //{
                        //    newApplicationForm.Release = false;
                        //    existingViewModel.AppliedCourse.Person = existingViewModel.Person;

                        //    AdmissionCriteriaLogic admissionCriteriaLogic = new AdmissionCriteriaLogic();
                        //    string rejectReason = admissionCriteriaLogic.EvaluateApplication(existingViewModel.AppliedCourse);
                        //    if (string.IsNullOrEmpty(rejectReason))
                        //    {
                        //        newApplicationForm.Rejected = false;
                        //    }
                        //    else
                        //    {
                        //        newApplicationForm.Rejected = true;
                        //        newApplicationForm.RejectReason = rejectReason;
                        //    }
                        //}
                        //else
                        //{
                        //    newApplicationForm.Release = true;
                        //    newApplicationForm.Rejected = false;
                        //}

                        //applicationFormLogic.SetRejectReason(newApplicationForm);



                        //if (existingViewModel.Programme.Id == 3)
                        //{
                        //    if (existingViewModel.PreviousEducation != null)
                        //    {
                        //        PreviousEducationLogic previousEducationLogic = new PreviousEducationLogic();
                        //        existingViewModel.PreviousEducation.ApplicationForm = newApplicationForm;
                        //        existingViewModel.PreviousEducation.Person = viewModel.Person;
                        //        previousEducationLogic.Create(existingViewModel.PreviousEducation);
                        //    }
                        //}
                        //else
                        //{
                        //    if (existingViewModel.ApplicantJambDetail != null)
                        //    {
                        //        ApplicantJambDetailLogic applicantJambDetailLogic = new ApplicantJambDetailLogic();
                        //        existingViewModel.ApplicantJambDetail.ApplicationForm = newApplicationForm;
                        //        existingViewModel.ApplicantJambDetail.Person = viewModel.Person;
                        //        applicantJambDetailLogic.Modify(existingViewModel.ApplicantJambDetail);
                        //    }
                        //}


                        //string junkFilePath;
                        //string destinationFilePath;
                        //SetPersonPassportDestination(existingViewModel, out junkFilePath, out destinationFilePath);

                        //PersonLogic personLogic = new PersonLogic();
                        //bool personModified = personLogic.Modify(existingViewModel.Person);
                        //if (personModified)
                        //{
                        //    SavePersonPassport(junkFilePath, destinationFilePath, existingViewModel.Person);
                        //    transaction.Complete();
                        //}


                        
                    }
                }
                //else
                //{
                //    existingViewModel.ApplicationFormNumber = viewModel.ApplicationForm.Number;
                //}

                TempData["FormViewModel"] = viewModel;
                return RedirectToAction("AcknowledgementSlip", "Information");
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            TempData["FormViewModel"] = viewModel;
            return View(viewModel);
        }

        public ActionResult AcknowledgementSlip()
        {
            FormViewModel existingViewModel = (FormViewModel)TempData["FormViewModel"];

            TempData["FormViewModel"] = existingViewModel;
            return View(existingViewModel);
        }

        private void UpdateOLevelResultDetail(FormViewModel viewModel)
        {
            try
            {
                if (viewModel != null && viewModel.FirstSittingOLevelResultDetails != null && viewModel.FirstSittingOLevelResultDetails.Count > 0)
                {
                    foreach (OLevelResultDetail firstSittingOLevelResultDetail in viewModel.FirstSittingOLevelResultDetails)
                    {
                        if (firstSittingOLevelResultDetail.Subject != null)
                        {
                            firstSittingOLevelResultDetail.Subject = viewModel.OLevelSubjects.Where(m => m.Id == firstSittingOLevelResultDetail.Subject.Id).SingleOrDefault();
                        }
                        if (firstSittingOLevelResultDetail.Grade != null)
                        {
                            firstSittingOLevelResultDetail.Grade = viewModel.OLevelGrades.Where(m => m.Id == firstSittingOLevelResultDetail.Grade.Id).SingleOrDefault();
                        }
                    }
                }

                if (viewModel != null && viewModel.SecondSittingOLevelResultDetails != null && viewModel.SecondSittingOLevelResultDetails.Count > 0)
                {
                    foreach (OLevelResultDetail secondSittingOLevelResultDetail in viewModel.SecondSittingOLevelResultDetails)
                    {
                        if (secondSittingOLevelResultDetail.Subject != null)
                        {
                            secondSittingOLevelResultDetail.Subject = viewModel.OLevelSubjects.Where(m => m.Id == secondSittingOLevelResultDetail.Subject.Id).SingleOrDefault();
                        }
                        if (secondSittingOLevelResultDetail.Grade != null)
                        {
                            secondSittingOLevelResultDetail.Grade = viewModel.OLevelGrades.Where(m => m.Id == secondSittingOLevelResultDetail.Grade.Id).SingleOrDefault();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetStudentUploadedPassport(FormViewModel viewModel)
        {
            if (viewModel != null && viewModel.Person != null && !string.IsNullOrEmpty((string)TempData["imageUrl"]))
            {
                viewModel.Person.ImageFileUrl = (string)TempData["imageUrl"];
            }
            else
            {
                viewModel.Person.ImageFileUrl = DEFAULT_PASSPORT;
            }
        }

        private void PopulateAllDropDowns(int programmeId)
        {
            FormViewModel existingViewModel = (FormViewModel)TempData["FormViewModel"];

            try
            {
                if (existingViewModel == null)
                {
                    viewModel = new FormViewModel();

                    ViewBag.StateId = viewModel.StateSelectList;
                    ViewBag.SexId = viewModel.SexSelectList;
                    ViewBag.FirstChoiceFacultyId = viewModel.FacultySelectList;
                    ViewBag.SecondChoiceFacultyId = viewModel.FacultySelectList;
                    ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), ID, NAME);
                    ViewBag.RelationshipId = viewModel.RelationshipSelectList;
                    ViewBag.FirstSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
                    ViewBag.SecondSittingOLevelTypeId = viewModel.OLevelTypeSelectList;
                    ViewBag.FirstSittingExamYearId = viewModel.ExamYearSelectList;
                    ViewBag.SecondSittingExamYearId = viewModel.ExamYearSelectList;
                    ViewBag.ReligionId = viewModel.ReligionSelectList;
                    ViewBag.AbilityId = viewModel.AbilitySelectList;
                    ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                    ViewBag.MonthOfBirthId = viewModel.MonthOfBirthSelectList;
                    ViewBag.YearOfBirthId = viewModel.YearOfBirthSelectList;
                    
                    ViewBag.TitleId = viewModel.TitleSelectList;
                    ViewBag.MaritalStatusId = viewModel.MaritalStatusSelectList;
                    ViewBag.BloodGroupId = viewModel.BloodGroupSelectList;
                    ViewBag.GenotypeId = viewModel.GenotypeSelectList;
                    ViewBag.ModeOfEntryId = viewModel.ModeOfEntrySelectList;
                    ViewBag.ModeOfStudyId = viewModel.ModeOfStudySelectList;

                    //ViewBag.StudentTypeId = viewModel.StudentTypeSelectList;
                    //ViewBag.StudentStatusId = viewModel.StudentStatusSelectList;

                    ViewBag.StudentCategoryId = viewModel.StudentCategorySelectList;
                    ViewBag.StudentTypeId = viewModel.StudentTypeSelectList;

                    ViewBag.LevelId = viewModel.LevelSelectList;
                    ViewBag.ModeOfFinanceId = viewModel.ModeOfFinanceSelectList;
                    ViewBag.RelationshipId = viewModel.RelationshipSelectList;
                    ViewBag.FacultyId = viewModel.FacultySelectList;
                    ViewBag.AdmissionYearId = viewModel.AdmissionYearSelectList;
                    ViewBag.GraduationYearId = viewModel.GraduationYearSelectList;
                    ViewBag.ProgrammeId = viewModel.ProgrammeSelectList;

                    if (viewModel.DepartmentSelectList != null)
                    {
                        ViewBag.DepartmentId = viewModel.DepartmentSelectList;
                    }
                    else
                    {
                        ViewBag.DepartmentId = new SelectList(new List<Department>(), ID, NAME);
                    }

                    if (programmeId == 3 || programmeId == 4)
                    {
                        ViewBag.StudentNdResultDayAwardedId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.StudentNdResultMonthAwardedId = viewModel.StudentNdResultMonthAwardedSelectList;
                        ViewBag.StudentNdResultYearAwardedId = viewModel.StudentNdResultYearAwardedSelectList;

                        ViewBag.StudentLastEmploymentStartDayId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.StudentLastEmploymentStartMonthId = viewModel.StudentLastEmploymentStartMonthSelectList;
                        ViewBag.StudentLastEmploymentStartYearId = viewModel.StudentLastEmploymentStartYearSelectList;

                        ViewBag.StudentLastEmploymentEndDayId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.StudentLastEmploymentEndMonthId = viewModel.StudentLastEmploymentEndMonthSelectList;
                        ViewBag.StudentLastEmploymentEndYearId = viewModel.StudentLastEmploymentEndYearSelectList;

                        ViewBag.PreviousEducationStartDayId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.PreviousEducationStartMonthId = viewModel.PreviousEducationStartMonthSelectList;
                        ViewBag.PreviousEducationStartYearId = viewModel.PreviousEducationStartYearSelectList;

                        ViewBag.PreviousEducationEndDayId = new SelectList(new List<Value>(), ID, NAME);
                        ViewBag.PreviousEducationEndMonthId = viewModel.PreviousEducationEndMonthSelectList;
                        ViewBag.PreviousEducationEndYearId = viewModel.PreviousEducationEndYearSelectList;

                        ViewBag.ResultGradeId = viewModel.ResultGradeSelectList;
                        //ViewBag.QualificationId = viewModel.EducationalQualificationSelectList;
                        //ViewBag.ITDurationId = viewModel.ITDurationSelectList;
                    }
                    //else
                    //{
                    //    ViewBag.JambScoreId = viewModel.JambScoreSelectList;
                    //    ViewBag.InstitutionChoiceId = viewModel.InstitutionChoiceSelectList;
                    //}

                    SetDefaultSelectedSittingSubjectAndGrade(viewModel);
                }
                else
                {
                    if (existingViewModel.Student.Title == null) { existingViewModel.Student.Title = new Title(); }
                    if (existingViewModel.Person.Religion == null) { existingViewModel.Person.Religion = new Religion(); }
                    if (existingViewModel.Person.State == null) { existingViewModel.Person.State = new State(); }
                    if (existingViewModel.Person.Sex == null) { existingViewModel.Person.Sex = new Sex(); }
                    if (existingViewModel.AppliedCourse.Programme == null) { existingViewModel.AppliedCourse.Programme = new Programme(); }
                    if (existingViewModel.Sponsor.Relationship == null) { existingViewModel.Sponsor.Relationship = new Relationship(); }
                    if (existingViewModel.FirstSittingOLevelResult.Type == null) { existingViewModel.FirstSittingOLevelResult.Type = new OLevelType(); }
                    if (existingViewModel.Applicant.Ability == null) { existingViewModel.Applicant.Ability = new Ability(); }
                    if (existingViewModel.Person.YearOfBirth == null) { existingViewModel.Person.YearOfBirth = new Value(); }
                    if (existingViewModel.Person.MonthOfBirth == null) { existingViewModel.Person.MonthOfBirth = new Value(); }
                    if (existingViewModel.Person.DayOfBirth == null) { existingViewModel.Person.DayOfBirth = new Value(); }
                    if (existingViewModel.AppliedCourse.Department == null) { existingViewModel.AppliedCourse.Department = new Department(); }
                    if (existingViewModel.Student.BloodGroup == null) { existingViewModel.Student.BloodGroup = new BloodGroup(); }
                    if (existingViewModel.Student.Genotype == null) { existingViewModel.Student.Genotype = new Genotype(); }

                    ViewBag.ReligionId = new SelectList(existingViewModel.ReligionSelectList, VALUE, TEXT, existingViewModel.Person.Religion.Id);
                    ViewBag.StateId = new SelectList(existingViewModel.StateSelectList, VALUE, TEXT, existingViewModel.Person.State.Id);
                    ViewBag.SexId = new SelectList(existingViewModel.SexSelectList, VALUE, TEXT, existingViewModel.Person.Sex.Id);
                    ViewBag.ProgrammeId = new SelectList(existingViewModel.FacultySelectList, VALUE, TEXT, existingViewModel.AppliedCourse.Programme.Id);
                    ViewBag.RelationshipId = new SelectList(existingViewModel.RelationshipSelectList, VALUE, TEXT, existingViewModel.Sponsor.Relationship.Id);
                    ViewBag.FirstSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, existingViewModel.FirstSittingOLevelResult.Type.Id);
                    ViewBag.FirstSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, VALUE, TEXT, existingViewModel.FirstSittingOLevelResult.ExamYear);
                    ViewBag.SecondSittingExamYearId = new SelectList(existingViewModel.ExamYearSelectList, VALUE, TEXT, existingViewModel.SecondSittingOLevelResult.ExamYear);
                    ViewBag.AbilityId = new SelectList(existingViewModel.AbilitySelectList, VALUE, TEXT, existingViewModel.Applicant.Ability.Id);
                    ViewBag.ProgrammeId = new SelectList(existingViewModel.ProgrammeSelectList, VALUE, TEXT, existingViewModel.AppliedCourse.Programme.Id);
                    //ViewBag.DepartmentId = new SelectList(existingViewModel.DepartmentSelectList, VALUE, TEXT, existingViewModel.AppliedCourse.Department.Id);

                    SetDepartmentIfExist(viewModel);
                    SetDepartmentOptionIfExist(viewModel);

                    if (programmeId == 3 || programmeId == 4)
                    {
                        SetStudentNdResultDateAwardedDropDown(existingViewModel);
                        SetStudentLastEmploymentEndDateDropDown(existingViewModel);
                        SetStudentLastEmploymentStartDateDropDown(existingViewModel);
                        SetPreviousEducationEndDateDropDowns(existingViewModel);
                        SetPreviousEducationStartDateDropDowns(existingViewModel);

                        ViewBag.ResultGradeId = new SelectList(existingViewModel.ResultGradeSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.ResultGrade.Id);
                        //ViewBag.QualificationId = new SelectList(existingViewModel.EducationalQualificationSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.Qualification.Id);
                        //ViewBag.ITDurationId = new SelectList(existingViewModel.ITDurationSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.ITDuration.Id);
                    }
                    //else
                    //{
                    //    ViewBag.InstitutionChoiceId = new SelectList(existingViewModel.InstitutionChoiceSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.InstitutionChoice.Id);
                    //    ViewBag.JambScoreId = new SelectList(existingViewModel.JambScoreSelectList, VALUE, TEXT, existingViewModel.ApplicantJambDetail.JambScore);
                    //}

                    ViewBag.TitleId = new SelectList(existingViewModel.TitleSelectList, VALUE, TEXT, existingViewModel.Student.Title.Id);
                    ViewBag.MaritalStatusId = new SelectList(existingViewModel.MaritalStatusSelectList, VALUE, TEXT, existingViewModel.Student.MaritalStatus.Id);
                    ViewBag.BloodGroupId = new SelectList(existingViewModel.BloodGroupSelectList, VALUE, TEXT, existingViewModel.Student.BloodGroup.Id);
                    ViewBag.GenotypeId = new SelectList(existingViewModel.GenotypeSelectList, VALUE, TEXT, existingViewModel.Student.Genotype.Id);
                    ViewBag.ModeOfEntryId = new SelectList(existingViewModel.ModeOfEntrySelectList, VALUE, TEXT, existingViewModel.StudentAcademicInformation.ModeOfEntry.Id);
                    ViewBag.ModeOfStudyId = new SelectList(existingViewModel.ModeOfStudySelectList, VALUE, TEXT, existingViewModel.StudentAcademicInformation.ModeOfStudy.Id);

                    ViewBag.StudentCategoryId = new SelectList(viewModel.StudentCategorySelectList, VALUE, TEXT, existingViewModel.Student.Category.Id);
                    ViewBag.StudentTypeId = new SelectList(viewModel.StudentTypeSelectList, VALUE, TEXT, existingViewModel.Student.Type.Id);

                    //ViewBag.StudentTypeId = new SelectList(existingViewModel.StudentTypeSelectList, VALUE, TEXT, existingViewModel.Student.Type.Id);
                    //ViewBag.StudentStatusId = new SelectList(existingViewModel.StudentStatusSelectList, VALUE, TEXT, existingViewModel.Student.Status.Id);
                    
                    ViewBag.LevelId = new SelectList(existingViewModel.LevelSelectList, VALUE, TEXT, existingViewModel.StudentAcademicInformation.Level.Id);
                    ViewBag.ModeOfFinanceId = new SelectList(existingViewModel.ModeOfFinanceSelectList, VALUE, TEXT, existingViewModel.StudentFinanceInformation.Mode.Id);
                    ViewBag.RelationshipId = new SelectList(existingViewModel.RelationshipSelectList, VALUE, TEXT, existingViewModel.StudentSponsor.Relationship.Id);
                    ViewBag.AdmissionYearId = new SelectList(existingViewModel.AdmissionYearSelectList, VALUE, TEXT, existingViewModel.StudentAcademicInformation.YearOfAdmission);
                    ViewBag.GraduationYearId = new SelectList(existingViewModel.GraduationYearSelectList, VALUE, TEXT, existingViewModel.StudentAcademicInformation.YearOfGraduation);
                    ViewBag.FacultyId = new SelectList(existingViewModel.FacultySelectList, VALUE, TEXT, existingViewModel.AppliedCourse.Department.Faculty.Id);

                    SetDateOfBirthDropDown(existingViewModel);
                   
                    if (existingViewModel.Person.LocalGovernment != null && existingViewModel.Person.LocalGovernment.Id > 0)
                    {
                        ViewBag.LgaId = new SelectList(existingViewModel.LocalGovernmentSelectList, VALUE, TEXT, existingViewModel.Person.LocalGovernment.Id);
                    }
                    else
                    {
                        ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), VALUE, TEXT);
                    }

                    if (existingViewModel.SecondSittingOLevelResult.Type != null)
                    {
                        ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, existingViewModel.SecondSittingOLevelResult.Type.Id);
                    }
                    else
                    {
                        ViewBag.SecondSittingOLevelTypeId = new SelectList(existingViewModel.OLevelTypeSelectList, VALUE, TEXT, 0);
                    }

                    SetSelectedSittingSubjectAndGrade(existingViewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        
        private void SetStudentLastEmploymentStartDateDropDown(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.StudentLastEmploymentStartMonthId = new SelectList(existingViewModel.StudentLastEmploymentStartMonthSelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.StartMonth.Id);
                ViewBag.StudentLastEmploymentStartYearId = new SelectList(existingViewModel.StudentLastEmploymentStartYearSelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.StartYear.Id);
                
                if ((existingViewModel.StudentLastEmploymentStartDaySelectList == null || existingViewModel.StudentLastEmploymentStartDaySelectList.Count == 0) && (existingViewModel.StudentEmploymentInformation.StartMonth.Id > 0 && existingViewModel.StudentEmploymentInformation.StartYear.Id > 0))
                {
                    existingViewModel.StudentLastEmploymentStartDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.StudentEmploymentInformation.StartMonth, existingViewModel.StudentEmploymentInformation.StartYear);
                }
                else
                {
                    if (existingViewModel.StudentLastEmploymentStartDaySelectList != null && existingViewModel.StudentEmploymentInformation.StartDay.Id > 0)
                    {
                        ViewBag.StudentLastEmploymentStartDayId = new SelectList(existingViewModel.StudentLastEmploymentStartDaySelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.StartDay.Id);
                    }
                    else if (existingViewModel.StudentLastEmploymentStartDaySelectList != null && existingViewModel.StudentEmploymentInformation.StartDay.Id <= 0)
                    {
                        ViewBag.StudentLastEmploymentStartDayId = existingViewModel.StudentLastEmploymentStartDaySelectList;
                    }
                    else if (existingViewModel.StudentLastEmploymentStartDaySelectList == null)
                    {
                        existingViewModel.StudentLastEmploymentStartDaySelectList = new List<SelectListItem>();
                        ViewBag.StudentLastEmploymentStartDayId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.StudentEmploymentInformation.StartDay != null && existingViewModel.StudentEmploymentInformation.StartDay.Id > 0)
                {
                    ViewBag.StudentLastEmploymentStartDayId = new SelectList(existingViewModel.StudentLastEmploymentStartDaySelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.StartDay.Id);
                }
                else
                {
                    ViewBag.StudentLastEmploymentStartDayId = existingViewModel.StudentLastEmploymentStartDaySelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetStudentLastEmploymentEndDateDropDown(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.StudentLastEmploymentEndMonthId = new SelectList(existingViewModel.StudentLastEmploymentEndMonthSelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.EndMonth.Id);
                ViewBag.StudentLastEmploymentEndYearId = new SelectList(existingViewModel.StudentLastEmploymentEndYearSelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.EndYear.Id);

                if ((existingViewModel.StudentLastEmploymentEndDaySelectList == null || existingViewModel.StudentLastEmploymentEndDaySelectList.Count == 0) && (existingViewModel.StudentEmploymentInformation.EndMonth.Id > 0 && existingViewModel.StudentEmploymentInformation.EndYear.Id > 0))
                {
                    existingViewModel.StudentLastEmploymentEndDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.StudentEmploymentInformation.EndMonth, existingViewModel.StudentEmploymentInformation.EndYear);
                }
                else
                {
                    if (existingViewModel.StudentLastEmploymentEndDaySelectList != null && existingViewModel.StudentEmploymentInformation.EndDay.Id > 0)
                    {
                        ViewBag.StudentLastEmploymentEndDayId = new SelectList(existingViewModel.StudentLastEmploymentEndDaySelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.EndDay.Id);
                    }
                    else if (existingViewModel.StudentLastEmploymentEndDaySelectList != null && existingViewModel.StudentEmploymentInformation.EndDay.Id <= 0)
                    {
                        ViewBag.StudentLastEmploymentEndDayId = existingViewModel.StudentLastEmploymentEndDaySelectList;
                    }
                    else if (existingViewModel.StudentLastEmploymentEndDaySelectList == null)
                    {
                        existingViewModel.StudentLastEmploymentEndDaySelectList = new List<SelectListItem>();
                        ViewBag.StudentLastEmploymentEndDayId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.StudentEmploymentInformation.EndDay != null && existingViewModel.StudentEmploymentInformation.EndDay.Id > 0)
                {
                    ViewBag.StudentLastEmploymentEndDayId = new SelectList(existingViewModel.StudentLastEmploymentEndDaySelectList, VALUE, TEXT, existingViewModel.StudentEmploymentInformation.EndDay.Id);
                }
                else
                {
                    ViewBag.StudentLastEmploymentEndDayId = existingViewModel.StudentLastEmploymentEndDaySelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        private void SetStudentNdResultDateAwardedDropDown(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.StudentNdResultMonthAwardedId = new SelectList(existingViewModel.StudentNdResultMonthAwardedSelectList, VALUE, TEXT, existingViewModel.StudentNdResult.MonthAwarded.Id);
                ViewBag.StudentNdResultYearAwardedId = new SelectList(existingViewModel.StudentNdResultYearAwardedSelectList, VALUE, TEXT, existingViewModel.StudentNdResult.YearAwarded.Id);
                if ((existingViewModel.StudentNdResultDayAwardedSelectList == null || existingViewModel.StudentNdResultDayAwardedSelectList.Count == 0) && (existingViewModel.StudentNdResult.MonthAwarded.Id > 0 && existingViewModel.StudentNdResult.YearAwarded.Id > 0))
                {
                    existingViewModel.StudentNdResultDayAwardedSelectList = Utility.PopulateDaySelectListItem(existingViewModel.StudentNdResult.MonthAwarded, existingViewModel.StudentNdResult.YearAwarded);
                }
                else
                {
                    if (existingViewModel.StudentNdResultDayAwardedSelectList != null && existingViewModel.StudentNdResult.DayAwarded.Id > 0)
                    {
                        ViewBag.StudentNdResultDayAwardedId = new SelectList(existingViewModel.StudentNdResultDayAwardedSelectList, VALUE, TEXT, existingViewModel.StudentNdResult.DayAwarded.Id);
                    }
                    else if (existingViewModel.StudentNdResultDayAwardedSelectList != null && existingViewModel.StudentNdResult.DayAwarded.Id <= 0)
                    {
                        ViewBag.StudentNdResultDayAwardedId = existingViewModel.StudentNdResultDayAwardedSelectList;
                    }
                    else if (existingViewModel.StudentNdResultDayAwardedSelectList == null)
                    {
                        existingViewModel.StudentNdResultDayAwardedSelectList = new List<SelectListItem>();
                        ViewBag.StudentNdResultDayAwardedId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.StudentNdResult.DayAwarded != null && existingViewModel.StudentNdResult.DayAwarded.Id > 0)
                {
                    ViewBag.StudentNdResultDayAwardedId = new SelectList(existingViewModel.StudentNdResultDayAwardedSelectList, VALUE, TEXT, existingViewModel.StudentNdResult.DayAwarded.Id);
                }
                else
                {
                    ViewBag.StudentNdResultDayAwardedId = existingViewModel.StudentNdResultDayAwardedSelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetDateOfBirthDropDown(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.MonthOfBirthId = new SelectList(existingViewModel.MonthOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.MonthOfBirth.Id);
                ViewBag.YearOfBirthId = new SelectList(existingViewModel.YearOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.YearOfBirth.Id);
                if ((existingViewModel.DayOfBirthSelectList == null || existingViewModel.DayOfBirthSelectList.Count == 0) && (existingViewModel.Person.MonthOfBirth.Id > 0 && existingViewModel.Person.YearOfBirth.Id > 0))
                {
                    existingViewModel.DayOfBirthSelectList = Utility.PopulateDaySelectListItem(existingViewModel.Person.MonthOfBirth, existingViewModel.Person.YearOfBirth);
                }
                else
                {
                    if (existingViewModel.DayOfBirthSelectList != null && existingViewModel.Person.DayOfBirth.Id > 0)
                    {
                        ViewBag.DayOfBirthId = new SelectList(existingViewModel.DayOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.DayOfBirth.Id);
                    }
                    else if (existingViewModel.DayOfBirthSelectList != null && existingViewModel.Person.DayOfBirth.Id <= 0)
                    {
                        ViewBag.DayOfBirthId = existingViewModel.DayOfBirthSelectList;
                    }
                    else if (existingViewModel.DayOfBirthSelectList == null)
                    {
                        existingViewModel.DayOfBirthSelectList = new List<SelectListItem>();
                        ViewBag.DayOfBirthId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.Person.DayOfBirth != null && existingViewModel.Person.DayOfBirth.Id > 0)
                {
                    ViewBag.DayOfBirthId = new SelectList(existingViewModel.DayOfBirthSelectList, VALUE, TEXT, existingViewModel.Person.DayOfBirth.Id);
                }
                else
                {
                    ViewBag.DayOfBirthId = existingViewModel.DayOfBirthSelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetPreviousEducationStartDateDropDowns(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.PreviousEducationStartMonthId = new SelectList(existingViewModel.PreviousEducationStartMonthSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.StartMonth.Id);
                ViewBag.PreviousEducationStartYearId = new SelectList(existingViewModel.PreviousEducationStartYearSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.StartYear.Id);
                if ((existingViewModel.PreviousEducationStartDaySelectList == null || existingViewModel.PreviousEducationStartDaySelectList.Count == 0) && (existingViewModel.ApplicantPreviousEducation.StartMonth.Id > 0 && existingViewModel.ApplicantPreviousEducation.StartYear.Id > 0))
                {
                    existingViewModel.PreviousEducationStartDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.ApplicantPreviousEducation.StartMonth, existingViewModel.ApplicantPreviousEducation.StartYear);
                }
                else
                {
                    if (existingViewModel.PreviousEducationStartDaySelectList != null && existingViewModel.ApplicantPreviousEducation.StartDay.Id > 0)
                    {
                        ViewBag.PreviousEducationStartDayId = new SelectList(existingViewModel.PreviousEducationStartDaySelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.StartDay.Id);
                    }
                    else if (existingViewModel.PreviousEducationStartDaySelectList != null && existingViewModel.ApplicantPreviousEducation.StartDay.Id <= 0)
                    {
                        ViewBag.PreviousEducationStartDayId = existingViewModel.PreviousEducationStartDaySelectList;
                    }
                    else if (existingViewModel.PreviousEducationStartDaySelectList == null)
                    {
                        existingViewModel.PreviousEducationStartDaySelectList = new List<SelectListItem>();
                        ViewBag.PreviousEducationStartDayId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.ApplicantPreviousEducation.StartDay != null && existingViewModel.ApplicantPreviousEducation.StartDay.Id > 0)
                {
                    ViewBag.PreviousEducationStartDayId = new SelectList(existingViewModel.PreviousEducationStartDaySelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.StartDay.Id);
                }
                else
                {
                    ViewBag.PreviousEducationStartDayId = existingViewModel.PreviousEducationStartDaySelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetPreviousEducationEndDateDropDowns(FormViewModel existingViewModel)
        {
            try
            {
                ViewBag.PreviousEducationEndMonthId = new SelectList(existingViewModel.PreviousEducationEndMonthSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.EndMonth.Id);
                ViewBag.PreviousEducationEndYearId = new SelectList(existingViewModel.PreviousEducationEndYearSelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.EndYear.Id);
                if ((existingViewModel.PreviousEducationEndDaySelectList == null || existingViewModel.PreviousEducationEndDaySelectList.Count == 0) && (existingViewModel.ApplicantPreviousEducation.EndMonth.Id > 0 && existingViewModel.ApplicantPreviousEducation.EndYear.Id > 0))
                {
                    existingViewModel.PreviousEducationEndDaySelectList = Utility.PopulateDaySelectListItem(existingViewModel.ApplicantPreviousEducation.EndMonth, existingViewModel.ApplicantPreviousEducation.EndYear);
                }
                else
                {
                    if (existingViewModel.PreviousEducationEndDaySelectList != null && existingViewModel.ApplicantPreviousEducation.EndDay.Id > 0)
                    {
                        ViewBag.PreviousEducationEndDayId = new SelectList(existingViewModel.PreviousEducationEndDaySelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.EndDay.Id);
                    }
                    else if (existingViewModel.PreviousEducationEndDaySelectList != null && existingViewModel.ApplicantPreviousEducation.EndDay.Id <= 0)
                    {
                        ViewBag.PreviousEducationEndDayId = existingViewModel.PreviousEducationEndDaySelectList;
                    }
                    else if (existingViewModel.PreviousEducationEndDaySelectList == null)
                    {
                        existingViewModel.PreviousEducationEndDaySelectList = new List<SelectListItem>();
                        ViewBag.PreviousEducationEndDayId = new List<SelectListItem>();
                    }
                }

                if (existingViewModel.ApplicantPreviousEducation.EndDay != null && existingViewModel.ApplicantPreviousEducation.EndDay.Id > 0)
                {
                    ViewBag.PreviousEducationEndDayId = new SelectList(existingViewModel.PreviousEducationEndDaySelectList, VALUE, TEXT, existingViewModel.ApplicantPreviousEducation.EndDay.Id);
                }
                else
                {
                    ViewBag.PreviousEducationEndDayId = existingViewModel.PreviousEducationEndDaySelectList;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SetDefaultSelectedSittingSubjectAndGrade(FormViewModel viewModel)
        {
            try
            {
                if (viewModel != null && viewModel.FirstSittingOLevelResultDetails != null)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        ViewData["FirstSittingOLevelSubjectId" + i] = viewModel.OLevelSubjectSelectList;
                        ViewData["FirstSittingOLevelGradeId" + i] = viewModel.OLevelGradeSelectList;
                    }
                }

                if (viewModel != null && viewModel.SecondSittingOLevelResultDetails != null)
                {
                    for (int i = 0; i < 9; i++)
                    {
                        ViewData["SecondSittingOLevelSubjectId" + i] = viewModel.OLevelSubjectSelectList;
                        ViewData["SecondSittingOLevelGradeId" + i] = viewModel.OLevelGradeSelectList;
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetLgaIfExist(FormViewModel viewModel)
        {
            try
            {
                if (viewModel.Person.State != null && !string.IsNullOrEmpty(viewModel.Person.State.Id))
                {
                    LocalGovernmentLogic localGovernmentLogic = new LocalGovernmentLogic();
                    List<LocalGovernment> lgas = localGovernmentLogic.GetModelsBy(l => l.State_Id == viewModel.Person.State.Id);
                    if (viewModel.Person.LocalGovernment != null && viewModel.Person.LocalGovernment.Id > 0)
                    {
                        ViewBag.LgaId = new SelectList(lgas, ID, NAME, viewModel.Person.LocalGovernment.Id);
                    }
                    else
                    {
                        ViewBag.LgaId = new SelectList(lgas, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.LgaId = new SelectList(new List<LocalGovernment>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetDepartmentIfExist(FormViewModel viewModel)
        {
            try
            {
                if (viewModel.AppliedCourse.Programme != null && viewModel.AppliedCourse.Programme.Id > 0)
                {
                    ProgrammeDepartmentLogic departmentLogic = new ProgrammeDepartmentLogic();
                    List<Department> departments = departmentLogic.GetBy(viewModel.AppliedCourse.Programme);
                    if (viewModel.AppliedCourse.Department != null && viewModel.AppliedCourse.Department.Id > 0)
                    {
                        ViewBag.DepartmentId = new SelectList(departments, ID, NAME, viewModel.AppliedCourse.Department.Id);
                    }
                    else
                    {
                        ViewBag.DepartmentId = new SelectList(departments, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.DepartmentId = new SelectList(new List<LocalGovernment>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetDepartmentOptionIfExist(FormViewModel viewModel)
        {
            try
            {
                if (viewModel.AppliedCourse.Department != null && viewModel.AppliedCourse.Department.Id > 0)
                {
                    DepartmentOptionLogic departmentOptionLogic = new DepartmentOptionLogic();
                    List<DepartmentOption> departmentOptions = departmentOptionLogic.GetModelsBy(l => l.Department_Id == viewModel.AppliedCourse.Department.Id);
                    if (viewModel.AppliedCourse.Option != null && viewModel.AppliedCourse.Option.Id > 0)
                    {
                        ViewBag.DepartmentOptionId = new SelectList(departmentOptions, ID, NAME, viewModel.AppliedCourse.Option.Id);
                    }
                    else
                    {
                        ViewBag.DepartmentOptionId = new SelectList(departmentOptions, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.DepartmentOptionId = new SelectList(new List<LocalGovernment>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }
        
        private void SetPreviousEducationStartDate()
        {
            try
            {
                if (viewModel.ApplicantPreviousEducation != null && viewModel.ApplicantPreviousEducation.StartDate != null)
                {
                    int noOfDays = DateTime.DaysInMonth(viewModel.ApplicantPreviousEducation.StartYear.Id, viewModel.ApplicantPreviousEducation.StartMonth.Id);
                    List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
                    if (days != null && days.Count > 0)
                    {
                        days.Insert(0, new Value() { Name = "--DD--" });
                    }

                    if (viewModel.ApplicantPreviousEducation.StartDay != null && viewModel.ApplicantPreviousEducation.StartDay.Id > 0)
                    {
                        ViewBag.PreviousEducationStartDayId = new SelectList(days, ID, NAME, viewModel.ApplicantPreviousEducation.StartDay.Id);
                    }
                    else
                    {
                        ViewBag.PreviousEducationStartDayId = new SelectList(days, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.PreviousEducationStartDayId = new SelectList(new List<Value>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetPreviousEducationEndDate()
        {
            try
            {
                if (viewModel.ApplicantPreviousEducation != null && viewModel.ApplicantPreviousEducation.EndDate != null)
                {
                    int noOfDays = DateTime.DaysInMonth(viewModel.ApplicantPreviousEducation.EndYear.Id, viewModel.ApplicantPreviousEducation.EndMonth.Id);
                    List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
                    if (days != null && days.Count > 0)
                    {
                        days.Insert(0, new Value() { Name = "--DD--" });
                    }

                    if (viewModel.ApplicantPreviousEducation.EndDay != null && viewModel.ApplicantPreviousEducation.EndDay.Id > 0)
                    {
                        ViewBag.PreviousEducationEndDayId = new SelectList(days, ID, NAME, viewModel.ApplicantPreviousEducation.EndDay.Id);
                    }
                    else
                    {
                        ViewBag.PreviousEducationEndDayId = new SelectList(days, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.PreviousEducationEndDayId = new SelectList(new List<Value>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetDateOfBirth()
        {
            try
            {
                if (viewModel.Person.DateOfBirth.HasValue)
                {
                    int noOfDays = DateTime.DaysInMonth(viewModel.Person.YearOfBirth.Id, viewModel.Person.MonthOfBirth.Id);
                    List<Value> days = Utility.CreateNumberListFrom(1, noOfDays);
                    if (days != null && days.Count > 0)
                    {
                        days.Insert(0, new Value() { Name = "--DD--" });
                    }

                    if (viewModel.Person.DayOfBirth != null && viewModel.Person.DayOfBirth.Id > 0)
                    {
                        ViewBag.DayOfBirthId = new SelectList(days, ID, NAME, viewModel.Person.DayOfBirth.Id);
                    }
                    else
                    {
                        ViewBag.DayOfBirthId = new SelectList(days, ID, NAME);
                    }
                }
                else
                {
                    ViewBag.DayOfBirthId = new SelectList(new List<Value>(), ID, NAME);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        private void SetSelectedSittingSubjectAndGrade(FormViewModel existingViewModel)
        {
            try
            {
                if (existingViewModel != null && existingViewModel.FirstSittingOLevelResultDetails != null && existingViewModel.FirstSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail firstSittingOLevelResultDetail in existingViewModel.FirstSittingOLevelResultDetails)
                    {
                        if (firstSittingOLevelResultDetail.Subject != null && firstSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Subject.Id);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, firstSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["FirstSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["FirstSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
                        }

                        i++;
                    }
                }

                if (existingViewModel != null && existingViewModel.SecondSittingOLevelResultDetails != null && existingViewModel.SecondSittingOLevelResultDetails.Count > 0)
                {
                    int i = 0;
                    foreach (OLevelResultDetail secondSittingOLevelResultDetail in existingViewModel.SecondSittingOLevelResultDetails)
                    {
                        if (secondSittingOLevelResultDetail.Subject != null && secondSittingOLevelResultDetail.Grade != null)
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Subject.Id);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, secondSittingOLevelResultDetail.Grade.Id);
                        }
                        else
                        {
                            ViewData["SecondSittingOLevelSubjectId" + i] = new SelectList(existingViewModel.OLevelSubjectSelectList, VALUE, TEXT, 0);
                            ViewData["SecondSittingOLevelGradeId" + i] = new SelectList(existingViewModel.OLevelGradeSelectList, VALUE, TEXT, 0);
                        }

                        i++;
                    }
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
        }

        public JsonResult GetLocalGovernmentsByState(string id)
        {
            try
            {
                LocalGovernmentLogic lgaLogic = new LocalGovernmentLogic();
                List<LocalGovernment> lgas = lgaLogic.GetModelsBy(l => l.State_Id == id);

                return Json(new SelectList(lgas, "Id", "Name"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetDayOfBirthBy(string monthId, string yearId)
        {
            try
            {
                if (string.IsNullOrEmpty(monthId) || string.IsNullOrEmpty(yearId))
                {
                    return null;
                }

                Value month = new Value() { Id = Convert.ToInt32(monthId) };
                Value year = new Value() { Id = Convert.ToInt32(yearId) };
                List<Value> days = Utility.GetNumberOfDaysInMonth(month, year);

                return Json(new SelectList(days, ID, NAME), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };

                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);

                return Json(new SelectList(departments, ID, NAME), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

               

       



	}

}