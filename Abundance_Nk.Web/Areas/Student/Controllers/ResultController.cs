﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Web.Areas.Student.ViewModels;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Models;

namespace Abundance_Nk.Web.Areas.Student.Controllers
{
    [AllowAnonymous]
    public class ResultController : BaseController
    {
        private ResultViewModel viewModel;
        private StudentLogic studentLogic;
        private StudentLevelLogic studentLevelLogic;
        public ResultController()
        {
            viewModel = new ResultViewModel();
            studentLogic = new StudentLogic();
        }

        public ActionResult Check()
        {
            viewModel = new ResultViewModel();
            ViewBag.Session = viewModel.SessionSelectList;
            ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Check(ResultViewModel vModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Model.Model.Student student = studentLogic.GetBy(vModel.MatricNumber);
                    //if (student != null && student.Id > 0)
                    //{
                    //    SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                    //    SessionSemester sessionSemester = sessionSemesterLogic.GetModelBy(ss => ss.Session_Id == vModel.Session.Id && ss.Semester_Id == vModel.Semester.Id);
                    //    if (sessionSemester == null)
                    //    {
                    //        SetMessage("Invalid SessionSemester!", Message.Category.Error);
                    //        ViewBag.Session = viewModel.SessionSelectList;
                    //        ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
                    //        return View(vModel);
                    //    }

                    //    return RedirectToAction("Statement", new { sid = Utility.Encrypt(student.Id.ToString()), sessionSemesterId = sessionSemester.Id });
                    //}
                    //else
                    //{
                    //    SetMessage("Invalid Matric Number!", Message.Category.Error);
                    //} 
                    SetMessage("No Result to display!", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            ViewBag.Session = viewModel.SessionSelectList;
            ViewBag.Semester = new SelectList(new List<Semester>(), "Id", "Name");
            return View(vModel);
        }

        public ActionResult Statement(string sid, int sessionSemesterId)
        {
            long Id = Convert.ToInt64(Utility.Decrypt(sid));
            ViewBag.StudentId = Id;
            ViewBag.SessionSemesterId = sessionSemesterId;

            return View();
        }
        public JsonResult GetSemester(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Session session = new Session() { Id = Convert.ToInt32(id) };
                SemesterLogic semesterLogic = new SemesterLogic();
                List<SessionSemester> sessionSemesterList = new List<SessionSemester>();
                SessionSemesterLogic sessionSemesterLogic = new SessionSemesterLogic();
                sessionSemesterList = sessionSemesterLogic.GetModelsBy(p => p.Session_Id == session.Id);

                List<Semester> semesters = new List<Semester>();
                foreach (SessionSemester sessionSemester in sessionSemesterList)
                {
                    semesters.Add(sessionSemester.Semester);
                }

                return Json(new SelectList(semesters, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}