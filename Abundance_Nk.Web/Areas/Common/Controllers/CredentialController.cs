﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;

namespace Abundance_Nk.Web.Areas.Common.Controllers
{
    [AllowAnonymous]
    public class CredentialController : BaseController
    {
        public ActionResult ApplicationForm(long fid)
        {
            try
            {
                ApplicationFormViewModel applicationFormViewModel = new ApplicationFormViewModel();
                ApplicationForm form = new ApplicationForm() { Id = fid };

                applicationFormViewModel.GetApplicationFormBy(form);
                if (applicationFormViewModel.Person != null && applicationFormViewModel.Person.Id > 0)
                {
                    applicationFormViewModel.SetApplicantAppliedCourse(applicationFormViewModel.Person);
                }

                return View(applicationFormViewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult StudentForm(string fid)
        {
            try
            {
                Int64 formId = Convert.ToInt64(Abundance_Nk.Web.Models.Utility.Decrypt(fid));

                ApplicationForm form = new ApplicationForm() { Id = formId };
                StudentFormViewModel studentFormViewModel = new StudentFormViewModel();

                studentFormViewModel.LoadApplicantionFormBy(formId);
                if (studentFormViewModel.ApplicationForm.Person != null && studentFormViewModel.ApplicationForm.Person.Id > 0)
                {
                    studentFormViewModel.LoadStudentInformationFormBy(studentFormViewModel.ApplicationForm.Person.Id);
                }

                return View(studentFormViewModel);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public ActionResult Receipt(long pmid)
        {
            Receipt receipt = null;
            try
            {
                receipt = GetReceiptBy(pmid);
                if (receipt == null)
                {
                    SetMessage("No receipt found!", Message.Category.Error);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(receipt);
        }

        public bool ValidatePayment(string pmid, string pid)
        {

            PaymentLogic paymentLogic = new PaymentLogic();
            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();

            try
            {
                Payment payment = paymentLogic.GetBy(pmid);
                if (payment == null || payment.Id <= 0)
                {
                    return false;
                }

                PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetModelBy(o => o.Payment_Id == payment.Id);
                if (paymentEtranzact != null)
                {
                    if (payment.FeeDetails == null || payment.FeeDetails.Count <= 0)
                    {
                        throw new Exception("Fee Details for " + payment.FeeType.Name + " not set! Please contact your system administrator.");
                    }

                    if (payment.FeeType.Id != (int)FeeTypes.AcceptanceFee)
                    {
                        if(payment.FeeType.Id != (int)FeeTypes.SupplementaryAcceptanceFee)
                        {
                            return false;
                        }
                    }
                    if (payment.Person.Id.ToString() != pid)
                    {
                        return false;
                    }
                    return true;
                }

                RemitaPayment remitaPayment = new RemitaPayment();
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                if (remitaPayment != null && remitaPayment.Status.Contains("01:"))
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return false;

        }

        public Receipt GetReceiptBy(long pmid)
        {
            Receipt receipt = null;
            PaymentLogic paymentLogic = new PaymentLogic();
            PaymentEtranzactLogic paymentEtranzactLogic = new PaymentEtranzactLogic();

            try
            {
                Payment payment = paymentLogic.GetBy(pmid);
                if (payment == null || payment.Id <= 0)
                {
                    return null;
                }

                PaymentEtranzact paymentEtranzact = paymentEtranzactLogic.GetModelBy(o => o.Payment_Id == payment.Id);
                if (paymentEtranzact != null)
                {
                    if (payment.FeeDetails == null || payment.FeeDetails.Count <= 0)
                    {
                        throw new Exception("Fee Details for " + payment.FeeType.Name + " not set! Please contact your system administrator.");
                    }

                    decimal amount = payment.FeeDetails.Sum(p => p.Fee.Amount);
                    receipt = BuildReceipt(payment.Person.FullName, payment.InvoiceNumber, paymentEtranzact, amount, payment.FeeType.Name);
                }

                return receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Receipt BuildReceipt(string name, string invoiceNumber, PaymentEtranzact paymentEtranzact, decimal amount, string purpose)
        {
            try
            {
                Receipt receipt = new Receipt();
                receipt.Number = paymentEtranzact.ReceiptNo;
                receipt.Name = name;
                receipt.ConfirmationOrderNumber = paymentEtranzact.ConfirmationNo;
                receipt.Amount = amount;
                receipt.AmountInWords = "";
                receipt.Purpose = purpose;
                receipt.Date = DateTime.Now;

                return receipt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult AdmissionLetter(string fid)
        {
            AdmissionLetter admissionLetter = null;
            Int64 formId = Convert.ToInt64(Abundance_Nk.Web.Models.Utility.Decrypt(fid));
            
            try
            {
                admissionLetter = GetAdmissionLetterBy(formId);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(admissionLetter);
        }

        public ActionResult AcceptanceLetter(string fid, string pmid)
        {
            AdmissionLetter admissionLetter = null;

            Int64 formId = Convert.ToInt64(Abundance_Nk.Web.Models.Utility.Decrypt(fid));
            string paymentId = Abundance_Nk.Web.Models.Utility.Decrypt(pmid).ToString();
            ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
            ApplicationForm applicationForm = applicationFormLogic.GetBy(formId);
           
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                Payment payment = paymentLogic.GetBy(paymentId);
                if (ValidatePayment(paymentId, applicationForm.Person.Id.ToString()))
                {
                    admissionLetter = GetAcceptanceLetterBy(formId);
                    admissionLetter.Payment = payment;
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return View(admissionLetter);
        }

        public AdmissionLetter GetAdmissionLetterBy(long formId)
        {
            try
            {
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                AdmissionLetter admissionLetter = null;
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                ApplicationForm applicationForm = applicationFormLogic.GetBy(formId);
                AdmissionList admissionList = new AdmissionList();
                admissionList = admissionListLogic.GetBy(formId);
                if (applicationForm != null && applicationForm.Id > 0)
                {
                    FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                    List<FeeDetail> feeDetails = feeDetailLogic.GetModelsBy(f => f.Fee_Type_Id == (int)FeeTypes.SchoolFees);

                    
                    admissionLetter = new AdmissionLetter();
                    admissionLetter.applicationform = applicationForm;
                    admissionLetter.Person = applicationForm.Person;
                    admissionLetter.Session = applicationForm.Setting.Session;
                    admissionLetter.FeeDetails = feeDetails;
                    admissionLetter.AdmissionList = admissionList;
                    admissionLetter.Programme = admissionList.Programme;
                    admissionLetter.Department = admissionList.Deprtment;
                    admissionLetter.RegistrationEndDate = applicationForm.Setting.RegistrationEndDate;
                    admissionLetter.RegistrationEndTime = applicationForm.Setting.RegistrationEndTime;
                    admissionLetter.RegistrationEndTimeString = applicationForm.Setting.RegistrationEndTimeString;

                    if (admissionLetter.Session == null || admissionLetter.Session.Id <= 0)
                    {
                        throw new Exception("Session not set for this admission period! Please contact your system administrator.");
                    }
                    else if (!admissionLetter.RegistrationEndDate.HasValue)
                    {
                        throw new Exception("Registration End Date not set for this admission period! Please contact your system administrator.");
                    }
                    else if (!admissionLetter.RegistrationEndTime.HasValue)
                    {
                        throw new Exception("Registration End Time not set for this admission period! Please contact your system administrator.");
                    }

               
                }

                return admissionLetter;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult AdmissionSlip(long fid)
        {
            AdmissionLetter admissionLetter = null;

            try
            {
                admissionLetter = GetAdmissionLetterBy(fid);
            }
            catch(Exception)
            {
                throw;
            }

            return View(admissionLetter);
        }
        public AdmissionLetter GetAcceptanceLetterBy(long formId)
        {
            try
            {
                AdmissionLetter admissionLetter = null;
                ApplicationFormLogic applicationFormLogic = new ApplicationFormLogic();
                AdmissionListLogic admissionListLogic = new AdmissionListLogic();
                ApplicationForm applicationForm = applicationFormLogic.GetBy(formId);
                AdmissionList admissionList = new AdmissionList();
                admissionList = admissionListLogic.GetBy(formId);
                if (applicationForm != null && applicationForm.Id > 0)
                {
                    FeeDetailLogic feeDetailLogic = new FeeDetailLogic();
                    List<FeeDetail> feeDetails = feeDetailLogic.GetModelsBy(f => f.Fee_Type_Id == (int)FeeTypes.SchoolFees);

                    
                    admissionLetter = new AdmissionLetter();
                    admissionLetter.applicationform = applicationForm;
                    admissionLetter.Person = applicationForm.Person;
                    admissionLetter.Session = applicationForm.Setting.Session;
                    admissionLetter.FeeDetails = feeDetails;
                    admissionLetter.Programme = admissionList.Programme;
                    admissionLetter.Department = admissionList.Deprtment;
                    admissionLetter.RegistrationEndDate = applicationForm.Setting.RegistrationEndDate;
                    admissionLetter.RegistrationEndTime = applicationForm.Setting.RegistrationEndTime;
                    admissionLetter.RegistrationEndTimeString = applicationForm.Setting.RegistrationEndTimeString;

                    if (admissionLetter.Session == null || admissionLetter.Session.Id <= 0)
                    {
                        throw new Exception("Session not set for this admission period! Please contact your system administrator.");
                    }
                    else if (!admissionLetter.RegistrationEndDate.HasValue)
                    {
                        throw new Exception("Registration End Date not set for this admission period! Please contact your system administrator.");
                    }
                    else if (!admissionLetter.RegistrationEndTime.HasValue)
                    {
                        throw new Exception("Registration End Time not set for this admission period! Please contact your system administrator.");
                    }


                }

                return admissionLetter;
            }
            catch (Exception)
            {
                throw;
            }
        }
     
        public ActionResult FinancialClearanceSlip(long pid)
        {
            try
            {
                StudentLogic studentLogic = new StudentLogic();
                Model.Model.Student student = studentLogic.GetBy(pid);
                                

                PaymentLogic paymentLogic = new PaymentLogic();
                PaymentHistory paymentHistory = new PaymentHistory();
                paymentHistory.Payments = paymentLogic.GetBy(student);
                paymentHistory.Student = student;

                return View(paymentHistory);
            }
            catch(Exception)
            {
                throw;
            }
        }

        public ActionResult Invoice(long pmid)
        {
            try
            {
                PaymentLogic paymentLogic = new PaymentLogic();
                Payment payment = paymentLogic.GetBy(pmid);
                Invoice invoice = new Invoice();
                invoice.Person = payment.Person;
                invoice.Payment = payment;

                return View(invoice);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult TranscriptInvoice(string pmid)
        {
            try
            {
                string type = Convert.ToString(TempData["RequestType"]);
                if (type == "")
                {
                    type = null;
                }
                TempData.Keep("RequestType");

                int paymentid = Convert.ToInt32(pmid);
                PaymentLogic paymentLogic = new PaymentLogic();
                Payment payment = paymentLogic.GetBy(paymentid);

                Invoice invoice = new Invoice();
                invoice.Person = payment.Person;
                invoice.Payment = payment;

                if (type == null || type == "Transcript Verification")
                {
                    invoice.Payment.FeeType.Name = "Transcript";
                }
                if (type == "Certificate Collection" || type == "Certificate Verification")
                {
                    invoice.Payment.FeeType.Name = "Certificate";
                }

                RemitaPayment remitaPayment = new RemitaPayment();
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                remitaPayment = remitaPaymentLogic.GetBy(payment.Id);
                if (remitaPayment != null)
                {
                    invoice.remitaPayment = remitaPayment;
                    invoice.Amount = remitaPayment.TransactionAmount;
                }

                TranscriptViewModel viewModel = new TranscriptViewModel();
                string hash = "538661740" + remitaPayment.RRR + "918567";
                RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(hash);
                viewModel.Hash = remitaProcessor.HashPaymentDetailToSHA512(hash);
                viewModel.RemitaPayment = remitaPayment;
                viewModel.RemitaPayment.MerchantCode = "538661740";
                viewModel.RemitaPayment.RRR = remitaPayment.RRR;
                TempData["TranscriptViewModel"] = viewModel;
                return View(invoice);
            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}