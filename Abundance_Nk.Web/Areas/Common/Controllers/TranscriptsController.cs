﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Entity;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Admin.ViewModels;
using Abundance_Nk.Web.Areas.Common.Models;
using Abundance_Nk.Web.Areas.Common.ViewModels;
using Abundance_Nk.Web.Controllers;
using BarcodeLib;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;

namespace Abundance_Nk.Web.Areas.Common.Controllers
{
    [AllowAnonymous]
    public class TranscriptsController : BaseController
    {
        // GET: Common/Transcripts
        private TranscriptViewModel _transcriptViewModel;
        private string appRoot = ConfigurationManager.AppSettings["AppRoot"];
        public TranscriptsController()
        {
            _transcriptViewModel = new TranscriptViewModel();
        }

        private void PopulateDropdown(TranscriptViewModel transcriptViewModel)
        {
            ViewBag.CountryId = transcriptViewModel.CountrySelectList;
            ViewBag.StateId = transcriptViewModel.StateSelectList;
            ViewBag.ProgrammeId = transcriptViewModel.ProgrammeSelectList;
            if (transcriptViewModel.StudentLevel != null && transcriptViewModel.StudentLevel.Programme != null && transcriptViewModel.StudentLevel.Programme.Id > 0 && transcriptViewModel.StudentLevel.Department != null)
            {
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(transcriptViewModel.StudentLevel.Programme);
                ViewBag.DepartmentId = new SelectList(departments, "Id", "Name", transcriptViewModel.StudentLevel.Department.Id);
            }
            else
            {
                ViewBag.DepartmentId = new SelectList(new List<Department>(), "Id", "Name");
            }
        }
        public ActionResult Index()
        {
            try
            {

            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(_transcriptViewModel);
        }
        [HttpPost]
        public ActionResult Index(TranscriptViewModel transcriptViewModel)
        {
            try
            {
                if (!String.IsNullOrEmpty(transcriptViewModel.TranscriptRequest.student.MatricNumber.Trim()))
                {
                    transcriptViewModel.InitialiseStudent();
                }

                if (transcriptViewModel.TranscriptRequests != null && transcriptViewModel.TranscriptRequests.Count > 0)
                {
                    return View(transcriptViewModel);
                }

            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }

            TempData["transcriptViewModel"] = transcriptViewModel;
            return RedirectToAction("TranscriptForm");
        }

        public ActionResult EditRequest(string MatricNumber, long TranscriptRequestId)
        {
            _transcriptViewModel.TranscriptRequest.student.MatricNumber = MatricNumber.Trim();
            _transcriptViewModel.InitialiseStudent(TranscriptRequestId);
            if (_transcriptViewModel.TranscriptRequest.Id > 0 &&
                !string.IsNullOrEmpty(_transcriptViewModel.TranscriptRequest.OnlinePayment.TransactionNumber))
            {

            }
            else
            {

            }
            TempData["transcriptViewModel"] = _transcriptViewModel;
            return RedirectToAction("TranscriptForm");

        }
        public ActionResult MakeRequest(string MatricNumber)
        {
            _transcriptViewModel.TranscriptRequest.student.MatricNumber = MatricNumber.Trim();
            _transcriptViewModel.InitialiseStudentNewRequest();
            TempData["transcriptViewModel"] = _transcriptViewModel;
            return RedirectToAction("TranscriptForm");

        }

        public ActionResult TranscriptForm()
        {
            TranscriptViewModel transcriptViewModel = (TranscriptViewModel)TempData["transcriptViewModel"];
            try
            {

                if (transcriptViewModel == null || string.IsNullOrEmpty(transcriptViewModel.TranscriptRequest.student.MatricNumber))
                {
                    return RedirectToAction("Index");
                }

                PopulateDropdown(transcriptViewModel);
            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(transcriptViewModel);
        }

        [HttpPost]
        public ActionResult TranscriptForm(TranscriptViewModel transcriptViewModel)
        {
            TranscriptViewModel initialViewModel = transcriptViewModel;
            try
            {
                if (transcriptViewModel == null || string.IsNullOrEmpty(transcriptViewModel.TranscriptRequest.student.MatricNumber))
                {
                    return RedirectToAction("Index");
                }
                if (!string.IsNullOrEmpty(transcriptViewModel.TranscriptRequest.AlumniReceiptUrl) && !string.IsNullOrEmpty(transcriptViewModel.TranscriptRequest.StatementOfResultUrl))
                {
                    using (var transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        if (transcriptViewModel.Student == null && transcriptViewModel.TranscriptRequest.student.Id <= 0)
                        {
                            transcriptViewModel.CreateStudent();
                        }

                        transcriptViewModel.ProcessTranscriptRequest();
                        //ProcessRemitaPayment(transcriptViewModel.TranscriptRequest.Id);
                        //ProcessRemitaPaymentSingle(transcriptViewModel.TranscriptRequest.Id);
                        transaction.Complete();
                        //return RedirectToAction("TranscriptInvoice",new {TranscriptRequestId = transcriptViewModel.TranscriptRequest.Id});
                        return RedirectToAction("ProcessRemitaPaymentSingle", new { TranscriptRequestId = transcriptViewModel.TranscriptRequest.Id });

                    }
                }
                else
                {
                    SetMessage("You must upload a scanned copy of both Alumni Receipt and Statement of Result", Message.Category.Error);
                }


            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            if (initialViewModel.TranscriptRequest != null && initialViewModel.TranscriptRequest.student == null)
            {
                initialViewModel.TranscriptRequest.student = initialViewModel.Student;
            }
            PopulateDropdown(initialViewModel);

            return View(initialViewModel);
        }

        public ActionResult TranscriptInvoice(long TranscriptRequestId = 0)
        {
            try
            {
                if (TranscriptRequestId <= 0)
                {
                    return RedirectToAction("Index");
                }
                _transcriptViewModel.GetRequestById(TranscriptRequestId);
                _transcriptViewModel.RemitaHash = GenerateHash("587460", _transcriptViewModel.TranscriptRequest.remitaPayment);
                _transcriptViewModel.RemitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(_transcriptViewModel);
        }

        [ValidateInput(false)]
        public ActionResult Listen(string orderID)
        {
            try
            {
                _transcriptViewModel.InterswitchResponse = new InterswitchResponse();
                _transcriptViewModel.PaymentInterswitch = new PaymentInterswitch();

                if (Request.QueryString["orderID"] != null)
                {
                    orderID = Request.QueryString["orderID"].ToString().Trim();
                    RemitaSettings settings = new RemitaSettings();
                    RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                    settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 3);
                    RemitaPayment remitaPayment = new RemitaPayment();
                    RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                    remitaPayment = remitaPaymentLogic.GetBy(orderID);
                    string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"];
                    RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
                    RemitaResponse remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
                    if (remitaResponse != null && remitaResponse.Status != null)
                    {
                        remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                        _transcriptViewModel.PaymentInterswitch.ResponseDescription = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                        remitaPaymentLogic.Modify(remitaPayment);
                        if (remitaResponse.StatusCode.Contains("01"))
                        {
                            _transcriptViewModel.UpdateTranscriptPayment(remitaPayment.payment.InvoiceNumber, remitaPayment.RRR);
                        }

                        return View(_transcriptViewModel);
                    }

                    _transcriptViewModel.PaymentInterswitch.ResponseDescription = "Order ID was not generated from this system";

                }
                else
                {
                    _transcriptViewModel.PaymentInterswitch.ResponseDescription = "No data was received!";
                }

            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(_transcriptViewModel);
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Listen(InterswitchResponse interswitchResponse)
        {
            try
            {
                var resolveRequest = HttpContext.Request;
                resolveRequest.InputStream.Seek(0, SeekOrigin.Begin);
                string jsondata = new StreamReader(resolveRequest.InputStream).ReadToEnd();
                List<RemitaResponse> remitaResponse = new JavaScriptSerializer().Deserialize<List<RemitaResponse>>(jsondata);
                RemitaPayment remitaPayment = new RemitaPayment();
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                using (StreamWriter w = new StreamWriter(@"C:\Users\abundance\Documents\transcriptremita.txt"))
                {
                    w.WriteLine("RRR: " + remitaResponse[0].rrr + " Order_Id:" + remitaResponse[0].orderRef);
                }
                if (remitaResponse != null && remitaResponse.Count > 0)
                {
                    foreach (RemitaResponse response in remitaResponse)
                    {

                        RemitaSettings settings = new RemitaSettings();
                        RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();
                        settings = settingsLogic.GetModelBy(s => s.Payment_SettingId == 3);

                        remitaPayment = remitaPaymentLogic.GetModelBy(m => m.OrderId == response.orderRef);

                        if (remitaResponse != null && response.responseCode != null && remitaPayment != null)
                        {
                            remitaPayment.Description = response.Message;
                            remitaPayment.RRR = response.rrr;
                            remitaPayment.Status = response.responseCode + ":" + response.StatusCode;
                            if (response.bank != null)
                            {
                                remitaPayment.BankCode = response.bank;
                                remitaPayment.Status = "01:approved";

                                _transcriptViewModel.UpdateTranscriptPayment(remitaPayment.payment.InvoiceNumber, remitaPayment.RRR);

                            }
                            if (response.RemitaDetails != null)
                            {
                                remitaPayment.CustomerName = remitaResponse[0].RemitaDetails.payerName;
                            }
                            if (response.amount > 0)
                            {
                                remitaPayment.TransactionAmount = response.amount;
                            }

                            remitaPaymentLogic.Modify(remitaPayment);

                        }
                        return Json(remitaResponse);

                    }
                    remitaResponse[0].Message = "Order ID was not generated from this system";

                }
                else
                {
                    remitaResponse = null;
                    remitaResponse[0].Message = "No data received";
                }

            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(_transcriptViewModel);
        }

        public ActionResult Receipt(long TranscriptRequestId)
        {
            try
            {
                _transcriptViewModel.GetRequestById(TranscriptRequestId);
                BarcodeLib.Barcode barcode = new BarcodeLib.Barcode(_transcriptViewModel.TranscriptRequest.payment.InvoiceNumber, TYPE.CODE39);
                Image image = barcode.Encode(TYPE.CODE39, _transcriptViewModel.TranscriptRequest.payment.InvoiceNumber);
                byte[] imageByteData = imageToByteArray(image);
                string imageBase64Data = Convert.ToBase64String(imageByteData);
                string imageDataURL = string.Format("data:image/jpg;base64,{0}", imageBase64Data);
                _transcriptViewModel.BarcodeImageUrl = imageDataURL;

            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return View(_transcriptViewModel);
        }

        public ActionResult GetStatus(string TransactionReference)
        {
            try
            {
                RemitaPayementProcessor remitaPayementProcessor = new RemitaPayementProcessor("587460");
                RemitaPayment remita = remitaPayementProcessor.GetStatus(TransactionReference);

                if (remita != null && (remita.Status.Contains("00") || remita.Status.Contains("01")) && remita.payment != null && remita.payment.Id > 0)
                {
                    TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                    TranscriptRequest request = requestLogic.GetModelsBy(t => t.Payment_Id == remita.payment.Id).LastOrDefault();
                    if (request != null && request.transcriptStatus.TranscriptStatusId == 1)
                    {
                        _transcriptViewModel.UpdateTranscriptPayment(remita.payment.InvoiceNumber, remita.RRR);
                    }
                }

                SetMessage("Task Completed.", Message.Category.Information);

                if (User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Payments");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                SetMessage("The following error Occurred" + ex.Message, Message.Category.Error);
            }
            return RedirectToAction("Index");
        }

        public ActionResult ProcessRemitaPayment(long TranscriptRequestId)
        {

            try
            {
                TranscriptRequestLogic transcriptRequestLogic = new TranscriptRequestLogic();
                _transcriptViewModel.GetRequestById(TranscriptRequestId);
                if (_transcriptViewModel.TranscriptRequest != null)
                {
                    using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
                    {
                        if (_transcriptViewModel.TranscriptRequest.payment != null && _transcriptViewModel.TranscriptRequest.remitaPayment == null)
                        {
                            RemitaSettings settings = new RemitaSettings();
                            RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();

                            settings = settingsLogic.GetBy(3);
                            decimal Amt = 0;
                            Amt = _transcriptViewModel.TranscriptRequest.payment.FeeDetails.Sum(a => a.Fee.Amount);

                            //Get Split Specific details;
                            List<RemitaSplitItems> splitItems = new List<RemitaSplitItems>();
                            RemitaSplitItems singleItem = new RemitaSplitItems();
                            RemitaSplitItemLogic splitItemLogic = new RemitaSplitItemLogic();

                            singleItem = splitItemLogic.GetBy(2);
                            singleItem.deductFeeFrom = "1";
                            singleItem.beneficiaryAmount = "0";
                            singleItem.lineItemsId = "5";
                            splitItems.Add(singleItem);
                            singleItem = splitItemLogic.GetBy(1);
                            singleItem.deductFeeFrom = "0";
                            singleItem.lineItemsId = "1";
                            singleItem.beneficiaryAmount = Convert.ToString(Amt);
                            splitItems.Add(singleItem);


                            //Get BaseURL
                            string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();
                            RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(settings.Api_key);
                            RemitaPayment remitaPayment = remitaProcessor.GenerateRRR(_transcriptViewModel.TranscriptRequest.payment.InvoiceNumber, remitaBaseUrl, "TRANSCRIPT", splitItems, settings, Amt);

                            if (remitaPayment != null)
                            {
                                transaction.Complete();
                            }
                            else
                            {
                                transaction.Dispose();
                                SetMessage("Error Occurred! Remita Response: 'Invalid Request' ", Message.Category.Error);
                                return RedirectToAction("Index");
                            }


                            _transcriptViewModel.RemitaHash = GenerateHash(settings.Api_key, remitaPayment);
                            _transcriptViewModel.RemitaBaseUrl = remitaBaseUrl;
                            _transcriptViewModel.RemitaPayment = remitaPayment;
                            TempData["TranscriptViewModel"] = _transcriptViewModel;

                        }
                    }

                    return RedirectToAction("TranscriptInvoice", new { TranscriptRequestId = _transcriptViewModel.TranscriptRequest.Id });

                }

            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return RedirectToAction("Index");
        }
        public ActionResult ProcessRemitaPaymentSingle(long TranscriptRequestId)
        {
            try
            {
                _transcriptViewModel.GetRequestById(TranscriptRequestId);
                if (_transcriptViewModel.TranscriptRequest != null)
                {
                    if (_transcriptViewModel.TranscriptRequest.payment != null && _transcriptViewModel.TranscriptRequest.remitaPayment == null)
                    {
                        RemitaSettings settings = new RemitaSettings();
                        RemitaSettingsLogic settingsLogic = new RemitaSettingsLogic();

                        settings = settingsLogic.GetBy(3);
                        decimal Amt = 0;
                        Amt = _transcriptViewModel.TranscriptRequest.payment.FeeDetails.Sum(a => a.Fee.Amount);

                        //Get BaseURL
                        string remitaBaseUrl = ConfigurationManager.AppSettings["RemitaBaseUrl"].ToString();

                        Remita remita = new Remita()
                        {
                            merchantId = settings.MarchantId,
                            serviceTypeId = settings.serviceTypeId,
                            orderId = _transcriptViewModel.TranscriptRequest.payment.InvoiceNumber.ToString(),
                            totalAmount = (decimal)Amt,
                            payerName = _transcriptViewModel.TranscriptRequest.student.FullName,
                            payerEmail = !string.IsNullOrEmpty(_transcriptViewModel.TranscriptRequest.student.Email) ? _transcriptViewModel.TranscriptRequest.student.Email : "test@lloydant.com",
                            payerPhone = _transcriptViewModel.TranscriptRequest.student.MobilePhone,
                            responseurl = "http://ndapplication.fpno.edu.ng/Common/Transcripts/RemitaResponse",
                            paymenttype = "RRRGEN",
                            amt = Amt.ToString()
                        };

                        remita.amt = remita.amt.Split('.')[0];

                        string hash_string = remita.merchantId + remita.serviceTypeId + remita.orderId + remita.amt + remita.responseurl + settings.Api_key;
                        System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                        Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                        sha512.Clear();
                        string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();

                        _transcriptViewModel.RemitaHash = hashed;
                        _transcriptViewModel.RemitaBaseUrl = remitaBaseUrl;
                        _transcriptViewModel.Remita = remita;
                        TempData["TranscriptViewModel"] = _transcriptViewModel;
                    }

                    return View(_transcriptViewModel);
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }

            return RedirectToAction("Index");
        }
        [AllowAnonymous]
        public ActionResult RemitaResponse(string orderID)
        {
            RemitaResponse remitaResponse = new RemitaResponse();
            TranscriptViewModel viewModel = new TranscriptViewModel();

            try
            {
                string merchant_id = "532776942";
                string apiKey = "587460";
                string hashed;
                string checkstatusurl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                string url;

                if (Request.QueryString["orderID"] != null)
                {
                    orderID = Request.QueryString["orderID"].ToString();

                    PaymentLogic paymentLogic = new PaymentLogic();
                    RemitaPaymentLogic remitaLogic = new RemitaPaymentLogic();

                    Payment payment = paymentLogic.GetBy(orderID);
                    RemitaPayment remitaPyament = payment != null ? remitaLogic.GetModelBy(p => p.Payment_Id == payment.Id) : null;

                    string hash_string = orderID + apiKey + merchant_id;
                    System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                    Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hash_string));
                    sha512.Clear();
                    hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
                    url = checkstatusurl + "/" + merchant_id + "/" + orderID + "/" + hashed + "/" + "orderstatus.reg";

                    if (remitaPyament == null)
                    {
                        string jsondata = new WebClient().DownloadString(url);
                        remitaResponse = JsonConvert.DeserializeObject<RemitaResponse>(jsondata);
                    }
                    
                    if (remitaResponse != null && remitaResponse.Status != null)
                    {
                        TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();

                        if (payment != null)
                        {
                            if (remitaPyament != null)
                            {
                                remitaPyament.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                                remitaLogic.Modify(remitaPyament);
                            }
                            else
                            {
                                remitaPyament = new RemitaPayment();
                                remitaPyament.payment = payment;
                                remitaPyament.RRR = remitaResponse.rrr;
                                remitaPyament.OrderId = remitaResponse.orderId;
                                remitaPyament.Status = remitaResponse.Status;
                                remitaPyament.TransactionAmount = remitaResponse.amount;
                                remitaPyament.TransactionDate = DateTime.Now;
                                remitaPyament.MerchantCode = merchant_id;
                                remitaPyament.Description = "TRANSCRIPT";
                                if (remitaLogic.GetBy(payment.Id) == null)
                                {
                                    remitaLogic.Create(remitaPyament);
                                }
                            }

                            viewModel.RemitaPayment = remitaPyament;
                            viewModel.TranscriptRequest = requestLogic.GetModelBy(t => t.Payment_Id == payment.Id);
                            if (viewModel.TranscriptRequest != null)
                            {
                                viewModel.TranscriptRequest.payment = payment;
                                viewModel.TranscriptRequest.remitaPayment = remitaPyament;
                            }
                        }
                        else
                        {
                            RemitaPayment remitaPayment = remitaLogic.GetBy(orderID);
                            string remitaVerifyUrl = ConfigurationManager.AppSettings["RemitaVerifyUrl"].ToString();
                            RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(apiKey);
                            remitaResponse = remitaProcessor.TransactionStatus(remitaVerifyUrl, remitaPayment);
                            if (remitaResponse != null && remitaResponse.Status != null)
                            {
                                remitaPayment.Status = remitaResponse.Status + ":" + remitaResponse.StatusCode;
                                remitaLogic.Modify(remitaPayment);
                                
                                viewModel.TranscriptRequest = requestLogic.GetModelBy(t => t.Payment_Id == payment.Id);
                                if (viewModel.TranscriptRequest != null)
                                {
                                    viewModel.TranscriptRequest.remitaPayment = remitaPayment;
                                }
                            }
                            else
                            {
                                SetMessage("Payment does not exist!", Message.Category.Error);
                            }

                            viewModel.RemitaPayment = remitaPayment;
                        }
                    }
                    else
                    {
                        SetMessage("Order ID was not generated from this system", Message.Category.Error);
                    }
                }
                else
                {
                    remitaResponse.Message = "No data was received!";
                }
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
            }

            return View(viewModel);
        }

        private string GenerateHash(string apiKey, RemitaPayment remitaPayment)
        {
            try
            {
                string hash = remitaPayment.MerchantCode + remitaPayment.RRR + apiKey;
                RemitaPayementProcessor remitaProcessor = new RemitaPayementProcessor(hash);
                string hashConcatenate = remitaProcessor.HashPaymentDetailToSHA512(hash);
                return hashConcatenate;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            return ms.ToArray();
        }
        [HttpPost]
        public virtual ActionResult UploadAlumniCredential(FormCollection form)
        {
            HttpPostedFileBase file = Request.Files["TranscriptRequest.MyFile"];

            bool isUploaded = false;
            string personId = form["TranscriptRequest.student.MatricNumber"].Replace("/", "").Replace("\\", "").Replace("-", "").ToString();
            string passportUrl = form["TranscriptRequest.AlumniReceiptUrl"].ToString();
            string message = "File upload failed";

            string path = null;
            string imageUrl = null;
            string imageUrlDisplay = null;

            try
            {
                if (file != null && file.ContentLength != 0)
                {
                    FileInfo fileInfo = new FileInfo(file.FileName);
                    string fileExtension = fileInfo.Extension;
                    string newFile = personId + "__alumni_receipt__";
                    string newFileName = newFile + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + fileExtension;

                    decimal sizeAllowed = 200; //200kb
                    string invalidFileMessage = InvalidFile(file.ContentLength, fileExtension, sizeAllowed);
                    if (!string.IsNullOrEmpty(invalidFileMessage))
                    {
                        isUploaded = false;
                        TempData["imageUrl"] = null;
                        return Json(new { isUploaded = isUploaded, message = invalidFileMessage, imageUrl = passportUrl }, "text/html", JsonRequestBehavior.AllowGet);
                    }

                    string pathForSaving = Server.MapPath("~/Content/Junk");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        DeleteFileIfExist(pathForSaving, newFileName);

                        file.SaveAs(Path.Combine(pathForSaving, newFileName));

                        isUploaded = true;
                        message = "File uploaded successfully!";

                        path = Path.Combine(pathForSaving, newFileName);
                        if (path != null)
                        {


                            imageUrl = "/Content/Junk/" + newFileName;
                            imageUrlDisplay = appRoot + imageUrl + "?t=" + DateTime.Now;
                            TempData["imageUrl"] = imageUrl;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = string.Format("File upload failed: {0}", ex.Message);
            }

            return Json(new { isUploaded = isUploaded, message = message, imageUrl = imageUrl }, "text/html", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual ActionResult UploadStatementCredential(FormCollection form)
        {
            HttpPostedFileBase file = Request.Files["TranscriptRequest.MyFile"];

            bool isUploaded = false;
            string personId = form["TranscriptRequest.student.MatricNumber"].Replace("/", "").Replace("\\", "").Replace("-", "").ToString();
            string passportUrl = form["TranscriptRequest.StatementOfResultUrl"].ToString();
            string message = "File upload failed";

            string path = null;
            string imageUrl = null;
            string imageUrlDisplay = null;

            try
            {
                if (file != null && file.ContentLength != 0)
                {
                    FileInfo fileInfo = new FileInfo(file.FileName);
                    string fileExtension = fileInfo.Extension;
                    string newFile = personId + "__statement_result__";
                    string newFileName = newFile + DateTime.Now.ToString().Replace("/", "").Replace(":", "").Replace(" ", "") + fileExtension;

                    decimal sizeAllowed = 200; //200kb
                    string invalidFileMessage = InvalidFile(file.ContentLength, fileExtension, sizeAllowed);
                    if (!string.IsNullOrEmpty(invalidFileMessage))
                    {
                        isUploaded = false;
                        TempData["imageUrl"] = null;
                        return Json(new { isUploaded = isUploaded, message = invalidFileMessage, imageUrl = passportUrl }, "text/html", JsonRequestBehavior.AllowGet);
                    }

                    string pathForSaving = Server.MapPath("~/Content/Junk");
                    if (this.CreateFolderIfNeeded(pathForSaving))
                    {
                        DeleteFileIfExist(pathForSaving, newFileName);

                        file.SaveAs(Path.Combine(pathForSaving, newFileName));

                        isUploaded = true;
                        message = "File uploaded successfully!";

                        path = Path.Combine(pathForSaving, newFileName);
                        if (path != null)
                        {


                            imageUrl = "/Content/Junk/" + newFileName;
                            imageUrlDisplay = appRoot + imageUrl + "?t=" + DateTime.Now;
                            TempData["imageUrl"] = imageUrl;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                message = string.Format("File upload failed: {0}", ex.Message);
            }

            return Json(new { isUploaded = isUploaded, message = message, imageUrl = imageUrl }, "text/html", JsonRequestBehavior.AllowGet);
        }

        private string InvalidFile(decimal uploadedFileSize, string fileExtension, decimal sizeAllowed)
        {
            try
            {
                string message = null;
                decimal oneKiloByte = 1024;
                decimal maximumFileSize = sizeAllowed * oneKiloByte;

                decimal actualFileSizeToUpload = Math.Round(uploadedFileSize / oneKiloByte, 1);
                if (InvalidFileType(fileExtension))
                {
                    message = "File type '" + fileExtension + "' is invalid! File type must be any of the following: .jpg, .jpeg, .png or .jif ";
                }
                else if (actualFileSizeToUpload > (maximumFileSize / oneKiloByte))
                {
                    message = "Your file size of " + actualFileSizeToUpload.ToString("0.#") + " Kb is too large, maximum allowed size is " + (maximumFileSize / oneKiloByte) + " Kb";
                }

                return message;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool InvalidFileType(string extension)
        {
            extension = extension.ToLower();
            switch (extension)
            {
                case ".jpg":
                    return false;
                case ".png":
                    return false;
                case ".jpeg":
                    return false;
                default:
                    return true;
            }
        }

        private void DeleteFileIfExist(string folderPath, string fileName)
        {
            try
            {
                string wildCard = fileName + "*.*";
                IEnumerable<string> files = Directory.EnumerateFiles(folderPath, wildCard, SearchOption.TopDirectoryOnly);

                if (files != null && files.Count() > 0)
                {
                    foreach (string file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private bool CreateFolderIfNeeded(string path)
        {
            try
            {
                bool result = true;
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch (Exception)
                    {
                        /*TODO: You must process this exception.*/
                        result = false;
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Payments()
        {
            try
            {
                RemitaPaymentLogic remitaPaymentLogic = new RemitaPaymentLogic();
                _transcriptViewModel.RemitaPayments = remitaPaymentLogic.GetModelsBy(a => a.PAYMENT.Fee_Type_Id == 11 || a.PAYMENT.Fee_Type_Id == 12);
            }
            catch (Exception)
            {

                throw;
            }
            return View(_transcriptViewModel);
        }

        public JsonResult GetDepartmentByProgrammeId(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    return null;
                }

                Programme programme = new Programme() { Id = Convert.ToInt32(id) };
                DepartmentLogic departmentLogic = new DepartmentLogic();
                List<Department> departments = departmentLogic.GetBy(programme);
                return Json(new SelectList(departments, "Id", "Name"), JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}