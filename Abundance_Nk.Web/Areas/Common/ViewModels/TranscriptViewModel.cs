﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Abundance_Nk.Business;
using Abundance_Nk.Model.Model;
using Abundance_Nk.Web.Areas.Common.Models;
using Abundance_Nk.Web.Models;
using RestSharp;
using  System.Configuration;
using RestSharp.Authenticators;

namespace Abundance_Nk.Web.Areas.Common.ViewModels
{
    public class TranscriptViewModel
    {
        private readonly StudentLogic _studentLogic;
        private readonly PersonLogic _personLogic;
        private readonly TranscriptRequestLogic _transcriptRequestLogic;
        private readonly PaymentLogic _paymentLogic;
        private readonly OnlinePaymentLogic _onlinePaymentLogic;
        private readonly PaymentInterswitchLogic _paymentInterswitchLogic;
        private readonly RemitaPaymentLogic _remitaPaymentLogic;
        private string MacKey ;
        private int ProductId;
        private int PaymentItemId ;
        private string responseApi ;
        private string returnUrl;
        
        public TranscriptViewModel()
        {
            MacKey = ConfigurationManager.AppSettings["InterswitchMacKey"];
            ProductId = Convert.ToInt32(ConfigurationManager.AppSettings["InterswitchProductId"]);
            PaymentItemId =  Convert.ToInt32(ConfigurationManager.AppSettings["InterswitchPaymentItemId"]);
            responseApi = ConfigurationManager.AppSettings["InterswitchResponseApi"];
            returnUrl = ConfigurationManager.AppSettings["InterswitchRetrunUrl"];

            StateSelectList = Utility.PopulateStateSelectListItem();
            CountrySelectList = Utility.PopulateCountrySelectListItem();
            TranscriptRequest = new TranscriptRequest();
            TranscriptRequests = new List<TranscriptRequest>();
            TranscriptRequest.student = new Model.Model.Student();
            _studentLogic = new StudentLogic();
            _personLogic = new PersonLogic();
            _paymentLogic = new PaymentLogic();
            _onlinePaymentLogic = new OnlinePaymentLogic();
            _transcriptRequestLogic = new TranscriptRequestLogic();
            _paymentInterswitchLogic = new PaymentInterswitchLogic();
            _remitaPaymentLogic = new RemitaPaymentLogic();

            ProgrammeSelectList = Utility.PopulateAllProgrammeSelectListItem();
        }
        public string RemitaBaseUrl { get; set; }
        public RemitaPayment RemitaPayment { get; set; }
        public string RemitaHash  { get; set; }
        public InterswitchResponse InterswitchResponse { get; set; }
        public PaymentInterswitch PaymentInterswitch { get; set; }
        public List<PaymentInterswitch> PaymentInterswitches { get; set; } 
        public List<RemitaPayment> RemitaPayments { get; set; } 
        public string BarcodeImageUrl { get; set; }
        public Model.Model.Student Student { get; set; }
        public TranscriptRequest TranscriptRequest { get; set; }
        public List<TranscriptRequest> TranscriptRequests { get; set; } 
        public List<SelectListItem> StateSelectList { get; set; }
        public List<SelectListItem> CountrySelectList { get; set; }
        public List<SelectListItem> ProgrammeSelectList { get; set; }
        public StudentLevel StudentLevel { get; set; }
        public Remita Remita { get; set; }
        public void GetRequestById(long Id)
        {
            TranscriptRequest = _transcriptRequestLogic.GetBy(Id);
            TranscriptRequest.ReturnUrl = returnUrl;
            if (TranscriptRequest != null && TranscriptRequest.payment.Id > 0)
            {
                TranscriptRequest.payment = _paymentLogic.GetBy(TranscriptRequest.payment.Id);
               
                if (TranscriptRequest.payment != null)
                {
                    TranscriptRequest.Amount =  Convert.ToString(TranscriptRequest.payment.FeeDetails.Sum(p => p.Fee.Amount));
                    TranscriptRequest.AmountInKobo = Convert.ToInt32(TranscriptRequest.payment.FeeDetails.Sum(p => p.Fee.Amount)*100);
                    string ToHash = TranscriptRequest.payment.InvoiceNumber + ProductId + PaymentItemId +
                                   TranscriptRequest.AmountInKobo +
                                    TranscriptRequest.ReturnUrl + MacKey;
                    TranscriptRequest.Hash = GetHash(ToHash);
                    TranscriptRequest.OnlinePayment = _onlinePaymentLogic.GetBy(TranscriptRequest.payment.Id);
                    TranscriptRequest.remitaPayment = _remitaPaymentLogic.GetBy(TranscriptRequest.payment.Id);

                }
            }
        }
        public void InitialiseStudent(long Id=0)
        {
            Student = _studentLogic.GetBy(TranscriptRequest.student.MatricNumber.Trim());
            if (Student != null)
            {
                TranscriptRequest.student = Student;
                if (Id == 0)
                {
                    TranscriptRequests = _transcriptRequestLogic.GetBy(Student);
                    if (TranscriptRequests.Count > 0)
                    {
                        TranscriptRequest = TranscriptRequests[0];
                    }

                }
                else
                {
                    GetRequestById(Id);
                    TranscriptRequests.Add(TranscriptRequest);
                }
               
                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                StudentLevel = studentLevelLogic.GetModelsBy(s => s.Person_Id == Student.Id).LastOrDefault();
            }
        }
        public void InitialiseStudentNewRequest()
        {
            Student = _studentLogic.GetBy(TranscriptRequest.student.MatricNumber.Trim());
            if (Student != null)
            {
                TranscriptRequest.student = Student;
            }
        }
        public void CreateStudent()
        {
            try
            {
                Person person = new Person();
                person.PersonType = new PersonType(){Id=4};
                person.LastName = TranscriptRequest.student.LastName;
                person.FirstName = TranscriptRequest.student.FirstName;
                person.OtherName = TranscriptRequest.student.OtherName;
                person.MobilePhone = TranscriptRequest.student.MobilePhone;
                person.Email = TranscriptRequest.student.Email ?? "support@fpno.edu.ng";
                person.State = new State(){Id = "IM"};
                person.Nationality = new Nationality(){ Id = 1};
                person.DateEntered = DateTime.Now;
                person.Role = new Role(){Id = 6};
                person = _personLogic.Create(person);

                Model.Model.Student student = new Model.Model.Student();
                student.Id = person.Id;
                student.MatricNumber = TranscriptRequest.student.MatricNumber.Trim();
                student.Type = new StudentType(){Id = 2};
                student.Status = new StudentStatus(){Id = 1};
                student.Category = new StudentCategory(){Id = 2};
                student = _studentLogic.CreateStudent(student);
                Student = student;
                TranscriptRequest.student = student;

                StudentLevelLogic studentLevelLogic = new StudentLevelLogic();
                SessionLogic sessionLogic = new SessionLogic();

                StudentLevel.Level = new Level(){ Id = (int)Levels.NDII };
                int[] hndProgrammes = { (int)Programmes.HNDEvening, (int)Programmes.HNDFullTime, (int)Programmes.HNDWeekend };
                if (StudentLevel != null && StudentLevel.Programme != null && StudentLevel.Programme.Id > 0 && hndProgrammes.Contains(StudentLevel.Programme.Id))
                {
                    StudentLevel.Level = new Level() { Id = (int)Levels.HNDII };
                }
                StudentLevel.Session = sessionLogic.GetModelsBy(s => s.Activated).LastOrDefault() ?? new Session(){Id = 3};
                StudentLevel.Student = Student;

                studentLevelLogic.Create(StudentLevel);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public void ProcessTranscriptRequest()
        {
            try
            {
                if (Student == null && TranscriptRequest.student.Id <= 0)
                {
                    InitialiseStudent();
                }
                else
                {
                    Student = TranscriptRequest.student;
                    if (!String.IsNullOrEmpty(Student.LastName) && !String.IsNullOrEmpty(Student.FirstName))
                    {
                        _personLogic.Modify(Student);
                    }
                    
                }

                if (TranscriptRequest.Id > 0)
                {
                    _transcriptRequestLogic.Modify(TranscriptRequest);
                }
                else
                {
                    FeeType feeType;
                    if (TranscriptRequest.DestinationCountry.Id == "NIG")
                    {
                        feeType = new FeeType() {Id = (int)FeeTypes.TranscriptLocal};
                    }
                    else
                    {
                         feeType = new FeeType() {Id = (int)FeeTypes.TranscriptForeign};
                    }
                   Payment payment = CreatePayment(Student,feeType);
                   TranscriptRequest.DateRequested = DateTime.Now;
                   TranscriptRequest.payment = payment;
                   TranscriptRequest.transcriptClearanceStatus = new TranscriptClearanceStatus(){TranscriptClearanceStatusId = 1};
                   TranscriptRequest.transcriptStatus = new TranscriptStatus(){TranscriptStatusId = 1};
                   TranscriptRequest = _transcriptRequestLogic.Create(TranscriptRequest);
                   TranscriptRequests.Add(TranscriptRequest);
                }
              
            }
            catch (Exception)
            {
                
                throw;
            }


        }
        private Payment CreatePayment(Model.Model.Student student, FeeType feeType)
            {
                try
                {
                    Payment payment = new Payment();
                   
                    payment.PaymentMode = new PaymentMode() { Id = 1 };
                    payment.PaymentType = new PaymentType() { Id = 2 };
                    payment.PersonType = new PersonType() { Id = 4 };
                    payment.FeeType = feeType;
                    payment.DatePaid = DateTime.Now;
                    payment.Person = student;
                    payment.Session = new Session { Id = 3 };

                    OnlinePayment newOnlinePayment = null;
                    Payment newPayment = _paymentLogic.Create(payment);
                    if (newPayment != null)
                    {
                        PaymentChannel channel = new PaymentChannel() { Id = (int)PaymentChannel.Channels.Etranzact };
                        OnlinePaymentLogic onlinePaymentLogic = new OnlinePaymentLogic();
                        OnlinePayment onlinePayment = new OnlinePayment();
                        onlinePayment.Channel = channel;
                        onlinePayment.Payment = newPayment;
                        newOnlinePayment = onlinePaymentLogic.Create(onlinePayment);
                    }

                    return newPayment;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        private string GetHash(string toHash)
        {
            try
            {
                
                System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
                Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(toHash));
                sha512.Clear();
                string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
                return hashed;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public void UpdateTranscriptPayment(string InovoiceNumber, string TransactionReference)
        {
            try
            {
                var payment = _paymentLogic.GetBy(InovoiceNumber);
                if (payment != null && payment.Id > 0)
                {
                   _onlinePaymentLogic.UpdateTransactionNumber(payment, TransactionReference);
                   var request = _transcriptRequestLogic.GetBy(payment);

                    int oldStatusId = request.transcriptStatus.TranscriptStatusId;
                    bool canSendMail = false;

                   request.transcriptStatus.TranscriptStatusId = 2;

                   if (oldStatusId != request.transcriptStatus.TranscriptStatusId)
                   {
                       canSendMail = true;
                   }

                   _transcriptRequestLogic.Modify(request);
                    TranscriptRequest = _transcriptRequestLogic.GetBy(payment);
                    int Amount = (int) (payment.FeeDetails.Sum(a => a.Fee.Amount)*100);
                    // TransactionStatus(responseApi, payment, Amount);

                    if (canSendMail)
                    {
                        SendMailToStudent(request);
                        if (request.transcriptStatus.TranscriptStatusId > 1)
                        {
                            SendMailToStaff(request);
                        }
                    }
                }
                
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        private void SendMailToStaff(Model.Model.TranscriptRequest request)
        {
            try
            {
                TranscriptRequestLogic requestLogic = new TranscriptRequestLogic();
                

                if (request.student != null && !string.IsNullOrEmpty(request.student.MatricNumber))
                {
                    int pendingRequestCount = 0;
                    string requestLink = "";
                    string userEmail = "";
                    if (request.student.MatricNumber.Split('/').Length > 0 && request.student.MatricNumber.Split('/').FirstOrDefault().Contains('H'))
                    {
                        List<TranscriptRequest> requests = requestLogic.GetModelsBy(r => r.STUDENT.Matric_Number.Contains("H"));
                        pendingRequestCount = requests.Count(r => r.student.MatricNumber.Split('/').FirstOrDefault().Contains("H"));
                        requestLink = "http://ndapplication.fpno.edu.ng/Admin/TranscriptProcessor/TranscripRequestsHnd";
                        userEmail = "ndtranscriptrequest@fpno.edu.ng";
                    }
                    else
                    {
                        List<TranscriptRequest> requests = requestLogic.GetModelsBy(r => r.STUDENT.Matric_Number.Contains("H"));
                        pendingRequestCount = requests.Count(r => !r.student.MatricNumber.Split('/').FirstOrDefault().Contains("H"));
                        requestLink = "http://ndapplication.fpno.edu.ng/Admin/TranscriptProcessor/Index";
                        userEmail = "hndtranscriptrequest@fpno.edu.ng";
                    }
                    //userEmail = "lawsgacc@gmail.com";
                    string messageBody =
                        "Hello, \n There is a new transcript request from the Federal Polytechnic, Nekede Transcript Module." +
                        "\n Follow this link to access the request: " + requestLink + "." +
                        "\n Total pending request: " + pendingRequestCount + ".";

                    EmailClient emailClient = new EmailClient("TRANSCRIPT REQUEST UPDATE", messageBody, userEmail);
                }

            }
            catch (Exception)
            {
                return;
            }
        }

        private void SendMailToStudent(Model.Model.TranscriptRequest request)
        {
            try
            {
                TranscriptStatusLogic transcriptStatusLogic = new TranscriptStatusLogic();
                TranscriptStatus status = transcriptStatusLogic.GetModelBy(t => t.Transcript_Status_Id == request.transcriptStatus.TranscriptStatusId);

                string messageBody = "Hello " + request.student.FullName + ", there has been an update on your transcript request from the Federal Polytechnic, Nekede." + "\n";
                messageBody += "Current Transcript Status: " + status.TranscriptStatusName + ".";

                EmailClient emailClient = new EmailClient("TRANSCRIPT REQUEST UPDATE", messageBody, request.student.Email);
                
            }
            catch (Exception)
            {
                throw;
            }
        }   
        public PaymentInterswitch TransactionStatus(string baseAddress, Payment payment,int amount)
        {
            var interswitchResponse = new PaymentInterswitch();
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                string toHash = ProductId + payment.InvoiceNumber + MacKey;
                string hash = GetHash(toHash);
                WebClient webClient = new WebClient();
                webClient.QueryString.Add("productid",ProductId.ToString());
                webClient.QueryString.Add("transactionreference",payment.InvoiceNumber);
                webClient.QueryString.Add("amount", amount.ToString());
                webClient.Headers["hash"] = hash;
                webClient.Headers["accept"] = "application/json";
                webClient.Headers["content-type"] = "application/json";
                string jsondata = webClient.DownloadString(baseAddress);
                interswitchResponse = new JavaScriptSerializer().Deserialize<PaymentInterswitch>(jsondata);
                if (!string.IsNullOrEmpty(interswitchResponse.ResponseCode))
                {
                    
                    var TransactionLog =   _paymentInterswitchLogic.GetBy(payment.InvoiceNumber);
                    if (TransactionLog == null)
                    {
                        interswitchResponse.Payment = payment;
                        if (interswitchResponse.TransactionDate.Year == 1)
                        {
                            interswitchResponse.TransactionDate = DateTime.Now;
                        }
                       interswitchResponse = _paymentInterswitchLogic.Create(interswitchResponse);
                        if (interswitchResponse.ResponseCode == "00")
                        {
                            interswitchResponse.Payment = payment;
                            var message = SendSimpleMessage(interswitchResponse);
                        }
                    }
                    else
                    {
                        interswitchResponse.Payment = payment;
                         if (interswitchResponse.TransactionDate.Year == 1)
                        {
                            interswitchResponse.TransactionDate = DateTime.Now;
                        }
                        _paymentInterswitchLogic.Modify(interswitchResponse);
                    }
                    interswitchResponse.Payment = payment;
                }

               

            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return interswitchResponse;
        }
          public static IRestResponse SendSimpleMessage (PaymentInterswitch transcriptRequest)
        {
            string MailGunAPI = ConfigurationManager.AppSettings["MailGunAPI"];
            string MailGunAPIKEY = ConfigurationManager.AppSettings["MailGunAPIKEY"];
            string htmlMail = "<html><body><div><h3>Payment Received</h3><table>";
            htmlMail+= "Hello " + transcriptRequest.Payment.Person.LastName + " , your payment for Transcript with reference " +transcriptRequest.Payment.InvoiceNumber;
            htmlMail+= " was successful. ";
            htmlMail += "</table></div></body></html>";

            RestClient client = new RestClient ();
            client.BaseUrl = new Uri (MailGunAPI);
            client.Authenticator = new HttpBasicAuthenticator ("api",MailGunAPIKEY);
            RestRequest request = new RestRequest ();
            request.AddParameter ("domain", "emails.lloydant.com", ParameterType.UrlSegment);
            request.Resource = "{domain}/messages";
            request.AddParameter ("from", "Payments <noreply@emails.lloydant.com>");
            request.AddParameter ("to", transcriptRequest.Payment.Person.Email);
            request.AddParameter ("subject", "FPNO TRANSCRIPT PAYMENT");
            request.AddParameter ("text", "Testing some Mailgun awesomness!");
            request.Method = Method.POST;
            return client.Execute (request);
        }
    
        public PaymentInterswitch TransactionStatus(string TransactionReference)
        {
            try
            {
                var payment = _paymentLogic.GetBy(TransactionReference);
                if (payment != null && payment.Id > 0)
                {
                  int Amount = (int) (payment.FeeDetails.Sum(a => a.Fee.Amount)*100);
                  return TransactionStatus(responseApi, payment, Amount);

                }
            }
            catch (Exception)
            {
                
                throw;
            }
            return null;
        }
        public PaymentInterswitch ReturnPaymentInterswitch(string TransactionReference)
        {
            return _paymentInterswitchLogic.GetBy(TransactionReference);
        }
    }
}