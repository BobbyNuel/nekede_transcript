﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Entity;
using System.Net;
using Abundance_Nk.Model.Entity;
using System.Xml;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;

using Abundance_Nk.Model.Model;
using Abundance_Nk.Business;
using System.Linq.Expressions;
using System.Web.Routing;
using Abundance_Nk.Web.Controllers;
using Abundance_Nk.Web.Models;
using System.Transactions;
using Abundance_Nk.Web.Areas.Applicant.ViewModels;

namespace Abundance_Nk.Web
{
    public partial class WebApi : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["payee_id"] != null && Request.QueryString["payment_type"] != null)
            {
                string payeeid = Request.QueryString["payee_id"];
                string payment_type = Request.QueryString["payment_type"];
                BuidXml(payeeid, payment_type);
            }
        }
        private void BuidXml(string InvoiceNumber, string paymentpurpose)
        {
            string url = "";
            try
            {
                string filename = InvoiceNumber.Replace("-", "").Replace("/", "").Replace(":", "").Replace(" ", "") + DateTime.Now.ToString().Replace("-", "").Replace("/", "").Replace(":", "").Replace(" ", "");
                url = "~/PayeeId/" + filename + ".xml";
                if (!Directory.Exists(Server.MapPath("~/PayeeId")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/PayeeId"));
                }

             

                Payment payment = new Payment();
                PaymentLogic paymentLogic = new PaymentLogic();
                payment = paymentLogic.GetBy(InvoiceNumber);
                if (payment == null)
                {
                    CreateErrorTree(url);
                    return;
                }

                //Abundance_Nk.Web.Areas.Applicant.Controllers.AdmissionController.CheckAndUpadteSupplementaryAcceptance(payment.InvoiceNumber);

                PaymentEtranzactType paymentEtranzactType = new PaymentEtranzactType();
                PaymentEtranzactTypeLogic paymentEtranzactTypeLogic = new PaymentEtranzactTypeLogic();
                paymentEtranzactType = paymentEtranzactTypeLogic.GetModelBy(m => m.Payment_Etranzact_Type_Name == paymentpurpose && m.Session_Id == payment.Session.Id && m.Fee_Type_Id == payment.FeeType.Id);
                if (paymentEtranzactType == null)
                {
                    CreateErrorTree(url);
                    return;
                }

                if (payment.PersonType.Id == 4)
                {
                    AppliedCourse appliedCourse = new AppliedCourse();
                    AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                    appliedCourse = appliedCourseLogic.GetModelBy(a => a.Person_Id == payment.Person.Id);
                    if (appliedCourse.Programme.Id == 5)
                    {
                        AdmissionListLogic listLogic = new AdmissionListLogic();
                        if (appliedCourse.ApplicationForm != null)
                        {
                            AdmissionList list = listLogic.GetModelsBy(l => l.Application_Form_Id == appliedCourse.ApplicationForm.Id).LastOrDefault();
                            if (list != null && list.Programme.Id == 5)
                            {
                                CreateErrorTree(url);
                                return;
                            }
                            else if (list == null)
                            {
                                CreateErrorTree(url);
                                return;
                            }
                        }
                        else 
                        {
                            CreateErrorTree(url);
                            return;
                        }
                    } 
                }  

                Person person = new Person();
                PersonLogic personLogic = new PersonLogic();

                person = personLogic.GetModelBy(p => p.Person_Id == payment.Person.Id);
                if (person != null)
                {
                    string fullname = person.FullName;
                    string faculty = "";
                    string dept = "";
                    string level = "";
                    string studenttypeid = "";
                    string modeofentry = "";
                    string sessionid = "";
                    string Amount = "";
                    string paymentstatus = "";
                    string PaymentType = "";
                    string phoneNo = person.MobilePhone;
                    string email = person.Email;
                    string MatricNo = ""; ;
                    string levelid = ""; ;
                    string PaymentCategory = ""; ;
                    string semester = "";
                    string semesterid = "";
                    if (payment.PersonType.Id == 4)
                    {
                        AppliedCourse appliedCourse = new AppliedCourse();
                        AppliedCourseLogic appliedCourseLogic = new AppliedCourseLogic();
                        appliedCourse = appliedCourseLogic.GetModelBy(m => m.Person_Id == payment.Person.Id);
                        PaymentEtranzactLogic etranzactLogic = new PaymentEtranzactLogic();

                        faculty = appliedCourse.Department.Faculty.Name;
                        dept = appliedCourse.Department.Name;
                        level = "N/A";
                        studenttypeid = "N/A";
                        modeofentry = "N/A";
                        sessionid = payment.Session.Name;

                        Amount = payment.FeeDetails.Sum(p => p.Fee.Amount).ToString();
                        paymentstatus = etranzactLogic.GetPaymentStatus(payment.InvoiceNumber);
                        PaymentType = paymentEtranzactType.Name; 
                        phoneNo = person.MobilePhone;
                        email = person.Email;
                        if (appliedCourse.ApplicationForm != null)
                        {
                            MatricNo = appliedCourse.ApplicationForm.Number; 
                        }
                        else
                        {
                            MatricNo = InvoiceNumber; 
                        } 

                        levelid = "N/A"; ;
                        PaymentCategory = paymentEtranzactType.Name;
                        semester = "N/A";
                    }
                    else
                    {
                        faculty = "";
                        dept = "";
                        level = "";
                        studenttypeid = "";
                        modeofentry = "";
                        sessionid = "";
                        Amount = "";
                        paymentstatus = "";
                        PaymentType = "";
                        phoneNo = person.MobilePhone;
                        email = person.Email;
                        MatricNo = ""; ;
                        levelid = ""; ;
                        PaymentCategory = ""; ;
                        semester = "";
                        semesterid = "";
                    }

                    CreateTree(fullname, faculty, dept, level, studenttypeid, modeofentry, sessionid, InvoiceNumber, Amount, paymentstatus, semester, PaymentType, MatricNo, email, phoneNo, PaymentCategory, url);

                    return;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return;
        }

        private void CreateTree(string payeename, string Faculty, string Department, string Level, string ProgrammeType, string StudyType, string Session, string PayeeID, string Amount, string FeeStatus, string Semester, string PaymentType, string MatricNumber, string Email, string PhoneNumber, string category, string url)
        {
            try
            {

                FileStream fs = new FileStream(Server.MapPath(url), FileMode.Create);
                fs.Close();


                XmlTextWriter writer = new XmlTextWriter(Server.MapPath(url), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 1;

                writer.WriteStartElement("FeeRequest");
                createNode(payeename, Faculty, Department, Level, ProgrammeType, StudyType, Session, PayeeID, Amount, FeeStatus, Semester, PaymentType, MatricNumber, Email, PhoneNumber, category, writer);
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                WriteXmlToPage(Server.MapPath(url));


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private void createNode(string payeeName, string Faculty, string Department, string Level, string ProgrammeType, string StudyType, string Session, string PayeeID, string Amount, string FeeStatus, string Semester, string PaymentType, string MatricNumber, string Email, string PhoneNumber, string category, XmlTextWriter writer)
        {
            try
            {

                writer.WriteStartElement("PayeeName");
                writer.WriteString(payeeName);
                writer.WriteEndElement();

                writer.WriteStartElement("Faculty");
                writer.WriteString(Faculty);
                writer.WriteEndElement();

                writer.WriteStartElement("Department");
                writer.WriteString(Department);
                writer.WriteEndElement();

                writer.WriteStartElement("Level");
                writer.WriteString(Level);
                writer.WriteEndElement();

                writer.WriteStartElement("ProgrammeType");
                writer.WriteString(ProgrammeType);
                writer.WriteEndElement();

                writer.WriteStartElement("StudyType");
                writer.WriteString(StudyType);
                writer.WriteEndElement();

                writer.WriteStartElement("Session");
                writer.WriteString(Session);
                writer.WriteEndElement();

                writer.WriteStartElement("PayeeID");
                writer.WriteString(PayeeID);
                writer.WriteEndElement();

                writer.WriteStartElement("Amount");
                writer.WriteString(Amount);
                writer.WriteEndElement();

                writer.WriteStartElement("FeeStatus");
                writer.WriteString(FeeStatus);
                writer.WriteEndElement();

                writer.WriteStartElement("Semester");
                writer.WriteString(Semester);
                writer.WriteEndElement();
               
                writer.WriteStartElement("PaymentType");
                writer.WriteString(PaymentType);
                writer.WriteEndElement();

               

                writer.WriteStartElement("PaymentCategory");
                writer.WriteString(category);
                writer.WriteEndElement();

                writer.WriteStartElement("MatricNumber");
                writer.WriteString(MatricNumber);
                writer.WriteEndElement();

                writer.WriteStartElement("Email");
                writer.WriteString(Email);
                writer.WriteEndElement();

                writer.WriteStartElement("PhoneNumber");
                writer.WriteString(PhoneNumber);
                writer.WriteEndElement();

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private void WriteXmlToPage(string url)
        {
            Response.Clear();
            Response.ContentType = "text/xml";
            Response.ContentEncoding = Encoding.UTF8;

            System.Net.WebRequest req = System.Net.WebRequest.Create(url);
            System.Net.WebResponse resp = req.GetResponse();
            System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
            System.Windows.Forms.RichTextBox t = new System.Windows.Forms.RichTextBox();
            t.Text = sr.ReadToEnd().Trim();
            sr.Close();

            for (int i = 0; i <= t.Lines.GetUpperBound(0); i++)
            {
                Response.Write(t.Lines[i].ToString());
            }

            File.Delete(url);
            Response.End();
        }

        private void CreateErrorTree(string url)
        {
            try
            {
                FileStream fs = new FileStream(Server.MapPath(url), FileMode.Create);
                fs.Close();

                XmlTextWriter writer = new XmlTextWriter(Server.MapPath(url), System.Text.Encoding.UTF8);
                writer.WriteStartDocument(true);
                writer.Formatting = Formatting.Indented;
                writer.Indentation = 1;

                writer.WriteStartElement("FeeRequest");
                writer.WriteStartElement("Error");
                writer.WriteString("-1");
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                WriteXmlToPage(Server.MapPath(url));


            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

     

    }
}